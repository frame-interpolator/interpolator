import cv2
import glob
import numpy as np
import os.path
from data.flow.flow_data_preprocessor import FlowDataPreprocessor


class KittiFlowDataPreprocessor(FlowDataPreprocessor):
    def __init__(self, directory, output_directory, validation_size=1, max_flow=1000.0, shard_size=1, verbose=False,
                 config=None):
        super().__init__(directory, output_directory, validation_size=validation_size, max_flow=max_flow,
                         shard_size=shard_size, verbose=verbose, config=config)

    def get_data_paths(self):
        """
        Gets the paths of [image_a, image_b, flow] tuples from a kitti dataset structure:
        self.directory
            <any number of directories>
                training
                    flow_occ
                        000000_10.png
                        ...
                    image_2
                        000000_10.png
                        000000_11.png
                        ...
        or for 2012:
        self.directory
            <any number of directories>
                training
                    flow_occ
                        000000_10.png
                        ...
                    colored_0
                        000000_10.png
                        000000_11.png
                        ...
        2012 and 2015 datasets can also be mixed.
        :return: List of image_path strings, list of flow_path strings, list of metadata.
        """
        images_a_2012, images_b_2012, flows_2012, metadata_2012 = self._get_kitti_data_paths('colored_0')
        images_a_2015, images_b_2015, flows_2015, metadata_2015 = self._get_kitti_data_paths('image_2')
        images_a = images_a_2012 + images_a_2015
        images_b = images_b_2012 + images_b_2015
        flow = flows_2012 + flows_2015
        metadata = metadata_2012 + metadata_2015
        return images_a, images_b, flow, metadata

    def _get_kitti_data_paths(self, images_dir_name='image_2'):
        """
        :param images_dir_name: Name of the folder containing color images. For kitti 2012, use 'colored_0'.
        :return: Same as get_data_paths.
        """
        # Get sorted lists.
        images = glob.glob(os.path.join(self.directory, '**', 'training', images_dir_name, '*.png'), recursive=True)
        if len(images) == 0:
            return [], [], [], []
        flow_base_dir = os.path.dirname(os.path.dirname(images[0]))
        flows = glob.glob(os.path.join(flow_base_dir, 'flow_occ', '*.png'), recursive=True)
        if self.verbose:
            print('Sorting file paths...')
        images.sort()
        flows.sort()
        if self.verbose:
            print('Filtering file paths...')
        assert len(images) == len(flows) * 2
        # Make sure the tuples are all under the same directory.
        filtered_images_a = []
        filtered_images_b = []
        filtered_flows = []
        meta_data = []
        for i in range(len(flows)):
            filtered_images_a.append(images[2 * i])
            filtered_images_b.append(images[2 * i + 1])
            filtered_flows.append(flows[i])
            meta_data.append(None)
            assert os.path.basename(filtered_images_a[-1]) == os.path.basename(filtered_flows[-1])
        return filtered_images_a, filtered_images_b, filtered_flows, meta_data

    def preprocess_example(self, image_a, image_b, flow, valid, meta_data):
        """
        Overridden.
        """
        _ = meta_data
        assert image_a.shape == image_b.shape
        assert image_a.shape[0] == flow.shape[0] and image_a.shape[1] == flow.shape[1]
        assert valid is not None and valid.dtype == np.bool
        assert image_a.dtype == np.uint8 and image_b.dtype == np.uint8 and flow.dtype == np.float32
        # Expected dimensions are the 2015 dimensions.
        # 2012 examples vary in height and width slightly, so resize if needed.
        expected_height, expected_width = 375, 1242
        original_height, original_width = image_a.shape[0], image_a.shape[1]
        if original_height != expected_height or original_width != expected_width:
            # Cast to the same type.
            image_a = image_a.astype(np.float32)
            image_b = image_b.astype(np.float32)
            valid = valid.astype(np.float32)
            # Jointly resize all images using nearest neighbour to preserve the valid mask.
            all = np.concatenate([image_a, image_b, flow, valid], axis=-1)
            all = cv2.resize(all, (expected_width, expected_height), cv2.INTER_NEAREST)
            assert all.shape[2] == 9
            image_a, image_b, flow, valid = all[..., :3], all[..., 3:6], all[..., 6:8], all[..., 8:]
            # Scale flow according to the modified width and height.
            flow *= np.asarray([[[expected_width / original_width,
                                  expected_height / original_height]]], dtype=np.float32)
            # Cast back to original type.
            assert np.max(valid) == 1.0 and np.min(valid) == 0.0
            assert np.max(all[..., :6]) <= 255.0 and np.min(all[..., :6]) >= 0
            image_a, image_b, flow, valid = image_a.astype(np.uint8), image_b.astype(np.uint8), flow, valid > 0.5
        return image_a, image_b, flow, valid
