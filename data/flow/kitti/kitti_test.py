import os
import os.path
import unittest
from data.flow.flow_data import FlowDataSet
from data.flow.kitti.kitti_preprocessor import KittiFlowDataPreprocessor
from data.flow.flow_data_test_base import TestFlowDataSet


class Kitti2015TestPaths:
    full_directory = os.path.join('data', 'flow', 'kitti', 'test_data', '2015', 'training')
    image_directory = os.path.join(full_directory, 'image_2')
    flow_directory = os.path.join(full_directory, 'flow_occ')
    expected_image_a_paths = [os.path.join(image_directory, '000000_10.png'),
                              os.path.join(image_directory, '000001_10.png'),
                              os.path.join(image_directory, '000002_10.png'),
                              os.path.join(image_directory, '000003_10.png'),
                              os.path.join(image_directory, '000004_10.png')]
    expected_image_b_paths = [os.path.join(image_directory, '000000_11.png'),
                              os.path.join(image_directory, '000001_11.png'),
                              os.path.join(image_directory, '000002_11.png'),
                              os.path.join(image_directory, '000003_11.png'),
                              os.path.join(image_directory, '000004_11.png')]
    expected_flow_paths = [os.path.join(flow_directory, '000000_10.png'),
                           os.path.join(flow_directory, '000001_10.png'),
                           os.path.join(flow_directory, '000002_10.png'),
                           os.path.join(flow_directory, '000003_10.png'),
                           os.path.join(flow_directory, '000004_10.png')]


class Kitti2012TestPaths:
    full_directory = os.path.join('data', 'flow', 'kitti', 'test_data', '2012', 'training')
    image_directory = os.path.join(full_directory, 'colored_0')
    flow_directory = os.path.join(full_directory, 'flow_occ')
    expected_image_a_paths = [os.path.join(image_directory, '000000_10.png'),
                              os.path.join(image_directory, '000001_10.png'),
                              os.path.join(image_directory, '000002_10.png'),
                              os.path.join(image_directory, '000003_10.png'),
                              os.path.join(image_directory, '000004_10.png')]
    expected_image_b_paths = [os.path.join(image_directory, '000000_11.png'),
                              os.path.join(image_directory, '000001_11.png'),
                              os.path.join(image_directory, '000002_11.png'),
                              os.path.join(image_directory, '000003_11.png'),
                              os.path.join(image_directory, '000004_11.png')]
    expected_flow_paths = [os.path.join(flow_directory, '000000_10.png'),
                           os.path.join(flow_directory, '000001_10.png'),
                           os.path.join(flow_directory, '000002_10.png'),
                           os.path.join(flow_directory, '000003_10.png'),
                           os.path.join(flow_directory, '000004_10.png')]


class TestKitti2015FlowDataSet(TestFlowDataSet.TestCases):
    def setUp(self):
        super().setUp()

        data_directory = os.path.join('data', 'flow', 'kitti', 'test_data', '2015')
        self.resolution = [375, 1242]
        # No data augmentation so that the tests are deterministic.
        self.data_set = FlowDataSet(data_directory, batch_size=2, training_augmentations=False)
        self.data_set_preprocessor = KittiFlowDataPreprocessor(data_directory, data_directory,
                                                               validation_size=1, shard_size=2)

        # Test paths.
        self.expected_image_a_paths = Kitti2015TestPaths.expected_image_a_paths
        self.expected_image_b_paths = Kitti2015TestPaths.expected_image_b_paths
        self.expected_flow_paths = Kitti2015TestPaths.expected_flow_paths


class TestKitti2012FlowDataSet(TestFlowDataSet.TestCases):
    def setUp(self):
        super().setUp()

        data_directory = os.path.join('data', 'flow', 'kitti', 'test_data', '2012')
        self.resolution = [375, 1242]
        # No data augmentation so that the tests are deterministic.
        self.data_set = FlowDataSet(data_directory, batch_size=2, training_augmentations=False)
        self.data_set_preprocessor = KittiFlowDataPreprocessor(data_directory, data_directory,
                                                               validation_size=1, shard_size=2)

        # Test paths.
        self.expected_image_a_paths = Kitti2012TestPaths.expected_image_a_paths
        self.expected_image_b_paths = Kitti2012TestPaths.expected_image_b_paths
        self.expected_flow_paths = Kitti2012TestPaths.expected_flow_paths


class TestKittiMixedFilePaths(unittest.TestCase):
    def test_mixed_file_paths(self):
        data_directory = os.path.join('data', 'flow', 'kitti', 'test_data')
        data_set_preprocessor = KittiFlowDataPreprocessor(data_directory, data_directory)
        images_a, images_b, flow, metadata = data_set_preprocessor.get_data_paths()
        self.assertEqual(len(images_a), len(metadata))
        self.assertListEqual(Kitti2012TestPaths.expected_image_a_paths + Kitti2015TestPaths.expected_image_a_paths,
                             images_a)
        self.assertListEqual(Kitti2012TestPaths.expected_image_b_paths + Kitti2015TestPaths.expected_image_b_paths,
                             images_b)
        self.assertListEqual(Kitti2012TestPaths.expected_flow_paths + Kitti2015TestPaths.expected_flow_paths, flow)


if __name__ == '__main__':
    unittest.main()
