import os.path
from common.utils.img import centered_crop
from data.flow.flow_data_preprocessor import FlowDataPreprocessor


class CreativeFlowDataPreprocessor(FlowDataPreprocessor):
    def __init__(self, directory, output_directory, validation_size=1, max_flow=1000.0, shard_size=1, verbose=False,
                 config=None):
        """
        Requires config to be in the following format:
        {
            "filelists": [
                "<path>/<to>/test_style.stylit_ALL.txt",
                "<path>/<to>/test_style.textured_ALL.txt",
                ...
            ],
            "whitelist": "<path>/<to>/whitelist_ALL.txt"
        }
        The whitelist is optional. If it is present, lines in the filelists will be
        checked against lines in whitelist file.
        Requires the following directory structure:
            self.directory
                mixamo
                shapenet
                web
                ...
        """
        super().__init__(directory, output_directory, validation_size=validation_size, max_flow=max_flow,
                         shard_size=shard_size, verbose=verbose, config=config)

    def get_data_paths(self):
        """
        :return: List of image_path strings, list of flow_path strings.
        """
        filelist_paths = self.config['filelists']
        whitelist = None
        if 'whitelist' in self.config:
            whitelist_file = self.config['whitelist']
            whitelist = {}
            with open(whitelist_file, 'r') as file:
                while True:
                    line = file.readline()
                    if not line:
                        break
                    if line == '':
                        break
                    whitelist[line] = None

        images_a = []
        images_b = []
        flows = []
        meta_data = []
        for filelist_path in filelist_paths:
            # Read file line by line.
            with open(filelist_path, 'r') as file:
                while True:
                    line = file.readline()
                    if not line:
                        break
                    if line == '':
                        break
                    if whitelist is not None and line not in whitelist:
                        continue
                    # Parse the line.
                    # Format is "mixamo/<path/to>/frame_a.png mixamo/<path/to>/frame_b.png mixamo/<path/to>/flow_ab.flo"
                    # There is an optional 4th field for objectids.txt.
                    paths = line.split()
                    assert len(paths) == 3 or len(paths) == 4
                    # Convert to absolute path based on README spec.
                    paths = [os.path.join(self.directory, path) for path in paths]
                    image_a, image_b, flow_ab = paths[0], paths[1], paths[2]
                    objid = None if len(paths) == 3 else paths[3]
                    # Assert that the files actually exist.
                    assert os.path.isfile(image_a)
                    assert os.path.isfile(image_b)
                    assert os.path.isfile(flow_ab)
                    # Append to output.
                    images_a.append(image_a)
                    images_b.append(image_b)
                    flows.append(flow_ab)
                    meta_data.append(objid)

        return images_a, images_b, flows, meta_data

    def preprocess_example(self, image_a, image_b, flow, valid, meta_data):
        """
        Overridden.
        """
        _ = meta_data
        image_a = centered_crop(image_a, 1000, 1000)
        image_b = centered_crop(image_b, 1000, 1000)
        flow = centered_crop(flow, 1000, 1000)
        if valid is not None:
            valid = centered_crop(valid, 1000, 1000)
        return image_a, image_b, flow, valid
