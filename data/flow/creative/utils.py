import numpy as np


def parse_objectids_foreground(objectids):
    """
    Parses the foreground from the objid image.
    :param objectids: Np matrix of shape [H, W, 3 or 4].
    :return: Foreground mask of shape [H, W, 1]. Values are 0.0 or 1.0.
    """
    if len(objectids.shape) >= 3 and objectids.shape[2] >= 4:
        alpha = objectids[:, :, 3:4]
    else:
        alpha = np.ones((objectids.shape[0], objectids.shape[1], 1), dtype=np.uint8)
    alpha = alpha > 0
    foreground = np.sum(objectids[:, :, 0:3], axis=2, keepdims=True) > 0
    foreground = np.logical_and(foreground, alpha)
    return foreground.astype(np.float32)
