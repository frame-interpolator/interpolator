import cv2
import os
import os.path
import unittest
from data.flow.flow_data import FlowDataSet
from data.flow.creative.creative_preprocessor import CreativeFlowDataPreprocessor
from data.flow.creative.utils import *
from data.flow.flow_data_test_base import TestFlowDataSet


class TestCreativeFlowDataSet(TestFlowDataSet.TestCases):
    def setUp(self):
        super().setUp()

        data_directory = os.path.join('data', 'flow', 'creative', 'test_data', 'test')
        mixamo_breakdance_dir = os.path.join(data_directory, 'mixamo', 'bigvegas.Breakdance_Freezes-50-7',
                                             'cam0', 'renders', 'composite', 'style.cpencil1.paint5')
        mixamo_evading_dir = os.path.join(data_directory, 'mixamo', 'bigvegas.Evading_A_Threat-33-31',
                                          'cam0', 'renders', 'composite', 'style.paint3.marker2')
        mixamo_breakdance_flow_dir = os.path.join(data_directory, 'mixamo', 'bigvegas.Breakdance_Freezes-50-7',
                                                  'cam0', 'metadata', 'flow')
        mixamo_evading_flow_dir = os.path.join(data_directory, 'mixamo', 'bigvegas.Evading_A_Threat-33-31',
                                               'cam0', 'metadata', 'flow')
        shapenet_dir = os.path.join(data_directory, 'shapenet', '03624134_dde6b5fad601b25e80b72a37a25b7e72',
                                    'cam0', 'renders', 'composite', 'style.textured1.ink6')
        shapenet_flow_dir = os.path.join(data_directory, 'shapenet',
                                         '03624134_dde6b5fad601b25e80b72a37a25b7e72', 'cam0', 'metadata', 'flow')
        config = {
            'filelists': [
                os.path.join('data', 'flow', 'creative', 'test_data', 'filelists', 'test_style.stylit_ALL.txt'),
                os.path.join('data', 'flow', 'creative', 'test_data', 'filelists', 'test_style.textured_ALL.txt')
            ]
        }
        self.resolution = [1000, 1000]
        # No data augmentation so that the tests are deterministic.
        self.data_set = FlowDataSet(data_directory, batch_size=2, training_augmentations=False)
        self.data_set_preprocessor = CreativeFlowDataPreprocessor(data_directory, data_directory,
                                                                  validation_size=1, shard_size=2, config=config)

        # Test paths.
        self.expected_image_a_paths = [os.path.join(mixamo_breakdance_dir, 'frame000171.png'),
                                       os.path.join(mixamo_evading_dir, 'frame000074.png'),
                                       os.path.join(shapenet_dir, 'frame000069.png'),
                                       os.path.join(shapenet_dir, 'frame000070.png'),
                                       os.path.join(shapenet_dir, 'frame000071.png')]
        self.expected_image_b_paths = [os.path.join(mixamo_breakdance_dir, 'frame000172.png'),
                                       os.path.join(mixamo_evading_dir, 'frame000075.png'),
                                       os.path.join(shapenet_dir, 'frame000070.png'),
                                       os.path.join(shapenet_dir, 'frame000071.png'),
                                       os.path.join(shapenet_dir, 'frame000072.png')]
        self.expected_flow_paths = [os.path.join(mixamo_breakdance_flow_dir, 'flow000171.flo'),
                                    os.path.join(mixamo_evading_flow_dir, 'flow000074.flo'),
                                    os.path.join(shapenet_flow_dir, 'flow000069.flo'),
                                    os.path.join(shapenet_flow_dir, 'flow000070.flo'),
                                    os.path.join(shapenet_flow_dir, 'flow000071.flo')]


class TestWhiteList(unittest.TestCase):
    def test_white_list(self):
        config = {
            'filelists': [
                os.path.join('data', 'flow', 'creative', 'test_data', 'filelists', 'test_style.stylit_ALL.txt'),
                os.path.join('data', 'flow', 'creative', 'test_data', 'filelists', 'test_style.textured_ALL.txt')
            ],
            'whitelist': os.path.join('data', 'flow', 'creative', 'test_data', 'whitelist.txt')
        }
        data_directory = os.path.join('data', 'flow', 'creative', 'test_data', 'test')
        data_set_preprocessor = CreativeFlowDataPreprocessor(data_directory, data_directory,
                                                             validation_size=1, shard_size=2, config=config)
        images_a, images_b, flows, meta_data = data_set_preprocessor.get_data_paths()
        self.assertEqual(3, len(images_a))
        self.assertEqual(3, len(images_b))
        self.assertEqual(3, len(flows))
        self.assertEqual(3, len(meta_data))


class TestUtils(unittest.TestCase):
    def test_parse_objid_foreground(self):
        objids = cv2.imread(os.path.join('data', 'flow', 'creative', 'test_data', 'objid.png'), cv2.IMREAD_UNCHANGED)
        self.assertTrue(3, len(objids.shape))
        foreground = parse_objectids_foreground(objids)
        self.assertTupleEqual((objids.shape[0], objids.shape[1], 1), foreground.shape)
        self.assertAlmostEqual(1.0, np.max(foreground))
        self.assertAlmostEqual(0.0, np.min(foreground))

    def test_parse_objid_foreground_no_alpha(self):
        objids = cv2.imread(os.path.join('data', 'flow', 'creative', 'test_data', 'objid.png'), cv2.IMREAD_UNCHANGED)
        foreground = parse_objectids_foreground(objids[:, :, 0:3])
        foreground2 = parse_objectids_foreground(objids)
        self.assertTupleEqual((objids.shape[0], objids.shape[1], 1), foreground.shape)
        self.assertAlmostEqual(1.0, np.max(foreground))
        self.assertAlmostEqual(0.0, np.min(foreground))
        self.assertTrue(np.allclose(foreground2, foreground))

    def test_parse_objid_all_alpha(self):
        objids = cv2.imread(os.path.join('data', 'flow', 'creative', 'test_data', 'objid.png'), cv2.IMREAD_UNCHANGED)
        self.assertTrue(3, len(objids.shape))
        self.assertTrue(4, objids.shape[2])
        foreground = parse_objectids_foreground(objids)
        objids[:, :, 3] = 255
        foreground2 = parse_objectids_foreground(objids)
        self.assertTrue(np.allclose(foreground2, foreground))


if __name__ == '__main__':
    unittest.main()
