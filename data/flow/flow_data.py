import os.path
from common.affine_transform.affine_transform import affine_transform
from common.utils.data import *
from common.utils.img import tf_random_crop, tf_image_augmentation
from common.utils.flow import tf_random_transform_flow
from data.dataset import DataSet


class FlowDataSet(DataSet):
    HEIGHT = 'height'
    WIDTH = 'width'
    IMAGE_A_RAW = 'image_a_raw'
    IMAGE_B_RAW = 'image_b_raw'
    FLOW_RAW = 'flow_raw'

    TRAIN_FILENAME = 'flowdataset_train.tfrecords'
    VALID_FILENAME = 'flowdataset_valid.tfrecords'

    def __init__(self, directory, batch_size=1, crop_size=None, training_augmentations=True, config=None,
                 parent=None):
        """
        :param directory: Str. Directory of the dataset file structure and tf records.
        :param batch_size: Int.
        :param crop_size: Tuple of (int (H), int (W)). Size to crop the training examples to before feeding to network.
                          If None, then no cropping will be performed.
        :param training_augmentations: Whether to do live augmentations while training.
        :param config: Configurations for data augmentation. If None, the default will be used.
        :param parent: FlowDataSet.
        """
        super().__init__(directory, batch_size, training_augmentations=training_augmentations, parent=parent)

        # Initialized during load().
        self.train_dataset = None  # Tensorflow DataSet object.
        self.valid_dataset = None  # Tensorflow DataSet object.
        self.handle_placeholder = None  # Handle placeholder for switching between datasets.
        self.train_handle = None  # Handle to feed for the training dataset.
        self.validation_handle = None  # Handle to feed for the validation dataset.
        self.iterator = None  # Iterator for getting the next batch.
        self.train_iterator = None  # Iterator for getting just the train data.
        self.validation_iterator = None  # Iterator for getting just the validation data.
        self.next_images_a = None  # Data iterator batch.
        self.next_images_b = None  # Data iterator batch.
        self.next_flows = None  # Data iterator batch.
        self.next_valid = None  # Data iterator batch.

        self.crop_size = crop_size

        self.config = config
        if config is None:
            self.config = {
                'contrast_min': 0.8, 'contrast_max': 1.25, 'contrast_noise': 0.00,
                'gamma_min': 0.8, 'gamma_max': 1.25, 'gamma_noise': 0.00,
                'gain_min': 0.8, 'gain_max': 1.25, 'gain_noise': 0.00,
                'brightness_stddev': 0.2, 'brightness_noise': 0.00,
                'color_min': 0.7, 'color_max': 1.4, 'color_noise': 0.00,
                'noise_stddev': 0.04,
                'zoom_min': 1.0, 'zoom_max': 1.2,
                'scale_min': 0.7, 'scale_max': 1.4,
                'do_scaling': True, 'flip_hor': True, 'flip_ver': True,
                'do_flipping': True,
                'do_rotating': True, 'rotate_min': -15.0, 'rotate_max': 15.0,
                'do_transform_difference': True,
                'zoom_noise': 0.03, 'scale_noise': 0.03, 'translate_x_noise': 3, 'translate_y_noise': 3,
                'rotate_noise': 3
            }

    def get_train_file_names(self):
        """
        Overridden.
        """
        return glob.glob(self._get_train_file_name_pattern())

    def get_validation_file_names(self):
        """
        :return: List of string.
        """
        return glob.glob(self._get_valid_file_name_pattern())

    def load(self, session):
        """
        Overridden.
        """
        if self.train_handle is not None and self.validation_handle is not None:
            return
        # Load parent dependency.
        if self.parent is not None:
            self.parent.load(session)

        with tf.name_scope('dataset_ops'):
            self.train_dataset = self._load_dataset(self._get_train_file_name_pattern(), True,
                                                    do_augmentations=self.training_augmentations)
            self.valid_dataset = self._load_dataset(self._get_valid_file_name_pattern(), False,
                                                    do_augmentations=False)

            if self.handle_placeholder is None:
                self.handle_placeholder = (tf.placeholder(tf.string, shape=[]) if self.parent is None else
                                           self.parent.handle_placeholder)
            self.iterator = tf.data.Iterator.from_string_handle(
                self.handle_placeholder, self.train_dataset.output_types, self.train_dataset.output_shapes)
            self.next_images_a, self.next_images_b, self.next_flows, self.next_valid = self.iterator.get_next()
            self.train_iterator = self.train_dataset.make_one_shot_iterator()
            self.validation_iterator = self.valid_dataset.make_initializable_iterator()

        self.train_handle = session.run(self.train_iterator.string_handle())
        self.validation_handle = session.run(self.validation_iterator.string_handle())

    def get_next_batch(self):
        """
        Overridden.
        """
        # next_valid corresponds to regions of the flow that are valid. Values of 1.0 indicate valid, and values of 0.0
        # indicate invalid.
        return self.next_images_a, self.next_images_b, self.next_flows, self.next_valid

    def get_train_feed_dict(self):
        """
        Overridden.
        """
        return {self.handle_placeholder: self.train_handle}

    def get_validation_feed_dict(self):
        """
        Overridden.
        """
        return {self.handle_placeholder: self.validation_handle}

    def init_validation_data(self, session):
        """
        Overridden
        """
        session.run(self.validation_iterator.initializer)

    def _get_train_file_name_pattern(self):
        """
        :return: Str.
        """
        return os.path.join(self.directory, '*' + self.TRAIN_FILENAME)

    def _get_valid_file_name_pattern(self):
        """
        :return: Str.
        """
        return os.path.join(self.directory, '*' + self.VALID_FILENAME)

    def _load_dataset(self, filename_pattern, repeat, do_augmentations=False):
        """
        :param filename_pattern: Str. Pattern for globbing file names.
        :param repeat: Bool. Whether to repeat the dataset indefinitely.
        :param do_augmentations: Bool. Whether to do image augmentations.
        :return: Tensorflow dataset object.
        """
        def _parse_function(example_proto):
            features = {
                FlowDataSet.HEIGHT: tf.FixedLenFeature((), tf.int64, default_value=0),
                FlowDataSet.WIDTH: tf.FixedLenFeature((), tf.int64, default_value=0),
                FlowDataSet.IMAGE_A_RAW: tf.FixedLenFeature((), tf.string),
                FlowDataSet.IMAGE_B_RAW: tf.FixedLenFeature((), tf.string),
                FlowDataSet.FLOW_RAW: tf.FixedLenFeature((), tf.string)
            }
            parsed_features = tf.parse_single_example(example_proto, features)
            H = tf.reshape(tf.cast(parsed_features[FlowDataSet.HEIGHT], tf.int32), ())
            W = tf.reshape(tf.cast(parsed_features[FlowDataSet.WIDTH], tf.int32), ())
            image_a = tf.cast(tf.decode_raw(parsed_features[FlowDataSet.IMAGE_A_RAW], tf.uint8), tf.float32) / 255.0
            image_a = tf.reshape(image_a, [H, W, 3])
            image_b = tf.cast(tf.decode_raw(parsed_features[FlowDataSet.IMAGE_B_RAW], tf.uint8), tf.float32) / 255.0
            image_b = tf.reshape(image_b, [H, W, 3])
            raw_flow = tf.decode_raw(parsed_features[FlowDataSet.FLOW_RAW], tf.float32)
            has_valid = tf.equal(tf.size(raw_flow), tf.cast(H * W * 3, tf.int32))
            raw_flow = tf.cond(has_valid, lambda: tf.reshape(raw_flow, [H, W, 3]),
                               lambda: tf.reshape(raw_flow, [H, W, 2]))
            valid = tf.cond(has_valid, lambda: raw_flow[..., 2:], lambda: tf.ones([H, W, 1], dtype=tf.float32))
            flow = tf.cond(has_valid, lambda: raw_flow[..., :2], lambda: raw_flow)

            # Cropping augmentation.
            image_a, image_b, flow, valid = tf_random_crop([image_a, image_b, flow, valid], self.crop_size)

            if do_augmentations:
                # Basic image augmentations.
                image_a, image_b = tf_image_augmentation([image_a, image_b], self.config)
                flow, images, transforms = tf_random_transform_flow(flow, [image_a, image_b], self.config)
                image_a, image_b = images
                valid = affine_transform(valid, transforms[0], is_optical_flow=False)
                # Compensate any areas of the flow that were decreased during bilinear sampling.
                flow /= valid + 1e-10
                # Accept only the pixels that are close to 1 after bilinear sampling during the affine transform.
                valid = tf.cast(valid > 0.95, tf.float32)

            return image_a, image_b, flow * valid, valid

        files = tf.data.Dataset.list_files(filename_pattern, shuffle=True)
        dataset = tf.data.TFRecordDataset(files, compression_type='GZIP', num_parallel_reads=min(4, self.batch_size))
        if repeat:
            dataset = dataset.repeat()
        dataset = dataset.map(_parse_function, num_parallel_calls=2)
        dataset = dataset.batch(self.batch_size)
        dataset = dataset.prefetch(1)
        return dataset

    def get_learning_rate(self):
        """
        :return: The learning rate associated with this flow dataset.
        """
        return self.config['learning_rate']
