import cv2
import multiprocessing
import numpy as np
import os.path
import shutil
import time
from common.utils import img
from joblib import Parallel, delayed


class _ProcessState:
    def __init__(self):
        # Frame index.
        self.frame_index = 0
        # Clip index.
        self.clip = 0
        # Current frame handle.
        self.frame = None
        # Previous frame handle.
        self.prev_frame = None
        # Number of frame skips.
        self.num_skips = 0
        # List of tuples (image_a, image_b, frame_index) to be batched into the GPU.
        self.queued_frames = []
        # Smoothed processing time per image.
        self.time_per_image = 0.0
        # Clip frame counter.
        self.clip_frame_count = {}

    def advance_frame(self):
        """
        Called when a new frame is read from the video stream (not during the actual processing of the frame).
        """
        self.frame_index += 1
        self.prev_frame = self.frame

    def add_time(self, time_per_image):
        """
        Called at the end of processing a batch of frames.
        :param time_per_image: Float.
        :return: Nothing.
        """
        self.time_per_image = 0.8 * self.time_per_image + 0.2 * time_per_image
        print('Processing ' + str(1 / self.time_per_image) + ' frames per second.', end='\r')

    def frame_outputted(self):
        """
        Called when a frame is queued to be written to disk.
        """
        self.num_skips = 0
        if self.clip not in self.clip_frame_count:
            self.clip_frame_count[self.clip] = 0
        self.clip_frame_count[self.clip] += 1

    def frame_skipped(self):
        """
        Called when a frame is skipped instead of being queued for write.
        """
        self.num_skips += 1

    def clip_changed(self):
        """
        Called when the clip changes.
        """
        self.clip += 1
        self.num_skips = 0

    def num_frames_in_clip(self, clip_id):
        """
        :param clip_id: Int.
        :return: Int.
        """
        if clip_id not in self.clip_frame_count:
            return 0
        return self.clip_frame_count[clip_id]


def process_video(compute_stats, num_downsamples_required, video_path, output_dir, batch_size=4, max_frame_skips=60,
                  min_max_flow=0.0, max_max_flow=500.0, min_avg_flow=0.0, max_diff_warp=0.50, min_diff=0.0005,
                  max_combined_diff=2.0, min_clip_length=6, desired_height=720, desired_width=1280):
    """
    Processes videos into separate clips. Directory structure is:
        <video_path>
            <clip 0>
                <frame>.png
                ...
            <clip 1>
            <clip 2>
            ...
    Files are saved in alpha-numeric order.
    :param compute_stats: Function. Inputs are 2 numpy images, outputs are avg_mag, max_mag, diff_warp, diff stats.
    :param num_downsamples_required: Int. Number of times the image is expected to be downsampled by the compute_stats
                                     function.
    :param video_path: Str.
    :param output_dir: Str.
    :param batch_size: Int. Number of frames to process on the GPU simultaneously.
    :param max_frame_skips: Int. Maximum number of skipped frames before a scene is cut to a new scene.
    :param min_max_flow: Float. Minimum max-flow-magnitude.
    :param max_max_flow: Float. Maximum max-flow-magnitude.
    :param min_avg_flow: Float. Minimum avg-flow-magnitude.
    :param max_diff_warp: Float. Maximum difference between images after warping them. If exceeded, a new clip will be
                          formed. Note that this is meant to be a hard cutoff, as most new clips will be triggered by
                          exceeding max_combined_diff.
    :param min_diff: Float. Minimum diff between 2 images (no warp).
    :param max_combined_diff: This is to primarily determine when the scene changes. It is defined by
                              warp_diff * diff * 100. warp_diff is a percent difference in the warped frames, and diff
                              is the absolute pixel difference between the two images without warping. Multiplying them
                              together balances any anomalies in either values.
    :param min_clip_length: Int. All shots that have less than this many frames are deleted.
    :param desired_height: Int. Frames will be resized to this height when read in. I.e. the output height.
    :param desired_width: Int. Frames will be resized to this width when read in. I.e. the output width.
    :return: Nothing
    """
    cap = cv2.VideoCapture(video_path)
    state = _ProcessState()

    def process_batch():
        if len(state.queued_frames) == 0:
            return

        start = time.time()
        # Resize the images.
        Parallel(n_jobs=multiprocessing.cpu_count(), backend='threading')(
            delayed(_resize_queued_frame)(i, state.queued_frames, desired_height, desired_width)
            for i in range(len(state.queued_frames))
        )
        # Get flow stats.
        preprocessed = Parallel(n_jobs=multiprocessing.cpu_count(), backend='threading')(
            delayed(_preprocess)(image_a, image_b, num_downsamples_required)
            for image_a, image_b, _ in state.queued_frames
        )
        images_a = [image for image, _ in preprocessed]
        images_b = [image for _, image in preprocessed]
        avg_mags, max_mags, diff_warps, diffs = compute_stats(images_a, images_b)

        writes = []
        prev_clips = []
        # Loops through the computed stats for each image.
        for i, (output_image, _, frame_index) in enumerate(state.queued_frames):
            avg_mag, max_mag, diff_warp, diff = avg_mags[i], max_mags[i], diff_warps[i], diffs[i]
            diff_combined = diff_warp * diff * 100.0  # 100 because diff_warp is a percentage.

            # Decide whether to skip the frame.
            if min_max_flow < max_mag <= max_max_flow and avg_mag > min_avg_flow and diff > min_diff:
                # Format is <frame>_<avg_mag>_<max_mag>_<diff_warp>_<diff>_<diff_combined>.png
                file_name = ('{0:0>7}'.format(frame_index - 1) + '_' + str(avg_mag) + '_' + str(max_mag) +
                             '_' + str(diff_warp) + '_' + str(diff) + '_' + str(diff_combined) + '.png')
                writes.append((_get_frame_dir(output_dir, state.clip), file_name, output_image))
                state.frame_outputted()
            else:
                state.frame_skipped()

            # New clip.
            new_clip = (diff_combined > max_combined_diff or diff_warp > max_diff_warp) and diff > min_diff
            if new_clip or state.num_skips > max_frame_skips:
                prev_clips.append(state.clip)
                state.clip_changed()

        # Do batched writes in parallel.
        if len(writes) > 0:
            Parallel(n_jobs=multiprocessing.cpu_count(), backend='threading')(
                delayed(_do_write)(frames_dir, file_name, output_image)
                for frames_dir, file_name, output_image in writes
            )

        # After writing all frames, delete any previous clips (not the current clip) if they aren't long enough.
        _maybe_delete_clips(output_dir, state, min_clip_length, prev_clips)

        # Empty the queue now that it is consumed.
        state.queued_frames = []

        # Time the code.
        end = time.time()
        state.add_time((end - start) / batch_size)

    while cap.isOpened():
        # Get frame.
        _, state.frame = cap.read()
        if state.frame is None:
            break
        assert state.frame.dtype == np.uint8

        if state.prev_frame is not None:
            if len(state.queued_frames) == batch_size:
                process_batch()
                assert len(state.queued_frames) == 0
            state.queued_frames.append((state.prev_frame, state.frame, state.frame_index - 1))

        state.advance_frame()

    process_batch()
    # Check if the last clip needs to be deleted.
    _maybe_delete_clips(output_dir, state, min_clip_length, [state.clip])
    print('')
    print('Done.')
    cap.release()


def _preprocess(image_a, image_b, num_downsamples_required):
    """
    Helper function to preprocess images for ingestion into compute_stats.
    :param image_a: Numpy image.
    :param image_b: Numpy image.
    :param num_downsamples_required: Int.
    :return: processed_a, processed_b numpy images.
    """
    processed_a = img.stretch_image_for_divisibility(image_a, num_downsamples_required)
    processed_a = cv2.cvtColor(processed_a, cv2.COLOR_BGR2RGB).astype(dtype=np.float32) / 255.0
    processed_b = img.stretch_image_for_divisibility(image_b, num_downsamples_required)
    processed_b = cv2.cvtColor(processed_b, cv2.COLOR_BGR2RGB).astype(dtype=np.float32) / 255.0
    return processed_a, processed_b


def _do_write(frames_dir, file_name, output_image):
    """
    Helper function to write images to disk.
    :param frames_dir: Str.
    :param file_name: Str.
    :param output_image: Numpy image.
    :return: Nothing.
    """
    os.makedirs(frames_dir, exist_ok=True)
    cv2.imwrite(os.path.join(frames_dir, file_name), output_image)


def _get_frame_dir(output_dir, clip):
    """
    :param output_dir: Str. Output directory for the whole video.
    :param clip: Int. Clip ID.
    :return: Str. Output directory for a frame (i.e. the clip directory).
    """
    return os.path.join(output_dir, 'frames', str(clip))


def _maybe_delete_clips(output_dir, state, min_clip_length, clips):
    """
    :param output_dir: Str. Output directory for the whole video.
    :param state: _ProcessState.
    :param min_clip_length: Int. The clip is deleted if its length is less than min_clip_length.
    :param clips: List of int (clip IDs) to delete.
    :return: Nothing.
    """
    for clip in clips:
        if state.num_frames_in_clip(clip) < min_clip_length:
            try:
                shutil.rmtree(_get_frame_dir(output_dir, clip))
            except Exception as e:
                print('Failed to remove clip directory.')
                print(e)


def _resize_queued_frame(i, queued_frames, height, width):
    """
    Helper to parallelize frame resizing.
    :param i: Int. Index of queued_frame to resize.
    :param queued_frames: List of tuples (image_a, image_b, frame_index).
    :return: Nothing.
    """
    image_a, image_b, frame_index = queued_frames[i]
    image_a = cv2.resize(image_a, (width, height), cv2.INTER_LINEAR)
    image_b = cv2.resize(image_b, (width, height), cv2.INTER_LINEAR)
    queued_frames[i] = (image_a, image_b, frame_index)
