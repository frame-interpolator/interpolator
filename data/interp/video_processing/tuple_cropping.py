import cv2
import numpy as np
from common.utils import img


def find_contours(image):
    """
    Runs cv2.findContours depending on the version of cv2.
    :param image: Image.
    :return: Contours.
    """
    if cv2.__version__.startswith('4.'):
        contours, _ = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    else:
        _, contours, _ = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    return contours


def crop_tuples_for_flow(images, crop_height, crop_width, reject_threshold=12.0):
    """
    Crops a tuple such that they contain "interesting" high flow pixels.
    :param images: list/tuple of images.
    :param crop_height: Int. Desired height.
    :param crop_width: Int. Desired width.
    :param reject_threshold: Float. Each frame in the triplet must differ by at least this much.
    :return: List of cropped images in the same order as the input images. None if the tuple was rejected.
    """
    # Use a canned OpenCV flow estimator. For this use case, it's probably faster and more reliable than our PWCNet.
    image_first_gray = cv2.cvtColor(images[0], cv2.COLOR_RGB2GRAY)
    image_last_gray = cv2.cvtColor(images[-1], cv2.COLOR_RGB2GRAY)
    forward_flow = cv2.calcOpticalFlowFarneback(image_first_gray, image_last_gray, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    backward_flow = cv2.calcOpticalFlowFarneback(image_last_gray, image_first_gray, None, 0.5, 3, 15, 3, 5, 1.2, 0)

    # Get the magnitude and stats.
    forward_mag = np.expand_dims(np.sqrt(np.square(forward_flow[..., 0]) + np.square(forward_flow[..., 1])),
                                 axis=-1)
    backward_mag = np.expand_dims(np.sqrt(np.square(backward_flow[..., 0]) + np.square(backward_flow[..., 1])),
                                  axis=-1)
    forward_mean = np.average(forward_mag)
    backward_mean = np.average(backward_mag)
    forward_std = np.std(forward_mag)
    backward_std = np.std(backward_mag)

    # Threshold the magnitude.
    forward_blob = np.ones_like(forward_mag, dtype=np.uint8) * 255
    forward_blob[forward_mag < (forward_mean + forward_std * 0.25)] = 0
    backward_blob = np.ones_like(backward_mag, dtype=np.uint8) * 255
    backward_blob[backward_mag < (backward_mean + backward_std * 0.25)] = 0

    # Clean it up.
    erode_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    forward_blob = cv2.erode(forward_blob, erode_kernel, iterations=1)
    backward_blob = cv2.erode(backward_blob, erode_kernel, iterations=1)
    dilate_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))
    forward_blob = cv2.dilate(forward_blob, dilate_kernel, iterations=1)
    backward_blob = cv2.dilate(backward_blob, dilate_kernel, iterations=1)

    # Contour detection.
    contours_forward = find_contours(forward_blob)
    contours_backward = find_contours(backward_blob)

    # Get the center of the largest contour in both images.
    max_forward_contour = max(contours_forward, key=cv2.contourArea)
    max_backward_contour = max(contours_backward, key=cv2.contourArea)
    forward_moments = cv2.moments(max_forward_contour)
    forward_x = forward_moments['m10'] / forward_moments['m00']
    forward_y = forward_moments['m01'] / forward_moments['m00']
    backward_moments = cv2.moments(max_backward_contour)
    backward_x = backward_moments['m10'] / backward_moments['m00']
    backward_y = backward_moments['m01'] / backward_moments['m00']

    # Use the average of the contours as the center of the crop.
    average_x = int((forward_x + backward_x) / 2)
    average_y = int((forward_y + backward_y) / 2)

    cropped = [img.crop_around_point(image, average_x, average_y, crop_height, crop_width) for image in images]
    for i in range(len(cropped) - 1):
        first = cropped[i]
        second = cropped[i + 1]
        diff = np.mean(np.abs(first - second))
        if diff <= reject_threshold:
            return None
    return cropped
