import unittest
from data.interp.video_processing.tuple_cropping import *


class TestTupleCropping(unittest.TestCase):
    def test_tuple_cropping_top_left(self):
        image_a = np.zeros(shape=(40, 50, 3), dtype=np.float32)
        image_b = np.zeros(shape=(40, 50, 3), dtype=np.float32)
        image_c = np.zeros(shape=(40, 50, 3), dtype=np.float32)

        image_a[0:20, 0:20] = 1.0
        image_b[5:15, 5:15] = 1.0
        image_c[5:25, 5:25] = 1.0

        cropped_a, cropped_b, cropped_c = crop_tuples_for_flow([image_a, image_b, image_c], 30, 40, reject_threshold=0)
        self.assertTupleEqual((30, 40, 3), cropped_a.shape)
        self.assertTupleEqual((30, 40, 3), cropped_b.shape)
        self.assertTupleEqual((30, 40, 3), cropped_c.shape)

        expected_a = np.zeros_like(cropped_a)
        expected_b = np.zeros_like(cropped_b)
        expected_c = np.zeros_like(cropped_c)
        expected_a[0:20, 0:20] = 1.0
        expected_b[5:15, 5:15] = 1.0
        expected_c[5:25, 5:25] = 1.0
        self.assertTrue(np.allclose(expected_a, cropped_a))
        self.assertTrue(np.allclose(expected_b, cropped_b))
        self.assertTrue(np.allclose(expected_c, cropped_c))

    def test_tuple_cropping_bottom_right(self):
        image_a = np.zeros(shape=(40, 50, 3), dtype=np.float32)
        image_b = np.zeros(shape=(40, 50, 3), dtype=np.float32)
        image_c = np.zeros(shape=(40, 50, 3), dtype=np.float32)

        image_a[35:40, 35:40] = 1.0
        image_b[38, 48] = 1.0
        image_c[30:35, 35:40] = 1.0

        cropped_a, cropped_b, cropped_c = crop_tuples_for_flow([image_a, image_b, image_c], 30, 40, reject_threshold=0)
        self.assertTupleEqual((30, 40, 3), cropped_a.shape)
        self.assertTupleEqual((30, 40, 3), cropped_b.shape)
        self.assertTupleEqual((30, 40, 3), cropped_c.shape)

        expected_a = np.zeros_like(cropped_a)
        expected_b = np.zeros_like(cropped_b)
        expected_c = np.zeros_like(cropped_c)
        expected_a[25:30, 25:30] = 1.0
        expected_b[28, 38] = 1.0
        expected_c[20:25, 25:30] = 1.0
        self.assertTrue(np.allclose(expected_a, cropped_a))
        self.assertTrue(np.allclose(expected_b, cropped_b))
        self.assertTrue(np.allclose(expected_c, cropped_c))

    def test_tuple_reject(self):
        image_a = np.zeros(shape=(40, 50, 3), dtype=np.float32)
        image_b = np.zeros(shape=(40, 50, 3), dtype=np.float32)
        image_c = np.zeros(shape=(40, 50, 3), dtype=np.float32)

        image_a[35:40, 35:40] = 1.0
        image_b[38, 48] = 1.0
        image_c[30:35, 35:40] = 1.0

        none_type = crop_tuples_for_flow([image_a, image_b, image_c], 30, 40, reject_threshold=1)
        self.assertEqual(None, none_type)


if __name__ == '__main__':
    unittest.main()
