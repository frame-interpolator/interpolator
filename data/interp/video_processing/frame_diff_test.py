import numpy as np
import unittest
from data.interp.video_processing.frame_diff import *


class FakePWCNet(PWCNet):
    def __init__(self):
        super().__init__()

    def get_forward(self, image_a, image_b, reuse_variables=tf.AUTO_REUSE, training=False):
        """
        Overridden
        :param image_a: Tensor.
        :param image_b: Tensor.
        :param reuse_variables: Tf reuse option.
        :param training: Bool.
        :return: final_flow, previous_flows.
        """
        B, H, W, _ = tf.unstack(tf.shape(image_a))
        return tf.ones(shape=(B, H, W, 2), dtype=tf.float32), [], {'features': []}


class TestPWCNetFrameDiff(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        self.model = FakePWCNet()
        out = get_pwcnet_diff_graph(self.model)
        self.image_a_input, self.image_b_input, self.avg_flow, self.max_flow, self.diff_warp, self.diff = out
        self.sess.run(tf.global_variables_initializer())

    def test_pwcnet_frame_diff(self):
        feed_dict = {
            self.image_a_input: np.ones([2, 128, 128, 3], dtype=np.float32),
            self.image_b_input: np.ones([2, 128, 128, 3], dtype=np.float32)
        }
        avg_flow, max_flow, diff_warp, diff = self.sess.run(
            [self.avg_flow, self.max_flow, self.diff_warp, self.diff], feed_dict=feed_dict)
        self.assertTupleEqual((2,), avg_flow.shape)
        self.assertTupleEqual((2,), max_flow.shape)
        self.assertTupleEqual((2,), diff_warp.shape)
        self.assertTupleEqual((2,), diff.shape)
        self.assertGreaterEqual(avg_flow[0], 0.0)
        self.assertGreaterEqual(max_flow[0], 0.0)
        self.assertGreaterEqual(diff_warp[0], 0.0)
        self.assertGreaterEqual(diff[0], 0.0)
        self.assertGreaterEqual(avg_flow[1], 0.0)
        self.assertGreaterEqual(max_flow[1], 0.0)
        self.assertGreaterEqual(diff_warp[1], 0.0)
        self.assertGreaterEqual(diff[1], 0.0)


class TestStatCompute(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_get_magnitude_stats(self):
        flow_a = tf.placeholder(shape=(2, 3, 3, 2), dtype=tf.float32)
        flow_b = tf.placeholder(shape=(2, 3, 3, 2), dtype=tf.float32)
        avg_flow_mag, max_flow_mag = get_flow_magnitude_stats(flow_a, flow_b)

        flow_a_np = np.ones(shape=(2, 3, 3, 2), dtype=np.float32)
        flow_a_np[1, ..., 1] = np.sqrt(3.0)
        flow_b_np = np.ones(shape=(2, 3, 3, 2), dtype=np.float32)
        flow_b_np[1, ..., 1] = np.sqrt(8.0)
        feed_dict = {
            flow_a: flow_a_np,
            flow_b: flow_b_np
        }
        avg, max = self.sess.run([avg_flow_mag, max_flow_mag], feed_dict=feed_dict)
        self.assertTupleEqual((2,), avg.shape)
        self.assertTupleEqual((2,), max.shape)
        self.assertAlmostEqual(np.sqrt(2.0), avg[0], places=5)
        self.assertAlmostEqual(2.5, avg[1])
        self.assertAlmostEqual(np.sqrt(2.0), max[0], places=5)
        self.assertAlmostEqual(3.0, max[1])

    def test_get_average_photometric_diff(self):
        image_a = tf.placeholder(shape=(2, 3, 3, 3), dtype=tf.float32)
        image_b = tf.placeholder(shape=(2, 3, 3, 3), dtype=tf.float32)
        avg_diff = get_average_photometric_diff(image_a, image_b)

        image_a_np = np.ones(shape=(2, 3, 3, 3), dtype=np.float32)
        image_b_np = np.ones(shape=(2, 3, 3, 3), dtype=np.float32)
        image_b_np[1, ..., 1] = -8.0
        feed_dict = {
            image_a: image_a_np,
            image_b: image_b_np
        }
        diff = self.sess.run(avg_diff, feed_dict=feed_dict)
        self.assertTupleEqual((2,), diff.shape)
        self.assertAlmostEqual(0.0, diff[0])
        self.assertAlmostEqual(3.0, diff[1])


if __name__ == '__main__':
    unittest.main()
