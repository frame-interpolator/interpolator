import tensorflow as tf
from pwcnet.model import PWCNet
from pwcnet.warp.warp import backward_warp


def get_pwcnet_diff_graph(model):
    """
    Image diffing graph for a batch of image pairs. Calculates diffs taking into account potential pixel movements.
    :param model: PWCNet.
    :return: image_a_input: Placeholder of shape [None, None, None, 3].
             image_b_input: Placeholder of shape [None, None, None, 3].
             avg_flow_mag: 1D Tensor.
             max_flow_mag: 1D Tensor.
             warp_diff: 1D Tensor. Measure of the difference between the 2 frames with flow taken into account.
             diff: 1D Tensor. Measure of difference between the 2 frames without flow.
    """
    assert isinstance(model, PWCNet)
    image_a_input = tf.placeholder(shape=[None, None, None, 3], dtype=tf.float32)
    image_b_input = tf.placeholder(shape=[None, None, None, 3], dtype=tf.float32)
    final_forward_flow, final_backward_flow, _, _, _, _ = model.get_bidirectional(image_a_input, image_b_input)

    avg_flow_mag, max_flow_mag = get_flow_magnitude_stats(final_forward_flow, final_backward_flow)
    warp_diff = _get_post_warp_percent_diff(image_a_input, image_b_input, final_forward_flow, final_backward_flow)
    diff = get_average_photometric_diff(image_a_input, image_b_input)

    return image_a_input, image_b_input, avg_flow_mag, max_flow_mag, warp_diff, diff


def get_flow_magnitude_stats(forward_flow, backward_flow):
    """
    :param forward_flow: Flow tensor.
    :param backward_flow: Flow tensor.
    :return: avg_flow_mag, max_flow_mag tensors.
    """
    with tf.name_scope('magnitude_stats'):
        forward_flow_mag = tf.sqrt(tf.square(forward_flow[..., 0]) + tf.square(forward_flow[..., 1]))
        backward_flow_mag = tf.sqrt(tf.square(backward_flow[..., 0]) + tf.square(backward_flow[..., 1]))

        avg_forward_flow_mag = tf.reduce_mean(forward_flow_mag, axis=[1, 2])
        max_forward_flow_mag = tf.reduce_max(forward_flow_mag, axis=[1, 2])
        avg_backward_flow_mag = tf.reduce_mean(backward_flow_mag, axis=[1, 2])
        max_backward_flow_mag = tf.reduce_max(backward_flow_mag, axis=[1, 2])
        avg_flow_mag = (avg_forward_flow_mag + avg_backward_flow_mag) / 2.0
        max_flow_mag = tf.maximum(max_forward_flow_mag, max_backward_flow_mag)

        return avg_flow_mag, max_flow_mag


def _get_post_warp_percent_diff(image_a, image_b, final_forward_flow, final_backward_flow):
    """
    Gets a diff after warping as a percentage of the images' magnitude.
    :param image_a: Image tensor.
    :param image_b: Image tensor.
    :param final_forward_flow: Flow tensor.
    :param final_backward_flow: Flow tensor.
    :return: Diff tensor.
    """
    with tf.name_scope('post_warp_percent_diff'):
        ones = tf.ones_like(image_a, dtype=tf.float32)

        warp_b_to_a = backward_warp(image_b, final_forward_flow)
        warp_b_to_a_mask = backward_warp(ones, final_forward_flow)
        warp_a_to_b = backward_warp(image_a, final_backward_flow)
        warp_a_to_b_mask = backward_warp(ones, final_backward_flow)

        mean_a, variance_a = tf.nn.moments(image_a, axes=[1, 2, 3])
        std_dev_a = tf.sqrt(variance_a)
        diff_a = tf.reduce_mean(tf.abs(image_a - warp_b_to_a) * warp_b_to_a_mask, axis=[1, 2, 3])
        diff_a = diff_a / (mean_a + std_dev_a * 2.0 + 1e-10)

        mean_b, variance_b = tf.nn.moments(image_b, axes=[1, 2, 3])
        std_dev_b = tf.sqrt(variance_b)
        diff_b = tf.reduce_mean(tf.abs(image_b - warp_a_to_b) * warp_a_to_b_mask, axis=[1, 2, 3])
        diff_b = diff_b / (mean_b + std_dev_b * 2.0 + 1e-10)

        return (diff_a + diff_b) / 2.0


def get_average_photometric_diff(image_a, image_b):
    """
    :param image_a: Image tensor.
    :param image_b: Image tensor.
    :return: Diff tensor.
    """
    with tf.name_scope('average_photometric_diff'):
        return tf.reduce_mean(tf.abs(image_a - image_b), axis=[1, 2, 3])
