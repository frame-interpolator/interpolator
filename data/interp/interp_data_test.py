import os
import os.path
import shutil
import unittest
import numpy as np
import tensorflow as tf
from common.utils.img import write_image
from data.interp.interp_data import InterpDataSet, InterpAugmentations
from data.interp.interp_data_preprocessor import InterpDataPreprocessor


class TestInterpDataSet(unittest.TestCase):
    def setUp(self):
        self.cur_dir = os.path.dirname(__file__)
        self.data_directory = os.path.join(self.cur_dir, 'test_data', 'davis')
        self.extra_val_directory = os.path.join(self.cur_dir, 'test_data', 'extra_val')
        self.tf_record_directory = os.path.join(self.data_directory, 'tfrecords')

        # Test folder for writing images.
        self.test_images_folder = os.path.join('output_test_images', 'interp_data')
        if not os.path.exists(self.test_images_folder):
            os.makedirs(self.test_images_folder)

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_maximum_shot_len(self):
        preprocessor = InterpDataPreprocessor(self.tf_record_directory, [[1]])
        image_paths = [
            ['a0', 'a1', 'a2'],
            ['b0', 'b1', 'b2', 'b3', 'b4', 'b5', 'b6'],
            ['c1', 'c2', 'c3', 'c4']
        ]
        expected_split = [
            ['a0', 'a1', 'a2'],
            ['b0', 'b1', 'b2'],
            ['b3', 'b4', 'b5'],
            ['b6'],
            ['c1', 'c2', 'c3'],
            ['c4']
        ]
        split_paths = preprocessor._enforce_maximum_shot_len(image_paths, 3)
        self.assertListEqual(split_paths, expected_split)

    def test_val_split(self):
        # Sequences of lengths 3, 4, 5.
        preprocessor = InterpDataPreprocessor(self.tf_record_directory, [[1], [1, 0], [1, 0, 0]])
        image_paths = [
            ['a0', 'a1', 'a2'],
            ['b0', 'b1', 'b2', 'b3', 'b4', 'b5', 'b6'],
            ['c1', 'c2', 'c3', 'c4']
        ]
        expected_val = [
            ['a0', 'a1', 'a2'],
            ['b0', 'b1', 'b2', 'b3']
        ]
        expected_train = [
            ['b4', 'b5', 'b6'],
            ['c1', 'c2', 'c3', 'c4']
        ]
        val, train = preprocessor._split_for_validation(image_paths, 4)
        self.assertListEqual(val, expected_val)
        self.assertListEqual(train, expected_train)

    def test_val_split_all(self):

        # Sequences of lengths 3, 4, 5.
        preprocessor = InterpDataPreprocessor(self.tf_record_directory, [[1], [1, 0], [1, 0, 0]])
        image_paths = [
            ['a0', 'a1', 'a2'],
            ['b0', 'b1', 'b2', 'b3', 'b4', 'b5', 'b6'],
            ['c1', 'c2', 'c3', 'c4']
        ]
        expected_val = [
            ['a0', 'a1', 'a2'],
            ['b0', 'b1', 'b2', 'b3', 'b4', 'b5', 'b6'],
            ['c1', 'c2', 'c3', 'c4']
        ]
        expected_train = []
        val, train = preprocessor._split_for_validation(image_paths, 200)
        self.assertListEqual(val, expected_val)
        self.assertListEqual(train, expected_train)

    def test_data_read_write(self):
        augmentations = InterpAugmentations(crop_size=(256, 256))
        data_set = InterpDataSet(self.tf_record_directory, [[1]], batch_size=2, augmentations=augmentations)
        preprocessor = InterpDataPreprocessor(self.tf_record_directory, [[1]], shard_size=1)
        preprocessor.preprocess_raw(self.data_directory)

        output_paths = data_set.get_tf_record_names()
        [self.assertTrue(os.path.isfile(output_path)) for output_path in output_paths]

        # For a shard size of 1 the number of files is the number of video shots (or the number of sub-folders).
        self.assertEqual(len(output_paths), 2)

        data_set.load(self.sess)

        next_sequence_tensor, next_sequence_timing_tensor = data_set.get_next_batch()

        # There are 6 valid sequences in total, and we are using a batch size of 2.
        allowable_times = [[0.0, 0.5, 1.0]]
        for i in range(3):
            query = [next_sequence_tensor, next_sequence_timing_tensor]
            next_sequence, next_sequence_timing = self.sess.run(query,
                                                                feed_dict=data_set.get_train_feed_dict())
            self.assertTrue(self._is_list_allowable(next_sequence_timing[0].tolist(), allowable_times))
            self.assertTrue(self._is_list_allowable(next_sequence_timing[1].tolist(), allowable_times))
            self.assertTupleEqual(np.shape(next_sequence), (2, 3, 256, 256, 3))

    def test_data_read_write_duplicated_end_frames(self):
        out_folder = os.path.join(self.test_images_folder, 'endpoint_dup')
        if os.path.exists(out_folder):
            shutil.rmtree(out_folder)
        os.mkdir(out_folder)

        augmentations = InterpAugmentations(flip_prob=0.0, duplicate_endpoint_frames_prob=1.0, do_center_crop=True,
                                            crop_size=(256, 256))
        data_set = InterpDataSet(self.tf_record_directory, [[1]], batch_size=2,
                                 augmentations=augmentations)
        preprocessor = InterpDataPreprocessor(self.tf_record_directory, [[1]], shard_size=1)
        preprocessor.preprocess_raw(self.data_directory)

        output_paths = data_set.get_tf_record_names()
        [self.assertTrue(os.path.isfile(output_path)) for output_path in output_paths]

        # For a shard size of 1 the number of files is the number of video shots (or the number of sub-folders).
        self.assertEqual(len(output_paths), 2)

        data_set.load(self.sess)
        next_sequence_tensor, next_sequence_timing_tensor = data_set.get_next_batch()

        # There are 6 valid sequences in total, and we are using a batch size of 2.
        allowable_times = [[0.0, 0.0, 1.0], [0.0, 1.0, 1.0]]
        for k in range(3):
            query = [next_sequence_tensor, next_sequence_timing_tensor]
            next_sequence, next_sequence_timing = self.sess.run(query,
                                                                feed_dict=data_set.get_train_feed_dict())
            self.assertTrue(self._is_list_allowable(next_sequence_timing[0].tolist(), allowable_times))
            self.assertTrue(self._is_list_allowable(next_sequence_timing[1].tolist(), allowable_times))
            self.assertTupleEqual(np.shape(next_sequence), (2, 3, 256, 256, 3))
            for i in range(2):
                if next_sequence_timing[i].tolist() == allowable_times[0]:
                    cur_times = allowable_times[0]
                    self.assertEqual(0.0, np.sum(np.abs(next_sequence[i][0] - next_sequence[i][1])))
                else:
                    self.assertListEqual(next_sequence_timing[i].tolist(), allowable_times[1])
                    cur_times = allowable_times[1]
                    self.assertEqual(0.0, np.sum(np.abs(next_sequence[i][1] - next_sequence[i][2])))

                for j in range(3):
                    img_name = 'batch' + str(k) + '-sample' + str(i) + '-idx' + str(j) + '-' + str(cur_times[j]) + '.jpg'
                    img_path = os.path.join(out_folder, img_name)
                    write_image(img_path, next_sequence[i][j])

    def test_val_data_read_write(self):
        data_set = InterpDataSet(self.tf_record_directory, [[1]], batch_size=1)
        preprocessor = InterpDataPreprocessor(self.tf_record_directory, [[1], [1, 0], [1, 0, 0]], shard_size=5,
                                              validation_size=1)
        preprocessor.preprocess_raw(self.data_directory)

        output_paths = data_set.get_tf_record_names()
        [self.assertTrue(os.path.isfile(output_path)) for output_path in output_paths]

        # We're using a larger shard size, so there should be 1 path for val and 1 for train.
        self.assertEqual(len(output_paths), 2)
        data_set.load(self.sess)

        data_set.init_validation_data(self.sess)
        next_sequence_tensor, next_sequence_timing_tensor = data_set.get_next_batch()

        query = [next_sequence_tensor, next_sequence_timing_tensor]
        next_sequence, next_sequence_timing = self.sess.run(query,
                                                            feed_dict=data_set.get_validation_feed_dict())

        self.assertListEqual(next_sequence_timing[0].tolist(), [0.0, 0.5, 1.0])
        self.assertTupleEqual(np.shape(next_sequence), (1, 3, 264, 470, 3))
        self.assertLessEqual(np.max(next_sequence), 1.0)

        out_folder = os.path.join(self.test_images_folder, 'simple')
        if os.path.exists(out_folder):
            shutil.rmtree(out_folder)
        os.mkdir(out_folder)
        times = [0.0, 0.5, 1.0]
        for j in range(3):
            img_path = os.path.join(out_folder, str(times[j]) + '.jpg')
            write_image(img_path, next_sequence[0][j])

        end_of_val = False
        try:
            _, _ = self.sess.run(query, feed_dict=data_set.get_validation_feed_dict())
        except tf.errors.OutOfRangeError:
            end_of_val = True

        self.assertTrue(end_of_val)

    def test_val_np_data_read_write(self):
        data_set = InterpDataSet(self.tf_record_directory, [[1]], batch_size=1,
                                 extra_validation_dir=os.path.join(self.extra_val_directory),
                                 augmentations=InterpAugmentations(256, 258))
        preprocessor = InterpDataPreprocessor(self.tf_record_directory, [[1], [1, 0], [1, 0, 0]], shard_size=5,
                                              validation_size=2)
        preprocessor.preprocess_raw(self.data_directory)

        output_paths = data_set.get_tf_record_names()
        [self.assertTrue(os.path.isfile(output_path)) for output_path in output_paths]

        # We're using a larger shard size, so there should be 1 path for val and 1 for train.
        self.assertEqual(len(output_paths), 2)
        data_set.load(self.sess)

        data_set.init_validation_data(self.sess)
        next_sequence_tensor, next_sequence_timing_tensor = data_set.get_next_batch()

        query = [next_sequence_tensor, next_sequence_timing_tensor]

        # TFRecords validation set.
        end_of_val = False
        try:
            _, _ = self.sess.run(query, feed_dict=data_set.get_validation_feed_dict())
            self.assertTrue(not end_of_val)
            _, _ = self.sess.run(query, feed_dict=data_set.get_validation_feed_dict())
            self.assertTrue(not end_of_val)
            _, _ = self.sess.run(query, feed_dict=data_set.get_validation_feed_dict())
        except tf.errors.OutOfRangeError:
            end_of_val = True

        self.assertTrue(end_of_val)

        # Numpy validation set.
        hit_end = data_set.ready_next_np_validation_element(self.sess)
        self.assertTrue(not hit_end)
        next_sequence, next_sequence_timing = self.sess.run(query,
                                                            feed_dict=data_set.get_validation_np_feed_dict())

        # The extra validation images should not be cropped.
        self.assertListEqual(next_sequence_timing[0].tolist(), [0.0, 0.5, 1.0])
        self.assertTupleEqual(np.shape(next_sequence), (1, 3, 768, 1280, 3))
        self.assertLessEqual(np.max(next_sequence), 1.0)

        out_folder = os.path.join(self.test_images_folder, 'simple_np')
        if os.path.exists(out_folder):
            shutil.rmtree(out_folder)
        os.mkdir(out_folder)
        times = [0.0, 0.5, 1.0]
        for j in range(3):
            img_path = os.path.join(out_folder, str(times[j]) + '.jpg')
            write_image(img_path, next_sequence[0][j])

        hit_end = data_set.ready_next_np_validation_element(self.sess)
        next_sequence, _ = self.sess.run(query, feed_dict=data_set.get_validation_np_feed_dict())
        self.assertTupleEqual(np.shape(next_sequence), (1, 3, 768, 1280, 3))
        self.assertTrue(hit_end)

    def test_data_read_write_crop(self):
        augmentations = InterpAugmentations(crop_size=(220, 200))
        data_set = InterpDataSet(self.tf_record_directory, [[1]], batch_size=2,
                                 augmentations=augmentations)
        preprocessor = InterpDataPreprocessor(self.tf_record_directory, [[1]], crop_size=(224, 220))
        preprocessor.preprocess_raw(self.data_directory)

        output_paths = data_set.get_tf_record_names()
        [self.assertTrue(os.path.isfile(output_path)) for output_path in output_paths]

        # For a shard size of 1 the number of files is the number of video shots (or the number of sub-folders).
        self.assertEqual(len(output_paths), 2)

        data_set.load(self.sess)

        next_sequence_tensor, next_sequence_timing_tensor = data_set.get_next_batch()

        # There are 6 valid sequences in total, and we are using a batch size of 2.
        allowable_times = [[0.0, 0.5, 1.0]]
        for i in range(3):
            query = [next_sequence_tensor, next_sequence_timing_tensor]
            next_sequence, next_sequence_timing = self.sess.run(query,
                                                                feed_dict=data_set.get_train_feed_dict())
            self.assertTrue(self._is_list_allowable(next_sequence_timing[0].tolist(), allowable_times))
            self.assertTrue(self._is_list_allowable(next_sequence_timing[1].tolist(), allowable_times))
            self.assertTupleEqual(np.shape(next_sequence), (2, 3, 220, 200, 3))

    def test_data_read_write_multi(self):
        """
        Tests for the case where multiple inbetween_location configs are provided.
        """
        # Setup output folder where we write test images.
        out_folder = os.path.join(self.test_images_folder, 'multi_inbetween_locs')
        if os.path.exists(out_folder):
            shutil.rmtree(out_folder)
        os.mkdir(out_folder)

        augmentations = InterpAugmentations(crop_size=(256, 256))
        data_set = InterpDataSet(self.tf_record_directory, [[1], [1, 0, 0]], batch_size=1,
                                 augmentations=augmentations)
        preprocessor = InterpDataPreprocessor(self.tf_record_directory, [[1], [1, 0], [1, 0, 0]], shard_size=1)
        preprocessor.preprocess_raw(self.data_directory)

        output_paths = data_set.get_tf_record_names()
        [self.assertTrue(os.path.isfile(output_path)) for output_path in output_paths]

        # For a shard size of 1 the number of files is the number of video shots (or the number of sub-folders).
        self.assertEqual(len(output_paths), 2)

        data_set.load(self.sess)

        next_sequence_tensor, next_sequence_timing_tensor = data_set.get_next_batch()

        # There are 6 + 4 valid sequences in total (different inbetweening locations),
        # and we are using a batch size of 1.
        # For the sparse sequence, the dog-agility shot is not long enough, so a Tensor of zeros will be returned.
        num_dense_sequences = 0
        num_sparse_sequences = 0
        for i in range(10):
            query = [next_sequence_tensor, next_sequence_timing_tensor]
            next_sequence, next_sequence_timing = self.sess.run(query,
                                                                feed_dict=data_set.get_train_feed_dict())

            is_sparse = False
            if next_sequence_timing[0].tolist() == [0.0, 0.5, 1.0]:
                num_dense_sequences += 1
                is_sparse = False
            elif next_sequence_timing[0].tolist() == [0.0, 0.25, 1.0]:
                num_sparse_sequences += 1
                is_sparse = True

            self.assertTupleEqual(np.shape(next_sequence), (1, 3, 256, 256, 3))
            times = [0.0, 0.25, 1.0] if is_sparse else [0.0, 0.5, 1.0]
            for j in range(3):
                img_path = os.path.join(out_folder, 'sample-' + str(i) + '-' + str(times[j]) + '.jpg')
                write_image(img_path, next_sequence[0][j])

        self.assertEqual(num_dense_sequences, 6)
        self.assertEqual(num_sparse_sequences, 4)

    def test_data_read_write_multi_flip(self):
        """
        Tests for the case where multiple inbetween_location configs are provided.
        The x, y, and temporal axes are also flipped for this test.
        """
        # Setup output folder where we write test images.
        out_folder = os.path.join(self.test_images_folder, 'multi_inbetween_locs_flip')
        if os.path.exists(out_folder):
            shutil.rmtree(out_folder)
        os.mkdir(out_folder)

        augmentations = InterpAugmentations(flip_prob=1.0, crop_size=(256, 256))
        data_set = InterpDataSet(self.tf_record_directory, [[1], [1, 0, 0]], batch_size=1, augmentations=augmentations)
        preprocessor = InterpDataPreprocessor(self.tf_record_directory, [[1], [1, 0], [1, 0, 0]], shard_size=1)
        preprocessor.preprocess_raw(self.data_directory)

        output_paths = data_set.get_tf_record_names()
        [self.assertTrue(os.path.isfile(output_path)) for output_path in output_paths]

        # For a shard size of 1 the number of files is the number of video shots (or the number of sub-folders).
        self.assertEqual(len(output_paths), 2)

        data_set.load(self.sess)

        next_sequence_tensor, next_sequence_timing_tensor = data_set.get_next_batch()

        # There are 6 + 4 valid sequences in total (different inbetweening locations),
        # and we are using a batch size of 1.
        # For the sparse sequence, the dog-agility shot is not long enough, so a Tensor of zeros will be returned.
        num_dense_sequences = 0
        num_sparse_sequences = 0
        for i in range(10):
            query = [next_sequence_tensor, next_sequence_timing_tensor]
            next_sequence, next_sequence_timing = self.sess.run(query,
                                                                feed_dict=data_set.get_train_feed_dict())

            is_sparse = False
            if next_sequence_timing[0].tolist() == [0.0, 0.5, 1.0]:
                num_dense_sequences += 1
                is_sparse = False
            elif next_sequence_timing[0].tolist() == [0.0, 0.75, 1.0]:
                num_sparse_sequences += 1
                is_sparse = True

            self.assertTupleEqual(np.shape(next_sequence), (1, 3, 256, 256, 3))
            times = [0.0, 0.75, 1.0] if is_sparse else [0.0, 0.5, 1.0]
            for j in range(3):
                img_path = os.path.join(out_folder, 'sample-' + str(i) + '-' + str(times[j]) + '.jpg')
                write_image(img_path, next_sequence[0][j])

        self.assertEqual(num_dense_sequences, 6)
        self.assertEqual(num_sparse_sequences, 4)

    def test_data_paths_davis(self):
        expected_image_paths_0 = [
            os.path.join(self.data_directory, 'breakdance-flare', '00000.jpg'),
            os.path.join(self.data_directory, 'breakdance-flare', '00001.jpg'),
            os.path.join(self.data_directory, 'breakdance-flare', '00002.jpg'),
            os.path.join(self.data_directory, 'breakdance-flare', '00003.jpg'),
            os.path.join(self.data_directory, 'breakdance-flare', '00004.jpg'),
            os.path.join(self.data_directory, 'breakdance-flare', '00005.jpg'),
            os.path.join(self.data_directory, 'breakdance-flare', '00006.jpg')
        ]

        expected_image_paths_1 = [
            os.path.join(self.data_directory, 'dog-agility', '00000.jpg'),
            os.path.join(self.data_directory, 'dog-agility', '00001.jpg'),
            os.path.join(self.data_directory, 'dog-agility', '00002.jpg'),
        ]

        data_set = InterpDataPreprocessor(self.data_directory, [[1]])
        image_paths = data_set._get_data_paths(self.data_directory)
        self.assertListEqual(image_paths[0], expected_image_paths_0)
        self.assertListEqual(image_paths[1], expected_image_paths_1)

    def test_data_paths_creative(self):
        """
        The Creative Flow paths are a little more interesting (and longer), so it's worth having this extra test here.
        """
        data_directory = os.path.join(self.cur_dir, '..', 'flow', 'creative', 'test_data', 'test')
        bigvegas1_directory = os.path.join(data_directory, 'mixamo', 'bigvegas.Breakdance_Freezes-50-7',
                                           'cam0', 'renders', 'composite', 'style.cpencil1.paint5')
        bigvegas2_directory = os.path.join(data_directory, 'mixamo', 'bigvegas.Evading_A_Threat-33-31',
                                           'cam0', 'renders', 'composite', 'style.paint3.marker2')
        shapenet_directory = os.path.join(data_directory, 'shapenet',
                                          '03624134_dde6b5fad601b25e80b72a37a25b7e72',
                                          'cam0', 'renders', 'composite', 'style.textured1.ink6')

        expected_image_paths_0 = [
            os.path.join(bigvegas1_directory, 'frame000171.png'),
            os.path.join(bigvegas1_directory, 'frame000172.png'),
        ]
        expected_image_paths_1 = [
            os.path.join(bigvegas2_directory, 'frame000074.png'),
            os.path.join(bigvegas2_directory, 'frame000075.png'),
        ]

        expected_image_paths_2 = [
            os.path.join(shapenet_directory, 'frame000069.png'),
            os.path.join(shapenet_directory, 'frame000070.png'),
            os.path.join(shapenet_directory, 'frame000071.png'),
            os.path.join(shapenet_directory, 'frame000072.png'),
        ]
        data_set = InterpDataPreprocessor(data_directory, [[1, 0, 0]])
        image_paths = data_set._get_data_paths(data_directory)
        self.assertListEqual(image_paths[0], expected_image_paths_0)
        self.assertListEqual(image_paths[1], expected_image_paths_1)
        self.assertListEqual(image_paths[2], expected_image_paths_2)

    def test_data_paths_whitelist(self):
        whitelist_path = os.path.join(self.data_directory, 'whitelist.txt')
        with open(whitelist_path, 'r') as f:
            # I can't ever remember how to do this:
            # https://stackoverflow.com/questions/3277503/how-to-read-a-file-line-by-line-into-a-list
            whitelist = f.readlines()
        whitelist = [x.strip() for x in whitelist]

        # The whitelist does a bit of path gymnastics.
        extra_str = os.path.join('..', '..', '..', 'interp', 'test_data', 'davis')

        expected_image_paths_0 = [
            os.path.join(self.data_directory, extra_str, 'dog-agility', '00000.jpg'),
            os.path.join(self.data_directory, extra_str, 'dog-agility', '00001.jpg'),
            os.path.join(self.data_directory, extra_str, 'dog-agility', '00002.jpg'),
        ]

        data_set = InterpDataPreprocessor(self.data_directory, [[1]])
        image_paths = data_set._get_data_paths(self.data_directory, whitelist=whitelist)
        self.assertListEqual(image_paths[0], expected_image_paths_0)

    def _is_list_allowable(self, x, allowable_lists):
        truth_statement = False
        for i in range(len(allowable_lists)):
            truth_statement |= (x == allowable_lists[i])
        return truth_statement

    def tearDown(self):
        data_set = InterpDataSet(self.tf_record_directory, [[1]], batch_size=2)
        output_paths = data_set.get_tf_record_names()
        for output_path in output_paths:
            if os.path.isfile(output_path):
                os.remove(output_path)

        json_path = os.path.join(self.tf_record_directory, 'val_split.json')
        if os.path.isfile(json_path):
            os.remove(json_path)

        if os.path.isdir(data_set.get_tf_record_dir()):
            if os.listdir(data_set.get_tf_record_dir()) == []:
                shutil.rmtree(data_set.get_tf_record_dir())


if __name__ == '__main__':
    unittest.main()
