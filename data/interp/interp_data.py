from common.utils.data import *
from data.dataset import DataSet
from data.interp.interp_data_readers import InterpTfRecordDataSetReader, InterpNumpyDataSetReader
from data.interp.interp_data_readers import InterpAugmentations


class InterpDataSet(DataSet):
    def __init__(self, tf_record_directory, inbetween_locations, batch_size=1,
                 augmentations=InterpAugmentations(),
                 extra_validation_dir=None):
        """
        :param inbetween_locations: A list of lists. Each element specifies where inbetweens will be placed,
                                    and each configuration will appear with uniform probability.
                                    For example, let a single element in the list be [0, 1, 0].
                                    With this, dataset elements will be sequences of 3 ordered frames,
                                    where the middle (inbetween) frame is 2 frames away from the first and last frames.
                                    The number of 1s must be the same for each list in this argument.
        :param augmentations: An instance of InterpAugmentations.
        :param extra_validation_dir: Path to directory that hosts extra triplets for validation.
                                     Triplets should have names that follow the "sequence_name_idx" structure.
        """
        super().__init__(tf_record_directory, batch_size)

        # Initialized during load().
        self.handle_placeholder = None  # Handle placeholder for switching between datasets.
        self.next_sequences = None  # Data iterator batch.
        self.next_sequence_timing = None  # Data iterator batch.

        self.tf_record_directory = tf_record_directory
        self.inbetween_locations = inbetween_locations
        self.train_tf_record_name = 'interp_dataset_train.tfrecords'
        self.validation_tf_record_name = 'interp_dataset_validation.tfrecords'
        self.train_data = InterpTfRecordDataSetReader(self.tf_record_directory, inbetween_locations,
                                                      self.train_tf_record_name,
                                                      batch_size=batch_size,
                                                      augmentations=augmentations)

        # Validation set still needs to crop.
        val_augmentations = InterpAugmentations(crop_size=augmentations.crop_size)
        self.validation_data = InterpTfRecordDataSetReader(self.tf_record_directory, inbetween_locations,
                                                           self.validation_tf_record_name,
                                                           batch_size=batch_size,
                                                           augmentations=val_augmentations)

        # Extra (should be small) validation set.
        # Don't crop for this one, but do pad the images for divisibility.
        self.extra_validation_dir = extra_validation_dir
        self.has_extra_validation = self.extra_validation_dir is not None
        if self.extra_validation_dir is not None:
            self.validation_np_data = InterpNumpyDataSetReader(self.extra_validation_dir, crop_size=None)

    def get_tf_record_dir(self):
        return self.tf_record_directory

    def get_tf_record_names(self):
        """
        :return: All the tf record names that were saved with this instance.
        """
        return self.get_train_file_names() + self.get_validation_file_names()

    def get_train_file_names(self):
        """
        Overriden.
        """
        return self.train_data.get_tf_record_names()

    def get_validation_file_names(self):
        """
        Overriden.
        """
        return self.validation_data.get_tf_record_names()

    def load(self, session):
        """
        Overriden.
        """
        with tf.name_scope('interp_data'):
            self.train_data.load(session, repeat=True, shuffle=True)
            self.validation_data.load(session, repeat=False, shuffle=False,
                                      initializable=True, only_first_sequence_tuple=True)
            self.handle_placeholder = tf.placeholder(tf.string, shape=[])
            self.iterator = tf.data.Iterator.from_string_handle(
                self.handle_placeholder, self.train_data.get_output_types(), self.train_data.get_output_shapes())
            self.next_sequences, self.next_sequence_timing = self.iterator.get_next()
            if self.extra_validation_dir is not None:
                self.validation_np_data.load(session)

    def get_next_batch(self):
        return self.next_sequences, self.next_sequence_timing

    def get_train_feed_dict(self):
        """
        Overridden.
        """
        return {self.handle_placeholder: self.train_data.get_feed_dict_value()}

    def get_validation_feed_dict(self):
        """
        Overriden.
        """
        return {self.handle_placeholder: self.validation_data.get_feed_dict_value()}

    def get_validation_np_feed_dict(self):
        return {self.handle_placeholder: self.validation_np_data.get_feed_dict_value()}

    def init_validation_data(self, session):
        """
        Overridden
        """
        self.validation_data.init_data(session)

    def ready_next_np_validation_element(self, session):
        return self.validation_np_data.ready_next(session)
