from common.utils.data import get_triplet_filepaths
from common.utils.tf import sliding_window_slice, tf_coin_flip
from common.utils.img import read_image, centered_crop, pad_image_for_divisibility
import glob
import multiprocessing
import os.path
import numpy as np
import tensorflow as tf


class InterpAugmentations:
    def __init__(self, flip_prob=0.0, duplicate_endpoint_frames_prob=0.0, do_center_crop=True,
                 crop_size=None):
        """
        The default parameters all correspond to no augmentation.
        :param flip_prob: Float. Probability of flipping the sequence in the x, y, or temporal axes.
        :param duplicate_endpoint_frames_prob: Float. Probability of duplicating the endpoint frame for augmentation.
                                               Currently applies only if there's 1 inbetween location.
        :param do_center_crop: Bool. If False, the image crop will always be taken in the center,
                               else it will be randomly offset.
        :param crop_size: Tuple of (int (H), int (W)). Size to crop the training examples to before feeding to network.
                          If None, then no cropping will be performed.
        """
        self.flip_prob = flip_prob
        self.duplicate_endpoint_frames_prob = duplicate_endpoint_frames_prob
        self.do_center_crop = do_center_crop
        self.crop_size = crop_size


class InterpNumpyDataSetReader:
    """
    Note that this class is a very limited version of InterpTfRecordDataSetReader,
    as it is meant only for validation on image triplets (thus, no augmentations).
    The batch_size is always 1, since images might not be the same size.
    :param crop_size: A (height, width) tuple indicating the size to which images will be center cropped.
    """
    def __init__(self, directory, crop_size=(256, 256), num_expected_downsamples=6):
        """
        :param directory: Directory that has the image triplets.
        """
        self.directory = directory
        self.num_expected_downsamples = num_expected_downsamples
        self.iterator = None
        self.handle = None
        self.dataset = None
        self.tuples_placeholder = None
        self.timing_placeholder = None
        self.crop_size = crop_size
        self.timing_array = None
        self.triplets = None
        self.iter = 0
        self.num_elements = 0

    def load(self, session):
        self.tuples_placeholder = tf.placeholder(tf.float32, [None, None, None, None, 3])
        self.timing_placeholder = tf.placeholder(tf.float32, [None, None])
        self.dataset = tf.data.Dataset.from_tensor_slices((self.tuples_placeholder, self.timing_placeholder))
        self.dataset = self.dataset.batch(1)
        self.iterator = self.dataset.make_initializable_iterator()
        self.handle = session.run(self.iterator.string_handle())

        # Load Numpy arrays.
        triplet_names = get_triplet_filepaths(self.directory)
        triplets = []
        for names in triplet_names:
            image_a = read_image(names[0], as_float=True)
            image_b = read_image(names[1], as_float=True)
            image_c = read_image(names[2], as_float=True)
            if self.crop_size is not None:
                image_a = centered_crop(image_a, self.crop_size[0], self.crop_size[1])
                image_b = centered_crop(image_b, self.crop_size[0], self.crop_size[1])
                image_c = centered_crop(image_c, self.crop_size[0], self.crop_size[1])
            image_a = pad_image_for_divisibility(image_a, self.num_expected_downsamples)
            image_b = pad_image_for_divisibility(image_b, self.num_expected_downsamples)
            image_c = pad_image_for_divisibility(image_c, self.num_expected_downsamples)
            triplets.append((image_a, image_b, image_c))

        self.num_elements = len(triplets)
        self.triplets = triplets
        self.timing_array = np.zeros((1, 3))
        self.timing_array[:, 0] = 0.0
        self.timing_array[:, 1] = 0.5
        self.timing_array[:, 2] = 1.0

    def ready_next(self, session):
        """
        :return: True if we've hit the end of the epoch (next element is the last element in the dataset).
                 If it hits the end, it will also reset the count.
        """
        hit_end = self.iter == self.num_elements - 1
        session.run(self.iterator.initializer, feed_dict={self.tuples_placeholder: [self.triplets[self.iter]],
                                                          self.timing_placeholder: self.timing_array})
        self.iter += 1
        self.iter %= self.num_elements
        return hit_end

    def get_feed_dict_value(self):
        return self.handle


class InterpTfRecordDataSetReader:
    SHOT_LEN = 'shot_len'
    WIDTH = 'width'
    HEIGHT = 'height'
    SHOT = 'shot'

    TRAIN_TF_RECORD_NAME = 'interp_dataset_train.tfrecords'
    VALIDATION_TF_RECORD_NAME = 'interp_dataset_validation.tfrecords'

    def __init__(self, directory, inbetween_locations, tf_record_name, batch_size=1,
                 augmentations=InterpAugmentations()):
        """
        :param inbetween_locations: A list of lists. Each element specifies where inbetweens will be placed,
                                    and each configuration will appear with uniform probability.

                                    For example, Let frame0 be the start of a sequence. Then:
                                        [1] equates to [frame0, frame1, frame2]
                                        [0, 1, 0] equates to [frame0, frame2, frame4]
                                        [1, 0, 0] equates to [frame0, frame1, frame4]
        :param augmentations: An instance of InterpAugmentations.
        """
        # Initialized during load().
        self.dataset = None  # Tensorflow DataSet object.
        self.handle = None  # Handle to feed for the dataset.
        self.iterator = None  # Iterator for getting the next batch.

        self.batch_size = batch_size
        self.crop_size = augmentations.crop_size
        self.directory = directory
        self.tf_record_name = tf_record_name
        self.inbetween_locations = inbetween_locations

        # Setting this higher may cause drastic slowdown.
        self.num_parallel_calls = 1

        # Augmentation params.
        assert isinstance(augmentations, InterpAugmentations)
        self.augmentations = augmentations

        # Check for number of ones, as the number of elements per-sequence must be the same.
        num_ones = (np.asarray(self.inbetween_locations[0]) == 1).sum()
        for i in range(1, len(self.inbetween_locations)):
            if (np.asarray(self.inbetween_locations[i]) == 1).sum() != num_ones:
                raise ValueError('The number of ones for each element in inbetween_locations must be the same.')

    def get_tf_record_names(self):
        return sorted(glob.glob(self._get_tf_record_pattern()))

    def init_data(self, session):
        session.run(self.iterator.initializer)

    def load(self, session, repeat=False, shuffle=False, initializable=False,
             max_num_elements=None, only_first_sequence_tuple=False):
        """
        :param session: tf Session.
        :param repeat: Whether to call repeat on the tf DataSet.
        :param shuffle: Whether to shuffle on the tf DataSet.
        :param initializable: Whether to use an initializable or a one_shot iterator.
                              If True, init_data must be called to use the DataSet.
        :param max_num_elements: The maximum number of elements allowed in the final dataset. If None, it is ignored.
        :param only_first_sequence_tuple: Whether to use only the first tuple in a sequence.
                                          This saves time for validation without losing much diversity.
        """
        with tf.name_scope(self.tf_record_name + '_dataset_ops'):
            datasets = []
            for i in range(len(self.inbetween_locations)):
                inbetween_locations = self.inbetween_locations[i]
                dataset = self._load_for_inbetween_locations(inbetween_locations, shuffle,
                                                             only_first_sequence_tuple=only_first_sequence_tuple)
                datasets.append(dataset)

            if shuffle:
                self.dataset = tf.data.experimental.sample_from_datasets(datasets)
            else:
                self.dataset = datasets[0]
                for i in range(1, len(datasets)):
                    self.dataset = self.dataset.concatenate(datasets[i])

            if max_num_elements is not None:
                assert max_num_elements >= 0
                self.dataset = self.dataset.take(max_num_elements)

            buffer_size = 100
            if shuffle:
                self.dataset = self.dataset.shuffle(buffer_size=buffer_size)

            self.dataset = self.dataset.batch(self.batch_size)
            self.dataset = self.dataset.prefetch(buffer_size=2)

            if repeat:
                self.dataset = self.dataset.repeat()

            if initializable:
                self.iterator = self.dataset.make_initializable_iterator()
            else:
                self.iterator = self.dataset.make_one_shot_iterator()

        self.handle = session.run(self.iterator.string_handle())

    def get_output_shapes(self):
        return self.dataset.output_shapes

    def get_output_types(self):
        return self.dataset.output_types

    def get_feed_dict_value(self):
        return self.handle

    def _get_tf_record_pattern(self):
        return os.path.join(self.directory, '*' + self.tf_record_name)

    def _load_for_inbetween_locations(self, inbetween_locations, shuffle, only_first_sequence_tuple=False):
        """
        :param inbetween_locations: An element of self.inbetween_locations.
        :param shuffle: Whether to shuffle the shards.
        :return: Tensorflow dataset object.
        """
        # Compute timing information.
        slice_indices = [1] + inbetween_locations + [1]
        slice_times = []
        for i in range(len(slice_indices)):
            if slice_indices[i] == 1:
                slice_times.append(i * 1.0 / (len(slice_indices) - 1))

        def _parse_function(example_proto):
            features = {
                InterpTfRecordDataSetReader.SHOT_LEN: tf.FixedLenFeature((), tf.int64, default_value=0),
                InterpTfRecordDataSetReader.SHOT: tf.VarLenFeature(tf.string),
                InterpTfRecordDataSetReader.HEIGHT: tf.FixedLenFeature((), tf.int64, default_value=0),
                InterpTfRecordDataSetReader.WIDTH: tf.FixedLenFeature((), tf.int64, default_value=0)
            }
            parsed_features = tf.parse_single_example(example_proto, features)
            shot_len = tf.reshape(tf.cast(parsed_features[InterpTfRecordDataSetReader.SHOT_LEN], tf.int32), ())
            H = tf.reshape(tf.cast(parsed_features[InterpTfRecordDataSetReader.HEIGHT], tf.int32), ())
            W = tf.reshape(tf.cast(parsed_features[InterpTfRecordDataSetReader.WIDTH], tf.int32), ())

            shot_bytes = tf.sparse_tensor_to_dense(parsed_features[InterpTfRecordDataSetReader.SHOT],
                                                   default_value=tf.as_string(0))
            shot = tf.map_fn(lambda bytes: tf.image.decode_image(bytes), shot_bytes, dtype=tf.uint8)
            shot = tf.image.convert_image_dtype(shot, tf.float32)
            shot = tf.reshape(shot, (shot_len, H, W, 3))
            slice_locations = [1] + inbetween_locations + [1]
            if only_first_sequence_tuple:
                return tf.expand_dims(sliding_window_slice(shot, slice_locations)[0], 0)
            else:
                return sliding_window_slice(shot, slice_locations)

        # Shuffle filenames.
        # Ideas taken from: https://github.com/tensorflow/tensorflow/issues/14857
        dataset = tf.data.Dataset.list_files(self._get_tf_record_pattern(), shuffle=shuffle)
        dataset = tf.data.TFRecordDataset(dataset)

        # Parse sequences.
        dataset = dataset.map(_parse_function, num_parallel_calls=multiprocessing.cpu_count())

        # Each element in the dataset is currently a group of sequences (grouped by video shot),
        # so we need to 'unbatch' them first.
        dataset = dataset.apply(tf.data.experimental.unbatch())

        def _add_timing_and_maybe_duplicate(sequence):
            timing_tensor = tf.constant(slice_times)

            # Randomly augment triplets with timestamps like [0.0, 1.0, 1.0], or [0.0, 0.0, 1.0].
            if len(inbetween_locations) == 1 and self.augmentations.duplicate_endpoint_frames_prob > 0:
                img_0, img_1, img_2 = sequence[0], sequence[1], sequence[2]
                start_duplicate = tf.stack([img_0, img_0, img_2], axis=0)
                end_duplicate = tf.stack([img_0, img_2, img_2], axis=0)
                is_start_duplicate = tf_coin_flip(0.5)[0]
                duplicated = tf.cond(is_start_duplicate, lambda: start_duplicate, lambda: end_duplicate)
                duplicated_timing = tf.cond(is_start_duplicate,
                                            lambda: tf.constant([0.0, 0.0, 1.0]),
                                            lambda: tf.constant([0.0, 1.0, 1.0]))
                do_duplicate = tf_coin_flip(self.augmentations.duplicate_endpoint_frames_prob)[0]
                timing_tensor = tf.cond(do_duplicate, lambda: duplicated_timing, lambda: timing_tensor)
                sequence = tf.cond(do_duplicate, lambda: duplicated, lambda: sequence)
            return sequence, timing_tensor

        def _crop(sequence):
            s = tf.shape(sequence)
            sequence_len, H, W = s[0], s[1], s[2]
            crop_h, crop_w = self.crop_size
            if not self.augmentations.do_center_crop:
                sequence = tf.random_crop(sequence, [sequence_len, crop_h, crop_w, 3])
            else:
                crop_top = tf.cast(H / 2 - crop_h / 2, tf.int32)
                crop_left = tf.cast(W / 2 - crop_w / 2, tf.int32)
                sequence = tf.image.crop_to_bounding_box(sequence, crop_top, crop_left, crop_h, crop_w)
            return sequence

        def _flip(sequence, timing_information):
            # Randomly flip in temporal, y, and x axes.
            p = self.augmentations.flip_prob
            do_flip_t = tf_coin_flip(p)[0]
            sequence = tf.cond(do_flip_t, lambda: tf.reverse(sequence, [0]), lambda: sequence)
            sequence = tf.cond(tf_coin_flip(p)[0], lambda: tf.reverse(sequence, [1]), lambda: sequence)
            sequence = tf.cond(tf_coin_flip(p)[0], lambda: tf.reverse(sequence, [2]), lambda: sequence)
            timing_information = tf.cond(do_flip_t,
                                         lambda: 1.0 - tf.reverse(timing_information, [0]),
                                         lambda: timing_information)
            return sequence, timing_information

        if self.crop_size is not None:
            dataset = dataset.map(_crop, num_parallel_calls=self.num_parallel_calls)
        dataset = dataset.map(_add_timing_and_maybe_duplicate, num_parallel_calls=self.num_parallel_calls)
        dataset = dataset.map(_flip, num_parallel_calls=self.num_parallel_calls)
        return dataset
