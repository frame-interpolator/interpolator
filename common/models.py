import numpy as np
import os.path
import tensorflow as tf
from common.utils.tf import leaky_relu, group_norm


_default = object()


# Allows saving and restoring all network parameters its scope into an npz file.
# Provides a way to serialize the network in a minimal form without any tensorflow metadata.
# If the network is fully convolutional, this also allows restoring weights into a network with larger inputs/outputs.
class RestorableNetwork():
    def __init__(self, name):
        """
        :param name: Str. For variable scoping.
        """
        self.name = name

        # Dictionary with key = variable name and value = (assign_op, placeholder).
        self._assign_ops = {}

    def save_to(self, file_path, sess):
        """
        Serializes the network into an npz file.
        :param file_path: Str.
        :param sess: Tensorflow session.
        :return: Nothing.
        """
        save_dict = self.get_save_np(sess)
        np.savez(file_path, **save_dict)

    def get_save_np(self, sess):
        """
        Creates a dict of np arrays corresponding to the weights of each variable.
        :param sess: Tensorflow session.
        :return: Dict of np arrays. Key is the name of the variable.
        """
        trainable_vars = tf.trainable_variables(self.name)
        trainable_vars_np = sess.run(trainable_vars)
        # Create a dict of variable_name:variable_np.
        save_dict = {}
        for i, var in enumerate(trainable_vars):
            var_name = var.name
            save_dict[var_name] = trainable_vars_np[i]
        return save_dict

    def restore_from(self, file_path, sess, scope_prefix=''):
        """
        Deserializes a network from an npz file.
        Instructions on how to handle changes to network name and scope:
            If the network's name has changed:
                Call restore_from_np(rename_np_dict(np.load(file_path), <old_name>, <new_name>), sess)
            If the network's scope has gotten deeper:
                Call restore_from(file_path, sess, scope_prefix=<new_scope>)
            If the network's name and scope has changed:
                Call restore_from_np(rename_np_dict(np.load(file_path), <old_name>, <new_name>), sess,
                                     scope_prefix=<new_scope>)
         Note that we only "officially" support the scope getting deeper, because it doesn't make sense for a trained
         and saved network to become shallower -- networks shouldn't be trained in a scope deeper than their name.
        :param file_path: Str.
        :param sess: Tensorflow session.
        :param scope_prefix: Adds a prefix to the scope filter when getting the vars to restore. If it is '', then no
            prefix will be used. The npz file does not need to be modified if name of the network is still the same.
        :return: Nothing.
        """
        if os.path.isfile(file_path):
            var_dict = np.load(file_path)
            self.restore_from_np(var_dict, sess, scope_prefix)
        else:
            print(file_path, 'does not exist.')

    def restore_from_np(self, var_dict, sess, scope_prefix=''):
        """
        Restores the network from the var_dict.
        :param var_dict: Dict of np arrays. Key is the name of the variable.
        :param sess: Tensorflow session.
        :param scope_prefix: Adds a prefix to the scope filter when getting the vars to restore. If it is '', then no
            prefix will be used. The var_dict does not need to be modified if name of the network is still the same.
        :return: Nothing
        """
        def _append_prefix(name):
            return scope_prefix + '/' + name
        scope_filter = self.name
        if scope_prefix != '':
            scope_filter = _append_prefix(self.name)
            var_dict = {_append_prefix(key): value for (key, value) in var_dict.items()}
        with tf.name_scope(self.name + '_assign_ops'):
            trainable_vars = tf.trainable_variables(scope_filter)
            assert len(trainable_vars) == len(var_dict.keys())
            feed_dict = {}
            assign_ops = []
            for var in trainable_vars:
                var_name = var.name
                assert var_name in var_dict
                var_np = var_dict[var_name]
                assign_op, placeholder = self.get_assign_op(var)
                assign_ops.append(assign_op)
                assert placeholder not in feed_dict
                feed_dict[placeholder] = var_np
            sess.run(assign_ops, feed_dict=feed_dict)

    def get_assign_op(self, var):
        """
        :param var: Tensorflow variable.
        :return: Operation, placeholder.
        """
        var_name = var.name
        if var_name not in self._assign_ops:
            ph = tf.placeholder(dtype=tf.float32)
            op = tf.assign(var, ph, validate_shape=True)
            self._assign_ops[var_name] = op, ph
        return self._assign_ops[var_name]

    @staticmethod
    def rename_np_dict(var_dict, old_network_name, new_network_name):
        """
        Renames the np dict so a new network with a different name but same architecture can restore from it.
        :param old_network_name: Str. Name of the old network.
        :param new_network_name: Str. Name of the new network.
        :return: New var dict.
        """
        new_dict = {}
        for key, value in var_dict.items():
            new_key = key.replace(old_network_name, new_network_name)
            new_dict[new_key] = value
        return new_dict


class ConvNetwork(RestorableNetwork):
    class Norm:
        BATCH_NORM = 'batch_norm'
        GROUP_NORM = 'group_norm'
        LAYER_NORM = 'layer_norm'

        def __init__(self, norm_type=None, groups=32):
            """
            :param type: BATCH_NORM or GROUP_NORM. None for no norm.
            :param groups: Int. Used for group norm.
            """
            self.norm_type = norm_type
            self.groups = groups

    def __init__(self, name, layer_specs=None,
                 activation_fn=leaky_relu,
                 last_activation_fn=_default,
                 regularizer=None, bias_regularizer=None, padding='SAME', dense_net=False,
                 kernel_initializer=None, bias_initializer=tf.initializers.zeros):
        """
        Generic conv-net
        :param name: Str. For variable scoping.
        :param layer_specs: Array of shape [num_layers, 4] or [num_layers, 5].
                            The second dimension consists of [kernel_size, num_output_features, dilation, stride]
                            + [norm_type], where norm_type is an optional Norm descriptor.
                            If stride is < 1 (i.e. 0.5), the layer will perform a deconvolution upsampling.
        :param activation_fn: Tensorflow activation function.
        :param last_activation_fn: Tensorflow activation function. Applied after the final convolution
                                   in place of activation_fn. Defaults to the value of activation_fn.
        :param regularizer: Tf regularizer such as tf.contrib.layers.l2_regularizer.
        :param bias_regularizer: Regularizer for the bias term.
        :param padding: Str. Either 'SAME' or 'VALID' case insensitive.
        :param dense_net: Bool. If true, then it is expected that all layers have the same width and height.
        :param kernel_initializer: Tensorflow initializer. Defaults to glorot (xavier) initialization if None.
        :param bias_initializer: Tensorflow initializer. Defaults to zeros.
        """
        super().__init__(name)
        self.layer_specs = layer_specs
        self.activation_fn = activation_fn
        self.regularizer = regularizer
        self.bias_regularizer = bias_regularizer
        self.padding = padding
        self.dense_net = dense_net
        self.kernel_initializer = kernel_initializer
        self.bias_initializer = bias_initializer

        if last_activation_fn == _default:
            self.last_activation_fn = self.activation_fn
        else:
            self.last_activation_fn = last_activation_fn

    def _get_conv_tower(self, features, training=False, reuse_variables=tf.AUTO_REUSE):
        """
        :param features: Tensor. Feature map of shape [batch_size, H, W, num_features].
        :param training: Python bool or Tensorflow bool. Switches between training and test mode (i.e. for batch norm).
        :param reuse_variables: Tensorflow reuse option.
        :return: final_output: Tensor of shape [batch_size, H, W, num_output_features].
                 layer_outputs: List of all convolution outputs of the network. The last item is the final_output.
                 dense_outputs: List of tensors. This can be used to chain this dense-net to another. The last item
                                includes the network's output. If this is not a dense-net, then the list will be empty.
        """
        layer_outputs = []

        # Create the network layers.
        previous_output = features
        # Stores the dense output of each layer.
        dense_outputs = []
        for i, layer_spec in enumerate(self.layer_specs):
            # Get specs.
            kernel_size = layer_spec[0]
            num_output_features = layer_spec[1]
            dilation = layer_spec[2]
            stride = layer_spec[3]
            norm = ConvNetwork.Norm(norm_type=None) if len(layer_spec) <= 4 else layer_spec[4]

            is_last_layer = i == len(self.layer_specs) - 1
            activation_fn = self.last_activation_fn if is_last_layer else self.activation_fn
            if len(dense_outputs) > 0:
                inputs = dense_outputs[-1]
            else:
                inputs = previous_output

            # Create the convolution layer.
            conv_name = 'conv_' + str(i)
            if stride < 1.0:
                previous_output = tf.layers.conv2d_transpose(inputs=inputs,
                                                             filters=num_output_features,
                                                             kernel_size=[kernel_size, kernel_size],
                                                             strides=(int(1.0 / stride), int(1.0 / stride)),
                                                             padding='SAME',
                                                             activation=None,
                                                             kernel_regularizer=self.regularizer,
                                                             bias_regularizer=self.bias_regularizer,
                                                             kernel_initializer=self.kernel_initializer,
                                                             bias_initializer=self.bias_initializer,
                                                             name=conv_name)
            else:
                previous_output = tf.layers.conv2d(inputs=inputs,
                                                   filters=num_output_features,
                                                   kernel_size=[kernel_size, kernel_size],
                                                   strides=(stride, stride),
                                                   padding='SAME',
                                                   dilation_rate=(dilation, dilation),
                                                   activation=None,
                                                   kernel_regularizer=self.regularizer,
                                                   bias_regularizer=self.bias_regularizer,
                                                   kernel_initializer=self.kernel_initializer,
                                                   bias_initializer=self.bias_initializer,
                                                   name=conv_name)
            if norm.norm_type == ConvNetwork.Norm.BATCH_NORM:
                previous_output = tf.layers.batch_normalization(previous_output, training=training, axis=-1, fused=True,
                                                                name=conv_name + '_bn')
            elif norm.norm_type == ConvNetwork.Norm.GROUP_NORM:
                previous_output = group_norm(previous_output, num_output_features, groups=norm.groups,
                                             name=conv_name + '_gn', reuse=reuse_variables)
            elif norm.norm_type == ConvNetwork.Norm.LAYER_NORM:
                previous_output = tf.contrib.layers.layer_norm(previous_output, scope=conv_name + '_ln',
                                                               reuse=reuse_variables)
            if activation_fn is not None:
                with tf.variable_scope(conv_name + '_activation'):
                    previous_output = activation_fn(previous_output)

            if self.dense_net:
                with tf.name_scope(conv_name + '_dense_concat'):
                    # Dense layer output consists of all previous layer outputs and the input.
                    dense_outputs.append(tf.concat([inputs, previous_output], axis=-1))

            layer_outputs.append(previous_output)

        assert previous_output == layer_outputs[-1]
        assert features not in layer_outputs
        final_output = previous_output
        return final_output, layer_outputs, dense_outputs

    def get_forward_conv(self, features, reuse_variables=tf.AUTO_REUSE, training=False):
        """
        Public API for getting the forward ops.
        :param features: Feature map or images.
        :param reuse_variables: Whether to reuse the variables under the scope.
        :param training: Python bool or Tensorflow bool. Switches between training and test mode (i.e. for batch norm).
        :return: final_output, layer_outputs, dense_outputs.
        """
        with tf.variable_scope(self.name, reuse=reuse_variables):
            return self._get_conv_tower(features, training=training, reuse_variables=reuse_variables)


class FrozenModel:
    def __init__(self, directory):
        """
        :param directory: Str. Directory of the saved/frozen model.
        """
        assert directory is not None
        self.directory = directory
        self.predictor = None

    def freeze(self, model, session):
        """
        Saves the model by freezing it.
        :param model: PWCNet.
        :param session: tf.Session.
        :return: Nothing.
        """
        raise NotImplementedError

    def load(self, session_config_proto=None):
        assert os.path.exists(self.directory) and len(os.listdir(self.directory)) > 0, 'Model not saved/frozen.'
        print('Loading frozen model from:', self.directory)
        self.predictor = tf.contrib.predictor.from_saved_model(self.directory, config=session_config_proto)

    def run(self, inputs):
        raise NotImplementedError
