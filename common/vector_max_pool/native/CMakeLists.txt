cmake_minimum_required(VERSION 3.5)

add_op_library(NAME vector_max_pool_op SOURCES
    "vector_max_pool_op.cc"
    "vector_max_pool_op.cc.cu"
)