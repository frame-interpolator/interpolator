#if GOOGLE_CUDA

#define EIGEN_USE_GPU

#define _USE_MATH_DEFINES
#include <cmath>

#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/util/cuda_kernel_helper.h"

using namespace tensorflow;

typedef Eigen::GpuDevice GPUDevice;

__global__ void VectorMaxPoolKernel(const int32 nthreads,
	const float* src_images, int batch, int height, int width, int channels,
	float* output, int kernel_size) {
    const int out_width = width / kernel_size;
    const int out_height = height / kernel_size;
    CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
        // Breakdown: out_idx = out_x + out_width * (out_y + out_height * b).
        int idx = out_idx;
        const int out_x = idx % out_width;
        idx /= out_width;
        const int out_y = idx % out_height;
        const int b = idx / out_height;

#define SRC_IMG_OFFSET(iy, ix, ic) ((ic) + channels * ((ix) + width * ((iy) + height * b)))
#define OUT_IMG_OFFSET(iy, ix, ic) ((ic) + channels * ((ix) + out_width * ((iy) + out_height * b)))
        auto compute_mag_squared = [=](int y, int x) {
            float mag_squared = 0.0f;
            for (int c = 0; c < channels; ++c) {
                const float val = src_images[SRC_IMG_OFFSET(y, x, c)];
                mag_squared += val * val;
            }
            return mag_squared;
        };

        // max_x and max_y store the (x, y) coord of the maximum value.
        float max_mag = -1.0f;
        int max_x = 0, max_y = 0;
#pragma unroll
        for (int i = 0; i < kernel_size; ++i) {
            const int src_y = out_y * kernel_size + i;
#pragma unroll
            for (int j = 0; j < kernel_size; ++j) {
                const int src_x = out_x * kernel_size + j;
                const float mag_squared = compute_mag_squared(src_y, src_x);
                if (mag_squared > max_mag) {
                    max_mag = mag_squared;
                    max_y = src_y;
                    max_x = src_x;
                }
            }
        }

        // Copy the max_x and max_y to the output.
        for (int c = 0; c < channels; ++c) {
            output[OUT_IMG_OFFSET(out_y, out_x, c)] = src_images[SRC_IMG_OFFSET(max_y, max_x, c)];
        }
#undef OUT_IMG_OFFSET
#undef SRC_IMG_OFFSET
    }
}

__global__ void VectorMaxPoolGradKernel(const int32 nthreads,
    const float* input_grad, const float* src_images,
    int batch, int height, int width, int channels,
    float* output, int kernel_size) {
    const int out_width = width / kernel_size;
    const int out_height = height / kernel_size;
    CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
        // Breakdown: out_idx = out_x + out_width * (out_y + out_height * b).
        int idx = out_idx;
        const int out_x = idx % out_width;
        idx /= out_width;
        const int out_y = idx % out_height;
        const int b = idx / out_height;

#define SRC_IMG_OFFSET(iy, ix, ic) ((ic) + channels * ((ix) + width * ((iy) + height * b)))
#define OUT_IMG_OFFSET(iy, ix, ic) ((ic) + channels * ((ix) + out_width * ((iy) + out_height * b)))
        auto compute_mag_squared = [=](int y, int x) {
            float mag_squared = 0.0f;
            for (int c = 0; c < channels; ++c) {
                const float val = src_images[SRC_IMG_OFFSET(y, x, c)];
                mag_squared += val * val;
            }
            return mag_squared;
        };

        // max_x and max_y store the (x, y) coord of the maximum value.
        float max_mag = -1.0f;
        int max_x = 0, max_y = 0;
#pragma unroll
        for (int i = 0; i < kernel_size; ++i) {
            const int src_y = out_y * kernel_size + i;
#pragma unroll
            for (int j = 0; j < kernel_size; ++j) {
                const int src_x = out_x * kernel_size + j;
                const float mag_squared = compute_mag_squared(src_y, src_x);
                if (mag_squared > max_mag) {
                    max_mag = mag_squared;
                    max_y = src_y;
                    max_x = src_x;
                }
            }
        }

        // Gradient pass-through.
        for (int c = 0; c < channels; ++c) {
            output[SRC_IMG_OFFSET(max_y, max_x, c)] = input_grad[OUT_IMG_OFFSET(out_y, out_x, c)];
        }
#undef OUT_IMG_OFFSET
#undef SRC_IMG_OFFSET
    }
}

void VectorMaxPool(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor images,
    typename TTypes<float, 4>::Tensor output,
    int kernel_size) {
    const int batch = images.dimension(0);
    const int height = images.dimension(1);
    const int width = images.dimension(2);
    const int channels = images.dimension(3);

    const int total_count = (batch * height * width) / (kernel_size * kernel_size);
    if (total_count == 0) return;

    CudaLaunchConfig config = GetCudaLaunchConfig(total_count, d);
    VectorMaxPoolKernel
        << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
            config.virtual_thread_count, images.data(),
            batch, height, width, channels,
            output.data(), kernel_size);
}

void VectorMaxPoolGrad(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor input_grad,
    typename TTypes<float, 4>::ConstTensor original_images,
    typename TTypes<float, 4>::Tensor output,
    int kernel_size) {
    const int batch = original_images.dimension(0);
    const int height = original_images.dimension(1);
    const int width = original_images.dimension(2);
    const int channels = original_images.dimension(3);

    int total_count = batch * height * width * channels;
    if (total_count == 0) return;

    // Initialize output with all zeros.
    CudaLaunchConfig config = GetCudaLaunchConfig(total_count, d);
    SetZero << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
        config.virtual_thread_count, output.data());

    total_count = (batch * height * width) / (kernel_size * kernel_size);
    if (total_count == 0) return;
    config = GetCudaLaunchConfig(total_count, d);

    // Accumulate gradients.
    VectorMaxPoolGradKernel
        << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
            config.virtual_thread_count, input_grad.data(),
            original_images.data(), batch, height, width, channels,
            output.data(), kernel_size);
}

#endif  // GOOGLE_CUDA
