#define EIGEN_USE_THREADS

#include <memory>
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/framework/types.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/common_shape_fns.h"

typedef Eigen::ThreadPoolDevice CPUDevice;
typedef Eigen::GpuDevice GPUDevice;

using namespace tensorflow;

void VectorMaxPool(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor images,
    typename TTypes<float, 4>::Tensor output,
    int kernel_size);

void VectorMaxPoolGrad(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor input_grad,
    typename TTypes<float, 4>::ConstTensor original_images,
    typename TTypes<float, 4>::Tensor output,
    int kernel_size);

class VectorMaxPoolOp : public OpKernel {
public:
    explicit VectorMaxPoolOp(OpKernelConstruction* context) : OpKernel(context) {
        OP_REQUIRES_OK(context, context->GetAttr("kernel_size", &kernel_size_));
        OP_REQUIRES(context, kernel_size_ > 0,
			errors::InvalidArgument("Need kernel_size > 0, got ", kernel_size_));
    }

    void Compute(OpKernelContext* context) override {
        const Tensor& image = context->input(0);
        OP_REQUIRES(context, image.dims() == 4,
                    errors::InvalidArgument("image expecting a 4-D vector."));
        OP_REQUIRES(context, image.dim_size(1) % kernel_size_ == 0 && image.dim_size(2) % kernel_size_ == 0,
                    errors::InvalidArgument("image dimensions must be divisible by the kernel size."));

        TensorShape output_shape = image.shape();
        output_shape.set_dim(1, int(image.dim_size(1) / kernel_size_));
        output_shape.set_dim(2, int(image.dim_size(2) / kernel_size_));

        Tensor* output = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(0, output_shape, &output));

        typename TTypes<float, 4>::ConstTensor image_data = image.tensor<float, 4>();
        typename TTypes<float, 4>::Tensor output_data = output->tensor<float, 4>();

        VectorMaxPool(context->eigen_device<GPUDevice>(),
            image_data, output_data, kernel_size_);
    }

private:
    int kernel_size_;
};

class VectorMaxPoolOpGrad : public OpKernel {
public:
    explicit VectorMaxPoolOpGrad(OpKernelConstruction* context) : OpKernel(context) {
        OP_REQUIRES_OK(context, context->GetAttr("kernel_size", &kernel_size_));
        OP_REQUIRES(context, kernel_size_ > 0,
			errors::InvalidArgument("Need kernel_size > 0, got ", kernel_size_));
    }

    void Compute(OpKernelContext* context) override {
        const Tensor& input_grads = context->input(0);
        const Tensor& original_images = context->input(1);

        OP_REQUIRES(context, original_images.dim_size(1) == input_grads.dim_size(1) * kernel_size_ &&
                    original_images.dim_size(2) == input_grads.dim_size(2) * kernel_size_,
                    errors::InvalidArgument("input grad size is expected to be pooled."));

        Tensor* output_grads = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(0, original_images.shape(), &output_grads));

        typename TTypes<float, 4>::ConstTensor input_grads_data = input_grads.tensor<float, 4>();
        typename TTypes<float, 4>::ConstTensor original_images_data = original_images.tensor<float, 4>();
        typename TTypes<float, 4>::Tensor output_grads_data = output_grads->tensor<float, 4>();

        VectorMaxPoolGrad(context->eigen_device<GPUDevice>(),
            input_grads_data, original_images_data, output_grads_data, kernel_size_);
    }

private:
    int kernel_size_;
};

using shape_inference::DimensionHandle;
using shape_inference::ShapeHandle;
using shape_inference::DimensionOrConstant;

REGISTER_OP("VectorMaxPool")
.Attr("kernel_size: int = 2")
.Input("images: float")
.Output("output: float")
.SetShapeFn([](shape_inference::InferenceContext* c) {
    int kernel_size;
    c->GetAttr("kernel_size", &kernel_size);
    ShapeHandle in = c->input(0);
    DimensionHandle batch = c->Dim(in, 0);
    DimensionHandle height = c->Dim(in, 1);
    DimensionHandle width = c->Dim(in, 2);
    DimensionHandle channels = c->Dim(in, 3);

    DimensionHandle output_height;
    c->Divide(height, DimensionOrConstant(kernel_size),
        true,  // evenly_divisible.
        &output_height);
    DimensionHandle output_width;
        c->Divide(width, DimensionOrConstant(kernel_size),
        true,  // evenly_divisible.
        &output_width);

    c->set_output(0, c->MakeShape({ batch, output_height, output_width, channels }));
    return Status::OK();
});

REGISTER_OP("VectorMaxPoolGrad")
.Attr("kernel_size: int = 2")
.Input("grads: float")
.Input("original_images: float")
.Output("output_image_grad: float")
.SetShapeFn([](shape_inference::InferenceContext* c) {
	c->set_output(0, c->input(1));
	return Status::OK();
});

#if GOOGLE_CUDA

REGISTER_KERNEL_BUILDER(Name("VectorMaxPool").Device(DEVICE_GPU), VectorMaxPoolOp);
REGISTER_KERNEL_BUILDER(Name("VectorMaxPoolGrad").Device(DEVICE_GPU), VectorMaxPoolOpGrad);

#endif // GOOGLE_CUDA
