import numpy as np
import tensorflow as tf
from common.vector_max_pool.vector_max_pool import vector_max_pool, tf_vector_max_pool
from common.utils.profile import run_profiler

if __name__ == '__main__':
    height = 512
    width = 1024
    im_channels = 16
    batch_size = 4

    input_np = np.random.rand(batch_size, height, width, im_channels)
    input_tensor = tf.placeholder(shape=[batch_size, height, width, im_channels], dtype=tf.float32)

    # Create the graph.
    vector_max_pooled = vector_max_pool(input_tensor)
    tf_vector_max_pooled = tf_vector_max_pool(input_tensor)
    tf_max_pooled_2d = tf.layers.max_pooling2d(input_tensor, 2, 2)

    feed_dict = {input_tensor: input_np}
    run_profiler([vector_max_pooled], feed_dict, name='vector-max-pool')
    run_profiler([tf_vector_max_pooled], feed_dict, name='tf-vector-max-pool')
    run_profiler([tf_max_pooled_2d], feed_dict, name='tf-max-pooled_2d')
