import unittest
import numpy as np
import tensorflow as tf
from common.vector_max_pool.vector_max_pool import vector_max_pool, tf_vector_max_pool
from tensorflow.python.ops import gradient_checker


class TestVectorMaxPool(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        self.max_allowable_grad_err = 1e-4

    def test_max(self):
        input_np = np.asarray([
            [
                [[0, 0], [1, -4], [0, 0], [0, 0]],
                [[5, 1], [0, 0], [0, 0], [1, 2]],
                [[1, 5], [8, 8], [0, 0], [0, 0]],
                [[8, 9], [0, 0], [0, 0], [0, 0]]
            ],
            [
                [[0, 0], [0, 0], [0, 9], [0, 0]],
                [[0, 0], [0, 0], [0, 0], [1, 2]],
                [[0, 0], [8, 8], [0, 0], [0, 0]],
                [[8, 8], [0, 0], [-8, 0], [0, 0]]
            ]
        ])
        output = np.asarray([
            [
                [[5, 1], [1, 2]],
                [[8, 9], [0, 0]]
            ],
            [
                [[0, 0], [0, 9]],
                [[8, 8], [-8, 0]]
            ]
        ])
        input_tensor = tf.constant(input_np, dtype=tf.float32)
        self.assertTrue(np.allclose(output, self.sess.run(vector_max_pool(input_tensor))))
        self.assertTrue(np.allclose(output, self.sess.run(tf_vector_max_pool(input_tensor))))

    def test_max_4(self):
        input_np = np.asarray([
            [
                [[0, 0], [1, -4], [0, 0], [0, 0]],
                [[5, 1], [0, 0], [0, 0], [1, 2]],
                [[1, 5], [8, 8], [0, 0], [0, 0]],
                [[8, 9], [0, 0], [0, 0], [0, 0]]
            ],
            [
                [[0, 0], [0, 0], [0, 9], [0, 0]],
                [[0, 0], [0, 0], [0, 0], [1, 2]],
                [[0, 0], [8, 8], [0, 0], [0, 0]],
                [[8, 8], [0, 0], [-8, 0], [0, 0]]
            ]
        ])
        output = np.asarray([
            [
                [[8, 9]]
            ],
            [
                [[8, 8]]
            ]
        ])
        input_tensor = tf.constant(input_np, dtype=tf.float32)
        self.assertTrue(np.allclose(output, self.sess.run(vector_max_pool(input_tensor, kernel_size=4))))
        self.assertTrue(np.allclose(output, self.sess.run(tf_vector_max_pool(input_tensor, kernel_size=4))))

    def test_max_32(self):
        input_np = np.random.rand(8, 256, 512, 3) * 128
        input_tensor = tf.constant(input_np, dtype=tf.float32)
        self.assertTrue(np.allclose(self.sess.run(tf_vector_max_pool(input_tensor, kernel_size=32)),
                        self.sess.run(vector_max_pool(input_tensor, kernel_size=32))))

    def test_min_size(self):
        input_np = np.asarray([
            [
                [[0, 0], [1, 4]],
                [[5, 1], [0, 0]]
            ]
        ])
        output = np.asarray([
            [
                [[5, 1]],
            ]
        ])
        input_tensor = tf.constant(input_np, dtype=tf.float32)
        self.assertTrue(np.allclose(output, self.sess.run(vector_max_pool(input_tensor))))
        self.assertTrue(np.allclose(output, self.sess.run(tf_vector_max_pool(input_tensor))))

    def test_different_dimensions(self):
        input_np = np.asarray([
            [
                [[0, 0], [1, 4], [0, 0], [0, 0]],
                [[-5, -9], [0, 0], [0, 0], [1, 2]]
            ],
            [
                [[0, 0], [0, 0], [0, 9], [0, 0]],
                [[0, 0], [0, 0], [0, 0], [1, 2]]
            ]
        ])
        output = np.asarray([
            [
                [[-5, -9], [1, 2]]
            ],
            [
                [[0, 0], [0, 9]]
            ]
        ])
        input_tensor = tf.constant(input_np, dtype=tf.float32)
        self.assertTrue(np.allclose(output, self.sess.run(vector_max_pool(input_tensor))))
        self.assertTrue(np.allclose(output, self.sess.run(tf_vector_max_pool(input_tensor))))

    def test_three_channels(self):
        input_np = np.asarray([
            [
                [[0, 0, 3], [1, 4, 0]],
                [[-5, 1, 0], [0, 0, 0]]
            ]
        ])
        output = np.asarray([
            [
                [[-5, 1, 0]],
            ]
        ])
        input_tensor = tf.constant(input_np, dtype=tf.float32)
        self.assertTrue(np.allclose(output, self.sess.run(vector_max_pool(input_tensor))))
        self.assertTrue(np.allclose(output, self.sess.run(tf_vector_max_pool(input_tensor))))

    def test_same_as_single_channel_max_pool(self):
        batch = 3
        height = 16
        width = 8
        channels = 1
        input_np = np.random.rand(batch, height, width, channels)
        input_tensor = tf.constant(input_np, dtype=tf.float32)
        expected = self.sess.run(tf.layers.max_pooling2d(input_tensor, 2, 2))
        self.assertTrue(np.allclose(expected, self.sess.run(vector_max_pool(input_tensor))))
        self.assertTrue(np.allclose(expected, self.sess.run(tf_vector_max_pool(input_tensor))))

    def test_gradients(self):
        with self.sess:
            input_np = np.asarray([
                [
                    [[0, 0], [1, -4], [0, 0], [0, 0]],
                    [[5, 1], [0, 0], [0, 0], [1, 2]],
                    [[1, 5], [8, 8], [0, 0], [0, 0]],
                    [[8, 9], [0, 0], [0, 0], [0, 0]]
                ],
                [
                    [[0, 0], [0, 0], [0, 9], [0, 0]],
                    [[0, 0], [0, 0], [0, 0], [1, 2]],
                    [[0, 0], [8, 8], [0, 0], [0, 0]],
                    [[8, 8], [0, 0], [-8, 0], [0, 0]]
                ]
            ])

            img_shape = input_np.shape
            output_shape = (img_shape[0], int(img_shape[1] / 2), int(img_shape[2] / 2), img_shape[3])
            input_tensor = tf.constant(input_np, dtype=tf.float32)
            pooled_tensor = vector_max_pool(input_tensor)

            error = gradient_checker.compute_gradient_error(input_tensor, img_shape, pooled_tensor, output_shape)
            self.assertLessEqual(error, self.max_allowable_grad_err, 'Exceeded the error threshold.')

    def test_gradients_4(self):
        input_np = np.asarray([
            [
                [[0, 0], [1, -4], [0, 0], [0, 0]],
                [[5, 1], [0, 0], [0, 0], [1, 2]],
                [[1, 5], [8, 8], [0, 0], [0, 0]],
                [[8, 16], [0, 0], [0, 0], [0, 0]]
            ],
            [
                [[0, 0], [0, 0], [0, 9], [0, 0]],
                [[0, 0], [0, 0], [50, 0], [1, 2]],
                [[0, 0], [8, -7], [0, 0], [0, 0]],
                [[8, 9], [0, 0], [-8, 0], [0, 0]]
            ]
        ])

        input_tensor = tf.constant(input_np, dtype=tf.float32)
        pooled_tensor = vector_max_pool(input_tensor, kernel_size=4)
        tf_pooled_tensor = tf_vector_max_pool(input_tensor, kernel_size=4)

        grad = tf.gradients(pooled_tensor, input_tensor)[0]
        expected_grad = tf.gradients(tf_pooled_tensor, input_tensor)[0]
        grad, expected_grad = self.sess.run([grad, expected_grad])
        self.assertTrue(np.allclose(expected_grad, grad, atol=1e-4))
        self.assertEqual(4.0, float(np.sum(grad)))

    def test_gradients_8(self):
        input_np = np.random.rand(5, 16, 32, 7)
        input_grad_np = np.random.rand(5, 2, 4, 7)

        input_tensor = tf.constant(input_np, dtype=tf.float32)
        input_grad_tensor = tf.constant(input_grad_np, dtype=tf.float32)
        pooled_tensor = vector_max_pool(input_tensor, kernel_size=8)
        tf_pooled_tensor = tf_vector_max_pool(input_tensor, kernel_size=8)

        grad = tf.gradients(pooled_tensor, input_tensor, grad_ys=input_grad_tensor)[0]
        expected_grad = tf.gradients(tf_pooled_tensor, input_tensor, grad_ys=input_grad_tensor)[0]
        grad, expected_grad = self.sess.run([grad, expected_grad])
        self.assertTrue(np.allclose(expected_grad, grad, atol=1e-4))
        self.assertGreater(float(np.sum(grad)), 0.0)

    def test_gradients_32(self):
        input_np = np.random.rand(8, 128, 256, 2)
        input_grad_np = np.random.rand(8, int(128/32), int(256/32), 2)

        input_tensor = tf.constant(input_np, dtype=tf.float32)
        input_grad_tensor = tf.constant(input_grad_np, dtype=tf.float32)
        pooled_tensor = vector_max_pool(input_tensor, kernel_size=32)
        tf_pooled_tensor = tf_vector_max_pool(input_tensor, kernel_size=32)

        grad = tf.gradients(pooled_tensor, input_tensor, grad_ys=input_grad_tensor)[0]
        expected_grad = tf.gradients(tf_pooled_tensor, input_tensor, grad_ys=input_grad_tensor)[0]
        grad, expected_grad = self.sess.run([grad, expected_grad])
        self.assertTrue(np.allclose(expected_grad, grad, atol=1e-4))
        self.assertGreater(float(np.sum(grad)), 0.0)

    def test_gradients_different_dimensions(self):
        with self.sess:
            input_np = np.asarray([
                [
                    [[0, 0], [1, 4], [0, 0], [0, 0]],
                    [[-5, -9], [0, 0], [0, 0], [1, 2]]
                ],
                [
                    [[0, 0], [0, 0], [0, 9], [0, 0]],
                    [[0, 0], [0, 0], [0, 0], [1, 2]]
                ]
            ])

            img_shape = input_np.shape
            output_shape = (img_shape[0], int(img_shape[1] / 2), int(img_shape[2] / 2), img_shape[3])
            input_tensor = tf.constant(input_np, dtype=tf.float32)
            pooled_tensor = vector_max_pool(input_tensor)

            error = gradient_checker.compute_gradient_error(input_tensor, img_shape, pooled_tensor, output_shape)
            self.assertLessEqual(error, self.max_allowable_grad_err, 'Exceeded the error threshold.')

    def test_gradients_three_channels(self):
        with self.sess:
            input_np = np.asarray([
                [
                    [[0, 0, 3], [1, 4, 0]],
                    [[-5, 1, 0], [0, 0, 0]]
                ]
            ])

            img_shape = input_np.shape
            output_shape = (img_shape[0], int(img_shape[1] / 2), int(img_shape[2] / 2), img_shape[3])
            input_tensor = tf.constant(input_np, dtype=tf.float32)
            pooled_tensor = vector_max_pool(input_tensor)

            error = gradient_checker.compute_gradient_error(input_tensor, img_shape, pooled_tensor, output_shape)
            self.assertLessEqual(error, self.max_allowable_grad_err, 'Exceeded the error threshold.')


if __name__ == '__main__':
    unittest.main()
