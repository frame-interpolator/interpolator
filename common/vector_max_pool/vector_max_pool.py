import tensorflow as tf
from common.utils.tf import load_op_library, norm
from tensorflow.python.framework import ops


# Load op library.
mod = load_op_library('vector_max_pool_op', 'build')


def vector_max_pool(images, kernel_size=2):
    """
    Implementation of max pool 2D except it does a maximization of vector magnitude along the channel dimension.
    Has the same output shape as tf.layers.max_pooling2d(images, kernel_size, kernel_size).
    If there is only 1 channel, then this is the same as
    tf.layers.max_pooling2d(input_tensor, kernel_size, kernel_size).
    :param images: Tensor of shape [B, H, W, C].
    :param kernel_size: Int. Pooling size and stride.
    :return: Tensor of shape [B, H / kernel_size, W / kernel_size, C].
    """
    if mod is not None:
        return mod.vector_max_pool(images, kernel_size=kernel_size)
    else:
        return tf_vector_max_pool(images)


if mod is not None:
    @ops.RegisterGradient('VectorMaxPool')
    def _VectorMaxPoolGrad(op, grad):
        return mod.vector_max_pool_grad(grad, op.inputs[0], kernel_size=op.get_attr('kernel_size'))


def tf_vector_max_pool(images, kernel_size=2):
    """
    Tensorflow-only implementation of vector max pool.
    :param images: Tensor of shape [B, H, W, C].
    :param kernel_size: Int. Pooling size and stride.
    :return: Tensor of shape [B, H / kernel_size, W / kernel_size, C].
    """
    with tf.name_scope('vector_max_pool'):
        B, H, W, C = tf.unstack(tf.shape(images))
        out_height = tf.cast(H / kernel_size, tf.int32)
        out_width = tf.cast(W / kernel_size, tf.int32)
        magnitudes = norm(images)
        norm_patches = tf.extract_image_patches(magnitudes, ksizes=[1, kernel_size, kernel_size, 1],
                                                strides=[1, kernel_size, kernel_size, 1],
                                                rates=[1, 1, 1, 1], padding='VALID')
        max_indices = tf.math.argmax(norm_patches, axis=-1)
        bool_mask = tf.one_hot(max_indices, kernel_size * kernel_size, on_value=True, off_value=False)
        image_patches = tf.extract_image_patches(images, ksizes=[1, kernel_size, kernel_size, 1],
                                                 strides=[1, kernel_size, kernel_size, 1],
                                                 rates=[1, 1, 1, 1], padding='VALID')
        image_patches = tf.reshape(image_patches, shape=[B, out_height, out_width, -1, C])
        pooled = tf.reshape(tf.boolean_mask(image_patches, bool_mask), shape=[B, out_height, out_width, C])
        return pooled
