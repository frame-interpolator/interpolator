from common.utils.tf import load_op_library


# Load op library.
mod = load_op_library('affine_transform_op', 'build')


def affine_transform(image, transform, is_optical_flow=False):
    """
    Applies an affine transform to the image.
    :param image: Tensor of shape [H, W, C].
    :param transform: Tensor of shape [2, 3]. The affine transformation matrix.
    :param is_optical_flow: Bool. Whether the image is an optical flow.
                            If it is, we will apply the transform on the values of the image as well.
    :return: Transformed image that has the same shape as the input image.
    """
    if mod is not None:
        transformed = mod.affine_transform(image, transform, is_optical_flow=is_optical_flow)
        return transformed
    else:
        raise NotImplementedError('Please build and install.')
