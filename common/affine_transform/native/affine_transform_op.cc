#define EIGEN_USE_THREADS

#include <memory>
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/framework/types.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/common_shape_fns.h"
#include <vector>
#include <iostream>

using namespace tensorflow;
using namespace std;


class AffineTransformOp : public OpKernel {
public:
    explicit AffineTransformOp(OpKernelConstruction* context) : OpKernel(context) {
        OP_REQUIRES_OK(context, context->GetAttr("is_optical_flow", &is_optical_flow_));
    }

    void Compute(OpKernelContext* context) override {
        const Tensor& input_image = context->input(0);
        const Tensor& transform = context->input(1);

        // Assert correct number of dimensions.
        OP_REQUIRES(context, input_image.dims() == 3,
                    errors::InvalidArgument("input_image expected to be a 3-D tensor."));
        OP_REQUIRES(context, transform.dims() == 2,
                    errors::InvalidArgument("transform expected to be a 2-D tensor (matrix)."));

        typename TTypes<float, 3>::ConstTensor input_image_data = input_image.tensor<float, 3>();
        typename TTypes<float, 2>::ConstTensor transform_data = transform.tensor<float, 2>();
        const int height = input_image_data.dimension(0);
        const int width = input_image_data.dimension(1);
        const int channels = input_image_data.dimension(2);

        OP_REQUIRES(context, transform_data.dimension(0) == 2,
                    errors::InvalidArgument("transform expected to be a 2x3 matrix."));
        OP_REQUIRES(context, transform_data.dimension(1) == 3,
                    errors::InvalidArgument("transform expected to be a 2x3 matrix."));

        if (is_optical_flow_) {
            OP_REQUIRES(context, channels == 2, errors::InvalidArgument("Optical flow must have 2 channels."));
        }

        // Set output data and shape, and allocate memory for output tensor.
        Tensor* output_image = NULL;
        Tensor* intermediate = NULL;
        TensorShape output_shape({ height, width, channels });
        OP_REQUIRES_OK(context, context->allocate_output(0, output_shape, &output_image));
        typename TTypes<float, 3>::Tensor output_image_data = output_image->tensor<float, 3>();
        const float* t = transform_data.data();

        // Compute the inverse of the transform. This is needed for the backwards warp.
        float det = (t[0] * t[4] - t[1] * t[3]);
        OP_REQUIRES(context, abs(det) > 0, errors::InvalidArgument("transform must be an invertible matrix."));
        float inv_det = 1.0 / det;
        float t_inv[6];
        t_inv[0] = inv_det * t[4];
        t_inv[4] = inv_det * t[0];
        t_inv[1] = -inv_det * t[1];
        t_inv[3] = -inv_det * t[3];
        t_inv[2] = -(t_inv[0] * t[2] + t_inv[1] * t[5]);
        t_inv[5] = -(t_inv[3] * t[2] + t_inv[4] * t[5]);

        // Access underlying arrays.
        const float* input_image_ptr = input_image_data.data();
        const float* intermediate_ptr = input_image_ptr;
        float* output_image_ptr = output_image_data.data();

        // If image was an optical flow, transform the flow vectors.
        // The optical flow is assumed to be in (x, y) order.
        vector<float> intermediate_data;
        if (is_optical_flow_) {
            const int n = height * width * channels;
            intermediate_data = vector<float>(n);
            for (int i = 0; i < n; i += 2) {
                float dst_x = input_image_ptr[i] * t[0] + input_image_ptr[i + 1] * t[1];
                float dst_y = input_image_ptr[i] * t[3] + input_image_ptr[i + 1] * t[4];
                intermediate_data[i] = dst_x;
                intermediate_data[i + 1] = dst_y;
            }
            intermediate_ptr = intermediate_data.data();
        }

        // Backward warp the image using the inverse transform.
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                // Compute the sample vectors.
                float x_cen = t_inv[0] * x + t_inv[1] * y + t_inv[2];
                float y_cen = t_inv[3] * x + t_inv[4] * y + t_inv[5];
                int x0 = floor(x_cen);
                int y0 = floor(y_cen);
                int y1 = y0 + 1;
                int x1 = x0 + 1;

                // Compute the interpolation weights.
                float w_right = x_cen - x0;
                float w_left = x1 - x_cen;
                float w_bottom = y_cen - y0;
                float w_top = y1 - y_cen;
                bool x0_bound = x0 >= 0 && x0 < width;
                bool x1_bound = x1 >= 0 && x1 < width;
                bool y0_bound = y0 >= 0 && y0 < height;
                bool y1_bound = y1 >= 0 && y1 < height;
                for (int c = 0; c < channels; ++c) {
                    float sum = 0;

                    // Bi-linear interpolate.
#define IDX(iy, ix) (c + channels * (ix + width * iy))
                    // Top-left neighbor.
                    if(x0_bound && y0_bound) {
                        sum += w_left * w_top * intermediate_ptr[IDX(y0, x0)];
                    }

                    // Top-right neighbor.
                    if(x1_bound && y0_bound) {
                        sum += w_right * w_top * intermediate_ptr[IDX(y0, x1)];
                    }

                    // Bottom-left neighbor.
                    if(x0_bound && y1_bound) {
                        sum += w_left * w_bottom * intermediate_ptr[IDX(y1, x0)];
                    }

                    // Bottom-right neighbor.
                    if(x1_bound && y1_bound) {
                        sum += w_right * w_bottom * intermediate_ptr[IDX(y1, x1)];
                    }
                    output_image_ptr[IDX(y, x)] = sum;
#undef IDX
                }
            }
        }
    }

private:
    bool is_optical_flow_;
};

REGISTER_OP("AffineTransform")
.Input("image: float")
.Input("transform: float")
.Attr("is_optical_flow: bool")
.Output("transformed_image: float")
.SetShapeFn([](shape_inference::InferenceContext* c) {
    c->set_output(0, c->input(0));
    return Status::OK();
});


REGISTER_KERNEL_BUILDER(Name("AffineTransform")
.Device(DEVICE_CPU), AffineTransformOp);
