import numpy as np
import tensorflow as tf
import unittest
from common.affine_transform.affine_transform import affine_transform


class TestAffineTransform(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        np.set_printoptions(precision=3)

    def test_zeros_identity(self):
        image_shape = (6, 4, 1)
        transform = [[1, 0, 0],
                     [0, 1, 0]]
        image = np.zeros(image_shape)
        expected = np.zeros(image_shape)
        image_tensor = tf.placeholder(shape=image_shape, dtype=tf.float32)
        transformed_tensor = affine_transform(image_tensor, transform, is_optical_flow=False)
        transformed = self.sess.run(transformed_tensor, feed_dict={image_tensor: image})
        self.assertEqual(transformed.tolist(), expected.tolist())

    def test_ones_identity(self):
        image_shape = (6, 4, 1)
        transform = [[1, 0, 0],
                     [0, 1, 0]]
        image = np.ones(image_shape)
        expected = np.ones(image_shape)
        image_tensor = tf.placeholder(shape=image_shape, dtype=tf.float32)
        transformed_tensor = affine_transform(image_tensor, transform, is_optical_flow=False)
        transformed = self.sess.run(transformed_tensor, feed_dict={image_tensor: image})
        self.assertEqual(transformed.tolist(), expected.tolist())

    def test_scale_box(self):
        image = [[0, 0, 0],
                 [0, 1, 0],
                 [0, 0, 0]]
        expected = [[0, 0, 0],
                    [0, 0.25, 0.50],
                    [0, 0.50, 1]]
        transform = [[2, 0, 0],
                     [0, 2, 0]]
        image = np.expand_dims(image, axis=-1)
        expected = np.expand_dims(expected, axis=-1)
        image_tensor = tf.placeholder(shape=np.shape(image), dtype=tf.float32)
        transformed_tensor = affine_transform(image_tensor, transform, is_optical_flow=False)
        transformed = self.sess.run(transformed_tensor, feed_dict={image_tensor: image})
        self.assertEqual(transformed.tolist(), expected.tolist())

    def test_scale_box_flow(self):
        image = [[[0, 0], [0, 0], [0, 0]],
                 [[0, 0], [1, 1], [0, 0]],
                 [[0, 0], [0, 0], [0, 0]]]
        expected = [[[0, 0], [0, 0], [0, 0]],
                    [[0, 0], [0.5, 0.5], [1, 1]],
                    [[0, 0], [1, 1], [2, 2]]]
        transform = [[2, 0, 0],
                     [0, 2, 0]]
        image = np.array(image)
        expected = np.array(expected)
        image_tensor = tf.placeholder(shape=np.shape(image), dtype=tf.float32)
        transformed_tensor = affine_transform(image_tensor, transform, is_optical_flow=True)
        transformed = self.sess.run(transformed_tensor, feed_dict={image_tensor: image})
        self.assertEqual(transformed.tolist(), expected.tolist())

    def test_shear_box(self):
        image = [[0, 0, 0],
                 [0, 1, 0],
                 [0, 0, 0]]
        expected = [[0, 0, 0],
                    [0, 0.125, 0.375],
                    [0, 0.0, 0.5]]
        transform = [[2, 1, 0],
                     [0, 2, 0]]
        image = np.expand_dims(image, axis=-1)
        expected = np.expand_dims(expected, axis=-1)
        image_tensor = tf.placeholder(shape=np.shape(image), dtype=tf.float32)
        transformed_tensor = affine_transform(image_tensor, transform, is_optical_flow=False)
        transformed = self.sess.run(transformed_tensor, feed_dict={image_tensor: image})
        self.assertEqual(transformed.tolist(), expected.tolist())

    def test_shear_box_flow(self):
        image = [[[0, 0], [0, 0], [0, 0]],
                 [[0, 0], [1, 1], [0, 0]],
                 [[0, 0], [0, 0], [0, 0]]]
        expected = [[[0, 0], [0, 0],        [0, 0]],
                    [[0, 0], [0.375, 0.25], [1.125, 0.75]],
                    [[0, 0], [0, 0],        [1.5, 1]]]
        transform = [[2, 1, 0],
                     [0, 2, 0]]
        image = np.array(image)
        expected = np.array(expected)
        image_tensor = tf.placeholder(shape=np.shape(image), dtype=tf.float32)
        transformed_tensor = affine_transform(image_tensor, transform, is_optical_flow=True)
        transformed = self.sess.run(transformed_tensor, feed_dict={image_tensor: image})
        self.assertEqual(transformed.tolist(), expected.tolist())

    def test_shift(self):
        image = [[0, 0],
                 [1, 0]]
        expected = [[0, 1],
                    [0, 0]]
        transform = [[1, 0, 1],
                     [0, 1, -1]]
        image = np.expand_dims(image, axis=-1)
        expected = np.expand_dims(expected, axis=-1)
        image_tensor = tf.placeholder(shape=np.shape(image), dtype=tf.float32)
        transformed_tensor = affine_transform(image_tensor, transform, is_optical_flow=False)
        transformed = self.sess.run(transformed_tensor, feed_dict={image_tensor: image})
        self.assertEqual(transformed.tolist(), expected.tolist())

    def test_shift_flow(self):
        image = [[[0, 0], [0, 0]],
                 [[1, 0], [0, 0]]]
        expected = [[[0, 0], [1, 0]],
                    [[0, 0], [0, 0]]]
        transform = [[1, 0, 1],
                     [0, 1, -1]]
        image = np.array(image)
        expected = np.array(expected)
        image_tensor = tf.placeholder(shape=np.shape(image), dtype=tf.float32)
        transformed_tensor = affine_transform(image_tensor, transform, is_optical_flow=True)
        transformed = self.sess.run(transformed_tensor, feed_dict={image_tensor: image})
        self.assertEqual(transformed.tolist(), expected.tolist())

    def test_scale_and_translate_box(self):
        image = [[0, 0, 0],
                 [0, 1, 0],
                 [0, 0, 0]]
        expected = [[0.25, 0.5, 0.25],
                    [0.5, 1, 0.5],
                    [0.25, 0.5, 0.25]]
        transform = [[2, 0, -1],
                     [0, 2, -1]]
        image = np.expand_dims(image, axis=-1)
        expected = np.expand_dims(expected, axis=-1)
        image_tensor = tf.placeholder(shape=np.shape(image), dtype=tf.float32)
        transformed_tensor = affine_transform(image_tensor, transform, is_optical_flow=False)
        transformed = self.sess.run(transformed_tensor, feed_dict={image_tensor: image})
        self.assertEqual(transformed.tolist(), expected.tolist())

    def test_scale_and_translate_box_flow(self):
        image = [[[0, 0], [0, 0], [0, 0]],
                 [[0, 0], [1, 1], [0, 0]],
                 [[0, 0], [0, 0], [0, 0]]]
        expected = [[[0.5, 0.5], [1, 1], [0.5, 0.5]],
                    [[1, 1], [2, 2], [1, 1]],
                    [[0.5, 0.5], [1, 1], [0.5, 0.5]]]
        transform = [[2, 0, -1],
                     [0, 2, -1]]
        image = np.array(image)
        expected = np.array(expected)
        image_tensor = tf.placeholder(shape=np.shape(image), dtype=tf.float32)
        transformed_tensor = affine_transform(image_tensor, transform, is_optical_flow=True)
        transformed = self.sess.run(transformed_tensor, feed_dict={image_tensor: image})
        self.assertEqual(transformed.tolist(), expected.tolist())


if __name__ == '__main__':
    unittest.main()
