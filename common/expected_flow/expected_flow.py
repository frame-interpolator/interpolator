import tensorflow as tf


def expected_flow(corr, do_normalize=False):
    """
    :param corr: Tensor of shape [batch, H, W, L, K].
                 This should be a probability distribution describing the flow at each image location.
    :param do_normalize: Whether to normalize coordinates to be within [-1, 1].
    :return: The optical flow tensor of shape [batch, H, W, 2], in order (x, y).
    """
    with tf.name_scope('expected_flow'):
        # Create shifted meshgrid.
        L = tf.shape(corr)[3]
        K = tf.shape(corr)[4]
        x_1d = tf.cast(tf.range(0, K) - K // 2, tf.float32)
        y_1d = tf.cast(tf.range(0, L) - L // 2, tf.float32)
        if do_normalize:
            x_1d = x_1d / tf.cast((K // 2), tf.float32)
            y_1d = y_1d / tf.cast((L // 2), tf.float32)
        x_2d, y_2d = tf.meshgrid(x_1d, y_1d)

        # Perform weighted sum.
        coords = tf.stack([x_2d, y_2d], axis=-1)
        corr = tf.expand_dims(corr, axis=-1)
        flows = tf.reduce_sum(corr * coords, axis=[3, 4])
    return flows
