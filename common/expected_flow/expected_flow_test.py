import numpy as np
import tensorflow as tf
import unittest
from common.expected_flow.expected_flow import expected_flow
from pwcnet.cost_volume.cost_volume import cost_volume

PRINT_TEST = False


class TestExpectedFlow(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        self.error_thresh = 1E-6

    def test_zero_flow(self):
        corr_shape = (1, 1, 1, 5, 5)
        corr_placeholder = tf.placeholder(tf.float32, shape=corr_shape)
        flow_tensor = expected_flow(corr_placeholder)
        corr = 1.0 / 25.0 * np.ones(corr_shape)
        flows = self.sess.run(flow_tensor, feed_dict={corr_placeholder: corr})
        expected = np.array([0, 0])
        errors = np.mean(np.abs(expected - flows))
        self.assertLessEqual(errors, self.error_thresh)

    def test_up_left_flow(self):
        corr_shape = (1, 1, 1, 3, 3)
        corr_placeholder = tf.placeholder(tf.float32, shape=corr_shape)
        flow_tensor = expected_flow(corr_placeholder)
        corr = np.zeros(corr_shape)
        corr[..., 0, 0] = 1.0
        flows = self.sess.run(flow_tensor, feed_dict={corr_placeholder: corr})
        expected = np.array([-1, -1])
        errors = np.mean(np.abs(expected - flows))
        self.assertLessEqual(errors, self.error_thresh)

    def test_down_right_flow(self):
        corr_shape = (1, 2, 1, 5, 5)
        corr_placeholder = tf.placeholder(tf.float32, shape=corr_shape)
        flow_tensor = expected_flow(corr_placeholder)
        corr = 1.0 / 25.0 * np.ones(corr_shape)
        corr[0, 0, 0, ...] = 0.0
        corr[0, 0, 0, 4, 3] = 1.0
        flows = self.sess.run(flow_tensor, feed_dict={corr_placeholder: corr})
        expected = np.array([[1, 2], [0, 0]])
        errors = np.mean(np.abs(expected - np.squeeze(flows)))
        self.assertLessEqual(errors, self.error_thresh)

    def test_down_right_flow_normalized(self):
        corr_shape = (1, 2, 1, 5, 5)
        corr_placeholder = tf.placeholder(tf.float32, shape=corr_shape)
        flow_tensor = expected_flow(corr_placeholder, do_normalize=True)
        corr = 1.0 / 25.0 * np.ones(corr_shape)
        corr[0, 0, 0, ...] = 0.0
        corr[0, 0, 0, 4, 3] = 1.0
        flows = self.sess.run(flow_tensor, feed_dict={corr_placeholder: corr})
        expected = np.array([[0.5, 1.0], [0, 0]])
        errors = np.mean(np.abs(expected - np.squeeze(flows)))
        self.assertLessEqual(errors, self.error_thresh)

    def test_corr_flow(self):
        # Create dummy features.
        x_dim = np.arange(1, 4)
        y_dim = np.arange(1, 4)
        f_x, f_y = np.meshgrid(y_dim, x_dim)
        features = np.expand_dims(np.stack([f_x, f_y], axis=-1), axis=0)
        features_placeholder = tf.placeholder(tf.float32, shape=(1, 3, 3, 2))
        corr_tensor = cost_volume(features_placeholder, features_placeholder, search_range=2, do_normalize=True)
        corr_tensor_flattened = tf.reshape(corr_tensor, (1, 3, 3, 5 * 5))
        corr_tensor_flattened = tf.nn.softmax(1E4 * corr_tensor_flattened, axis=-1)
        corr_tensor = tf.reshape(corr_tensor_flattened, (1, 3, 3, 5, 5))
        corr_flows_tensor = expected_flow(corr_tensor)
        corr_flows, corr = self.sess.run([corr_flows_tensor, corr_tensor], feed_dict={features_placeholder: features})

        # Just to check if it makes sense.
        if PRINT_TEST:
            print(corr_flows)
            print(corr)
            print(features)
