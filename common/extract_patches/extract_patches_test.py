import unittest
import numpy as np
import tensorflow as tf
from common.extract_patches.extract_patches import extract_patches
from tensorflow.python.ops import gradient_checker


class TestExtractPatches(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        self.max_allowable_grad_err = 1e-4

    def test_forward(self):
        kernel_size = 3

        input = np.asarray([[
            [[1, 2], [3, 4]],
            [[5, 6], [7, 8]]
        ]])
        output = np.asarray([[
            [[0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 0, 0, 5, 6, 7, 8],
             [0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 0, 0, 5, 6, 7, 8, 0, 0]],
            [[0, 0, 1, 2, 3, 4, 0, 0, 5, 6, 7, 8, 0, 0, 0, 0, 0, 0],
             [1, 2, 3, 4, 0, 0, 5, 6, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0]],
        ]])

        input_tensor = tf.constant(input, dtype=tf.float32)
        patches_tensor = extract_patches(input_tensor, kernel_size=kernel_size)
        patches = self.sess.run(patches_tensor)
        self.assertTupleEqual((1, 2, 2, 2 * kernel_size * kernel_size), patches.shape)
        self.assertTrue(np.allclose(output, patches))

    def test_forward_consistency(self):
        batch = 3
        height = 16
        width = 32
        channels = 4
        kernel_size = 5
        input_tensor = tf.constant(np.random.rand(batch, height, width, channels), dtype=tf.float32)
        patches_tensor = extract_patches(input_tensor, kernel_size=kernel_size)
        patches_tf_tensor = tf.extract_image_patches(input_tensor, ksizes=[1, kernel_size, kernel_size, 1],
                                                     strides=[1, 1, 1, 1], rates=[1, 1, 1, 1], padding='SAME')

        patches, patches_tf = self.sess.run([patches_tensor, patches_tf_tensor])

        self.assertTupleEqual((batch, height, width, channels * kernel_size * kernel_size), patches.shape)
        self.assertTrue(np.allclose(patches, patches_tf))

    def test_backward_consistency(self):
        batch = 3
        height = 16
        width = 32
        channels = 4
        kernel_size = 3
        input_tensor = tf.constant(np.random.rand(batch, height, width, channels), dtype=tf.float32)
        grad_ys_tensor = tf.constant(np.random.rand(batch, height, width, channels * kernel_size * kernel_size),
                                     dtype=tf.float32)
        patches_tensor = extract_patches(input_tensor, kernel_size=kernel_size)
        patches_tf_tensor = tf.extract_image_patches(input_tensor, ksizes=[1, kernel_size, kernel_size, 1],
                                                     strides=[1, 1, 1, 1], rates=[1, 1, 1, 1], padding='SAME')
        patches_gradients_tensor = tf.gradients(patches_tensor, input_tensor, grad_ys=grad_ys_tensor)[0]
        patches_tf_gradients_tensor = tf.gradients(patches_tf_tensor, input_tensor, grad_ys=grad_ys_tensor)[0]

        patches_grad, patches_tf_grad = self.sess.run([patches_gradients_tensor, patches_tf_gradients_tensor])

        self.assertTupleEqual((batch, height, width, channels), patches_grad.shape)
        self.assertTrue(np.allclose(patches_grad, patches_tf_grad))

    def test_variable_input_size_gradients(self):
        batch = 3
        height = 16
        width = 32
        channels = 4
        kernel_size = 3
        input_tensor = tf.placeholder(dtype=tf.float32, shape=[None, None, None, channels])
        grad_ys_tensor = tf.placeholder(dtype=tf.float32, shape=[None, None, None,
                                                                 channels * kernel_size * kernel_size])
        patches_tensor = extract_patches(input_tensor, kernel_size=kernel_size)
        patches_gradients_tensor = tf.gradients(patches_tensor, input_tensor, grad_ys=grad_ys_tensor)[0]

        input = np.random.rand(batch, height, width, channels)
        grad_ys = np.random.rand(batch, height, width, channels * kernel_size * kernel_size)

        self.sess.run(patches_gradients_tensor, feed_dict={input_tensor: input, grad_ys_tensor: grad_ys})

    def test_gradients(self):
        with self.sess:
            img_shape = (8, 4, 5, 2)
            output_shape = (8, 4, 5, 2 * 3 * 3)
            input = tf.ones(shape=img_shape, dtype=tf.float32)
            patches_tensor = extract_patches(input, kernel_size=3)

            error = gradient_checker.compute_gradient_error(input, img_shape, patches_tensor, output_shape)
            self.assertLessEqual(error, self.max_allowable_grad_err, 'Exceeded the error threshold.')


if __name__ == '__main__':
    unittest.main()
