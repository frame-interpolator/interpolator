import numpy as np
import tensorflow as tf
from common.extract_patches.extract_patches import extract_patches
from common.utils.profile import run_profiler

if __name__ == '__main__':
    height = 256
    width = 256
    im_channels = 16
    batch_size = 4

    # Create the graph.
    image_shape = [batch_size, height, width, im_channels]
    flow_shape = [batch_size, height, width, 2]
    image_placeholder = tf.placeholder(shape=image_shape, dtype=tf.float32)
    extracted = extract_patches(image_placeholder, kernel_size=3)
    grads = tf.gradients(extracted, image_placeholder)[0]

    # Create dummy images.
    image = np.ones(shape=[batch_size, height, width, im_channels], dtype=np.float32)

    query = [extracted, grads]
    feed_dict = {image_placeholder: image}
    run_profiler(query, feed_dict, name='extract-patches')
