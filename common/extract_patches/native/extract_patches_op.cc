#define EIGEN_USE_THREADS

#include <memory>
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/framework/types.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/common_shape_fns.h"

typedef Eigen::ThreadPoolDevice CPUDevice;
typedef Eigen::GpuDevice GPUDevice;

using namespace tensorflow;

void ExtractPatches(const GPUDevice& d, int kernel_size,
	typename TTypes<float, 4>::ConstTensor images,
	typename TTypes<float, 4>::Tensor output);

void ExtractPatchesGrad(const GPUDevice& d, int kernel_size,
	typename TTypes<float, 4>::ConstTensor input_grad,
	typename TTypes<float, 4>::ConstTensor original_images,
	typename TTypes<float, 4>::Tensor output);

class ExtractPatchesOp : public OpKernel {
public:
	explicit ExtractPatchesOp(OpKernelConstruction* context) : OpKernel(context) {
	    OP_REQUIRES_OK(context, context->GetAttr("kernel_size", &kernel_size_));
		OP_REQUIRES(context, kernel_size_ > 0,
			errors::InvalidArgument("Need kernel_size > 0, got ", kernel_size_));
        OP_REQUIRES(context, kernel_size_ % 2 == 1,
			errors::InvalidArgument("kernel_size must be odd, got ", kernel_size_));
	}

	void Compute(OpKernelContext* context) override {
	    const Tensor& image = context->input(0);
	    OP_REQUIRES(context, image.dims() == 4,
                    errors::InvalidArgument("image expecting a 4-D vector."));

        TensorShape output_shape = image.shape();
        output_shape.set_dim(3, image.dim_size(3) * kernel_size_ * kernel_size_);

        Tensor* output = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(0, output_shape, &output));

		typename TTypes<float, 4>::ConstTensor image_data = image.tensor<float, 4>();
		typename TTypes<float, 4>::Tensor output_data = output->tensor<float, 4>();

		ExtractPatches(context->eigen_device<GPUDevice>(), kernel_size_,
		    image_data, output_data);
	}

private:
    int kernel_size_;
};

class ExtractPatchesOpGrad : public OpKernel {
public:
	explicit ExtractPatchesOpGrad(OpKernelConstruction* context) : OpKernel(context) {
	    OP_REQUIRES_OK(context, context->GetAttr("kernel_size", &kernel_size_));
		OP_REQUIRES(context, kernel_size_ > 0,
			errors::InvalidArgument("Need kernel_size > 0, got ", kernel_size_));
        OP_REQUIRES(context, kernel_size_ % 2 == 1,
			errors::InvalidArgument("kernel_size must be odd, got ", kernel_size_));
	}

	void Compute(OpKernelContext* context) override {
	    const Tensor& input_grads = context->input(0);
		const Tensor& original_images = context->input(1);

		Tensor* output_grads = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(0, original_images.shape(), &output_grads));

		typename TTypes<float, 4>::ConstTensor input_grads_data = input_grads.tensor<float, 4>();
		typename TTypes<float, 4>::ConstTensor original_images_data = original_images.tensor<float, 4>();
		typename TTypes<float, 4>::Tensor output_grads_data = output_grads->tensor<float, 4>();

        ExtractPatchesGrad(context->eigen_device<GPUDevice>(), kernel_size_,
			input_grads_data, original_images_data, output_grads_data);
	}

private:
    int kernel_size_;
};

using shape_inference::DimensionHandle;
using shape_inference::ShapeHandle;
using shape_inference::DimensionOrConstant;

REGISTER_OP("ExtractPatches")
.Attr("kernel_size: int = 3")
.Input("images: float")
.Output("output: float")
.SetShapeFn([](shape_inference::InferenceContext* c) {
	ShapeHandle in = c->input(0);
	DimensionHandle batch = c->Dim(in, 0);
	DimensionHandle height = c->Dim(in, 1);
	DimensionHandle width = c->Dim(in, 2);
	DimensionHandle channels = c->Dim(in, 3);
	int kernel_size;
	c->GetAttr<int>("kernel_size", &kernel_size);

    DimensionHandle output_channels;
    c->Multiply(channels, DimensionOrConstant(kernel_size * kernel_size), &output_channels);

	c->set_output(0, c->MakeShape({ batch, height, width, output_channels }));
	return Status::OK();
});

REGISTER_OP("ExtractPatchesGrad")
.Attr("kernel_size: int = 3")
.Input("grads: float")
.Input("original_images: float")
.Output("output_image_grad: float")
.SetShapeFn([](shape_inference::InferenceContext* c) {
	c->set_output(0, c->input(1));
	return Status::OK();
});

#if GOOGLE_CUDA

REGISTER_KERNEL_BUILDER(Name("ExtractPatches").Device(DEVICE_GPU), ExtractPatchesOp);
REGISTER_KERNEL_BUILDER(Name("ExtractPatchesGrad").Device(DEVICE_GPU), ExtractPatchesOpGrad);

#endif // GOOGLE_CUDA
