#if GOOGLE_CUDA

#define EIGEN_USE_GPU

#define _USE_MATH_DEFINES
#include <cmath>

#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/util/cuda_kernel_helper.h"

using namespace tensorflow;

typedef Eigen::GpuDevice GPUDevice;

__global__ void ExtractPatchesKernel(const int32 nthreads,
	const float* images, int batch, int height, int width, int channels, int kernel_size,
	float* output) {
	CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
		// out_idx = c + channels * (src_x + width * (src_y + height * b)).
		int idx = out_idx;
		const int c = idx % channels;
		idx /= channels;
		const int pixel_index = idx;
		const int src_x = idx % width;
		idx /= width;
		const int src_y = idx % height;
		const int b = idx / height;

        const int half_kernel = kernel_size / 2;
        const int kernel_squared = kernel_size * kernel_size;

#define IMG_OFFSET(iy, ix) (c + channels * ((ix) + width * ((iy) + height * b)))
#define OUT_OFFSET(ridx) (c + channels * ((ridx) + kernel_squared * pixel_index))
        int relative_idx = 0;
        for (int y = src_y - half_kernel; y <= src_y + half_kernel; ++y) {
            bool y_bounds = y >= 0 && y < height;
#pragma unroll
            for (int x = src_x - half_kernel; x <= src_x + half_kernel; ++x, ++relative_idx) {
                const int out_offset = OUT_OFFSET(relative_idx);
                if (x >= 0 && x < width && y_bounds) {
                    output[out_offset] = images[IMG_OFFSET(y, x)];
                }
                else {
                    output[out_offset] = 0.0f;
                }
            }
        }
#undef OUT_OFFSET
#undef IMG_OFFSET
	}
}

__global__ void ExtractPatchesGradKernel(const int32 nthreads,
	const float* input_grad, const float* images,
	int batch, int height, int width, int channels, int kernel_size,
	float* output) {
	CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
		// out_idx = c + channels * (src_x + width * (src_y + height * b)).
		int idx = out_idx;
		const int c = idx % channels;
		idx /= channels;
		const int pixel_index = idx;
		const int src_x = idx % width;
		idx /= width;
		const int src_y = idx % height;
		const int b = idx / height;

        const int half_kernel = kernel_size / 2;
        const int kernel_squared = kernel_size * kernel_size;

#define IMG_OFFSET(iy, ix) (c + channels * ((ix) + width * ((iy) + height * b)))
#define OUT_OFFSET(ridx) (c + channels * ((ridx) + kernel_squared * pixel_index))
        int relative_idx = 0;
        for (int y = src_y - half_kernel; y <= src_y + half_kernel; ++y) {
            bool y_bounds = y >= 0 && y < height;
#pragma unroll
            for (int x = src_x - half_kernel; x <= src_x + half_kernel; ++x, ++relative_idx) {
                if (x >= 0 && x < width && y_bounds) {
                    CudaAtomicAdd(output + IMG_OFFSET(y, x), input_grad[OUT_OFFSET(relative_idx)]);
                }
            }
        }
#undef OUT_OFFSET
#undef IMG_OFFSET
	}
}

void ExtractPatches(const GPUDevice& d, int kernel_size,
	typename TTypes<float, 4>::ConstTensor images,
	typename TTypes<float, 4>::Tensor output) {
	const int batch = images.dimension(0);
	const int height = images.dimension(1);
	const int width = images.dimension(2);
	const int channels = images.dimension(3);

	const int total_count = batch * height * width * channels;
	if (total_count == 0) return;

	CudaLaunchConfig config = GetCudaLaunchConfig(total_count, d);
	ExtractPatchesKernel
		<< <config.block_count, config.thread_per_block, 0, d.stream() >> >(
			config.virtual_thread_count, images.data(),
			batch, height, width, channels, kernel_size,
			output.data());
}

void ExtractPatchesGrad(const GPUDevice& d, int kernel_size,
	typename TTypes<float, 4>::ConstTensor input_grad,
	typename TTypes<float, 4>::ConstTensor original_images,
	typename TTypes<float, 4>::Tensor output) {
	const int batch = original_images.dimension(0);
	const int height = original_images.dimension(1);
	const int width = original_images.dimension(2);
	const int channels = original_images.dimension(3);

	int total_count = batch * height * width * channels;
	if (total_count == 0) return;

	// Initialize output with all zeros.
	CudaLaunchConfig config = GetCudaLaunchConfig(total_count, d);
	SetZero << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
		config.virtual_thread_count, output.data());

    // Accumulate gradients.
	ExtractPatchesGradKernel
		<< <config.block_count, config.thread_per_block, 0, d.stream() >> >(
			config.virtual_thread_count, input_grad.data(),
			original_images.data(), batch, height, width, channels, kernel_size,
			output.data());
}

#endif  // GOOGLE_CUDA
