import tensorflow as tf
from common.utils.tf import load_op_library
from tensorflow.python.framework import ops


# Load op library.
mod = load_op_library('extract_patches_op', 'build')


def extract_patches(images, kernel_size=3):
    """
    Implements tf.extract_image_patches but with variable-input-size gradients. At the time of writing this,
    tf.extract_image_patches does not implement variable-input-size-gradients.
    Implementation is the equivalent of:
        tf.extract_image_patches(images, ksizes=[1, kernel_size, kernel_size, 1], strides=[1, 1, 1, 1],
                                 rates=[1, 1, 1, 1], padding='SAME')
    :param images: Tensor of shape [B, H, W, C].
    :param kernel_size: Int. Size of the kernel. Must be an odd number.
    :return: Tensor of shape [B, H, W, kernel_size * kernel_size * C].
    """
    if mod is not None:
        return mod.extract_patches(images, kernel_size=kernel_size)
    else:
        return tf.extract_image_patches(images, ksizes=[1, kernel_size, kernel_size, 1], strides=[1, 1, 1, 1],
                                        rates=[1, 1, 1, 1], padding='SAME')


if mod is not None:
    @ops.RegisterGradient('ExtractPatches')
    def _ExtractPatchesGrad(op, grad):
        return mod.extract_patches_grad(grad, op.inputs[0], kernel_size=op.get_attr('kernel_size'))
