# To resolve some issues with MacOS and matplotlib:
# https://stackoverflow.com/questions/2512225/matplotlib-plots-not-showing-up-in-mac-osx
import platform
if platform.system() == 'Darwin':
    import matplotlib
    matplotlib.use('TkAgg')
import cv2
import io
from PIL import Image, ImageCms
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf


def show_image(img):
    """
    Opens a window displaying the image.
    :param img: Numpy array of shape (Height, Width, Channels)
    :return: Nothing.
    """
    plt.imshow(img)
    plt.show()


def read_image(img_path, as_float=False):
    """
    :param img_path: Str.
    :param as_float: Bool. If true, then return the image as floats between [0, 1] instead of uint8s between [0, 255].
    :return: Numpy array of shape [H, W, 3].
    """
    # This is to handle image ICC profiles. To simplify things we convert all images to standard RGB (sRGB).
    # https://stackoverflow.com/questions/50622180/does-pil-image-convertrgb-converts-images-to-srgb-or-adobergb
    def _convert_to_srgb(img):
        '''Convert PIL image to sRGB color space (if possible)'''
        icc = img.info.get('icc_profile', '')
        if icc:
            io_handle = io.BytesIO(icc)  # virtual file
            src_profile = ImageCms.ImageCmsProfile(io_handle)
            dst_profile = ImageCms.createProfile('sRGB')
            img = ImageCms.profileToProfile(img, src_profile, dst_profile)
        return img

    img = Image.open(img_path)
    img = np.array(_convert_to_srgb(img))
    if len(img.shape) == 2:
        img = np.stack(3 * [img], axis=-1)

    # Discard the alpha channel.
    if img.shape[2] == 4:
        img = img[..., :3]

    if as_float:
        return img.astype(dtype=np.float32) / 255.0
    else:
        return img


def write_image(img_path, image):
    """
    :param img_path: Str.
    :param image: Numpy array of floats (values between 0.0 and 1.0) or uint8s (values between 0 and 255).
                  Shape can either be [H, W, 1] or [H, W, 3]. If there are 3 channels, the channels are expected to be
                  RGB.
    :return: Nothing.
    """
    assert len(image.shape) == 3
    if image.dtype == np.float32:
        image = image * 255.0
        image = image.astype(np.uint8)
    if image.shape[2] == 3:
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    cv2.imwrite(img_path, image)


def pad_image_for_divisibility(image, n):
    """
    Pads an image with zeros such that the dimensions can be divisible by 2 at least n times.
    If the dimensions can already be sufficiently divisible by 2, it does nothing.
    :param image: Numpy array of shape [H, W, C].
    :param n: Int. Number of times the resolution needs to be divisible by 2.
    :return: Numpy array of shape [H_d, W_d, C] where H_d, W_d can be divisible by 2 n times.
    """
    original_height, original_width = image.shape[0], image.shape[1]
    height = int((2 ** n) * np.ceil(original_height / (2 ** n)))
    width = int((2 ** n) * np.ceil(original_width / (2 ** n)))
    delta_h = height - original_height
    delta_w = width - original_width
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)
    padded = cv2.copyMakeBorder(image, top, bottom, left, right, cv2.BORDER_CONSTANT, value=[0, 0, 0])
    # Maintain the columns.
    padded = np.reshape(padded, (padded.shape[0], padded.shape[1], image.shape[2]))
    return padded


def stretch_image_for_divisibility(image, n, nearest_neighbour=False):
    """
    Stretches an image such that the dimensions can be divisible by 2 at least n times.
    If the dimensions can already be sufficiently divisible by 2, it does nothing.
    :param image: Numpy array of shape [H, W, C].
    :param n: Int. Number of times the resolution needs to be divisible by 2.
    :param nearest_neighbour: Bool. Whether to do nearest neighbour interpolation.
    :return: Numpy array of shape [H_d, W_d, C] where H_d, W_d can be divisible by 2 n times.
    """
    original_height, original_width = image.shape[0], image.shape[1]
    height = int((2 ** n) * np.ceil(original_height / (2 ** n)))
    width = int((2 ** n) * np.ceil(original_width / (2 ** n)))
    return resize(image, height, width, nearest_neighbour=nearest_neighbour)


def resize(image, height, width, nearest_neighbour=False):
    """
    cv2 resize wrapper.
    :param image: Numpy array of shape [H, W, C].
    :param height: Int.
    :param width: Int.
    :param nearest_neighbour: Bool. Whether to do nearest neighbour interpolation.
    :return: Numpy array of shape [height, height, C].
    """
    interp_method = cv2.INTER_LINEAR
    if nearest_neighbour:
        interp_method = cv2.INTER_NEAREST
    resized = cv2.resize(image, (width, height), interpolation=interp_method)
    resized = np.reshape(resized, (resized.shape[0], resized.shape[1], image.shape[2]))
    return resized


def centered_crop(image, height, width):
    """
    Crops the image in the center by removing a border around the image.
    :param image: Numpy array of shape [H, W, C].
    :param height: Int. The new height.
    :param width: Int. The new width.
    :return: Numpy array of shape [height, width, C].
    """
    original_height, original_width = image.shape[0], image.shape[1]
    assert original_height >= height
    assert original_width >= width
    delta_h = original_height - height
    delta_w = original_width - width
    y, x = delta_h // 2, delta_w // 2
    return image[y:y + height, x:x + width, ...]


def crop_around_point(image, x, y, crop_height, crop_width):
    """
    Crops an image such that the crop is centered at (x, y). If the crop region is outside the image boundaries,
    the region will be bounded inside the image.
    :param image: Numpy image.
    :param x: Int.
    :param y: Int.
    :param crop_height: Int. Desired crop height.
    :param crop_width: Int. Desired crop width.
    :return:
    """
    image_height, image_width = image.shape[0], image.shape[1]
    assert crop_height <= image_height
    assert crop_width <= image_width
    top = int(crop_height / 2)
    left = int(crop_width / 2)
    bottom = crop_height - top
    right = crop_width - left

    if x - left < 0:
        x = left
    elif x + right >= image_width:
        x = image_width - right
    if y - top < 0:
        y = top
    elif y + bottom >= image_height:
        y = image_height - bottom

    return image[y - top:y + bottom, x - left:x + right]


def tf_random_crop(images, crop_size):
    """
    Picks a random crop region based on crop_size, and applies the same crop rect to all images.
    :param images: List of image tensors. Number of channels does not matter. Shape is (H, W, C).
    :param crop_size: Tuple of (int (H), int (W)). Size to crop the training examples to before feeding to network.
                      If None, then no cropping will be performed.
    :return: List of image tensors
    """
    if crop_size is not None and len(images) > 0:
        crop_height = crop_size[0]
        crop_width = crop_size[1]
        assert crop_height > 0 and crop_width > 0
        shape = tf.shape(images[0])
        H = shape[0]
        W = shape[1]
        # Pick out the crop region location.
        rand_y_start = tf.random_uniform((), 0, H - crop_height + 1, dtype=tf.int32)
        rand_x_start = tf.random_uniform((), 0, W - crop_width + 1, dtype=tf.int32)
        rand_y_end = rand_y_start + crop_height
        rand_x_end = rand_x_start + crop_width
        # Do cropping.
        cropped_images = []
        for image in images:
            cropped_images.append(image[rand_y_start:rand_y_end, rand_x_start:rand_x_end, :])
        return cropped_images
    return images


def tf_image_augmentation(images, config):
    """
    Does a set of basic image augmentations to a list of images.
    :param images: List of image tensors. Number of channels does not matter. Shape is (H, W, C).
    :param config: Dict.
    :return: List of image tensors
    """
    if len(images) > 0:
        shape = tf.shape(images[0])
        H = shape[-3]
        W = shape[-2]
        C = shape[-1]

        # Contrast.
        curr_contrast = tf.random_uniform((), config['contrast_min'], config['contrast_max'], dtype=tf.float32)
        # Gamma and gain.
        curr_gamma = tf.random_uniform((), config['gamma_min'], config['gamma_max'], dtype=tf.float32)
        curr_gain = tf.random_uniform((), config['gain_min'], config['gain_max'], dtype=tf.float32)
        # Brightness.
        curr_brightness = tf.random_normal((), mean=0.0, stddev=config['brightness_stddev'], dtype=tf.float32)
        # Color.
        curr_color = tf.random_uniform((1, 1, C), config['color_min'], config['color_max'], dtype=tf.float32)
        # Noise.
        rand_sigma = tf.random_uniform((), 0.0, config['noise_stddev'], dtype=tf.float32)

        randomized_images = []
        for i, image in enumerate(images):
            # Gaussian noise is created per image.
            rand_image = tf.random_normal((H, W, C), mean=0, stddev=rand_sigma, dtype=tf.float32)

            # Noise between images.
            # No noise is applied to the first image. Beyond the first image, noise accumulates.
            if i != 0:
                contrast_noise = tf.random_normal((), mean=0.0, stddev=config['contrast_noise'], dtype=tf.float32)
                gamma_noise = tf.random_normal((), mean=0.0, stddev=config['gamma_noise'], dtype=tf.float32)
                gain_noise = tf.random_normal((), mean=0.0, stddev=config['gain_noise'], dtype=tf.float32)
                brightness_noise = tf.random_normal((), mean=0.0, stddev=config['brightness_noise'], dtype=tf.float32)
                color_noise = tf.random_normal((1, 1, C), mean=0.0, stddev=config['color_noise'], dtype=tf.float32)
                curr_contrast = curr_contrast + contrast_noise
                curr_gamma = curr_gamma + gamma_noise
                curr_gain = curr_gain + gain_noise
                curr_brightness = curr_brightness + brightness_noise
                curr_color = curr_color + color_noise

            # Modify the color, compensating for the change in brightness.
            new_image = image
            color_in = tf.reduce_sum(new_image, axis=-1, keepdims=True)
            new_image = new_image * curr_color
            color_out = tf.reduce_sum(new_image, axis=-1, keepdims=True)
            new_image = new_image * (color_in / (color_out + 1e-2))

            # Modify gamma, brightness, and contrast.
            new_image = new_image ** curr_gamma
            new_image = new_image + curr_brightness
            new_image = new_image * curr_gain
            new_image = (new_image - 0.5) * curr_contrast + 0.5

            # Add noise.
            new_image = new_image + rand_image

            randomized_images.append(tf.clip_by_value(new_image, 0.0, 1.0))
        return randomized_images
    return images
