import subprocess
from appdirs import user_cache_dir


def get_cache_dir():
    """
    :return: Str. Cache directory for this project.
    """
    return user_cache_dir('interpolator')


# https://stackoverflow.com/questions/14989858/get-the-current-git-hash-in-a-python-script
def get_git_revision_short_hash():
    return subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).decode().strip()


# https://stackoverflow.com/questions/9764298/is-it-possible-to-sort-two-listswhich-reference-each-other-in-the-exact-same-w
def sort_in_unison(key_list, lists):
    """
    :param key_list: The list whose elements we use to sort.
    :param lists: A list of lists, each of which will be sorted in unison with key_list.
    :return: (sorted_key_list, sorted_lists)
    """
    indices = [*range(len(key_list))]
    indices.sort(key=key_list.__getitem__)
    sorted_lists = []
    sorted_key_list = [key_list[indices[i]] for i in range(len(key_list))]
    for list in lists:
        sorted_list = [list[indices[i]] for i in range(len(list))]
        sorted_lists.append(sorted_list)
    return sorted_key_list, sorted_lists


# Initially copied from https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
def print_progress_bar(iteration, total, prefix= '', suffix='', decimals=1, length=100, fill='█', use_percentage=True):
    """
    Call in a loop to create terminal progress bar.
    :param iteration: Current iteration (Int)
    :param total: Total iterations (Int)
    :param prefix: Prefix string (Str)
    :param suffix: Suffix string (Str)
    :param decimals: Positive number of decimals in percent complete (Int)
    :param length: Character length of bar (Int)
    :param fill: Bar fill character (Str)
    :param use_percentage: Whether to print percentage, or 'iter of total'.
    """
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '-' * (length - filled_length)

    if use_percentage:
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r', flush=True)
    else:
        ratio = '%d of %d' % (iteration, total)
        print('\r%s |%s| %s %s' % (prefix, bar, ratio, suffix), end='\r', flush=True)

    # Print New Line on Complete
    if iteration == total:
        print(flush=True)


def sorted_list_insert(sorted_list, score, info, max_len=None):
    """
    :param sorted_list: List of tuples (score, info). Sorted from highest to lowest.
    :param score: The score for the image.
    :param info: Any python object.
    :param max_len: The maximum length of the list.
    :return: The sorted list with the new tuple inserted.
    """
    if max_len is 0:
        return []

    tuple = (score, info)
    if sorted_list is None or len(sorted_list) == 0:
        return [tuple]

    if sorted_list[-1][0] >= score:
        r_idx = len(sorted_list)
    elif sorted_list[0][0] <= score:
        r_idx = 0
    else:
        # Binary search insert into the list.
        l_idx = 0
        r_idx = len(sorted_list) - 1
        while l_idx < r_idx:
            c_idx = (l_idx + r_idx) // 2
            c_score = sorted_list[c_idx][0]
            if c_score == score:
                break
            elif c_score > score:
                l_idx = c_idx + 1
            else:
                r_idx = c_idx

    sorted_list.insert(r_idx, tuple)
    if max_len is not None and len(sorted_list) > max_len:
        sorted_list = sorted_list[:max_len]
    return sorted_list
