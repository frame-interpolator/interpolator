import unittest
from common.utils.data import *


class TestDataUtils(unittest.TestCase):
    def test_shard_empty_range(self):
        range = []
        shard_ranges = create_shard_ranges(range, shard_size=2)
        self.assertListEqual(shard_ranges, [])

        shard_ranges = create_shard_ranges(range, shard_size=0)
        self.assertListEqual(shard_ranges, [])

    def test_shard_one(self):
        range = [5]
        shard_ranges = create_shard_ranges(range, shard_size=2)
        self.assertListEqual(shard_ranges, [[5]])

        shard_ranges = create_shard_ranges(range, shard_size=256)
        self.assertListEqual(shard_ranges, [[5]])

        shard_ranges = create_shard_ranges(range, shard_size=1)
        self.assertListEqual(shard_ranges, [[5]])

    def test_shard_even_range(self):
        test_range = [3, 4, 5, 6, 7, 8]

        shard_ranges = create_shard_ranges(test_range, shard_size=6)
        self.assertListEqual(shard_ranges, [test_range])

        shard_ranges = create_shard_ranges(test_range, shard_size=3)
        self.assertListEqual(shard_ranges, [[3, 4, 5], [6, 7, 8]])

        shard_ranges = create_shard_ranges(test_range, shard_size=2)
        self.assertListEqual(shard_ranges, [[3, 4], [5, 6], [7, 8]])

        shard_ranges = create_shard_ranges(test_range, shard_size=1)
        self.assertListEqual(shard_ranges, [[3], [4], [5], [6], [7], [8]])

        shard_ranges = create_shard_ranges(test_range, shard_size=0)
        self.assertListEqual(shard_ranges, [])

    def test_shard_odd_range(self):
        test_range = [2, 3, 4, 5, 6, 7, 8]

        shard_ranges = create_shard_ranges(test_range, shard_size=6)
        self.assertListEqual(shard_ranges, [[2, 3, 4, 5, 6, 7], [8]])

        shard_ranges = create_shard_ranges(test_range, shard_size=4)
        self.assertListEqual(shard_ranges, [[2, 3, 4, 5], [6, 7, 8]])

        shard_ranges = create_shard_ranges(test_range, shard_size=3)
        self.assertListEqual(shard_ranges, [[2, 3, 4], [5, 6, 7], [8]])

        shard_ranges = create_shard_ranges(test_range, shard_size=2)
        self.assertListEqual(shard_ranges, [[2, 3], [4, 5], [6, 7], [8]])

    def test_get_group_and_idx(self):
        path = 'foo_100.jpg'
        group, idx = get_group_and_idx(path)
        self.assertEqual('foo', group)
        self.assertEqual('100', idx)

    def test_get_group_and_idx_underscores(self):
        path = os.path.join('ha', 'boo', 'bar_tender', 'foo_what_10.jpg')
        group, idx = get_group_and_idx(path)
        self.assertEqual('foo_what', group)
        self.assertEqual('10', idx)

    def test_get_group_and_idx_numbers(self):
        path = os.path.join('ha', 'boo', 'bar_tender_10', 'seq_90_1_22.png')
        group, idx = get_group_and_idx(path)
        self.assertEqual('seq_90_1', group)
        self.assertEqual('22', idx)

    def test_get_group_and_idx_numbers_cur_dir(self):
        path = 'seq_90_1_22.jpg'
        group, idx = get_group_and_idx(path)
        self.assertEqual('seq_90_1', group)
        self.assertEqual('22', idx)

    def test_get_group_and_idx_numbers_no_sep(self):
        path = '0022.jpg'
        group, idx = get_group_and_idx(path, use_underscore_sep=False)
        self.assertEqual('', group)
        self.assertEqual('0022', idx)

    def test_get_group_and_idx_fail_1(self):
        path = os.path.join('ha', 'foo.jpg')
        group, idx = get_group_and_idx(path)
        self.assertEqual(None, group)
        self.assertEqual(None, idx)

    def test_get_group_and_idx_fail_2(self):
        path = os.path.join('ha', 'ho', 'fo_2_oo.jpg')
        group, idx = get_group_and_idx(path)
        self.assertEqual(None, group)
        self.assertEqual(None, idx)

    def test_get_group_and_idx_fail_3(self):
        path = 'fo_2_o_.jpg'
        group, idx = get_group_and_idx(path)
        self.assertEqual(None, group)
        self.assertEqual(None, idx)

    def test_group_files_by_directory_unsorted(self):
        grouped = group_files_by_directory([
            'z/b/c/a.png',
            'a/b/c/a.png',
            'a/b/c/c.png',
            'z/b/c/b.png',
            'a/b/c/b.png'
        ])
        self.assertEqual([
            ['a/b/c/a.png', 'a/b/c/b.png', 'a/b/c/c.png'],
            ['z/b/c/a.png', 'z/b/c/b.png']
        ], grouped)

    def test_group_files_by_directory_single_file_at_end(self):
        grouped = group_files_by_directory([
            'a/b/c/a.png',
            'a/b/c/c.png',
            'z/b/c/b.png',
            'a/b/c/b.png'
        ])
        self.assertEqual([
            ['a/b/c/a.png', 'a/b/c/b.png', 'a/b/c/c.png'],
            ['z/b/c/b.png']
        ], grouped)

    def test_group_files_by_directory_no_files(self):
        grouped = group_files_by_directory([])
        self.assertEqual([], grouped)

    def test_group_files_by_directory_one_file(self):
        grouped = group_files_by_directory([
            'z/b/c/a.png'
        ])
        self.assertEqual([
            ['z/b/c/a.png']
        ], grouped)

    def test_shard_list_exact(self):
        shards = shard_list([1, 2, 3, 4], 2)
        self.assertEqual([[1, 2], [3, 4]], shards)

    def test_shard_list_remainder(self):
        shards = shard_list([1, 2, 3, 4], 3)
        self.assertEqual([[1, 2, 3], [4]], shards)

    def test_shard_list_empty(self):
        shards = shard_list([], 3)
        self.assertEqual([], shards)


if __name__ == '__main__':
    unittest.main()
