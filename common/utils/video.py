import cv2
import numpy as np


class VideoWriter:
    def __init__(self, video_path, fps=12):
        """
        :param video_path: Str. Path to the video that we will write to.
        :param fps: Int. Frames per second.
        """
        self.video_path = video_path
        self.fps = fps

        # Create when writing the first image.
        self.writer = None
        self.h = None
        self.w = None

    def write(self, image):
        """
        The very first image that is written determines the height and width of the video.
        :param image: Numpy array of dtype float32 and shape [H, W, 3], RGB. Values should be between 0 and 1.
        """
        h, w, _ = image.shape
        if self.writer is None:
            self.h = h
            self.w = w
            self.writer = cv2.VideoWriter(self.video_path, cv2.VideoWriter_fourcc(*'MJPG'), self.fps, (w, h))

        assert self.h == h
        assert self.w == w

        # OpenCV works with BGR by default, so reverse the channels.
        self.writer.write(np.uint8(255 * image)[..., ::-1])

    def release(self):
        if self.writer is not None:
            self.writer.release()
