# To resolve some issues with MacOS and matplotlib:
# https://stackoverflow.com/questions/2512225/matplotlib-plots-not-showing-up-in-mac-osx
import platform
if platform.system() == 'Darwin':
    import matplotlib
    matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
import io
import numpy as np
import os.path
import random
import tensorflow as tf
import cv2
from sys import platform
from common.utils.img import write_image
from tensorflow.python.ops import control_flow_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import state_ops
from tensorflow.python.framework import ops
from tensorflow.python.training import optimizer


def l2_diff(a, b):
    """
    Calculates the L2 norm of the difference between a and b along the last axis (dim = -1).
    :param a: Tensor.
    :param b: Tensor.
    :return: Tensor.
    """
    return norm(a - b)


def print_tensor_shape(x):
    print(x.get_shape().as_list())


def tf_coin_flip(heads_rate):
    rand_val = tf.random_uniform([1], minval=0.0, maxval=1.0)
    is_head = tf.less(rand_val, heads_rate)
    return is_head


# https://github.com/tensorflow/tensorflow/issues/7712
def pelu(x):
    """Parametric Exponential Linear Unit (https://arxiv.org/abs/1605.09332v1)."""
    with tf.variable_scope('activation', initializer=tf.constant_initializer(1.0)):
        shape = x.get_shape().as_list()[1:]
        alpha = tf.get_variable('alpha', shape)
        beta = tf.get_variable('beta', shape)
        positive = tf.nn.relu(x) * alpha / (beta + 1e-9)
        negative = alpha * (tf.exp((-tf.nn.relu(-x)) / (beta + 1e-9)) - 1)
        return negative + positive


# https://stackoverflow.com/questions/39975676/how-to-implement-prelu-activation-in-tensorflow
def prelu(_x):
    """
    :param _x: Tensor of shape [batch_size, H, W, C].
    :return: Prelu'd tensor of shape [batch_size, H, W, C].
    """
    with tf.variable_scope('activation', initializer=tf.constant_initializer(1.0)):
        alphas = tf.get_variable('alpha', _x.get_shape()[-1],
                                 initializer=tf.constant_initializer(0.0),
                                 dtype=tf.float32)
        pos = tf.nn.relu(_x)
        neg = alphas * (_x - abs(_x)) * 0.5
        return pos + neg


def sliding_window_slice(x, slice_locations):
    """
    :param x: The tensor to window slice.
    :param slice_locations: A list. The locations at which we gather values. Values are either 0 or 1.
                            E.g [1, 0, 1] means that at each window offset j, we form [x[j], x[j + 2]].
    :return: The window sliced tensor. Is 1 rank higher than x.
    """
    slice_indices = []
    for i in range(len(slice_locations)):
        if slice_locations[i] == 1:
            slice_indices.append(i)

    sequence_len = len(slice_indices)

    # Get the sliding window indices.
    slice_indices_tensor = tf.constant(slice_indices)
    num_offsets = tf.shape(x)[0] - tf.cast(len(slice_locations) - 1, tf.int32)

    def get_zeros(sequence_len, x):
        return tf.zeros(tf.concat([[1, sequence_len], tf.shape(x)[1:]], axis=0))

    def get_slice(slice_indices_tensor, num_offsets, x):
        num_offsets = tf.maximum(num_offsets, 0)

        # Compute the gather indices for forming sequences.
        tiled = tf.tile(slice_indices_tensor, [num_offsets])
        tiled = tf.expand_dims(tiled, axis=0)
        tiled = tf.reshape(tiled, [num_offsets, -1])
        offsets = tf.expand_dims(tf.range(0, num_offsets), axis=1)
        indices = tiled + offsets
        indices = tf.reshape(indices, [-1])

        # Gather and reshape.
        images = tf.gather(x, indices)
        images = tf.expand_dims(images, axis=0)
        final_shape = tf.concat([[num_offsets, sequence_len], tf.shape(images)[2:]], axis=0)
        images = tf.reshape(images, final_shape)
        return tf.cast(images, tf.float32)

    slices = tf.cond(
        num_offsets > 0,
        true_fn=lambda: get_slice(slice_indices_tensor, num_offsets, x),
        false_fn=lambda: get_zeros(sequence_len, x)
    )
    return slices


# Use this to restore when the variables from the checkpoint and current graph aren't exactly the same.
# Copied from: https://github.com/tensorflow/tensorflow/issues/312
def optimistic_restore(session, save_file):
    reader = tf.train.NewCheckpointReader(save_file)
    saved_shapes = reader.get_variable_to_shape_map()
    var_names = sorted([(var.name, var.name.split(':')[0]) for var in tf.global_variables()
                        if var.name.split(':')[0] in saved_shapes])
    restore_vars = []
    name2var = dict(zip(map(lambda x:x.name.split(':')[0], tf.global_variables()), tf.global_variables()))
    with tf.variable_scope('', reuse=True):
        for var_name, saved_var_name in var_names:
            curr_var = name2var[saved_var_name]
            var_shape = curr_var.get_shape().as_list()
            if var_shape == saved_shapes[saved_var_name]:
                restore_vars.append(curr_var)
    saver = tf.train.Saver(restore_vars)
    saver.restore(session, save_file)


# Mostly copied from: https://gist.github.com/gyglim/1f8dfb1b5c82627ae3efcfbbadb9f514#file-tensorboard_logging-py-L41
class Logger(object):
    """Logging in tensorboard without tensorflow ops."""

    def __init__(self, log_dir, graph):
        """Creates a summary writer logging to log_dir."""
        self.log_dir = log_dir
        self.writer = tf.summary.FileWriter(log_dir, graph)

    def log_scalar(self, tag, value, step):
        """Log a scalar variable.
        Parameter
        ----------
        tag : basestring
            Name of the scalar
        value
        step : int
            training iteration
        """
        summary = tf.Summary(value=[tf.Summary.Value(tag=tag,
                                                     simple_value=value)])
        self.writer.add_summary(summary, step)
        self.writer.flush()

    def log_images(self, tag, images, step, max_width=None):
        """Logs a list of images."""
        tag_is_list = False
        if type(tag) is list:
            assert len(tag) == len(images)
            tag_is_list = True

        im_summaries = []
        for nr, img in enumerate(images):
            # Resize image if it's too large. This can save us some disk space.
            if max_width is not None:
                cur_width = img.shape[1]
                if cur_width > max_width:
                    new_height = int(max_width / cur_width * img.shape[0])
                    img = cv2.resize(img, (max_width, new_height))

            # Squeeze if gray-scale so that the format is acceptable for matplotlib.
            if len(img.shape) > 2 and np.shape(img)[2] == 1:
                img = img[..., 0]

            # Write the image to a string. The cmap arg is ignored if it's RGB.
            s = io.BytesIO()
            plt.imsave(s, img, format='png', cmap='gray', vmin=0.0, vmax=1.0)

            # Create an Image object
            img_sum = tf.Summary.Image(encoded_image_string=s.getvalue(),
                                       height=img.shape[0],
                                       width=img.shape[1])
            # Create a Summary value
            tag_str = tag[nr] if tag_is_list else tag
            im_summaries.append(tf.Summary.Value(tag='%s/%d' % (tag_str, nr),
                                                 image=img_sum))

            # Expand if it was gray-scale.
            if len(np.shape(img)) == 2:
                img = np.expand_dims(img, axis=-1)

            # Write the image, independently of tensorboard logging.
            image_name = '%s-%d' % (tag_str, nr)
            write_image(os.path.join(self.log_dir, 'images', image_name + '.jpg'), img)

        # Create and write Summary
        summary = tf.Summary(value=im_summaries)
        self.writer.add_summary(summary, step)
        self.writer.flush()

    def log_histogram(self, tag, values, step, bins=1000):
        """Logs the histogram of a list/vector of values."""
        # Convert to a numpy array
        values = np.array(values)

        # Create histogram using numpy
        counts, bin_edges = np.histogram(values, bins=bins)

        # Fill fields of histogram proto
        hist = tf.HistogramProto()
        hist.min = float(np.min(values))
        hist.max = float(np.max(values))
        hist.num = int(np.prod(values.shape))
        hist.sum = float(np.sum(values))
        hist.sum_squares = float(np.sum(values ** 2))

        # Requires equal number as bins, where the first goes from -DBL_MAX to bin_edges[1]
        # See https://github.com/tensorflow/tensorflow/blob/master/tensorflow/core/framework/summary.proto#L30
        # Thus, we drop the start of the first bin
        bin_edges = bin_edges[1:]

        # Add bin edges and counts
        for edge in bin_edges:
            hist.bucket_limit.append(edge)
        for c in counts:
            hist.bucket.append(c)

        # Create and write Summary
        summary = tf.Summary(value=[tf.Summary.Value(tag=tag, histo=hist)])
        self.writer.add_summary(summary, step)
        self.writer.flush()

    def add_run_metadata(self, run_metadata, global_step):
        self.writer.add_run_metadata(run_metadata, 'step%d' % global_step, global_step=global_step)


# Copied from https://github.com/openai/iaf/blob/master/tf_utils/adamax.py.
# Commit 1f09a1b092d7bd406aededc5d5fc64fce766c55e.
class AdamaxOptimizer(optimizer.Optimizer):
    """Optimizer that implements the Adamax algorithm.
    See [Kingma et. al., 2014](http://arxiv.org/abs/1412.6980)
    ([pdf](http://arxiv.org/pdf/1412.6980.pdf)).
    @@__init__
    """

    def __init__(self, learning_rate=0.001, beta1=0.9, beta2=0.999, use_locking=False, name="Adamax"):
        super(AdamaxOptimizer, self).__init__(use_locking, name)
        self._lr = learning_rate
        self._beta1 = beta1
        self._beta2 = beta2

        # Tensor versions of the constructor arguments, created in _prepare().
        self._lr_t = None
        self._beta1_t = None
        self._beta2_t = None

    def _prepare(self):
        self._lr_t = ops.convert_to_tensor(self._lr, name="learning_rate")
        self._beta1_t = ops.convert_to_tensor(self._beta1, name="beta1")
        self._beta2_t = ops.convert_to_tensor(self._beta2, name="beta2")

    def _create_slots(self, var_list):
        # Create slots for the first and second moments.
        for v in var_list:
            self._zeros_slot(v, "m", self._name)
            self._zeros_slot(v, "v", self._name)

    def _apply_dense(self, grad, var):
        lr_t = math_ops.cast(self._lr_t, var.dtype.base_dtype)
        beta1_t = math_ops.cast(self._beta1_t, var.dtype.base_dtype)
        beta2_t = math_ops.cast(self._beta2_t, var.dtype.base_dtype)
        if var.dtype.base_dtype == tf.float16:
            eps = 1e-7  # Can't use 1e-8 due to underflow -- not sure if it makes a big difference.
        else:
            eps = 1e-8

        v = self.get_slot(var, "v")
        v_t = v.assign(beta1_t * v + (1. - beta1_t) * grad)
        m = self.get_slot(var, "m")
        m_t = m.assign(tf.maximum(beta2_t * m + eps, tf.abs(grad)))
        g_t = v_t / m_t

        var_update = state_ops.assign_sub(var, lr_t * g_t)
        return control_flow_ops.group(*[var_update, m_t, v_t])

    def _apply_sparse(self, grad, var):
        raise NotImplementedError("Sparse gradient updates are not supported.")


def leaky_relu(features, alpha=0.1, name=None):
    """
    Leaky relu wrapper function with the default alpha set to 0.1.
    :param features: Tensor.
    :param alpha: Slope of the activation function at x < 0.
    :param name: A name for the operation (optional).
    :return: Tensor. The activated value.
    """
    return tf.nn.leaky_relu(features, alpha=alpha, name=name)


def load_op_library(op_name, directory='build'):
    """
    Loads a Tensorflow native op, or returns None if not found.
    :param op_name: Str. Name of the op.
    :param directory: Str. Directory to search in.
    :return: Tensorflow op module, or None if the op was not found.
    """
    if platform == 'win32':
        lib_file_name = op_name + '.dll'
    else:
        lib_file_name = 'lib' + op_name + '.so'
    if os.path.isfile(os.path.join(directory, lib_file_name)):
        old_cwd = os.getcwd()
        os.chdir(directory)
        mod = tf.load_op_library(lib_file_name)
        os.chdir(old_cwd)
    else:
        print('Warning: No native implementation of', op_name, 'found. Falling back to the Tensorflow version.')
        mod = None
    return mod


def he_normal(seed=None, dtype=tf.float32):
    """
    This implementation is taken from the official Tensorflow implementation. This allows backwards compatibility.
    It draws samples from a truncated normal distribution centered on 0
    with `stddev = sqrt(2 / fan_in)`
    where `fan_in` is the number of input units in the weight tensor.
    References: He et al., http://arxiv.org/abs/1502.01852
    :param seed: A Python integer. Used to seed the random generator.
    :param dtype: Tensorflow type.
    :return: An initializer.
    """
    return tf.initializers.variance_scaling(
        scale=2.0, mode='fan_in', distribution='truncated_normal', seed=seed, dtype=dtype)


def convolve_with_gaussian(image, std_dev, kernel_radius, normalized=True):
    """
    Blurs the image by convolving it with a gaussian kernel channel-wise.
    :param image: Tensor of shape [B, H, W, C].
    :param std_dev: Standard deviation of the gaussian kernel.
    :param kernel_radius: Int. Radius of the kernel.
    :param normalized: Bool. Whether to normalize the gaussian.
    :return: Tensor of shape [B, H, W, C].
    """
    def np_kernel(width, std_dev):
        kernel = np.zeros((width, width), dtype=np.float32)
        center = int(width / 2)
        for row in range(width):
            for col in range(width):
                value = np.exp(-((row - center) ** 2 + (col - center) ** 2) / (2 * std_dev ** 2))
                kernel[row, col] = value
        if normalized:
            return kernel / (2 * np.pi * (std_dev ** 2))
        else:
            return kernel
    with tf.name_scope('gaussian_blur'):
        with tf.name_scope('shape'):
            channels = tf.shape(image)[3]
        with tf.name_scope('kernel'):
            conv_kernel = tf.constant(np_kernel(kernel_radius * 2 + 1, std_dev)[:, :, None, None], dtype=tf.float32)
            conv_kernel = tf.tile(conv_kernel, [1, 1, channels, 1])
        return tf.nn.depthwise_conv2d(image, conv_kernel, (1, 1, 1, 1), padding='SAME')


def norm(vectors, epsilon=1e-10):
    """
    Implementation of tf.norm(vectors, axis=-1, keepdims=True).
    The difference is that Tensorflow's sqrt(0) has numerical instabilities. Therefore, an epsilon is added here.
    :param vectors: Tensor.
    :return: Tensor with the last dimensions set to 1.
    """
    return tf.sqrt(tf.reduce_sum(tf.square(vectors), axis=-1, keepdims=True) + epsilon)


def sample_n_from_mask_py(mask, num_points):
    """
    Randomly samples exactly num_points from the mask.
    :param mask: Numpy array of shape [B, H, W, 1]. Values must be either 0.0 or 1.0.
    :param num_points: Int. Number of points to sample.
    :return: Numpy array of shape [B, H, W, 1]. Values are either 0.0 or 1.0.
    """
    batch_size, height, width = mask.shape[0], mask.shape[1], mask.shape[2]
    y = np.linspace(0, height - 1, height)
    x = np.linspace(0, width - 1, width)
    xs, ys = np.meshgrid(x, y)
    grid = np.concatenate((ys[..., None], xs[..., None]), axis=-1).reshape((-1, 2)).astype(np.int32)
    sampled_masks = np.zeros((batch_size, height, width, 1), dtype=np.float32)
    for batch_idx in range(batch_size):
        curr_mask = mask[batch_idx].reshape((-1,))
        # Get all points in the mask.
        masked_grid = grid[curr_mask > 0.5]
        np.random.shuffle(masked_grid)
        for i in range(min(masked_grid.shape[0], num_points)):
            row, col = masked_grid[i, 0], masked_grid[i, 1]
            sampled_masks[batch_idx, row, col, 0] = 1.0
    return sampled_masks


def sample_n_from_mask(mask, num_points, name='sample_from_mask'):
    """
    Randomly samples exactly num_points from the mask.
    :param mask: Tensor of shape [B, H, W, 1]. Values must be either 0.0 or 1.0.
    :param num_points: Int or tf.constant. Number of points to sample.
    :param name: Str.
    :return: Tensor of shape [B, H, W, 1]. Values are either 0.0 or 1.0.
    """
    with tf.name_scope(name):
        if isinstance(num_points, int):
            num_points = tf.constant(num_points, dtype=tf.int32)
        B, H, W, _ = tf.unstack(tf.shape(mask))
        sampled = tf.py_func(sample_n_from_mask_py, [mask, num_points], tf.float32,
                             stateful=True, name=name)
        # Convince tensorflow that the shape hasn't changed.
        return tf.reshape(sampled, [B, H, W, 1])


def sample_cdf_np(cdf, u):
    """
    Binary search for the smallest value that is <=u.
    I.e. cdf=[1, 3, 6] u=3.1 => return 2
        cdf=[1, 3, 6] u=3.0 => return 1
        cdf=[1, 3, 6] u=0.5 => return 0
    :param cdf: Array of values between 0 and C that monotonically increase.
    :param u: Value beween 0 and C.
    :return: Int. Index of the sampled value.
    """
    min_idx = 0
    max_idx = len(cdf) - 1
    curr_idx = int((max_idx + min_idx) / 2)
    while True:
        if min_idx >= max_idx:
            return curr_idx
        val = cdf[curr_idx]
        if u < val:
            max_idx = curr_idx
        elif u > val:
            min_idx = curr_idx + 1
        else:
            return curr_idx
        curr_idx = int((max_idx + min_idx) / 2)


def sample_n_from_weights_py(weights, num_points):
    """
    Samples N points based on a weighted probability mask.
    :param weights: Numpy array of shape [B, H, W, 1]. Float values. Represents probabilities.
    :param num_points: Int. Number of points to sample.
    :return: Numpy array of shape [B, H, W, 1]. Values are either 0.0 or 1.0.
    """
    batch_size, height, width = weights.shape[0], weights.shape[1], weights.shape[2]
    sampled_masks = np.zeros((batch_size, height, width, 1), dtype=np.float32)
    for batch_idx in range(batch_size):
        curr_weights = weights[batch_idx].reshape((-1,))
        # Do a cumulative sum.
        cum_sum = np.cumsum(curr_weights, axis=0)
        if cum_sum[-1] == 0.0:
            continue
        for i in range(num_points):
            u = random.uniform(0.0, cum_sum[-1])
            # index = col + width * row
            index = sample_cdf_np(cum_sum, u)
            col = int(index % width)
            row = int(index / width)
            sampled_masks[batch_idx, row, col, 0] = 1.0
    return sampled_masks


def sample_n_from_weights(weights, num_points, name='sample_from_weights'):
    """
    Randomly samples exactly num_points from a probability distribution.
    :param weights: Tensor of shape [B, H, W, 1]. Float values.
    :param num_points: Int or tf.constant. Number of points to sample.
    :param name: Str.
    :return: Tensor of shape [B, H, W, 1]. Values are either 0.0 or 1.0.
    """
    with tf.name_scope(name):
        if isinstance(num_points, int):
            num_points = tf.constant(num_points, dtype=tf.int32)
        B, H, W, _ = tf.unstack(tf.shape(weights))
        sampled = tf.py_func(sample_n_from_weights_py, [weights, num_points], tf.float32,
                             stateful=True, name=name)
        # Convince tensorflow that the shape hasn't changed.
        return tf.reshape(sampled, [B, H, W, 1])


def calculate_box_rotation_zoom_py(height, width, rotation):
    """
    Finds an axis aligned bounding box (AABB) that fits inside a rotated bounding box. The AABB's aspect ratio is also
    width:height. Therefore, this function simply returns a zoom factor such that the AABB dimensions are
    zoom * (width, height).
    :param height: Float. Height of the box.
    :param width: Float. Width of the box.
    :param rotation: Rotation of the box. Units are in degrees. Positive value is a CCW rotation.
    :return: Float. Amount of zoom.
    """
    def compute(height, width, rotation):
        # Compute function requires rotation to be positive and for width > height.
        # Coordinate system is (x: right, y: down).
        w, h = float(width), float(height)
        half_w, half_h = w / 2, h / 2
        theta = -rotation * np.pi / 180.0  # Negative indicates rotation in the CCW direction.
        cos, sin = np.cos(theta), np.sin(theta)
        r = np.asarray([[cos, -sin],
                        [sin, cos]], dtype=np.float32)
        c0 = np.matmul(r, np.asarray([[-half_w],
                                      [-half_h]], dtype=np.float32))
        c3 = np.matmul(r, np.asarray([[half_w],
                                      [half_h]], dtype=np.float32))
        a = np.asarray([[-2.0 * h, 2.0 * w],
                        [cos * cos - 1.0, sin * cos]], dtype=np.float32)
        c0_c3 = c0 + c3
        b = np.asarray([[w * c0_c3[1, 0] - h * c0_c3[0, 0]],
                        [c0[0, 0] * a[1, 0] + c0[1, 0] * a[1, 1]]], dtype=np.float32)
        p1 = np.matmul(np.linalg.inv(a), b)
        p2 = c3 + c0 - p1
        dp = p2 - p1
        return np.asarray(dp[0, 0] / w, dtype=np.float32)

    # Make the compute function work for any aspect ratio and rotation by doing equivalent angle wrapping and
    # width/height swapping.
    rotation = np.fmod(np.abs(rotation), 180.0)
    if rotation < 1e-3:
        return np.asarray(1.0, dtype=np.float32)
    if rotation > 90:
        rotation = 180.0 - rotation
    if height > width:
        return compute(width, height, rotation)
    else:
        return compute(height, width, rotation)


def calculate_box_rotation_zoom(height, width, rotation):
    """
    Finds an axis aligned bounding box (AABB) that fits inside a rotated bounding box. The AABB's aspect ratio is also
    width:height. Therefore, this function simply returns a zoom factor such that the AABB dimensions are
    zoom * (width, height).
    :param height: Float. Height of the box.
    :param width: Float. Width of the box.
    :param rotation: Rotation of the box. Units are in degrees. Positive value is a CCW rotation.
    :return: Float. Amount of zoom.
    """
    result = tf.py_func(calculate_box_rotation_zoom_py, [height, width, rotation], tf.float32, stateful=False,
                        name='calculate_box_rotation_zoom')
    return tf.reshape(result, ())


def group_norm(x, channels, groups=32, epsilon=1e-10, name='group_norm', reuse=tf.AUTO_REUSE):
    """
    Custom group norm implementation because tf.contrib.layers.group_norm requires all dimensions to be statically
    defined for some reason.
    Based on https://arxiv.org/pdf/1803.08494.pdf except using BHWC instead of BCHW.
    :param x: Input features of shape [B, H, W, C].
    :param channels: Int. Number of channels of x.
    :param groups: Int. Number of groups.
    :param epsilon: Float. Epsilon for numerical inaccuracies.
    :param name: String.
    :param reuse: Tensorflow reuse parameter.
    :return: Tensor with the same shape as x.
    """
    with tf.variable_scope(name, reuse=reuse):
        batch, height, width, _ = tf.unstack(tf.shape(x))
        groups = tf.minimum(groups, channels)
        x = tf.reshape(x, [batch, height, width, groups, channels // groups])
        mean, var = tf.nn.moments(x, [1, 2, 4], keep_dims=True)
        x = (x - mean) / tf.sqrt(var + epsilon)
        gamma = tf.get_variable('gamma', initializer=tf.ones((1, 1, 1, channels), dtype=tf.float32))
        beta = tf.get_variable('beta', initializer=tf.zeros((1, 1, 1, channels), dtype=tf.float32))
        x = tf.reshape(x, [batch, height, width, channels])
        x = x * gamma + beta
        return x
