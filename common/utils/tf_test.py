import cv2
import time
import unittest
from common.utils.tf import *


class TestTfUtils(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_sliding_window_slice_dense(self):
        x = tf.constant([
            [0, 0],
            [0, 1],
            [1, 0],
        ])
        expected = [
            [[0, 0], [0, 1]],
            [[0, 1], [1, 0]]
        ]
        slice_locations = [1, 1]
        sliced = sliding_window_slice(x, slice_locations)
        sliced_list = self.sess.run(sliced).tolist()
        self.assertListEqual(sliced_list, expected)

    def test_sliding_window_slice_sparse(self):
        x = tf.constant([
            [0, 0],
            [1, 1],
            [0, 0],
            [2, 2],
            [0, 0],
            [3, 3],
        ])
        expected = [
            [[0, 0], [0, 0], [0, 0]],
            [[1, 1], [2, 2], [3, 3]]
        ]
        slice_locations = [1, 0, 1, 0, 1]
        sliced = sliding_window_slice(x, slice_locations)
        sliced_list = self.sess.run(sliced).tolist()
        self.assertListEqual(sliced_list, expected)

    def test_sliding_window_slice_small(self):
        x = tf.constant([
            [1, 2.4]
        ])
        expected = [
            [[0, 0], [0, 0], [0, 0]]
        ]
        slice_locations = [1, 1, 1]
        sliced = sliding_window_slice(x, slice_locations)
        sliced_list = self.sess.run(sliced).tolist()
        self.assertListEqual(sliced_list, expected)

    def test_convolve_with_gaussian(self):
        mask = np.zeros((1, 5, 5, 1), dtype=np.float32)
        mask[:, 2, 2, :] = 1.0
        mask = tf.constant(mask, dtype=tf.float32)
        convolved = convolve_with_gaussian(mask, 1.5, 2)
        convolved = self.sess.run(convolved)
        expected = np.asarray([[0.01195525, 0.02328564, 0.02908025, 0.02328564, 0.01195525],
                               [0.02328564, 0.04535424, 0.05664058, 0.04535424, 0.02328564],
                               [0.02908025, 0.05664058, 0.07073553, 0.05664058, 0.02908025],
                               [0.02328564, 0.04535424, 0.05664058, 0.04535424, 0.02328564],
                               [0.01195525, 0.02328564, 0.02908025, 0.02328564, 0.01195525]])
        expected = expected[None, :, :, None]
        self.assertTrue(np.allclose(convolved, expected))

    def test_convolve_with_gaussian_multi_channel(self):
        image = np.zeros((1, 5, 5, 2), dtype=np.float32)
        image[:, 2, 2, :] = 1.0
        image = tf.constant(image, dtype=tf.float32)
        convolved = convolve_with_gaussian(image, 1.5, 2)
        convolved = self.sess.run(convolved)
        self.assertTupleEqual((1, 5, 5, 2), convolved.shape)
        expected = np.asarray([[0.01195525, 0.02328564, 0.02908025, 0.02328564, 0.01195525],
                               [0.02328564, 0.04535424, 0.05664058, 0.04535424, 0.02328564],
                               [0.02908025, 0.05664058, 0.07073553, 0.05664058, 0.02908025],
                               [0.02328564, 0.04535424, 0.05664058, 0.04535424, 0.02328564],
                               [0.01195525, 0.02328564, 0.02908025, 0.02328564, 0.01195525]])
        expected = expected[None, :, :, None]
        self.assertTrue(np.allclose(convolved[..., :1], expected))
        self.assertTrue(np.allclose(convolved[..., 1:], expected))

    def test_convolve_with_gaussian_is_normalized(self):
        mask = np.zeros((1, 21, 21, 1), dtype=np.float32)
        mask[:, 10, 10, :] = 1.0
        mask = tf.constant(mask, dtype=tf.float32)
        convolved = convolve_with_gaussian(mask, 1.5, 10)
        convolved = self.sess.run(convolved)
        sum = np.sum(convolved)
        self.assertAlmostEqual(1.0, float(sum))

    def test_norm(self):
        to_norm = np.asarray([
            [[1.0, 1.0], [0.0, -2.0], [1.0, 0.0], [0.0, 0.0], [3.0, 4.0]]
        ], dtype=np.float32)
        norm_tensor = norm(tf.constant(to_norm, dtype=tf.float32), epsilon=1e-20)
        self.assertTrue(np.allclose(
            np.asarray([
                [[np.sqrt(2.0)], [2.0], [1.0], [0.0], [5.0]]
            ]),
            self.sess.run(norm_tensor)
        ))


class TestMaskSampling(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_sample(self):
        mask = np.zeros((3, 7, 9, 1), dtype=np.float32)
        mask[0, 1:5, 0:3, :] = 1.0
        mask[2, 0, 0, :] = 1.0
        mask[2, 0, 1, :] = 1.0
        mask[2, 0, 2, :] = 1.0
        mask = tf.constant(mask, dtype=tf.float32)
        sampled_mask_tensor = sample_n_from_mask(mask, 3)
        sampled_mask = self.sess.run(sampled_mask_tensor)
        self.assertAlmostEquals(3.0, np.sum(sampled_mask[0]))
        self.assertAlmostEquals(0.0, np.sum(sampled_mask[1]))
        self.assertAlmostEquals(3.0, np.sum(sampled_mask[2]))

    def test_sample_more_than_mask(self):
        mask = np.zeros((2, 7, 9, 1), dtype=np.float32)
        mask[0, 1:5, 0:3, :] = 1.0
        mask_tensor = tf.constant(mask, dtype=tf.float32)
        sampled_mask_tensor = sample_n_from_mask(mask_tensor, int(np.sum(mask[0]) + 5))
        sampled_mask = self.sess.run(sampled_mask_tensor)
        self.assertAlmostEquals(np.sum(mask[0]), np.sum(sampled_mask[0]))
        self.assertAlmostEquals(0.0, np.sum(sampled_mask[1]))

    def test_sample_more_than_mask_tf_constant(self):
        mask = np.zeros((2, 7, 9, 1), dtype=np.float32)
        mask[0, 1:5, 0:3, :] = 1.0
        mask_tensor = tf.constant(mask, dtype=tf.float32)
        sampled_mask_tensor = sample_n_from_mask(mask_tensor, tf.constant(int(np.sum(mask[0]) + 5)))
        sampled_mask = self.sess.run(sampled_mask_tensor)
        self.assertAlmostEquals(np.sum(mask[0]), np.sum(sampled_mask[0]))
        self.assertAlmostEquals(0.0, np.sum(sampled_mask[1]))

    def test_entire_sample_is_inside_mask(self):
        mask = np.zeros((2, 7, 9, 1), dtype=np.float32)
        mask[0, 1:5, 3:8, :] = 1.0
        mask_tensor = tf.constant(mask, dtype=tf.float32)
        sampled_mask_tensor = sample_n_from_mask(mask_tensor, int(np.sum(mask[0])))
        sampled_mask = self.sess.run(sampled_mask_tensor)
        self.assertAlmostEquals(np.sum(mask[0]), np.sum(mask[0] * sampled_mask[0]))
        self.assertAlmostEquals(0, np.sum(mask[1] * sampled_mask[1]))

    def test_entire_sample_is_inside_mask_tf_constant(self):
        mask = np.zeros((2, 7, 9, 1), dtype=np.float32)
        mask[0, 1:5, 3:8, :] = 1.0
        mask_tensor = tf.constant(mask, dtype=tf.float32)
        sampled_mask_tensor = sample_n_from_mask(mask_tensor, tf.constant(int(np.sum(mask[0]))))
        sampled_mask = self.sess.run(sampled_mask_tensor)
        self.assertAlmostEquals(np.sum(mask[0]), np.sum(mask[0] * sampled_mask[0]))
        self.assertAlmostEquals(0, np.sum(mask[1] * sampled_mask[1]))

    def test_entire_sample_is_inside_mask_random(self):
        mask = np.zeros((2, 7, 9, 1), dtype=np.float32)
        mask[0, 1:5, 3:8, :] = 1.0
        mask_tensor = tf.constant(mask, dtype=tf.float32)
        num_hints = tf.random.uniform(shape=[], minval=np.sum(mask[0]), maxval=np.sum(mask[0]) + 1, dtype=tf.int32)
        sampled_mask_tensor = sample_n_from_mask(mask_tensor, num_hints)
        sampled_mask = self.sess.run(sampled_mask_tensor)
        self.assertAlmostEquals(np.sum(mask[0]), np.sum(mask[0] * sampled_mask[0]))
        self.assertAlmostEquals(0, np.sum(mask[1] * sampled_mask[1]))

    def test_sample_is_inside_mask(self):
        mask = np.zeros((2, 7, 9, 1), dtype=np.float32)
        mask[0, 1:5, 3:8, :] = 1.0
        mask_tensor = tf.constant(mask, dtype=tf.float32)
        sampled_mask_tensor = sample_n_from_mask(mask_tensor, 7)
        sampled_mask = self.sess.run(sampled_mask_tensor)
        self.assertAlmostEquals(7.0, np.sum(mask[0] * sampled_mask[0]))
        self.assertAlmostEquals(0, np.sum(mask[1] * sampled_mask[1]))

    def test_sample_nothing(self):
        mask = np.zeros((2, 7, 9, 1), dtype=np.float32)
        mask[0, 1:5, 3:8, :] = 1.0
        mask_tensor = tf.constant(mask, dtype=tf.float32)
        sampled_mask_tensor = sample_n_from_mask(mask_tensor, 0)
        sampled_mask = self.sess.run(sampled_mask_tensor)
        self.assertAlmostEquals(0.0, np.sum(mask[0] * sampled_mask[0]))
        self.assertAlmostEquals(0, np.sum(mask[1] * sampled_mask[1]))


class TestSampleCDF(unittest.TestCase):
    def test_sample_odd_right(self):
        cdf = np.cumsum([1.0, 2.0, 3.0])
        self.assertTrue(np.allclose([1.0, 3.0, 6.0], cdf))
        self.assertEqual(2, sample_cdf_np(cdf, 3.1))
        self.assertEqual(2, sample_cdf_np(cdf, 5.9))
        self.assertEqual(2, sample_cdf_np(cdf, 6.0))

    def test_sample_odd_left(self):
        cdf = np.cumsum([1.0, 2.0, 3.0])
        self.assertTrue(np.allclose([1.0, 3.0, 6.0], cdf))
        self.assertEqual(1, sample_cdf_np(cdf, 3.0))
        self.assertEqual(1, sample_cdf_np(cdf, 2.9))
        self.assertEqual(1, sample_cdf_np(cdf, 1.1))
        self.assertEqual(0, sample_cdf_np(cdf, 0.0))
        self.assertEqual(0, sample_cdf_np(cdf, 0.5))
        self.assertEqual(0, sample_cdf_np(cdf, 1.0))

    def test_sample_even_right(self):
        cdf = np.asarray([1.0, 2.0])
        self.assertEqual(1, sample_cdf_np(cdf, 2.0))
        self.assertEqual(1, sample_cdf_np(cdf, 1.1))

    def test_sample_even_left(self):
        cdf = np.asarray([1.0, 2.0])
        self.assertEqual(0, sample_cdf_np(cdf, 0.0))
        self.assertEqual(0, sample_cdf_np(cdf, 0.5))
        self.assertEqual(0, sample_cdf_np(cdf, 1.0))

    def test_sample_one_value(self):
        cdf = np.asarray([1.0])
        self.assertEqual(0, sample_cdf_np(cdf, 0.0))
        self.assertEqual(0, sample_cdf_np(cdf, 0.5))
        self.assertEqual(0, sample_cdf_np(cdf, 1.0))

    def test_sample_many_values(self):
        cdf = np.asarray([1.0, 10.0, 20.0, 50.0, 100.0, 1000.0])
        self.assertEqual(0, sample_cdf_np(cdf, 0.0))
        self.assertEqual(0, sample_cdf_np(cdf, 0.5))
        self.assertEqual(0, sample_cdf_np(cdf, 1.0))
        self.assertEqual(1, sample_cdf_np(cdf, 1.1))
        self.assertEqual(1, sample_cdf_np(cdf, 10.0))
        self.assertEqual(2, sample_cdf_np(cdf, 10.1))
        self.assertEqual(2, sample_cdf_np(cdf, 20.0))
        self.assertEqual(3, sample_cdf_np(cdf, 20.1))
        self.assertEqual(3, sample_cdf_np(cdf, 50.0))
        self.assertEqual(4, sample_cdf_np(cdf, 50.1))
        self.assertEqual(4, sample_cdf_np(cdf, 100.0))
        self.assertEqual(5, sample_cdf_np(cdf, 100.1))
        self.assertEqual(5, sample_cdf_np(cdf, 1000.0))

    def test_sample_larger(self):
        cdf = np.asarray([1.0, 10.0])
        self.assertEqual(1, sample_cdf_np(cdf, 10.1))

    def test_sample_smaller(self):
        cdf = np.asarray([1.0, 10.0])
        self.assertEqual(0, sample_cdf_np(cdf, -1.0))


class TestWeightedMaskSampling(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_sample_one(self):
        weights_np = np.asarray([
            [
                [[0.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]],
                [[1.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]]
            ],
            [
                [[0.0], [0.0], [1.0]],
                [[0.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]]
            ]
        ], dtype=np.float32)
        weights = tf.constant(weights_np, dtype=tf.float32)
        sampled_tensor = sample_n_from_mask(weights, 1)
        sampled = self.sess.run(sampled_tensor)
        self.assertAlmostEqual(1.0, np.sum(sampled[0]))
        self.assertAlmostEqual(1.0, np.sum(sampled[1]))
        self.assertTrue(np.allclose(weights_np, sampled))

    def test_sample_nothing(self):
        weights_np = np.asarray([
            [
                [[0.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]]
            ],
            [
                [[0.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]],
                [[0.0], [0.0], [1.0]],
                [[0.0], [0.0], [0.0]]
            ]
        ], dtype=np.float32)
        weights = tf.constant(weights_np, dtype=tf.float32)
        sampled_tensor = sample_n_from_mask(weights, 10)
        sampled = self.sess.run(sampled_tensor)
        self.assertAlmostEqual(0.0, np.sum(sampled[0]))
        self.assertAlmostEqual(1.0, np.sum(sampled[1]))

    def test_sample_one_with_constant(self):
        weights_np = np.asarray([
            [
                [[0.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]],
                [[1.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]]
            ],
            [
                [[0.0], [0.0], [1.0]],
                [[0.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]]
            ]
        ], dtype=np.float32)
        weights = tf.constant(weights_np, dtype=tf.float32)
        sampled_tensor = sample_n_from_mask(weights, tf.constant(1, dtype=tf.int32))
        sampled = self.sess.run(sampled_tensor)
        self.assertAlmostEqual(1.0, np.sum(sampled[0]))
        self.assertAlmostEqual(1.0, np.sum(sampled[1]))
        self.assertTrue(np.allclose(weights_np, sampled))

    def test_sample_two(self):
        weights_np = np.asarray([
            [
                [[0.0], [0.0], [1.0]],
                [[0.0], [0.0], [0.0]],
                [[1.0], [0.0], [0.0]],
                [[0.0], [0.0], [0.0]]
            ],
            [
                [[0.0], [0.0], [1.0]],
                [[0.0], [0.0], [0.0]],
                [[0.0], [1.0], [0.0]],
                [[0.0], [0.0], [0.0]]
            ]
        ], dtype=np.float32)
        weights = tf.constant(weights_np, dtype=tf.float32)
        sampled_tensor = sample_n_from_mask(weights, 1)
        sampled = self.sess.run(sampled_tensor)
        self.assertAlmostEqual(1.0, np.sum(sampled[0]))
        self.assertAlmostEqual(1.0, np.sum(sampled[1]))
        self.assertAlmostEqual(1.0, np.sum(weights_np[0] - sampled[0]))
        self.assertAlmostEqual(1.0, np.sum(weights_np[1] - sampled[1]))

    def test_probabilities(self):
        num_tries = 5000
        weights_np = np.asarray([
            [
                [[16.0], [0.0], [4.0]],
                [[0.0], [8.0], [0.0]]
            ]
        ], dtype=np.float32)
        weights = tf.constant(weights_np, dtype=tf.float32)
        sampled_tensor = sample_n_from_weights(weights, 1)
        total = np.zeros_like(weights_np)
        for i in range(num_tries):
            sampled = self.sess.run(sampled_tensor)
            total += sampled
        total *= np.sum(weights_np) / num_tries
        self.assertTrue(np.allclose(weights_np, np.round(total)), 'Potentially flaky.')

    def test_fill(self):
        weights_np = np.ones(shape=(1, 10, 10, 1), dtype=np.float32)
        weights_np[0, 3:6, 3:6, 0] = 0.0
        weights = tf.constant(weights_np, dtype=tf.float32)
        num_samples_ph = tf.placeholder(tf.int32, shape=())
        sampled_tensor = sample_n_from_mask(weights, num_samples_ph)
        sampled = self.sess.run(sampled_tensor, feed_dict={num_samples_ph: 5000})
        self.assertAlmostEqual(0.0, np.sum(sampled[0, 3:6, 3:6, 0]))
        self.assertAlmostEqual(91.0, np.sum(sampled), 'Potentially flaky.')

    def test_static_shape(self):
        weights_np = np.ones(shape=(1, 10, 10, 1), dtype=np.float32)
        weights = tf.constant(weights_np, dtype=tf.float32)
        num_samples_ph = tf.placeholder(tf.int32, shape=())
        sampled_tensor = sample_n_from_mask(weights, num_samples_ph)
        self.assertListEqual([1, 10, 10, 1], sampled_tensor.shape.as_list())

    def test_two_sample_probabilities(self):
        num_tries = 10000
        weights_np = np.asarray([
            [
                [[4.0], [1.0], [4.0]],
                [[0.0], [3.0], [2.0]]
            ]
        ], dtype=np.float32)
        weights = tf.constant(weights_np, dtype=tf.float32)
        sampled_tensor = sample_n_from_weights(weights, 2)
        total = np.zeros_like(weights_np)
        for i in range(num_tries):
            sampled = self.sess.run(sampled_tensor)
            total += sampled
        total *= np.sum(weights_np) / np.sum(total)
        self.assertTrue(np.allclose(weights_np, np.round(total)), 'Potentially flaky.')


class RotationZoomTest(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        self.width = tf.placeholder(dtype=tf.float32, shape=())
        self.height = tf.placeholder(dtype=tf.float32, shape=())
        self.angle = tf.placeholder(dtype=tf.float32, shape=())
        self.graph = calculate_box_rotation_zoom(self.height, self.width, self.angle)

    def test_identity(self):
        self.assertAlmostEqual(1.0, self.calculate_box_rotation_zoom(50, 50, 0.0), places=5)
        self.assertAlmostEqual(1.0, self.calculate_box_rotation_zoom(150, 50, 0.0), places=5)
        self.assertAlmostEqual(1.0, self.calculate_box_rotation_zoom(50, 150, 0.0), places=5)
        self.assertAlmostEqual(1.0, self.calculate_box_rotation_zoom(300, 100, 180.0), places=5)
        self.assertAlmostEqual(1.0, self.calculate_box_rotation_zoom(150, 50, 360.0), places=5)
        self.assertAlmostEqual(1.0, self.calculate_box_rotation_zoom(50, 150, 180.0), places=5)
        self.assertAlmostEqual(1.0, self.calculate_box_rotation_zoom(50, 150, -360.0), places=5)

    def test_90(self):
        self.assertAlmostEqual(1.0, self.calculate_box_rotation_zoom(100, 100, 90.0), places=5)
        self.assertAlmostEqual(1.0, self.calculate_box_rotation_zoom(50, 50, -90.0), places=5)
        self.assertAlmostEqual(1.0, self.calculate_box_rotation_zoom(50, 50, 90.0 + 180.0), places=5)
        self.assertAlmostEqual(0.5, self.calculate_box_rotation_zoom(200, 100, 90.0), places=5)
        self.assertAlmostEqual(0.5, self.calculate_box_rotation_zoom(50, 100, 90.0), places=5)
        self.assertAlmostEqual(0.5, self.calculate_box_rotation_zoom(200, 100, -90.0), places=5)
        self.assertAlmostEqual(0.5, self.calculate_box_rotation_zoom(100, 50, 90.0 + 180.0), places=5)

    def test_45(self):
        self.assertAlmostEqual(1.0 / np.sqrt(2), self.calculate_box_rotation_zoom(50, 50, 45.0), places=5)
        self.assertAlmostEqual(1.0 / np.sqrt(2), self.calculate_box_rotation_zoom(50, 50, 45.0 + 90.0), places=5)
        self.assertAlmostEqual(1.0 / np.sqrt(2), self.calculate_box_rotation_zoom(50, 50, -45.0), places=5)
        self.assertAlmostEqual(0.471404, self.calculate_box_rotation_zoom(50, 100, 45.0), places=5)
        self.assertAlmostEqual(0.471404, self.calculate_box_rotation_zoom(100, 50, -45.0), places=5)
        self.assertAlmostEqual(0.471404, self.calculate_box_rotation_zoom(100, 50, 135.0), places=5)
        self.assertAlmostEqual(0.628539, self.calculate_box_rotation_zoom(400, 500, 45.0), places=5)

    def test_rotation_zoom(self):
        self.assert_works(width=100, height=110, angle=90)
        self.assert_works(width=200, height=170, angle=45)
        self.assert_works(width=200, height=170, angle=-85)
        self.assert_works(width=200, height=170, angle=95)
        self.assert_works(width=170, height=200, angle=-85)
        self.assert_works(width=170, height=200, angle=95)
        self.assert_works(width=10, height=100, angle=13)
        self.assert_works(width=100, height=10, angle=19)
        self.assert_works(width=100, height=100, angle=73)
        self.assert_works(width=113, height=100, angle=73)
        self.assert_works(width=113, height=100, angle=-7)

    def assert_works(self, width, height, angle, save_output=False, print_time=False):
        """
        Automated test that checks if the center crop is all ones or not.
        :param width: Int.
        :param height: Int.
        :param angle: Float.
        :param save_output: Bool.
        :param print_time: Bool.
        :return: Nothing.
        """
        image = np.ones((height, width, 1), dtype=np.float32)
        rot_mat = cv2.getRotationMatrix2D((width / 2, height / 2), angle, 1)
        rotated = cv2.warpAffine(image, rot_mat, (width, height))
        start_time = time.time()
        factor = self.calculate_box_rotation_zoom(height, width, angle)
        end_time = time.time()
        if print_time:
            print('Run time (ms):', (end_time - start_time) * 1000)
        new_width, new_height = width * factor, height * factor
        start = (int(np.round((width - new_width) / 2)), int(np.round((height - new_height) / 2)))
        end = (int(np.round((width - new_width) / 2 + new_width - 1)),
               int(np.round((height - new_height) / 2 + new_height - 1)))
        inner_rect = rotated[start[1]+1:end[1], start[0]+1:end[0], ...]
        self.assertTrue(np.allclose(np.ones_like(inner_rect, dtype=np.float32), inner_rect))
        outer_rect = rotated[start[1]-1:end[1]+2, start[0]-1:end[0]+2, ...]
        self.assertFalse(np.allclose(np.ones_like(outer_rect, dtype=np.float32), outer_rect))
        if save_output:
            cv2.rectangle(rotated, start, end, (0, 0, 0), -1)
            cv2.imwrite('outputs/rotation_test.png', rotated * 255)

    def calculate_box_rotation_zoom(self, width, height, angle):
        return self.sess.run(self.graph, feed_dict={self.width: width, self.height: height, self.angle: angle})


class TestGroupNorm(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_norm_equal(self):
        num_features = 8
        x = np.zeros((2, 3, 4, num_features), dtype=np.float32)
        x[..., :int(num_features / 2)] = 1.0
        x = tf.constant(x, dtype=tf.float32)
        norm_tensor = group_norm(x, num_features, groups=2, name='norm_equal')
        self.sess.run(tf.global_variables_initializer())
        norm = self.sess.run(norm_tensor)
        expected = np.zeros((2, 3, 4, num_features), dtype=np.float32)
        self.assertTrue(np.allclose(expected, norm))

    def test_norm_split(self):
        num_features = 8
        x = np.zeros((2, 3, 4, num_features), dtype=np.float32)
        x[..., :int(num_features / 2)] = 1.0
        x = tf.constant(x, dtype=tf.float32)
        norm_tensor = group_norm(x, num_features, groups=1, name='norm_split')
        self.sess.run(tf.global_variables_initializer())
        norm = self.sess.run(norm_tensor)
        expected = -np.ones((2, 3, 4, num_features), dtype=np.float32)
        expected[..., :int(num_features / 2)] *= -1.0
        self.assertTrue(np.allclose(expected, norm), str(norm))


if __name__ == '__main__':
    unittest.main()
