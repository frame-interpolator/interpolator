import os.path
import unittest
from common.utils.data import silently_remove_file
from common.utils.img import *


class TestImgIO(unittest.TestCase):
    def setUp(self):
        self.image_path = os.path.join('common', 'utils', 'test_data', 'image_a.png')
        self.temp_image_path = os.path.join('common', 'utils', 'test_data', 'temp_image_a.png')

    def tearDown(self):
        silently_remove_file(self.temp_image_path)

    def test_read_write_uint8(self):
        image = read_image(self.image_path, as_float=False)
        write_image(self.temp_image_path, image)
        same_image = read_image(self.temp_image_path, as_float=False)
        self.assertTrue(np.allclose(image, same_image))

    def test_read_write_float(self):
        image = read_image(self.image_path, as_float=True)
        write_image(self.temp_image_path, image)
        same_image = read_image(self.temp_image_path, as_float=True)
        self.assertTrue(np.allclose(image, same_image))


class TestImagePadForDivisibility(unittest.TestCase):
    def test_pad_divisibility(self):
        image = np.ones(shape=(7, 9, 4), dtype=np.float32)
        padded_image = pad_image_for_divisibility(image, 2)
        expected_image = np.zeros(shape=(8, 12, 4), dtype=np.float32)
        expected_image[:7, 1:10, ...] = 1.0
        self.assertTupleEqual(expected_image.shape, padded_image.shape)
        self.assertTrue(np.allclose(expected_image, padded_image))

    def test_pad_divisibility_single_channel(self):
        image = np.ones(shape=(7, 9, 1), dtype=np.float32)
        padded_image = pad_image_for_divisibility(image, 2)
        expected_image = np.zeros(shape=(8, 12, 1), dtype=np.float32)
        expected_image[:7, 1:10, ...] = 1.0
        self.assertTupleEqual(expected_image.shape, padded_image.shape)
        self.assertTrue(np.allclose(expected_image, padded_image))

    def test_pad_divisibility_height_only(self):
        image = np.ones(shape=(436, 1024, 3), dtype=np.float32)
        padded_image = pad_image_for_divisibility(image, 6)
        expected_image = np.zeros(shape=(448, 1024, 3), dtype=np.float32)
        expected_image[6:6 + 436, :, ...] = 1.0
        self.assertTupleEqual(expected_image.shape, padded_image.shape)
        self.assertTrue(np.allclose(expected_image, padded_image))

    def test_pad_divisibility_width_only(self):
        image = np.ones(shape=(1024, 436, 2), dtype=np.float32)
        padded_image = pad_image_for_divisibility(image, 6)
        expected_image = np.zeros(shape=(1024, 448, 2), dtype=np.float32)
        expected_image[:, 6:6 + 436, ...] = 1.0
        self.assertTupleEqual(expected_image.shape, padded_image.shape)
        self.assertTrue(np.allclose(expected_image, padded_image))

    def test_pad_divisibility_three_times(self):
        image = np.ones(shape=(436, 436, 3), dtype=np.float32)
        padded_image = pad_image_for_divisibility(image, 3)
        expected_image = np.zeros(shape=(440, 440, 3), dtype=np.float32)
        expected_image[2:2 + 436, 2:2 + 436, ...] = 1.0
        self.assertTupleEqual(expected_image.shape, padded_image.shape)
        self.assertTrue(np.allclose(expected_image, padded_image))

    def test_pad_divisibility_identity(self):
        image = np.ones(shape=(8, 12, 3), dtype=np.float32)
        padded_image = pad_image_for_divisibility(image, 2)
        self.assertTupleEqual(image.shape, padded_image.shape)
        self.assertTrue(np.allclose(image, padded_image))


class TestImageResizeForDivisibility(unittest.TestCase):
    def test_stretch_divisibility(self):
        image = np.ones(shape=(7, 9, 4), dtype=np.float32)
        padded_image = stretch_image_for_divisibility(image, 2)
        expected_image = np.ones(shape=(8, 12, 4), dtype=np.float32)
        self.assertTupleEqual(expected_image.shape, padded_image.shape)
        self.assertTrue(np.allclose(expected_image, padded_image))

    def test_stretch_nearest_neighbour(self):
        image = np.ones(shape=(7, 9, 4), dtype=np.float32)
        padded_image = stretch_image_for_divisibility(image, 2, nearest_neighbour=True)
        padded_image[0:3, :, :] = 0.0
        expected_image = np.ones(shape=(8, 12, 4), dtype=np.float32)
        expected_image[0:3, :, :] = 0.0
        self.assertTupleEqual(expected_image.shape, padded_image.shape)
        self.assertTrue(np.allclose(expected_image, padded_image))

    def test_stretch_divisibility_single_channel(self):
        image = np.ones(shape=(7, 9, 1), dtype=np.float32)
        padded_image = stretch_image_for_divisibility(image, 2)
        expected_image = np.ones(shape=(8, 12, 1), dtype=np.float32)
        self.assertTupleEqual(expected_image.shape, padded_image.shape)
        self.assertTrue(np.allclose(expected_image, padded_image))

    def test_stretch_divisibility_height_only(self):
        image = np.ones(shape=(436, 1024, 3), dtype=np.float32)
        padded_image = stretch_image_for_divisibility(image, 6)
        expected_image = np.ones(shape=(448, 1024, 3), dtype=np.float32)
        self.assertTupleEqual(expected_image.shape, padded_image.shape)
        self.assertTrue(np.allclose(expected_image, padded_image))

    def test_stretch_divisibility_width_only(self):
        image = np.ones(shape=(1024, 436, 2), dtype=np.float32)
        padded_image = stretch_image_for_divisibility(image, 6)
        expected_image = np.ones(shape=(1024, 448, 2), dtype=np.float32)
        self.assertTupleEqual(expected_image.shape, padded_image.shape)
        self.assertTrue(np.allclose(expected_image, padded_image))

    def test_stretch_divisibility_three_times(self):
        image = np.ones(shape=(436, 436, 3), dtype=np.float32)
        padded_image = stretch_image_for_divisibility(image, 3)
        expected_image = np.ones(shape=(440, 440, 3), dtype=np.float32)
        self.assertTupleEqual(expected_image.shape, padded_image.shape)
        self.assertTrue(np.allclose(expected_image, padded_image))

    def test_stretch_divisibility_identity(self):
        image = np.ones(shape=(8, 12, 3), dtype=np.float32)
        padded_image = stretch_image_for_divisibility(image, 2)
        self.assertTupleEqual(image.shape, padded_image.shape)
        self.assertTrue(np.allclose(image, padded_image))


class TestCenteredCrop(unittest.TestCase):
    def test_centered_crop(self):
        image = np.ones(shape=(7, 9, 3), dtype=np.float32)
        padded_image = cv2.copyMakeBorder(image, 10, 10, 10, 10, cv2.BORDER_CONSTANT, value=[0, 0, 0])
        padded_image = np.reshape(padded_image, (27, 29, 3))
        cropped_image = centered_crop(padded_image, 7, 9)
        self.assertTupleEqual(image.shape, cropped_image.shape)
        self.assertTrue(np.allclose(image, cropped_image))

    def test_centered_crop_identity(self):
        image = np.ones(shape=(7, 9, 3), dtype=np.float32)
        cropped_image = centered_crop(image, 7, 9)
        self.assertTupleEqual(image.shape, cropped_image.shape)
        self.assertTrue(np.allclose(image, cropped_image))

    def test_centered_crop_even(self):
        image = np.zeros(shape=(10, 12, 2), dtype=np.float32)
        image[2:8, 3:9, ...] = 1.0
        cropped_image = centered_crop(image, 6, 6)
        self.assertTupleEqual((6, 6, 2), cropped_image.shape)
        self.assertEqual(6 * 6 * 2, np.sum(cropped_image))

    def test_centered_crop_odd(self):
        image = np.zeros(shape=(10, 12, 3), dtype=np.float32)
        image[2:7, 3:8, ...] = 1.0
        cropped_image = centered_crop(image, 5, 5)
        self.assertTupleEqual((5, 5, 3), cropped_image.shape)
        self.assertEqual(5 * 5 * 3, np.sum(cropped_image))


class TestCropAroundPoint(unittest.TestCase):
    def test_correctly_bounded_even_crop(self):
        image = np.zeros(shape=(10, 12, 3), dtype=np.float32)
        image[2:6, 1:7] = 1.0
        cropepd_image = crop_around_point(image, 4, 4, 4, 6)
        self.assertTupleEqual((4, 6, 3), cropepd_image.shape)
        self.assertEqual(4 * 6 * 3, np.sum(cropepd_image))

    def test_correctly_bounded_odd_crop(self):
        image = np.zeros(shape=(10, 12, 3), dtype=np.float32)
        image[2:7, 2:7] = 1.0
        cropepd_image = crop_around_point(image, 4, 4, 5, 5)
        self.assertTupleEqual((5, 5, 3), cropepd_image.shape)
        self.assertEqual(5 * 5 * 3, np.sum(cropepd_image))

    def test_incorrectly_bounded_even_crop(self):
        image = np.zeros(shape=(10, 12, 3), dtype=np.float32)
        image[6:10, :6] = 1.0
        cropepd_image = crop_around_point(image, 1, 11, 4, 6)
        self.assertTupleEqual((4, 6, 3), cropepd_image.shape)
        self.assertEqual(4 * 6 * 3, np.sum(cropepd_image))

    def test_incorrectly_bounded_odd_crop(self):
        image = np.zeros(shape=(10, 12, 3), dtype=np.float32)
        image[0:5, 7:12] = 1.0
        cropepd_image = crop_around_point(image, 11, 1, 5, 5)
        self.assertTupleEqual((5, 5, 3), cropepd_image.shape)
        self.assertEqual(5 * 5 * 3, np.sum(cropepd_image))


if __name__ == '__main__':
    unittest.main()
