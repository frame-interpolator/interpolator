import unittest
from common.utils.misc import *


class MiscUtils(unittest.TestCase):
    def _test_eq(self, in_list, expected_list_indices):
        in_list_indices = [elm[0] for elm in in_list]
        self.assertListEqual(in_list_indices, expected_list_indices)

    def test_list_insert(self):
        l = sorted_list_insert(None, 0, None, max_len=None)
        self._test_eq(l, [0])

        l = sorted_list_insert(l, 2, None)
        l = sorted_list_insert(l, 10, None)
        self._test_eq(l, [10, 2, 0])

        l = sorted_list_insert(l, 1, None)
        self._test_eq(l, [10, 2, 1, 0])

        l = sorted_list_insert(l, -3, None)
        self._test_eq(l, [10, 2, 1, 0, -3])

        l = sorted_list_insert(l, 100, None, max_len=5)
        self._test_eq(l, [100, 10, 2, 1, 0])

        l = sorted_list_insert(l, -3, None, max_len=5)
        self._test_eq(l, [100, 10, 2, 1, 0])

        l = sorted_list_insert(l, 1000, None, max_len=5)
        self._test_eq(l, [1000, 100, 10, 2, 1])

    def test_get_cache_dir(self):
        dir = get_cache_dir()
        self.assertGreater(len(dir), 0)
        self.assertTrue('interpolator' in dir)


if __name__ == '__main__':
    unittest.main()
