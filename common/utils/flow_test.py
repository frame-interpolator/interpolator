import unittest
from common.utils.data import silently_remove_file
from common.utils.img import show_image
from common.utils.flow import *


SHOW_FLOW_TEST_IMAGES = False


class TestSintelFlowReader(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_flow_vis(self):
        extras = {}
        flow_image = read_flow_file('common/utils/test_data/frame_0001.flo', extras)
        self.assertIsNone(extras['valid'])
        self.assertTrue(flow_image is not None)

        expected_shape = (436, 1024, 2)
        self.assertTupleEqual((436, 1024, 2), flow_image.shape)
        if SHOW_FLOW_TEST_IMAGES:
            show_flow_image(flow_image)

        flow_image = read_flow_file('common/utils/test_data/frame_0011.flo')
        self.assertTrue(flow_image is not None)

        self.assertTupleEqual(expected_shape, flow_image.shape)
        if SHOW_FLOW_TEST_IMAGES:
            show_flow_image(flow_image)

    def test_default_valid(self):
        extras = {}
        flow_image = read_flow_file('common/utils/test_data/frame_0001.flo', extras, use_default_valid=True)
        self.assertIsNotNone(extras['valid'])
        self.assertTrue(flow_image is not None)
        H, W = flow_image.shape[0], flow_image.shape[1]
        self.assertTupleEqual((H, W, 1), extras['valid'].shape)
        np.testing.assert_allclose(extras['valid'], np.ones((H, W, 1), dtype=np.bool))

    def test_tensorflow_vis(self):
        """
        Tests the tensorflow implementation and the batching against the numpy version.
        """
        flow_image = read_flow_file('common/utils/test_data/frame_0001.flo')
        flow_image_2 = read_flow_file('common/utils/test_data/frame_0011.flo')
        flow_ph = tf.placeholder(shape=[None, flow_image.shape[0], flow_image.shape[1], flow_image.shape[2]],
                                 dtype=tf.float32)
        visualization_tensor = get_tf_flow_visualization(flow_ph)
        feed_dict = {
            flow_ph: np.stack([flow_image, flow_image_2], axis=0)
        }
        visualization = self.sess.run(visualization_tensor, feed_dict=feed_dict)

        target_visualization = get_flow_visualization(flow_image) / 255.0
        target_visualization_2 = get_flow_visualization(flow_image_2) / 255.0
        self.assertTrue(np.allclose(target_visualization, visualization[0], atol=0.06))
        self.assertTrue(np.allclose(target_visualization_2, visualization[1], atol=0.06))

        if SHOW_FLOW_TEST_IMAGES:
            show_image(visualization[0])
            show_image(visualization[1])


class TestPFMFlowReader(unittest.TestCase):
    def test_pfm_read(self):
        extras = {}
        flow_image = read_flow_file('common/utils/test_data/flow.pfm', extras)
        self.assertIsNone(extras['valid'])
        self.assertTrue(flow_image is not None)
        self.assertTupleEqual((540, 960, 2), flow_image.shape)
        if SHOW_FLOW_TEST_IMAGES:
            show_flow_image(flow_image)

    def test_default_valid(self):
        extras = {}
        flow_image = read_flow_file('common/utils/test_data/flow.pfm', extras, use_default_valid=True)
        self.assertIsNotNone(extras['valid'])
        self.assertTrue(flow_image is not None)
        H, W = flow_image.shape[0], flow_image.shape[1]
        self.assertTupleEqual((H, W, 1), extras['valid'].shape)
        np.testing.assert_allclose(extras['valid'], np.ones((H, W, 1), dtype=np.bool))


class TestPngFlowReader(unittest.TestCase):
    def test_pfm_read(self):
        extras = {}
        flow = read_flow_file('common/utils/test_data/kitti_flow.png', extras)
        self.assertTrue('valid' in extras)
        self.assertIsNotNone(flow)
        self.assertEqual(np.float32, flow.dtype)
        valid = extras['valid']
        self.assertEqual(np.bool, valid.dtype)
        self.assertTupleEqual((375, 1242, 2), flow.shape)
        self.assertTupleEqual((375, 1242, 1), valid.shape)
        valid = valid.astype(np.float32)
        self.assertAlmostEqual(0.0, np.min(valid))
        self.assertAlmostEqual(1.0, np.max(valid))
        self.assertAlmostEqual(0.0, float(np.sum(flow * (1.0 - valid))))
        if SHOW_FLOW_TEST_IMAGES:
            show_flow_image(flow)

    def test_default_valid(self):
        extras = {}
        flow_image = read_flow_file('common/utils/test_data/kitti_flow.png', extras, use_default_valid=True)
        self.assertIsNotNone(extras['valid'])
        self.assertTrue(flow_image is not None)
        H, W = flow_image.shape[0], flow_image.shape[1]
        self.assertTupleEqual((H, W, 1), extras['valid'].shape)
        sum = float(np.sum(extras['valid']))
        self.assertLess(sum, float(H * W))
        self.assertEqual(88975.0, sum)


class TestFlowWriter(unittest.TestCase):
    def setUp(self):
        self.out_file = 'common/utils/test_data/temp_flow_write_test_file.flo'

    def test_write(self):
        flow_image_expected = read_flow_file('common/utils/test_data/frame_0001.flo')
        write_flow_file(self.out_file, flow_image_expected)
        flow_image = read_flow_file(self.out_file)
        self.assertTrue(np.allclose(flow_image_expected, flow_image))
        if SHOW_FLOW_TEST_IMAGES:
            show_flow_image(flow_image)

    def tearDown(self):
        silently_remove_file(self.out_file)


class TestFlowResize(unittest.TestCase):
    def test_resize_up(self):
        input = np.ones((2, 2, 2))
        input[..., 0] *= -1
        expected = np.ones((4, 8, 2))
        expected[..., 0] *= -4
        expected[..., 1] *= 2
        resized = resize_flow(input, 4, 8)
        self.assertTrue(np.allclose(expected, resized))

    def test_resize_down(self):
        input = np.ones((2, 2, 2))
        input[..., 0] *= -1
        expected = np.ones((1, 1, 2))
        expected[..., 0] *= -0.5
        expected[..., 1] *= 0.5
        resized = resize_flow(input, 1, 1)
        self.assertTrue(np.allclose(expected, resized))

    def test_resize_mixed(self):
        input = np.ones((2, 2, 2))
        input[..., 0] *= -1
        expected = np.ones((1, 4, 2))
        expected[..., 0] *= -2
        expected[..., 1] *= 0.5
        resized = resize_flow(input, 1, 4)
        self.assertTrue(np.allclose(expected, resized))


class TestFlowResizeForDivisibility(unittest.TestCase):
    def test_stretch_divisibility(self):
        flow = np.ones(shape=(7, 9, 2), dtype=np.float32)
        stretched_flow = stretch_flow_for_divisibility(flow, 2)
        expected_flow = np.ones(shape=(8, 12, 2), dtype=np.float32)
        expected_flow[..., 0] *= 12 / 9
        expected_flow[..., 1] *= 8 / 7
        self.assertTupleEqual(expected_flow.shape, stretched_flow.shape)
        self.assertTrue(np.allclose(expected_flow, stretched_flow))

    def test_stretch_divisibility_nearest_neighbour(self):
        flow = np.ones(shape=(7, 9, 2), dtype=np.float32)
        flow[0:3, :, :] = 0.0
        stretched_flow = stretch_flow_for_divisibility(flow, 2, nearest_neighbour=True)
        expected_flow = np.ones(shape=(8, 12, 2), dtype=np.float32)
        expected_flow[0:4, :, :] = 0.0
        expected_flow[..., 0] *= 12 / 9
        expected_flow[..., 1] *= 8 / 7
        self.assertTupleEqual(expected_flow.shape, stretched_flow.shape)
        self.assertTrue(np.allclose(expected_flow, stretched_flow),
                        'expected:\n' + str(expected_flow) + '\ngot:\n' + str(stretched_flow))

    def test_stretch_divisibility_height_only(self):
        flow = np.ones(shape=(436, 1024, 2), dtype=np.float32)
        stretched_flow = stretch_flow_for_divisibility(flow, 6)
        expected_flow = np.ones(shape=(448, 1024, 2), dtype=np.float32)
        expected_flow[..., 1] *= 448 / 436
        self.assertTupleEqual(expected_flow.shape, stretched_flow.shape)
        self.assertTrue(np.allclose(expected_flow, stretched_flow))

    def test_stretch_divisibility_width_only(self):
        flow = np.ones(shape=(1024, 436, 2), dtype=np.float32)
        stretched_flow = stretch_flow_for_divisibility(flow, 6)
        expected_flow = np.ones(shape=(1024, 448, 2), dtype=np.float32)
        expected_flow[..., 0] *= 448 / 436
        self.assertTupleEqual(expected_flow.shape, stretched_flow.shape)
        self.assertTrue(np.allclose(expected_flow, stretched_flow))

    def test_stretch_divisibility_three_times(self):
        flow = np.ones(shape=(436, 436, 2), dtype=np.float32)
        stretched_flow = stretch_flow_for_divisibility(flow, 3)
        expected_flow = np.ones(shape=(440, 440, 2), dtype=np.float32)
        expected_flow[..., 0] *= 440 / 436
        expected_flow[..., 1] *= 440 / 436
        self.assertTupleEqual(expected_flow.shape, stretched_flow.shape)
        self.assertTrue(np.allclose(expected_flow, stretched_flow))

    def test_stretch_divisibility_identity(self):
        flow = np.ones(shape=(8, 12, 2), dtype=np.float32)
        stretched_flow = stretch_flow_for_divisibility(flow, 2)
        self.assertTupleEqual(flow.shape, stretched_flow.shape)
        self.assertTrue(np.allclose(flow, stretched_flow))


class TestFlowAndMaskResizeForDivibility(unittest.TestCase):
    def test_stretch_divisibility(self):
        flow = np.ones(shape=(7, 9, 2), dtype=np.float32)
        mask = np.ones(shape=(7, 9, 1), dtype=np.float32)
        stretched_flow, stretched_mask = stretch_flow_and_mask_for_divisibility(flow, mask, 2)
        expected_flow = np.ones(shape=(8, 12, 2), dtype=np.float32)
        expected_flow[..., 0] *= 12 / 9
        expected_flow[..., 1] *= 8 / 7
        expected_mask = np.ones(shape=(8, 12, 1), dtype=np.float32)
        self.assertTupleEqual(expected_flow.shape, stretched_flow.shape)
        self.assertTrue(np.allclose(expected_flow, stretched_flow),
                        'expected:\n' + str(expected_flow) + '\ngot:\n' + str(stretched_flow))
        self.assertTupleEqual(expected_mask.shape, stretched_mask.shape)
        self.assertTrue(np.allclose(expected_mask, stretched_mask))

    def test_stretch_divisibility_nearest_neighbour(self):
        flow = np.ones(shape=(7, 9, 2), dtype=np.float32)
        flow[0:3, :, :] = 0.0
        mask = np.ones(shape=(7, 9, 1), dtype=np.float32)
        mask[0:3, :, :] = 0.0
        stretched_flow, stretched_mask = stretch_flow_and_mask_for_divisibility(flow, mask, 2, nearest_neighbour=True)
        expected_flow = np.ones(shape=(8, 12, 2), dtype=np.float32)
        expected_flow[0:4, :, :] = 0.0
        expected_flow[..., 0] *= 12 / 9
        expected_flow[..., 1] *= 8 / 7
        expected_mask = np.ones(shape=(8, 12, 1), dtype=np.float32)
        expected_mask[0:4, :, :] = 0.0
        self.assertTupleEqual(expected_flow.shape, stretched_flow.shape)
        self.assertTrue(np.allclose(expected_flow, stretched_flow),
                        'expected:\n' + str(expected_flow) + '\ngot:\n' + str(stretched_flow))
        self.assertTupleEqual(expected_mask.shape, stretched_mask.shape)
        self.assertTrue(np.allclose(expected_mask, stretched_mask))


if __name__ == '__main__':
    unittest.main()
