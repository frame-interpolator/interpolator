import unittest
import cv2
import numpy as np
import os.path
from common.affine_transform.affine_transform import affine_transform
from common.utils.img import read_image, show_image, write_image
from common.utils.flow import read_flow_file, tf_transform_flow, tf_create_affine_transform, get_flow_visualization
from pwcnet.warp.warp import *


SHOW_AUGMENTATION_DEBUG_IMAGES = False


class TestFlowAugmentation(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        # Load from files.
        s = 0.5  # Scale.
        self.flow_ab = cv2.resize(read_flow_file('common/utils/test_data/flow_ab.flo'), (0, 0), fx=s, fy=s) * s
        self.img_a = cv2.resize(read_image('common/utils/test_data/image_a.png', as_float=True), (0, 0), fx=s, fy=s)
        self.img_b = cv2.resize(read_image('common/utils/test_data/image_b.png', as_float=True), (0, 0), fx=s, fy=s)
        self.flow_cd = cv2.resize(read_flow_file('common/utils/test_data/flow_cd.flo'), (0, 0), fx=s, fy=s) * s
        self.img_c = cv2.resize(read_image('common/utils/test_data/image_c.png', as_float=True), (0, 0), fx=s, fy=s)
        self.img_d = cv2.resize(read_image('common/utils/test_data/image_d.png', as_float=True), (0, 0), fx=s, fy=s)
        self.tf_true = tf.constant(True)
        self.tf_false = tf.constant(False)

    def test_zoom(self):
        flow_ab, img_a, img_b = self.scale_immediate(self.flow_ab, self.img_a, self.img_b, 1.0, 2.0)
        flow_cd, img_c, img_d = self.scale_immediate(self.flow_cd, self.img_c, self.img_d, 1.0, 2.0)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d, max_error=0.012)

    def test_scale_vertical(self):
        flow_ab, img_a, img_b = self.scale_immediate(self.flow_ab, self.img_a, self.img_b, 1.4)
        flow_cd, img_c, img_d = self.scale_immediate(self.flow_cd, self.img_c, self.img_d, 1.4)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d, max_error=0.012)

    def test_scale_horizontal(self):
        flow_ab, img_a, img_b = self.scale_immediate(self.flow_ab, self.img_a, self.img_b, 0.6)
        flow_cd, img_c, img_d = self.scale_immediate(self.flow_cd, self.img_c, self.img_d, 0.6)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d, max_error=0.012)

    def test_scale_vertical_and_zoom(self):
        flow_ab, img_a, img_b = self.scale_immediate(self.flow_ab, self.img_a, self.img_b, 1.4, 2.0)
        flow_cd, img_c, img_d = self.scale_immediate(self.flow_cd, self.img_c, self.img_d, 1.4, 2.0)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d, max_error=0.011)

    def test_scale_horizontal_and_zoom(self):
        flow_ab, img_a, img_b = self.scale_immediate(self.flow_ab, self.img_a, self.img_b, 0.6, 2.0)
        flow_cd, img_c, img_d = self.scale_immediate(self.flow_cd, self.img_c, self.img_d, 0.6, 2.0)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d, max_error=0.013)

    def test_no_flip(self):
        flow_ab, img_a, img_b = self.flip_immediate(self.flow_ab, self.img_a, self.img_b, None, None)
        self.assertTrue(np.allclose(flow_ab, self.flow_ab))
        self.assertTrue(np.allclose(img_a, self.img_a))
        self.assertTrue(np.allclose(img_b, self.img_b))

    def test_flip_horizontal(self):
        flow_ab, img_a, img_b = self.flip_immediate(self.flow_ab, self.img_a, self.img_b, self.tf_true, self.tf_false)
        flow_cd, img_c, img_d = self.flip_immediate(self.flow_cd, self.img_c, self.img_d, self.tf_true, self.tf_false)
        self.assertFalse(np.allclose(flow_ab, self.flow_ab))
        self.assertFalse(np.allclose(img_a, self.img_a))
        self.assertFalse(np.allclose(img_b, self.img_b))
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d)

    def test_flip_vertical(self):
        flow_ab, img_a, img_b = self.flip_immediate(self.flow_ab, self.img_a, self.img_b, self.tf_false, self.tf_true)
        flow_cd, img_c, img_d = self.flip_immediate(self.flow_cd, self.img_c, self.img_d, self.tf_false, self.tf_true)
        self.assertFalse(np.allclose(flow_ab, self.flow_ab))
        self.assertFalse(np.allclose(img_a, self.img_a))
        self.assertFalse(np.allclose(img_b, self.img_b))
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d)

    def test_flip_both(self):
        flow_ab, img_a, img_b = self.flip_immediate(self.flow_ab, self.img_a, self.img_b, self.tf_true, self.tf_true)
        flow_cd, img_c, img_d = self.flip_immediate(self.flow_cd, self.img_c, self.img_d, self.tf_true, self.tf_true)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d)

    def test_scale_and_flip(self):
        flow_ab, img_a, img_b = self.transform_immediate(self.flow_ab, self.img_a, self.img_b,
                                                         flip_left_right=self.tf_true, flip_up_down=self.tf_true,
                                                         scale=1.4)
        flow_cd, img_c, img_d = self.transform_immediate(self.flow_cd, self.img_c, self.img_d,
                                                         flip_left_right=self.tf_true, flip_up_down=self.tf_true,
                                                         scale=1.4)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d)

    def test_all_transforms(self):
        flow_ab, img_a, img_b = self.transform_immediate(self.flow_ab, self.img_a, self.img_b,
                                                         flip_left_right=self.tf_true, flip_up_down=self.tf_false,
                                                         scale=1.8, rotation=20, zoom=1.5)
        flow_cd, img_c, img_d = self.transform_immediate(self.flow_cd, self.img_c, self.img_d,
                                                         flip_left_right=self.tf_true, flip_up_down=self.tf_false,
                                                         scale=1.8, rotation=20, zoom=1.5)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d, max_error=0.012)

    def test_rotate_none(self):
        flow_ab, img_a, img_b = self.rotate_immediate(self.flow_ab, self.img_a, self.img_b, 0.0)
        flow_cd, img_c, img_d = self.rotate_immediate(self.flow_cd, self.img_c, self.img_d, 0.0)
        self.assertTrue(np.allclose(img_a, self.img_a, atol=1e-4))
        self.assertTrue(np.allclose(img_b, self.img_b, atol=1e-4))
        self.assertTrue(np.allclose(img_c, self.img_c, atol=1e-4))
        self.assertTrue(np.allclose(img_d, self.img_d, atol=1e-4))
        self.assertTrue(np.allclose(flow_ab, self.flow_ab, atol=1e-4))
        self.assertTrue(np.allclose(flow_cd, self.flow_cd, atol=1e-4))

    def test_rotate_positive(self):
        flow_ab, img_a, img_b = self.rotate_immediate(self.flow_ab, self.img_a, self.img_b, 25.0)
        flow_cd, img_c, img_d = self.rotate_immediate(self.flow_cd, self.img_c, self.img_d, 25.0)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d, max_error=0.013)

    def test_rotate_negative(self):
        flow_ab, img_a, img_b = self.rotate_immediate(self.flow_ab, self.img_a, self.img_b, -25.0)
        flow_cd, img_c, img_d = self.rotate_immediate(self.flow_cd, self.img_c, self.img_d, -25.0)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d, max_error=0.011)

    def test_rotate_back_and_forth(self):
        flow_ab, img_a, img_b = self.rotate_immediate(self.flow_ab, self.img_a, self.img_b, -25.0)
        flow_cd, img_c, img_d = self.rotate_immediate(self.flow_cd, self.img_c, self.img_d, -25.0)
        flow_ab, img_a, img_b = self.rotate_immediate(flow_ab, img_a, img_b, 25.0)
        flow_cd, img_c, img_d = self.rotate_immediate(flow_cd, img_c, img_d, 25.0)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d, max_error=0.011)

    def test_transform_with_diff(self):
        flow_ab, img_a, img_b = self.transform_immediate(self.flow_ab, self.img_a, self.img_b,
                                                         flip_left_right=self.tf_true, flip_up_down=self.tf_false,
                                                         scale=1.3, rotation=20, zoom=1.2,
                                                         zoom_diff=1.2, rotation_diff=10, scale_diff=1.2,
                                                         translate_x_diff=50, translate_y_diff=-50,
                                                         use_diff=tf.constant(0.8) > 0.5)
        flow_cd, img_c, img_d = self.transform_immediate(self.flow_cd, self.img_c, self.img_d,
                                                         flip_left_right=self.tf_true, flip_up_down=self.tf_false,
                                                         scale=1.3, rotation=20, zoom=1.2,
                                                         zoom_diff=1.2, rotation_diff=10, scale_diff=1.2,
                                                         translate_x_diff=50, translate_y_diff=-50)
        self.run_case(flow_ab, img_a, img_b, flow_cd, img_c, img_d, max_error=0.029)

    def scale_immediate(self, flow_ab, img_a, img_b, scale, zoom=None):
        img_a_ph = tf.placeholder(shape=img_a.shape, dtype=tf.float32)
        img_b_ph = tf.placeholder(shape=img_b.shape, dtype=tf.float32)
        flowab_ph = tf.placeholder(shape=flow_ab.shape, dtype=tf.float32)
        flow_scaled, images_scaled, trans = tf_transform_flow(flowab_ph, [img_a_ph, img_b_ph], scale=scale, zoom=zoom)
        self.assertEqual(2, len(trans))
        img_a_scaled = images_scaled[0]
        img_b_scaled = images_scaled[1]
        feed_dict = {
            img_a_ph: img_a,
            img_b_ph: img_b,
            flowab_ph: flow_ab
        }
        return self.sess.run([flow_scaled, img_a_scaled, img_b_scaled], feed_dict=feed_dict)

    def flip_immediate(self, flow_ab, img_a, img_b, flip_left_right, flip_up_down):
        img_a_ph = tf.placeholder(shape=img_a.shape, dtype=tf.float32)
        img_b_ph = tf.placeholder(shape=img_b.shape, dtype=tf.float32)
        flowab_ph = tf.placeholder(shape=flow_ab.shape, dtype=tf.float32)
        flow_flipped, images_flipped, trans = tf_transform_flow(flowab_ph, [img_a_ph, img_b_ph],
                                                                flip_horizontal=flip_left_right,
                                                                flip_vertical=flip_up_down)
        self.assertEqual(2, len(trans))
        img_a_flipped = images_flipped[0]
        img_b_flipped = images_flipped[1]
        feed_dict = {
            img_a_ph: img_a,
            img_b_ph: img_b,
            flowab_ph: flow_ab
        }
        return self.sess.run([flow_flipped, img_a_flipped, img_b_flipped], feed_dict=feed_dict)

    def rotate_immediate(self, flow_ab, img_a, img_b, rotation):
        img_a_ph = tf.placeholder(shape=img_a.shape, dtype=tf.float32)
        img_b_ph = tf.placeholder(shape=img_b.shape, dtype=tf.float32)
        flowab_ph = tf.placeholder(shape=flow_ab.shape, dtype=tf.float32)
        flow_rotated, images_rotated, trans = tf_transform_flow(flowab_ph, [img_a_ph, img_b_ph], rotation=rotation)
        self.assertEqual(2, len(trans))
        img_a_rotated = images_rotated[0]
        img_b_rotated = images_rotated[1]
        feed_dict = {
            img_a_ph: img_a,
            img_b_ph: img_b,
            flowab_ph: flow_ab
        }
        return self.sess.run([flow_rotated, img_a_rotated, img_b_rotated], feed_dict=feed_dict)

    def transform_immediate(self, flow_ab, img_a, img_b, flip_left_right=None, flip_up_down=None,
                            scale=None, zoom=None, rotation=None, zoom_diff=None, scale_diff=None,
                            translate_x_diff=None, translate_y_diff=None, rotation_diff=None, use_diff=None):
        img_a_ph = tf.placeholder(shape=img_a.shape, dtype=tf.float32)
        img_b_ph = tf.placeholder(shape=img_b.shape, dtype=tf.float32)
        flowab_ph = tf.placeholder(shape=flow_ab.shape, dtype=tf.float32)
        flow_transformed, images_transformed, trans = tf_transform_flow(flowab_ph, [img_a_ph, img_b_ph],
                                                                        rotation=rotation,
                                                                        flip_horizontal=flip_left_right,
                                                                        flip_vertical=flip_up_down, zoom=zoom,
                                                                        scale=scale,
                                                                        zoom_diff=zoom_diff, scale_diff=scale_diff,
                                                                        translate_x_diff=translate_x_diff,
                                                                        translate_y_diff=translate_y_diff,
                                                                        rotation_diff=rotation_diff, use_diff=use_diff)
        self.assertEqual(2, len(trans))
        # Sanity check the transforms.
        img_a_trans_check = affine_transform(img_a_ph, trans[0])
        img_b_trans_check = affine_transform(img_b_ph, trans[1])
        img_a_transformed = images_transformed[0]
        img_b_transformed = images_transformed[1]
        feed_dict = {
            img_a_ph: img_a,
            img_b_ph: img_b,
            flowab_ph: flow_ab
        }
        query = [flow_transformed, img_a_transformed, img_b_transformed, img_a_trans_check, img_b_trans_check]
        results = self.sess.run(query, feed_dict=feed_dict)
        flow_transformed, img_a_transformed, img_b_transformed, img_a_trans_check, img_b_trans_check = results
        self.assertTrue(np.allclose(img_a_transformed, img_a_trans_check))
        self.assertTrue(np.allclose(img_b_transformed, img_b_trans_check))
        return flow_transformed, img_a_transformed, img_b_transformed

    def run_case(self, flow_ab, img_a, img_b, flow_cd, img_c, img_d, save_outputs=False, max_error=0.013):
        """
        Helper test method.
        """
        mask_ab = np.ones(shape=img_a.shape)
        mask_cd = np.ones(shape=img_a.shape)

        H = img_a.shape[0]
        W = img_a.shape[1]
        C = img_a.shape[2]

        # Create the graph.
        img_shape = [None, H, W, C]
        flow_shape = [None, H, W, 2]
        input = tf.placeholder(shape=img_shape, dtype=tf.float32)
        flow_tensor = tf.placeholder(shape=flow_shape, dtype=tf.float32)
        warped_tensor = backward_warp(input, flow_tensor)

        # Run.
        feed_dict = {
            input: [img_b, img_d, mask_ab, mask_cd],
            flow_tensor: [flow_ab, flow_cd, flow_ab, flow_cd]
        }
        warped_image = self.sess.run(warped_tensor, feed_dict=feed_dict)
        # Get the masked errors.
        error_ab_img = np.abs(warped_image[0] - img_a) * warped_image[2]
        error_cd_img = np.abs(warped_image[1] - img_c) * warped_image[3]
        error_ab = np.mean(error_ab_img)
        error_cd = np.mean(error_cd_img)
        # Assert a < 1.3% (default) average error.
        self.assertLess(error_ab, max_error)
        self.assertLess(error_cd, max_error)

        if SHOW_AUGMENTATION_DEBUG_IMAGES:
            show_image(warped_image[0])
            show_image(warped_image[1])
            show_image(error_ab_img)
            show_image(error_cd_img)

        if save_outputs:
            os.makedirs('outputs', exist_ok=True)
            write_image(os.path.join('outputs', 'flow_ab.png'), get_flow_visualization(flow_ab))
            write_image(os.path.join('outputs', 'flow_cd.png'), get_flow_visualization(flow_cd))
            write_image(os.path.join('outputs', 'image_a.png'), img_a)
            write_image(os.path.join('outputs', 'image_b.png'), img_b)
            write_image(os.path.join('outputs', 'image_c.png'), img_c)
            write_image(os.path.join('outputs', 'image_d.png'), img_d)
            write_image(os.path.join('outputs', 'warped_ab.png'), warped_image[0])
            write_image(os.path.join('outputs', 'warped_cd.png'), warped_image[1])
            write_image(os.path.join('outputs', 'error_ab.png'), error_ab_img)
            write_image(os.path.join('outputs', 'error_cd.png'), error_cd_img)


class TestCreateAffineTransform(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_identity(self):
        image = np.zeros((8, 8, 3), dtype=np.float32)
        transform = tf_create_affine_transform(image)
        transform = self.sess.run(transform)
        self.assertTrue(np.allclose(np.asarray([[1.0, 0.0, 0.0],
                                                [0.0, 1.0, 0.0],
                                                [0.0, 0.0, 1.0]]), transform))

    def test_translation(self):
        image = np.zeros((8, 8, 3), dtype=np.float32)
        transform = tf_create_affine_transform(image, translate_x=3.0, translate_y=tf.constant(6.0, dtype=tf.float32))
        transform = self.sess.run(transform)
        self.assertTrue(np.allclose(np.asarray([[1.0, 0.0, 3.0],
                                                [0.0, 1.0, 6.0],
                                                [0.0, 0.0, 1.0]]), transform))

    def test_rotation_ccw(self):
        image = np.zeros((8, 8, 3), dtype=np.float32)
        transform = tf_create_affine_transform(image, rotation=tf.constant(90.0, dtype=tf.float32))
        transform = self.sess.run(transform)
        self.assertTrue(np.allclose(np.asarray([[0.0, 1.0, 0.0],
                                                [-1.0, 0.0, 7.0],
                                                [0.0, 0.0, 1.0]]), transform, atol=1e-5))

    def test_rotation_cw(self):
        image = np.zeros((8, 8, 3), dtype=np.float32)
        transform = tf_create_affine_transform(image, rotation=tf.constant(-90.0, dtype=tf.float32))
        transform = self.sess.run(transform)
        self.assertTrue(np.allclose(np.asarray([[0.0, -1.0, 7.0],
                                                [1.0, 0.0, 0.0],
                                                [0.0, 0.0, 1.0]]), transform, atol=1e-5))

    def test_scale_horizontal_and_zoom(self):
        image = np.zeros((8, 8, 3), dtype=np.float32)
        transform = tf_create_affine_transform(image, scale=0.5, zoom=0.5)
        transform = self.sess.run(transform)
        self.assertTrue(np.allclose(np.asarray([[1.0, 0.0, 0.0],
                                                [0.0, 0.5, 1.75],
                                                [0.0, 0.0, 1.0]]), transform, atol=1e-5))

    def test_scale_vertical_and_zoom(self):
        image = np.zeros((8, 8, 3), dtype=np.float32)
        transform = tf_create_affine_transform(image, scale=2.0, zoom=tf.constant(2.0, dtype=tf.float32))
        transform = self.sess.run(transform)
        self.assertTrue(np.allclose(np.asarray([[2.0, 0.0, -3.5],
                                                [0.0, 4.0, -10.5],
                                                [0.0, 0.0, 1.0]]), transform, atol=1e-5))

    def test_scale_vertical(self):
        image = np.zeros((8, 8, 3), dtype=np.float32)
        transform = tf_create_affine_transform(image, scale=tf.constant(2.0, dtype=tf.float32))
        transform = self.sess.run(transform)
        self.assertTrue(np.allclose(np.asarray([[1.0, 0.0, 0.0],
                                                [0.0, 2.0, -3.5],
                                                [0.0, 0.0, 1.0]]), transform, atol=1e-5))

    def test_flip_vertical(self):
        image = np.zeros((7, 8, 3), dtype=np.float32)
        transform = tf_create_affine_transform(image, flip_vertical=tf.constant(True))
        transform = self.sess.run(transform)
        self.assertTrue(np.allclose(np.asarray([[1.0, 0.0, 0.0],
                                                [0.0, -1.0, 6.0],
                                                [0.0, 0.0, 1.0]]), transform))

    def test_flip_horizontal(self):
        image = np.zeros((7, 8, 3), dtype=np.float32)
        transform = tf_create_affine_transform(image, flip_horizontal=tf.constant(True))
        transform = self.sess.run(transform)
        self.assertTrue(np.allclose(np.asarray([[-1.0, 0.0, 7.0],
                                                [0.0, 1.0, 0.0],
                                                [0.0, 0.0, 1.0]]), transform))

    def test_rotation_zoom(self):
        image = np.zeros((8, 8, 3), dtype=np.float32)
        transform1 = tf_create_affine_transform(image, rotation=tf.constant(23.0, dtype=tf.float32))
        transform2 = tf_create_affine_transform(image, rotation=tf.constant(-23.0, dtype=tf.float32))
        transform1, transform2 = self.sess.run([transform1, transform2])
        self.assertTrue(np.allclose(np.asarray([[1.2069991, 0.51234066, -2.5176892],
                                                [-0.51234066, 1.2069991, 1.0686958],
                                                [0.0, 0.0, 1.0]]), transform1, atol=1e-5))
        self.assertTrue(np.allclose(np.asarray([[1.2069991, -0.51234066, 1.0686958],
                                                [0.51234066, 1.2069991, -2.5176892],
                                                [0.0, 0.0, 1.0]]), transform2, atol=1e-5))

    def test_small_rotation_zoom(self):
        image = np.zeros((8, 8, 3), dtype=np.float32)
        transform1 = tf_create_affine_transform(image, rotation=tf.constant(2.0, dtype=tf.float32))
        transform2 = tf_create_affine_transform(image, rotation=tf.constant(-2.0, dtype=tf.float32))
        transform1, transform2 = self.sess.run([transform1, transform2])
        self.assertTrue(np.allclose(np.asarray([[1.0336603, 0.03609621, -0.24414784],
                                                [-0.03609621, 1.0336603, 0.00852573],
                                                [0.0, 0.0, 1.0]]), transform1, atol=1e-5))
        self.assertTrue(np.allclose(np.asarray([[1.0336603, -0.03609621, 0.00852573],
                                                [0.03609621, 1.0336603, -0.24414784],
                                                [0.0, 0.0, 1.0]]), transform2, atol=1e-5))

    def test_rotation_zoom_different_width(self):
        image = np.zeros((8, 10, 3), dtype=np.float32)
        transform1 = tf_create_affine_transform(image, rotation=tf.constant(23.0, dtype=tf.float32))
        transform2 = tf_create_affine_transform(image, rotation=tf.constant(-23.0, dtype=tf.float32))
        transform1, transform2 = self.sess.run([transform1, transform2])
        self.assertTrue(np.allclose(np.asarray([[1.2969166, 0.5505084, -3.262904],
                                                [-0.5505084, 1.2969166, 1.4380798],
                                                [0.0, 0.0, 1.0]]), transform1, atol=1e-5))
        self.assertTrue(np.allclose(np.asarray([[1.2969166, -0.5505084, 0.5906551],
                                                [0.5505084, 1.2969166, -3.5164955],
                                                [0.0, 0.0, 1.0]]), transform2, atol=1e-5))


if __name__ == '__main__':
    unittest.main()
