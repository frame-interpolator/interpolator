import cv2
import readline
import glob
import os
import numpy as np
from skimage.measure import compare_ssim, compare_psnr
import threading
import tensorflow as tf
from common.utils import img, flow
from common.utils.lpips import lpips
from common.vector_max_pool.vector_max_pool import vector_max_pool
from interp.flow_spline.flow_spline import QuadraticMotionLayer, quadratic_bezier
from pathlib import Path
from pwcnet.hinted_model import HintedPWCNet
from pwcnet.model import PWCNet


def get_full_flows(img_0, img_1, flow_model, pad_res):
    """
    :param img_0: Numpy array with shape [H, W, 2].
    :param img_1: Numpy array with same shape as img_0.
    :param flow_model: Loaded flow saved model.
    :param pad_res: Int. Number of times the resolution needs to be divisible by 2.
    :return: flow_0_1: The full flow from img_0 to img_1. With shape [H, W, 2].
             flow_1_0: The full flow from img_1 to img_0. With shape [H, W, 2].
    """
    # Pad images as we need to downsample a number of times.
    img_0_resized = img.stretch_image_for_divisibility(img_0, pad_res)
    img_1_resized = img.stretch_image_for_divisibility(img_1, pad_res)

    # Interpolate.
    flow_0_1 = flow_model.run(([img_0_resized], [img_1_resized]))[0][0]
    flow_1_0 = flow_model.run(([img_1_resized], [img_0_resized]))[0][0]

    # Resize the flows back to their original sizes.
    h, w, _ = img_0.shape
    flow_0_1 = flow.resize_flow(flow_0_1, h, w)
    flow_1_0 = flow.resize_flow(flow_1_0, h, w)
    return flow_0_1, flow_1_0


def config_readline():
    # Additional readline functionality.
    # See: https://stackoverflow.com/questions/6656819/filepath-autocompletion-using-users-input
    # Also see: https://docs.python.org/3/library/os.path.html
    # Unfortunately, this does not work with slurm jobs :(.
    def complete(text, state):
        text = os.path.expanduser(text)
        text = os.path.expandvars(text)
        text = os.path.normpath(text)
        return (glob.glob(text + '*') + [None])[state]

    readline.set_completer_delims(' \t\n;')
    readline.parse_and_bind("tab: complete")
    readline.set_completer(complete)


def load_saved_models(synthesizer_model_dir, flow_model_dir=None, hinted_flow_model_dir=None):
    # Check synthesizer model path.
    if not os.path.exists(synthesizer_model_dir):
        raise NotADirectoryError('Specified synthesis model directory ' + synthesizer_model_dir +
                                 ' is not a directory')

    # Resolve flow model path.
    if flow_model_dir is None:
        # Find the sibling model that was saved with the synthesizer.
        parent_dir = str(Path(synthesizer_model_dir).parent)
        flow_model_dir = os.path.join(parent_dir, 'flow')
        if not os.path.exists(flow_model_dir):
            raise NotADirectoryError('Cannot find the default corresponding flow estimator model.')
    elif not os.path.exists(flow_model_dir):
        raise NotADirectoryError('Specified flow model directory ' + flow_model_dir + ' is not a directory.')

    # Imports take a while (> 5 seconds), so we want to save them for after preliminary checks.
    print('Importing necessary TensorFlow modules and ops...', flush=True)
    from pwcnet.downsample.downsample import downsample
    from pwcnet.model import PWCNet
    from interp.interp_saved_model.synth_saved_model import SynthSavedModel
    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True

    # Load models for inference and return them.
    print('Loading saved models...', flush=True)
    synthesizer_model = SynthSavedModel(synthesizer_model_dir)
    synthesizer_model.load(session_config_proto=config_proto)
    flow_model = PWCNet.create_frozen_model(flow_model_dir)
    flow_model.load(session_config_proto=config_proto)
    if hinted_flow_model_dir is None:
        return synthesizer_model, flow_model
    # Load hinted model.
    hinted_flow_model = HintedPWCNet.create_frozen_model(hinted_flow_model_dir)
    hinted_flow_model.load(session_config_proto=config_proto)
    return synthesizer_model, flow_model, hinted_flow_model


def assert_rgb_pair(img_a, img_b):
    """
    Asserts that two Numpy arrays have the same dimensions and are RGB images.
    :param img_a: Numpy array of shape [H, W, 3].
    :param img_b: Numpy array with same shape as pred.
    """
    assert len(img_a.shape) == 3
    assert img_a.shape[2] == 3
    assert img_b.shape == img_a.shape


class InterpEvaluator:
    def __init__(self, use_lpips=False, sess=None):
        """
        :param use_lpips: Whether to include LPIPS in evaluations (this uses a neural network and is slower).
        :param sess: A Tf Session.
        """
        self.sess = sess
        if self.sess is None:
            config_proto = tf.ConfigProto()
            config_proto.gpu_options.allow_growth = True
            self.sess = tf.Session(config=config_proto)

        self.use_lpips = use_lpips
        if self.use_lpips:
            self.lpips_im_a_tensor = tf.placeholder(tf.float32, shape=[1, None, None, 3])
            self.lpips_im_b_tensor = tf.placeholder(tf.float32, shape=[1, None, None, 3])
            self.lpips_dist_tensor = lpips(self.lpips_im_a_tensor, self.lpips_im_b_tensor)

    def get_eval_metrics(self, pred, gt):
        """
        :param pred: Numpy array of shape [H, W, 3].
        :param gt: Numpy array with same shape as pred.
        :return: PSNR, SSIM, LPIPS (this is left as None if use_lpips was False).
        """
        assert_rgb_pair(pred, gt)
        lpips_dist = None
        if self.use_lpips:
            lpips_dist = self.sess.run(self.lpips_dist_tensor, feed_dict={
                self.lpips_im_a_tensor: [gt],
                self.lpips_im_b_tensor: [pred]
            })[0]
        ssim = compare_ssim(gt, pred, multichannel=True)
        psnr = compare_psnr(gt, pred)
        return psnr, ssim, lpips_dist


class Interpolator:
    def __init__(self, synthesizer_model, flow_model, sess=None):
        """
        :param synthesizer_model: Loaded synthesizer model.
        :param flow_model: Loaded flow model.
        :param sess: A Tf Session.
        """
        self.use_spline = False
        self.sess = sess
        if self.sess is None:
            config_proto = tf.ConfigProto()
            config_proto.gpu_options.allow_growth = True
            self.sess = tf.Session(config=config_proto)

        self.synthesizer_model = synthesizer_model
        self.flow_model = flow_model

        # Numpy arrays.
        self.image_0 = None
        self.image_1 = None
        self.flow_0_1 = None
        self.flow_1_0 = None
        self.image_0_padded = None
        self.image_1_padded = None
        self.flow_0_1_padded = None
        self.flow_1_0_padded = None
        self.flow_midpoints_0_1 = None
        self.flow_midpoints_1_0 = None

        # Create spline tensors.
        with tf.name_scope('interp_eval_spline'):
            self.t_placeholder = tf.placeholder(dtype=tf.float32, shape=[None])
            self.flow_placeholder = tf.placeholder(dtype=tf.float32, shape=[None, None, None, 2])
            self.flow_midpoints_placeholder = tf.placeholder(dtype=tf.float32, shape=[None, None, None, 2])
            self.flow_midpoints_tensor = QuadraticMotionLayer(self.flow_placeholder).mid_points
            self.splined_flow = quadratic_bezier(self.flow_placeholder, self.flow_midpoints_placeholder,
                                                 self.t_placeholder)

    def set_end_frames_and_compute_flow(self, image_0, image_1, use_spline=False):
        """
        Sets the frames at t = 0 and t = 1 and computes the optical flows between them.
        :param image_0: Numpy array of shape [H, W, 3].
        :param image_1: Numpy array with same shape as image_0.
        :param use_spline: Whether to compute the intermediate flows using a spline fit.
        """
        assert_rgb_pair(image_0, image_1)
        pad_res = PWCNet.NUM_FEATURE_LEVELS
        self.use_spline = use_spline
        self.image_0 = image_0
        self.image_1 = image_1
        self.image_0_padded = img.pad_image_for_divisibility(image_0, pad_res)
        self.image_1_padded = img.pad_image_for_divisibility(image_1, pad_res)
        self.flow_0_1, self.flow_1_0 = get_full_flows(self.image_0, self.image_1, self.flow_model, pad_res)
        self.flow_0_1_padded = img.pad_image_for_divisibility(self.flow_0_1, pad_res)
        self.flow_1_0_padded = img.pad_image_for_divisibility(self.flow_1_0, pad_res)
        if self.use_spline:
            self.flow_midpoints_0_1 = self.sess.run(self.flow_midpoints_tensor,
                                                    feed_dict={self.flow_placeholder: [self.flow_0_1]})
            self.flow_midpoints_1_0 = self.sess.run(self.flow_midpoints_tensor,
                                                    feed_dict={self.flow_placeholder: [self.flow_1_0]})

    def get_interpolated(self, t):
        """
        Interpolates the two frames based on the previous call to set_end_frames_and_compute_flow.
        :param t: Scalar. The time to interpolate at, between [0, 1].
        :return: The interpolated frame with shape [H, W, 3].
        """
        pad_res = PWCNet.NUM_FEATURE_LEVELS
        flow_0_t, flow_1_t = self._get_flows_at_t(t)
        flow_0_t_padded = img.pad_image_for_divisibility(flow_0_t[0], pad_res)
        flow_1_t_padded = img.pad_image_for_divisibility(flow_1_t[0], pad_res)

        # Interpolate and crop the image (the inputs were padded).
        interpolated = self.synthesizer_model.run(
            ([self.image_0_padded], [self.image_1_padded],
             [self.flow_0_1_padded], [self.flow_1_0_padded], [flow_0_t_padded], [flow_1_t_padded], [t]))
        interpolated = np.clip(interpolated, 0, 1)[0]
        h, w, _ = np.shape(self.image_0)
        interpolated = img.centered_crop(interpolated, h, w)
        return interpolated

    def _get_flows_at_t(self, t):
        """
        Gets the flows that would warp the images towards time t.
        :param t: Scalar.
        :return: Numpy arrays flow_0_t, flow_1_t. These have shape [1, H, W, 2].
        """
        assert t <= 1
        assert t >= 0
        if self.use_spline:
            flow_0_t = self.sess.run(self.splined_flow,
                                     feed_dict={self.flow_placeholder: [self.flow_0_1],
                                                self.flow_midpoints_placeholder: self.flow_midpoints_0_1,
                                                self.t_placeholder: [t]})
            flow_1_t = self.sess.run(self.splined_flow,
                                     feed_dict={self.flow_placeholder: [self.flow_1_0],
                                                self.flow_midpoints_placeholder: self.flow_midpoints_1_0,
                                                self.t_placeholder: [1.0 - t]})
        else:
            flow_0_t = [self.flow_0_1 * t]
            flow_1_t = [self.flow_1_0 * (1.0 - t)]
        return flow_0_t, flow_1_t


class InterpRunner:
    def __init__(self, synthesizer_model_dir, flow_model_dir, hinted_flow_model_dir):
        """
        Creates an interpolation runtime.
        :param synthesizer_model_dir: String.
        :param flow_model_dir: String.
        :param hinted_flow_model_dir: String.
        """
        self._session = None
        self._forward_warp_middleburry = None
        self._forward_warp_bidir = None
        self._maxpool = None
        self._vector_maxpool = None
        self._optimize_quadratic_motion = None
        self._spline_flow = None
        self._lock = threading.Lock()
        self.synthesizer_model_dir = synthesizer_model_dir
        self.flow_model_dir = flow_model_dir
        self.hinted_flow_model_dir = hinted_flow_model_dir
        models = load_saved_models(self.synthesizer_model_dir, self.flow_model_dir,
                                   hinted_flow_model_dir=self.hinted_flow_model_dir)
        if len(models) == 3:
            self._synthesizer_model, self._flow_model, self._hinted_flow_model = models
        else:
            self._synthesizer_model, self._flow_model = models
            self._hinted_flow_model = None
        self._load_tf_ops()

    def _load_tf_ops(self):
        """
        Creates the Tensorflow runtime graph and functions to execute the graph.
        """
        import tensorflow as tf
        config_proto = tf.ConfigProto()
        config_proto.gpu_options.allow_growth = True
        self._session = tf.Session(config=config_proto)

        from common.forward_warp.forward_warp_interp import forward_warp_interp
        feature_0_tensor = tf.placeholder(dtype=tf.float32, shape=[None, None, None, None])
        feature_1_tensor = tf.placeholder(dtype=tf.float32, shape=[None, None, None, None])
        flow_tensor_0 = tf.placeholder(dtype=tf.float32, shape=[None, None, None, 2])
        flow_tensor_1 = tf.placeholder(dtype=tf.float32, shape=[None, None, None, 2])
        flow_tensor_0_t = tf.placeholder(dtype=tf.float32, shape=[None, None, None, 2])
        flow_tensor_1_t = tf.placeholder(dtype=tf.float32, shape=[None, None, None, 2])
        t_tensor = tf.placeholder(dtype=tf.float32, shape=[None])
        middlebury_warp, _, _, _ = forward_warp_interp(feature_0_tensor, feature_1_tensor, flow_tensor_0, t_tensor, True)

        from common.forward_warp.forward_warp_bidirectional import forward_warp_bidirectional
        warp_outputs = forward_warp_bidirectional(feature_0_tensor, feature_1_tensor, flow_tensor_0,
                                                  flow_tensor_1, t_tensor, flow_0_t=flow_tensor_0_t,
                                                  flow_1_t=flow_tensor_1_t, fill_holes=True)
        warp_0_1, warp_1_0, _ = warp_outputs
        bidir_warp = [warp_0_1, warp_1_0]

        maxpooled_features = tf.layers.max_pooling2d(feature_0_tensor, 2, 2)
        vector_maxpooled_features = vector_max_pool(feature_0_tensor)
        flow_midpoint_tensor = QuadraticMotionLayer(flow_tensor_0).mid_points
        splined_flow = quadratic_bezier(flow_tensor_0, flow_tensor_1, t_tensor)

        def forward_warp_middleburry(image_0, image_1, flow, t):
            return self._session.run(middlebury_warp, feed_dict={
                feature_0_tensor: image_0,
                feature_1_tensor: image_1,
                flow_tensor_0: flow,
                t_tensor: t
            })

        def forward_warp_bidir(image_0, image_1, flow_0_1, flow_1_0, flow_0_1_t, flow_1_0_t, t):
            return self._session.run(bidir_warp, feed_dict={
                feature_0_tensor: image_0,
                feature_1_tensor: image_1,
                flow_tensor_0: flow_0_1,
                flow_tensor_1: flow_1_0,
                flow_tensor_0_t: flow_0_1_t,
                flow_tensor_1_t: flow_1_0_t,
                t_tensor: t
            })

        def maxpool(image):
            return self._session.run(maxpooled_features, feed_dict={feature_0_tensor: image})

        def vector_maxpool(image):
            return self._session.run(vector_maxpooled_features, feed_dict={feature_0_tensor: image})

        def optimize_quadratic_motion(flow):
            return self._session.run(flow_midpoint_tensor, feed_dict={flow_tensor_0: flow})

        def spline_flow(flow, midpoints, t):
            return self._session.run(splined_flow, feed_dict={
                flow_tensor_0: flow,
                flow_tensor_1: midpoints,
                t_tensor: t
            })

        self._forward_warp_middleburry = forward_warp_middleburry
        self._forward_warp_bidir = forward_warp_bidir
        self._maxpool = maxpool
        self._vector_maxpool = vector_maxpool
        self._optimize_quadratic_motion = optimize_quadratic_motion
        self._spline_flow = spline_flow

    def get_flow(self, image_0, image_1, res_fraction=1.0):
        """
        :param image_0: Image of shape [H, W, 3].
        :param image_1: Image of shape [H, W, 3].
        :param res_fraction: Float. Runs the flow network at a fraction of the original resolution.
        :return: forward flow, stretched backward flow, cropped forward flow, cropped backward flow.
                Shapes are all [H_stretched, W_stretched, C].
        """
        # Special case for resolution fraction of 1.0.
        if res_fraction == 1.0:
            return self._get_flow(image_0, image_1)
        # Resize images.
        image_0, image_1, actual_scale_x, actual_scale_y = self._resize_images(image_0, image_1,
                                                                               res_fraction, res_fraction)
        # Run flow network.
        flows_0_1, flows_1_0 = self._get_flow(image_0, image_1)
        # Unresize the flows.
        flows_0_1, flows_1_0 = self._unsize_flows(flows_0_1, flows_1_0, actual_scale_x, actual_scale_y)
        return flows_0_1, flows_1_0

    def get_hinted_flow(self, image_0, image_1, hints_0_1, hint_mask_0_1, hints_1_0, hint_mask_1_0, res_fraction=1.0):
        """
        :param image_0: Image of shape [H, W, 3].
        :param image_1: Image of shape [H, W, 3].
        :param hints_0_1: Hints of shape [H, W, 2].
        :param hint_mask_0_1: Hint mask of shape [H, W, 1].
        :param hints_1_0: Hints of shape [H, W, 2].
        :param hint_mask_1_0: Hint mask of shape [H, W, 1].
        :param image_0_alpha: Optional image alpha mask of shape [H, W, 1]. Values of 1.0 indicates opaque areas.
        :param image_1_alpha: Optional image alpha mask of shape [H, W, 1]. Values of 1.0 indicates opaque areas.
        :param res_fraction: Float. Runs the flow network at a fraction of the original resolution.
        :return: forward flow, stretched backward flow, cropped forward flow, cropped backward flow.
                Shapes are all [H_stretched, W_stretched, C].
        """
        # Special case for resolution fraction of 1.0.
        if res_fraction == 1.0:
            return self._get_hinted_flow(image_0, image_1, hints_0_1, hint_mask_0_1, hints_1_0, hint_mask_1_0)
        # Find the actual res_fraction as a power of 2.
        old_height, old_width, _ = hints_0_1.shape
        current_height, current_width = old_height, old_width
        while (current_height / old_height) > res_fraction:
            with self._lock:
                vector_maxpooled = self._vector_maxpool(self._make_divisible_by_2([hints_0_1, hints_1_0]))
                hints_0_1, hints_1_0 = vector_maxpooled[0] / 2, vector_maxpooled[1] / 2
                maxpooled = self._maxpool(self._make_divisible_by_2([hint_mask_0_1, hint_mask_1_0]))
                hint_mask_0_1, hint_mask_1_0 = maxpooled[0], maxpooled[1]
            current_height, current_width, _ = hints_0_1.shape
        res_fraction_y, res_fraction_x = current_height / old_height, current_width / old_width
        # Resize images.
        image_0, image_1, actual_scale_x, actual_scale_y = self._resize_images(image_0, image_1,
                                                                               res_fraction_x, res_fraction_y)
        # Run flow network.
        flows_0_1, flows_1_0 = self._get_hinted_flow(image_0, image_1, hints_0_1, hint_mask_0_1,
                                                     hints_1_0, hint_mask_1_0)
        # Unresize the flows.
        flows_0_1, flows_1_0 = self._unsize_flows(flows_0_1, flows_1_0, actual_scale_x, actual_scale_y)
        return flows_0_1, flows_1_0

    def _resize_images(self, image_0, image_1, res_fraction_x, res_fraction_y):
        """
        :param image_0: Image of shape [H, W, 3].
        :param image_1: Image of shape [H, W, 3].
        :param res_fraction_x: Float. Horizontal fraction to resize the image to.
        :param res_fraction_y: Float. Vertical fraction to resize the image to.
        :return: Images of shape [H * res_fraction_y, W * res_fraction_x, 3]. Also returns the actual scaling applied.
        """
        old_height, old_width, _ = image_0.shape
        image_0 = cv2.resize(image_0, (0, 0), fx=res_fraction_x, fy=res_fraction_y)
        image_1 = cv2.resize(image_1, (0, 0), fx=res_fraction_x, fy=res_fraction_y)
        new_height, new_width, _ = image_0.shape
        actual_scale_x = new_width / old_width
        actual_scale_y = new_height / old_height
        return image_0, image_1, actual_scale_x, actual_scale_y

    def _unsize_flows(self, flows_0_1, flows_1_0, actual_scale_x, actual_scale_y):
        """
        :param flows_0_1: Flow of shape [H, W, 2].
        :param flows_1_0: Flow of shape [H, W, 2].
        :param actual_scale_x: Original scaling applied to the horizontal flow.
        :param actual_scale_y: Original scaling applied to the vertical flow.
        :return: Flow of shape [H / actual_scale_y, W / actual_scale_x, 2].
        """
        # Resize to the original image size and scale the vectors accordingly.
        actual_scale = np.asarray([[[actual_scale_x, actual_scale_y]]], dtype=np.float32)
        flows_0_1 = cv2.resize(flows_0_1 / actual_scale, (0, 0), fx=1.0 / actual_scale_x, fy=1.0 / actual_scale_y)
        flows_1_0 = cv2.resize(flows_1_0 / actual_scale, (0, 0), fx=1.0 / actual_scale_x, fy=1.0 / actual_scale_y)
        return flows_0_1, flows_1_0

    def _get_flow(self, image_0, image_1):
        """
        Gets flow without hints.
        :param image_0: Image of shape [H, W, 3].
        :param image_1: Image of shape [H, W, 3].
        :return: forward flow, stretched backward flow, cropped forward flow, cropped backward flow.
                Shapes are all [H_stretched, W_stretched, C].
        """
        original_height, original_width, _ = image_0.shape
        img_0_stretched = img.stretch_image_for_divisibility(image_0, PWCNet.NUM_FEATURE_LEVELS)
        img_1_stretched = img.stretch_image_for_divisibility(image_1, PWCNet.NUM_FEATURE_LEVELS)
        img_0_stretched = np.expand_dims(img_0_stretched, axis=0)
        img_1_stretched = np.expand_dims(img_1_stretched, axis=0)
        model_input = (np.concatenate([img_0_stretched, img_1_stretched], axis=0),
                       np.concatenate([img_1_stretched, img_0_stretched], axis=0))
        with self._lock:
            flows, _ = self._flow_model.run(model_input)
        flows_0_1 = flow.resize_flow(flows[0], original_height, original_width)
        flows_1_0 = flow.resize_flow(flows[1], original_height, original_width)
        return flows_0_1, flows_1_0

    def _get_hinted_flow(self, image_0, image_1, hints_0_1, hint_mask_0_1, hints_1_0, hint_mask_1_0):
        """
        Gets flow with hints.
        :param image_0: Image of shape [H, W, 3].
        :param image_1: Image of shape [H, W, 3].
        :param hints_0_1: Hints of shape [H, W, 2].
        :param hint_mask_0_1: Hint mask of shape [H, W, 1].
        :param hints_1_0: Hints of shape [H, W, 2].
        :param hint_mask_1_0: Hint mask of shape [H, W, 1].
        :return: forward flow, stretched backward flow, cropped forward flow, cropped backward flow.
                Shapes are all [H_stretched, W_stretched, C].
        """
        if self._hinted_flow_model is None:
            return self._get_flow(image_0, image_1)
        original_height, original_width, _ = image_0.shape
        img_0_stretched = img.stretch_image_for_divisibility(image_0, PWCNet.NUM_FEATURE_LEVELS)
        img_1_stretched = img.stretch_image_for_divisibility(image_1, PWCNet.NUM_FEATURE_LEVELS)
        # Hints should not be interpolated during resizing. Therefore, use nearest neighbour stretching.
        hints_0_1_stretched, hint_mask_0_1_stretched = flow.stretch_flow_and_mask_for_divisibility(
            hints_0_1, hint_mask_0_1, PWCNet.NUM_FEATURE_LEVELS, nearest_neighbour=True)
        hints_1_0_stretched, hint_mask_1_0_stretched = flow.stretch_flow_and_mask_for_divisibility(
            hints_1_0, hint_mask_1_0, PWCNet.NUM_FEATURE_LEVELS, nearest_neighbour=True)
        # Concat them in the batch dimension.
        img_0_stretched = np.expand_dims(img_0_stretched, axis=0)
        img_1_stretched = np.expand_dims(img_1_stretched, axis=0)
        hints_0_1_stretched = np.expand_dims(hints_0_1_stretched, axis=0)
        hint_mask_0_1_stretched = np.expand_dims(hint_mask_0_1_stretched, axis=0)
        hints_1_0_stretched = np.expand_dims(hints_1_0_stretched, axis=0)
        hint_mask_1_0_stretched = np.expand_dims(hint_mask_1_0_stretched, axis=0)
        model_input = (np.concatenate([img_0_stretched, img_1_stretched], axis=0),
                       np.concatenate([img_1_stretched, img_0_stretched], axis=0),
                       np.concatenate([hints_0_1_stretched, hints_1_0_stretched], axis=0),
                       np.concatenate([hint_mask_0_1_stretched, hint_mask_1_0_stretched], axis=0))
        with self._lock:
            flows, _ = self._hinted_flow_model.run(model_input)
        flows_0_1 = flow.resize_flow(flows[0], original_height, original_width)
        flows_1_0 = flow.resize_flow(flows[1], original_height, original_width)
        return flows_0_1, flows_1_0

    def apply_alpha_mask(self, flows_0_1, flows_1_0, image_0_alpha, image_1_alpha):
        """
        Applies the alpha mask to the inputted flows by invalidating transparent areas.
        """
        flows_0_1 = flows_0_1 + (1.0 - image_0_alpha) * 1e6
        flows_1_0 = flows_1_0 + (1.0 - image_1_alpha) * 1e6
        return flows_0_1, flows_1_0

    def interp(self, image_0, image_1, flow_0_1, flow_1_0, t, flow_0_1_midpoints=None, flow_1_0_midpoints=None):
        """
        :param image_0: Image of shape (H, W, 3).
        :param image_1: Image of shape (H, W, 3).
        :param flow_0_1: Flow of shape (H, W, 2).
        :param flow_1_0: Flow of shape (H, W, 2).
        :param t: Float between 0 and 1.
        :param flow_0_1_midpoints: Quadratic bezier midpoints for flow_0_1.
        :param flow_1_0_midpoints: Quadratic bezier midpoints for flow_1_0.
        :return: Interpolated image with the same shape as the input images.
        """
        flow_0_1_t, flow_1_0_t = self._get_flow_at_t(flow_0_1, flow_1_0, t, flow_0_1_midpoints, flow_1_0_midpoints)
        original_height, original_width, _ = image_0.shape
        image_0 = img.pad_image_for_divisibility(image_0, PWCNet.NUM_FEATURE_LEVELS)
        image_1 = img.pad_image_for_divisibility(image_1, PWCNet.NUM_FEATURE_LEVELS)
        flow_0_1 = img.pad_image_for_divisibility(flow_0_1, PWCNet.NUM_FEATURE_LEVELS)
        flow_1_0 = img.pad_image_for_divisibility(flow_1_0, PWCNet.NUM_FEATURE_LEVELS)
        flow_0_1_t = img.pad_image_for_divisibility(flow_0_1_t, PWCNet.NUM_FEATURE_LEVELS)
        flow_1_0_t = img.pad_image_for_divisibility(flow_1_0_t, PWCNet.NUM_FEATURE_LEVELS)
        with self._lock:
            interpolated = self._synthesizer_model.run(([image_0], [image_1], [flow_0_1], [flow_1_0],
                                                        [flow_0_1_t], [flow_1_0_t], [t]))
        interpolated = np.clip(interpolated, 0.0, 1.0)[0]
        interpolated = img.centered_crop(interpolated, original_height, original_width)
        return interpolated

    def get_middlebury_interp(self, image_0, image_1, flow_0_1, flow_1_0, t,
                              flow_0_1_midpoints=None, flow_1_0_midpoints=None):
        """
        Runs the middlebury interpolation algorithm.
        :param image_0: Image of shape (H, W, 3).
        :param image_1: Image of shape (H, W, 3).
        :param flow_0_1: Flow of shape (H, W, 2).
        :param flow_1_0: Flow of shape (H, W, 2).
        :param t: Float between 0 and 1.
        :param flow_0_1_midpoints: Quadratic bezier midpoints for flow_0_1.
        :param flow_1_0_midpoints: Quadratic bezier midpoints for flow_1_0.
        :return: Interpolated image with the same shape as the input images.
        """
        # Midpoints not supported by middlebury interp.
        _, _ = flow_0_1_midpoints, flow_1_0_midpoints
        with self._lock:
            forward = self._forward_warp_middleburry([image_0], [image_1], [flow_0_1], [t])
            backward = self._forward_warp_middleburry([image_1], [image_0], [flow_1_0], [(1.0 - t)])
        forward = np.clip(forward, 0.0, 1.0)[0]
        backward = np.clip(backward, 0.0, 1.0)[0]
        return forward, backward

    def get_bidir_interp(self, image_0, image_1, flow_0_1, flow_1_0, t,
                         flow_0_1_midpoints=None, flow_1_0_midpoints=None):
        """
        Runs the middlebury interpolation algorithm.
        :param image_0: Image of shape (H, W, 3).
        :param image_1: Image of shape (H, W, 3).
        :param flow_0_1: Flow of shape (H, W, 2).
        :param flow_1_0: Flow of shape (H, W, 2).
        :param t: Float between 0 and 1.
        :param flow_0_1_midpoints: Quadratic bezier midpoints for flow_0_1.
        :param flow_1_0_midpoints: Quadratic bezier midpoints for flow_1_0.
        :return: Interpolated image with the same shape as the input images.
        """
        flow_0_1_t, flow_1_0_t = self._get_flow_at_t(flow_0_1, flow_1_0, t, flow_0_1_midpoints, flow_1_0_midpoints)
        with self._lock:
            forward, backward = self._forward_warp_bidir([image_0], [image_1], [flow_0_1], [flow_1_0],
                                                         [flow_0_1_t], [flow_1_0_t], [t])
        forward = np.clip(forward, 0.0, 1.0)[0]
        backward = np.clip(backward, 0.0, 1.0)[0]
        return forward, backward

    def _get_flow_at_t(self, flow_0_1, flow_1_0, t, flow_0_1_midpoints=None, flow_1_0_midpoints=None):
        """
        :param flow_0_1: Flow of shape [H, W, 2].
        :param flow_1_0: Flow of shape [H, W, 2].
        :param t: Float between 0.0 and 1.0.
        :param flow_0_1_midpoints: Optional flow of shape [H, W, 2]. If provided, a spline will be used.
        :param flow_1_0_midpoints: Optional flow of shape [H, W, 2]. If provided, a spline will be used.
        :return: Bidirectional flows at time t.
        """
        if flow_0_1_midpoints is not None:
            flow_0_1_t = self.get_splined_flow(flow_0_1, flow_0_1_midpoints, t)
        else:
            flow_0_1_t = flow_0_1 * t
        if flow_1_0_midpoints is not None:
            flow_1_0_t = self.get_splined_flow(flow_1_0, flow_1_0_midpoints, 1.0 - t)
        else:
            flow_1_0_t = flow_1_0 * (1.0 - t)
        return flow_0_1_t, flow_1_0_t

    def get_quadratic_motion_midpoints(self, flow):
        """
        :param flow: Flow of shape [H, W, 2].
        :return: Flow midpoints of shape [H, W, 2].
        """
        with self._lock:
            return self._optimize_quadratic_motion([flow])[0]

    def get_splined_flow(self, flow, midpoints, t):
        """
        :param flow: Flow of shape [H, W, 2].
        :param midpoints: Flow midpoints of shape [H, W, 2].
        :param t: Float from 0.0 to 1.0.
        :return: Flow of shape [H, W, 2] at t.
        """
        with self._lock:
            return self._spline_flow([flow], [midpoints], [t])[0]

    def _make_divisible_by_2(self, images):
        """
        Given a list of images, make their dimensions divisible by 2. Width and/or height may decrease by 1.
        Resizes using nearest neighbour.
        :param images: List of images with shape [H, W, C].
        :return: List of images with shape [H, W, C].
        """
        divisible_images = []
        for image in images:
            height, width = int(image.shape[0]), int(image.shape[1])
            new_height = int(height if height % 2 == 0 else height - 1)
            new_width = int(width if width % 2 == 0 else width - 1)
            processed_image = image
            if height != new_height or width != new_width:
                processed_image = img.resize(image, new_height, new_width, nearest_neighbour=True)
            divisible_images.append(processed_image)
        return divisible_images
