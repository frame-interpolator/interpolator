# To resolve some issues with MacOS and matplotlib:
# https://stackoverflow.com/questions/2512225/matplotlib-plots-not-showing-up-in-mac-osx
import platform
if platform.system() == 'Darwin':
    import matplotlib
    matplotlib.use('TkAgg')
import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from common.affine_transform.affine_transform import affine_transform
from common.utils.img import stretch_image_for_divisibility
from common.utils.pfm import load_pfm
from common.utils.tf import calculate_box_rotation_zoom


FLOW_CHANNELS = 2
FLOW_TAG_FLOAT = 202021.25


def read_flow_file(file_name, extras=None, use_default_valid=False):
    """
    Supports .pfm, .flo, or .png extensions.
    The .png extension assumes the Kitti format.
    :param file_name: Str.
    :param extras: Optional dict.
        A valid mask is returned under the 'valid' key if there is one (i.e. if the format is png). Otherwise, the
        'valid' key will return None.
    :param use_default_valid: Bool. If True, extras['valid'] will return ones (true) by default if the file was valid.
        Otherwise, None will be returned if no valid mask was found in the flow file.
    :return: Numpy array of shape (height, width, FLOW_CHANNELS).
        Returns None if the tag in the file header was invalid.
    """
    if extras is None:
        extras = {}
    extras['valid'] = None
    if file_name.endswith('.flo'):
        with open(file_name, 'rb') as file:
            tag = np.fromfile(file, dtype=np.float32, count=1)[0]
            if tag != FLOW_TAG_FLOAT:
                return None
            width = np.fromfile(file, dtype=np.int32, count=1)[0]
            height = np.fromfile(file, dtype=np.int32, count=1)[0]

            num_image_floats = width * height * FLOW_CHANNELS
            image = np.fromfile(file, dtype=np.float32, count=num_image_floats)
            image = image.reshape((height, width, FLOW_CHANNELS))
            final_flow = image
    elif file_name.endswith('.pfm'):
        np_array, scale = load_pfm(file_name)
        if scale != 1.0:
            raise Exception('Pfm flow scale must be 1.0.')
        # PFM file convention has the y axis going upward. This is unconventional, so we need to flip it.
        # Also, PFM can only have 1 or 3 colour channels. For flow, the last channel is set to 0.0.
        final_flow = np.flip(np_array[..., 0:2], axis=0)
    elif file_name.endswith('.png'):
        raw = cv2.imread(file_name, cv2.IMREAD_UNCHANGED)
        raw = cv2.cvtColor(raw, cv2.COLOR_BGR2RGB)
        if raw is None:
            return None
        assert raw.dtype == np.uint16
        flow = (raw[..., :2].astype(np.float32) - (2 ** 15)) / 64.0
        valid = raw[..., 2:].astype(np.bool)
        assert valid.shape[2] == 1
        extras['valid'] = valid
        final_flow = flow * valid.astype(np.float32)
    else:
        raise Exception('Not a supported flow format.')
    if extras['valid'] is None and use_default_valid:
        height, width = final_flow.shape[0], final_flow.shape[1]
        extras['valid'] = np.ones((height, width, 1), dtype=np.bool)
    return final_flow


def write_flow_file(file_name, flow):
    """
    Writes .flo formatted file.
    :param file_name: Str.
    :param flow: Np array.
    :return: Nothing.
    """
    if flow.dtype != np.float32:
        flow = flow.astype(np.float32)
    tag = np.array([FLOW_TAG_FLOAT], np.float32)
    height, width = flow.shape[:2]
    height, width = np.array([height], np.int32), np.array([width], np.int32)
    with open(file_name, 'wb') as file:
        tag.tofile(file)
        width.tofile(file)
        height.tofile(file)
        flow.tofile(file)


def get_flow_visualization(flow):
    """
    Uses the HSV color wheel for the visualization.
    Red means movement to the right, blue means movement to the left, yellow means movement downward,
    purple means movement upward.
    :param flow: Numpy array of shape (Height, Width, 2).
    :return: Rgb uint8 image.
    """
    abs_image = np.abs(flow)
    flow_mean = np.mean(abs_image)
    flow_std = np.std(abs_image)

    # Apply some kind of normalization. Divide by the perceived maximum (mean + std)
    flow = flow / (flow_mean + flow_std + 1e-10)

    # Get the angle and radius.
    # sqrt(x^2 + y^2).
    radius = np.sqrt(np.square(flow[..., 0]) + np.square(flow[..., 1]))
    radius_clipped = np.clip(radius, 0.0, 1)
    # Arctan2(-y, -x) / pi so that it's between [-1, 1].
    angle = np.arctan2(-flow[..., 1], -flow[..., 0]) / np.pi

    # Hue is between [0, 1] but scaled by (179.0 / 255.0) because hue is usually between [0, 179].
    hue = np.clip((angle + 1.0) / 2.0, 0.0, 1.0) * (179.0 / 255.0)
    # Saturation is always 0.75 (it's more aesthetic).
    saturation = np.ones(shape=hue.shape, dtype=np.float) * 0.75
    # Value (brightness), is the radius (magnitude) of the flow.
    value = radius_clipped

    hsv_img = (np.clip(np.stack([hue, saturation, value], axis=-1) * 255, 0, 255)).astype(dtype=np.uint8)
    return cv2.cvtColor(hsv_img, cv2.COLOR_HSV2RGB)


def get_tf_flow_visualization(flow):
    """
    :param flow: Optical flow tensor.
    :return: RGB image normalized between 0 and 1.
    """
    with tf.name_scope('flow_visualization'):
        # B, H, W, C dimensions.
        abs_image = tf.abs(flow)
        flow_mean, flow_var = tf.nn.moments(abs_image, axes=[1, 2, 3])
        flow_std = tf.sqrt(flow_var)

        # Apply some kind of normalization. Divide by the perceived maximum (mean + std)
        flow = flow / tf.expand_dims(tf.expand_dims(
            tf.expand_dims(flow_mean + flow_std + 1e-10, axis=-1), axis=-1), axis=-1)

        radius = tf.sqrt(tf.reduce_sum(tf.square(flow), axis=-1))
        radius_clipped = tf.clip_by_value(radius, 0.0, 1.0)
        angle = tf.atan2(-flow[..., 1], -flow[..., 0]) / np.pi

        hue = tf.clip_by_value((angle + 1.0) / 2.0, 0.0, 1.0)
        saturation = tf.ones(shape=tf.shape(hue), dtype=tf.float32) * 0.75
        value = radius_clipped
        hsv = tf.stack([hue, saturation, value], axis=-1)
        return tf.image.hsv_to_rgb(hsv)


def show_flow_image(flow):
    """
    Opens a window displaying an optical flow visualization.
    :param flow: Numpy array of shape (Height, Width, 2).
    :return: Nothing.
    """
    plt.imshow(get_flow_visualization(flow))
    plt.show()


def _scale_flow(flow, x_scale, y_scale):
    """
    Scales the x and y values of the flow via broadcasting.
    :param flow: Np array of shape (height, width, 2).
    :param x_scale: Float.
    :param y_scale: Float.
    :return: Same shape as the input flow.
    """
    return flow * np.asarray([[[x_scale, y_scale]]], dtype=np.float32)


def resize_flow(flow, height, width, nearest_neighbour=False):
    """
    Resizes the flow image and rescales the flow vectors.
    :param flow: Numpy array of shape (original_height, original_width, 2).
    :param height: Int.
    :param width: Int.
    :param nearest_neighbour: Bool. Whether to do nearest neighbour interpolation.
    :return: Flow of shape (height, width, 2).
    """
    interp_method = cv2.INTER_LINEAR
    if nearest_neighbour:
        interp_method = cv2.INTER_NEAREST
    original_height, original_width = flow.shape[0], flow.shape[1]
    flow = np.reshape(cv2.resize(flow, (width, height), interpolation=interp_method), (height, width, 2))
    return _scale_flow(flow, (float(width) / float(original_width)), (float(height) / float(original_height)))


def stretch_flow_for_divisibility(flow, n, nearest_neighbour=False):
    """
    Stretches an optical flow image such that the dimensions can be divisible by 2 at least n times.
    If the dimensions can already be sufficiently divisible by 2, it does nothing.
    :param flow: Numpy array of shape [H, W, 2].
    :param n: Int. Number of times the resolution needs to be divisible by 2.
    :param nearest_neighbour: Bool. Whether to do nearest neighbour interpolation.
    :return: Numpy array of shape [H_d, W_d, 2] where H_d, W_d can be divisible by 2 n times.
    """
    original_height, original_width = flow.shape[0], flow.shape[1]
    flow = stretch_image_for_divisibility(flow, n, nearest_neighbour=nearest_neighbour)
    height, width = flow.shape[0], flow.shape[1]
    return _scale_flow(flow, (float(width) / float(original_width)), (float(height) / float(original_height)))


def stretch_flow_and_mask_for_divisibility(flow, mask, n, nearest_neighbour=False):
    """
    Jointly stretches an optical flow image and a mask such that the dimensions can be divisible by 2 at least n times.
    If the dimensions can already be sufficiently divisible by 2, it does nothing.
    :param flow: Numpy array of shape [H, W, 2].
    :param mask: Numpy array of shape [H, W, 1].
    :param n: Int. Number of times the resolution needs to be divisible by 2.
    :param nearest_neighbour: Bool. Whether to do nearest neighbour interpolation.
    :return: Numpy array of shape [H_d, W_d, 2] where H_d, W_d can be divisible by 2 n times.
    """
    concatted = np.concatenate((flow, mask), axis=-1)
    original_height, original_width = flow.shape[0], flow.shape[1]
    image = stretch_image_for_divisibility(concatted, n, nearest_neighbour=nearest_neighbour)
    height, width = image.shape[0], image.shape[1]
    flow = image[..., 0:2]
    flow = _scale_flow(flow, (float(width) / float(original_width)), (float(height) / float(original_height)))
    mask = image[..., 2:3]
    return flow, mask


def _tf_get_identity_transform():
    transform = tf.constant([[1.0, 0.0, 0.0],
                             [0.0, 1.0, 0.0],
                             [0.0, 0.0, 1.0]], dtype=tf.float32)
    return transform


def _tf_get_zoom_transform(zoom, x_cen, y_cen):
    transform = tf.convert_to_tensor([[zoom, 0.0, -x_cen * zoom + x_cen],
                                      [0.0, zoom, -y_cen * zoom + y_cen],
                                      [0.0, 0.0, 1.0]])
    return transform


def tf_create_affine_transform(image, rotation=None, scale=None, zoom=None, flip_vertical=None, flip_horizontal=None,
                               translate_x=None, translate_y=None):
    """
    Creates a 3x3 affine transform for an image in homogeneous coordinates.
    Applies transforms in the following order:
        Flip vertical
        Flip horizontal
        Rotate
        Scale
        Zoom
        Translate X
        Translate Y
    :param image: Image tensor. Shape is (H, W, C).
    :param rotation: In degrees. Positive values rotate counter-clockwise. Negative values rotate clockwise.
    :param scale: How much to scale by. >1 means vertical scale, <1 means horizontal scale.
    :param zoom: How much to zoom by. >1 means zooming in, <1 means zooming out. None means a zoom of 1.
    :param flip_vertical: Whether to flip up/down. None also means False.
    :param flip_horizontal: Whether to flip left/right. None also means False.
    :param translate_x: Number of pixels to translate in the x direction.
    :param translate_y: Number of pixels to translate in the y direction.
    :return: An affine transform tensor of shape [3, 3].
    """
    height = tf.cast(tf.shape(image)[0], tf.float32)
    width = tf.cast(tf.shape(image)[1], tf.float32)
    y_max = height - 1.0
    y_cen = y_max / 2.0
    x_max = width - 1.0
    x_cen = x_max / 2.0
    transform = _tf_get_identity_transform()
    if flip_vertical is not None:
        current_transform = tf.convert_to_tensor([[1.0, 0.0, 0.0],
                                                  [0.0, -1.0, y_max],
                                                  [0.0, 0.0, 1.0]])
        transform = tf.cond(flip_vertical, lambda: tf.matmul(current_transform, transform),
                            lambda: transform)
    if flip_horizontal is not None:
        current_transform = tf.convert_to_tensor([[-1.0, 0.0, x_max],
                                                  [0.0, 1.0, 0.0],
                                                  [0.0, 0.0, 1.0]])
        transform = tf.cond(flip_horizontal, lambda: tf.matmul(current_transform, transform),
                            lambda: transform)
    if rotation is not None:
        # Convert rotation to radians.
        rotation_rad = rotation * (np.pi / 180.0)
        cost = tf.cos(-rotation_rad)
        sint = tf.sin(-rotation_rad)
        current_transform = tf.convert_to_tensor([[cost, -sint, y_cen * sint - x_cen * cost + x_cen],
                                                  [sint, cost, -(x_cen * sint + y_cen * cost) + y_cen],
                                                  [0.0, 0.0, 1.0]])
        rot_zoom = 1 / (calculate_box_rotation_zoom(height, width, rotation) + 1e-10)
        current_transform = tf.matmul(_tf_get_zoom_transform(rot_zoom, x_cen, y_cen), current_transform)
        transform = tf.matmul(current_transform, transform)
    if scale is not None:
        scale_vert_cond = tf.greater(scale, 1.0)
        current_transform = tf.cond(scale_vert_cond,
                                    lambda: tf.convert_to_tensor([[1.0, 0.0, 0.0],
                                                                  [0.0, scale, -y_cen * scale + y_cen],
                                                                  [0.0, 0.0, 1.0]]),
                                    lambda: tf.convert_to_tensor([[1 / scale, 0.0, -x_cen / scale + x_cen],
                                                                  [0.0, 1.0, 0.0],
                                                                  [0.0, 0.0, 1.0]]))
        transform = tf.matmul(current_transform, transform)
    if zoom is not None:
        current_transform = _tf_get_zoom_transform(zoom, x_cen, y_cen)
        transform = tf.matmul(current_transform, transform)
    if translate_x is not None:
        current_transform = tf.convert_to_tensor([[1.0, 0.0, translate_x],
                                                  [0.0, 1.0, 0.0],
                                                  [0.0, 0.0, 1.0]])
        transform = tf.matmul(current_transform, transform)
    if translate_y is not None:
        current_transform = tf.convert_to_tensor([[1.0, 0.0, 0.0],
                                                  [0.0, 1.0, translate_y],
                                                  [0.0, 0.0, 1.0]])
        transform = tf.matmul(current_transform, transform)
    return transform


def tf_transform_flow(flow, images, rotation=None, scale=None, zoom=None, flip_vertical=None, flip_horizontal=None,
                      zoom_diff=None, scale_diff=None, translate_x_diff=None, translate_y_diff=None,
                      rotation_diff=None, use_diff=True):
    """
    Applies transforms in the following order:
        Flip vertical
        Flip horizontal
        Rotate
        Scale
        Zoom
    :param flow: Optical flow tensor. Shape is (H, W, 2)
    :param images: List of image tensors. Length is 2.
    :param rotation: In degrees. Positive values rotate counter-clockwise. Negative values rotate clockwise.
    :param scale: How much to scale by. >1 means vertical scale, <1 means horizontal scale.
    :param zoom: How much to zoom by. >1 means zooming in, <1 means zooming out. None means a zoom of 1.
    :param flip_vertical: Whether to flip up/down. None also means False.
    :param flip_horizontal: Whether to flip left/right. None also means False.
    :param zoom_diff: Zoom difference between the images.
    :param scale_diff: Scale difference between the images.
    :param translate_x_diff: Translate x difference between the images. Units are in pixels.
    :param translate_y_diff: Translate y difference between the images. Units are in pixels.
    :param rotation_diff: Rotation difference between the images. Units are in degrees.
    :param use_diff: Boolean tensor. Whether to use the diff between images. If None the diff will be used.
    :return: new_flow (tensor), new_images (list of tensors), transforms applied to each image (list of tensors).
    """
    identity = _tf_get_identity_transform()
    assert len(images) == 2, 'Length of the images must be 2.'
    if (rotation, scale, zoom, flip_vertical, flip_horizontal,
        zoom_diff, scale_diff, translate_x_diff, translate_y_diff, rotation_diff) == (None, None, None, None, None,
                                                                                      None, None, None, None, None):
        return flow, images, [identity, identity]
    H, W, _ = tf.unstack(tf.shape(flow))
    transform = tf_create_affine_transform(flow, rotation=rotation, scale=scale, zoom=zoom, flip_vertical=flip_vertical,
                                           flip_horizontal=flip_horizontal)
    diff_transform = tf_create_affine_transform(flow, rotation=rotation_diff, scale=scale_diff, zoom=zoom_diff,
                                                translate_x=translate_x_diff, translate_y=translate_y_diff)
    if use_diff is not None:
        diff_transform = tf.cond(tf.convert_to_tensor(use_diff, dtype=tf.bool),
                                 lambda: diff_transform, lambda: identity)
    # Remove the last row.
    transform_0 = transform[0:2, :]
    transform_1 = (tf.matmul(transform, diff_transform))[0:2, :]
    # Apply the diff_transform to the new flow.
    x = tf.linspace(0.0, tf.cast(W, dtype=tf.float32) - 1.0, W)
    y = tf.linspace(0.0, tf.cast(H, dtype=tf.float32) - 1.0, H)
    x_t, y_t = tf.meshgrid(x, y)
    dx_t = (x_t * diff_transform[0, 0] + y_t * diff_transform[0, 1] + diff_transform[0, 2]) - x_t
    dy_t = (x_t * diff_transform[1, 0] + y_t * diff_transform[1, 1] + diff_transform[1, 2]) - y_t
    new_flow = flow + tf.concat([tf.expand_dims(dx_t, axis=-1), tf.expand_dims(dy_t, axis=-1)], axis=-1)
    # Apply the transform to the flow.
    new_flow = affine_transform(new_flow, transform_0, is_optical_flow=True)
    # Apply transforms to the images.
    new_images = [affine_transform(images[0], transform_0), affine_transform(images[1], transform_1)]
    return new_flow, new_images, [transform_0, transform_1]


def tf_random_transform_flow(flow, images, config):
    """
    Randomly rotates a flow and a set of corresponding images in unison.
    :param flow: Optical flow tensor. Shape is (H, W, C).
    :param images: List of image tensors. Length is 2.
    :param config: Dict.
    :return: new_flow (tensor), new_images (list of tensors).
    """
    left_right_cond, up_down_cond, rotation, scale, zoom = None, None, None, None, None
    zoom_noise, scale_noise, translate_x_noise, translate_y_noise, rotate_noise = None, None, None, None, None
    use_diff = False
    if config['do_flipping']:
        left_right_cond = tf.less(tf.random_uniform([], 0, 1.0), .5) if config['flip_hor'] else None
        up_down_cond = tf.less(tf.random_uniform([], 0, 1.0), .5) if config['flip_ver'] else None
    if config['do_rotating']:
        rotation = tf.random_uniform((), config['rotate_min'], config['rotate_max'], dtype=tf.float32)
    if config['do_scaling']:
        scale = tf.random_uniform((), config['scale_min'], config['scale_max'], dtype=tf.float32)
        zoom = tf.random_uniform((), config['zoom_min'], config['zoom_max'], dtype=tf.float32)
    if config['do_transform_difference']:
        zoom_noise = tf.clip_by_value(tf.random_normal((), mean=1.0, stddev=config['zoom_noise'], dtype=tf.float32),
                                      0.0, 2.0)
        scale_noise = tf.clip_by_value(tf.random_normal((), mean=1.0, stddev=config['scale_noise'], dtype=tf.float32),
                                       0.0, 2.0)
        translate_x_noise = tf.random_normal((), mean=0.0, stddev=config['translate_x_noise'], dtype=tf.float32)
        translate_y_noise = tf.random_normal((), mean=0.0, stddev=config['translate_y_noise'], dtype=tf.float32)
        rotate_noise = tf.random_normal((), mean=0.0, stddev=config['rotate_noise'], dtype=tf.float32)
        use_diff = tf.random_uniform((), 0.0, 1.0, dtype=tf.float32) > 0.5
    return tf_transform_flow(flow, images, rotation=rotation, scale=scale, zoom=zoom,
                             flip_horizontal=left_right_cond, flip_vertical=up_down_cond,
                             zoom_diff=zoom_noise, scale_diff=scale_noise, translate_x_diff=translate_x_noise,
                             translate_y_diff=translate_y_noise, rotation_diff=rotate_noise, use_diff=use_diff)
