import numpy as np
import unittest
from common.utils.multi_gpu import *


class TestMultiGPUUtils(unittest.TestCase):
    def setUp(self):
        # Environment must mock at least 2 CPUs just in case there are no GPUs.
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_average_gradients(self):
        grad_tensor_gpu0_1 = tf.placeholder(shape=(2, 2), dtype=tf.float32)
        grad_tensor_gpu0_2 = tf.placeholder(shape=(1, 3), dtype=tf.float32)
        grad_tensor_gpu1_1 = tf.placeholder(shape=(2, 2), dtype=tf.float32)
        grad_tensor_gpu1_2 = tf.placeholder(shape=(1, 3), dtype=tf.float32)
        dummy_var_1 = object()
        dummy_var_2 = object()

        grads_and_vars_gpu0 = [(grad_tensor_gpu0_1, dummy_var_1), (grad_tensor_gpu0_2, dummy_var_2)]
        grads_and_vars_gpu1 = [(grad_tensor_gpu1_1, dummy_var_1), (grad_tensor_gpu1_2, dummy_var_2)]

        averaged_grads_and_vars = average_gradients([grads_and_vars_gpu0, grads_and_vars_gpu1])
        self.assertEqual(2, len(averaged_grads_and_vars))
        self.assertEqual(2, len(averaged_grads_and_vars[0]))
        self.assertEqual(2, len(averaged_grads_and_vars[1]))
        self.assertEqual(dummy_var_1, averaged_grads_and_vars[0][1])
        self.assertEqual(dummy_var_2, averaged_grads_and_vars[1][1])

        average_grad_tensor_1 = averaged_grads_and_vars[0][0]
        average_grad_tensor_2 = averaged_grads_and_vars[1][0]

        feed_dict = {
            grad_tensor_gpu0_1: [
                [2, 2],
                [3, 3]
            ],
            grad_tensor_gpu0_2: [
                [3, 3, 3]
            ],
            grad_tensor_gpu1_1: [
                [4, 4],
                [1, 1]
            ],
            grad_tensor_gpu1_2: [
                [1, 3, 5]
            ]
        }
        average_grad_1, average_grad_2 = self.sess.run(
            [average_grad_tensor_1, average_grad_tensor_2], feed_dict=feed_dict)
        self.assertTrue(np.allclose(np.asarray([[3, 3],
                                                [2, 2]]), average_grad_1))
        self.assertTrue(np.allclose(np.asarray([[2, 3, 4]]), average_grad_2))

    def test_average_gradients_no_grads(self):
        grads_and_vars_gpu0 = []
        grads_and_vars_gpu1 = []
        averaged_grads_and_vars = average_gradients([grads_and_vars_gpu0, grads_and_vars_gpu1])
        self.assertListEqual([], averaged_grads_and_vars)

    def test_average_gradients_empty(self):
        averaged_grads_and_vars = average_gradients([])
        self.assertListEqual([], averaged_grads_and_vars)

    def test_average_gradients_single_gpu(self):
        grad_tensor_gpu0_1 = tf.placeholder(shape=(2, 2), dtype=tf.float32)
        grad_tensor_gpu0_2 = tf.placeholder(shape=(1, 3), dtype=tf.float32)
        dummy_var_1 = object()
        dummy_var_2 = object()
        grads_and_vars_gpu0 = [(grad_tensor_gpu0_1, dummy_var_1), (grad_tensor_gpu0_2, dummy_var_2)]
        averaged_grads_and_vars = average_gradients([grads_and_vars_gpu0])
        self.assertEqual(2, len(averaged_grads_and_vars))
        self.assertEqual(2, len(averaged_grads_and_vars[0]))
        self.assertEqual(dummy_var_1, averaged_grads_and_vars[0][1])
        self.assertEqual(dummy_var_2, averaged_grads_and_vars[1][1])

        average_grad_tensor_1 = averaged_grads_and_vars[0][0]
        average_grad_tensor_2 = averaged_grads_and_vars[1][0]

        feed_dict = {
            grad_tensor_gpu0_1: [
                [2, 2],
                [3, 3]
            ],
            grad_tensor_gpu0_2: [
                [3, 3, 3]
            ]
        }
        average_grad_1, average_grad_2 = self.sess.run(
            [average_grad_tensor_1, average_grad_tensor_2], feed_dict=feed_dict)
        self.assertTrue(np.allclose(np.asarray([[2, 2],
                                                [3, 3]]), average_grad_1))
        self.assertTrue(np.allclose(np.asarray([[3, 3, 3]]), average_grad_2))

    def test_accumulating_tensor_io_empty(self):
        accumulating_tensor_io = AccumulatingTensorIO(TensorIO.AVERAGED_SCALAR)
        accumulated_io = accumulating_tensor_io.accumulate()
        self.assertTrue(isinstance(accumulated_io, TensorIO))
        self.assertEqual(TensorIO.AVERAGED_SCALAR, accumulated_io.tensor_type)
        self.assertEqual(0, len(accumulated_io.tensors))

    def test_accumulating_tensor_io_averaged_scalar(self):
        scalar_0_1 = tf.constant(1.0, dtype=tf.float32)
        scalar_0_2 = tf.constant(2.0, dtype=tf.float32)
        scalar_1_1 = tf.constant(3.0, dtype=tf.float32)
        scalar_1_2 = tf.constant(7.0, dtype=tf.float32)
        accumulating_tensor_io = AccumulatingTensorIO(TensorIO.AVERAGED_SCALAR)
        accumulating_tensor_io.add([scalar_0_1, scalar_1_1])
        accumulating_tensor_io.add([scalar_0_2, scalar_1_2])
        accumulated_io = accumulating_tensor_io.accumulate()
        self.assertTrue(isinstance(accumulated_io, TensorIO))
        self.assertEqual(TensorIO.AVERAGED_SCALAR, accumulated_io.tensor_type)
        outputs = self.sess.run(accumulated_io.tensors)
        self.assertEqual(2, len(outputs))
        self.assertEqual(1.5, outputs[0])
        self.assertEqual(5, outputs[1])

    def test_accumulating_tensor_io_summed_scalar(self):
        scalar_0_1 = tf.constant(1.0, dtype=tf.float32)
        scalar_0_2 = tf.constant(2.0, dtype=tf.float32)
        scalar_1_1 = tf.constant(3.0, dtype=tf.float32)
        scalar_1_2 = tf.constant(7.0, dtype=tf.float32)
        accumulating_tensor_io = AccumulatingTensorIO(TensorIO.SUMMED_SCALAR)
        accumulating_tensor_io.add([scalar_0_1, scalar_1_1])
        accumulating_tensor_io.add([scalar_0_2, scalar_1_2])
        accumulated_io = accumulating_tensor_io.accumulate()
        self.assertTrue(isinstance(accumulated_io, TensorIO))
        self.assertEqual(TensorIO.SUMMED_SCALAR, accumulated_io.tensor_type)
        outputs = self.sess.run(accumulated_io.tensors)
        self.assertEqual(2, len(outputs))
        self.assertEqual(3, outputs[0])
        self.assertEqual(10, outputs[1])

    def test_accumulating_tensor_io_batched_scalar(self):
        scalar_0_1 = tf.constant([1.0], dtype=tf.float32)
        scalar_0_2 = tf.constant([2.0], dtype=tf.float32)
        scalar_1_1 = tf.constant([3.0], dtype=tf.float32)
        scalar_1_2 = tf.constant([7.0], dtype=tf.float32)
        accumulating_tensor_io = AccumulatingTensorIO(TensorIO.BATCH)
        accumulating_tensor_io.add([scalar_0_1, scalar_1_1])
        accumulating_tensor_io.add([scalar_0_2, scalar_1_2])
        accumulated_io = accumulating_tensor_io.accumulate()
        self.assertTrue(isinstance(accumulated_io, TensorIO))
        self.assertEqual(TensorIO.BATCH, accumulated_io.tensor_type)
        outputs = self.sess.run(accumulated_io.tensors)
        self.assertEqual(2, len(outputs))
        self.assertTrue(np.allclose(np.asarray([1.0, 2.0]), outputs[0]))
        self.assertTrue(np.allclose(np.asarray([3.0, 7.0]), outputs[1]))

    def test_accumulating_tensor_io_deep_batched_scalar(self):
        scalar_0_1 = tf.constant([[1.0]], dtype=tf.float32)
        scalar_0_2 = tf.constant([[2.0]], dtype=tf.float32)
        scalar_1_1 = tf.constant([[3.0]], dtype=tf.float32)
        scalar_1_2 = tf.constant([[7.0]], dtype=tf.float32)
        accumulating_tensor_io = AccumulatingTensorIO(TensorIO.BATCH)
        accumulating_tensor_io.add([scalar_0_1, scalar_1_1])
        accumulating_tensor_io.add([scalar_0_2, scalar_1_2])
        accumulated_io = accumulating_tensor_io.accumulate()
        self.assertTrue(isinstance(accumulated_io, TensorIO))
        self.assertEqual(TensorIO.BATCH, accumulated_io.tensor_type)
        outputs = self.sess.run(accumulated_io.tensors)
        self.assertEqual(2, len(outputs))
        self.assertTrue(np.allclose(np.asarray([[1.0], [2.0]]), outputs[0]))
        self.assertTrue(np.allclose(np.asarray([[3.0], [7.0]]), outputs[1]))

    def test_create_train_op_default_device(self):
        self.create_train_op_one_device_helper(None)

    def test_create_train_op_default_device_gradient_clipping(self):
        self.create_train_op_one_device_helper(None, gradient_clipping_norm=6.0)

    def test_create_train_op_default_device_more_gradient_clipping(self):
        self.create_train_op_one_device_helper(None, gradient_clipping_norm=5.5, expected_loss=0.5,
                                               expected_variable=0.08333)

    def test_create_train_op_one_device(self):
        self.create_train_op_one_device_helper(['/cpu:0'])

    def create_train_op_one_device_helper(self, devices, gradient_clipping_norm=None, expected_loss=0.0,
                                          expected_variable=0.0):
        variables = []
        return_dict = {}

        def build_network_outputs(tensor_1, tensor_2, tensor_3):
            variable = tf.Variable(1.0, trainable=True, dtype=tf.float32)
            loss = tf.reduce_mean((tensor_1 + tensor_2) * tensor_3 * variable)
            variables.append(variable)
            return_dict['loss'] = TensorIO([loss])
            return_dict['variable'] = TensorIO([variable])
            return return_dict

        # Note this learning rate is set up such that the loss is 0.0 after 1 iteration.
        optimizer = tf.train.GradientDescentOptimizer(1.0 / 6.0)
        batched_network_args = [tf.constant([1.0, 2.0], dtype=tf.float32), tf.constant([2.0, 1.0], dtype=tf.float32)]
        other_network_args = [tf.constant(2.0, dtype=tf.float32)]

        train_op, global_step, output_dict = create_train_op(
            optimizer, build_network_outputs, batched_network_args, other_network_args,
            gradient_clipping_norm=gradient_clipping_norm, available_devices=devices)
        self.assertEqual(output_dict, return_dict)  # This ensures the accumulating is bypassed.
        self.assertEqual(1, len(variables))
        self.assertEqual(2, len(output_dict.keys()))
        self.assertTrue('loss' in output_dict)
        self.sess.run(tf.global_variables_initializer())

        loss = self.sess.run(output_dict['loss'].first())
        self.assertEqual(6.0, loss)

        self.sess.run(train_op)
        step, loss, variable = self.sess.run(
            [global_step, output_dict['loss'].first(), output_dict['variable'].first()])
        self.assertAlmostEqual(expected_loss, loss, places=5)
        self.assertAlmostEqual(expected_variable, variable, places=5)
        self.assertEqual(1, step)

    def test_create_train_op_multiple_devices(self):
        self.create_train_op_multiple_devices_helper()

    def test_create_train_op_multiple_devices_gradient_clipping(self):
        self.create_train_op_multiple_devices_helper(gradient_clipping_norm=6.0)

    def test_create_train_op_multiple_devices_more_gradient_clipping(self):
        self.create_train_op_multiple_devices_helper(gradient_clipping_norm=5.5, expected_loss=0.5,
                                                     expected_variable=0.08333)

    def create_train_op_multiple_devices_helper(self, gradient_clipping_norm=None, expected_loss=0.0,
                                                expected_variable=0.0):
        devices = self.get_devices_for_testing()
        variables = []
        losses = []

        def build_network_outputs(tensor_1, tensor_2, tensor_3):
            with tf.variable_scope('multi_device_test_scope', reuse=tf.AUTO_REUSE):
                variable = tf.get_variable('test_variable', shape=(), dtype=tf.float32,
                                           initializer=tf.constant_initializer(1.0))
                variables.append(variable)
                loss_per_example = tf.expand_dims((tensor_1 + tensor_2) * tensor_3 * variable, axis=-1)
                loss = tf.reduce_mean(loss_per_example)
                losses.append(loss)
                return {'loss': TensorIO([loss]), 'variable': TensorIO([variable]),
                        'loss_per_example': TensorIO([loss_per_example], TensorIO.BATCH)}

        # Note this learning rate is set up such that the loss is 0.0 after 1 iteration.
        optimizer = tf.train.GradientDescentOptimizer(1.0 / 6.0)
        batched_network_args = [tf.constant([1.0, 2.0], dtype=tf.float32), tf.constant([2.0, 1.0], dtype=tf.float32)]
        other_network_args = [tf.constant(2.0, dtype=tf.float32)]

        train_op, global_step, output_dict = create_train_op(
            optimizer, build_network_outputs, batched_network_args, other_network_args,
            gradient_clipping_norm=gradient_clipping_norm, available_devices=devices)
        self.assertEqual(3, len(output_dict.keys()))
        self.assertTrue('loss' in output_dict)
        self.assertEqual(2, len(variables))  # This ensures 2 sub-batches are made.
        self.assertEqual(variables[0], variables[1])  # This ensures the shared variables are the same.
        self.assertEqual(devices[0], losses[0].device)
        self.assertEqual(devices[1], losses[1].device)
        self.sess.run(tf.global_variables_initializer())

        loss = self.sess.run(output_dict['loss'].first())
        self.assertEqual(6.0, loss)

        self.sess.run(train_op)
        step, loss, variable, loss_per_example = self.sess.run(
            [global_step, output_dict['loss'].first(), output_dict['variable'].first(),
             output_dict['loss_per_example'].first()])
        self.assertAlmostEqual(expected_loss, loss, places=5)
        self.assertAlmostEqual(expected_variable, variable, places=5)
        self.assertEqual(1, step)
        self.assertTupleEqual((2, 1), loss_per_example.shape)
        self.assertAlmostEqual(expected_loss, loss_per_example[0][0], places=5)
        self.assertAlmostEqual(expected_loss, loss_per_example[1][0], places=5)

    def test_create_train_op_multiple_devices_unshared_variable_failure(self):
        devices = self.get_devices_for_testing()
        variables = []
        losses = []

        def build_network_outputs(tensor_1, tensor_2, tensor_3):
            with tf.variable_scope('multi_device_test_scope_failure', reuse=False):
                variable = tf.Variable(1.0, trainable=True, dtype=tf.float32)
                variables.append(variable)
                loss = tf.reduce_mean((tensor_1 + tensor_2) * tensor_3 * variable)
                losses.append(loss)
                return {'loss': TensorIO([loss]), 'variable': TensorIO([variable])}

        # Note this learning rate is set up such that the loss is 0.0 after 1 iteration.
        optimizer = tf.train.GradientDescentOptimizer(1.0 / 6.0)
        batched_network_args = [tf.constant([1.0, 2.0], dtype=tf.float32), tf.constant([2.0, 1.0], dtype=tf.float32)]
        other_network_args = [tf.constant(2.0, dtype=tf.float32)]

        train_op, global_step, output_dict = create_train_op(
            optimizer, build_network_outputs, batched_network_args, other_network_args, available_devices=devices)
        self.assertEqual(2, len(output_dict.keys()))
        self.assertTrue('loss' in output_dict)
        self.assertEqual(2, len(variables))  # This ensures 2 sub-batches are made.
        self.assertNotEqual(variables[0], variables[1])  # This ensures the shared variables are the same.
        self.assertEqual(devices[0], losses[0].device)
        self.assertEqual(devices[1], losses[1].device)
        self.sess.run(tf.global_variables_initializer())

        loss = self.sess.run(output_dict['loss'].first())
        self.assertEqual(6.0, loss)

        self.sess.run(train_op)
        step, loss, variable = self.sess.run(
            [global_step, output_dict['loss'].first(), output_dict['variable'].first()])
        self.assertAlmostEqual(3.0, loss, places=5)
        self.assertAlmostEqual(0.5, variable, places=5)
        self.assertEqual(1, step)

        variable_1, variable_2 = self.sess.run(variables)
        self.assertAlmostEqual(0.0, variable_1, places=5)
        self.assertAlmostEqual(1.0, variable_2, places=5)

    def test_add_update_ops_single_device(self):
        variables = []
        return_dict = {}
        devices = None  # Default device.
        tf.get_default_graph().clear_collection(tf.GraphKeys.UPDATE_OPS)

        def build_network_outputs():
            with tf.variable_scope('variables', reuse=tf.AUTO_REUSE):
                one = tf.constant(1.0, dtype=tf.float32)
                variable = tf.get_variable('add_update_ops_single_device', initializer=one, trainable=True,
                                           dtype=tf.float32)
                variable2 = tf.get_variable('add_update_ops_single_device2', initializer=one, trainable=False,
                                            dtype=tf.float32)
            variables.append(variable)
            variables.append(variable2)
            assign_op = tf.assign(variable2, variable2 + 1.0)
            tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, assign_op)
            loss = variable * 2.0
            return_dict['loss'] = TensorIO([loss])
            return_dict['variable'] = TensorIO([variable])
            return_dict['variable2'] = TensorIO([variable2])
            return return_dict

        # Note this learning rate is set up such that the loss is 0.0 after 1 iteration.
        optimizer = tf.train.GradientDescentOptimizer(1.0 / 2.0)

        train_op, _, output_dict = create_train_op(
            optimizer, build_network_outputs, [], [], add_update_ops=True, available_devices=devices)
        self.assertEqual(output_dict, return_dict)  # This ensures the accumulating is bypassed.
        self.assertEqual(2, len(variables))
        self.assertEqual(3, len(output_dict.keys()))
        self.sess.run(tf.global_variables_initializer())
        self.assertEqual(2.0, self.sess.run(output_dict['loss'].first()))
        self.assertEqual(1.0, self.sess.run(output_dict['variable2'].first()))
        self.sess.run(train_op)
        self.assertEqual(0.0, self.sess.run(output_dict['loss'].first()))
        self.assertEqual(2.0, self.sess.run(output_dict['variable2'].first()))

    def test_add_update_ops_false_single_device(self):
        return_dict = {}
        devices = None  # Default device.
        tf.get_default_graph().clear_collection(tf.GraphKeys.UPDATE_OPS)

        def build_network_outputs():
            with tf.variable_scope('variables', reuse=tf.AUTO_REUSE):
                one = tf.constant(1.0, dtype=tf.float32)
                variable = tf.get_variable('add_update_ops_false_single_device', initializer=one, trainable=True,
                                           dtype=tf.float32)
                variable2 = tf.get_variable('add_update_ops_false_single_device2', initializer=one, trainable=False,
                                            dtype=tf.float32)
            assign_op = tf.assign(variable2, variable2 + 1.0)
            tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, assign_op)
            loss = variable * 2.0
            return_dict['loss'] = TensorIO([loss])
            return_dict['variable'] = TensorIO([variable])
            return_dict['variable2'] = TensorIO([variable2])
            return return_dict

        # Note this learning rate is set up such that the loss is 0.0 after 1 iteration.
        optimizer = tf.train.GradientDescentOptimizer(1.0 / 2.0)

        train_op, _, output_dict = create_train_op(
            optimizer, build_network_outputs, [], [], add_update_ops=False, available_devices=devices)
        self.assertEqual(output_dict, return_dict)  # This ensures the accumulating is bypassed.
        self.sess.run(tf.global_variables_initializer())
        self.assertEqual(2.0, self.sess.run(output_dict['loss'].first()))
        self.assertEqual(1.0, self.sess.run(output_dict['variable2'].first()))
        self.sess.run(train_op)
        self.assertEqual(0.0, self.sess.run(output_dict['loss'].first()))
        self.assertEqual(1.0, self.sess.run(output_dict['variable2'].first()))

    def test_add_update_ops_multiple_devices(self):
        variables = []
        return_dict = {}
        devices = self.get_devices_for_testing()
        tf.get_default_graph().clear_collection(tf.GraphKeys.UPDATE_OPS)

        def build_network_outputs(first, second):
            with tf.variable_scope('test_add_update_ops_multiple_devices', reuse=tf.AUTO_REUSE):
                one = tf.constant(1.0, dtype=tf.float32)
                variable = tf.get_variable('add_update_ops_multiple_devices', initializer=one, trainable=True,
                                           dtype=tf.float32)
                variable2 = tf.get_variable('add_update_ops_multiple_devices2', initializer=one, trainable=False,
                                            dtype=tf.float32)
            variables.append(variable)
            variables.append(variable2)
            assign_op = tf.assign(variable2, variable2 + 1.0)
            tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, assign_op)
            loss = variable * 2.0
            return_dict['loss'] = TensorIO([loss])
            return_dict['variable'] = TensorIO([variable])
            return_dict['variable2'] = TensorIO([variable2])
            return return_dict

        # Note this learning rate is set up such that the loss is 0.0 after 1 iteration.
        optimizer = tf.train.GradientDescentOptimizer(1.0 / 2.0)
        batched_network_args = [tf.constant([1.0, 2.0], dtype=tf.float32), tf.constant([2.0, 1.0], dtype=tf.float32)]

        train_op, _, output_dict = create_train_op(
            optimizer, build_network_outputs, batched_network_args, [], add_update_ops=True, available_devices=devices)
        self.assertNotEqual(output_dict, return_dict)  # This ensures the accumulating is not bypassed.
        self.assertEqual(4, len(variables))
        self.assertEqual(variables[0], variables[2])
        self.assertEqual(variables[1], variables[3])
        self.assertEqual(3, len(output_dict.keys()))
        self.sess.run(tf.global_variables_initializer())
        self.assertEqual(2.0, self.sess.run(output_dict['loss'].first()))
        self.assertEqual(1.0, self.sess.run(output_dict['variable2'].first()))
        self.sess.run(train_op)
        self.assertEqual(0.0, self.sess.run(output_dict['loss'].first()))
        self.assertEqual(2.0, self.sess.run(output_dict['variable2'].first()))

    def test_add_update_ops_false_multiple_devices(self):
        return_dict = {}
        devices = self.get_devices_for_testing()
        tf.get_default_graph().clear_collection(tf.GraphKeys.UPDATE_OPS)

        def build_network_outputs(first, second):
            with tf.variable_scope('variables', reuse=tf.AUTO_REUSE):
                one = tf.constant(1.0, dtype=tf.float32)
                variable = tf.get_variable('add_update_ops_false_multiple_devices', initializer=one, trainable=True,
                                           dtype=tf.float32)
                variable2 = tf.get_variable('add_update_ops_false_multiple_devices2', initializer=one, trainable=False,
                                            dtype=tf.float32)
            assign_op = tf.assign(variable2, variable2 + 1.0)
            tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, assign_op)
            loss = variable * 2.0
            return_dict['loss'] = TensorIO([loss])
            return_dict['variable'] = TensorIO([variable])
            return_dict['variable2'] = TensorIO([variable2])
            return return_dict

        # Note this learning rate is set up such that the loss is 0.0 after 1 iteration.
        optimizer = tf.train.GradientDescentOptimizer(1.0 / 2.0)
        batched_network_args = [tf.constant([1.0, 2.0], dtype=tf.float32), tf.constant([2.0, 1.0], dtype=tf.float32)]

        train_op, _, output_dict = create_train_op(
            optimizer, build_network_outputs, batched_network_args, [], add_update_ops=False, available_devices=devices)
        self.assertNotEqual(output_dict, return_dict)  # This ensures the accumulating is not bypassed.
        self.sess.run(tf.global_variables_initializer())
        self.assertEqual(2.0, self.sess.run(output_dict['loss'].first()))
        self.assertEqual(1.0, self.sess.run(output_dict['variable2'].first()))
        self.sess.run(train_op)
        self.assertEqual(0.0, self.sess.run(output_dict['loss'].first()))
        self.assertEqual(1.0, self.sess.run(output_dict['variable2'].first()))

    def test_multiple_optimizers_single_device(self):
        return_dict = {}
        devices = None  # Default device.

        def build_network_outputs():
            with tf.variable_scope('test_multiple_optimizers_single_device', reuse=tf.AUTO_REUSE):
                one = tf.constant(1.0, dtype=tf.float32)
                variable = tf.get_variable('var1', initializer=one, trainable=True,
                                           dtype=tf.float32)
                variable2 = tf.get_variable('var2', initializer=one, trainable=True,
                                            dtype=tf.float32)
            loss1 = variable * 2.0
            loss2 = variable2 * 2.0
            return_dict['loss'] = TensorIO([loss1, loss2])
            return_dict['variable'] = TensorIO([variable, variable2])
            return return_dict

        # Note this learning rate is set up such that the loss is 0.0 after 1 iteration.
        optimizers = [tf.train.GradientDescentOptimizer(1.0 / 2.0), tf.train.GradientDescentOptimizer(1.0 / 2.0)]

        train_ops, global_steps, output_dict = create_train_op(
            optimizers, build_network_outputs, [], [], add_update_ops=False, available_devices=devices)
        self.assertEqual(2, len(train_ops))
        self.assertEqual(2, len(global_steps))
        self.assertEqual(output_dict, return_dict)  # This ensures the accumulating is not bypassed.
        self.sess.run(tf.global_variables_initializer())
        self.assertEqual(2.0, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertEqual(2.0, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertEqual(1.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertEqual(1.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.sess.run(train_ops[0])
        self.assertEqual(0.0, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertEqual(0.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertEqual(2.0, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertEqual(1.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(1, self.sess.run(global_steps[0]))
        self.assertEqual(0, self.sess.run(global_steps[1]))
        self.sess.run(train_ops[1])
        self.assertEqual(0.0, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertEqual(0.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertEqual(0.0, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertEqual(0.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(1, self.sess.run(global_steps[0]))
        self.assertEqual(1, self.sess.run(global_steps[1]))
        self.sess.run(train_ops)
        self.assertEqual(-2.0, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertEqual(-1.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertEqual(-2.0, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertEqual(-1.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(2, self.sess.run(global_steps[0]))
        self.assertEqual(2, self.sess.run(global_steps[1]))

    def test_multiple_optimizers_multiple_devices(self):
        return_dict = {}
        devices = None  # Default device.

        def build_network_outputs(first, second):
            with tf.variable_scope('test_multiple_optimizers_multiple_devices', reuse=tf.AUTO_REUSE):
                one = tf.constant(1.0, dtype=tf.float32)
                variable = tf.get_variable('var1', initializer=one, trainable=True,
                                           dtype=tf.float32)
                variable2 = tf.get_variable('var2', initializer=one, trainable=True,
                                            dtype=tf.float32)
            loss1 = tf.reduce_mean(variable * first)
            loss2 = tf.reduce_mean(variable2 * second)
            return_dict['loss'] = TensorIO([loss1, loss2], tensor_type=TensorIO.AVERAGED_SCALAR)
            return_dict['variable'] = TensorIO([variable, variable2])
            return_dict['first'] = TensorIO([first], tensor_type=TensorIO.BATCH)
            return_dict['second'] = TensorIO([second], tensor_type=TensorIO.BATCH)
            return return_dict

        # Note this learning rate is set up such that the loss is 0.0 after 1 iteration.
        # The second optimizer is set up to learn half as quickly.
        optimizers = [tf.train.GradientDescentOptimizer(2.0 / 3.0), tf.train.GradientDescentOptimizer(2.0 / 6.0)]
        batched_network_args = [tf.constant([1.0, 2.0], dtype=tf.float32), tf.constant([2.0, 1.0], dtype=tf.float32)]

        train_ops, global_steps, output_dict = create_train_op(
            optimizers, build_network_outputs, batched_network_args, [], add_update_ops=False,
            available_devices=devices)
        self.assertEqual(2, len(train_ops))
        self.assertEqual(2, len(global_steps))
        self.assertEqual(output_dict, return_dict)  # This ensures the accumulating is bypassed.
        self.sess.run(tf.global_variables_initializer())
        self.assertEqual(1.5, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertEqual(1.5, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertEqual(1.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertEqual(1.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertTrue(np.allclose(np.asarray([1.0, 2.0], dtype=np.float32),
                                    self.sess.run(output_dict['first'].first())))
        self.assertTrue(np.allclose(np.asarray([2.0, 1.0], dtype=np.float32),
                                    self.sess.run(output_dict['second'].first())))
        self.sess.run(train_ops[0])
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertAlmostEqual(1.5, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertAlmostEqual(1.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(1, self.sess.run(global_steps[0]))
        self.assertEqual(0, self.sess.run(global_steps[1]))
        self.sess.run(train_ops[1])
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertAlmostEqual(1.5 / 2.0, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertAlmostEqual(0.5, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(1, self.sess.run(global_steps[0]))
        self.assertEqual(1, self.sess.run(global_steps[1]))
        self.sess.run(train_ops)
        self.assertAlmostEqual(-1.5, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertAlmostEqual(-1.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(2, self.sess.run(global_steps[0]))
        self.assertEqual(2, self.sess.run(global_steps[1]))

    def test_multiple_optimizers_var_list_single_device(self):
        return_dict = {}
        devices = None  # Default device.

        def build_network_outputs(first, second):
            one = tf.constant(1.0, dtype=tf.float32)
            with tf.variable_scope('test_multiple_optimizers_var_list_single_device_1', reuse=tf.AUTO_REUSE):
                variable = tf.get_variable('var', initializer=one, trainable=True, dtype=tf.float32)
            with tf.variable_scope('test_multiple_optimizers_var_list_single_device_2', reuse=tf.AUTO_REUSE):
                variable2 = tf.get_variable('var', initializer=one, trainable=True, dtype=tf.float32)
            loss1 = tf.reduce_mean((variable + variable2) * first)
            loss2 = tf.reduce_mean((variable + variable2) * second)
            return_dict['loss'] = TensorIO([loss1, loss2], tensor_type=TensorIO.AVERAGED_SCALAR)
            return_dict['variable'] = TensorIO([variable, variable2])
            return_dict['first'] = TensorIO([first], tensor_type=TensorIO.BATCH)
            return_dict['second'] = TensorIO([second], tensor_type=TensorIO.BATCH)
            return return_dict

        # Note this learning rate is set up such that the loss is 0.0 after 1 iteration.
        # The second optimizer is set up to learn half as quickly.
        optimizers = [tf.train.GradientDescentOptimizer(2.0 / 3.0), tf.train.GradientDescentOptimizer(2.0 / 6.0)]
        batched_network_args = [tf.constant([1.0, 2.0], dtype=tf.float32), tf.constant([2.0, 1.0], dtype=tf.float32)]
        var_list_scopes = [['test_multiple_optimizers_var_list_single_device_1'],
                           ['test_multiple_optimizers_var_list_single_device_2']]

        train_ops, global_steps, output_dict = create_train_op(
            optimizers, build_network_outputs, batched_network_args, [], add_update_ops=False,
            available_devices=devices, var_list_scopes=var_list_scopes)
        self.assertEqual(2, len(train_ops))
        self.assertEqual(2, len(global_steps))
        self.assertEqual(output_dict, return_dict)  # This ensures the accumulating is bypassed.
        self.sess.run(tf.global_variables_initializer())
        self.assertEqual(3.0, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertEqual(3.0, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertEqual(1.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertEqual(1.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertTrue(np.allclose(np.asarray([1.0, 2.0], dtype=np.float32),
                                    self.sess.run(output_dict['first'].first())))
        self.assertTrue(np.allclose(np.asarray([2.0, 1.0], dtype=np.float32),
                                    self.sess.run(output_dict['second'].first())))
        self.sess.run(train_ops[0])
        self.assertAlmostEqual(1.5, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertAlmostEqual(1.5, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertAlmostEqual(1.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(1, self.sess.run(global_steps[0]))
        self.assertEqual(0, self.sess.run(global_steps[1]))
        self.sess.run(train_ops[1])
        self.assertAlmostEqual(1.5 / 2.0, self.sess.run(output_dict['loss'].tensors[0]), places=5)
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertAlmostEqual(1.5 / 2.0, self.sess.run(output_dict['loss'].tensors[1]), places=5)
        self.assertAlmostEqual(0.5, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(1, self.sess.run(global_steps[0]))
        self.assertEqual(1, self.sess.run(global_steps[1]))
        self.sess.run(train_ops)
        self.assertAlmostEqual(-1.5, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertAlmostEqual(-1.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertAlmostEqual(-1.5, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(2, self.sess.run(global_steps[0]))
        self.assertEqual(2, self.sess.run(global_steps[1]))

    def test_multiple_optimizers_var_list_multiple_devices(self):
        return_dict = {}
        devices = self.get_devices_for_testing()

        def build_network_outputs(first, second):
            one = tf.constant(1.0, dtype=tf.float32)
            with tf.variable_scope('test_multiple_optimizers_var_list_multiple_devices_1', reuse=tf.AUTO_REUSE):
                variable = tf.get_variable('var', initializer=one, trainable=True, dtype=tf.float32)
            with tf.variable_scope('test_multiple_optimizers_var_list_multiple_devices_2', reuse=tf.AUTO_REUSE):
                variable2 = tf.get_variable('var', initializer=one, trainable=True, dtype=tf.float32)
            loss1 = tf.reduce_mean((variable + variable2) * first)
            loss2 = tf.reduce_mean((variable + variable2) * second)
            return_dict['loss'] = TensorIO([loss1, loss2], tensor_type=TensorIO.AVERAGED_SCALAR)
            return_dict['variable'] = TensorIO([variable, variable2])
            return_dict['first'] = TensorIO([first], tensor_type=TensorIO.BATCH)
            return_dict['second'] = TensorIO([second], tensor_type=TensorIO.BATCH)
            return return_dict

        # Note this learning rate is set up such that the loss is 0.0 after 1 iteration.
        # The second optimizer is set up to learn half as quickly.
        optimizers = [tf.train.GradientDescentOptimizer(2.0 / 3.0), tf.train.GradientDescentOptimizer(2.0 / 6.0)]
        batched_network_args = [tf.constant([1.0, 2.0], dtype=tf.float32), tf.constant([2.0, 1.0], dtype=tf.float32)]
        var_list_scopes = [['test_multiple_optimizers_var_list_multiple_devices_1'],
                           ['test_multiple_optimizers_var_list_multiple_devices_2']]

        train_ops, global_steps, output_dict = create_train_op(
            optimizers, build_network_outputs, batched_network_args, [], add_update_ops=False,
            available_devices=devices, var_list_scopes=var_list_scopes)
        self.assertEqual(2, len(train_ops))
        self.assertEqual(2, len(global_steps))
        self.assertNotEqual(output_dict, return_dict)  # This ensures the accumulating is not bypassed.
        self.sess.run(tf.global_variables_initializer())
        self.assertEqual(3.0, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertEqual(3.0, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertEqual(1.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertEqual(1.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertTrue(np.allclose(np.asarray([1.0, 2.0], dtype=np.float32),
                                    self.sess.run(output_dict['first'].first())))
        self.assertTrue(np.allclose(np.asarray([2.0, 1.0], dtype=np.float32),
                                    self.sess.run(output_dict['second'].first())))
        self.sess.run(train_ops[0])
        self.assertAlmostEqual(1.5, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertAlmostEqual(1.5, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertAlmostEqual(1.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(1, self.sess.run(global_steps[0]))
        self.assertEqual(0, self.sess.run(global_steps[1]))
        self.sess.run(train_ops[1])
        self.assertAlmostEqual(1.5 / 2.0, self.sess.run(output_dict['loss'].tensors[0]), places=5)
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertAlmostEqual(1.5 / 2.0, self.sess.run(output_dict['loss'].tensors[1]), places=5)
        self.assertAlmostEqual(0.5, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(1, self.sess.run(global_steps[0]))
        self.assertEqual(1, self.sess.run(global_steps[1]))
        self.sess.run(train_ops)
        self.assertAlmostEqual(-1.5, self.sess.run(output_dict['loss'].tensors[0]))
        self.assertAlmostEqual(-1.0, self.sess.run(output_dict['variable'].tensors[0]))
        self.assertAlmostEqual(-1.5, self.sess.run(output_dict['loss'].tensors[1]))
        self.assertAlmostEqual(0.0, self.sess.run(output_dict['variable'].tensors[1]))
        self.assertEqual(2, self.sess.run(global_steps[0]))
        self.assertEqual(2, self.sess.run(global_steps[1]))

    @staticmethod
    def get_devices_for_testing():
        available_gpus = get_available_gpus()
        if len(available_gpus) == 0:
            devices = ['/device:CPU:0', '/device:CPU:0']
        elif len(available_gpus) == 1:
            devices = [available_gpus[0], '/device:CPU:0']
        else:
            devices = [available_gpus[0], available_gpus[1]]
        return devices


class TestListAccumulation(unittest.TestCase):
    def test_accumulate_once(self):
        items_list = []
        items = [1, 2]
        accumulate_list_into(items, items_list)
        self.assertListEqual([[1], [2]], items_list)

    def test_accumulate_twice(self):
        items_list = []
        items = [1, 2]
        accumulate_list_into(items, items_list)
        items = [3, 4]
        accumulate_list_into(items, items_list)
        self.assertListEqual([[1, 3], [2, 4]], items_list)

    def test_empty(self):
        items_list = []
        items = []
        accumulate_list_into(items, items_list)
        self.assertListEqual([], items_list)

    def test_single(self):
        items_list = []
        items = [1]
        accumulate_list_into(items, items_list)
        items = [2]
        accumulate_list_into(items, items_list)
        self.assertListEqual([[1, 2]], items_list)


if __name__ == '__main__':
    unittest.main()
