import errno
import os
import re
import shutil
import tensorflow as tf
import glob
import ntpath


def create_shard_ranges(iter_range, shard_size):
    """
    :param iter_range: List of increasing integers representing a range to iterate over.
    :param shard_size: Maximum shard size.
    :return: List of ranges.
    """
    if shard_size == 0:
        return []

    sharded_iter_ranges = []
    num_shards = int(len(iter_range) / shard_size) + 1
    for shard_id in range(num_shards):
        start_idx = shard_id * shard_size
        end_idx = min((shard_id + 1) * shard_size, len(iter_range))
        if start_idx - end_idx == 0:
            break
        sharded_iter_ranges.append(iter_range[start_idx:end_idx])

    return sharded_iter_ranges


def tf_int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def tf_bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def tf_bytes_list_feature(bytes_list):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=bytes_list))


def silently_remove_file(filename):
    try:
        os.remove(filename)
    except OSError as e:
        # errno.ENOENT = no such file or directory.
        if e.errno != errno.ENOENT:
            raise


def silently_remove_directory(directory):
    shutil.rmtree(directory, ignore_errors=True)


def get_group_and_idx(filepath, use_underscore_sep=True):
    """
    Example:
        get_group_and_idx('ha/foo_10.jpg') -> 'foo', 10
        get_group_and_idx('ha/foo10.jpg', use_underscore_sep=False) -> 'foo', 10

    :param filename: Path to the file. Should have format as shown in the example.
                     If this is not conformed to exactly, the return values will both be None.
    :param use_underscore_sep: Whether the group and idx are separated by an underscore.
                               If you are not sure, it is safer to set this to False
                               (i.e if you're wrong then the only consequence is an extra underscore in the group name).
    :return: group: Str. Prefix of filename. Empty string '' if there is no group.
             idx: Str. Id of filename.
    """
    base_name = os.path.basename(filepath)
    if use_underscore_sep:
        regex = re.compile('(.+)_(\d+)\..+')
    else:
        regex = re.compile('([^\d]*)(\d+)\..+')
    matches = regex.match(base_name)
    if matches is None:
        return None, None
    group = matches.group(1)
    idx = matches.group(2)
    return group, idx


def group_files_by_directory(files):
    """
    Transforms a flat list of file paths and groups them into a 2D list where the first dimension is the directory
    and the second dimension are the files in the directory.
    :param files: List of str.
    :return: List of list of str.
    """
    if len(files) == 0:
        return []
    directories = [[]]
    files.sort()
    for i in range(len(files) - 1):
        directory_a = os.path.dirname(files[i])
        directory_b = os.path.dirname(files[i + 1])
        directories[-1].append(files[i])
        if directory_a != directory_b:
            directories.append([])
    # Last file belongs to the last directory.
    directories[-1].append(files[-1])
    return directories


def shard_list(to_shard, shard_size):
    """
    Splits a list into a list of lists such that each sublist has items_per_shard items. The last sublist may contain
    less than items_per_shard items.
    :param to_shard: List.
    :param shard_size: Int.
    :return: List of lists.
    """
    sharded = []
    for i, item in enumerate(to_shard):
        if i % shard_size == 0:
            sharded.append([])
        sharded[-1].append(item)
    return sharded


def get_sequence_dict(subdir, recursive=True, use_underscore_sep=True):
    """
    :param subdir: Str. The directory to scan.
    :param recursive: Bool. Whether to look for file paths recursively.
    :param use_underscore_sep: Bool. Whether file names have sequence name and idx separated by an underscore.
    :return: A dictionary of (sequence_name, idx_list) key-value pairs, where idx_list is a list of
             indices encountered for this sequence. The filenames are e.g sequence_name + '_' + idx_list[0].
             See get_group_and_idx for how the sequence_name and indices are extracted from the filepaths.
    """
    # Group files according to their prefix.
    file_names = []
    supported_extensions = ['.jpg', '.png']
    for ext in supported_extensions:
        pattern = os.path.join(subdir, '**', '*' + ext) if recursive else os.path.join(subdir, '*' + ext)
        file_names += glob.glob(pattern, recursive=recursive)
    groups = {}
    for name in file_names:
        group, idx = get_group_and_idx(name, use_underscore_sep=use_underscore_sep)
        if group is None:
            continue
        if group in groups:
            groups[group].append(idx)
        else:
            groups[group] = [idx]

    for key, value in groups.items():
        groups[key].sort()
    return groups


def get_triplet_filepaths(directory, use_underscore_sep=True):
    """
    :param directory: Input directory to scan.
    :return: A list of tuples (image_a_name, image_b_name, image_c_name).
    """
    triplets = []
    subdirs = os.walk(directory, followlinks=True)
    subdirs = [x[0] for x in subdirs]
    name_sep = '_' if use_underscore_sep else ''
    for subdir in subdirs:
        groups = get_sequence_dict(subdir, recursive=False, use_underscore_sep=use_underscore_sep)
        keys = list(groups.keys())
        keys.sort()
        for group_name in keys:
            indices = groups[group_name]
            num_triplets = len(indices) - 2

            # Get the triplet file names.
            for i in range(num_triplets):
                path_a = glob.glob(os.path.join(subdir, group_name + name_sep + str(indices[i]) + '*'))
                path_b = glob.glob(os.path.join(subdir, group_name + name_sep + str(indices[i + 1]) + '*'))
                path_c = glob.glob(os.path.join(subdir, group_name + name_sep + str(indices[i + 2]) + '*'))
                assert len(path_a) == 1
                assert len(path_b) == 1
                assert len(path_c) == 1
                triplets.append((path_a[0], path_b[0], path_c[0]))
    return triplets
