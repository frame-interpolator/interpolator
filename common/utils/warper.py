import tensorflow as tf
from common.forward_warp.forward_warp_interp import forward_warp_interp
from common.forward_warp.forward_warp_bidirectional import forward_warp_bidirectional


class Warper:
    """
    A class for warping Numpy images using TensorFlow ops.
    """
    def __init__(self, sess=None, use_middlebury=False, fill_holes=True):
        """
        :param sess: Tf Session.
        :param use_middlebury: Whether to use the Middlebury warp.
        :param fill_holes: Whether to fill holes for the warps.
        """
        if sess is None:
            config_proto = tf.ConfigProto()
            config_proto.gpu_options.allow_growth = True
            self.sess = tf.Session()
        else:
            self.sess = sess
        self.use_middlebury = bool(use_middlebury)

        # Create the forward pass graph.
        self.img_0_tensor = tf.placeholder(dtype=tf.float32, shape=(1, None, None, None))
        self.img_1_tensor = tf.placeholder(dtype=tf.float32, shape=(1, None, None, None))
        self.flow_0_1_tensor = tf.placeholder(dtype=tf.float32, shape=(1, None, None, 2))
        self.flow_1_0_tensor = tf.placeholder(dtype=tf.float32, shape=(1, None, None, 2))
        self.t_tensor = tf.placeholder(dtype=tf.float32, shape=(1,))
        if self.use_middlebury:
            self.warp_tensor, _, _, _ = forward_warp_interp(self.img_0_tensor, self.img_1_tensor, self.flow_0_1_tensor,
                                                   self.t_tensor, fill_holes=fill_holes)
        else:
            warps = forward_warp_bidirectional(self.img_0_tensor, self.img_1_tensor, self.flow_0_1_tensor,
                                               self.flow_1_0_tensor, self.t_tensor, fill_holes=fill_holes)
            self.warp_0_1_tensor, self.warp_1_0_tensor, _ = warps

    def get_bidir_warps(self, img_0, img_1, flow_0_1, flow_1_0, t):
        """
        :param img_0: Numpy array of shape [H, W, C].
        :param img_1: Numpy array of shape [H, W, C].
        :param flow_0_1: Numpy array of shape [H, W, 2].
        :param flow_1_0: Numpy array of shape [H, W, 2].
        :param t: scalar.
        :return: warp_0_1, warp_1_0. The images warped to the target time.
        """
        if self.use_middlebury:
            warp_0_1 = self.sess.run(self.warp_tensor, feed_dict={self.img_0_tensor: [img_0],
                                                                  self.img_1_tensor: [img_1],
                                                                  self.flow_0_1_tensor: [flow_0_1],
                                                                  self.flow_1_0_tensor: [flow_1_0],
                                                                  self.t_tensor: [t]})[0][0]
            warp_1_0 = self.sess.run(self.warp_tensor, feed_dict={self.img_0_tensor: [img_1],
                                                                  self.img_1_tensor: [img_0],
                                                                  self.flow_0_1_tensor: [flow_1_0],
                                                                  self.flow_1_0_tensor: [flow_0_1],
                                                                  self.t_tensor: [1.0 - t]})[0][0]
        else:
            feed_dict = {
                self.img_0_tensor: [img_0],
                self.img_1_tensor: [img_1],
                self.flow_0_1_tensor: [flow_0_1],
                self.flow_1_0_tensor: [flow_1_0],
                self.t_tensor: [t]
            }
            warps = self.sess.run([self.warp_0_1_tensor, self.warp_1_0_tensor], feed_dict=feed_dict)
            warp_0_1, warp_1_0 = warps
            warp_0_1 = warp_0_1[0]
            warp_1_0 = warp_1_0[0]
        return warp_0_1, warp_1_0
