import numpy as np
import os
import glob
import json
import tensorflow as tf
from common.utils import img
from common.utils.misc import sorted_list_insert
from common.utils.data import get_sequence_dict
from common.utils.interp_eval import InterpEvaluator, Interpolator


class EvaluatedMetrics:
    """
    Keeps track of a number of interpolation quality metrics and provides methods for reading to and writing to disk.
    """
    def __init__(self, model_name=None, ssim_steps=[], psnr_steps=[], lpips_steps=[],
                 num_evaluated=[]):
        """
        :param model_name: Str. The name of the model.
        :param ssim_steps: List of length steps. The SSIM at each evaluated time step.
        :param psnr_steps: List of length steps. The PSNR at each evaluated time step.
        :param lpips_steps: List of length steps. The LPIPS at each evaluated time step.
        :param num_evaluated: List of length steps. The number of evaluated images at each time step.
        """
        assert isinstance(num_evaluated, list)
        self.model_name = model_name
        self.ssim_steps = np.array(ssim_steps)
        self.psnr_steps = np.array(psnr_steps)
        self.lpips_steps = np.array(lpips_steps)
        self.num_evaluated = np.array(num_evaluated)

    def write_to_json(self, path):
        """
        :param path: Path to the json file that we will write to. If this is None then we will dump to stdout.
        """
        avg_ssim = np.sum(self.ssim_steps * self.num_evaluated) / np.sum(self.num_evaluated)
        avg_psnr = np.sum(self.psnr_steps * self.num_evaluated) / np.sum(self.num_evaluated)
        avg_lpips = np.sum(self.lpips_steps * self.num_evaluated) / np.sum(self.num_evaluated)
        json_obj = {
            'model_name': self.model_name,
            'time_varying_ssim': self.ssim_steps.tolist(),
            'time_varying_psnr': self.psnr_steps.tolist(),
            'time_varying_lpips': self.lpips_steps.tolist(),
            'num_evaluated': self.num_evaluated.tolist(),
            'avg_ssim': avg_ssim,
            'avg_psnr': avg_psnr,
            'avg_lpips': avg_lpips
        }
        if path is not None:
            with open(path, 'w') as outfile:
                json.dump(json_obj, outfile, sort_keys=True, indent=4)
        else:
            print(json.dumps(json_obj, sort_keys=True, indent=4))

    # TODO: This isn't used anywhere and is untested, but I might use it in a future script (so it's left here for now).
    def load_from_json(self, path):
        with open(path, 'r') as infile:
            json_obj = json.load(infile)
            self.model_name = np.array(json_obj['model_name'])
            self.ssim_steps = np.array(json_obj['time_varying_ssim'])
            self.psnr_steps = np.array(json_obj['time_varying_psnr'])
            self.num_evaluated = np.array(json_obj['num_evaluated'])
            self.lpips_steps = np.array(json_obj['time_varying_lpips'])


class RankedTuples:
    """
    Keeps track of a list of (score, ((filename_1, filename_2, ...), (image_1, image_2, ...))) elements.
    The entries are sorted according to the value of score.
    Provides a method for writing this information to disk.
    """
    def __init__(self, max_num_ranked, is_ascending_order=True):
        self.max_num_ranked = max_num_ranked
        self.is_ascending_order = is_ascending_order
        self.sorted_list = []

    def append(self, score, file_names, images):
        """
        Appends info to the list, if it meets the requirements (i.e it is within max_num_ranked elements).
        :param score: Scalar. The score.
        :param file_names: List or tuple of file names.
        :param images: List or tuple of images corresponding to each filename.
        :return:
        """
        if self.is_ascending_order:
            score *= -1
        info_tuple = (tuple(file_names), tuple(images))
        assert len(info_tuple) == len(images)
        self.sorted_list = sorted_list_insert(self.sorted_list, score, info_tuple, max_len=self.max_num_ranked)

    def write_to_folder(self, output_dir, prefix=None):
        """
        :param output_dir: The directory to write the images to.
        :param prefix: Prefix for the file names.
        :return: None
        """
        if len(self.sorted_list) <= 0:
            return

        converted_list = self.sorted_list
        if self.is_ascending_order:
            converted_list = [(-elm[0], elm[1]) for elm in self.sorted_list]

        if not os.path.exists(output_dir):
            os.mkdir(output_dir)

        for elm in converted_list:
            score, info_tuple = elm
            file_names, images = info_tuple
            for i in range(len(file_names)):
                file_name = file_names[i]
                image = images[i]
                if prefix is not None:
                    file_name = prefix + file_name
                out_img_path = os.path.join(output_dir, file_name)
                img.write_image(out_img_path, image)


def get_metrics(synthesizer_model, flow_model, input_directory, percentage_eval, sess,
                num_inbetween_steps=1, use_underscore_sep=False, max_ranked=0, use_endpoints=True, verbose=False,
                use_lpips=False, use_flow_spline=False):
    """
    :param synthesizer_model: Loaded saved model for synthesis.
    :param flow_model: Loaded saved model for flow prediction.
    :param input_directory: The directory for which we will recursively evaluate image sequences.
    :param percentage_eval: Float from 0.0 to 100.0. The percentage of image sequences that we will evaluated.
    :param sess: A Tf session.
    :param num_inbetween_steps: Number of inbetweening steps for each sequence. 1 for contiguous triplet frames only.
                                Vimeo septuplet (7 frames per sequence) can allow up to a value of 5.
    :param use_underscore_sep: Whether the image names have an underscore separating the prefix from the sequence idx.
    :param max_ranked: The maximum number of images to save based on their ranking for metrics.
    :param use_endpoints: Whether to include the endpoints in the evaluation.
    :param verbose: Whether to print useful information such as evaluation progress.
    :param use_lpips: Whether to include the LPIPS metric (it's slower).
    :param use_flow_spline: Whether to spline the flow before feeding it to the synthesizer.
    :return: metrics: A Metrics object.
             ranked_tuples_dict: A dictionary of RankedTuples.
                                 Has best_ssim, best_psnr, worst_ssim, worst_psnr, best_lpips, worst_lpips.
    """
    assert isinstance(sess, tf.Session)
    n = num_inbetween_steps
    name_separator = '_' if use_underscore_sep else ''

    # Walk through each subdirectory and evaluate on frame triplets.
    # Keep some of the best and worst examples in lists for later.
    subdirs = [x[0] for x in os.walk(input_directory, followlinks=True)]
    best_ssim = RankedTuples(max_ranked, is_ascending_order=False)
    best_psnr = RankedTuples(max_ranked, is_ascending_order=False)
    worst_ssim = RankedTuples(max_ranked, is_ascending_order=True)
    worst_psnr = RankedTuples(max_ranked, is_ascending_order=True)
    total_ssim = 0.0
    total_psnr = 0.0
    total_lpips = 0.0
    num_evaluated = 0

    # Get LPIPS distance tensor. This will involve some downloading if it's the very first time.
    if verbose:
        print('Loading Evaluator...')
    evaluator = InterpEvaluator(use_lpips=use_lpips, sess=sess)
    interpolator = Interpolator(synthesizer_model, flow_model, sess=sess)

    # Same metrics as above but look at each individual time slot (i.e if n = 1, we have t = 0, t = 0.5, t = 1).
    num_steps = (n + 2) if use_endpoints else n
    steps_total_ssim = num_steps * [0.0]
    steps_total_psnr = num_steps * [0.0]
    steps_total_lpips = num_steps * [0.0]
    steps_num_evaluated = num_steps * [0.0]
    iter_amount = int(1.0 / (percentage_eval / 100.0))
    if verbose:
        print('There are a total of %d subdirectories and we are going to iterate over them with stride %d.' %
              (len(subdirs), iter_amount))
    for k in range(0, len(subdirs), iter_amount):
        subdir = subdirs[k]
        groups = get_sequence_dict(subdir, recursive=False, use_underscore_sep=use_underscore_sep)
        if len(groups) > 0 and verbose:
            print('Evaluating %d sequences in %s...' % (len(groups), subdir))
        for group_name, indices in groups.items():
            assert group_name is not None
            num_endpoint_pairs = len(indices) - (n + 1)
            if verbose:
                print('Evaluating sequence %s in directory %s, with %d endpoint pairs...'
                      % (group_name, subdir, num_endpoint_pairs))

            # Go through each pair of endpoint frames.
            for i in range(num_endpoint_pairs):
                first_idx = i
                second_idx = i + n + 1
                path_a_pattern = os.path.join(subdir, group_name + name_separator + indices[first_idx] + '*')
                path_c_pattern = os.path.join(subdir, group_name + name_separator + indices[second_idx] + '*')
                path_a = glob.glob(path_a_pattern)
                path_c = glob.glob(path_c_pattern)
                assert len(path_a) == 1, path_a_pattern
                assert len(path_c) == 1, path_c_pattern
                img_a = img.read_image(path_a[0], as_float=True)
                img_c = img.read_image(path_c[0], as_float=True)
                interpolator.set_end_frames_and_compute_flow(img_a, img_c, use_spline=use_flow_spline)

                # Evaluate at all the frames between the endpoints.
                for j in range(num_steps):
                    step = j if use_endpoints else j + 1
                    path_b_pattern = os.path.join(subdir, group_name + name_separator + indices[first_idx + step] + '*')
                    path_b = glob.glob(path_b_pattern)
                    assert len(path_b) == 1, path_b_pattern
                    img_b = img.read_image(path_b[0], as_float=True)
                    t = float(step) / (n + 1)
                    interpolated = interpolator.get_interpolated(t)

                    # Get the metrics.
                    psnr, ssim, lpips_dist = evaluator.get_eval_metrics(interpolated, img_b)
                    lpips_dist = 0 if lpips_dist is None else lpips_dist
                    num_evaluated += 1
                    total_ssim += ssim
                    total_psnr += psnr
                    total_lpips += lpips_dist
                    steps_total_ssim[j] += ssim
                    steps_total_psnr[j] += psnr
                    steps_total_lpips[j] += lpips_dist
                    steps_num_evaluated[j] += 1

                    # Insert into lists.
                    if n == 1 and step == 1:
                        rel_path = os.path.relpath(subdir, input_directory)
                        folder_str = '_'.join(rel_path.split(os.sep))
                        file_name_overlay = folder_str + '_' + group_name + '_overlay_' + indices[i + 1] + '.jpg'
                        file_name_pred = folder_str + '_' + group_name + '_pred_' + indices[i + 1] + '.jpg'
                        file_names = (file_name_pred, file_name_overlay)
                        images = (interpolated, 0.6 * img_a + 0.4 * img_c)
                        best_ssim.append(ssim, file_names, images)
                        best_psnr.append(psnr, file_names, images)
                        worst_ssim.append(ssim, file_names, images)
                        worst_psnr.append(psnr, file_names, images)

        if verbose:
            print('Processed sequence %d (of %d sequences).' % (k + 1, len(subdirs)))

    if num_evaluated > 0:
        steps_total_psnr = np.array(steps_total_psnr) / np.array(steps_num_evaluated)
        steps_total_ssim = np.array(steps_total_ssim) / np.array(steps_num_evaluated)
        steps_total_lpips = np.array(steps_total_lpips) / np.array(steps_num_evaluated)
        if not use_lpips:
            steps_total_lpips = []
        metrics = EvaluatedMetrics(model_name=None, ssim_steps=steps_total_ssim, psnr_steps=steps_total_psnr,
                                   lpips_steps=steps_total_lpips, num_evaluated=steps_num_evaluated)
    else:
        metrics = EvaluatedMetrics()
    ranked_tuples_dict = {
        'best_ssim': best_ssim,
        'best_psnr': best_psnr,
        'worst_ssim': worst_ssim,
        'worst_psnr': worst_psnr
    }
    return metrics, ranked_tuples_dict
