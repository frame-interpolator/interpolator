import tensorflow as tf
from tensorflow.python.client import device_lib


def get_available_gpus():
    """
    Gets a list of available GPU device names that can be used in "with tf.device(get_available_gpus()[i]):".
    :return: List of available GPU device names. I.e. ['/device:GPU:0', '/device:GPU:1'].
    """
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']


# Copied from https://github.com/simonmeister/UnFlow/blob/master/src/e2eflow/core/train.py.
# Commit bac9bbaf49be44b9e1c1f004fce4fb04b247763d.
def average_gradients(tower_grads_and_vars):
    """
    Calculate the average gradient for each shared variable across all towers.
    Note that this function provides a synchronization point across all towers.
    :param tower_grads_and_vars: List of lists of (gradient, variable) tuples. The outer list is over individual
                                 gradients. The inner list is over the gradient calculation for each tower.
    :return: List of pairs of (gradient, variable) where the gradient has been averaged across all towers.
    """
    averaged_grads = []
    for grad_and_vars in zip(*tower_grads_and_vars):
        # Note that each grad_and_vars looks like the following:
        #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
        grads = []
        for g, _ in grad_and_vars:
            if g is not None:
                # Add 0 dimension to the gradients to represent the tower.
                expanded_g = tf.expand_dims(g, 0)

                # Append on a 'tower' dimension which we will average over below.
                grads.append(expanded_g)
        if len(grads) != 0:
            # Average over the 'tower' dimension.
            grad = tf.concat(grads, axis=0)
            grad = tf.reduce_mean(grad, axis=0)

            # Keep in mind that the Variables are redundant because they are shared
            # across towers. So .. we will just return the first tower's pointer to
            # the Variable.
            v = grad_and_vars[0][1]
            grad_and_var = (grad, v)
            averaged_grads.append(grad_and_var)
    return averaged_grads


def accumulate_list_into(items, items_list):
    """
    The primary use case of this function is to accumulate multiple lists of tensors into a list of a list of tensors
    for joining together the same outputs from multiple GPUs.
    This is best explained by an example:
    items_list = []
    items = [1, 2]
    accumulate_list_into(items, items_list)  # items_list is now [[1], [2]].
    items = [3, 4]
    accumulate_list_into(items, items_list)  # items_list is now [[1, 3], [2, 4]].
    :param items: List of items.
    :param items_list: Empty list.
    :return: Nothing.
    """
    for j, item in enumerate(items):
        items_list.append([item]) if len(items_list) < len(items) else items_list[j].append(item)


class TensorIO:
    AVERAGED_SCALAR = 0
    SUMMED_SCALAR = 1
    BATCH = 2

    def __init__(self, tensors, tensor_type=AVERAGED_SCALAR):
        """
        :param tensors: 1D list of tensors.
        :param type: Int. Can be either AVERAGED_SCALAR, SUMMED_SCALAR, or BATCH.
                     AVERAGED_SCALAR types are scalars that have been averaged together.
                     SUMMED_SCALAR types are scalars that have been summed together.
                     BATCH types are tensors with the 0th dimension as the batch.
        """
        assert isinstance(tensors, list)
        if len(tensors) > 0:
            assert not isinstance(tensors[0], list)
        self.tensors = tensors
        self.tensor_type = tensor_type

    def first(self):
        """
        :return: The first tensor or None if empty.
        """
        if len(self.tensors) == 0:
            return None
        assert not isinstance(self.tensors[0], list)
        return self.tensors[0]


class AccumulatingTensorIO:
    def __init__(self, tensor_type=TensorIO.AVERAGED_SCALAR):
        """
        :param tensors: 2D array of tensors.
        :param tensor_type: Int. Can be either AVERAGED_SCALAR, SUMMED_SCALAR, or BATCH.
                            AVERAGED_SCALAR types will be averaged into a single scalar.
                            SUMMED_SCALAR types will be summed into a single scalar.
                            BATCH types will be concatenated along the 0th (batch) axis.
        """
        self.tensors = []
        self.tensor_type = tensor_type

    def accumulate(self):
        """
        Accumulates the tensors according to the type.
        :return: TensorIO.
        """
        tensors = []
        if len(self.tensors) > 0:
            assert isinstance(self.tensors[0], list)
            if self.tensor_type == TensorIO.AVERAGED_SCALAR:
                tensors = [tf.reduce_mean(tf.stack(item)) for item in self.tensors]
            elif self.tensor_type == TensorIO.SUMMED_SCALAR:
                tensors = [tf.reduce_sum(tf.stack(item)) for item in self.tensors]
            elif self.tensor_type == TensorIO.BATCH:
                tensors = [tf.concat(item, axis=0) for item in self.tensors]
        return TensorIO(tensors, self.tensor_type)

    def add(self, to_accumulate):
        """
        Adds a list of tensors for accumulation.
        :param to_accumulate: List of tensors.
        :return: Nothing.
        """
        assert isinstance(to_accumulate, list)
        accumulate_list_into(to_accumulate, self.tensors)


def _add_update_ops(train_op, verbose=False):
    """
    Adds all update ops to the train op.
    :param train_op: Existing train op.
    :param verbose: Bool.
    :return: Train op grouped with all update ops.
    """
    # Check if there are update ops.
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    if len(update_ops) > 0:
        if verbose:
            print('Found ' + str(len(update_ops)) + ' update ops.', flush=True)
        train_op = tf.group([train_op, update_ops], name='train_and_update_ops')
    return train_op


def _apply_gradients(optimizer, grads_and_vars, global_step, gradient_clipping_norm=None):
    """
    Applies gradients with optional gradient clipping.
    :param optimizer: Tensorflow optimizer.
    :param grads_and_vars: Grads and vars outputted by optimizer.compute_gradients().
    :param global_step: Scalar int variable.
    :param gradient_clipping_norm: None or float. If float, then the gradients will be clipped via global norm.
    :return: Train op, list of gradients (potentially after clipping).
    """
    grads, variables = zip(*grads_and_vars)
    if gradient_clipping_norm is not None:
        grads, _ = tf.clip_by_global_norm(grads, gradient_clipping_norm)
        grads_and_vars = zip(grads, variables)
    train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)
    return train_op, list(grads)


def _get_var_list(scopes, verbose=False):
    """
    :param scopes: List of scope names (strings).
    :param verbose: Bool.
    :return: List of trainable vars under the scope. No duplicates in the list.
    """
    vars = set()
    for scope in scopes:
        for var in tf.trainable_variables(scope=scope):
            vars.add(var)
    vars_list = [var for var in vars]
    if verbose:
        print('Found ' + str(len(vars_list)) + ' vars under the scopes: ' + str(scopes))
    return vars_list


def _create_train_op_single_device(optimizers, build_network_outputs, batched_network_args, other_network_args,
                                   available_devices=None, gradient_clipping_norm=None, add_update_ops=False,
                                   var_list_scopes=None, verbose=False):
    """
    See docstring for create_train_op.
    """
    if available_devices is None:
        if verbose:
            print('Creating single device train op for the default device.')
        outputs = build_network_outputs(*(batched_network_args + other_network_args))
    else:
        device_name = available_devices[0]
        if verbose:
            print('Creating single device train op for', device_name)
        with tf.device(device_name):
            outputs = build_network_outputs(*(batched_network_args + other_network_args))
    assert isinstance(outputs, dict)

    with tf.variable_scope('train'):
        global_steps = []
        train_ops = []
        assert len(outputs['loss'].tensors) == len(optimizers), 'Number of losses must be the same as optimizers.'
        for o, optimizer in enumerate(optimizers):
            global_step = tf.Variable(initial_value=0, trainable=False, dtype=tf.int32, name='global_step')
            global_steps.append(global_step)
            var_list = None if var_list_scopes is None else _get_var_list(var_list_scopes[o], verbose=verbose)
            grads_and_vars = optimizer.compute_gradients(outputs['loss'].tensors[o], var_list=var_list)
            train_op, _ = _apply_gradients(optimizer, grads_and_vars, global_step,
                                           gradient_clipping_norm=gradient_clipping_norm)
            if add_update_ops:
                train_op = _add_update_ops(train_op, verbose=verbose)
            train_ops.append(train_op)

    if len(optimizers) == 1:
        return train_ops[0], global_steps[0], outputs
    return train_ops, global_steps, outputs


def create_train_op(optimizers, build_network_outputs, batched_network_args, other_network_args, available_devices=None,
                    gradient_clipping_norm=None, add_update_ops=False, var_list_scopes=None, verbose=False):
    """
    :param optimizers: Tensorflow optimizer or list of optimizers. For example, tf.train.AdamOptimizer(3e-4).
                       If there are multiple optimizers, there are expected to be the same number of losses.
    :param build_network_outputs: A callback to build the network's outputs. It is expected that variable sharing is
                                  enabled under this callback's variable scope.
                                  The first len(batched_network_args) args are batched_network_args and the next
                                  len(other_network_args) are other_network_args. In python speak, this function will be
                                  called like so: build_network_outputs(*(batched_network_args + other_network_args))
                                  This must return a dictionary in the following format:
                                      key: Str<variable name>
                                      value: TensorIO
                                  It is expected that one of the keys is 'loss' and that it corresponds to a scalar
                                  tensor. Thus, the minimum output is {'loss': TensorIO([loss_tensor])}.
    :param batched_network_args: List of Tensors. These inputs will be sliced into sub-batches to be fed into
                                 different GPU devices. It is assumed that dimension 0 is the batch dimension and that
                                 all Tensors here have the same batch size.
    :param other_network_args: List. These can be anything. Each device will receive the same copy of these args.
    :param available_devices: List of str. I.e. ['/device:GPU:0', '/device:GPU:1']. If there is only 1 device in the
                              list, then the function simply returns build_network_outputs(). If this is None, that is
                              the equivalent of using the single default device.
    :param gradient_clipping_norm: None or float. If float, then the gradients will be clipped via global norm.
    :param add_update_ops: Bool. Whether to add all ops in the udpate ops collection. Set to True if the network uses
                           batch normalization.
    :param var_list_scopes: Optional list of lists (one list per optimizer) of variable scopes to optimize.
    :param verbose: Bool. Whether to make print statements.
    :return: train_op: Tensorflow op. If there are multiple optimizers, this will be a list.
             global_step: Tensor (variable). Int variable incremented every time train_op is run.
                          If there are multiple optimizers, this will be a list.
             output_dict: Dict. This can be treated exactly the same as the return value of build_network_outputs() for
                          one GPU. It is in the following format:
                             key: Str<variable name>
                             value: TensorIO
    """
    if not isinstance(optimizers, list):
        assert optimizers is not None
        optimizers = [optimizers]
    else:
        assert len(optimizers) > 0
    if var_list_scopes is not None:
        assert len(var_list_scopes) == len(optimizers), 'Must have one var list scope per optimizer.'
    # Do single-device version of this function and bypass all the complex accumulating.
    if available_devices is None or len(available_devices) == 1:
        return _create_train_op_single_device(optimizers, build_network_outputs, batched_network_args,
                                              other_network_args, available_devices=available_devices,
                                              gradient_clipping_norm=gradient_clipping_norm,
                                              add_update_ops=add_update_ops, var_list_scopes=var_list_scopes,
                                              verbose=verbose)

    # Get the number of examples per GPU.
    with tf.name_scope('examples_per_gpu'):
        examples_per_gpu = None
        if len(batched_network_args) > 0:
            batch_size = tf.shape(batched_network_args[0])[0]
            num_gpus = len(available_devices)
            examples_per_gpu = tf.cast(batch_size / num_gpus, dtype=tf.int32)

    # Accumulation variables.
    accumulation_dict = {}
    tower_grads_and_vars = [[] for _ in optimizers]

    # Create the towers.
    for i, device in enumerate(available_devices):
        if verbose:
            print('Creating tower for', device)

        with tf.name_scope('batch_distribution'):
            sub_batched_network_args = []
            if examples_per_gpu is not None:
                start = tf.cast(examples_per_gpu * i, dtype=tf.int32)
                end = tf.cast(examples_per_gpu * (i + 1), dtype=tf.int32)
                sub_batched_network_args = [arg[start:end, ...] for arg in batched_network_args]

        # Create the loss under this tower.
        with tf.device(device):
            outputs = build_network_outputs(*(sub_batched_network_args + other_network_args))

            # Accumulate outputs.
            for key, value in outputs.items():
                tensor_list = value.tensors
                accumulation_type = value.tensor_type
                if key not in accumulation_dict:
                    accumulation_dict[key] = AccumulatingTensorIO(accumulation_type)
                assert accumulation_dict[key].tensor_type == accumulation_type
                accumulation_dict[key].add(tensor_list)

            # Create the gradient ops.
            assert len(outputs['loss'].tensors) == len(optimizers), 'Number of losses must be the same as optimizers.'
            for o, optimizer in enumerate(optimizers):
                var_list = None if var_list_scopes is None else _get_var_list(var_list_scopes[o], verbose=verbose)
                grads_and_vars = optimizer.compute_gradients(outputs['loss'].tensors[o], var_list=var_list)
                tower_grads_and_vars[o].append(grads_and_vars)

    # Create the train ops.
    with tf.variable_scope('train'):
        global_steps = []
        train_ops = []
        for o, optimizer in enumerate(optimizers):
            global_step = tf.Variable(initial_value=0, trainable=False, dtype=tf.int32, name='global_step')
            global_steps.append(global_step)
            averaged_grads_and_vars = average_gradients(tower_grads_and_vars[o])
            train_op, _ = _apply_gradients(optimizer, averaged_grads_and_vars, global_step,
                                           gradient_clipping_norm=gradient_clipping_norm)
            if add_update_ops:
                train_op = _add_update_ops(train_op, verbose=verbose)
            train_ops.append(train_op)

    # Sets the outputs to be the equivalent of a single-gpu output.
    with tf.name_scope('accumulate_outputs'):
        output_dict = {}
        for key, value in accumulation_dict.items():
            output_dict[key] = value.accumulate()

    if len(optimizers) == 1:
        return train_ops[0], global_steps[0], output_dict
    return train_ops, global_steps, output_dict
