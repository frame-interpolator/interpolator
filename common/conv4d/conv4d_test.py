import numpy as np
import tensorflow as tf
import unittest
from common.conv4d.conv4d import Conv4d


class TestConv4d(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_vars_and_bias(self):
        in_channels = 2
        out_channels = 1
        i = np.zeros((1, in_channels, 2, 3, 2, 2))
        input_tensor = tf.placeholder(tf.float32, shape=np.shape(i))
        bias_val = 3
        bias_init = tf.constant_initializer(bias_val)
        kernel_init = tf.constant_initializer(1)
        output = Conv4d(input_tensor, out_channels,
                        name='conv4d_test_vars',
                        kernel_size=(3, 3, 3, 3),
                        kernel_initializer=kernel_init,
                        bias_initializer=bias_init)

        # Test that we have all the trainable variables.
        self.sess.run(tf.global_variables_initializer())
        trainable_vars = tf.trainable_variables(scope='conv4d_test_vars')
        self.assertEqual(len(trainable_vars), 2)

        o = self.sess.run(output, feed_dict={input_tensor: i})
        self.assertEqual(o[0, 0, 0, 0, 0, 0], 2 ** 4 * in_channels * bias_val)

    def test_ones_kernel_zeros_image(self):
        in_channels = 2
        out_channels = 1
        i = np.zeros((1, in_channels, 2, 3, 2, 2))
        input_tensor = tf.placeholder(tf.float32, shape=np.shape(i))
        bias_init = tf.constant_initializer(0)
        kernel_init = tf.constant_initializer(1)
        output = Conv4d(input_tensor, out_channels,
                        name='conv4d_test_ones_kernel_zeros_image',
                        kernel_size=(3, 3, 3, 3), kernel_initializer=kernel_init)
        self.sess.run(tf.global_variables_initializer())
        o = self.sess.run(output, feed_dict={input_tensor: i})

        self.assertTupleEqual(np.shape(o), (1, 1, 2, 3, 2, 2))
        self.assertEqual(np.sum(np.abs(o)), 0)

    def test_ones_kernel_ones_image(self):
        in_channels = 3
        out_channels = 1
        i = np.ones((1, in_channels, 3, 3, 3, 4))
        input_tensor = tf.placeholder(tf.float32, shape=np.shape(i))
        bias_init = tf.constant_initializer(0)
        kernel_init = tf.constant_initializer(1)
        output = Conv4d(input_tensor, out_channels,
                        name='conv4d_test_ones_kernel_ones_image',
                        kernel_size=(3, 3, 3, 3), kernel_initializer=kernel_init)
        self.sess.run(tf.global_variables_initializer())
        o = self.sess.run(output, feed_dict={input_tensor: i})

        self.assertTupleEqual(np.shape(o), (1, 1, 3, 3, 3, 4))
        self.assertNotEqual(np.sum(np.abs(o)), 0)

        # Border should only catch 2 pixels per dimension x in_channels.
        self.assertEqual(o[0, 0, 0, 0, 0, 0], 2 ** 4 * in_channels)

        # Interior should catch full kernel size x in_channels.
        self.assertEqual(o[0, 0, 1, 1, 1, 1], 3 ** 4 * in_channels)

    def test_ones_kernel(self):
        in_channels = 2
        out_channels = 2
        i = np.zeros((1, in_channels, 3, 3, 3, 4))
        i[0, 0, 0, 0, 0, 0] = 3
        i[0, 1, 1, 1, 1, 1] = 5
        input_tensor = tf.placeholder(tf.float32, shape=np.shape(i))
        bias_init = tf.constant_initializer(0)
        kernel_init = tf.constant_initializer(1)
        output = Conv4d(input_tensor, out_channels,
                        name='conv4d_test_ones_kernel',
                        kernel_size=(3, 3, 3, 3), kernel_initializer=kernel_init)
        self.sess.run(tf.global_variables_initializer())
        o = self.sess.run(output, feed_dict={input_tensor: i})

        self.assertTupleEqual(np.shape(o), (1, 2, 3, 3, 3, 4))
        self.assertNotEqual(np.sum(np.abs(o)), 0)

        self.assertEqual(o[0, 0, 0, 0, 0, 0], 8)
        self.assertEqual(o[0, 0, 2, 2, 2, 2], 5)
        self.assertEqual(np.sum(o[0, 0]), 2 ** 4 * 3 + 3 ** 4 * 5)

    def test_ones_kernel_strided(self):
        in_channels = 2
        out_channels = 2
        i = np.zeros((1, in_channels, 3, 3, 3, 4))
        i[0, 0, 0, 0, 0, 0] = 3
        i[0, 1, 1, 1, 1, 1] = 5
        input_tensor = tf.placeholder(tf.float32, shape=np.shape(i))
        bias_init = tf.constant_initializer(0)
        kernel_init = tf.constant_initializer(1)
        output = Conv4d(input_tensor, out_channels,
                        name='conv4d_test_ones_kernel_strided',
                        strides=(1, 1, 2, 2),
                        kernel_size=(3, 3, 3, 3), kernel_initializer=kernel_init)
        self.sess.run(tf.global_variables_initializer())
        o = self.sess.run(output, feed_dict={input_tensor: i})

        self.assertTupleEqual(np.shape(o), (1, 2, 3, 3, 2, 2))
        self.assertNotEqual(np.sum(np.abs(o)), 0)

        self.assertEqual(o[0, 0, 0, 0, 0, 0], 8)
        self.assertEqual(o[0, 0, 1, 1, 1, 1], 5)
        self.assertEqual(np.sum(o[0, 0]), 2 ** 2 * 3 + 9 * 4 * 5)

    def test_zeros_kernel(self):
        in_channels = 2
        out_channels = 1
        i = np.ones((1, in_channels, 2, 3, 2, 2))
        input_tensor = tf.placeholder(tf.float32, shape=np.shape(i))
        bias_init = tf.constant_initializer(0)
        kernel_init = tf.constant_initializer(0)
        output = Conv4d(input_tensor, out_channels,
                        name='conv4d_test_zeros_kernel_ones_image',
                        kernel_size=(3, 3, 3, 3), kernel_initializer=kernel_init)
        self.sess.run(tf.global_variables_initializer())
        o = self.sess.run(output, feed_dict={input_tensor: i})

        self.assertTupleEqual(np.shape(o), (1, 1, 2, 3, 2, 2))
        self.assertEqual(np.sum(np.abs(o)), 0)
