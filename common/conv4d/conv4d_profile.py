import numpy as np
import tensorflow as tf
from common.utils.profile import run_profiler
from common.conv4d import Conv4d

if __name__ == '__main__':
    height = 32
    width = 32
    c = 32
    k = 32
    input_channels = 1
    output_channels = 1
    batch_size = 4

    input_shape = [batch_size, height, width, c, k, input_channels]
    i = np.round(np.random.random(input_shape)*100)
    input = tf.constant(i, dtype=tf.float32)
    bias_init = tf.constant_initializer(0)

    # Create the graph.
    input_placeholder = tf.placeholder(shape=input_shape, dtype=tf.float32)
    input = tf.transpose(input_placeholder, perm=[0, 5, 1, 2, 3, 4])
    output_tensor = Conv4d(
        input,
        1,
        (3, 3, 3, 3),
        data_format='channels_first',
        bias_initializer=bias_init,
        name='conv4d_valid')
    grads = tf.gradients(output_tensor, input_placeholder)

    # Create dummy images.
    image = np.zeros(shape=input_shape, dtype=np.float32)
    image[:, 2:height - 2, 2:width - 2, ...] = 1.0

    query = [output_tensor, grads]
    feed_dict = {input_placeholder: image}
    run_profiler(query, feed_dict, name='conv4d')
