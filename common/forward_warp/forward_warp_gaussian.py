import tensorflow as tf
from common.forward_warp.forward_warp_bilinear import forward_warp_bilinear
from common.utils.tf import load_op_library
from tensorflow.python.framework import ops


DISOCC_THRESH = 0.5


# Load op library.
mod = load_op_library('forward_warp_gaussian_op', 'build')


def is_forward_warp_gaussian_cuda():
    return mod is not None


def forward_warp_gaussian(features, flow, splat_variance=0.25):
    """
    Forward warp with gaussian splat is implemented only on CUDA. If the CUDA implementation is not available, it will
    fall back to the bilateral implementation.
    :param features: A Tensor. Features to be warped, of shape [batch_size, H, W, C].
    :param flow: A Tensor. Un-normalized flow in image pixel units, of shape [batch_size, H, W, 2].
                 Flow vectors should have (x, y) ordering.
    :param splat_variance: Float. Variance of the splat. Only used for the CUDA op.
    """
    if is_forward_warp_gaussian_cuda():
        return mod.forward_warp_gaussian(features, flow, variance=splat_variance)
    else:
        return forward_warp_bilinear(features, flow)


if is_forward_warp_gaussian_cuda():
    @ops.RegisterGradient('ForwardWarpGaussian')
    def _ForwardWarpGaussianGrad(op, grad):
        image_grad, flow_grad = mod.forward_warp_gaussian_grad(
            grad, op.inputs[0], op.inputs[1], variance=op.get_attr('variance'))
        return [image_grad, flow_grad]


def create_disocclusion_mask(flow, splat_variance=1.0):
    """
    Creates a disocclusion mask representing areas that were previously occluded and will become visible.
    This is done by forward warping some ones and thresholding them for visibility.

    Disocclusion maps are used by the UnFlow loss:
    https://github.com/simonmeister/UnFlow/blob/8bff4939963c7d0adb9435880dc506fb3f988080/src/e2eflow/core/losses.py#L28
    This isn't mentioned in the paper anywhere, but clearly enough, it is in the code.
    :param flow: Tensor of shape [B, H, W, 2].
    :param splat_variance: Float. Variance of the splat. Only used for the CUDA op.
    :return: Tensor of shape [B, H, W, 1].
    """
    with tf.name_scope('disocclusion_mask'):
        batch, height, width, _ = tf.unstack(tf.shape(flow))
        prewarp_mask = tf.ones([batch, height, width, 1], dtype=tf.float32)
        forward_warped_mask = forward_warp_gaussian(prewarp_mask, flow, splat_variance=splat_variance)
        return tf.cast(forward_warped_mask < DISOCC_THRESH, dtype=tf.float32)

