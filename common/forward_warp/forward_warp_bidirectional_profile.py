import numpy as np
import tensorflow as tf
from common.forward_warp.forward_warp_bidirectional import forward_warp_bidirectional
from common.utils.profile import run_profiler

if __name__ == '__main__':
    height = 256
    width = 256
    im_channels = 64 + 6
    batch_size = 16

    # Create the graph.
    image_shape = [batch_size, height, width, im_channels]
    flow_shape = [batch_size, height, width, 2]
    image_0_placeholder = tf.placeholder(shape=image_shape, dtype=tf.float32)
    image_1_placeholder = tf.placeholder(shape=image_shape, dtype=tf.float32)
    flow_0_1_placeholder = tf.placeholder(shape=flow_shape, dtype=tf.float32)
    flow_1_0_placeholder = tf.placeholder(shape=flow_shape, dtype=tf.float32)
    warped_0_1, warped_1_0, _ = forward_warp_bidirectional(image_0_placeholder, image_1_placeholder,
                                                           flow_0_1_placeholder, flow_1_0_placeholder,
                                                           batch_size * [0.5],
                                                           fill_holes=False)
    warped = [warped_0_1, warped_1_0]
    grads = tf.gradients(warped, [image_0_placeholder, flow_0_1_placeholder,
                                  image_1_placeholder, flow_1_0_placeholder])

    # Create dummy images.
    image = np.zeros(shape=[batch_size, height, width, im_channels], dtype=np.float32)
    flow = np.zeros(shape=[batch_size, height, width, 2], dtype=np.float32)
    image[:, 2:height - 2, 2:width - 2, :] = 1.0
    flow[:, 4:height - 4, 5:width - 5, :] = 1.0

    query = [warped, grads]
    feed_dict = {image_0_placeholder: image,
                 flow_0_1_placeholder: flow,
                 image_1_placeholder: image,
                 flow_1_0_placeholder: flow}
    run_profiler(query, feed_dict, name='forward-warp-bidirectional')
