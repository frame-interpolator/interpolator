import os
import unittest
import numpy as np
import cv2
import tensorflow as tf
from common.utils.img import read_image, write_image
from common.utils.flow import read_flow_file, get_flow_visualization
from common.forward_warp.forward_warp_bidirectional import forward_warp_bidirectional
from common.forward_warp.forward_warp_interp import forward_warp_interp


class TestForwardWarpBidirectional(unittest.TestCase):
    """
    All of the tests here are image snapshot / regression tests.
    If you want to update the tests, set self.update_refs = True.
    A bunch of videos will be written to self.output_video_folder.
    """
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        # Sintel reference image testing.
        self.flow_a_b_path = os.path.join('common', 'forward_warp', 'test_data', 'flow_ab.flo')
        self.flow_b_a_path = os.path.join('common', 'forward_warp', 'test_data', 'flow_ba.flo')
        self.image_path_a = os.path.join('common', 'forward_warp', 'test_data', 'image_a.png')
        self.image_path_b = os.path.join('common', 'forward_warp', 'test_data', 'image_b.png')
        self.ref_image_path = os.path.join('common', 'forward_warp', 'test_data', 'ref.png')
        self.err_image_path = os.path.join('common', 'forward_warp', 'test_data', 'err.png')
        self.err_diff_image_path = os.path.join('common', 'forward_warp', 'test_data', 'err_diff.png')
        self.ref_holes_fw_path = os.path.join('common', 'forward_warp', 'test_data', 'ref_fw_holes.png')
        self.ref_holes_bw_path = os.path.join('common', 'forward_warp', 'test_data', 'ref_bw_holes.png')

        # Circle warp reference image. fg is foreground, bg is background.
        self.ref_fg_fw_warp_path = os.path.join('common', 'forward_warp', 'test_data', 'ref_fg_fw_warp.png')
        self.ref_bg_fw_warp_path = os.path.join('common', 'forward_warp', 'test_data', 'ref_bg_fw_warp.png')
        self.ref_fg_bw_warp_path = os.path.join('common', 'forward_warp', 'test_data', 'ref_fg_bw_warp.png')
        self.ref_bg_bw_warp_path = os.path.join('common', 'forward_warp', 'test_data', 'ref_bg_bw_warp.png')

        self.max_allowable_grad_err = 5e-4
        self.update_refs = False
        self.output_video_folder = os.path.join('output_test_images', 'forward_warp')
        if not os.path.exists(self.output_video_folder):
            os.makedirs(self.output_video_folder, exist_ok=True)

    def test_sintel(self):
        """
        We are actually using PWCNet-predicted image b -> image a flow, since ground truth is not available.
        """
        flow_ab = [read_flow_file(self.flow_a_b_path)]
        flow_ba = [read_flow_file(self.flow_b_a_path)]
        img_a = [read_image(self.image_path_a, as_float=True)]
        img_b = [read_image(self.image_path_b, as_float=True)]
        img_gt = None
        holes_fw_gt = None
        holes_bw_gt = None

        if not self.update_refs:
            img_gt = [read_image(self.ref_image_path, as_float=True)]
            holes_fw_gt = read_image(self.ref_holes_fw_path, as_float=True)[..., 0:1]
            holes_bw_gt = read_image(self.ref_holes_bw_path, as_float=True)[..., 0:1]
        img_a_tensor = tf.placeholder(tf.float32, shape=np.shape(img_a))
        img_b_tensor = tf.placeholder(tf.float32, shape=np.shape(img_a))
        flow_ab_tensor = tf.placeholder(tf.float32, shape=np.shape(flow_ab))
        flow_ba_tensor = tf.placeholder(tf.float32, shape=np.shape(flow_ab))
        warp_0_1, warp_1_0, others_dict = forward_warp_bidirectional(img_a_tensor, img_b_tensor, flow_ab_tensor,
                                                                     flow_ba_tensor, 0.8, fill_holes=False,
                                                                     num_blending_channels=3)
        query = [warp_0_1, warp_1_0, others_dict['holes_0_1'], others_dict['holes_1_0']]
        outputs = self.sess.run(query, feed_dict={
            img_a_tensor: img_a,
            img_b_tensor: img_b,
            flow_ab_tensor: flow_ab,
            flow_ba_tensor: flow_ba
        })

        warped_forward, warped_backward, holes_forward, holes_backward = outputs
        warped_forward = np.clip(warped_forward, 0, 1.0)
        holes_forward = holes_forward[0] / np.max(holes_forward[0])
        holes_backward = holes_backward[0] / np.max(holes_backward[0])

        if self.update_refs:
            write_image(self.ref_image_path, warped_forward[0])
            write_image(self.ref_holes_fw_path, holes_forward)
            write_image(self.ref_holes_bw_path, holes_backward)
        else:
            diff = self._image_dist(img_gt[0], warped_forward[0])
            diff_img = diff
            write_image(os.path.join('common', 'forward_warp', 'test_data', 'flow_ab.png'),
                        get_flow_visualization(flow_ab[0]))
            write_image(os.path.join('common', 'forward_warp', 'test_data', 'flow_ba.png'),
                        get_flow_visualization(flow_ba[0]))
            write_image(self.err_image_path, warped_forward[0])
            write_image(self.err_diff_image_path, diff_img / np.max(diff_img))
            self.assertTupleEqual(np.shape(warped_forward), np.shape(img_gt))
            self.assertLessEqual(np.mean(diff), 4E-3)
            self.assertLessEqual(np.max(self._image_dist(holes_fw_gt, holes_forward)), 0.004)
            self.assertLessEqual(np.max(self._image_dist(holes_bw_gt, holes_backward)), 0.004)
            os.remove(self.err_image_path)
            os.remove(self.err_diff_image_path)

    def test_has_gradients(self):
        flow_ab = [read_flow_file(self.flow_a_b_path)]
        flow_ba = [read_flow_file(self.flow_b_a_path)]
        img_a = [read_image(self.image_path_a, as_float=True)]
        img_b = [read_image(self.image_path_b, as_float=True)]
        img_a_tensor = tf.placeholder(tf.float32, shape=np.shape(img_a))
        img_b_tensor = tf.placeholder(tf.float32, shape=np.shape(img_a))
        flow_ab_tensor = tf.placeholder(tf.float32, shape=np.shape(flow_ab))
        flow_ba_tensor = tf.placeholder(tf.float32, shape=np.shape(flow_ab))
        warp_tensor_0_1, warp_tensor_1_0, _ = forward_warp_bidirectional(img_a_tensor, img_b_tensor, flow_ab_tensor,
                                                                         flow_ba_tensor, 0.5, fill_holes=True,
                                                                         num_blending_channels=3)
        warp_tensor = [warp_tensor_0_1, warp_tensor_1_0]
        grads = tf.gradients(warp_tensor, [img_a_tensor, img_b_tensor, flow_ab_tensor, flow_ba_tensor])
        for grad_tensor in grads:
            self.assertNotEqual(grad_tensor, None)
        grads = self.sess.run(grads, feed_dict={img_a_tensor: img_a,
                                                img_b_tensor: img_b,
                                                flow_ab_tensor: flow_ab,
                                                flow_ba_tensor: flow_ba})
        for gradient in grads:
            self.assertNotEqual(np.sum(gradient), 0.0)

    def test_blend_partial(self):
        h = 2
        w = 2
        c = 4

        flow_ab = np.zeros((1, h, w, 2))
        flow_ba = np.zeros((1, h, w, 2))
        img_a = np.ones((1, h, w, c))
        img_b = 3 * np.ones((1, h, w, c))
        img_a_tensor = tf.placeholder(tf.float32, shape=np.shape(img_a))
        img_b_tensor = tf.placeholder(tf.float32, shape=np.shape(img_a))
        flow_ab_tensor = tf.placeholder(tf.float32, shape=np.shape(flow_ab))
        flow_ba_tensor = tf.placeholder(tf.float32, shape=np.shape(flow_ab))
        warp_tensor_0_1, warp_tensor_1_0, _ = forward_warp_bidirectional(img_a_tensor, img_b_tensor, flow_ab_tensor,
                                                                         flow_ba_tensor, 0.5, fill_holes=False,
                                                                         num_blending_channels=2)
        query = [warp_tensor_0_1, warp_tensor_1_0]
        outputs = self.sess.run(query, feed_dict={img_a_tensor: img_a,
                                                  img_b_tensor: img_b,
                                                  flow_ab_tensor: flow_ab,
                                                  flow_ba_tensor: flow_ba})
        warp_0_1, warp_1_0 = outputs

        # The outputs only have the first 2 channels blended.
        expected_blend = 2 * np.ones((1, h, w, 2))
        self.assertListEqual(warp_0_1.tolist(), np.concatenate([expected_blend,
                                                                np.ones((1, h, w, 2))], axis=-1).tolist())
        self.assertListEqual(warp_1_0.tolist(), np.concatenate([expected_blend,
                                                                3 * np.ones((1, h, w, 2))], axis=-1).tolist())

    def test_blend_full(self):
        h = 2
        w = 2
        c = 2

        flow_ab = np.zeros((1, h, w, 2))
        flow_ba = np.zeros((1, h, w, 2))
        img_a = np.ones((1, h, w, c))
        img_b = 3 * np.ones((1, h, w, c))
        img_a_tensor = tf.placeholder(tf.float32, shape=np.shape(img_a))
        img_b_tensor = tf.placeholder(tf.float32, shape=np.shape(img_a))
        flow_ab_tensor = tf.placeholder(tf.float32, shape=np.shape(flow_ab))
        flow_ba_tensor = tf.placeholder(tf.float32, shape=np.shape(flow_ab))
        warp_tensor_0_1, warp_tensor_1_0, _ = forward_warp_bidirectional(img_a_tensor, img_b_tensor, flow_ab_tensor,
                                                                         flow_ba_tensor, 0.5, fill_holes=False,
                                                                         num_blending_channels=2)
        query = [warp_tensor_0_1, warp_tensor_1_0]
        outputs = self.sess.run(query, feed_dict={img_a_tensor: img_a,
                                                  img_b_tensor: img_b,
                                                  flow_ab_tensor: flow_ab,
                                                  flow_ba_tensor: flow_ba})
        warp_0_1, warp_1_0 = outputs

        # The outputs only have the first 2 channels blended.
        expected_blend = 2 * np.ones((1, h, w, 2))
        self.assertListEqual(warp_0_1.tolist(), expected_blend.tolist())
        self.assertListEqual(warp_1_0.tolist(), expected_blend.tolist())

    def run_warps_and_write_to_video(self, object_shift, background_shift, object_hue_shift, background_hue_shift,
                                     video_path, use_middlebury=False):
        """
        :return: The warp that is temporally in the middle.
        """
        dummies = generate_image_and_flows(object_shift, background_shift,
                                           object_color_shift=object_hue_shift,
                                           background_colour_shift=background_hue_shift)
        image_1, image_2, flow_1_2, flow_2_1 = dummies
        t_tensor = tf.placeholder(tf.float32, shape=np.shape([0.5]))
        image_1_tensor = tf.placeholder(tf.float32, shape=np.shape([image_1]))
        image_2_tensor = tf.placeholder(tf.float32, shape=np.shape([image_2]))
        flow_1_2_tensor = tf.placeholder(tf.float32, shape=np.shape([flow_1_2]))
        flow_2_1_tensor = tf.placeholder(tf.float32, shape=np.shape([flow_2_1]))
        if use_middlebury:
            warped_tensor = forward_warp_interp(image_1_tensor, image_2_tensor, flow_1_2_tensor, t_tensor)
        else:
            warped_0_1_tensor, warped_1_0_tensor, _ = forward_warp_bidirectional(image_1_tensor, image_2_tensor,
                                                                                 flow_1_2_tensor, flow_2_1_tensor,
                                                                                 t_tensor, fill_holes=False,
                                                                                 num_blending_channels=3)
            warped_tensor = [warped_0_1_tensor, warped_1_0_tensor]
        width, height = np.shape(image_1)[1], np.shape(image_1)[0]
        writer = cv2.VideoWriter(video_path, cv2.VideoWriter_fourcc(*'MJPG'), 20, (width, height))
        writer.write(image_1.astype(np.uint8)[..., ::-1])
        num_frames = 48
        middle_warp_fw, middle_warp_bw = None, None
        for i in range(num_frames):
            t = (i + 1) * 1.0 / (num_frames + 1)
            feed_dict = {
                t_tensor: [t],
                image_1_tensor: [image_1],
                image_2_tensor: [image_2],
                flow_1_2_tensor: [flow_1_2],
                flow_2_1_tensor: [flow_2_1]
            }
            if use_middlebury:
                warped, _, _, _ = self.sess.run(warped_tensor, feed_dict=feed_dict)
            else:
                warped_1_2, warped_2_1 = self.sess.run(warped_tensor, feed_dict=feed_dict)
                warped = warped_1_2
            if i == num_frames // 2 and not use_middlebury:
                middle_warp_fw = warped_1_2[0]
                middle_warp_bw = warped_2_1[0]

            writer.write(np.clip(warped[0], 0.0, 255.0).astype(np.uint8)[..., ::-1])
        writer.write(image_2.astype(np.uint8)[..., ::-1])
        writer.release()
        return middle_warp_fw, middle_warp_bw

    def test_warp_circle(self):
        use_middlebury = False
        if use_middlebury:
            video_path = os.path.join(self.output_video_folder, 'warp_circle_middlebury.avi')
        else:
            video_path = os.path.join(self.output_video_folder, 'warp_circle.avi')

        # The object flow is down and to the right. The background flow is up and to the left.
        middle_warps = self.run_warps_and_write_to_video(400, -40, -40, -20.0, video_path,
                                                         use_middlebury=use_middlebury)
        if not use_middlebury:
            middle_warp_fw, middle_warp_bw = middle_warps
            middle_warp_fw /= 255.0
            middle_warp_bw /= 255.0
            if self.update_refs:
                write_image(self.ref_fg_fw_warp_path, middle_warp_fw)
                write_image(self.ref_fg_bw_warp_path, middle_warp_bw)
            else:
                warp_fw_gt = read_image(self.ref_fg_fw_warp_path, as_float=True)
                warp_bw_gt = read_image(self.ref_fg_bw_warp_path, as_float=True)
                diff_fw = self._image_dist(middle_warp_fw, warp_fw_gt)
                diff_bw = self._image_dist(middle_warp_bw, warp_bw_gt)
                self.assertLessEqual(np.mean(diff_fw), 2.5E-3)
                self.assertLessEqual(np.mean(diff_bw), 2.5E-3)

    def test_warp_background(self):
        use_middlebury = False
        if use_middlebury:
            video_path = os.path.join(self.output_video_folder, 'warp_background_middlebury.avi')
        else:
            video_path = os.path.join(self.output_video_folder, 'warp_background.avi')

        # The object flow is up and to the left. The background flow is down and to the right.
        middle_warps = self.run_warps_and_write_to_video(40, -400, -40, -20.0, video_path,
                                                         use_middlebury=use_middlebury)
        if not use_middlebury:
            middle_warp_fw, middle_warp_bw = middle_warps
            middle_warp_fw /= 255.0
            middle_warp_bw /= 255.0
            if self.update_refs:
                write_image(self.ref_bg_fw_warp_path, middle_warp_fw)
                write_image(self.ref_bg_bw_warp_path, middle_warp_bw)
            else:
                warp_fw_gt = read_image(self.ref_bg_fw_warp_path, as_float=True)
                warp_bw_gt = read_image(self.ref_bg_bw_warp_path, as_float=True)
                diff_fw = self._image_dist(middle_warp_fw, warp_fw_gt)
                diff_bw = self._image_dist(middle_warp_bw, warp_bw_gt)
                self.assertLessEqual(np.mean(diff_fw), 2.5E-3)
                self.assertLessEqual(np.mean(diff_bw), 2.5E-3)

    def _image_dist(self, pred, ref):
        if len(np.shape(pred)) == 2:
            # Gray-scale image.
            return np.abs(pred - ref)
        return np.sqrt(np.sum(np.square(pred - ref), axis=-1, keepdims=True))


def shift_hsv(rgb, hue_shift):
    hsv = cv2.cvtColor(np.array([[rgb]]), cv2.COLOR_BGR2HSV)
    hsv[0, 0, 0] += hue_shift
    rgb = cv2.cvtColor(np.array(hsv), cv2.COLOR_HSV2BGR)[0, 0]
    return rgb


def generate_image_and_flows(object_shift, background_shift, object_color_shift=0.0, background_colour_shift=0.0):
    """
    Generate simple testing flows and images. The height and width will be 1024 by 1024.
    :param object_shift: Scalar. Translation amount in pixels of the foreground object.
    :param background_shift: Scalar. Translation amount in pixels of the background.
    :param object_color_shift: Hue shift of the foreground object going from frame 1 to frame 2.
    :param background_colour_shift: Hue shift of the background going from frame 1 to frame 2.
    :return: (image_1, image_2, flow_1_2, flow_2_1). The image shape is (1024, 1024, 3).
    """
    h, w = 1024, 1024
    unit_vec = np.array([1.0, 1.0]) / np.sqrt(2)
    rad = 160

    obj_flow = np.round(object_shift * unit_vec)
    background_flow = np.round(background_shift * unit_vec)

    # Compute colours and positions.
    obj_pos_1 = np.array([(h - 1) / 3.0, (w - 1) / 3.0]).astype(np.int32)
    obj_pos_2 = (obj_pos_1 + obj_flow).astype(np.int32)
    obj_colour_1 = np.array([180.0, 180.0, 0.0]).astype(np.float32)
    obj_colour_2 = shift_hsv(obj_colour_1, object_color_shift)
    background_colour_1 = np.array([0.0, 180.0, 180.0]).astype(np.float32)
    background_colour_2 = shift_hsv(background_colour_1, background_colour_shift)

    # Generate images.
    image_1 = np.zeros((h, w, 3), dtype=np.float32) + background_colour_1
    image_2 = np.zeros((h, w, 3), dtype=np.float32) + background_colour_2
    cv2.circle(image_1, tuple(obj_pos_1), rad, tuple([int(x) for x in obj_colour_1]), thickness=-1)
    cv2.circle(image_2, tuple(obj_pos_2), rad, tuple([int(x) for x in obj_colour_2]), thickness=-1)

    # Generate flows.
    flow_1_2 = np.zeros((h, w, 2)) + background_flow
    flow_2_1 = np.zeros((h, w, 2)) - background_flow
    cv2.circle(flow_1_2, tuple(obj_pos_1), rad, tuple([int(x) for x in obj_flow]), thickness=-1)
    cv2.circle(flow_2_1, tuple(obj_pos_2), rad, tuple([int(-x) for x in obj_flow]), thickness=-1)
    return image_1, image_2, flow_1_2, flow_2_1


if __name__ == '__main__':
    unittest.main()



