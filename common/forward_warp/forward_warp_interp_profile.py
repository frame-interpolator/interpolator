import os
import numpy as np
import tensorflow as tf
from common.utils.flow import read_flow_file
from common.utils.img import read_image
from common.utils.profile import run_profiler
from common.forward_warp.forward_warp_interp import forward_warp_interp

if __name__ == '__main__':
    height = 512
    width = 512
    im_channels = 32
    batch_size = 8

    # Create the graph.
    image_shape = [batch_size, height, width, im_channels]
    flow_shape = [batch_size, height, width, 2]
    t_shape = [batch_size]
    image_0_placeholder = tf.placeholder(shape=image_shape, dtype=tf.float32)
    image_1_placeholder = tf.placeholder(shape=image_shape, dtype=tf.float32)
    flow_placeholder = tf.placeholder(shape=flow_shape, dtype=tf.float32)
    t_placeholder = tf.placeholder(shape=t_shape, dtype=tf.float32)
    warped = forward_warp_interp(image_0_placeholder, image_1_placeholder, flow_placeholder, t_placeholder)

    # Create dummy images.
    image_0 = np.zeros(shape=[batch_size, height, width, im_channels], dtype=np.float32)
    image_1 = np.zeros(shape=[batch_size, height, width, im_channels], dtype=np.float32)
    flow = np.zeros(shape=[batch_size, height, width, 2], dtype=np.float32)
    t = np.linspace(0.25, 0.75, batch_size, dtype=np.float32)
    image_0[:, 2:height - 2, 2:width - 2, :] = 1.0
    image_1[:, 2:height - 2, 2:width - 2, :] = 1.0
    flow[:, 4:height - 4, 5:width - 5, :] = 1.0

    query = [warped]
    feed_dict = {flow_placeholder: flow, image_0_placeholder: image_0, image_1_placeholder: image_1, t_placeholder: t}
    run_profiler(query, feed_dict, name='forward-warp-interp')
