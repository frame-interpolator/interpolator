import os
import unittest
import numpy as np
import tensorflow as tf
from common.utils.img import read_image, show_image, write_image
from common.forward_warp.forward_warp_interp import forward_warp_interp
from common.utils.flow import read_flow_file, get_flow_visualization
from tensorflow.errors import InvalidArgumentError


VISUALIZE = False
WRITE_TO_FILE = False
WRITE_TO_VIDEO = False
FILL_HOLES = False


class TestForwardWarpInterp(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        self.flow_path = os.path.join('pwcnet', 'warp', 'test_data', 'flow_ab.flo')
        self.image_path_a = os.path.join('pwcnet', 'warp', 'test_data', 'image_a.png')
        self.image_path_b = os.path.join('pwcnet', 'warp', 'test_data', 'image_b.png')

        self.max_allowable_grad_err = 5e-4

    def test_photoconsistency(self):
        # Flow everything into the center pixel, then assert that the most
        # photoconsistent pixel wins.
        height = 3
        width = 3
        channels = 2
        # Flow is in (x, y) order.
        flow = [[
            [[ 2, 2], [ 0, 2], [-2, 2]],
            [[ 2, 0], [ 0, 0], [-2, 0]],
            [[ 2,-2], [ 0,-2], [-2,-2]]
        ]]
        feature_0 = [[
            [[ 2, 2], [ 1, 1], [ 1, 1]],
            [[ 1, 1], [ 1, 1], [ 1, 1]],
            [[ 1, 1], [ 1, 1], [ 1, 1]]
        ]]
        feature_1 = [[
            [[ 3, 3], [ 3, 3], [ 3, 3]],
            [[ 3, 3], [ 3, 3], [ 3, 3]],
            [[ 3, 3], [ 3, 3], [ 2, 2]]
        ]]
        t = [0.5]

        flow_tensor = tf.placeholder(tf.float32, (1, height, width, 2))
        feature_0_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        feature_1_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        t_tensor = tf.placeholder(tf.float32, 1)
        warp_tensor, _, _, _ = forward_warp_interp(feature_0_tensor, feature_1_tensor, flow_tensor, t_tensor, FILL_HOLES)
        warp = self.sess.run(warp_tensor, feed_dict={feature_0_tensor:feature_0, feature_1_tensor:feature_1, flow_tensor:flow, t_tensor:t})
        middle = warp.tolist()[0][1][1]
        self.assertEqual(middle, [2, 2])

    def test_shift_down_right(self):
        # Tests for OOB and whole pixel movements.
        height = 3
        width = 3
        channels = 2
        # Flow is in (x, y) order.
        flow = [[
            [[2, 2], [2, 2], [2, 2]],
            [[2, 2], [2, 2], [2, 2]],
            [[2, 2], [2, 2], [2, 2]]
        ]]
        feature_0 = [[
            [[1, 1], [1, 1], [0, 0]],
            [[1, 1], [1, 1], [0, 0]],
            [[0, 0], [0, 0], [0, 0]]
        ]]
        feature_1 = [[
            [[0, 0], [0, 0], [0, 0]],
            [[0, 0], [0, 0], [0, 0]],
            [[0, 0], [0, 0], [1, 1]]
        ]]
        t = [0.5]
        expected = [[
            [[0, 0], [0, 0], [0, 0]],
            [[0, 0], [1, 1], [1, 1]],
            [[0, 0], [1, 1], [1, 1]]
        ]]

        flow_tensor = tf.placeholder(tf.float32, (1, height, width, 2))
        feature_0_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        feature_1_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        t_tensor = tf.placeholder(tf.float32, 1)
        warp_tensor, _, _, _ = forward_warp_interp(feature_0_tensor, feature_1_tensor, flow_tensor, t_tensor, FILL_HOLES)
        warp = self.sess.run(warp_tensor, feed_dict={feature_0_tensor:feature_0, feature_1_tensor:feature_1, flow_tensor:flow, t_tensor:t})
        self.assertEqual(warp.tolist(), expected)

    def test_dimension_assert(self):
        height = 3
        width = 3
        channels = 2
        # Flow has incorrect dimensions.
        flow = [[
            [[2], [2], [2]],
            [[2], [2], [2]],
            [[2], [2], [2]]
        ]]
        feature_0 = [[
            [[1, 1], [1, 1], [0, 0]],
            [[1, 1], [1, 1], [0, 0]],
            [[0, 0], [0, 0], [0, 0]]
        ]]
        feature_1 = [[
            [[0, 0], [0, 0], [0, 0]],
            [[0, 0], [0, 0], [0, 0]],
            [[0, 0], [0, 0], [1, 1]]
        ]]
        t = [0.5]

        flow_tensor = tf.placeholder(tf.float32, (1, height, width, 1))
        feature_0_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        feature_1_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        t_tensor = tf.placeholder(tf.float32, 1)
        warp_tensor, _, _, _ = forward_warp_interp(feature_0_tensor, feature_1_tensor, flow_tensor, t_tensor)
        failed = False
        try:
            warp = self.sess.run(warp_tensor, feed_dict={feature_0_tensor:feature_0, feature_1_tensor:feature_1, flow_tensor:flow, t_tensor:t})
        except InvalidArgumentError:
            failed = True
        self.assertEqual(failed, True)

    def test_splatting(self):
        # Test partial pixel movement, photoconsistency, and trivial splatting.
        height = 3
        width = 3
        channels = 2
        # Flow is in (x, y) order.
        flow = [[
            [[ 1, 1], [ 0, 0], [  0, 0]],
            [[ 0, 0], [ 0, 0], [  0, 0]],
            [[ 0, 0], [ 0, 0], [  0, 0]]
        ]]
        feature_0 = [[
            [[ 1, 1], [ 0, 0], [ 0, 0]],
            [[ 0, 0], [ 0, 0], [ 0, 0]],
            [[ 0, 0], [ 0, 0], [ 0, 0]]
        ]]
        feature_1 = [[
            [[ 0, 0], [ 0, 0], [ 0, 0]],
            [[ 0, 0], [ 1, 1], [ 0, 0]],
            [[ 0, 0], [ 0, 0], [ 0, 0]]
        ]]
        t = [0.5]
        expected = [[
            [[ 1, 1], [ 0, 0], [ 0, 0]],
            [[ 0, 0], [ 1, 1], [ 0, 0]],
            [[ 0, 0], [ 0, 0], [ 0, 0]]
        ]]
        # Note: Expect 1s in top-left and middle pixels only due to strong
        # photoconsistency.

        flow_tensor = tf.placeholder(tf.float32, (1, height, width, 2))
        feature_0_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        feature_1_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        t_tensor = tf.placeholder(tf.float32, 1)
        warp_tensor, _, _, _ = forward_warp_interp(feature_0_tensor, feature_1_tensor, flow_tensor, t_tensor, FILL_HOLES)
        warp = self.sess.run(warp_tensor, feed_dict={feature_0_tensor:feature_0, feature_1_tensor:feature_1, flow_tensor:flow, t_tensor:t})
        self.assertEqual(warp.tolist(), expected)

    def test_zero_flow(self):
        # Test that when flow=0, output is the same as input.
        height = 2
        width = 2
        channels = 2
        # Flow is in (x, y) order.
        flow = [[
            [[ 1, 1], [-1, 1]],
            [[ 1,-1], [-1,-1]]
        ]]
        feature_0 = [[
            [[ 1, 1], [ 2, 2]],
            [[ 3, 3], [ 4, 4]]
        ]]
        feature_1 = [[
            [[ 4, 4], [ 3, 3]],
            [[ 2, 2], [ 1, 1]]
        ]]
        t = [0]

        flow_tensor = tf.placeholder(tf.float32, (1, height, width, 2))
        feature_0_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        feature_1_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        t_tensor = tf.placeholder(tf.float32, 1)
        warp_tensor, _, _, _ = forward_warp_interp(feature_0_tensor, feature_1_tensor, flow_tensor, t_tensor, FILL_HOLES)
        warp = self.sess.run(warp_tensor, feed_dict={feature_0_tensor:feature_0, feature_1_tensor:feature_1, flow_tensor:flow, t_tensor:t})
        self.assertEqual(warp.tolist(), feature_0)

    def test_one_flow(self):
        # Test that when flow=0, output is the same as input.
        height = 2
        width = 2
        channels = 2
        # Flow is in (x, y) order.
        flow = [[
            [[ 1, 1], [-1, 1]],
            [[ 1,-1], [-1,-1]]
        ]]
        feature_0 = [[
            [[ 1, 1], [ 2, 2]],
            [[ 3, 3], [ 4, 4]]
        ]]
        feature_1 = [[
            [[ 4, 4], [ 3, 3]],
            [[ 2, 2], [ 1, 1]]
        ]]
        t = [1]

        flow_tensor = tf.placeholder(tf.float32, (1, height, width, 2))
        feature_0_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        feature_1_tensor = tf.placeholder(tf.float32, (1, height, width, channels))
        t_tensor = tf.placeholder(tf.float32, 1)
        warp_tensor, _, _, _ = forward_warp_interp(feature_0_tensor, feature_1_tensor, flow_tensor, t_tensor, FILL_HOLES)
        warp = self.sess.run(warp_tensor, feed_dict={feature_0_tensor:feature_0, feature_1_tensor:feature_1, flow_tensor:flow, t_tensor:t})
        self.assertEqual(warp.tolist(), feature_1)


    def test_visualization(self):
        if not (VISUALIZE or WRITE_TO_FILE):
            return

        flow_ab = [read_flow_file(self.flow_path)]
        img_a = [read_image(self.image_path_a, as_float=True)]
        img_b = [read_image(self.image_path_b, as_float=True)]
        t_tensor = tf.placeholder(tf.float32, 1)
        flow_ab_tensor = tf.placeholder(tf.float32, np.shape(flow_ab))
        img_a_tensor = tf.placeholder(tf.float32, np.shape(img_a))
        img_b_tensor = tf.placeholder(tf.float32, np.shape(img_b))
        warp_tensor = forward_warp_interp(img_a_tensor, img_b_tensor, flow_ab_tensor, t_tensor, FILL_HOLES)

        warp, flow_at_t, omask0, omask1 = self.sess.run(warp_tensor, feed_dict={flow_ab_tensor: flow_ab, img_a_tensor: img_a, img_b_tensor: img_b, t_tensor: [0.5]})
        warp = np.clip(warp[0], 0.0, 1.0)
        where_are_NaNs = np.isnan(flow_at_t)
        flow_at_t[where_are_NaNs] = 0
        flow_at_t = np.clip(flow_at_t[0], -1000.0, 1000.0)
        flow_at_t = get_flow_visualization(flow_at_t)
        omask0 = omask0[0].astype(np.uint8) * 255
        omask1 = omask1[0].astype(np.uint8) * 255
        if VISUALIZE:
            try:
                show_image(warp)
            except:
                print('show_image(warp) failed.')

        if WRITE_TO_FILE:
            write_image("image_ab.png", warp)
            write_image("flow_ab.png", flow_at_t)
            import cv2
            cv2.imwrite("omask0.png", omask0)
            cv2.imwrite("omask1.png", omask1)

        # For writing to video.
        if WRITE_TO_VIDEO:
            if not os.path.exists('outputs'):
                os.makedirs('outputs')

            import cv2
            import matplotlib.image as mpimg
            height = img_a[0].shape[0]
            width = img_a[0].shape[1]
            writer = cv2.VideoWriter('outputs/warped.avi',
                                     cv2.VideoWriter_fourcc(*'MJPG'), 20, (width, height))
            steps = 60
            for i in range(steps):
                print('Writing video at step %d' % i)
                t = i * (1.0 / float(steps))
                warped = self.sess.run(warp_tensor,
                                       feed_dict={flow_ab_tensor: flow_ab, img_a_tensor: img_a, t_tensor: t})
                warped = warped[0]
                warped = np.clip(warped, 0.0, 1.0)
                output_path = 'outputs/out-%.2f.png' % t
                mpimg.imsave(output_path, warped)
                writer.write(cv2.imread(output_path))
            writer.release()


if __name__ == '__main__':
    unittest.main()
