from common.forward_warp.forward_warp_bilinear import forward_warp_bilinear
from common.utils.tf import norm
from pwcnet.warp.warp import backward_warp
from pwcnet.losses.unflow import length_sq
import tensorflow as tf


def forward_warp_bidirectional(images_0, images_1, flow_0_1, flow_1_0, t,
                               flow_0_t=None,
                               flow_1_t=None,
                               fill_holes=True,
                               num_blending_channels=3):
    """
    :param images_0: Tensor of shape [batch_size, height, width, channels].
    :param images_1: Tensor of shape [batch_size, height, width, channels].
    :param flow_0_1: Tensor of shape [batch_size, height, width, 2]. The flows from image 0 to 1.
    :param flow_1_0: Tensor of shape [batch_size, height, width, 2]. The flows from image 1 to 0.
    :param flow_0_t: Tensor of shape [batch_size, height, width, 2]. The flows from image 0 to time t.
    :param flow_1_t: Tensor of shape [batch_size, height, width, 2]. The flows from image 1 to time t.
    :param t: Tensor of shape [batch_size]. The target warp times.
    :param fill_holes: Whether to fill holes.
    :param num_blending_channels: Int. The number of channels to blend. 0 for no channels.
                                  E.g if the value is 3, the first 3 channels will be blended.
    :return: warped_a_b: The warped images using the time-scaled flow from images 0 to 1.
             warped_c_b: The warped images using the time-scaled flow from images 1 to 0.
             additional_outputs: A dictionary of other tensors.
                 holes_0_1: The holes in the warp from images 0 to 1. A value of 1.0 indicates a hole, 0.0 for no hole.
                 holes_1_0: The holes in the warp from images 1 to 0. A value of 1.0 indicates a hole, 0.0 for no hole.
    """
    with tf.name_scope('forward_warp_bidirectional'):
        # Get the scaled flows for the target time.
        t_reshaped = tf.reshape(t, [-1, 1, 1, 1])

        flow_a_b = flow_0_1 * t_reshaped if flow_0_t is None else flow_0_t
        flow_c_b = flow_1_0 * (1.0 - t_reshaped) if flow_1_t is None else flow_1_t

        # Stack things along the batch dimension for better efficiency (and worse readability).
        batch_size = tf.shape(images_0)[0]
        flow_sums = get_forward_backward_flow_sums(flow_0_1, flow_1_0)
        flow_sum_a_c, flow_sum_c_a, flow_sum_a_c_normalized, flow_sum_c_a_normalized = flow_sums
        images_0_stacked = tf.concat([images_0, images_1], axis=0)
        images_1_stacked = tf.concat([images_1, images_0], axis=0)
        flow_sum_stacked = tf.concat([flow_sum_a_c, flow_sum_c_a], axis=0)
        flow_sum_normalized_stacked = tf.concat([flow_sum_a_c_normalized, flow_sum_c_a_normalized], axis=0)
        flows_stacked = tf.concat([flow_a_b, flow_c_b], axis=0)
        full_flows_stacked = tf.concat([flow_0_1, flow_1_0], axis=0)
        t_stacked = tf.concat([t_reshaped, 1.0 - t_reshaped], axis=0)

        # Get the warps and the fill masks, and unstack.
        warped, do_fill, warp_errors = _get_warp_and_fill_mask(images_0_stacked, images_1_stacked, flow_sum_stacked,
                                                               flow_sum_normalized_stacked, flows_stacked,
                                                               full_flows_stacked, t_stacked,
                                                               num_blending_channels)
        warped_a_b, warped_c_b = warped[:batch_size], warped[batch_size:]
        do_fill_a_b, do_fill_c_b = do_fill[:batch_size], do_fill[batch_size:]
        warp_confidence_a_c, warp_confidence_c_a = warp_errors[:batch_size], warp_errors[batch_size:]

        if fill_holes:
            # Fill the holes with pixels from the other warp.
            warped_a_b += do_fill_a_b * warped_c_b
            warped_c_b += do_fill_c_b * warped_a_b

        additional_outputs = {
            'holes_0_1': do_fill_a_b,
            'holes_1_0': do_fill_c_b,
            'warp_confidence_0_1': warp_confidence_a_c,
            'warp_confidence_1_0': warp_confidence_c_a
        }
        return warped_a_b, warped_c_b, additional_outputs


def _get_warp_and_fill_mask(images_0, images_1, flow_sum, flow_sum_normalized, flow, full_flow, t,
                            num_blending_channels):
    """
    Note that flow is used to warp the image.
    We do not pass in t, as we do not want to restrict ourselves to scaling full_flow by t in this function.
    :param images_0: Tensor of shape [batch_size, height, width, channels].
                     The images at t = 0.
    :param images_1: Tensor of shape [batch_size, height, width, channels].
                     The images at t = 1.
    :param flow_sum: Tensor of shape [batch_size, height, width, 2]. The forward and backward flow sum.
    :param flow_sum_normalized: Tensor of shape [batch_size, height, width, 2].
                                The normalized forward and backward flow sum.
    :param flow: Tensor of shape [batch_size, height, width, 2].
                 The flows with which to warp images (it has been scaled by t already).
    :param full_flow: Tensor of shape [batch_size, height, width, 2].
                      The unscaled full flow from images_0 to images_1.
    :param t: Tensor of shape [batch_size]. The time between images_0 and images_1 to warp.
              This value is used for pixel brightness change interpolation only.
    :param num_blending_channels: Int. The number of channels to blend.
    :return: warped: The warped images.
             do_fill: The fill mask of shape [batch_size, height, width, 1].
                      1.0 for fill=True, 0.0 for fill=False.
             warped_confidence: The warped confidences of shape [batch_size, height, width, 1].
                                A value of 1 indicates high confidence in the accuracy of the warp.
    """
    eps = 1E-10
    s = tf.shape(images_0)
    ones_shape = (s[0], s[1], s[2], 1)
    ones = tf.ones(ones_shape)

    # Use the exponentiated squared flow distances to represent "soft" occlusion weights.
    # An occluded weight value of 1.0 means that this pixel will be occluded in the other frame.
    exp_scale = 8
    errors = tf.reduce_sum(exp_scale * tf.square(flow_sum_normalized), axis=-1, keepdims=True)
    non_occluded_weights = tf.exp(-tf.minimum(16.0, errors))
    occluded_weights = 1.0 - non_occluded_weights

    # Weight the image for brightness changes.
    # If the pixel is occluded, give its corresponding pixel a weight of zero
    # (i.e we will not do brightness interpolation for occluded pixels).
    images_0_blend = images_0[..., :num_blending_channels]
    images_1_blend_warped = backward_warp(images_1[..., :num_blending_channels], full_flow)
    lerped = occluded_weights * images_0_blend + (1.0 - occluded_weights) * images_1_blend_warped
    images_blended = t * lerped + (1.0 - t) * images_0_blend
    images = tf.concat([images_blended, images_0[..., num_blending_channels:]], axis=-1)

    # Warp the occlusion weights to the objects that occluded these pixels.
    # To do so, we must forward warp the weights and then backward warp them.
    # An occluding weight value of 1.0 means that this pixel will occlude a pixel in the other frame.
    ones_full_warped = forward_warp_bilinear(ones, full_flow)
    numerator = forward_warp_bilinear(occluded_weights, full_flow)
    pushed_occluding_weights = numerator / tf.maximum(ones_full_warped, eps)
    occluding_weights = backward_warp(pushed_occluding_weights, full_flow)

    # Assume that regions that flow out of the image are occluding regions.
    not_oob = backward_warp(ones, full_flow)
    occluding_weights = tf.clip_by_value(occluding_weights + (1.0 - not_oob), 0.0, 1.0)

    # Pixels that occlude must be involved in the conflict at the occluded pixel, and must also be non-occluded.
    # The small constant beta is added so that occluded regions are still weighted less than non-occluded ones,
    # since occluded regions have a non-zero value of occluding_weights but a small value of non_occluded_weights.
    beta = 1E-3
    warp_weights = (occluding_weights + beta) * non_occluded_weights

    # Warp the images using the occluding weights.
    weights_warped = tf.maximum(forward_warp_bilinear(warp_weights, flow), eps)
    warped = forward_warp_bilinear(warp_weights * images, flow)
    warped /= weights_warped

    # Get optional confidence warps.
    zero_kernel_3 = tf.zeros((3, 3, 1))
    errors = norm(flow_sum)

    # Remove island artifacts caused by flow boundaries,
    # and lower the intensity in areas that are holes / close to holes.
    # This helps reduce warp "chunkiness" near edges.
    ones_warped = forward_warp_bilinear(ones, flow)
    ones_warped = tf.nn.erosion2d(ones_warped, zero_kernel_3, [1, 1, 1, 1], [1, 1, 1, 1], 'SAME')
    ones_warped = tf.nn.dilation2d(ones_warped, zero_kernel_3, [1, 1, 1, 1], [1, 1, 1, 1], 'SAME')
    lower_thresh = 0.25
    attenuation = tf.minimum(ones_warped, lower_thresh) / lower_thresh
    warped *= attenuation

    # The value of -1.4 was chosen such that a 1 pixel flow inconsistency at t = 0.5 will lead to a confidence of 0.5.
    confidence_map = tf.exp(-1.4 * t * errors) * not_oob
    warped_confidence = forward_warp_bilinear(warp_weights * confidence_map, flow)
    warped_confidence /= weights_warped
    warped_confidence = tf.nn.erosion2d(warped_confidence, zero_kernel_3, [1, 1, 1, 1], [1, 1, 1, 1], 'SAME')

    # Get hole-filling masks.
    do_fill = 1.0 - attenuation

    # Mask out the holes.
    warped *= (1.0 - do_fill)
    warped_confidence *= (1.0 - do_fill)
    return warped, do_fill, warped_confidence


def get_forward_backward_flow_sums(flow_fw, flow_bw):
    """
    This is very similar to part of UnFlow's scheme for computing occlusion masks.
    :param flow_fw: Tensor of shape [B, H, W, 2].
    :param flow_bw: Tensor of shape [B, H, W, 2].
    :return: flow_sum_fw: Tensor of shape [B, H, W, 2]. The forward flow added with the backward warped backward flow.
             flow_sum_bw: Tensor of shape [B, H, W, 2]. The backward flow added with the backward warped forward flow.
             flow_sum_fw_normalized: Tensor of shape [B, H, W, 2]. Normalized difference between forward backward flows.
             flow_sum_bw_normalized. Tensor of shape [B, H, W, 2]. Normalized difference between forward backward flows.
    """
    with tf.name_scope('forward_backward_flow_sums'):
        eps = 1E-5
        flow_bw_warped = backward_warp(flow_bw, flow_fw)
        flow_fw_warped = backward_warp(flow_fw, flow_bw)
        flow_sum_fw = (flow_fw + flow_bw_warped)
        flow_sum_bw = (flow_bw + flow_fw_warped)
        flow_sum_fw_normalized = flow_sum_fw / tf.sqrt(length_sq(flow_bw_warped) + length_sq(flow_fw) + eps)
        flow_sum_bw_normalized = flow_sum_bw / tf.sqrt(length_sq(flow_fw_warped) + length_sq(flow_bw) + eps)
        return flow_sum_fw, flow_sum_bw, flow_sum_fw_normalized, flow_sum_bw_normalized
