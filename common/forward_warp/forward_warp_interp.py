import tensorflow as tf
import numpy as np
from common.forward_warp.forward_warp_bilinear import forward_warp_bilinear
from common.utils.tf import load_op_library

DISOCC_THRESH = 0.5

# Load op library.
mod = load_op_library('forward_warp_interp_op', 'build')


def forward_warp_interp(image_0, image_1, flow, t, fill_holes=False):
    """
    Based on specification in http://vision.middlebury.edu/flow/floweval-ijcv2011.pdf
    Forward warp interpolation is implemented only on CUDA. If the CUDA implementation is not available, it will fall
    back to the bilateral implementation, using only image_0 and t=1. It will scale the flow tensor to compensate.
    :param image_0: A Tensor. Features at t=0, of shape [batch_size, H, W, C].
    :param image_1: A Tensor. Features at t=1, of shape [batch_size, H, W, C].
    :param flow: A Tensor. Un-normalized flow in image pixel units, of shape [batch_size, H, W, 2].
                 Flow vectors should have (x, y) ordering.
    :param t: A Tensor of shape [batch_size]. Output is generated at 0 < t < 1 between image_0 and image_1.
    :param fill_holes: Boolean. If false, pixels that aren't flowed to will appear black (value of 0).
    """
    if mod is not None:
        return mod.forward_warp_interp(image_0, image_1, flow, t, fill_holes=fill_holes)
    else:
        return forward_warp_bilinear(image_0, t * flow), None, None, None
