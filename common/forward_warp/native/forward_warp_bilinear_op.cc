// Taken from https://github.com/simonmeister/UnFlow/blob/master/ops/forward_warp_op.cc.
// Commit bac9bbaf49be44b9e1c1f004fce4fb04b247763d.
#define EIGEN_USE_THREADS

#include <memory>
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/framework/types.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/common_shape_fns.h"

// TODO assert input flow channel count = 2, assert matching numbers in all other dims

typedef Eigen::ThreadPoolDevice CPUDevice;
typedef Eigen::GpuDevice GPUDevice;

using namespace tensorflow;

void ForwardWarpBilinear(const GPUDevice& d,
	typename TTypes<float, 4>::ConstTensor images,
	typename TTypes<float, 4>::ConstTensor flows,
	typename TTypes<float, 4>::Tensor output);

void ForwardWarpBilinearGrad(const GPUDevice& d,
	typename TTypes<float, 4>::ConstTensor input_grad,
	typename TTypes<float, 4>::ConstTensor original_images,
	typename TTypes<float, 4>::ConstTensor original_flows,
	typename TTypes<float, 4>::Tensor output_image_grad,
	typename TTypes<float, 4>::Tensor output_flow_grad);

class ForwardWarpBilinearOp : public OpKernel {
public:
	explicit ForwardWarpBilinearOp(OpKernelConstruction* context) : OpKernel(context) {}

	void Compute(OpKernelContext* context) override {
		const Tensor& image = context->input(0);
		const Tensor& flow = context->input(1);

        // Assert correct number of dimensions.
        OP_REQUIRES(context, image.dims() == 4,
                    errors::InvalidArgument("image expecting a 4-D vector."));
        OP_REQUIRES(context, flow.dims() == 4,
                    errors::InvalidArgument("flow expecting a 4-D vector."));

        // Assert dimension sizes match.
        for (int c = 0; c < 3; ++c) {
            OP_REQUIRES(context, image.dim_size(c) == flow.dim_size(c),
                        errors::InvalidArgument("Input dimensions did not match."));
        }
        OP_REQUIRES(context, flow.dim_size(3) == 2,
                    errors::InvalidArgument("Flow has invalid dimensions."));

		typename TTypes<float, 4>::ConstTensor image_data = image.tensor<float, 4>();
		typename TTypes<float, 4>::ConstTensor flow_data = flow.tensor<float, 4>();

		Tensor* output = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(0, image.shape(), &output));
		typename TTypes<float, 4>::Tensor output_data = output->tensor<float, 4>();

		ForwardWarpBilinear(context->eigen_device<GPUDevice>(), image_data, flow_data, output_data);
	}
};

class ForwardWarpBilinearOpGrad : public OpKernel {
public:
	explicit ForwardWarpBilinearOpGrad(OpKernelConstruction* context) : OpKernel(context) {}

	void Compute(OpKernelContext* context) override {
		const Tensor& input = context->input(0);
		const Tensor& original_images = context->input(1);
		const Tensor& original_flows = context->input(2);

		Tensor* output_image_grads = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(0, original_images.shape(),
			&output_image_grads));
		Tensor* output_flow_grads = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(1, original_flows.shape(),
			&output_flow_grads));

		typename TTypes<float, 4>::ConstTensor input_data = input.tensor<float, 4>();
		typename TTypes<float, 4>::ConstTensor original_images_data = original_images.tensor<float, 4>();
		typename TTypes<float, 4>::ConstTensor original_flows_data = original_flows.tensor<float, 4>();
		typename TTypes<float, 4>::Tensor output_image_grads_data = output_image_grads->tensor<float, 4>();
		typename TTypes<float, 4>::Tensor output_flow_grads_data = output_flow_grads->tensor<float, 4>();

		ForwardWarpBilinearGrad(context->eigen_device<GPUDevice>(),
			input_data, original_images_data, original_flows_data,
			output_image_grads_data, output_flow_grads_data);
	}
};

using shape_inference::DimensionHandle;
using shape_inference::ShapeHandle;

REGISTER_OP("ForwardWarpBilinear")
.Input("images: float")
.Input("flows: float")
.Output("output: float")
.SetShapeFn([](shape_inference::InferenceContext* c) {
	ShapeHandle in = c->input(0);
	DimensionHandle batch = c->Dim(in, 0);
	DimensionHandle height = c->Dim(in, 1);
	DimensionHandle width = c->Dim(in, 2);
	DimensionHandle channels = c->Dim(in, 3);
	c->set_output(0, c->MakeShape({ batch, height, width, channels }));
	return Status::OK();
});

REGISTER_OP("ForwardWarpBilinearGrad")
.Input("grads: float")
.Input("original_images: float")
.Input("original_flows: float")
.Output("output_image_grad: float")
.Output("output_flow_grad: float")
.SetShapeFn([](shape_inference::InferenceContext* c) {
	c->set_output(0, c->input(1));
	c->set_output(1, c->input(2));
	return Status::OK();
});

#if GOOGLE_CUDA

REGISTER_KERNEL_BUILDER(Name("ForwardWarpBilinear").Device(DEVICE_GPU), ForwardWarpBilinearOp);
REGISTER_KERNEL_BUILDER(Name("ForwardWarpBilinearGrad").Device(DEVICE_GPU), ForwardWarpBilinearOpGrad);

#endif // GOOGLE_CUDA
