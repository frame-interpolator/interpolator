// Taken from https://github.com/simonmeister/UnFlow/blob/master/ops/forward_warp_op.cu.cc.
// Commit bac9bbaf49be44b9e1c1f004fce4fb04b247763d.
#if GOOGLE_CUDA

#define EIGEN_USE_GPU

#define _USE_MATH_DEFINES
#include <cmath>

#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/util/cuda_kernel_helper.h"

using namespace tensorflow;

typedef Eigen::GpuDevice GPUDevice;

__global__ void ForwardWarpBilinearKernel(const int32 nthreads,
	const float* images, const float* flows,
	int batch, int height, int width, int channels,
	float* output) {
	CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
		// out_idx = c + channels * (src_x + width * (src_y + height * b)).
		int idx = out_idx;
		const int c = idx % channels;
		idx /= channels;
		const int pixel_index = idx;
		const int flow_index = pixel_index * 2;
		const int src_x = idx % width;
		idx /= width;
		const int src_y = idx % height;
		const int b = idx / height;

		const float target_x = src_x + flows[flow_index];
		const float target_y = src_y + flows[flow_index + 1];

		const int x0 = floorf(target_x);
		const int x1 = x0 + 1;
		const int y0 = floorf(target_y);
		const int y1 = y0 + 1;

		const float w_right = target_x - x0;
		const float w_left = x1 - target_x;
		const float w_bottom = target_y - y0;
		const float w_top = y1 - target_y;

		const float image_value = images[out_idx];

#define IMG_OFFSET(iy, ix) (c + channels * (ix + width * (iy + height * b)))
		// top-left neighbor
		if (x0 >= 0 && x0 < width && y0 >= 0 && y0 < height) {
			CudaAtomicAdd(output + IMG_OFFSET(y0, x0), w_left * w_top * image_value);
		}

		// top-right neigbor
		if (x1 >= 0 && x1 < width && y0 >= 0 && y0 < height) {
			CudaAtomicAdd(output + IMG_OFFSET(y0, x1), w_right * w_top * image_value);
		}

		// bottom-left neighbor
		if (x0 >= 0 && x0 < width && y1 >= 0 && y1 < height) {
			CudaAtomicAdd(output + IMG_OFFSET(y1, x0), w_left * w_bottom * image_value);
		}

		// bottom-right neighbor
		if (x1 >= 0 && x1 < width && y1 >= 0 && y1 < height) {
			CudaAtomicAdd(output + IMG_OFFSET(y1, x1), w_right * w_bottom * image_value);
		}
#undef IMG_OFFSET
	}
}

__global__ void ForwardWarpBilinearGradKernel(const int32 nthreads,
	const float* input_grad, const float* images, const float* flows,
	int batch, int height, int width, int channels,
	float* output_image_grad, float* output_flow_grad) {
	CUDA_1D_KERNEL_LOOP(in_idx, nthreads) {
		// in_idx = c + channels * (src_x + width * (src_y + height * b)).
		int idx = in_idx;
		const int c = idx % channels;
		idx /= channels;
		const int pixel_index = idx;
		const int flow_index = pixel_index * 2;
		const int src_x = idx % width;
		idx /= width;
		const int src_y = idx % height;
		const int b = idx / height;

		const float target_x = src_x + flows[flow_index];
		const float target_y = src_y + flows[flow_index + 1];

		const int x0 = floorf(target_x);
		const int x1 = x0 + 1;
		const int y0 = floorf(target_y);
		const int y1 = y0 + 1;

		const float w_right = target_x - x0;
		const float w_left = x1 - target_x;
		const float w_bottom = target_y - y0;
		const float w_top = y1 - target_y;

		float du = 0.0;
		float dv = 0.0;
		float d_img = 0.0;
		float din = 0.0;
		float px = 0.0;
		const float image_value = images[in_idx];

#define IMG_OFFSET(iy, ix) (c + channels * (ix + width * (iy + height * b)))
		// top-left neighbor
		if (x0 >= 0 && x0 < width && y0 >= 0 && y0 < height) {
			din = input_grad[IMG_OFFSET(y0, x0)];
			d_img += w_left * w_top * din;
			px = image_value * din;
			du -= w_top * px;
			dv -= w_left * px;
		}

		// top-right neigbor
		if (x1 >= 0 && x1 < width && y0 >= 0 && y0 < height) {
			din = input_grad[IMG_OFFSET(y0, x1)];
			d_img += w_right * w_top * din;
			px = image_value * din;
			du += w_top * px;
			dv -= w_right * px;
		}

		// bottom-left neighbor
		if (x0 >= 0 && x0 < width && y1 >= 0 && y1 < height) {
			din = input_grad[IMG_OFFSET(y1, x0)];
			d_img += w_left * w_bottom * din;
			px = image_value * din;
			du -= w_bottom * px;
			dv += w_left * px;
		}

		// bottom-right neighbor
		if (x1 >= 0 && x1 < width && y1 >= 0 && y1 < height) {
			din = input_grad[IMG_OFFSET(y1, x1)];
			d_img += w_right * w_bottom * din;
			px = image_value * din;
			du += w_bottom * px;
			dv += w_right * px;
		}

		output_image_grad[in_idx] = d_img;
		CudaAtomicAdd(output_flow_grad + flow_index, du);
		CudaAtomicAdd(output_flow_grad + flow_index + 1, dv);
	}
#undef IMG_OFFSET
}

void ForwardWarpBilinear(const GPUDevice& d,
	typename TTypes<float, 4>::ConstTensor images,
	typename TTypes<float, 4>::ConstTensor flows,
	typename TTypes<float, 4>::Tensor output) {
	const int batch = images.dimension(0);
	const int height = images.dimension(1);
	const int width = images.dimension(2);
	const int channels = images.dimension(3);

	const int total_count = batch * height * width * channels;
	if (total_count == 0) return;

	CudaLaunchConfig config;

	// Initialize output with all zeros.
	config = GetCudaLaunchConfig(total_count, d);
	SetZero << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
		config.virtual_thread_count, output.data());

	config = GetCudaLaunchConfig(total_count, d);
	ForwardWarpBilinearKernel
		<< <config.block_count, config.thread_per_block, 0, d.stream() >> >(
			config.virtual_thread_count, images.data(), flows.data(),
			batch, height, width, channels,
			output.data());
}

void ForwardWarpBilinearGrad(const GPUDevice& d,
	typename TTypes<float, 4>::ConstTensor input_grad,
	typename TTypes<float, 4>::ConstTensor original_images,
	typename TTypes<float, 4>::ConstTensor original_flows,
	typename TTypes<float, 4>::Tensor output_image_grad,
	typename TTypes<float, 4>::Tensor output_flow_grad) {
	const int batch = input_grad.dimension(0);
	const int height = input_grad.dimension(1);
	const int width = input_grad.dimension(2);
	const int channels = input_grad.dimension(3);

	int total_count = batch * height * width * 2;
	if (total_count == 0) return;

	// Initialize output_flow_grad with all zeros.
	CudaLaunchConfig config = GetCudaLaunchConfig(total_count, d);
	SetZero << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
		config.virtual_thread_count, output_flow_grad.data());

	// Initialize output_image_grad with all zeros.
	total_count = batch * height * width * channels;
	config = GetCudaLaunchConfig(total_count, d);
	SetZero << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
		config.virtual_thread_count, output_image_grad.data());

	// Accumulate.
	config = GetCudaLaunchConfig(total_count, d);
	ForwardWarpBilinearGradKernel
		<< <config.block_count, config.thread_per_block, 0, d.stream() >> >(
			config.virtual_thread_count, input_grad.data(),
			original_images.data(), original_flows.data(),
			batch, height, width, channels,
			output_image_grad.data(), output_flow_grad.data());
}

#endif  // GOOGLE_CUDA
