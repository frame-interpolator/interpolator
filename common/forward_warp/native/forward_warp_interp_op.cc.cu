#if GOOGLE_CUDA

#define EIGEN_USE_GPU

#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/util/cuda_kernel_helper.h"

using namespace tensorflow;

typedef Eigen::GpuDevice GPUDevice;

__global__ void GenerateFlowAtTKernel(const int32 nthreads,
                                      const float* image_0s,
                                      const float* bwarp_image_1s,
                                      const float* flows,
                                      const float* ts,
                                      const float* p_buffers,
                                      int batch,
                                      int height,
                                      int width,
                                      int channels,
                                      float* output) {
    CUDA_1D_KERNEL_LOOP(in_idx, nthreads) {
        // in_idx = x + width * (y + height * b)
        int idx = in_idx;
        const int flow_index = in_idx * 2;
        const int src_x = idx % width;
        idx /= width;
        const int src_y = idx % height;
        const int b = idx / height;

        const float t = ts[b];

        const float target_x = src_x + t * flows[flow_index];
        const float target_y = src_y + t * flows[flow_index + 1];

        // Compute splat indices.
        const int x_left = floorf(target_x - 0.5);
        const int x_right = x_left + 1;
        const int y_top = floorf(target_y - 0.5);
        const int y_bot = y_top + 1;

        // Compute photoconsistency at in_idx.
        float photoconsistency = 0;
        #pragma unroll 3
        for (int c = 0; c < channels; ++c) {
            const float diff = image_0s[in_idx * channels + c] - bwarp_image_1s[in_idx * channels + c];
            photoconsistency += fabs(diff);
        }

#define FLOW_IF_PHOTOCONSISTENT(x, y) \
if (x >= 0 && x < width && y >= 0 && y < height) { \
    const int target_index = x + width * (y + height * b); \
    if (photoconsistency <= p_buffers[target_index] * 1.00000001) { \
        output[target_index * 2] = flows[flow_index]; \
        output[target_index * 2 + 1] = flows[flow_index + 1]; \
    } \
}
        FLOW_IF_PHOTOCONSISTENT(x_left, y_top)
        FLOW_IF_PHOTOCONSISTENT(x_left, y_bot)
        FLOW_IF_PHOTOCONSISTENT(x_right, y_top)
        FLOW_IF_PHOTOCONSISTENT(x_right, y_bot)
#undef FLOW_IF_PHOTOCONSISTENT
    }
}

__global__ void PhotoconsistencyAtTKernel(const int32 nthreads,
                                          const float* image_0s,
                                          const float* bwarp_image_1s,
                                          const float* flows,
                                          const float* ts,
                                          int batch,
                                          int height,
                                          int width,
                                          int channels,
                                          float* output) {
    CUDA_1D_KERNEL_LOOP(in_idx, nthreads) {
        // in_idx = x + width * (y + height * b)
        int idx = in_idx;
        const int flow_index = in_idx * 2;
        const int src_x = idx % width;
        idx /= width;
        const int src_y = idx % height;
        const int b = idx / height;

        const float t = ts[b];

        const float target_x = src_x + t * flows[flow_index];
        const float target_y = src_y + t * flows[flow_index + 1];

        // Compute splat indices.
        const int x_left = floorf(target_x - 0.5);
        const int x_right = floorf(target_x + 0.5);
        const int y_top = floorf(target_y - 0.5);
        const int y_bot = floorf(target_y + 0.5);
        const int y_top_offset = width * (y_top + height * b);
        const int y_bot_offset = width * (y_bot + height * b);
        const int top_left_idx = x_left + y_top_offset;
        const int top_right_idx = x_right + y_top_offset;
        const int bot_right_idx = x_right + y_bot_offset;
        const int bot_left_idx = x_left + y_bot_offset;

        // Compute photoconsistency at in_idx.
        float photoconsistency = 0;
        #pragma unroll 3
        for (int c = 0; c < channels; ++c) {
            const float diff = image_0s[in_idx * channels + c] - bwarp_image_1s[in_idx * channels + c];
            photoconsistency += fabs(diff);
        }

#define SAFE_SET_IF_MIN(x, y) \
if (x >= 0 && x < width && y >= 0 && y < height) { \
    CudaAtomicMin(output + x + width * (y + height * b), photoconsistency); \
}
        SAFE_SET_IF_MIN(x_left, y_top)
        SAFE_SET_IF_MIN(x_left, y_bot)
        SAFE_SET_IF_MIN(x_right, y_top)
        SAFE_SET_IF_MIN(x_right, y_bot)
    }
#undef SAFE_SET_IF_MIN
}

__global__ void HoleFillKernel(const int32 nthreads,
                               float* flows,
                               int batch,
                               int height,
                               int width,
                               int* num_filled_ptr) {
    CUDA_1D_KERNEL_LOOP(idx, nthreads) {
        // idx = src_x + width * (src_y + height * b)
        const int flow_idx = idx * 2;
        const int src_x = idx % width;
        const int src_y = idx / width % height;
        const int b = idx / width / height;

        if (isnan(flows[flow_idx])) {
            const int ceil_half_length = 3;
            int num_avged = 0;
            float avg_x = 0;
            float avg_y = 0;
            #pragma unroll
            for (int xi = 0; xi <= ceil_half_length * 2; ++xi) {
                const int x = src_x - ceil_half_length + xi;
                const bool x_in_bounds = x >= 0 && x < width;
                #pragma unroll
                for (int yi = 0; yi <= ceil_half_length * 2; ++yi) {
                    const int y = src_y - ceil_half_length + yi;
                    const bool y_in_bounds = y >= 0 && y < height;
                    const int new_flow_idx = 2 * (x + width * (y + height * b));
                    if (x_in_bounds && y_in_bounds && !isnan(flows[new_flow_idx])) {
                        avg_x += flows[new_flow_idx];
                        avg_y += flows[new_flow_idx + 1];
                        ++num_avged;
                    }
                }
            }
            if (num_avged > 0) {
                flows[flow_idx] = avg_x / num_avged;
                flows[flow_idx + 1] = avg_y / num_avged;
                CudaAtomicAdd(num_filled_ptr, 1);
            }
        }
    }
}

__global__ void EstimateOcclusionMasksKernel(const int32 nthreads,
                                             const float* flows,
                                             const float* flows_at_t_1,
                                             const float* bwarp_flows_at_t_1,
                                             int batch,
                                             int height,
                                             int width,
                                             bool* occlusion_mask_0,
                                             bool* occlusion_mask_1) {
    CUDA_1D_KERNEL_LOOP(idx, nthreads) {
        // idx = src_x + width * (src_y + height * b)
        const int flow_idx = idx * 2;
        const int src_x = idx % width;
        const int src_y = idx / width % height;
        const int b = idx / width / height;

        const float dx = flows[flow_idx];
        const float dy = flows[flow_idx + 1];

        const int t_1_x = src_x + floorf(dx);
        const int t_1_y = src_y + floorf(dy);
        const int t_1_idx = 2 * (t_1_x + width * (t_1_y + height * b));

        if (isnan(flows_at_t_1[flow_idx])) {
            occlusion_mask_1[idx] = true;
        }

        const float x_diff = dx - bwarp_flows_at_t_1[flow_idx];
        const float y_diff = dy - bwarp_flows_at_t_1[flow_idx + 1];

        if (x_diff * x_diff + y_diff * y_diff > 1) {
            occlusion_mask_0[idx] = true;

            // Dilate except at boundaries.
            if (src_x >= 1 && src_x < width - 1 && src_y >= 1 && src_y < height - 1) {
                const int idx_above = idx - width;
                const int idx_below = idx + width;
                occlusion_mask_0[idx_above-1] = true;
                occlusion_mask_0[idx_above] = true;
                occlusion_mask_0[idx_above+1] = true;
                occlusion_mask_0[idx-1] = true;
                occlusion_mask_0[idx+1] = true;
                occlusion_mask_0[idx_below-1] = true;
                occlusion_mask_0[idx_below] = true;
                occlusion_mask_0[idx_below+1] = true;
            }
        }
    }
}

__global__ void InterpolatePixelsKernel(const int32 nthreads,
                                        const float* image_0s,
                                        const float* image_1s,
                                        const float* flows_at_t,
                                        const float* ts,
                                        const bool* occlusion_mask_0,
                                        const bool* occlusion_mask_1,
                                        int batch,
                                        int height,
                                        int width,
                                        int channels,
                                        float* output) {
    CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
        // idx = c + channels * (src_x + width * (src_y + height * b)
        int idx = out_idx;
        const int c = idx % channels;
        idx /= channels;
        const int pixel_index = idx;
        const int flow_index = pixel_index * 2;
        const int src_x = idx % width;
        idx /= width;
        const int src_y = idx % height;
        const int b = idx / height;

        const float t = ts[b];

        if (!isnan(flows_at_t[flow_index]))
        {
            const int x0 = floorf(src_x - t * flows_at_t[flow_index]);
            const int y0 = floorf(src_y - t * flows_at_t[flow_index + 1]);
            const int x1 = floorf(src_x + (1 - t) * flows_at_t[flow_index]);
            const int y1 = floorf(src_y + (1 - t) * flows_at_t[flow_index + 1]);

            const int pixel_index_0 = x0 + width * (y0 + height * b);
            const int pixel_index_1 = x1 + width * (y1 + height * b);
            const int index_0 = pixel_index_0 * channels + c;
            const int index_1 = pixel_index_1 * channels + c;

            bool can_use_image_0 = x0 >= 0 && x0 < width && y0 >= 0 && y0 < height;
            bool can_use_image_1 = x1 >= 0 && x1 < width && y1 >= 0 && y1 < height;

            if (can_use_image_0 && occlusion_mask_0[pixel_index_0]) {
                can_use_image_1 = false;
            }
            if (can_use_image_1 && occlusion_mask_1[pixel_index_1]) {
                can_use_image_0 = false;
            }

            if (can_use_image_0 && can_use_image_1) {
                output[out_idx] = (1 - t) * image_0s[index_0] + t * image_1s[index_1];
            } else if (can_use_image_0) {
                output[out_idx] = image_0s[index_0];
            } else if (can_use_image_1) {
                output[out_idx] = image_1s[index_1];
            }
        }
    }
}

void GenerateFlowAtT(const GPUDevice& d,
                     typename TTypes<float, 4>::ConstTensor image_0s,
                     typename TTypes<float, 4>::Tensor bwarp_image_1s,
                     typename TTypes<float, 4>::ConstTensor flows,
                     typename TTypes<float, 1>::ConstTensor ts,
                     typename TTypes<float, 4>::Tensor output,
                     bool fill_holes) {
    const int batch = flows.dimension(0);
    const int height = flows.dimension(1);
    const int width = flows.dimension(2);
    const int channels = image_0s.dimension(3);
    if (flows.dimension(3) != 2) return;

    const int total_count = batch * height * width;
    if (total_count * channels == 0) return;

    float* p_buffer = nullptr;
    cudaMalloc(&p_buffer, sizeof(float) * batch * height * width);
    if (!p_buffer) return;

    // NAN out output tensor.
    CudaLaunchConfig config = GetCudaLaunchConfig(total_count * 2, d);
    SetToValue
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, output.data(), NAN);

    // Set p_buffer to arbitrary large value.
    config = GetCudaLaunchConfig(total_count, d);
    SetToValue
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, p_buffer, 1e30f);

    PhotoconsistencyAtTKernel
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, image_0s.data(), bwarp_image_1s.data(),
            flows.data(), ts.data(), batch, height, width, channels, p_buffer);

    GenerateFlowAtTKernel
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, image_0s.data(), bwarp_image_1s.data(),
            flows.data(), ts.data(), p_buffer, batch, height, width, channels,
            output.data());

    cudaFree(p_buffer);
    p_buffer = nullptr;

    if (!fill_holes) return;

    int num_filled;
    int* num_filled_ptr = nullptr;
    cudaMalloc(&num_filled_ptr, sizeof(int));
    if (!num_filled_ptr) return;
    do {
        num_filled = 0;
        cudaMemcpy(num_filled_ptr, &num_filled, sizeof(int), cudaMemcpyHostToDevice);
        HoleFillKernel
            <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
                config.virtual_thread_count, output.data(), batch, height, width, num_filled_ptr);
        cudaMemcpy(&num_filled, num_filled_ptr, sizeof(int), cudaMemcpyDeviceToHost);
    } while (num_filled > 10);
    cudaFree(num_filled_ptr);
}

void GenerateFlowAtT1(const GPUDevice& d,
                      typename TTypes<float, 4>::ConstTensor image_0s,
                      typename TTypes<float, 4>::Tensor bwarp_image_1s,
                      typename TTypes<float, 4>::ConstTensor flows,
                      typename TTypes<float, 4>::Tensor output) {
    const int batch = flows.dimension(0);
    const int height = flows.dimension(1);
    const int width = flows.dimension(2);
    const int channels = image_0s.dimension(3);
    if (flows.dimension(3) != 2) return;

    const int total_count = batch * height * width;
    if (total_count == 0) return;

    float* p_buffer = nullptr;
    float* ts = nullptr;
    float* block = nullptr;
    const size_t p_buffer_size = batch * height * width;
    const size_t ts_size = batch;
    cudaMalloc(&block, sizeof(float) * (p_buffer_size + ts_size));
    if (!block) return;
    p_buffer = block;
    ts = block + p_buffer_size;

    // Fill t "tensor" with 1s.
    CudaLaunchConfig config = GetCudaLaunchConfig(batch, d);
    SetToValue
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, ts, 1.f);

    // NAN out output tensor.
    config = GetCudaLaunchConfig(total_count * 2, d);
    SetToValue
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, output.data(), NAN);

    config = GetCudaLaunchConfig(total_count, d);

    // Set p_buffer to arbitrary large value.
    config = GetCudaLaunchConfig(total_count, d);
    SetToValue
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, p_buffer, 1e30f);

    PhotoconsistencyAtTKernel
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, image_0s.data(), bwarp_image_1s.data(),
            flows.data(), ts, batch, height, width, channels, p_buffer);

    GenerateFlowAtTKernel
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, image_0s.data(), bwarp_image_1s.data(),
            flows.data(), ts, p_buffer, batch, height, width, channels,
            output.data());

    cudaFree(block);
}

void EstimateOcclusionMasks(const GPUDevice& d,
                            typename TTypes<float, 4>::ConstTensor flows,
                            typename TTypes<float, 4>::Tensor flows_at_t_1,
                            typename TTypes<float, 4>::Tensor bwarp_flows_at_t_1,
                            typename TTypes<bool, 4>::Tensor occlusion_mask_0,
                            typename TTypes<bool, 4>::Tensor occlusion_mask_1) {
    const int batch = occlusion_mask_0.dimension(0);
    const int height = occlusion_mask_0.dimension(1);
    const int width = occlusion_mask_0.dimension(2);
    if (occlusion_mask_0.dimension(3) != 1) return;

    const int total_count = batch * height * width;
    if (total_count == 0) return;
    CudaLaunchConfig config = GetCudaLaunchConfig(total_count, d);
    SetZero
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, occlusion_mask_0.data());
    SetZero
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, occlusion_mask_1.data());

    EstimateOcclusionMasksKernel
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, flows.data(), flows_at_t_1.data(),
            bwarp_flows_at_t_1.data(),
            batch, height, width, occlusion_mask_0.data(),
            occlusion_mask_1.data());
}

void InterpolatePixels(const GPUDevice& d,
                       typename TTypes<float, 4>::ConstTensor image_0s,
                       typename TTypes<float, 4>::ConstTensor image_1s,
                       typename TTypes<float, 4>::Tensor flows_at_t,
                       typename TTypes<float, 1>::ConstTensor t,
                       typename TTypes<bool, 4>::Tensor occlusion_mask_0s,
                       typename TTypes<bool, 4>::Tensor occlusion_mask_1s,
                       typename TTypes<float, 4>::Tensor output_images) {
    const int batch = output_images.dimension(0);
    const int height = output_images.dimension(1);
    const int width = output_images.dimension(2);
    const int channels = output_images.dimension(3);

    const int total_count = batch * height * width * channels;
    if (total_count == 0) return;

    CudaLaunchConfig config = GetCudaLaunchConfig(total_count, d);
    SetZero
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, output_images.data());

    InterpolatePixelsKernel
        <<< config.block_count, config.thread_per_block, 0, d.stream() >>>(
            config.virtual_thread_count, image_0s.data(), image_1s.data(),
            flows_at_t.data(), t.data(), occlusion_mask_0s.data(),
            occlusion_mask_1s.data(), batch, height, width, channels,
            output_images.data());
}

#endif // GOOGLE_CUDA