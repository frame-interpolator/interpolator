#define EIGEN_USE_THREADS

#include <memory>
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/framework/types.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/common_shape_fns.h"

#include "../pwcnet/warp/native/backward_warp_op.h"

typedef Eigen::GpuDevice GPUDevice;

using namespace tensorflow;

// See section 3.3.2, step (1) in http://vision.middlebury.edu/flow/floweval-ijcv2011.pdf
void GenerateFlowAtT(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor image_0,
    typename TTypes<float, 4>::Tensor bwarp_image_1,
    typename TTypes<float, 4>::ConstTensor flows,
    typename TTypes<float, 1>::ConstTensor t,
    typename TTypes<float, 4>::Tensor output_flow_t,
    bool fill_holes);

void GenerateFlowAtT1(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor image_0,
    typename TTypes<float, 4>::Tensor bwarp_image_1,
    typename TTypes<float, 4>::ConstTensor flows,
    typename TTypes<float, 4>::Tensor output_flow_t);

// See section 3.3.2, step (3) in http://vision.middlebury.edu/flow/floweval-ijcv2011.pdf
// Occlusion masks are of shape [batch, height, width, 1].
void EstimateOcclusionMasks(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor flows,
    typename TTypes<float, 4>::Tensor flows_at_t_1,
    typename TTypes<float, 4>::Tensor bwarp_flows_at_t_1,
    typename TTypes<bool, 4>::Tensor occlusion_mask_0,
    typename TTypes<bool, 4>::Tensor occlusion_mask_1);

// See section 3.3.2, step (4) in http://vision.middlebury.edu/flow/floweval-ijcv2011.pdf
void InterpolatePixels(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor image_0s,
    typename TTypes<float, 4>::ConstTensor image_1s,
    typename TTypes<float, 4>::Tensor flows_at_t,
    typename TTypes<float, 1>::ConstTensor t,
    typename TTypes<bool, 4>::Tensor occlusion_mask_0s,
    typename TTypes<bool, 4>::Tensor occlusion_mask_1s,
    typename TTypes<float, 4>::Tensor output_images);

// Tensorflow Op (see https://www.tensorflow.org/extend/adding_an_op).
class ForwardWarpInterpOp : public OpKernel {
public:
    explicit ForwardWarpInterpOp(OpKernelConstruction* context) : OpKernel(context) {
        OP_REQUIRES_OK(context, context->GetAttr("fill_holes", &fill_holes_));
    }

    void Compute(OpKernelContext* context) override {
        const Tensor& input_image_0s = context->input(0);
        const Tensor& input_image_1s = context->input(1);
        const Tensor& input_flows = context->input(2);
        const Tensor& input_ts = context->input(3);

        // Assert correct number of dimensions.
        OP_REQUIRES(context, input_ts.dims() == 1,
                    errors::InvalidArgument("input_ts expecting a 1-D vector."));
        OP_REQUIRES(context, input_image_0s.dims() == 4,
                    errors::InvalidArgument("input_image_0s expecting a 4-D vector."));
        OP_REQUIRES(context, input_image_1s.dims() == 4,
                    errors::InvalidArgument("input_image_1s expecting a 4-D vector."));
        OP_REQUIRES(context, input_flows.dims() == 4,
                    errors::InvalidArgument("input_flows expecting a 4-D vector."));

        // Assert dimension sizes match.
        OP_REQUIRES(context, input_ts.dim_size(0) == input_image_0s.dim_size(0),
                    errors::InvalidArgument("input_ts has invalid dimensions."));
        for (int c = 0; c < 3; ++c) {
            OP_REQUIRES(context, input_image_0s.dim_size(c) == input_image_1s.dim_size(c)
                        && input_image_0s.dim_size(c) == input_flows.dim_size(c),
                        errors::InvalidArgument("Input dimensions did not match."));
        }
        OP_REQUIRES(context, input_image_0s.dim_size(3) == input_image_1s.dim_size(3),
                    errors::InvalidArgument("Input dimensions did not match."));
        OP_REQUIRES(context, input_flows.dim_size(3) == 2,
                    errors::InvalidArgument("input_flows has invalid dimensions."));

        TensorShape occlusion_mask_shape = input_flows.shape();
        occlusion_mask_shape.set_dim(3, 1);

        Tensor inout_bwarp_image_1;
        Tensor inout_flow_at_t_1;
        Tensor inout_bwarp_flow_at_t_1;

        OP_REQUIRES_OK(context, context->allocate_temp(DT_FLOAT, input_image_1s.shape(),
            &inout_bwarp_image_1));
        OP_REQUIRES_OK(context, context->allocate_temp(DT_FLOAT, input_flows.shape(),
            &inout_flow_at_t_1));
        OP_REQUIRES_OK(context, context->allocate_temp(DT_FLOAT, input_flows.shape(),
            &inout_bwarp_flow_at_t_1));

        Tensor* output_images = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(0, input_image_0s.shape(),
            &output_images));
        Tensor* flow_at_t = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(1, input_flows.shape(),
            &flow_at_t));
        Tensor* occlusion_mask_0 = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(2, occlusion_mask_shape,
            &occlusion_mask_0));
        Tensor* occlusion_mask_1 = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(3, occlusion_mask_shape,
            &occlusion_mask_1));

        typename TTypes<float, 4>::ConstTensor image_0_data = input_image_0s.tensor<float, 4>();
        typename TTypes<float, 4>::ConstTensor image_1_data = input_image_1s.tensor<float, 4>();
        typename TTypes<float, 4>::ConstTensor flow_data = input_flows.tensor<float, 4>();
        typename TTypes<float, 1>::ConstTensor t_data = input_ts.tensor<float, 1>();
        typename TTypes<float, 4>::Tensor bwarp_image_1_data = inout_bwarp_image_1.tensor<float, 4>();
        typename TTypes<float, 4>::Tensor flow_at_t_data = flow_at_t->tensor<float, 4>();
        typename TTypes<float, 4>::Tensor flow_at_t_1_data = inout_flow_at_t_1.tensor<float, 4>();
        typename TTypes<float, 4>::Tensor bwarp_flow_at_t_1_data = inout_bwarp_flow_at_t_1.tensor<float, 4>();
        typename TTypes<bool, 4>::Tensor occlusion_mask_0_data = occlusion_mask_0->tensor<bool, 4>();
        typename TTypes<bool, 4>::Tensor occlusion_mask_1_data = occlusion_mask_1->tensor<bool, 4>();
        typename TTypes<float, 4>::Tensor output_images_data = output_images->tensor<float, 4>();

        // Backward warp image1.
        BackwardWarp(context->eigen_device<GPUDevice>(),
            image_1_data, flow_data, bwarp_image_1_data);

        // Generate flow at t=t, full of black holes.
        GenerateFlowAtT(context->eigen_device<GPUDevice>(),
            image_0_data, bwarp_image_1_data, flow_data, t_data, flow_at_t_data, fill_holes_);

        // Generate flow at t=1, used to generate occlusion mask at t=1.
        GenerateFlowAtT1(context->eigen_device<GPUDevice>(),
            image_0_data, bwarp_image_1_data, flow_data, flow_at_t_1_data);

        // Cast flow at t=1 from Tensor to ConstTensor so we can backwards warp it.
        const Tensor& const_test = inout_flow_at_t_1;
        typename TTypes<float, 4>::ConstTensor const_flow_at_t_1_data = const_test.tensor<float, 4>();

        BackwardWarp(context->eigen_device<GPUDevice>(),
            const_flow_at_t_1_data, flow_data, bwarp_flow_at_t_1_data);

        EstimateOcclusionMasks(context->eigen_device<GPUDevice>(),
            flow_data, flow_at_t_1_data, bwarp_flow_at_t_1_data, occlusion_mask_0_data,
            occlusion_mask_1_data);

        InterpolatePixels(context->eigen_device<GPUDevice>(),
            image_0_data, image_1_data, flow_at_t_data, t_data,
            occlusion_mask_0_data, occlusion_mask_1_data, output_images_data);
    }

private:
    bool fill_holes_;
};

// See https://www.tensorflow.org/extend/adding_an_op
REGISTER_OP("ForwardWarpInterp")
.Attr("fill_holes: bool = false")
.Input("image_0s: float")
.Input("image_1s: float")
.Input("flows: float")
.Input("ts: float")
.Output("warped_images: float")
.Output("warped_flows: float")
.Output("occlusion_mask_0: bool")
.Output("occlusion_mask_1: bool")
.SetShapeFn(shape_inference::UnchangedShape);

#if GOOGLE_CUDA

REGISTER_KERNEL_BUILDER(Name("ForwardWarpInterp").Device(DEVICE_GPU), ForwardWarpInterpOp);

#endif // GOOGLE_CUDA