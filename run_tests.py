import os
import unittest
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--fail_fast', dest='fail_fast', action='store_true',
                    help='Whether to fail fast.')
parser.set_defaults(fail_fast=False)
args = parser.parse_args()

original_cwd = os.getcwd()
loader = unittest.TestLoader()
start_dir = '.'
suite = loader.discover(start_dir, pattern='*_test.py')

runner = unittest.TextTestRunner(failfast=args.fail_fast)
os.chdir(original_cwd)
runner.run(suite)
