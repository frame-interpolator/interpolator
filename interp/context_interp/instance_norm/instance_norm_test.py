import os
import unittest
import numpy as np
import tensorflow as tf
from interp.context_interp.instance_norm.instance_norm import InstanceNorm


def _get_mean_and_var(x):
    mean_np = np.mean(x, axis=(1, 2), keepdims=True)
    var_np = np.mean(np.power(x - mean_np, 2), axis=(1, 2), keepdims=True)
    return mean_np, var_np


def _avg_abs_diff(x, y):
    return np.mean(np.abs(x - y))
    

class TestInstanceNorm(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        self.normalizer = InstanceNorm(name='instance_norm_test')

    def test_norm(self):
        input_shape = (3, 12, 12, 3)
        input_tensor = tf.placeholder(tf.float32, shape=input_shape)
        input_np = np.ones(input_shape)
        input_np[:, 4:5, 6:7, :2] = -1.0
        input_np[:, 4:5, 6:7, 2:] = 4.0

        normalized = self.normalizer.normalize(input_tensor)
        unnormalized = self.normalizer.unnormalize(normalized)
        normalized_np, unnormalized_np = self.sess.run([normalized, unnormalized],
                                                       feed_dict={input_tensor: input_np})

        mean_np, var_np = _get_mean_and_var(normalized_np)

        self.assertAlmostEqual(0.0, _avg_abs_diff(np.zeros(mean_np.shape), mean_np), places=5)
        self.assertAlmostEqual(0.0, _avg_abs_diff(np.ones(var_np.shape), var_np), places=5)
        self.assertTupleEqual(input_shape, normalized_np.shape)
        self.assertTupleEqual(input_shape, unnormalized_np.shape)
        self.assertAlmostEqual(0.0, _avg_abs_diff(input_np, unnormalized_np), places=5)

    def test_norm_multi(self):
        input_shape = [2, 12, 12, 3]
        input_tensor_1 = tf.placeholder(tf.float32, shape=input_shape)
        input_tensor_2 = tf.placeholder(tf.float32, shape=input_shape)
        input_tensor_3 = tf.placeholder(tf.float32, shape=input_shape)
        input_np_1 = np.zeros(input_shape)
        input_np_1[:, 4:5, 5:6, :1] = 10.0
        input_np_1[:, 4:5, 5:6, 1:] = 3.0
        input_np_2 = np.zeros(input_shape)
        input_np_2[:, 3:5, 3:4, :2] = -2.0
        input_np_2[:, 3:5, 3:4, 2:] = -5.0
        input_np_2[:1, 6:7, 2:6, :] = -1.0
        input_np_2[1:, 6:7, 2:6, :] = 4.0

        mean_np_1_before, var_np_1_before = _get_mean_and_var(input_np_1)
        mean_np_2_before, var_np_2_before = _get_mean_and_var(input_np_2)
        normalized_input_np = np.ones(input_shape)
        normalized_input_np[:, 2:5, 3:4, :] = 0.0
        mean, var = _get_mean_and_var(normalized_input_np)
        normalized_input_np = (normalized_input_np - mean) / np.sqrt(var)

        # Build graph and get outputs.
        normalized_1 = self.normalizer.normalize(input_tensor_1)
        normalized_2 = self.normalizer.normalize(input_tensor_2)
        unnormalized = self.normalizer.unnormalize(input_tensor_3)
        normalized_np_1, normalized_np_2, unnormalized_np = self.sess.run([normalized_1, normalized_2, unnormalized],
                                                                           feed_dict={input_tensor_1: input_np_1,
                                                                                      input_tensor_2: input_np_2,
                                                                                      input_tensor_3: normalized_input_np})

        # Normalized tensors should have 0 mean and unit variance.
        mean_np_1, var_np_1 = _get_mean_and_var(normalized_np_1)
        mean_np_2, var_np_2 = _get_mean_and_var(normalized_np_2)
        self.assertAlmostEqual(0.0, _avg_abs_diff(np.zeros(mean_np_1.shape), mean_np_1), places=5)
        self.assertAlmostEqual(0.0, _avg_abs_diff(np.ones(var_np_1.shape), var_np_1), places=5)
        self.assertAlmostEqual(0.0, _avg_abs_diff(np.zeros(mean_np_2.shape), mean_np_2), places=5)
        self.assertAlmostEqual(0.0, _avg_abs_diff(np.ones(var_np_2.shape), var_np_2), places=5)

        # Check that applying unnormalization on normalized input gives expected averaged mean and variance.
        avg_mean = (mean_np_1_before + mean_np_2_before) / 2.0
        avg_var = (var_np_1_before + var_np_2_before) / 2.0
        output_mean, output_var = _get_mean_and_var(unnormalized_np)
        self.assertAlmostEqual(0.0, _avg_abs_diff(avg_mean, output_mean), places=5)
        self.assertAlmostEqual(0.0, _avg_abs_diff(avg_var, output_var), places=5)


if __name__ == '__main__':
    unittest.main()
