import tensorflow as tf


class InstanceNorm:
    """
    Instance norm without learnable parameters, but stores the moments for unnormalization.
    See section 3.3 in https://arxiv.org/pdf/1803.10967.pdf.
    """
    def __init__(self, name='instance_norm'):
        """
        :param name: Str. For namescoping.
        """
        self.epsilon = 1E-10
        self.name = name
        # Computed instance variances.
        self.vars = []
        # Compute instance means.
        self.means = []

    def normalize(self, x):
        """
        If this is called multiple times (with tensors of the same shape),
        the statistics used for unnormalization will be averaged.
        :param x: Tensor. Features to normalize of shape [B, H, W, C].
        :return: Tensor. Instance normalized features of shape [B, H, W, C].
        """
        with tf.name_scope(self.name + '_' + str(len(self.vars))):
            mean, var = tf.nn.moments(x, [1, 2], keep_dims=True)
            self.means.append(mean)
            self.vars.append(var)
            normalized = (x - mean) / tf.sqrt(self.epsilon + var)
        return normalized

    def unnormalize(self, x):
        """
        :param x: Tensor. Features to unnormalize of shape [B, H, W, C].
        :return: Tensor. Instance normalized features of shape [B, H, W, C].
        """
        assert len(self.means) > 0, 'normalize must have been called beforehand.'
        with tf.name_scope(self.name + '_unnormalize'):
            avg_mean = tf.reduce_mean(tf.stack(self.means, axis=0), axis=0)
            avg_var = tf.reduce_mean(tf.stack(self.vars, axis=0), axis=0)
            unnormalized = x * tf.sqrt(self.epsilon + avg_var) + avg_mean
        return unnormalized

    def reset(self):
        """
        Clears running variance and means.
        """
        self.vars = []
        self.means = []

