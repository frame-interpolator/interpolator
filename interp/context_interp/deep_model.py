import tensorflow as tf
from common.forward_warp.forward_warp_gaussian import forward_warp_gaussian
from common.forward_warp.forward_warp_bidirectional import forward_warp_bidirectional
from common.forward_warp.forward_warp_interp import forward_warp_interp
from interp.context_interp.gridnet.model import GridNet
from interp.context_interp.model import ContextInterp
from pwcnet.model import PWCNet


class DeepContextInterp(ContextInterp):
    def __init__(self, name='deep_context_interp', use_instance_norm=True, stop_gradient_to_pwcnet=True,
                 use_differentiable_warp=False, pwcnet=PWCNet(use_flcon=False, bilateral_range_stddev=-1.0)):
        """
        :param name: Str. For Tf variable scoping.
        :param use_instance_norm: Bool. Whether to use instance normalization on the input images and contexts.
        :param stop_gradient_to_pwcnet: Bool. Whether to stop synthesis gradients from flowing back to PWCNet.
        :param use_differentiable_warp: Bool. Whether to use differentiable forward warp,
                                              as opposed to conflict resolution one.
        :param pwcnet: PWCNet object.
        """
        super().__init__(name=name, use_instance_norm=use_instance_norm,
                         stop_gradient_to_pwcnet=stop_gradient_to_pwcnet,
                         use_differentiable_warp=use_differentiable_warp, pwcnet=pwcnet)
        self.gridnet = GridNet([32, 64, 96, 128], 6, num_output_channels=3)

    def _get_forward(self, images_0, images_1, t,
                     flows_0_1=None, flows_1_0=None,
                     flows_0_t=None, flows_1_t=None,
                     reuse_variables=tf.AUTO_REUSE):
        """
        Overriden.
        :return: interpolated: The interpolated image. Tensor of shape [batch_size, H, W, 3].
                 others: Tuple with members:
                     warped_0_1: Image and features from image_0 forward-flowed towards image_b, before synthesis.
                                 The first 3 channels are the image.
                     warped_1_0: Image and features from image_b forward-flowed towards image_a, before synthesis.
                                 The first 3 channels are the image.
                     flow_0_1: Flow from images 0 to 1 (centered at images 0).
                     flow_1_0: Flow from images 1 to 0 (centered at images 1).
        """
        with tf.variable_scope(self.name, reuse=reuse_variables):
            self._clear_state()
            batch_size, height, width, _ = tf.unstack(tf.shape(images_0))
            from_frames = tf.concat([images_0, images_1], axis=0)
            layers = self.feature_extractor.get_deep_context_features(from_frames)
            feature_layers = [layers.conv1_2, layers.conv2_2, layers.conv3_4, layers.conv4_4]

            if flows_0_1 is None or flows_1_0 is None:
                flows_0_1, flows_1_0, _, _, _, _ = self.pwcnet.get_bidirectional(images_0, images_1)

            pyr_flows_0_1 = self.get_scaled_pyr_flows(flows_0_1, len(feature_layers) - 1)
            pyr_flows_1_0 = self.get_scaled_pyr_flows(flows_1_0, len(feature_layers) - 1)
            assert len(pyr_flows_0_1) == len(pyr_flows_1_0)
            assert len(pyr_flows_0_1) == len(feature_layers)

            features_a, features_b = [], []

            # Helper function for resizing images and getting their resized contexts.
            def get_resized_contexts(images_0_processed, images_1_processed, contexts):
                for i, context in enumerate(contexts):
                    divisor = 2 ** i
                    if divisor == 1:
                        resized_images_0 = images_0_processed
                        resized_images_1 = images_1_processed
                    else:
                        resized_images_0 = tf.image.resize_bilinear(
                            images_0_processed,
                            [tf.cast(height / divisor, dtype=tf.int32), tf.cast(width / divisor, dtype=tf.int32)],
                            align_corners=True)
                        resized_images_1 = tf.image.resize_bilinear(
                            images_1_processed,
                            [tf.cast(height / divisor, dtype=tf.int32), tf.cast(width / divisor, dtype=tf.int32)],
                            align_corners=True)
                    features_a.append(tf.concat([resized_images_0, context[:batch_size]], axis=-1))
                    features_b.append(tf.concat([resized_images_1, context[batch_size:]], axis=-1))

            if self.use_instance_norm:
                # Apply instance normalization to context and images separately.
                normalized_images_0 = self.images_normalizer.normalize(images_0)
                normalized_images_1 = self.images_normalizer.normalize(images_1)
                normalized_contexts = [self.contexts_normalizer.normalize(conv_feat) for conv_feat in feature_layers]
                get_resized_contexts(normalized_images_0, normalized_images_1, normalized_contexts)
            else:
                get_resized_contexts(images_0, images_1, feature_layers)

            assert len(features_a) == len(features_b)
            assert len(features_a) == len(pyr_flows_1_0)

            # Warp the contexts.
            warped_forward = []
            warped_backward = []
            if self.use_forward_warp_interp:
                reverse_t = 1.0 - t
                for i, (feature_a, feature_b) in enumerate(zip(features_a, features_b)):
                    forward, _, _, _ = forward_warp_interp(feature_a, feature_b, pyr_flows_0_1[i], t)
                    backward, _, _, _ = forward_warp_interp(feature_b, feature_a, pyr_flows_1_0[i], reverse_t)
                    warped_forward.append(forward)
                    warped_backward.append(backward)
            else:
                t_reshaped = tf.reshape(t, [-1, 1, 1, 1])
                reverse_t_reshaped = 1.0 - t_reshaped
                for i, (feature_a, feature_b) in enumerate(zip(features_a, features_b)):
                    all_warp_flows = tf.concat([t_reshaped * pyr_flows_0_1[i],
                                                reverse_t_reshaped * pyr_flows_1_0[i]], axis=0)
                    all_features = tf.concat([feature_a, feature_b], axis=0)
                    # Warp images and their contexts from images 0->1 and from images 1->0.
                    all_warped = forward_warp_gaussian(all_features, all_warp_flows)
                    warped_forward.append(all_warped[:batch_size])
                    warped_backward.append(all_warped[batch_size:])

            # Stop gradients.
            assert len(warped_forward) == len(warped_backward)
            assert len(warped_forward) == len(pyr_flows_1_0)
            if self.stop_gradient_to_pwcnet:
                for i in range(len(warped_forward)):
                    warped_forward[i] = tf.stop_gradient(warped_forward[i])
                    warped_backward[i] = tf.stop_gradient(warped_backward[i])

            # Get debug warps.
            with tf.name_scope('debug_warps'):
                warped_0_1, warped_1_0, _ = forward_warp_bidirectional(images_0, images_1, flows_0_1, flows_1_0, t,
                                                                       fill_holes=False)

            # Feed into GridNet for final synthesis.
            warped_combined = []
            for forward, backward in zip(warped_forward, warped_backward):
                warped_combined.append(tf.concat([forward, backward], axis=-1))
            synthesized, _, _, _ = self.gridnet.get_forward(warped_combined, training=True)

            # Undo the instance normalization to get the final synthesis.
            final = self.images_normalizer.unnormalize(synthesized) if self.use_instance_norm else synthesized
            others = (warped_0_1, warped_1_0, flows_0_1, flows_1_0)
            return final, others

    def get_scaled_pyr_flows(self, flows, num_downsamples):
        """
        :param flows: Optical flow tensor of shape [batch_size, H, W, 2].
        :param num_downsamples: The number of times to downsample.
        :return: List of tensors, of length 1 + num_downsamples.
                 The first element is the full-resolution flow.
        """
        H, W = tf.shape(flows)[1], tf.shape(flows)[2]
        all_flows = [flows]
        for i in range(1, num_downsamples + 1):
            factor = (2 ** i)
            new_height = H / factor
            new_width = W / factor
            scaled_flow = tf.image.resize_nearest_neighbor(flows, (new_height, new_width), align_corners=True)
            scaled_flow = scaled_flow / factor
            all_flows.append(scaled_flow)
        return all_flows
