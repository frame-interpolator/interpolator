import tensorflow as tf
from common.forward_warp.forward_warp_gaussian import forward_warp_gaussian
from common.forward_warp.forward_warp_interp import forward_warp_interp
from common.forward_warp.forward_warp_bidirectional import forward_warp_bidirectional
from common.utils.tf import l2_diff
from interp.interp import Interp
from interp.context_interp.instance_norm.instance_norm import InstanceNorm
from interp.context_interp.vgg19_features.vgg19_features import Vgg19Features
from interp.context_interp.gridnet.model import GridNet
from interp.context_interp.laplacian_pyramid.laplacian_pyramid import LaplacianPyramid
from pwcnet.model import PWCNet


class ContextInterp(Interp):
    def __init__(self, name='context_interp',
                 use_instance_norm=True,
                 stop_gradient_to_pwcnet=True,
                 use_differentiable_warp=False,
                 use_time_channel=False,
                 pwcnet=PWCNet(use_flcon=False, bilateral_range_stddev=-1.0)):
        """
        :param name: Str. For Tf variable scoping.
        :param use_instance_norm: Bool. Whether to use instance normalization on the input images and contexts.
        :param stop_gradient_to_pwcnet: Bool. Whether to stop synthesis gradients from flowing back to PWCNet.
                                        If True, use_differentiable_warp should be True as well.
        :param use_differentiable_warp: Bool. Whether to use differentiable forward warp,
                                              as opposed to conflict resolution one.
        :param use_time_channel: Bool. Whether to feed time information as a single channel.
        :param pwcnet: PWCNet object.
        """
        super().__init__(name)
        self.gridnet = GridNet([32, 64, 96], 6, num_output_channels=3)
        self.laplacian_pyramid = LaplacianPyramid(5)
        self.pwcnet = pwcnet
        self.feature_extractor = Vgg19Features()
        self.feature_extractor.load_pretrained_weights()
        self.use_instance_norm = use_instance_norm
        self.stop_gradient_to_pwcnet = stop_gradient_to_pwcnet
        self.use_forward_warp_interp = not use_differentiable_warp
        self.use_time_channel = use_time_channel
        assert not (self.use_forward_warp_interp and not self.stop_gradient_to_pwcnet)
        self.images_normalizer = InstanceNorm('images_instance_norm')
        self.contexts_normalizer = InstanceNorm('contexts_instance_norm')

    def _get_forward(self, images_0, images_1, t, flows_0_1=None, flows_1_0=None,
                     flows_0_t=None, flows_1_t=None,
                     reuse_variables=tf.AUTO_REUSE):
        """
        Overriden.
        :return: interpolated: The interpolated image. Tensor of shape [batch_size, H, W, 3].
                 auxiliary_tensors: Dictionary.
                     warped_0_1: Image and features from image_0 forward-flowed towards image_b, before synthesis.
                                 The first 3 channels are the image.
                     warped_1_0: Image and features from image_b forward-flowed towards image_a, before synthesis.
                                 The first 3 channels are the image.
                     flow_0_1: Flow from images 0 to 1 (centered at images 0).
                     flow_1_0: Flow from images 1 to 0 (centered at images 1).
                     warped_0_1_interp: Image warped with flows and using Middlebury warp.
                     warped_1_0_interp: Image warped with flows and using Middlebury warp.
        """
        with tf.variable_scope(self.name, reuse=reuse_variables):
            if flows_0_1 is None or flows_1_0 is None:
                # Get flows from PWCNet.
                flows_0_1, flows_1_0, _, _, _, _ = self.pwcnet.get_bidirectional(images_0, images_1)
                if self.stop_gradient_to_pwcnet:
                    flows_0_1 = tf.stop_gradient(flows_0_1)
                    flows_1_0 = tf.stop_gradient(flows_1_0)

            # Extract context features.
            self._clear_state()
            batch_size = tf.shape(images_0)[0]
            from_frames = tf.concat([images_0, images_1], axis=0)
            all_contexts = self.feature_extractor.get_context_features(from_frames)

            t_reshaped = tf.reshape(t, [-1, 1, 1, 1])
            if self.use_instance_norm:
                # Apply instance normalization to context and images separately.
                normalized_images_0 = self.images_normalizer.normalize(images_0)
                normalized_images_1 = self.images_normalizer.normalize(images_1)
                normalized_contexts = self.contexts_normalizer.normalize(all_contexts)
                features_a = tf.concat([normalized_images_0, normalized_contexts[:batch_size]], axis=-1)
                features_b = tf.concat([normalized_images_1, normalized_contexts[batch_size:]], axis=-1)
            else:
                features_a = tf.concat([images_0, all_contexts[:batch_size]], axis=-1)
                features_b = tf.concat([images_1, all_contexts[batch_size:]], axis=-1)

            features_a = tf.stop_gradient(features_a)
            features_b = tf.stop_gradient(features_b)
            if self.use_forward_warp_interp:
                warped_0_1, _, _, _ = forward_warp_interp(features_a, features_b, flows_0_1, t, fill_holes=False)
                warped_1_0, _, _, _ = forward_warp_interp(features_b, features_a, flows_1_0, 1.0 - t, fill_holes=False)
                warped_1_0 = tf.stop_gradient(warped_1_0)
                warped_0_1 = tf.stop_gradient(warped_0_1)
            else:
                # This is currently the only warp that can incorporate the spline directly.
                warp_outputs = forward_warp_bidirectional(features_a, features_b, flows_0_1, flows_1_0, t,
                                                          flow_0_t=flows_0_t,
                                                          flow_1_t=flows_1_t,
                                                          fill_holes=False)
                warped_0_1, warped_1_0, others_dict = warp_outputs

            # These will be used for visualization (not training) purposes.
            with tf.name_scope('debug_warps'):
                warped_0_1_interp, _, _, _ = forward_warp_interp(images_0, images_1, flows_0_1, t, fill_holes=True)
                warped_1_0_interp, _, _, _ = forward_warp_interp(images_1, images_0, flows_1_0, 1 - t, fill_holes=True)
                warped_0_1_gaussian = forward_warp_gaussian(images_0, t_reshaped * flows_0_1)
                warped_1_0_gaussian = forward_warp_gaussian(images_1, (1.0 - t_reshaped) * flows_1_0)
                warped_0_1_bidir, warped_1_0_bidir, _ = forward_warp_bidirectional(images_0, images_1,
                                                                                   flows_0_1, flows_1_0, t,
                                                                                   fill_holes=False)

            # Optionally add channel just for time information.
            if self.use_time_channel:
                with tf.name_scope('time_information'):
                    im_shape = tf.shape(warped_0_1)
                    height = im_shape[1] // 4
                    width = im_shape[2] // 4
                    timestamp_tensor = tf.ones((batch_size, height, width, 1)) * t_reshaped
            all_warps = tf.concat([warped_0_1, warped_1_0], axis=-1)
            input_list = [all_warps, None, timestamp_tensor] if self.use_time_channel else [all_warps]
            synthesized, _, _, _ = self.gridnet.get_forward(input_list, training=True)

            # Undo the instance normalization to get the final synthesis.
            final = self.images_normalizer.unnormalize(synthesized) if self.use_instance_norm else synthesized
            auxiliary_tensors = {
                'warped_0_1': warped_0_1,
                'warped_1_0': warped_1_0,
                'warped_0_1_gaussian': warped_0_1_gaussian,
                'warped_1_0_gaussian': warped_1_0_gaussian,
                'warped_0_1_bidir': warped_0_1_bidir,
                'warped_1_0_bidir': warped_1_0_bidir,
                'flow_0_1': flows_0_1,
                'flow_1_0': flows_1_0,
                'warped_0_1_interp': warped_0_1_interp,
                'warped_1_0_interp': warped_1_0_interp
            }
            return final, auxiliary_tensors

    def _clear_state(self):
        """
        In case get_forward is called multiple times, we want the runs to have separate normalizer running states.
        """
        self.images_normalizer.reset()
        self.contexts_normalizer.reset()

    def load_pwcnet_weights(self, pwcnet_weights_path, sess):
        """
        Loads pre-trained PWCNet weights.
        For this to work:
            - It must be called after get_forward.
            - The pwcnet weights must have been saved under variable scope 'pwcnet'.
        :param pwcnet_weights_path: The full path to the PWCNet weights that will be loaded via
                                    the RestorableNetwork interface.
        :param sess: Tf Session.
        """
        assert self.enclosing_scope is not None, 'get_forward must have been called beforehand.'
        scope_prefix = self.enclosing_scope.name + self.name
        self.pwcnet.restore_from(pwcnet_weights_path, sess, scope_prefix=scope_prefix)

    def get_training_loss(self, prediction, expected):
        """
        :param prediction: Tensor of shape [batch, H, W, num_features]. Predicted image.
        :param expected: Tensor of shape [batch, H, W, num_features]. Ground truth image.
        :return: Tf scalar loss term.
        """
        return self._get_laplacian_loss(prediction, expected)

    def get_fine_tuning_loss(self, prediction, expected):
        """
        :param prediction: Tensor of shape [batch, H, W, num_features]. Predicted image.
        :param expected: Tensor of shape [batch, H, W, num_features]. Ground truth image.
        :return: Tf scalar loss term.
        """
        # The values for the weights are taken from the SuperSlowMo Paper:
        # http://openaccess.thecvf.com/content_cvpr_2018/papers/Jiang_Super_SloMo_High_CVPR_2018_paper.pdf
        feature_loss = self._get_feature_loss(prediction, expected)
        color_loss = self._get_l1_loss(prediction, expected)
        return 0.005 * feature_loss + 204 * color_loss

    def _get_feature_loss(self, prediction, expected):
        with tf.variable_scope('feature_loss'):
            batch_size = tf.shape(prediction)[0]
            prediction_features = self.feature_extractor.get_perceptual_features(prediction)
            expected_features = self.feature_extractor.get_perceptual_features(expected)
            dist = l2_diff(prediction_features, expected_features)
            return tf.reduce_sum(dist) / tf.cast(batch_size, tf.float32)

    def _get_l1_loss(self, prediction, expected):
        batch_size = tf.shape(prediction)[0]
        return tf.reduce_sum(tf.abs(prediction - expected)) / tf.cast(batch_size, tf.float32)

    def _get_laplacian_loss(self, prediction, expected):
        with tf.name_scope('laplacian_loss'):
            batch_size = tf.shape(prediction)[0]
            combined = tf.concat([prediction, expected], axis=0)
            pyrs, _, _ = self.laplacian_pyramid.get_forward(combined)
            loss = 0
            for i in range(len(pyrs)):
                loss += 2 ** i * tf.reduce_sum(tf.abs(pyrs[i][:batch_size] - pyrs[i][batch_size:]))
            return loss / tf.cast(batch_size, tf.float32)
