from interp.context_interp.vgg19_features.model.model import Vgg19


class Vgg19Features:
    def __init__(self):
        self.vgg19 = None

    def load_pretrained_weights(self):
        vgg19_data = Vgg19.load_params_dict(load_small=True)
        self.vgg19 = Vgg19(data_dict=vgg19_data)

    def get_context_features(self, images):
        """
        :param images: A Tensor. Of shape [batch, H, W, num_features].
        :return: A Tensor. Of shape [batch, H, W, 64].
        """
        assert self.vgg19 is not None
        image_features, _ = self.vgg19.build_up_to_conv1_2(images, trainable=False)
        return image_features

    def get_deep_context_features(self, images):
        """
        :param images: A Tensor. Of shape [batch, H, W, num_features].
        :return: Layers object. Use conv1_2, conv2_2, conv3_4, conv4_4. Respective shapes are:
            [batch, H, W, 64], [batch, H / 2, W / 2, 128], [batch, H / 4, W / 4, 256], [batch, H / 8, W / 8, 512].
        """
        assert self.vgg19 is not None
        _, layers = self.vgg19.build_up_to_conv4_4(images, trainable=False)
        return layers

    def get_perceptual_features(self, images):
        """
        :param images: A Tensor. Of shape [batch, H, W, num_features].
        :return: A Tensor. Of shape [batch, H / 8, W / 8, 512].
        """
        assert self.vgg19 is not None
        image_features, _ = self.vgg19.build_up_to_conv4_4(images, trainable=False)
        return image_features
