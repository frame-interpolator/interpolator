import os
import unittest
import numpy as np
import tensorflow as tf
from common.utils.img import read_image, write_image
from interp.context_interp.vgg19_features.vgg19_features import Vgg19Features


class TestVgg19Features(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        self.feature_extractor = Vgg19Features()
        self.feature_extractor.load_pretrained_weights()

    def test_features(self):
        height = 32
        width = 32
        num_features = 3
        batch_size = 3

        # Create the graph.
        input_image = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        context_feature_tensor = self.feature_extractor.get_context_features(input_image)
        perceptual_feature_tensor = self.feature_extractor.get_perceptual_features(input_image)

        input_images_np = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        input_images_np[:, 2:height-2, 2:width-2, :] = 3.0

        self.sess.run(tf.global_variables_initializer())
        query = [context_feature_tensor, perceptual_feature_tensor]
        context_features, perceptual_features = self.sess.run(query, feed_dict={input_image: input_images_np})

        # Test that the default values are working.
        self.assertTrue(np.allclose(perceptual_features.shape, np.asarray([batch_size, height / 8, width / 8, 512])))
        self.assertTrue(np.allclose(context_features.shape, np.asarray([batch_size, height, width, 64])))
        self.assertNotEqual(np.sum(context_features), 0.0)
        self.assertNotEqual(np.sum(perceptual_features), 0.0)

        # Models should not be trainable.
        vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='vgg19')
        trainable_vars = tf.trainable_variables(scope='vgg19')
        self.assertEqual(len(vars), 24)
        self.assertEqual(len(trainable_vars), 0)

        tf.reset_default_graph()

    def test_deep_features(self):
        height = 32
        width = 32
        num_features = 3
        batch_size = 3

        # Create the graph.
        input_image = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        layers = self.feature_extractor.get_deep_context_features(input_image)
        conv1_2 = layers.conv1_2
        conv2_2 = layers.conv2_2
        conv3_4 = layers.conv3_4
        conv4_4 = layers.conv4_4

        input_images_np = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        input_images_np[:, 2:height-2, 2:width-2, :] = 3.0

        self.sess.run(tf.global_variables_initializer())
        query = [conv1_2, conv2_2, conv3_4, conv4_4]
        feats = self.sess.run(query, feed_dict={input_image: input_images_np})
        conv1_2_feats, conv2_2_feats, conv3_4_feats, conv4_4_feats = feats

        # Test that the default values are working.
        self.assertTrue(np.allclose(conv4_4_feats.shape, np.asarray([batch_size, height / 8, width / 8, 512])))
        self.assertTrue(np.allclose(conv3_4_feats.shape, np.asarray([batch_size, height / 4, width / 4, 256])))
        self.assertTrue(np.allclose(conv2_2_feats.shape, np.asarray([batch_size, height / 2, width / 2, 128])))
        self.assertTrue(np.allclose(conv1_2_feats.shape, np.asarray([batch_size, height, width, 64])))
        self.assertNotEqual(np.sum(conv4_4_feats), 0.0)
        self.assertNotEqual(np.sum(conv3_4_feats), 0.0)
        self.assertNotEqual(np.sum(conv2_2_feats), 0.0)
        self.assertNotEqual(np.sum(conv1_2_feats), 0.0)

        # Models should not be trainable.
        vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='vgg19')
        trainable_vars = tf.trainable_variables(scope='vgg19')
        self.assertEqual(len(vars), 24)
        self.assertEqual(len(trainable_vars), 0)

        tf.reset_default_graph()

    def test_visualization(self):
        # Load image.
        cur_dir = os.path.dirname(os.path.realpath(__file__))
        img_path = os.path.join(cur_dir, 'test_data', 'hamid.jpg')
        img = read_image(img_path, as_float=True)

        height = img.shape[0]
        width = img.shape[1]
        channels = img.shape[2]
        img = [img]

        # Create the graph.
        input_image = tf.placeholder(shape=[None, height, width, channels], dtype=tf.float32)
        context_feature_tensor = self.feature_extractor.get_context_features(input_image)
        perceptual_feature_tensor = self.feature_extractor.get_perceptual_features(input_image)

        self.sess.run(tf.global_variables_initializer())
        query = [context_feature_tensor, perceptual_feature_tensor]
        context_features, perceptual_features = self.sess.run(query, feed_dict={input_image: img})

        output_folder = os.path.join('output_test_images', 'test_feature_outputs')
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)

        # Visualize context features.
        for i in range(0, 32, 4):
            np_arr = context_features[0][..., i]
            normalized = np.expand_dims(np_arr / (np.max(np_arr) - np.min(np_arr)) + np.min(np_arr), axis=-1)
            write_image(os.path.join(output_folder, 'shallow_features_channel_' + str(i) + '.jpg'), normalized)

        # Visualize perceptual features.
        for i in range(0, 512, 64):
            np_arr = perceptual_features[0][..., i]
            normalized = np.expand_dims(np_arr / (np.max(np_arr) - np.min(np_arr)) + np.min(np_arr), axis=-1)
            write_image(os.path.join(output_folder, 'deep_features_channel_' + str(i)) + '.jpg', normalized)


if __name__ == '__main__':
    unittest.main()