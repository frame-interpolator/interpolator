import tensorflow as tf
from common.utils.tf import prelu
from interp.context_interp.gridnet.connections.connections import UpSamplingConnection, DownSamplingConnection, \
    LateralConnection


# Packaged activation functions.
def batch_norm_with_prelu(x):
    x = tf.layers.batch_normalization(x)
    return prelu(x)


def batch_norm_with_relu(x):
    x = tf.layers.batch_normalization(x)
    return tf.nn.relu(x)


_default = object()


class GridNet:
    def __init__(self, channel_sizes, width,
                 name='gridnet',
                 do_multi_level_output=False,
                 num_output_channels=_default,
                 num_lateral_convs=2,
                 num_upsample_convs=2,
                 num_downsampling_convs=2,
                 use_batch_norm=False,
                 connection_dropout_rate=0.0,
                 regularizer=None):
        """
        See https://arxiv.org/pdf/1707.07958.pdf, and modifications made in https://arxiv.org/pdf/1803.10967.pdf.
        :param channel_sizes: List of channel sizes for rows. Height of the GridNet = len(channel_sizes).
        :param width: Width of the GridNet. Must be an even number to ensure symmetry.
        :param name: Str. For variable scoping.
        :param do_multi_level_output: Bool. Whether to output a tensor for each resolution level.
                                      This will add a lateral convolution at each output resolution.
        :param num_output_channels: Number. Defaults to channel_sizes[0].
        :param num_lateral_convs: Number of convolutions in each lateral connection.
        :param num_upsample_convs: Number of convolutions in each up-sampling connection.
        :param num_downsampling_convs: Number of convolutions in each down-sampling connection.
        :param use_batch_norm: Whether to use batch normalization.
        :param connection_dropout_rate: E.g if 0.5, drops out each connection (not individual neurons) with 50% chance.
        :param regularizer: Tf regularizer such as tf.contrib.layers.l2_regularizer.

        Example for height = 3, width = 4:

                          grid[0][0]                  grid[0][3]

        features[0] +-------> +-------> +-------> +-------> +-------> output
                              |         |         ^         ^
                              |         |         |         |
                              |         |         |         |
                              |         |         |         |
                              v         v         |         |
        features[1] +-------> +-------> +-------> +-------> +-------> output (if do_multi_level_output)
                              |         |         ^         ^
                              |         |         |         |
                              |         |         |         |
                              |         |         |         |
                              v         v         |         |
        features[2] +-------> +-------> +-------> +-------> +-------> output (if do_multi_level_output)

        """
        height = len(channel_sizes)
        if height <= 0:
            raise ValueError('Height = len(channel_sizes) must be >= 1.')

        if width % 2 != 0:
            raise ValueError('Width must be an even number, to enforce GridNet symmetry.')

        if width <= 0:
            raise ValueError('Width must be non-zero.')

        self.width = width
        self.height = height
        self.channel_sizes = channel_sizes
        self.name = name
        self.do_multi_level_output = do_multi_level_output
        self.num_lateral_convs = num_lateral_convs
        self.num_downsampling_convs = num_downsampling_convs
        self.num_upsample_convs = num_upsample_convs
        self.use_batch_norm = use_batch_norm
        self.connection_dropout_rate = connection_dropout_rate
        self.regularizer = regularizer

        if num_output_channels == _default:
            num_output_channels = self.channel_sizes[0]
        self.num_output_channels = num_output_channels

        # More settings
        if self.use_batch_norm:
            self.activation_fn = batch_norm_with_prelu
        else:
            self.activation_fn = prelu

        # Construct specs for connections.
        # Entry specs[i][j] is the spec for the jth convolution for any connection in the ith row.
        # An exception is made for the output lateral connection.
        self.output_spec = [[self.num_output_channels, 1] for _ in range(self.num_lateral_convs)]
        self.lateral_specs = []
        self.upsample_specs = []
        self.downsample_specs = []
        for i in range(self.height):
            row_lateral_specs, row_upsample_specs, row_downsample_specs = [], [], []
            num_filters = channel_sizes[i]
            common_spec = [num_filters, 1]
            row_lateral_specs = [common_spec for _ in range(self.num_lateral_convs)]

            if i > 0:
                row_downsample_specs = [common_spec for _ in range(self.num_downsampling_convs)]

            if i < self.height - 1:
                row_upsample_specs = [common_spec for _ in range(self.num_upsample_convs)]

            self.lateral_specs.append(row_lateral_specs)
            self.upsample_specs.append(row_upsample_specs)
            self.downsample_specs.append(row_downsample_specs)

        # GridNet Tensor parameter containers.
        self.node_outputs = None
        self.lateral_inputs = None
        self.vertical_inputs = None

    def get_forward(self, features_list, training=False, reuse_variables=tf.AUTO_REUSE):
        """
        :param features_list: A list of Tensors.
                              Input feature maps of shape [batch_size, H / 2 ** i, W / 2 ** i, num_features],
                              where i is the index of the features in the list.
                              Note that the features can be None (or potentially len(features_list) == 1),
                              but there has to be at least 1 non-None feature tensor.
        :param training: Bool. Whether the graph is to be constructed for training (dropout will be applied).
        :param reuse_variables: tf reuse option. i.e. tf.AUTO_REUSE.
        :return final_output: A Tensor. Will take on the same shape as param features. If do_multi_level_output is True,
                              this will return a list of tensors, one for each resolution level (from highest res).
                node_outputs: A 2D list of Tensors. Represents the output at each grid node.
                lateral_inputs: A 2D list of Tensors. Represents the lateral stream input at each grid node.
                vertical_inputs: A 2D list of Tensors. Represents the vertical stream input at each grid node.
        """
        assert len(features_list) > 0
        assert len(features_list) <= self.height
        with tf.variable_scope(self.name, reuse=reuse_variables):
            first_half_features, _, _, _ = self.get_forward_downsample_half(features_list,
                                                                            training=training,
                                                                            reuse_variables=reuse_variables)
            return self.get_forward_upsample_half(first_half_features,
                                                  training=training,
                                                  reuse_variables=reuse_variables)

    def get_forward_downsample_half(self, features_list, training=False, reuse_variables=tf.AUTO_REUSE):
        """
        Creates the first half of GridNet (downsampling).
        :param features_list: A list of Tensors.
                              Input feature maps of shape [batch_size, H / 2 ** i, W / 2 ** i, num_features],
                              where i is the index of the features in the list.
                              Note that the features can be None (or potentially len(features_list) == 1),
                              but there has to be at least 1 non-None feature tensor.
        :param training: Bool. Whether the graph is to be constructed for training (dropout will be applied).
        :param reuse_variables: tf reuse option. i.e. tf.AUTO_REUSE.
        :return output_features_list: Same format as features_list.
                node_outputs: The current GridNet node output values.
                lateral_inputs: A 2D list of Tensors. Represents the lateral stream input at each grid node.
                vertical_inputs: A 2D list of Tensors. Represents the vertical stream input at each grid node.
        """
        assert len(features_list) > 0
        assert len(features_list) <= self.height

        # Reset GridNet Tensors.
        self.node_outputs = [[tf.constant(0.0) for x in range(self.width)] for y in range(self.height)]
        self.lateral_inputs = [[tf.constant(0.0) for x in range(self.width)] for y in range(self.height)]
        self.vertical_inputs = [[tf.constant(0.0) for x in range(self.width)] for y in range(self.height)]

        # First lateral connections.
        non_empty = False
        for i, features in enumerate(features_list):
            if features is not None:
                non_empty = True
                self.node_outputs[i][0] = self._process_rightwards(features, i, 0,
                                                                   training=training,
                                                                   reuse_variables=reuse_variables)
                self.lateral_inputs[i][0] = self.node_outputs[i][0]
        assert non_empty

        # Connect first half (Down-sampling streams) by iterating to the right, and downwards.
        for i in range(self.height):
            for j in range(int(self.width / 2)):
                if i == 0 and j == 0:
                    continue

                top_output, left_output = 0, 0
                if i > 0:
                    top_output = self._process_downwards(self.node_outputs[i-1][j], i, j,
                                                         reuse_variables=reuse_variables)
                    self.vertical_inputs[i][j] = top_output
                if j > 0:
                    left_output = self._process_rightwards(self.node_outputs[i][j-1], i, j,
                                                           training=training,
                                                           reuse_variables=reuse_variables)
                    self.lateral_inputs[i][j] = left_output

                self.node_outputs[i][j] += top_output + left_output

        output_features_list = []
        for row in self.node_outputs:
            output_features_list.append(row[self.width // 2 - 1])
        return output_features_list, self.node_outputs, self.lateral_inputs, self.vertical_inputs

    def get_forward_upsample_half(self, features_list, training=False, reuse_variables=tf.AUTO_REUSE):
        """
        Creates the second half of GridNet (upsampling).
        :param features_list: A list of Tensors.
                              Input feature maps of shape [batch_size, H / 2 ** i, W / 2 ** i, num_features],
                              where i is the index of the features in the list.
                              Note that the features can be None (or potentially len(features_list) == 1),
                              but there has to be at least 1 non-None feature tensor.
        :param training: Bool. Whether the graph is to be constructed for training (dropout will be applied).
        :param reuse_variables: tf reuse option. i.e. tf.AUTO_REUSE.
        :return final_output: A Tensor. Will take on the same shape as param features. If do_multi_level_output is True,
                              this will return a list of tensors, one for each resolution level (highest res first).
                node_outputs: The current GridNet node output values.
                lateral_inputs: A 2D list of Tensors. Represents the lateral stream input at each grid node.
                vertical_inputs: A 2D list of Tensors. Represents the vertical stream input at each grid node.
        """
        assert len(features_list) > 0
        assert len(features_list) <= self.height
        if self.node_outputs is None:
            self.node_outputs = [[tf.constant(0.0) for x in range(self.width)] for y in range(self.height)]
            self.lateral_inputs = [[tf.constant(0.0) for x in range(self.width)] for y in range(self.height)]
            self.vertical_inputs = [[tf.constant(0.0) for x in range(self.width)] for y in range(self.height)]

        # Set the lateral inputs to be features_list.
        non_empty = False
        start_col = self.width // 2
        for i, features in enumerate(features_list):
            if features is not None:
                non_empty = True
                self.node_outputs[i][start_col] = self._process_rightwards(features, i, start_col,
                                                                           training=training,
                                                                           reuse_variables=reuse_variables)
                self.lateral_inputs[i][start_col] = self.node_outputs[i][start_col]
        assert non_empty

        # Connect second half (Up-sampling streams) by iterating to the right, and upwards.
        for i in range(self.height - 1, -1, -1):
            for j in range(int(self.width // 2), self.width):
                bottom_output, left_output = 0, 0
                if i < self.height - 1:
                    bottom_output = self._process_upwards(self.node_outputs[i + 1][j], i, j,
                                                          reuse_variables=reuse_variables)
                    self.vertical_inputs[i][j] = bottom_output
                if j > start_col:
                    left_output = self._process_rightwards(self.node_outputs[i][j - 1], i, j, training=training,
                                                           reuse_variables=reuse_variables)
                    self.lateral_inputs[i][j] = left_output

                self.node_outputs[i][j] += bottom_output + left_output

        # Final lateral connection(s).
        if self.do_multi_level_output:
            final_outputs = []
            for i in range(self.height):
                previous_output = self.node_outputs[i][self.width - 1]
                final_output = self._process_rightwards(previous_output, i, self.width, training=training,
                                                        reuse_variables=reuse_variables)
                final_outputs.append(final_output)
            return final_outputs, self.node_outputs, self.lateral_inputs, self.vertical_inputs
        else:
            previous_output = self.node_outputs[0][self.width - 1]
            final_output = self._process_rightwards(previous_output, 0, self.width, training=training,
                                                    reuse_variables=reuse_variables)
            return final_output, self.node_outputs, self.lateral_inputs, self.vertical_inputs

    # Private helper functions.
    def _process_rightwards(self, input, i, j, training=False, reuse_variables=tf.AUTO_REUSE):
        # The input and output lateral connections should never be dropped, as they cutoff gradients hard.
        is_output = j == self.width and i == 0
        force_alive = (j == 0 or j == self.width)
        total_dropout_rate = 0.0 if force_alive else self.connection_dropout_rate
        return LateralConnection(
            'right_%d_%d' % (i, j),
            self.output_spec if is_output else self.lateral_specs[i],
            activation_fn=self.activation_fn,
            total_dropout_rate=total_dropout_rate,
            regularizer=self.regularizer
        ).get_forward(input, training=training, reuse_variables=reuse_variables)

    def _process_upwards(self, input, i, j, reuse_variables=tf.AUTO_REUSE):
        return UpSamplingConnection(
            'up_%d_%d' % (i, j),
            self.upsample_specs[i],
            activation_fn=self.activation_fn,
            regularizer=self.regularizer
        ).get_forward(input, reuse_variables=reuse_variables)

    def _process_downwards(self, input, i, j, reuse_variables=tf.AUTO_REUSE):
        return DownSamplingConnection(
            'down_%d_%d' % (i, j),
            self.downsample_specs[i],
            activation_fn=self.activation_fn,
            regularizer=self.regularizer
        ).get_forward(input, reuse_variables=reuse_variables)
