import tensorflow as tf


class Interp:
    def __init__(self, name='interp'):
        """
        :param name: Str. For Tf variable scoping.
        """
        self.name = name
        self.enclosing_scope = tf.get_variable_scope()

    def get_forward(self, images_0, images_1, t,
                    flows_0_1=None, flows_1_0=None,
                    flows_0_t=None, flows_1_t=None,
                    reuse_variables=tf.AUTO_REUSE):
        """
        :param images_0: Tensor of shape [batch_size, H, W, 3].
        :param images_1: Tensor of shape [batch_size, H, W, 3].
        :param t: Tensor of shape [batch_size]. Specifies the interpolation point (i.e 0 for images_0, 1 for images_1).
        :param flows_0_1: Tensor of shape [batch_size, H, W, 2]. The optical flow from images_0 to images_1.
        :param flows_1_0: Tensor of shape [batch_size, H, W, 2]. The optical flow from images_0 to images_1.
        :param flows_0_t: Tensor of shape [batch_size, H, W, 2]. The optical flow from images_0 (t = 0) to time t.
        :param flows_1_t: Tensor of shape [batch_size, H, W, 2]. The optical flow from images_0 (t = 1) to time t.
        :return: interpolated: The interpolated image. Tensor of shape [batch_size, H, W, 3].
                 *others: There may be other return values after the first 3 (e.g interpolated, ret1, ret2).
                          If not, this field must be left as None.
        """
        self.enclosing_scope = tf.get_variable_scope()
        outputs = self._get_forward(images_0, images_1, t,
                                    flows_0_1=flows_0_1,
                                    flows_1_0=flows_1_0,
                                    flows_0_t=flows_0_t,
                                    flows_1_t=flows_1_t,
                                    reuse_variables=reuse_variables)
        assert isinstance(outputs, tuple)
        assert isinstance(outputs[0], tf.Tensor)
        return outputs

    def _get_forward(self, images_0, images_1, t,
                     flows_0_1=None, flows_1_0=None,
                     flows_0_t=None, flows_1_t=None,
                     reuse_variables=tf.AUTO_REUSE):
        """
        Subclasses should implement this method. See get_forward.
        """
        raise NotImplementedError
