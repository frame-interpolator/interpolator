import cv2
import numpy as np
import os
import unittest
from common.utils.img import read_image, write_image
from common.utils.flow import read_flow_file
from common.forward_warp.forward_warp_bilinear import forward_warp_bilinear
from common.utils.flow import get_flow_visualization
from interp.flow_spline.flow_spline import *


WRITE_TO_VIDEO = False


def generate_test_image_and_flow():
    """
    Generates a fake image and flow pair for testing quadratic motion.
    :return: image_a, flow_ab
    """
    image_a = np.zeros((1, 512, 512, 3), dtype=np.float32)
    image_a[:, 256-18:256+18, 256:512] = [1.0, 1.0, 1.0]
    flow_ab = np.zeros((1, 512, 512, 2), dtype=np.float32)
    for i in range(256-18, 256+18):
        for j in range(256, 512):
            flow_ab[:, i, j] = np.asarray([512 - i, j]) - np.asarray([j, i])
    return image_a, flow_ab


def generate_second_test_image_and_flow():
    """
    Generates a fake image and flow pair for testing quadratic motion.
    :return: image_a, flow_ab
    """
    image_a = np.zeros((1, 512, 512, 3), dtype=np.float32)
    image_a[:, 256-18:256+18, 0:256] = [1.0, 1.0, 1.0]
    flow_ab = np.zeros((1, 512, 512, 2), dtype=np.float32)
    for i in range(256-18, 256+18):
        for j in range(0, 256):
            flow_ab[:, i, j] = np.asarray([i, 512 - j]) - np.asarray([j, i])
    return image_a, flow_ab


class TestQuadraticBezier(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_spline_midpoint(self):
        flow = tf.constant([[[[0, 2], [2, 0]]]], dtype=tf.float32)
        mid_point_offset = tf.constant([[[[0.5, 0], [0, 0.5]]]], dtype=tf.float32)
        splined_tensor = quadratic_bezier(flow, mid_point_offset, 0.5)
        splined = self.sess.run(splined_tensor)
        self.assertTrue(np.allclose([[[[0.5, 1], [1, 0.5]]]],
                                    splined))

    def test_spline_begin(self):
        flow = tf.constant([[[[0, 2], [2, 0]]]], dtype=tf.float32)
        mid_point_offset = tf.constant([[[[0.5, 0], [0, 0.5]]]], dtype=tf.float32)
        splined_tensor = quadratic_bezier(flow, mid_point_offset, 0)
        splined = self.sess.run(splined_tensor)
        self.assertTrue(np.allclose([[[[0.0, 0.0], [0.0, 0.0]]]],
                                    splined))

    def test_spline_end(self):
        flow = tf.constant([[[[0, 2], [2, 0]]]], dtype=tf.float32)
        mid_point_offset = tf.constant([[[[0.5, 0], [0, 0.5]]]], dtype=tf.float32)
        splined_tensor = quadratic_bezier(flow, mid_point_offset, 1.0)
        splined = self.sess.run(splined_tensor)
        self.assertTrue(np.allclose([[[[0.0, 2.0], [2.0, 0.0]]]],
                                    splined))

    def test_spline(self):
        flow = tf.constant([[[[0, 2], [2, 0]]]], dtype=tf.float32)
        mid_point_offset = tf.constant([[[[0.5, 0], [0, 0.5]]]], dtype=tf.float32)
        splined_tensor = quadratic_bezier(flow, mid_point_offset, 0.25)
        splined = self.sess.run(splined_tensor)
        self.assertTrue(np.allclose([[[[0.375, 0.5], [0.5, 0.375]]]],
                                    splined))

        splined_tensor = quadratic_bezier(flow, mid_point_offset, 0.75)
        splined = self.sess.run(splined_tensor)
        self.assertTrue(np.allclose([[[[0.375, 1.5], [1.5, 0.375]]]],
                                    splined))


class TestQuadraticMotionCUDA(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_a_and_b(self):
        width = 3
        height = 3
        flow_ab = np.ones((1, height, width, 2))
        flow_tensor = tf.placeholder(tf.float32, np.shape(flow_ab))
        quadratic_motion = QuadraticMotionLayer(flow_tensor)

        a_data_tensor, a_cols_tensor, a_indptr_tensor, b_tensor = \
            quadratic_motion.a_data, quadratic_motion.a_cols, quadratic_motion.a_indptr, quadratic_motion.b

        a_data, a_cols, a_indptr, b = self.sess.run([a_data_tensor, a_cols_tensor, a_indptr_tensor, b_tensor],
                                                    feed_dict={flow_tensor: flow_ab})
        self.assertTupleEqual((width * height + 1,), a_indptr.shape)
        self.assertTupleEqual((width * height * 5,), a_cols.shape)
        self.assertTupleEqual((1, width * height * 5), a_data.shape)
        self.assertTupleEqual((1, width * height, 2), b.shape)
        b_0 = b[0, :, 0]
        a_data_0 = a_data[0, ...]

        # Regression test.
        self.assertTrue(np.array_equal([2., -1., -1.,  0.,  0.,  3., -1., -1., -1.,  0.,  2.,  0., -1., -1.,  0.,  3.,
                                        -1., -1., 0., -1.,  4., -1., -1., -1., -1.,  3.,  0., -1., -1., -1.,  2., -1.,
                                        0.,  0., -1.,  3., -1.,  0., -1., -1.,  2.,  0.,  0., -1., -1.], a_data_0))
        self.assertTrue(np.array_equal([0, 1, 3, 0, 0, 1, 2, 4, 0, 0, 2, 0, 5, 1, 0, 3, 4, 6, 0, 0, 4, 5, 7, 3, 1, 5, 0,
                                        8, 4, 2, 6, 7, 0, 0, 3, 7, 8, 0, 6, 4, 8, 0, 0, 7, 5], a_cols))
        self.assertTrue(np.array_equal([0, 5, 10, 15, 20, 25, 30, 35, 40, 45], a_indptr))
        self.assertTrue(np.array_equal([0., 0., 0., 0., 0., 0., 0., 0., 0.], b_0))

    def test_rectangle_image(self):
        width = 2
        height = 3
        flow_ab = np.ones((1, height, width, 2))
        flow_tensor = tf.placeholder(tf.float32, np.shape(flow_ab))
        quadratic_motion = QuadraticMotionLayer(flow_tensor)

        a_data_tensor, a_cols_tensor, a_indptr_tensor, b_tensor = \
            quadratic_motion.a_data, quadratic_motion.a_cols, quadratic_motion.a_indptr, quadratic_motion.b

        a_data, a_cols, a_indptr, b = self.sess.run([a_data_tensor, a_cols_tensor, a_indptr_tensor, b_tensor],
                                                    feed_dict={flow_tensor: flow_ab})
        self.assertTupleEqual((width * height + 1,), a_indptr.shape)
        self.assertTupleEqual((width * height * 5,), a_cols.shape)
        self.assertTupleEqual((1, width * height * 5), a_data.shape)
        self.assertTupleEqual((1, width * height, 2), b.shape)
        b_0 = b[0, :, 0]
        a_data_0 = a_data[0, ...]

        # Regression test.
        self.assertTrue(np.array_equal([2., -1., -1., 0., 0., 2., 0., -1., -1., 0., 3., -1., -1., 0., -1., 3., 0., -1.,
                                        -1., -1., 2., -1., 0., 0., -1., 2., 0., 0., -1., -1.], a_data_0))
        self.assertTrue(np.array_equal([0, 1, 2, 0, 0, 1, 0, 3, 0, 0, 2, 3, 4, 0, 0, 3, 0, 5, 2, 1, 4, 5, 0, 0, 2, 5, 0,
                                        0, 4, 3], a_cols))
        self.assertTrue(np.array_equal([0, 5, 10, 15, 20, 25, 30], a_indptr))
        self.assertTrue(np.array_equal([0., 0., 0., 0., 0., 0.], b_0))

    def test_nonzero_b(self):
        width = 3
        height = 3
        flow_ab = np.ones((1, height, width, 2))
        flow_ab[:, :, 1:3, 0] *= 1.5
        flow_tensor = tf.placeholder(tf.float32, np.shape(flow_ab))
        quadratic_motion = QuadraticMotionLayer(flow_tensor)

        a_data_tensor, a_cols_tensor, a_indptr_tensor, b_tensor = \
            quadratic_motion.a_data, quadratic_motion.a_cols, quadratic_motion.a_indptr, quadratic_motion.b

        a_data, a_cols, a_indptr, b = self.sess.run([a_data_tensor, a_cols_tensor, a_indptr_tensor, b_tensor],
                                                    feed_dict={flow_tensor: flow_ab})
        self.assertTupleEqual((width * height + 1,), a_indptr.shape)
        self.assertTupleEqual((width * height * 5,), a_cols.shape)
        self.assertTupleEqual((1, width * height * 5), a_data.shape)
        self.assertTupleEqual((1, width * height, 2), b.shape)
        b_0 = b[0, :, 0]
        a_data_0 = a_data[0, ...]

        # Regression test.
        self.assertTrue(np.array_equal([2., -1., -1.,  0.,  0.,  3., -1., -1., -1.,  0.,  2.,  0., -1., -1.,  0.,  3.,
                                        -1., -1., 0., -1.,  4., -1., -1., -1., -1.,  3.,  0., -1., -1., -1.,  2., -1.,
                                        0.,  0., -1.,  3., -1.,  0., -1., -1.,  2.,  0.,  0., -1., -1.], a_data_0))
        self.assertTrue(np.array_equal([0, 1, 3, 0, 0, 1, 2, 4, 0, 0, 2, 0, 5, 1, 0, 3, 4, 6, 0, 0, 4, 5, 7, 3, 1, 5, 0,
                                        8, 4, 2, 6, 7, 0, 0, 3, 7, 8, 0, 6, 4, 8, 0, 0, 7, 5], a_cols))
        self.assertTrue(np.array_equal([0, 5, 10, 15, 20, 25, 30, 35, 40, 45], a_indptr))
        self.assertTrue(np.allclose([0.02525508, -0.02525508, 0.,
                                     0.02525508, -0.02525508, 0.,
                                     0.02525508, -0.02525508, 0.], b_0))

    def test_solve(self):
        width = 3
        height = 3
        flow_ab = np.ones((1, height, width, 2))
        flow_ab[:, :, 1:3, 0] *= 1.5
        flow_ab[:, :, 1:3, 1] *= 1.1
        flow_tensor = tf.placeholder(tf.float32, np.shape(flow_ab))
        quadratic_motion = QuadraticMotionLayer(flow_tensor)

        midpoints_tensor = quadratic_motion.mid_points
        midpoints = self.sess.run(midpoints_tensor, feed_dict={flow_tensor: flow_ab})
        self.assertNotAlmostEqual(0.0, np.sum(np.abs(midpoints)))

    def test_solve_batch(self):
        width = 3
        height = 3
        flow_ab = np.ones((2, height, width, 2))
        flow_ab[0, :, 1:3, 0] *= 1.5
        flow_ab[0, :, 1:3, 1] *= 1.1
        flow_tensor = tf.placeholder(tf.float32, np.shape(flow_ab))
        quadratic_motion = QuadraticMotionLayer(flow_tensor)

        midpoints_tensor = quadratic_motion.mid_points
        midpoints = self.sess.run(midpoints_tensor, feed_dict={flow_tensor: flow_ab})
        self.assertNotAlmostEqual(0.0, np.sum(np.abs(midpoints[0])))
        self.assertAlmostEqual(0.0, np.sum(np.abs(midpoints[1])))

    def test_image(self):
        img_a, flow_ab = generate_test_image_and_flow()
        flow_tensor = tf.placeholder(tf.float32, np.shape(flow_ab))
        quadratic_motion = QuadraticMotionLayer(flow_tensor)
        hor_mask_tensor, ver_mask_tensor,\
        hor_constant_tensor, ver_constant_tensor, \
        a_indptr_tensor, a_cols_tensor = \
            quadratic_motion.hor_mask, quadratic_motion.ver_mask,\
            quadratic_motion.hor_constant, quadratic_motion.ver_constant,\
            quadratic_motion.a_indptr, quadratic_motion.a_cols

        hor_mask, ver_mask, hor_constant, ver_constant, a_indptr, a_cols = self.sess.run(
            [hor_mask_tensor, ver_mask_tensor, hor_constant_tensor, ver_constant_tensor,
             a_indptr_tensor, a_cols_tensor],
            feed_dict={flow_tensor: flow_ab})

        gt_hor_mask = np.linalg.norm(flow_ab[:, :, :-1] - flow_ab[:, :, 1:], axis=-1, keepdims=True)
        gt_hor_mask = (gt_hor_mask < quadratic_motion.flow_diff_thresh).astype(np.float32)
        gt_ver_mask = np.linalg.norm(flow_ab[:, :-1] - flow_ab[:, 1:], axis=-1, keepdims=True)
        gt_ver_mask = (gt_ver_mask < quadratic_motion.flow_diff_thresh).astype(np.float32)

        hor_mask = hor_mask.astype(np.float32)
        ver_mask = ver_mask.astype(np.float32)
        self.assertTupleEqual((1, 512, 511, 1), hor_mask.shape)
        self.assertTupleEqual((1, 511, 512, 1), ver_mask.shape)
        self.assertTrue(np.allclose(gt_hor_mask, hor_mask))
        self.assertTrue(np.allclose(gt_ver_mask, ver_mask))

        self.assertTupleEqual((1, 512, 511, 2), hor_constant.shape)
        self.assertTupleEqual((1, 511, 512, 2), ver_constant.shape)

        self.assertAlmostEqual(3805.982, np.sum(hor_constant), delta=0.001)
        self.assertAlmostEqual(-2863.809, np.sum(ver_constant), delta=0.001)

        self.assertTupleEqual((512 * 512 + 1,), a_indptr.shape)
        self.assertEqual(0, a_indptr[0])
        self.assertEqual(5, a_indptr[1])
        self.assertEqual((512 * 512) * 5, a_indptr[-1])

        self.assertEqual((512 * 512 * 5,), a_cols.shape)
        for i in range(512):
            for j in range(512):
                idx = (i * 512 + j)
                self.assertEqual(i * 512 + j, a_cols[idx * 5])
                if j + 1 < 512:
                    self.assertEqual(i * 512 + (j + 1), a_cols[idx * 5 + 1])
                if i + 1 < 512:
                    self.assertEqual((i + 1) * 512 + j, a_cols[idx * 5 + 2])
                if j - 1 >= 0:
                    self.assertEqual(i * 512 + (j - 1), a_cols[idx * 5 + 3])
                if i - 1 >= 0:
                    self.assertEqual((i - 1) * 512 + j, a_cols[idx * 5 + 4])

    def test_full_solve(self):
        # Batch size of 2.
        img_a, flow_ab = generate_test_image_and_flow()
        img_a_2, flow_ab_2 = generate_second_test_image_and_flow()
        img_a = np.concatenate([img_a, img_a_2], axis=0)
        flow_ab = np.concatenate([flow_ab, flow_ab_2], axis=0)

        flow_tensor_shape = list(flow_ab.shape)
        flow_tensor_shape[0] = None
        img_tensor_shape = list(img_a.shape)
        img_tensor_shape[0] = None

        # Input placeholders.
        t_tensor = tf.placeholder(tf.float32, None)
        flow_ab_tensor = tf.placeholder(tf.float32, flow_tensor_shape)
        mid_point_tensor = tf.placeholder(tf.float32, flow_tensor_shape)
        img_a_tensor = tf.placeholder(tf.float32, img_tensor_shape)

        # Graph.
        quadratic_motion = QuadraticMotionLayer(flow_ab_tensor)
        hor_mask_tensor, ver_mask_tensor = quadratic_motion.hor_mask, quadratic_motion.ver_mask
        mid_point_solve_tensor = quadratic_motion.mid_points
        splined_tensor = quadratic_bezier(flow_ab_tensor, mid_point_tensor, t_tensor)
        warp_tensor = forward_warp_bilinear(img_a_tensor, splined_tensor)
        warp_linear_tensor = forward_warp_bilinear(img_a_tensor, flow_ab_tensor * t_tensor)

        # Compute the midpoint.
        mid_point, hor_mask, ver_mask = self.sess.run([mid_point_solve_tensor, hor_mask_tensor, ver_mask_tensor],
                                                      feed_dict={flow_ab_tensor: flow_ab})
        hor_mask, ver_mask = hor_mask.astype(np.float32), ver_mask.astype(np.float32)

        # Assert the midpoint flows are near 0 everywhere except the part that's supposed to move.
        mid_point_zeros = np.copy(mid_point)
        mid_point_zeros[0, 256 - 18:256 + 18, 256:512] *= 0.0
        self.assertAlmostEqual(0.0, np.mean(mid_point_zeros), places=2)
        # Rotate the input image by 45 degrees.
        rot_matrix = cv2.getRotationMatrix2D((256, 256), -45, 1.0)
        rotated_a = cv2.warpAffine(img_a[0], rot_matrix, img_a[0].shape[1::-1], flags=cv2.INTER_LINEAR)
        # Assert that the error between the warp and the ground truth is small.
        output_halfway = self.sess.run(warp_tensor,
                                       feed_dict={flow_ab_tensor: flow_ab, img_a_tensor: img_a, t_tensor: 0.5,
                                                  mid_point_tensor: mid_point})
        output_halfway = output_halfway[0]
        error = np.mean(np.abs(rotated_a - output_halfway))
        self.assertLess(error, 0.004)

        if not WRITE_TO_VIDEO:
            return

        height, width, _ = img_a[0].shape
        trace_pixels_0 = []
        trace_pixels_1 = []
        for i in range(0, height, 32):
            for j in range(0, width, 32):
                trace_pixels_0.append([i, j])
                trace_pixels_1.append([i, j])
        traces_0 = []
        traces_1 = []

        # Midpoint visualizations.
        cv2.imwrite('outputs/hor_mask_0.png', hor_mask[0] * 255)
        cv2.imwrite('outputs/ver_mask_0.png', ver_mask[0] * 255)
        cv2.imwrite('outputs/mid_point_vis_0.png', get_flow_visualization(mid_point[0]))
        cv2.imwrite('outputs/flow_vis_0.png', get_flow_visualization(flow_ab[0]))
        cv2.imwrite('outputs/hor_mask_1.png', hor_mask[1] * 255)
        cv2.imwrite('outputs/ver_mask_1.png', ver_mask[1] * 255)
        cv2.imwrite('outputs/mid_point_vis_1.png', get_flow_visualization(mid_point[1]))
        cv2.imwrite('outputs/flow_vis_1.png', get_flow_visualization(flow_ab[1]))

        if not os.path.exists('outputs'):
            os.makedirs('outputs')

        def add_traces(splined_flow, traces):
            for pixel in trace_pixels_0:
                flow = splined_flow[pixel[0], pixel[1]]  # Flow is [x, y].
                traces.append([int(pixel[0] + flow[1]), int(pixel[1] + flow[0])])

        def draw_traces(traces, image):
            for pixel in traces:
                if 0 <= pixel[0] < height and 0 <= pixel[1] < width:
                    image[pixel[0], pixel[1]] = [1, 0, 0]

        height = img_a[0].shape[0]
        width = img_a[0].shape[1]
        writer_0 = cv2.VideoWriter('outputs/warped_0.avi',
                                   cv2.VideoWriter_fourcc(*'MJPG'), 20, (width * 2, height))
        writer_1 = cv2.VideoWriter('outputs/warped_1.avi',
                                   cv2.VideoWriter_fourcc(*'MJPG'), 20, (width * 2, height))
        steps = 60
        for i in range(steps):
            print('Writing video at step %d' % i)
            t = i * (1.0 / float(steps))
            warped_outputs = self.sess.run([warp_tensor, warp_linear_tensor, splined_tensor],
                                           feed_dict={flow_ab_tensor: flow_ab, img_a_tensor: img_a, t_tensor: t,
                                                      mid_point_tensor: mid_point})
            warped_0 = warped_outputs[0][0]
            warped_0 = np.clip(warped_0, 0.0, 1.0)
            warped_linear_0 = warped_outputs[1][0]
            warped_linear_0 = np.clip(warped_linear_0, 0.0, 1.0)
            splined_flow_0 = warped_outputs[2][0]
            add_traces(splined_flow_0, traces_0)
            draw_traces(traces_0, warped_0)
            concated_0 = np.concatenate((warped_0, warped_linear_0), axis=1)
            output_path = 'outputs/out_0-%.2f.png' % t
            write_image(output_path, concated_0)
            writer_0.write(cv2.imread(output_path))

            warped_1 = warped_outputs[0][1]
            warped_1 = np.clip(warped_1, 0.0, 1.0)
            warped_linear_1 = warped_outputs[1][1]
            warped_linear_1 = np.clip(warped_linear_1, 0.0, 1.0)
            splined_flow_1 = warped_outputs[2][1]
            add_traces(splined_flow_1, traces_1)
            draw_traces(traces_1, warped_1)
            concated_1 = np.concatenate((warped_1, warped_linear_1), axis=1)
            output_path = 'outputs/out_1-%.2f.png' % t
            write_image(output_path, concated_1)
            writer_1.write(cv2.imread(output_path))
        writer_0.release()
        writer_1.release()

    def test_visualize_stars(self):
        if not WRITE_TO_VIDEO:
            return

        flow_ab = np.asarray([read_flow_file('interp/flow_spline/test_data/star_flow.flo')])
        img_a = np.asarray([read_image('interp/flow_spline/test_data/star.png', as_float=True)])

        flow_tensor_shape = list(flow_ab.shape)
        flow_tensor_shape[0] = None
        img_tensor_shape = list(img_a.shape)
        img_tensor_shape[0] = None

        t_tensor = tf.placeholder(tf.float32, None)
        flow_ab_tensor = tf.placeholder(tf.float32, flow_tensor_shape)
        mid_point_tensor = tf.placeholder(tf.float32, flow_tensor_shape)
        img_a_tensor = tf.placeholder(tf.float32, img_tensor_shape)

        height, width, _ = img_a[0].shape
        trace_pixels_0 = []
        for i in range(0, height, 32):
            for j in range(0, width, 32):
                trace_pixels_0.append([i, j])
        traces_0 = []

        quadratic_motion = QuadraticMotionLayer(flow_ab_tensor)
        hor_mask_tensor, ver_mask_tensor = quadratic_motion.hor_mask, quadratic_motion.ver_mask
        mid_point_solve_tensor = quadratic_motion.mid_points
        mid_point, hor_mask, ver_mask = self.sess.run([mid_point_solve_tensor, hor_mask_tensor, ver_mask_tensor],
                                                      feed_dict={flow_ab_tensor: flow_ab})
        hor_mask, ver_mask = hor_mask.astype(np.float32), ver_mask.astype(np.float32)

        # Midpoint visualizations.
        cv2.imwrite('outputs/hor_mask_star.png', hor_mask[0] * 255)
        cv2.imwrite('outputs/ver_mask_star.png', ver_mask[0] * 255)
        cv2.imwrite('outputs/mid_point_vis_star.png', get_flow_visualization(mid_point[0]))
        cv2.imwrite('outputs/flow_vis_star.png', get_flow_visualization(flow_ab[0]))

        splined_tensor = quadratic_bezier(flow_ab_tensor, mid_point_tensor, t_tensor)
        warp_tensor = forward_warp_bilinear(img_a_tensor, splined_tensor)
        warp_linear_tensor = forward_warp_bilinear(img_a_tensor, flow_ab_tensor * t_tensor)

        if not os.path.exists('outputs'):
            os.makedirs('outputs')

        def add_traces(splined_flow, traces):
            for pixel in trace_pixels_0:
                flow = splined_flow[pixel[0], pixel[1]]  # Flow is [x, y].
                traces.append([int(pixel[0] + flow[1]), int(pixel[1] + flow[0])])

        def draw_traces(traces, image):
            for pixel in traces:
                if 0 <= pixel[0] < height and 0 <= pixel[1] < width:
                    image[pixel[0], pixel[1]] = [1, 0, 0]

        height = img_a[0].shape[0]
        width = img_a[0].shape[1]
        writer_0 = cv2.VideoWriter('outputs/warped_star.avi',
                                   cv2.VideoWriter_fourcc(*'MJPG'), 20, (width * 2, height))
        steps = 60
        for i in range(steps):
            print('Writing video at step %d' % i)
            t = i * (1.0 / float(steps))
            warped_outputs = self.sess.run([warp_tensor, warp_linear_tensor, splined_tensor],
                                           feed_dict={flow_ab_tensor: flow_ab, img_a_tensor: img_a, t_tensor: t,
                                                      mid_point_tensor: mid_point})
            warped_0 = warped_outputs[0][0]
            warped_0 = np.clip(warped_0, 0.0, 1.0)
            warped_linear_0 = warped_outputs[1][0]
            warped_linear_0 = np.clip(warped_linear_0, 0.0, 1.0)
            splined_flow_0 = warped_outputs[2][0]
            add_traces(splined_flow_0, traces_0)
            draw_traces(traces_0, warped_0)
            concated_0 = np.concatenate((warped_0, warped_linear_0), axis=1)
            output_path = 'outputs/out_star-%.2f.png' % t
            write_image(output_path, concated_0)
            writer_0.write(cv2.imread(output_path))
        writer_0.release()


if __name__ == '__main__':
    unittest.main()
