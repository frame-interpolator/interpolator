import numpy as np
import tensorflow as tf
from common.utils.profile import run_tensorboard_profiler
from interp.flow_spline.flow_spline import QuadraticMotionLayer
from interp.flow_spline.flow_spline_test import generate_test_image_and_flow

if __name__ == '__main__':
    _, flow_ab = generate_test_image_and_flow()
    flow_ab = np.concatenate([flow_ab, flow_ab], axis=0)

    # Create the graph.
    flow_tensor_shape = list(flow_ab.shape)
    flow_tensor_shape[0] = None
    t_tensor = tf.placeholder(tf.float32, None)
    flow_ab_tensor = tf.placeholder(tf.float32, flow_tensor_shape)
    mid_point_tensor = tf.placeholder(tf.float32, flow_tensor_shape)
    quadratic_motion = QuadraticMotionLayer(flow_ab_tensor)
    mid_point_solve_tensor = quadratic_motion.mid_points

    run_tensorboard_profiler([mid_point_solve_tensor], feed_dict={flow_ab_tensor: flow_ab},
                             num_runs=5, warmup_runs=3, name='solve_quadratic_flow', detailed=False)
