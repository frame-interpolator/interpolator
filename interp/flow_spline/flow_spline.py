import tensorflow as tf
from common.utils.tf import load_op_library


# Load op library.
mod = load_op_library('solve_quadratic_flow_op', 'build')


def _quadratic_motion_scipy_solver(height, width, a_data, a_cols, a_indptr, b, tolerance, max_iters):
    from scipy.sparse import csr_matrix
    from scipy.sparse.linalg import cg
    import numpy as np
    batch_size = b.shape[0]
    channels = b.shape[2]
    mid_points = []
    for batch_idx in range(batch_size):
        a_data_batch = a_data[batch_idx, ...]
        mid_points_batch = []
        for channel in range(channels):
            b_batch_channel = b[batch_idx, :, channel]
            A = csr_matrix((a_data_batch, a_cols, a_indptr), shape=(width * height, width * height))
            mid_points_batch_channel, _ = cg(A, b_batch_channel, maxiter=max_iters, atol=tolerance)
            mid_points_batch.append(np.expand_dims(mid_points_batch_channel, axis=-1))
        mid_points_batch = np.concatenate(mid_points_batch, axis=-1)
        mid_points.append(np.expand_dims(mid_points_batch, axis=0))
    mid_points = np.concatenate(mid_points, axis=0)
    return mid_points


class QuadraticMotionLayer:
    def __init__(self, flow, flow_diff_thresh=1.5, max_iters=1000, tolerance=1e-3, use_scipy_solver=False):
        """
        Given a flow, solves for the quadratic bezier midpoint.
        Optimizes for low deformation using the equations presented in http://hhoppe.com/morph.pdf.
        Note that some modifications have been made to the equations. The morph paper uses a halfway domain whereas we
        are using the t=0 domain. Here are the differences:
            p definition:
                Theirs: A point in the half-way domain (half way between the two images).
                Ours: A point in the first image (at alpha = 0).
            v definition:
                Theirs: Flow from the halfway point (alpha = 0.5) to alpha = 1.
                Ours: Flow from alpha = 0 to alpha = 1.
            w definition:
                Same definition.
            Bezier equation:
                Theirs: q = p + (2 * a - 1) * v(p) + 4 * a * (1 - a) * w(p)
                Ours: q = p + a * v(p) + 4 * a * (1 - a) * w(p)
            d_0:
                Theirs: d_0 = p_j - p_i - (v(p_j) - v(p_i))
                Ours: d_0 = p_j - p_i
            d_1:
                Same definition.
            d_0.5:
                Theirs: d_0.5 = p_j − p_i + (w(p_j) − w(p_i))
                Ours: d_0.5 = (p_j - p_i) + 0.5 * (v(p_j) - v(p_i)) + (w(p_j) - w(p_i))
            d_0.5_tilde:
                Same definition.
            Energy equations:
                Same definition.
        Additionally, since our flows are not smooth (i.e. they have sharp motion boundaries), we have to threshold
        motion boundaries that are too sharp (i.e. the difference in flow is too high). This thresholding can be
        specified via the flow_diff_thresh parameter.

        Usage:
            quadratic_motion = QuadraticMotionLayer(flow_ab_tensor)
            mid_point_solve_tensor = quadratic_motion.mid_points
            mid_point = self.sess.run(mid_point_solve_tensor, feed_dict={flow_ab_tensor: flow_ab})
        :param flow: Optical flow tensor. Shape of [B, H, W, 2].
        :param flow_diff_thresh: Maximum difference between adjacent flows. Difference measured in pixels.
        :param max_iters: Maximum number of conjugate gradient iterations.
        :param tolerance: Minimum tolerance to early-terminate the conjugate gradient solver.
        :param use_scipy_solver: Bool. Optionally run the scipy solver instead of the CUDA CG solver.
        """
        self.flow = flow
        self.flow_diff_thresh = flow_diff_thresh
        self.max_iters = max_iters
        self.tolerance = tolerance
        self.use_scipy_solver = use_scipy_solver

        _, self.height, self.width, _ = tf.unstack(tf.shape(flow))
        self.hor_mask, self.ver_mask, self.hor_constant, self.ver_constant,\
        self.a_data, self.a_cols, self.a_indptr, self.b, self.mid_points = self._solve_quadratic_flow()
        b_c0 = tf.expand_dims(self.b[:, 0, :], axis=-1)
        b_c1 = tf.expand_dims(self.b[:, 1, :], axis=-1)
        self.b = tf.concat([b_c0, b_c1], axis=-1)

        # self.mid_points is the final output with shape [B, H, W, 2].
        if self.use_scipy_solver:
            self.mid_points = self._get_scipy_solved_midpoints()
        else:
            mid_points_c0 = tf.expand_dims(self.mid_points[:, 0, :, :], axis=-1)
            mid_points_c1 = tf.expand_dims(self.mid_points[:, 1, :, :], axis=-1)
            self.mid_points = tf.concat([mid_points_c0, mid_points_c1], axis=-1)

    def _solve_quadratic_flow(self):
        return mod.solve_quadratic_flow(self.flow, flow_diff_thresh=self.flow_diff_thresh,
                                        max_iters=self.max_iters, tolerance=self.tolerance,
                                        run_solver=not self.use_scipy_solver)

    def _get_scipy_solved_midpoints(self):
        solved = tf.py_func(_quadratic_motion_scipy_solver, [self.height, self.width, self.a_data,
                                                             self.a_cols, self.a_indptr, self.b,
                                                             tf.constant(self.tolerance, dtype=tf.float32),
                                                             tf.constant(self.max_iters)], tf.float32)
        return tf.reshape(solved, [-1, self.height, self.width, 2])


def quadratic_bezier(flow, mid_point_offset, alpha):
    """
    Computes the quadratic bezier spline given a mid-point and alpha.
    :param flow: Optical flow tensor. Shape of [B, H, W, 2].
    :param mid_point_offset: Optical flow tensor. Shape of [B, H, W, 2].
        Actual midpoint of the spline will be 0.5 * flow + mid_point_offset.
    :param alpha: Floating point (scalar or tensor) between 0 and 1.
    :return: Splined optical flow.
    """
    return alpha * flow + 4 * alpha * (1 - alpha) * mid_point_offset
