#include <cusparse.h>
#include <cublas_v2.h>
#include <cuda_runtime.h>

#define VERBOSE_CG 0

struct CGState {
    cublasHandle_t cublasHandle;
    cusparseHandle_t cusparseHandle;
    cusparseMatDescr_t descr;
    cudaStream_t stream;

    float* d_r;
    float* d_Ax;
    float* d_p;

    int N;
    int nz;
};

// nz: # of values.
// N: # of rows/columns.
void CGSetup(struct CGState* state, int N, int nz, cudaStream_t stream) {
    state->stream = stream;
    // Get handle to the CUBLAS context.
    state->cublasHandle = 0;
    cublasCreate(&state->cublasHandle);
    cublasSetStream(state->cublasHandle, state->stream);
    // Get handle to the CUSPARSE context.
    state->cusparseHandle = 0;
    cusparseCreate(&state->cusparseHandle);
    cusparseSetStream(state->cusparseHandle, state->stream);

    // Create matrix descriptor.
    cusparseCreateMatDescr(&state->descr);
    cusparseSetMatType(state->descr, CUSPARSE_MATRIX_TYPE_GENERAL);
    cusparseSetMatIndexBase(state->descr, CUSPARSE_INDEX_BASE_ZERO);

    state->N = N;
    state->nz = nz;

    cudaMalloc((void**)&(state->d_p), N * sizeof(float));
    cudaMalloc((void**)&(state->d_Ax), N * sizeof(float));
    cudaMalloc((void**)&(state->d_r), N * sizeof(float));
}

void CGTeardown(struct CGState* state) {
    cusparseDestroy(state->cusparseHandle);
    cublasDestroy(state->cublasHandle);
    cusparseDestroyMatDescr(state->descr);

    cudaFree(state->d_p);
    cudaFree(state->d_Ax);
    cudaFree(state->d_r);
}

// d_r_in: right side (i.e. b).
// d_val: A values.
// d_col: A cols.
// d_row: A indptr.
// max_iter: maximum CG iterations.
// tol: termination tolerance.
void CG(const float* d_r_in, const float* d_val, const int* d_col, const int* d_row, float* d_x,
    const int max_iter, const float tol, struct CGState* state) {
    int N = state->N;
    int nz = state->nz;
    float* d_r;
    float* d_Ax;
    float dot;
    float* d_p;

    float alpha = 1.0f;
    float beta = 0.0f;
    float alpham1 = -1.0f;
    int k = 1;
    float a, b, na, r0, r1;

    cublasHandle_t cublasHandle = state->cublasHandle;
    cusparseHandle_t cusparseHandle = state->cusparseHandle;
    // Set sparse type.
    cusparseMatDescr_t descr = state->descr;

    d_p = state->d_p;
    d_Ax = state->d_Ax;
    d_r = state->d_r;
    cudaMemcpy(d_r, d_r_in, N * sizeof(float), cudaMemcpyDeviceToDevice);
    cudaMemset(d_x, 0, N * sizeof(float));

    cusparseScsrmv(cusparseHandle, CUSPARSE_OPERATION_NON_TRANSPOSE, N, N, nz, &alpha, descr, d_val, d_row, d_col,
        d_x, &beta, d_Ax);
    cublasSaxpy(cublasHandle, N, &alpham1, d_Ax, 1, d_r, 1);
    cublasSdot(cublasHandle, N, d_r, 1, d_r, 1, &r1);

    while (r1 > tol * tol && k <= max_iter)
    {
        if (k > 1)
        {
            b = r1 / r0;
            cublasSscal(cublasHandle, N, &b, d_p, 1);
            cublasSaxpy(cublasHandle, N, &alpha, d_r, 1, d_p, 1);
        }
        else
        {
            cublasScopy(cublasHandle, N, d_r, 1, d_p, 1);
        }

        cusparseScsrmv(cusparseHandle, CUSPARSE_OPERATION_NON_TRANSPOSE, N, N, nz, &alpha, descr, d_val,
            d_row, d_col, d_p, &beta, d_Ax);
        cublasSdot(cublasHandle, N, d_p, 1, d_Ax, 1, &dot);
        a = r1 / dot;

        cublasSaxpy(cublasHandle, N, &a, d_p, 1, d_x, 1);
        na = -a;
        cublasSaxpy(cublasHandle, N, &na, d_Ax, 1, d_r, 1);

        r0 = r1;
        cublasSdot(cublasHandle, N, d_r, 1, d_r, 1, &r1);
        cudaStreamSynchronize(state->stream);
#if VERBOSE_CG
        printf("iteration = %3d, residual = %e\n", k, sqrt(r1));
#endif
        k++;
    }
}
