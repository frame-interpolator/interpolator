#define EIGEN_USE_THREADS

#include <memory>
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/framework/types.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/common_shape_fns.h"

typedef Eigen::ThreadPoolDevice CPUDevice;
typedef Eigen::GpuDevice GPUDevice;

using namespace tensorflow;

void ComputeConstants(const GPUDevice& d,
	typename TTypes<float, 4>::ConstTensor flows,
	typename TTypes<bool, 4>::Tensor hor_mask,
	typename TTypes<bool, 4>::Tensor ver_mask,
	typename TTypes<float, 4>::Tensor hor_constant,
	typename TTypes<float, 4>::Tensor ver_constant,
	float flow_diff_thresh);

void ConstructMatrix(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor flows,
	typename TTypes<bool, 4>::ConstTensor hor_mask,
	typename TTypes<bool, 4>::ConstTensor ver_mask,
	typename TTypes<float, 4>::ConstTensor hor_constant,
	typename TTypes<float, 4>::ConstTensor ver_constant,
	typename TTypes<float, 2>::Tensor a_data,
	typename TTypes<int, 1>::Tensor a_cols,
	typename TTypes<int, 1>::Tensor a_indptr,
	typename TTypes<float, 3>::Tensor b);

void SolveMatrix(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor flows,
    typename TTypes<float, 2>::ConstTensor a_data,
	typename TTypes<int, 1>::ConstTensor a_cols,
	typename TTypes<int, 1>::ConstTensor a_indptr,
	typename TTypes<float, 3>::ConstTensor b,
	typename TTypes<float, 4>::Tensor midpoints,
	int max_iters, float tolerance, bool run_solver);

// This GPU op implements the full solving pipeline for quadratic flow described in http://hhoppe.com/morph.pdf.
// Note that our equations differ a bit from the morph paper. Here are the differences:
//      p definition:
//          Theirs: A point in the half-way domain (half way between the two images).
//          Ours: A point in the first image (at alpha = 0).
//      v definition:
//          Theirs: Flow from the halfway point (alpha = 0.5) to alpha = 1.
//          Ours: Flow from alpha = 0 to alpha = 1.
//      w definition:
//          Same definition.
//      Bezier equation:
//          Theirs: q = p + (2 * a - 1) * v(p) + 4 * a * (1 - a) * w(p)
//          Ours: q = p + a * v(p) + 4 * a * (1 - a) * w(p)
//      d_0:
//          Theirs: d_0 = p_j - p_i - (v(p_j) - v(p_i))
//          Ours: d_0 = p_j - p_i
//      d_1:
//          Same definition.
//      d_0.5:
//          Theirs: d_0.5 = p_j − p_i + (w(p_j) − w(p_i))
//          Ours: d_0.5 = (p_j - p_i) + 0.5 * (v(p_j) - v(p_i)) + (w(p_j) - w(p_i))
//      d_0.5_tilde:
//          Same definition.
//      Energy equations:
//          Same definition.
//
// To perform the optimization, we are essentially taking the gradient of the energy function, setting it to zero, and
// solving for the resulting system of linear equations (A * x = b). x is a vector of midpoint variables. A and b can
// be derived as follows:
//      The loss is summed over all vertical and horizontal pairs. For a single pair:
//          Let E_ji = Energy between point j and point i.
//          E_ji = ||d_0.5 - d_0.5_tilde||^2
//               = ||(p_j - p_i) + 0.5 * (v(p_j) - v(p_i)) + (w(p_j) - w(p_i)) -
//                   sqrt(||d_0|| * ||d_1||) * d_s_hat||^2
//               = ||(w(p_j) - w(p_i)) + c_ji)||^2
//          where c_ji = all the other terms = (p_j - p_i) + 0.5 * (v(p_j) - v(p_i)) - sqrt(||d_0|| * ||d_1||) * d_s_hat
//
//      Okay now let's switch indexing notations (letting i and j represent 2D image indices).
//          Let (i = row index, j = column index) of an optical flow.
//          Let w_ij be a matrix of midpoint variables corresponding to an optical flow.
//          Let c_ijh be the constant between pixel ij and its horizontal neighbour i(j+1).
//          Let c_ijv be the constant between pixel ij and its horizontal neighbour (i+1)j.
//
//      Notice that the energy function for any w_ij has 4 terms corresponding to each horizontal and vertical
//      neighbour plus the resting energy term:
//          E_ij = ||w_ij - w_i(j+1) + c_ijh||^2 + ||w_ij - w_(i+1)j + c_ijv||^2 +
//                 ||w_i(j-1) - w_ij + c_i(j-1)h||^2  + ||w_(i-1)j - w_ij + c_(i-1)jv||^2 +
//                 (1 - ||v_ij||) * ||w_ij||^2
//      Our goal is to optimize E_ji for w_ji, so take its derivative with respect to w_ji and set it to 0.
//          dE_ji/dw_ji = 0 = 2(w_ij - w_i(j+1) + c_ijh) + 2(w_ij - w_(i+1)j + c_ijv)
//                            - 2(w_i(j-1) - w_ij + c_i(j-1)h) - 2(w_(i-1)j - w_ij + c_(i-1)jv)
//                            + 2(1 - ||v_ij||) * w_ij
//      If you cancel the 2s and group up the terms, this looks like a linear equation:
//          (4 + (1 - ||v_ij||)) * w_ij - w_i(j+1) - w_(i+1)j - w_i(j-1) - w_(i-1)j =
//              - c_ijh - c_ijv + c_i(j-1)h + c_(i-1)jv
//
//      There's 1 linear equation per gradient term, so this forms a system of linear equations A * x = b:
//          Notice that A is a matrix with (i * j) rows and (i * j) columns, but there are a lot of zeros in the
//          columns. Basically, A is pretty sparse and we can represent it more efficiently in CSR format.
//          b_ij = - c_ijh - c_ijv + c_i(j-1)h + c_(i-1)jv
//          x_ij = w_ij = unknown values that we'd like to solve for.
//          Let (i, j) = j + width * i  -- row major order indexing
//          A_ij = row i*j = data: [4 + (1 - ||v_ij||), -1, -1, -1, -1]
//                           columns: [(i,j), (i,j+1), (i+1,j), (i,j-1), (i-1,j)]
//      A * x = b is then solved by shoving it into a conjugate gradient solver.
//
//      Note that at the edges of the flow image, horizontal or vertical pairs may not exist because the neighbours
//      are out of bounds. In these cases, the gradient terms (values in A and b) are dropped and set to 0.
//      Also, since our flows are not smooth (i.e. they have sharp motion boundaries), we have to threshold motion
//      boundaries that are too sharp (i.e. the difference in flow is too high). This produces horizontal and vertical
//      masks. If the mask is not 1, then the corresponding gradient terms (values in A and b) are dropped and set to 0.
//
//      The implementation is split into 3 stages:
//          1) Computation of the constants c_ij, and thresholding masks.
//          2) Constructing the matrix A and vector b.
//          3) Solving for x using a conjugate gradient solver.
//
//      Note: the x and y components of a w_ij are solved independently because ||w_ij||^2 is just w_ij_x^2 + w_ij_y^2.
//          As a result, you'll see that the CG solver is called twice (once for each of the x and y components).
class SolveQuadraticFlowOp : public OpKernel {
public:
	explicit SolveQuadraticFlowOp(OpKernelConstruction* context) : OpKernel(context) {
	    OP_REQUIRES_OK(context, context->GetAttr("flow_diff_thresh", &flow_diff_thresh_));
		OP_REQUIRES(context, flow_diff_thresh_ >= 0.0f,
			errors::InvalidArgument("Need flow_diff_thresh >= 0, got ", flow_diff_thresh_));

        OP_REQUIRES_OK(context, context->GetAttr("max_iters", &max_iters_));
		OP_REQUIRES(context, max_iters_ > 0,
			errors::InvalidArgument("Need max_iters > 0, got ", max_iters_));

        OP_REQUIRES_OK(context, context->GetAttr("tolerance", &tolerance_));
		OP_REQUIRES(context, tolerance_ > 0.0f,
			errors::InvalidArgument("Need flow_diff_thresh > 0, got ", tolerance_));

        OP_REQUIRES_OK(context, context->GetAttr("run_solver", &run_solver_));
	}

	void Compute(OpKernelContext* context) override {
		const Tensor& flow = context->input(0);

        // Assert correct number of dimensions.
        OP_REQUIRES(context, flow.dims() == 4,
                    errors::InvalidArgument("Flow expecting a 4-D vector."));

        // Assert dimension sizes.
        OP_REQUIRES(context, flow.dim_size(1) >= 2,
                    errors::InvalidArgument("Flow height must be at least 2."));
        OP_REQUIRES(context, flow.dim_size(2) >= 2,
                    errors::InvalidArgument("Flow width must be at least 2."));
        OP_REQUIRES(context, flow.dim_size(3) == 2,
                    errors::InvalidArgument("Flow has invalid dimensions."));

		typename TTypes<float, 4>::ConstTensor flow_data = flow.tensor<float, 4>();

        // Output shapes.
        TensorShape hor_mask_shape = flow.shape();
        hor_mask_shape.set_dim(2, flow.dim_size(2) - 1);
        hor_mask_shape.set_dim(3, 1);
        TensorShape ver_mask_shape = flow.shape();
        ver_mask_shape.set_dim(1, flow.dim_size(1) - 1);
        ver_mask_shape.set_dim(3, 1);
        TensorShape hor_shape = flow.shape();
        hor_shape.set_dim(2, flow.dim_size(2) - 1);
        TensorShape ver_shape = flow.shape();
        ver_shape.set_dim(1, flow.dim_size(1) - 1);
        // Allocate outputs.
		Tensor* hor_mask = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(0, hor_mask_shape, &hor_mask));
		Tensor* ver_mask = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(1, ver_mask_shape, &ver_mask));
        Tensor* hor_constant = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(2, hor_shape, &hor_constant));
        Tensor* ver_constant = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(3, ver_shape, &ver_constant));
        // Data.
        typename TTypes<bool, 4>::Tensor hor_mask_data = hor_mask->tensor<bool, 4>();
        typename TTypes<bool, 4>::Tensor ver_mask_data = ver_mask->tensor<bool, 4>();
        typename TTypes<float, 4>::Tensor hor_constant_data = hor_constant->tensor<float, 4>();
        typename TTypes<float, 4>::Tensor ver_constant_data = ver_constant->tensor<float, 4>();
        // Compute.
		ComputeConstants(context->eigen_device<GPUDevice>(), flow_data, hor_mask_data, ver_mask_data,
            hor_constant_data, ver_constant_data, flow_diff_thresh_);

        // Output shapes.
        TensorShape a_data_shape({flow.dim_size(0), flow.dim_size(1) * flow.dim_size(2) * 5});
        TensorShape a_cols_shape({flow.dim_size(1) * flow.dim_size(2) * 5});  // # cols * 5.
        TensorShape a_indptr_shape({flow.dim_size(1) * flow.dim_size(2) + 1});  // # cols + 1.
        TensorShape b_shape({flow.dim_size(0), 2, flow.dim_size(1) * flow.dim_size(2)});
        // Allocate outputs.
        Tensor* a_data = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(4, a_data_shape, &a_data));
        Tensor* a_cols = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(5, a_cols_shape, &a_cols));
        Tensor* a_indptr = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(6, a_indptr_shape, &a_indptr));
        Tensor* b = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(7, b_shape, &b));
        // Data.
        typename TTypes<float, 2>::Tensor a_data_data = a_data->tensor<float, 2>();
        typename TTypes<int, 1>::Tensor a_cols_data = a_cols->tensor<int, 1>();
        typename TTypes<int, 1>::Tensor a_indptr_data = a_indptr->tensor<int, 1>();
        typename TTypes<float, 3>::Tensor b_data = b->tensor<float, 3>();
        // Cast previous outputs to const.
        typename TTypes<bool, 4>::ConstTensor hor_mask_const_data =
                ((const Tensor*)hor_mask)->tensor<bool, 4>();
        typename TTypes<bool, 4>::ConstTensor ver_mask_const_data =
                ((const Tensor*)ver_mask)->tensor<bool, 4>();
        typename TTypes<float, 4>::ConstTensor hor_constant_const_data =
                ((const Tensor*)hor_constant)->tensor<float, 4>();
        typename TTypes<float, 4>::ConstTensor ver_constant_const_data =
                ((const Tensor*)ver_constant)->tensor<float, 4>();
        // Compute.
        ConstructMatrix(context->eigen_device<GPUDevice>(), flow_data, hor_mask_const_data, ver_mask_const_data,
            hor_constant_const_data, ver_constant_const_data, a_data_data, a_cols_data, a_indptr_data, b_data);

        // Output shapes.
        TensorShape midpoints_shape({flow.dim_size(0), 2, flow.dim_size(1), flow.dim_size(2)});
        // Allocate outputs.
        Tensor* midpoints = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(8, midpoints_shape, &midpoints));
        // Data.
        typename TTypes<float, 4>::Tensor midpoints_data = midpoints->tensor<float, 4>();
        // Cast previous outputs to const.
        typename TTypes<float, 2>::ConstTensor a_data_const_data =
            ((const Tensor*)a_data)->tensor<float, 2>();
        typename TTypes<int, 1>::ConstTensor a_cols_const_data =
            ((const Tensor*)a_cols)->tensor<int, 1>();
        typename TTypes<int, 1>::ConstTensor a_indptr_const_data =
            ((const Tensor*)a_indptr)->tensor<int, 1>();
        typename TTypes<float, 3>::ConstTensor b_const_data =
            ((const Tensor*)b)->tensor<float, 3>();
        // Compute.
        SolveMatrix(context->eigen_device<GPUDevice>(), flow_data, a_data_const_data, a_cols_const_data,
            a_indptr_const_data, b_const_data, midpoints_data, max_iters_, tolerance_, run_solver_);
	}

private:
    float flow_diff_thresh_;
    int max_iters_;
    float tolerance_;
    bool run_solver_;
};

using shape_inference::DimensionHandle;
using shape_inference::ShapeHandle;

REGISTER_OP("SolveQuadraticFlow")
.Attr("flow_diff_thresh: float = 2.0")
.Attr("max_iters: int = 1000")
.Attr("tolerance: float = 0.00001")
.Attr("run_solver: bool = true")
.Input("flows: float")
.Output("hor_mask: bool")
.Output("ver_mask: bool")
.Output("hor_constant: float")
.Output("ver_constant: float")
.Output("a_data: float")
.Output("a_cols: int32")
.Output("a_indptr: int32")
.Output("b: float")
.Output("midpoints: float")
.SetShapeFn(shape_inference::UnchangedShape);

#if GOOGLE_CUDA

REGISTER_KERNEL_BUILDER(Name("SolveQuadraticFlow").Device(DEVICE_GPU), SolveQuadraticFlowOp);

#endif // GOOGLE_CUDA
