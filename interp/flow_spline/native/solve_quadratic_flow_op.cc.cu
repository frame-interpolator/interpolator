#if GOOGLE_CUDA

#define EIGEN_USE_GPU

#define _USE_MATH_DEFINES
#include <cmath>

#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/util/cuda_kernel_helper.h"

#include "cusparse_cg.h"

#define EPSILON 1e-10f

using namespace tensorflow;

typedef Eigen::GpuDevice GPUDevice;

__device__ float clamp(float value, float min, float max) {
    if (value < min) value = min;
    if (value > max) value = max;
    return value;
}

// Computes horizontal and vertical masks.
// Also computes c_ji = (p_j - p_i) + 0.5 * (v(p_j) - v(p_i)) - sqrt(||d_0|| * ||d_1||) * d_s_hat.
// Masks are stored in hor_mask, ver_mask.
// c_ij values are stored in hor_constants, ver_constants.
__global__ void ComputeHorizontalAndVerticalDeformationConstants(const int32 nthreads,
	const float* flows, int batch, int height, int width,
	bool* hor_mask, bool* ver_mask,
	float* hor_constant, float* ver_constant, float flow_diff_thresh) {
	CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
		// out_idx = src_x + width * (src_y + height * b).
		int idx = out_idx;
		const int pixel_index = idx;
		const int flow_index = pixel_index * 2;
		const int src_x = idx % width;
		idx /= width;
		const int src_y = idx % height;
		const int b = idx / height;

#define FLOW_OFFSET(iy, ix) (2 * (ix + width * (iy + height * b)))
#define HOR_OFFSET(iy, ix) (ix + (width - 1) * (iy + height * b))
#define VER_OFFSET(iy, ix) (ix + width * (iy + (height - 1) * b))

        const float flow_x = flows[flow_index];
        const float flow_y = flows[flow_index + 1];

        if (src_x != width - 1) {
            // Horizontal.
            const float n_flow_x = flows[FLOW_OFFSET(src_y, src_x + 1)];
            const float n_flow_y = flows[FLOW_OFFSET(src_y, src_x + 1) + 1];

            const float flow_diff_x = flow_x - n_flow_x;
            const float flow_diff_y = flow_y - n_flow_y;
            const float distance = sqrtf(flow_diff_x * flow_diff_x + flow_diff_y * flow_diff_y);
            hor_mask[HOR_OFFSET(src_y, src_x)] = distance < flow_diff_thresh;

            const float p_diff_y = 0.0f;
            const float p_diff_x = -1.0f;

            const float d_0_x = p_diff_x;
            const float d_0_y = p_diff_y;
            const float d_1_x = p_diff_x + flow_diff_x;
            const float d_1_y = p_diff_y + flow_diff_y;
            const float d_0_norm = 1.0f;
            const float d_1_norm = sqrtf(d_1_x * d_1_x + d_1_y * d_1_y) + EPSILON;
            const float d_s_x = d_0_x / d_0_norm + d_1_x / d_1_norm;
            const float d_s_y = d_0_y / d_0_norm + d_1_y / d_1_norm;
            const float d_s_norm = sqrtf(d_s_x * d_s_x + d_s_y * d_s_y) + EPSILON;
            const float d_half_expected_norm = sqrtf(d_0_norm * d_1_norm);
            const float d_half_expected_x = d_half_expected_norm * (d_s_x / d_s_norm);
            const float d_half_expected_y = d_half_expected_norm * (d_s_y / d_s_norm);
            const float d_half_constant_x = p_diff_x + 0.5f * flow_diff_x;
            const float d_half_constant_y = p_diff_y + 0.5f * flow_diff_y;

            hor_constant[2 * HOR_OFFSET(src_y, src_x)] = d_half_constant_x - d_half_expected_x;
            hor_constant[2 * HOR_OFFSET(src_y, src_x) + 1] = d_half_constant_y - d_half_expected_y;
        }

        if (src_y != height - 1) {
            // Vertical.
            const float n_flow_x = flows[FLOW_OFFSET(src_y + 1, src_x)];
            const float n_flow_y = flows[FLOW_OFFSET(src_y + 1, src_x) + 1];

            const float flow_diff_x = flow_x - n_flow_x;
            const float flow_diff_y = flow_y - n_flow_y;
            const float distance = sqrtf(flow_diff_x * flow_diff_x + flow_diff_y * flow_diff_y);
            ver_mask[VER_OFFSET(src_y, src_x)] = distance < flow_diff_thresh;

            const float p_diff_y = -1.0f;
            const float p_diff_x = 0.0f;

            const float d_0_x = p_diff_x;
            const float d_0_y = p_diff_y;
            const float d_1_x = p_diff_x + flow_diff_x;
            const float d_1_y = p_diff_y + flow_diff_y;
            const float d_0_norm = 1.0f;
            const float d_1_norm = sqrtf(d_1_x * d_1_x + d_1_y * d_1_y) + EPSILON;
            const float d_s_x = d_0_x / d_0_norm + d_1_x / d_1_norm;
            const float d_s_y = d_0_y / d_0_norm + d_1_y / d_1_norm;
            const float d_s_norm = sqrtf(d_s_x * d_s_x + d_s_y * d_s_y) + EPSILON;
            const float d_half_expected_norm = sqrtf(d_0_norm * d_1_norm);
            const float d_half_expected_x = d_half_expected_norm * (d_s_x / d_s_norm);
            const float d_half_expected_y = d_half_expected_norm * (d_s_y / d_s_norm);
            const float d_half_constant_x = p_diff_x + 0.5f * flow_diff_x;
            const float d_half_constant_y = p_diff_y + 0.5f * flow_diff_y;

            ver_constant[2 * VER_OFFSET(src_y, src_x)] = d_half_constant_x - d_half_expected_x;
            ver_constant[2 * VER_OFFSET(src_y, src_x) + 1] = d_half_constant_y - d_half_expected_y;
        }

#undef VER_OFFSET
#undef HOR_OFFSET
#undef FLOW_OFFSET
	}
}

__global__ void FillAIndptr(const int32 nthreads,
	int* indptr) {
	CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
	    indptr[out_idx] = 5 * out_idx;
	}
}

// Computes cols = [(i,j), (i,j+1), (i+1,j), (i,j-1), (i-1,j)]
// where (i, j) = j + width * i  -- row major order indexing.
__global__ void FillACols(const int32 nthreads,
	int height, int width, int* a_cols) {
	CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
	    // out_idx = src_x + width * src_y.
	    int idx = out_idx;
        const int col = idx % width;
		idx /= width;
		const int row = idx % height;

#define COL(iy, ix) ((ix) + width * (iy))
        int val;
        bool oob;
		a_cols[out_idx * 5] = out_idx;

		oob = (col + 1) >= width;
		val = oob ? 0 : COL(row, col + 1);
		a_cols[out_idx * 5 + 1] = val;

		oob = (row + 1) >= height;
		val = oob ? 0 : COL(row + 1, col);
		a_cols[out_idx * 5 + 2] = val;

        oob = (col - 1) < 0;
		val = oob ? 0 : COL(row, col - 1);
		a_cols[out_idx * 5 + 3] = val;

        oob = (row - 1) < 0;
		val = oob ? 0 : COL(row - 1, col);
		a_cols[out_idx * 5 + 4] = val;
#undef COL
	}
}

// Computes A_ij = row i*j = [4 + (1 - ||v_ij||), -1, -1, -1, -1]
// This function takes into account pairs that are out of bounds and the masking.
__global__ void ComputeAData(const int32 nthreads,
    const float* flows, int batch, int height, int width, const bool* hor_mask, const bool* ver_mask,
    float* a_data) {
	CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
	    // out_idx = src_x + width * (src_y + height * b).
	    int idx = out_idx;
		const int pixel_index = idx;
		const int flow_index = pixel_index * 2;
		const int src_x = idx % width;
		idx /= width;
		const int src_y = idx % height;
		const int b = idx / height;

        const float flow_x = flows[flow_index];
        const float flow_y = flows[flow_index + 1];
        const float flow_norm = sqrtf(flow_x * flow_x + flow_y * flow_y);
        const float clamped_flow_norm = clamp(flow_norm, 0.0f, 1.0f);
        const float normalizing_weight = 1 - clamped_flow_norm;

#define MASK_OFFSET(iy, ix, w, h) ((ix) + (w) * ((iy) + (h) * b))
#define BOUNDS_OFFSET(idx) ((idx) + 5 * (src_x + width * src_y))
#define A_OFFSET(idx) (idx + 5 * out_idx)
        a_data[A_OFFSET(0)] = normalizing_weight;
        a_data[A_OFFSET(1)] = a_data[A_OFFSET(2)] = a_data[A_OFFSET(3)] = a_data[A_OFFSET(4)] = 0.0f;

        // If statements take into account the out of bounds and masked pairs.
        if (src_x < (width - 1) && hor_mask[MASK_OFFSET(src_y, src_x, width - 1, height)]) {
            a_data[A_OFFSET(1)] = -1.0f;
            a_data[A_OFFSET(0)] += 1.0f;
        }
        if (src_y < (height - 1) && ver_mask[MASK_OFFSET(src_y, src_x, width, height - 1)]) {
            a_data[A_OFFSET(2)] = -1.0f;
            a_data[A_OFFSET(0)] += 1.0f;
        }
        if ((src_x - 1) >= 0 && hor_mask[MASK_OFFSET(src_y, src_x - 1, width - 1, height)]) {
            a_data[A_OFFSET(3)] = -1.0f;
            a_data[A_OFFSET(0)] += 1.0f;
        }
        if ((src_y - 1) >= 0 && ver_mask[MASK_OFFSET(src_y - 1, src_x, width, height - 1)]) {
            a_data[A_OFFSET(4)] = -1.0f;
            a_data[A_OFFSET(0)] += 1.0f;
        }
#undef A_OFFSET
#undef BOUNDS_OFFSET
#undef MASK_OFFSET
	}
}

__device__ void ComputeBHelper(int c, int b, int height, int width,
	const bool* hor_mask, const bool* ver_mask,
	const float* hor_constant, const float* ver_constant, float* b_ptr,
	const int src_x, const int src_y, const int out_idx) {
	// Offset = src_x + width * (src_y + height * (c + 2 * b)).
#define MASK_OFFSET(iy, ix, w, h) ((ix) + (w) * ((iy) + (h) * b))
#define OFFSET(iy, ix, w, h) (c + 2 * ((ix) + (w) * ((iy) + (h) * b)))
#define B_OFFSET (src_x + width * (src_y + height * (c + 2 * b)))
        {
            int offset;
            float c_y_x_h = 0.0f;
            float c_y_x_v = 0.0f;
            float c_y_xm_h = 0.0f;
            float c_ym_x_v = 0.0f;

            if (src_x < (width - 1) && hor_mask[MASK_OFFSET(src_y, src_x, width - 1, height)]) {
                c_y_x_h = hor_constant[OFFSET(src_y, src_x, width - 1, height)];
            }
            if (src_y < (height - 1) && ver_mask[MASK_OFFSET(src_y, src_x, width, height - 1)]) {
                c_y_x_v = ver_constant[OFFSET(src_y, src_x, width, height - 1)];
            }
            if ((src_x - 1) >= 0 && hor_mask[MASK_OFFSET(src_y, src_x - 1, width - 1, height)]) {
                c_y_xm_h = hor_constant[OFFSET(src_y, src_x - 1, width - 1, height)];
            }
            if ((src_y - 1) >= 0 && ver_mask[MASK_OFFSET(src_y - 1, src_x, width, height - 1)]) {
                c_ym_x_v = ver_constant[OFFSET(src_y - 1, src_x, width, height - 1)];
            }

            b_ptr[B_OFFSET] = -c_y_x_h - c_y_x_v + c_y_xm_h + c_ym_x_v;
        }
#undef B_OFFSET
#undef OFFSET
#undef MASK_OFFSET
}

// Computes b_ij = - c_ijh - c_ijv + c_i(j-1)h + c_(i-1)jv.
// This function takes into account pairs that are out of bounds and the masking.
__global__ void ComputeB(const int32 nthreads,
    int batch, int height, int width,
	const bool* hor_mask, const bool* ver_mask,
	const float* hor_constant, const float* ver_constant,
	float* b_ptr) {
	CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
	    // out_idx = src_x + width * (src_y + height * b).
	    int idx = out_idx;
		const int src_x = idx % width;
		idx /= width;
		const int src_y = idx % height;
		const int b = idx / height;

        ComputeBHelper(0, b, height, width, hor_mask, ver_mask, hor_constant, ver_constant, b_ptr,
            src_x, src_y, out_idx);
        ComputeBHelper(1, b, height, width, hor_mask, ver_mask, hor_constant, ver_constant, b_ptr,
            src_x, src_y, out_idx);
	}
}

void ComputeConstants(const GPUDevice& d,
	typename TTypes<float, 4>::ConstTensor flows,
	typename TTypes<bool, 4>::Tensor hor_mask,
	typename TTypes<bool, 4>::Tensor ver_mask,
	typename TTypes<float, 4>::Tensor hor_constant,
	typename TTypes<float, 4>::Tensor ver_constant,
    float flow_diff_thresh) {
	const int batch = flows.dimension(0);
	const int height = flows.dimension(1);
	const int width = flows.dimension(2);
	const int channels = flows.dimension(3);

	const int total_count = batch * height * width;
	if (total_count == 0) return;

	CudaLaunchConfig config;

	config = GetCudaLaunchConfig(total_count, d);
	ComputeHorizontalAndVerticalDeformationConstants
		<< <config.block_count, config.thread_per_block, 0, d.stream() >> >(
			config.virtual_thread_count, flows.data(),
			batch, height, width,
			hor_mask.data(), ver_mask.data(),
			hor_constant.data(), ver_constant.data(), flow_diff_thresh);
}

void ConstructMatrix(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor flows,
	typename TTypes<bool, 4>::ConstTensor hor_mask,
	typename TTypes<bool, 4>::ConstTensor ver_mask,
	typename TTypes<float, 4>::ConstTensor hor_constant,
	typename TTypes<float, 4>::ConstTensor ver_constant,
	typename TTypes<float, 2>::Tensor a_data,
	typename TTypes<int, 1>::Tensor a_cols,
	typename TTypes<int, 1>::Tensor a_indptr,
	typename TTypes<float, 3>::Tensor b) {
	const int batch = flows.dimension(0);
	const int height = flows.dimension(1);
	const int width = flows.dimension(2);
	int total_count;
	CudaLaunchConfig config;

    // a_indptrs.
	total_count = height * width + 1;
	config = GetCudaLaunchConfig(total_count, d);
	FillAIndptr
		<< <config.block_count, config.thread_per_block, 0, d.stream() >> >(
			config.virtual_thread_count, a_indptr.data());

    // a_cols.
    total_count = height * width;
    if (total_count == 0) return;
	config = GetCudaLaunchConfig(total_count, d);
	FillACols
		<< <config.block_count, config.thread_per_block, 0, d.stream() >> >(
			config.virtual_thread_count, height, width, a_cols.data());

    // a_data.
    total_count = batch * height * width;
    if (total_count == 0) return;
	config = GetCudaLaunchConfig(total_count, d);
	ComputeAData
		<< <config.block_count, config.thread_per_block, 0, d.stream() >> >(
			config.virtual_thread_count, flows.data(), batch, height, width,
	        hor_mask.data(), ver_mask.data(), a_data.data());

    // b.
	ComputeB
		<< <config.block_count, config.thread_per_block, 0, d.stream() >> >(
			config.virtual_thread_count, batch, height, width,
			hor_mask.data(), ver_mask.data(),
	        hor_constant.data(), ver_constant.data(),
	        b.data());
}

void SolveMatrix(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor flows,
    typename TTypes<float, 2>::ConstTensor a_data,
	typename TTypes<int, 1>::ConstTensor a_cols,
	typename TTypes<int, 1>::ConstTensor a_indptr,
	typename TTypes<float, 3>::ConstTensor b,
	typename TTypes<float, 4>::Tensor midpoints,
	int max_iters, float tolerance, bool run_solver) {
    const int batch = flows.dimension(0);
	const int height = flows.dimension(1);
	const int width = flows.dimension(2);

    if (!run_solver) {
        cudaMemset(midpoints.data(), 0, batch * 2 * height * width);
        return;
    }

    struct CGState state;
    CGSetup(&state, width * height, width * height * 5, d.stream());
    for (int i = 0; i < batch; ++i) {
        // b offset = src_x + width * (src_y + height * (c + 2 * b)).
        const float* b_0_batch = b.data() + width * height * (0 + 2 * i);
        const float* b_1_batch = b.data() + width * height * (1 + 2 * i);
        // A offset = 5 * (src_x + width * (src_y + height * b)).
        const float* a_data_batch = a_data.data() + 5 * width * height * i;
        // Midpoints offset is the same as b offset.
        float* midpoints_0_batch = midpoints.data() + width * height * (0 + 2 * i);
        float* midpoints_1_batch = midpoints.data() + width * height * (1 + 2 * i);

        CG(b_0_batch, a_data_batch, a_cols.data(), a_indptr.data(), midpoints_0_batch, max_iters, tolerance, &state);
        CG(b_1_batch, a_data_batch, a_cols.data(), a_indptr.data(), midpoints_1_batch, max_iters, tolerance, &state);
    }
    CGTeardown(&state);
}

#endif  // GOOGLE_CUDA
