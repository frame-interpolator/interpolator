import tensorflow as tf
import os
import shutil
from common.models import FrozenModel
from interp.interp import Interp
# Imports the ops required for InterpSavedModels.
from pwcnet import model
from common.forward_warp.forward_warp_interp import forward_warp_interp


class InterpSavedModel(FrozenModel):
    def __init__(self, directory='saved_model'):
        """
        :param directory: Str. Path to the SavedModel export directory.
        """
        super().__init__(directory)
        self.images_0 = None
        self.images_1 = None
        self.t = None
        self.interpolated = None
        self.graph_created = False

    def run(self, inputs):
        """
        :param inputs: Tuple of:
                Numpy array. Image of shape [batch_size, H, W, C]. The image at t = 0.
                Numpy array. Image of shape [batch_size, H, W, C]. The image at t = 1.
                Float. Specifies the interpolation point (i.e 0 for images_0, 1 for images_1).
        :return: Numpy array of shape [batch_size, H, W, C]. The interpolation at time t.
        """
        assert isinstance(inputs, tuple) and len(inputs) == 3
        assert self.directory is not None
        images_0, images_1, t = inputs
        if self.predictor is None:
            self.load()
        predictions = self.predictor({
            'images_0': images_0,
            'images_1': images_1,
            't': t
        })
        return predictions['output']

    def freeze(self, model, session):
        """
        :param session: Tf Session.
        :param model: An instance of class Interp.
        """
        assert isinstance(model, Interp)
        if not self.graph_created:
            self.images_0, self.images_1, self.t, self.interpolated = self._get_tensors_for_saved_model(model)
            self.graph_created = True
        assert self.directory is not None
        inputs = {'images_0': self.images_0, 'images_1': self.images_1, 't': self.t}
        outputs = {'output': self.interpolated}
        if os.path.exists(self.directory) and len(os.listdir(self.directory)) > 0:
            print('Removing previous SavedModel from:', self.directory)
            shutil.rmtree(self.directory)
        tf.saved_model.simple_save(session, self.directory, inputs, outputs)
        print('SavedModel saved at:', self.directory)

    def _get_tensors_for_saved_model(self, model):
        """
        Runs _get_forward again internally to create a graph that's use-able for SavedModel API.
        :param model: An instance of class Interp.
        :return: images_0: Tensor tf.float32 placeholder of shape [None, None, None, 3].
                 images_1: Tensor tf.float32 placeholder of shape [None, None, None, 3].
                 t: Tensor tf.float32 placeholder of shape [1].
                 interpolated: Output interpolation. Tensor tf.float32 placeholder of shape [None, None, None, 3].
        """
        assert isinstance(model, Interp)
        with tf.variable_scope(model.enclosing_scope):
            images_0 = tf.placeholder(tf.float32, shape=[None, None, None, 3])
            images_1 = tf.placeholder(tf.float32, shape=[None, None, None, 3])
            t = tf.placeholder(tf.float32, shape=[1])
            save_outputs = model.get_forward(images_0, images_1, t, reuse_variables=tf.AUTO_REUSE)
            interpolated = save_outputs[0]
        return images_0, images_1, t, interpolated
