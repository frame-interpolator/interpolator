import numpy as np
import tensorflow as tf
import unittest
import os
import shutil
from common.utils.data import silently_remove_directory
from interp.interp import Interp
from interp.interp_saved_model.interp_saved_model import InterpSavedModel
from interp.interp_saved_model.synth_saved_model import SynthSavedModel
from data.interp.interp_data import InterpDataSet, InterpAugmentations
from data.interp.interp_data_preprocessor import InterpDataPreprocessor


# Fake interpolation class for testing purposes.
class SimpleInterp(Interp):
    def __init__(self):
        super().__init__('simple')

    def _get_forward(self, images_0, images_1, t,
                     flows_0_1=None,
                     flows_1_0=None,
                     flows_0_t=None,
                     flows_1_t=None,
                     reuse_variables=tf.AUTO_REUSE):
        with tf.variable_scope(self.name, reuse=tf.AUTO_REUSE):
            a = tf.layers.conv2d(images_0, 3, (1, 1))
            sum = images_0 * (1.0 - t) + images_1 * t + 0.001 * a
            if flows_0_1 is not None and flows_1_0 is not None:
                sum += tf.reduce_mean(flows_0_1) + tf.reduce_mean(flows_1_0)
            if flows_0_t is not None and flows_1_t is not None:
                sum += -0.2 * tf.reduce_mean(flows_0_t) + 0.1 * tf.reduce_mean(flows_1_t)
        return sum, None


class TestInterpSavedModel(unittest.TestCase):
    def setUp(self):
        tf.reset_default_graph()
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        # Define paths.
        cur_dir = os.path.dirname(__file__)
        self.saved_model_directory_0 = os.path.join(cur_dir, 'saved_model_0')
        self.saved_model_directory_1 = os.path.join(cur_dir, 'saved_model_1')
        self.data_directory = os.path.join(cur_dir, 'test_data')
        self.tf_record_directory = os.path.join(self.data_directory, 'tfrecords')

    def test_saved_model(self):
        # Create dataset.
        augmentations = InterpAugmentations(crop_size=(256, 256))
        data_set = InterpDataSet(self.tf_record_directory, [[1]], batch_size=1, augmentations=augmentations)
        preprocessor = InterpDataPreprocessor(self.tf_record_directory, [[1]], shard_size=1)
        preprocessor.preprocess_raw(self.data_directory)

        output_paths = data_set.get_tf_record_names()
        [self.assertTrue(os.path.isfile(output_path)) for output_path in output_paths]
        self.assertEqual(1, len(output_paths))
        data_set.load(self.sess)
        next_sequence_tensor, next_sequence_timing_tensor = data_set.get_next_batch()

        interp_t = 0.7
        model = SimpleInterp()

        with tf.variable_scope('foo'):
            interpolated_tensor, _ = model.get_forward(next_sequence_tensor[:, 0, ...],
                                                       next_sequence_tensor[:, 2, ...],
                                                       interp_t)

        # Check variables.
        self.sess.run(tf.global_variables_initializer())
        self.assertEqual(2, len(tf.trainable_variables()))
        values = self.sess.run(tf.trainable_variables())
        sums = np.array([np.sum(value) for value in values])
        self.assertNotAlmostEqual(0.0, np.sum(sums))

        interpolated, next_sequence = self.sess.run([interpolated_tensor, next_sequence_tensor],
                                                    feed_dict=data_set.get_train_feed_dict())

        self.assertTupleEqual((1, 256, 256, 3), np.shape(interpolated))
        self.assertTupleEqual((1, 3, 256, 256, 3), np.shape(next_sequence))
        self.assertNotAlmostEqual(0.0, np.sum(interpolated))

        # Save to SavedModel.
        interp_saved_model = InterpSavedModel(self.saved_model_directory_0)
        synth_saved_model = SynthSavedModel(self.saved_model_directory_1)

        with tf.variable_scope('bar'):
            interp_saved_model.freeze(model, self.sess)

        with tf.variable_scope('gre'):
            synth_saved_model.freeze(model, self.sess)

        interpolated_from_saved_model_0 = interp_saved_model.run((next_sequence[:, 0, ...],
                                                                  next_sequence[:, 2, ...],
                                                                  np.ones([len(next_sequence)]) * interp_t))

        interpolated_from_saved_model_1 = synth_saved_model.run((next_sequence[:, 0, ...],
                                                                 next_sequence[:, 2, ...],
                                                                 np.ones((1, 256, 256, 2)),
                                                                 np.ones((1, 256, 256, 2)),
                                                                 np.ones((1, 256, 256, 2)),
                                                                 np.ones((1, 256, 256, 2)),
                                                                 np.ones([len(next_sequence)]) * interp_t))

        # The dummy flows were added directly to the output.
        interpolated_from_saved_model_1 -= 2.0
        interpolated_from_saved_model_1 -= -0.1

        # Check that inference results are the same as during training.
        self.assertTupleEqual((1, 256, 256, 3), np.shape(interpolated_from_saved_model_0))
        self.assertEqual(interpolated.tolist(), interpolated_from_saved_model_0.tolist())
        self.assertTupleEqual((1, 256, 256, 3), np.shape(interpolated_from_saved_model_1))
        self.assertLessEqual(np.max(np.abs(interpolated_from_saved_model_1 - interpolated)), 1E-3)
        
    def tearDown(self):
        dirs_to_remove = [self.tf_record_directory, self.saved_model_directory_0, self.saved_model_directory_1]
        for directory in dirs_to_remove:
            silently_remove_directory(directory)


if __name__ == '__main__':
    unittest.main()
