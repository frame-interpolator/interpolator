import tensorflow as tf
import os
import shutil
from common.models import FrozenModel
from interp.interp import Interp


class SynthSavedModel(FrozenModel):
    """
    Similar to InterpSavedModel, but here we provide the flows as input as well.
    """
    def __init__(self, directory='saved_model'):
        """
        :param directory: Str. Path to the SavedModel export directory.
        """
        super().__init__(directory)
        self.images_0 = None
        self.images_1 = None
        self.flows_0_1 = None
        self.flows_1_0 = None
        self.flows_0_t = None
        self.flows_1_t = None
        self.t = None
        self.interpolated = None
        self.graph_created = False
        self.do_feed_flows_0_t = True

        # Imports the ops required for Synthesis.
        from pwcnet import model
        from common.forward_warp.forward_warp_interp import forward_warp_interp

    def run(self, inputs):
        """
        :param inputs: Tuple of:
                Numpy array. Image of shape [batch_size, H, W, C]. The image at t = 0.
                Numpy array. Image of shape [batch_size, H, W, C]. The image at t = 1.
                Numpy array. Flows of shape [batch_size, H, W, 2]. The flow from image 0 to 1.
                Numpy array. Flows of shape [batch_size, H, W, 2]. The flow from image 1 to 0.
                Numpy array. Flows of shape [batch_size, H, W, 2]. The flow from image 0 to t.
                Numpy array. Flows of shape [batch_size, H, W, 2]. The flow from image 1 to t.
                Float. Specifies the interpolation point (i.e 0 for images_0, 1 for images_1).
        :return: Numpy array of shape [batch_size, H, W, C]. The interpolation at time t.
        """
        assert isinstance(inputs, tuple) and len(inputs) == 7
        assert self.directory is not None
        images_0, images_1, flows_0_1, flows_1_0, flows_0_t, flows_1_t, t = inputs
        if self.predictor is None:
            self.load()
        if self.do_feed_flows_0_t:
            try:
                predictions = self.predictor({
                    'images_0': images_0,
                    'images_1': images_1,
                    'flows_0_1': flows_0_1,
                    'flows_1_0': flows_1_0,
                    'flows_0_t': flows_0_t,
                    'flows_1_t': flows_1_t,
                    't': t
                })
            except ValueError:
                # We want models saved before the interface change to still work (without the spline).
                self.do_feed_flows_0_t = False
                print('Warning: the loaded SynthSavedModel does not support inputting flows_0_t or flows_1_t. ' +
                      'All interpolation will be linear along the flows -- splined optical will not work.')
        if not self.do_feed_flows_0_t:
            predictions = self.predictor({
                'images_0': images_0,
                'images_1': images_1,
                'flows_0_1': flows_0_1,
                'flows_1_0': flows_1_0,
                't': t
            })
        return predictions['output']

    def freeze(self, model, session):
        """
        :param session: Tf Session.
        :param model: An instance of class Interp.
        """
        assert isinstance(model, Interp)
        self.create_graph(model)
        assert self.directory is not None
        inputs = {'images_0': self.images_0,
                  'images_1': self.images_1,
                  'flows_0_1': self.flows_0_1,
                  'flows_1_0': self.flows_1_0,
                  'flows_0_t': self.flows_0_t,
                  'flows_1_t': self.flows_1_t,
                  't': self.t}
        outputs = {'output': self.interpolated}
        if os.path.exists(self.directory) and len(os.listdir(self.directory)) > 0:
            print('Removing previous SavedModel from:', self.directory)
            shutil.rmtree(self.directory)
        tf.saved_model.simple_save(session, self.directory, inputs, outputs)
        print('SavedModel saved at:', self.directory)

    def create_graph(self, model):
        if not self.graph_created:
            self._set_tensors_for_saved_model(model)
            self.graph_created = True

    def _set_tensors_for_saved_model(self, model):
        """
        Runs _get_forward again internally to create a graph that's use-able for SavedModel API.
        :param model: An instance of class Interp.
        :return: images_0: Tensor tf.float32 placeholder of shape [None, None, None, 3].
                 images_1: Tensor tf.float32 placeholder of shape [None, None, None, 3].
                 flows_0_1: Tensor tf.float32 placeholder of shape [None, None, None, 2].
                 flows_1_0: Tensor tf.float32 placeholder of shape [None, None, None, 2].
                 t: Tensor tf.float32 placeholder of shape [1].
                 interpolated: Output interpolation. Tensor tf.float32 placeholder of shape [None, None, None, 3].
        """
        assert isinstance(model, Interp)
        with tf.variable_scope(model.enclosing_scope):
            self.images_0 = tf.placeholder(tf.float32, shape=[None, None, None, 3])
            self.images_1 = tf.placeholder(tf.float32, shape=[None, None, None, 3])
            self.flows_0_1 = tf.placeholder(tf.float32, shape=[None, None, None, 2])
            self.flows_1_0 = tf.placeholder(tf.float32, shape=[None, None, None, 2])
            self.flows_0_t = tf.placeholder(tf.float32, shape=[None, None, None, 2])
            self.flows_1_t = tf.placeholder(tf.float32, shape=[None, None, None, 2])
            self.t = tf.placeholder(tf.float32, shape=[1])
            save_outputs = model.get_forward(self.images_0, self.images_1, self.t,
                                             flows_0_1=self.flows_0_1,
                                             flows_1_0=self.flows_1_0,
                                             flows_0_t=self.flows_0_t,
                                             flows_1_t=self.flows_1_t,
                                             reuse_variables=tf.AUTO_REUSE)
            self.interpolated = save_outputs[0]
