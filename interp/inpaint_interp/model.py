import tensorflow as tf
from common.utils.tf import l2_diff
from common.forward_warp.forward_warp_bidirectional import forward_warp_bidirectional
from interp.interp import Interp
from interp.context_interp.gridnet.model import GridNet
from interp.inpaint_interp.unet.model import UNet
from interp.context_interp.vgg19_features.vgg19_features import Vgg19Features
from pwcnet.model import PWCNet
from interp.inpaint_interp.context_model import ContextExtractor, ContextTypes


class InpaintInterp(Interp):
    def __init__(self, pwcnet, name='inpaint_interp',
                 stop_gradient_to_pwcnet=True,
                 context_type=ContextTypes.SCRATCH_7,
                 use_time_channel=False,
                 use_confidence=True,
                 loss_weights=None,
                 use_gridnet=False):
        """
        :param name: Str. For Tf variable scoping.
        :param use_instance_norm: Bool. Whether to use instance normalization on the input images and contexts.
        :param stop_gradient_to_pwcnet: Bool. Whether to stop synthesis gradients from flowing back to PWCNet.
                                        If True, use_differentiable_warp should be True as well.
        :param use_time_channel: Bool. Whether to feed time information as a single channel.
        :param use_confidence: Bool. Whether to feed in warp confidence (otherwise there would be no "inpainting").
        :param use_gridnet: Bool. Whether to use a GridNet. If False, we will use a faster UNet instead.
        """
        super().__init__(name)
        assert isinstance(pwcnet, PWCNet)
        self.use_gridnet = use_gridnet
        self.stop_gradient_to_pwcnet = stop_gradient_to_pwcnet
        self.use_time_channel = use_time_channel
        self.use_confidence_mask = use_confidence
        self.loss_weights = loss_weights
        if self.loss_weights is None:
            self.loss_weights = {
                'colour': 1,
                'perceptual': 0.005,
                'style': 0
            }

        self.unet = UNet([64, 96, 128, 256], num_output_channels=3)
        self.gridnet = GridNet([32, 64, 96], width=6, num_output_channels=3)
        self.pwcnet = pwcnet
        self.context_type = ContextTypes(context_type)

        # Load feature extractor (needed for perceptual loss and potentially the context features).
        self.feature_extractor = Vgg19Features()
        self.feature_extractor.load_pretrained_weights()

        # Resolve the context extractor that we will use.
        if self.context_type == ContextTypes.NONE:
            self.context_extractor = None
        elif self.context_type == ContextTypes.PRE_TRAINED:
            self.context_extractor = self.feature_extractor
        else:
            self.context_extractor = ContextExtractor(self.context_type)

    def _get_forward(self, images_0, images_1, t, flows_0_1=None, flows_1_0=None,
                     flows_0_t=None, flows_1_t=None,
                     reuse_variables=tf.AUTO_REUSE):
        """
        Overriden.
        :return: interpolated: The interpolated image. Tensor of shape [batch_size, H, W, 3].
                 auxiliary_tensors: Dictionary.
                     warped_0_1: Image and features from image_0 forward-flowed towards image_b, before synthesis.
                                 The first 3 channels are the image.
                     warped_1_0: Image and features from image_b forward-flowed towards image_a, before synthesis.
                                 The first 3 channels are the image.
                     flow_0_1: Flow from images 0 to 1 (centered at images 0).
                     flow_1_0: Flow from images 1 to 0 (centered at images 1).
                     warped_0_1_interp: Image warped with flows and using Middlebury warp.
                     warped_1_0_interp: Image warped with flows and using Middlebury warp.
        """
        with tf.variable_scope(self.name, reuse=reuse_variables):
            batch_size, height, width, _ = tf.unstack(tf.shape(images_0))
            if flows_0_1 is None or flows_1_0 is None:
                # Get flows from PWCNet.
                flows_0_1, flows_1_0, _, _, _, _ = self.pwcnet.get_bidirectional(images_0, images_1)
                if self.stop_gradient_to_pwcnet:
                    flows_0_1 = tf.stop_gradient(flows_0_1)
                    flows_1_0 = tf.stop_gradient(flows_1_0)

            # Get features.
            dont_train_ctx = self.context_type == ContextTypes.NONE or self.context_type == ContextTypes.PRE_TRAINED
            with tf.name_scope('context_extraction'):
                images = tf.concat([images_0, images_1], axis=0)
                if self.context_type == ContextTypes.NONE:
                    features = images
                elif self.context_type == ContextTypes.PRE_TRAINED:
                    features = self.feature_extractor.get_context_features(images)
                    features = tf.concat([images, features], axis=-1)
                else:
                    features = self.context_extractor.get_forward(images)
                    features = tf.concat([images, features], axis=-1)
                if dont_train_ctx:
                    features = tf.stop_gradient(features)
                features_0, features_1 = features[:batch_size], features[batch_size:]

            # Warp the features.
            with tf.name_scope('get_warp_and_confidence'):
                warp_outputs = forward_warp_bidirectional(features_0, features_1, flows_0_1, flows_1_0, t,
                                                          flow_0_t=flows_0_t,
                                                          flow_1_t=flows_1_t,
                                                          fill_holes=False,
                                                          num_blending_channels=3)
                warped_0_1, warped_1_0, others_dict = warp_outputs
                warped_images_0_1 = warped_0_1[..., :3]
                warped_images_1_0 = warped_1_0[..., :3]
                holes_0_1 = others_dict['holes_0_1']
                holes_1_0 = others_dict['holes_1_0']
                c_0_1 = others_dict['warp_confidence_0_1']
                c_1_0 = others_dict['warp_confidence_1_0']
                c_sum = c_0_1 + c_1_0
                initial = (c_0_1 * warped_images_0_1 + c_1_0 * warped_images_1_0) / (c_sum + 1E-10)
                confidence = tf.clip_by_value(c_0_1 + c_1_0, 0.0, 1.0)

            # Maybe add confidence channel and initial warp prediction.
            if self.use_confidence_mask:
                all_inputs_list = [warped_0_1, warped_1_0, initial, confidence, holes_0_1, holes_1_0]
            else:
                all_inputs_list = [warped_0_1, warped_1_0, holes_0_1, holes_1_0]

            # Maybe add time channel.
            if self.use_time_channel:
                with tf.name_scope('time_channel'):
                    t_channel = tf.ones((batch_size, height, width, 1)) * tf.reshape(t, [-1, 1, 1, 1])
                    all_inputs_list += [t_channel]

            # Output of the UNet is the synthesized image.
            all_inputs = tf.concat(all_inputs_list, axis=-1)

            # Cut gradients if neither PWCNet nor context are being trained.
            if dont_train_ctx and self.stop_gradient_to_pwcnet:
                all_inputs = tf.stop_gradient(all_inputs)

            if self.use_gridnet:
                synthesized, _, _, _ = self.gridnet.get_forward([all_inputs])
            else:
                synthesized, _ = self.unet.get_forward(all_inputs)
            auxiliary_tensors = {
                'initial': initial,
                'warped_0_1': warped_images_0_1,
                'warped_1_0': warped_images_1_0,
                'flow_0_1': flows_0_1,
                'flow_1_0': flows_1_0,
                'confidence': confidence,
                'holes_0_1': holes_0_1,
                'holes_1_0': holes_1_0
            }
            return synthesized, auxiliary_tensors

    def load_pwcnet_weights(self, pwcnet_weights_path, sess):
        """
        Loads pre-trained PWCNet weights.
        For this to work:
            - It must be called after get_forward.
            - The pwcnet weights must have been saved under variable scope 'pwcnet'.
        :param pwcnet_weights_path: The full path to the PWCNet weights that will be loaded via
                                    the RestorableNetwork interface.
        :param sess: Tf Session.
        """
        assert self.enclosing_scope is not None, 'get_forward must have been called beforehand.'
        scope_prefix = self.enclosing_scope.name + self.name
        self.pwcnet.restore_from(pwcnet_weights_path, sess, scope_prefix=scope_prefix)

    def get_training_loss(self, prediction, expected):
        """
        :param prediction: Tensor of shape [batch, H, W, num_features]. Predicted image.
        :param expected: Tensor of shape [batch, H, W, num_features]. Ground truth image.
        :return: Tf scalar loss term.
        """
        feature_loss = 0.0
        color_loss = 0.0
        style_loss = 0.0
        if self.loss_weights['perceptual'] > 0:
            feature_loss = self.loss_weights['perceptual'] * self._get_feature_loss(prediction, expected)
        if self.loss_weights['colour'] > 0:
            color_loss = self.loss_weights['colour'] * self._get_l1_loss(prediction, expected)
        if self.loss_weights['style'] > 0:
            style_loss = self.loss_weights['style'] * self._get_style_loss(prediction, expected)
        return feature_loss + color_loss + style_loss

    def _get_l1_loss(self, prediction, expected):
        batch_size = tf.shape(prediction)[0]
        return tf.reduce_sum(tf.abs(prediction - expected)) / tf.cast(batch_size, tf.float32)

    def _get_style_loss(self, prediction, expected):
        with tf.variable_scope('style_loss'):
            a = self.feature_extractor.get_deep_context_features(prediction)
            b = self.feature_extractor.get_deep_context_features(expected)
            prediction_features = a.conv2_2
            expected_features = b.conv2_2

            # http://www.chioka.in/tensorflow-implementation-neural-algorithm-of-artistic-style
            def _gram_matrix(F, B, N, M):
                """
                The gram matrix G.
                """
                Ft = tf.reshape(F, (B, M, N))
                return tf.matmul(tf.transpose(Ft, perm=[0, 2, 1]), Ft)

            def _style_loss(a, x):
                """
                The style loss calculation.
                """
                B = tf.shape(a)[0]
                # N is the number of filters (at layer l).
                N = tf.shape(a)[3]
                # M is the height times the width of the feature map (at layer l).
                M = tf.shape(a)[1] * tf.shape(a)[2]
                # A is the style representation of the original image (at layer l).
                A = _gram_matrix(a, B, N, M)
                # G is the style representation of the generated image (at layer l).
                G = _gram_matrix(x, B, N, M)
                M = tf.cast(M, tf.float32)
                N = tf.cast(N, tf.float32)
                div = N * N * M * M
                result = (1.0 / (4.0 * div)) * tf.reduce_sum(tf.squared_difference(G, A))
                return result / tf.cast(B, tf.float32)

            return _style_loss(prediction_features, expected_features)

    def _get_feature_loss(self, prediction, expected):
        with tf.variable_scope('feature_loss'):
            batch_size = tf.shape(prediction)[0]
            prediction_features = self.feature_extractor.get_perceptual_features(prediction)
            expected_features = self.feature_extractor.get_perceptual_features(expected)
            dist = l2_diff(prediction_features, expected_features)
            return tf.reduce_sum(dist) / tf.cast(batch_size, tf.float32)



