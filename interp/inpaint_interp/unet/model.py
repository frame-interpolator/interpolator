import tensorflow as tf


class UNet:
    def __init__(self, channel_sizes, num_output_channels, name='unet', activation_fn=tf.nn.leaky_relu):
        """
        :param name: Str. For variable scoping.
        :param channel_sizes: List of channel sizes for rows. Height of the UNet = len(channel_sizes).
                              Should be from highest (2x downsampled) to lowest resolution.
        :param num_output_channels: The number of output channels.
        :param activation_fn: Tensorflow activation function.
        """
        self.activation_fn = activation_fn
        self.name = name
        self.channel_sizes = channel_sizes
        self.num_output_channels = num_output_channels

    def get_forward(self, images):
        """
        :param images: Tensor of shape [B, H, W, C].
        :return: output: Tensor of shape [B, H, W, self.num_output_channels].
                 others_dict: A dictionary of other intermediate tensors.
                    encoder_features: A list of features output from encoder. From highest to lowest resolution.
                    decoder_features: A list of features output from decoder. From highest to lowest resolution.
        """
        depth = len(self.channel_sizes)
        encoder_features = depth * [None]
        decoder_features = depth * [None]
        activation = self.activation_fn

        # Build the network forward pass.
        with tf.variable_scope(self.name):
            with tf.variable_scope('encoder'):
                for i in range(depth):
                    input_features = images if i == 0 else encoder_features[i - 1]
                    encoder_features[i] = tf.layers.conv2d(input_features,
                                                           self.channel_sizes[i], (3, 3), (2, 2),
                                                           padding='same',
                                                           activation=activation)

            # Note that decoder_features[i] is twice the resolution of encoder_features[i].
            with tf.variable_scope('decoder'):
                for i in range(depth - 1, -1, -1):
                    prev_decoder_features = decoder_features[i + 1] if i != depth - 1 else encoder_features[-1]
                    B, H, W, C = tf.unstack(tf.shape(prev_decoder_features))
                    prev_decoder_features = tf.image.resize_nearest_neighbor(prev_decoder_features, (2 * H, 2 * W))
                    if i == 0:
                        # Concatenate input image.
                        input_features = tf.concat([prev_decoder_features, images], axis=-1)
                    else:
                        input_features = tf.concat([prev_decoder_features, encoder_features[i - 1]], axis=-1)
                    decoder_features[i] = tf.layers.conv2d(input_features,
                                                           self.channel_sizes[i], (3, 3), (1, 1),
                                                           padding='same',
                                                           activation=activation)

            # Final output.
            output = tf.layers.conv2d(decoder_features[0],
                                      self.num_output_channels, (3, 3), (1, 1),
                                      padding='same',
                                      activation=activation)
        others_dict = {
            'encoder_features': encoder_features,
            'decoder_features': decoder_features
        }
        return output, others_dict




