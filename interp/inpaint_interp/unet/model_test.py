import numpy as np
import tensorflow as tf
import unittest
from interp.inpaint_interp.unet.model import UNet


class TestUNet(unittest.TestCase):

    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_network(self):
        batch_size = 4
        height = 64
        width = 64
        num_input_channels = 16
        num_output_channels = 3
        channel_sizes = [32, 64, 96]
        unet = UNet(channel_sizes,
                    num_output_channels,
                    name='unet')

        # Create the graph.
        input_features_tensor = tf.placeholder(shape=[None, height, width, num_input_channels], dtype=tf.float32)
        output_tensor, others_dict = unet.get_forward(input_features_tensor)
        encoder_features_tensors = others_dict['encoder_features']
        decoder_features_tensors = others_dict['decoder_features']

        # Note that the first parametric ReLU's gradient for alpha will be 0 if inputs are all non-negative.
        input_features = np.zeros(shape=[batch_size, height, width, num_input_channels], dtype=np.float32)
        input_features[:, 2:height-2, 2:width-2, :] = -1.0
        input_features[:, 4:height-4, 5:width-5, :] = 1.0

        self.sess.run(tf.global_variables_initializer())

        output = self.sess.run(output_tensor, feed_dict={input_features_tensor: input_features})
        encoder_features = self.sess.run(encoder_features_tensors, feed_dict={input_features_tensor: input_features})
        decoder_features = self.sess.run(decoder_features_tensors, feed_dict={input_features_tensor: input_features})

        self.assertTupleEqual(np.shape(output), (batch_size, height, width, num_output_channels))
        self.assertNotAlmostEqual(np.sum(np.abs(output)), 0.0)
        self.assertEqual(len(encoder_features), len(channel_sizes))
        self.assertEqual(len(decoder_features), len(channel_sizes))

        # Test encoder feature shapes.
        for i in range(len(channel_sizes)):
            expected_height = height / 2 ** (i + 1)
            expected_width = width / 2 ** (i + 1)
            expected_shape = (batch_size, expected_height, expected_width, channel_sizes[i])
            self.assertTupleEqual(np.shape(encoder_features[i]), expected_shape)

        # Test decoder feature shapes.
        for i in range(len(channel_sizes)):
            expected_height = height / 2 ** i
            expected_width = width / 2 ** i
            expected_shape = (batch_size, expected_height, expected_width, channel_sizes[i])
            self.assertTupleEqual(np.shape(decoder_features[i]), expected_shape)

        # Check that the gradients are flowing.
        trainable_vars = tf.trainable_variables(scope='unet')
        grad_op = tf.gradients(output_tensor,
                               trainable_vars + [input_features_tensor])
        gradients = self.sess.run(grad_op, feed_dict={input_features_tensor: input_features})
        for gradient in gradients:
            self.assertNotEqual(np.sum(gradient), 0.0)


if __name__ == '__main__':
    unittest.main()
