import tensorflow as tf
from common.models import ConvNetwork
from enum import Enum


class ContextTypes(Enum):
    NONE = 0
    PRE_TRAINED = 1  # VGG19 Features.
    SCRATCH_3 = 2  # 3x3 filters.
    SCRATCH_7 = 3  # Uses a 7x7 filter at the beginning.
    SCRATCH_SHALLOW = 4  # Copies the structure of our pre-trained feature extractor (first 2 convs of VGG19).


class ContextExtractor(ConvNetwork):
    def __init__(self, context_type, name='context_extractor'):
        self.name = name
        self.context_type = ContextTypes(context_type)
        assert self.context_type != ContextTypes.NONE
        assert self.context_type != ContextTypes.PRE_TRAINED

        first_kernel_size = 7 if self.context_type == ContextTypes.SCRATCH_7 else 3

        if self.context_type == ContextTypes.SCRATCH_SHALLOW:
            layer_specs = [[3, 64, 1, 1],
                           [3, 64, 1, 1]]
        else:
            layer_specs = [[first_kernel_size, 32, 1, 1],
                           [3, 32, 1, 1],
                           [3, 32, 1, 1],
                           [3, 32, 1, 1],
                           [3, 32, 1, 1]]

        super().__init__(name=name, layer_specs=layer_specs,
                         activation_fn=tf.nn.leaky_relu, last_activation_fn=tf.nn.relu,
                         regularizer=None, padding='SAME', dense_net=False)

    def get_forward(self, images):
        """
        :param images: Tensor of shape [B, H, W, 3].
        :return: extracted contexts of shape [B, H, W, 16].
        """
        contexts, _, _ = self.get_forward_conv(images)
        return contexts
