import numpy as np
import tensorflow as tf
from interp.inpaint_interp.model import InpaintInterp
from common.utils.profile import run_profiler

if __name__ == '__main__':
    height = 256
    width = 256
    im_channels = 3
    batch_size = 8

    # Create the graph.
    image_shape = [batch_size, height, width, im_channels]
    image_0_placeholder = tf.placeholder(shape=image_shape, dtype=tf.float32)
    image_1_placeholder = tf.placeholder(shape=image_shape, dtype=tf.float32)
    model = InpaintInterp(use_differentiable_warp=True)
    ts = tf.constant(batch_size * [0.5])
    synthesized, _ = model.get_forward(image_0_placeholder, image_1_placeholder, ts)
    grads = tf.gradients(synthesized, [image_0_placeholder, image_1_placeholder])

    # Create dummy images.
    image = np.zeros(shape=[batch_size, height, width, im_channels], dtype=np.float32)
    image[:, 2:height - 2, 2:width - 2, :] = 1.0

    query = [synthesized, grads]
    feed_dict = {image_0_placeholder: image,
                 image_1_placeholder: image}
    run_profiler(query, feed_dict, name='inpaint-interp')
