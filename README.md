# Interpolator

Check out the [wiki](https://gitlab.com/frame-interpolator/interpolator/wikis/home) to get started.

## PWC Net

In the below images from our validation set, flow_<0-4> are the intermediate estimator outputs, flow_5 is after the context network, final_flow is the upscaled version of flow_5, and gt_flow is the ground truth.

[![alt text is for people with bad internet](https://gitlab.com/frame-interpolator/interpolator/raw/master/docs/image1.png)](https://gitlab.com/frame-interpolator/interpolator/blob/master/docs/image1.png)

[![alt text is for people with bad internet](https://gitlab.com/frame-interpolator/interpolator/raw/master/docs/image2.png)](https://gitlab.com/frame-interpolator/interpolator/blob/master/docs/image2.png)

[![alt text is for people with bad internet](https://gitlab.com/frame-interpolator/interpolator/raw/master/docs/image8.png)](https://gitlab.com/frame-interpolator/interpolator/blob/master/docs/image8.png)

[![alt text is for people with bad internet](https://gitlab.com/frame-interpolator/interpolator/raw/master/docs/image13.png)](https://gitlab.com/frame-interpolator/interpolator/blob/master/docs/image13.png)
