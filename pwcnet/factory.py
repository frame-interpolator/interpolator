from argparse import ArgumentParser
from common.utils.config import preprocess_var_refs, import_json
from pwcnet.hinted_model import HintedPWCNet
from pwcnet.model import PWCNet
from tensorflow.contrib.layers import l2_regularizer


def create_pwcnet(config):
    """
    Creates a pwcnet given a specific configuration.
    If the config is a string, then it is assumed to be the path to a json file.
    :param config: Dictionary for configuration. Optional keys are:
        regularization_weight: Float.
        hinted: Bool.
        bilateral_range_stddev: Float.
        use_flcon: Bool.
        norm: Str. 'batch_norm' or 'group_norm'.
        residual_estimators: Bool.
    :return: PWCNet object.
        Minimal config dictionary containing only the necessary values to construct the model.
    """
    if isinstance(config, str):
        config = import_json(config)
        preprocess_var_refs(config)
    else:
        assert isinstance(config, dict)
    # Get values or defaults.
    regularizer = None if 'regularization_weight' not in config else l2_regularizer(config['regularization_weight'])
    bilateral_range_stddev = config.get('bilateral_range_stddev', -1.0)
    residual_estimators = config.get('residual_estimators', False)
    use_flcon = config.get('use_flcon', False)
    norm = config.get('norm', None)
    deeper_feature_pyramid = config.get('deeper_feature_pyramid', True)
    upres = config.get('upres', False)
    hinted = config.get('hinted', False)
    bilateral_hint_splat_range = config.get('bilateral_hint_splat_range', 0.5)
    residual_hints = config.get('residual_hints', True)
    # Create the minimal config reflecting the values used.
    min_config = {
        'bilateral_range_stddev': bilateral_range_stddev,
        'residual_estimators': residual_estimators,
        'use_flcon': use_flcon,
        'norm': norm,
        'deeper_feature_pyramid': deeper_feature_pyramid,
        'hinted': hinted,
        'bilateral_hint_splat_range': bilateral_hint_splat_range,
        'residual_hints': residual_hints,
        'upres': upres
    }
    if 'regularization_weight' in config:
        min_config['regularization_weight'] = config['regularization_weight']
    # Create model.
    if hinted:
        model = HintedPWCNet(regularizer=regularizer, bilateral_range_stddev=bilateral_range_stddev,
                             use_flcon=use_flcon, residual=residual_estimators, norm=norm,
                             deeper_feature_pyramid=deeper_feature_pyramid, upres=upres,
                             bilateral_hint_splat_range=bilateral_hint_splat_range, residual_hints=residual_hints)
    else:
        model = PWCNet(regularizer=regularizer, bilateral_range_stddev=bilateral_range_stddev,
                       use_flcon=use_flcon, residual=residual_estimators, norm=norm,
                       deeper_feature_pyramid=deeper_feature_pyramid, upres=upres)
    return model, min_config


def add_pwcnet_config_to_argparser(parser):
    """
    Adds all PWCNet configurations to an argparser.
    :param parser: ArgumentParser object.
    :return: Nothing.
    """
    assert isinstance(parser, ArgumentParser)
    parser.add_argument('-brs', '--bilateral_range_stddev', type=float, default=-1.0,
                        help='Standard deviation over which the bilateral filter should average.')
    parser.add_argument('-flc', '--use_flcon', type=bool, default=False,
                        help='Whether to use an flcon at the output layer.')
    parser.add_argument('-re', '--residual_estimators', type=bool, default=False,
                        help='Whether to use residual estimator connections.')
    parser.add_argument('-nm', '--norm', type=str, default=None,
                        help='Normalization on the feature pyramid. "batch_norm" or "group_norm".')
    parser.add_argument('-df', '--deeper_feature_pyramid', type=bool, default=True,
                        help='Whether to use a deeper feature pyramid architecture.')
    parser.add_argument('-ur', '--upres', type=bool, default=False,
                        help='Whether to use an up-res network instead of a context network.')
    parser.add_argument('-hi', '--hinted', type=bool, default=False,
                        help='Whether to use a hinted PWC Net architecture.')
    parser.add_argument('-bhsr', '--bilateral_hint_splat_range', type=float, default=0.5,
                        help='If negative, hints will not be splatted.  Otherwise, this value determines how much to ' +
                             'splat the hint over a flow edge. Use a large value (i.e. 1e3) to ignore edges.')
    parser.add_argument('-rh', '--residual_hints', type=bool, default=True,
                        help='Whether to feed in residual hints or the "ground truth" hint directly.')


def add_pwcnet_config_args_to_dict(args, config):
    """
    Adds all PWCNet configurations to a config dictionary from parsed argparse args.
    :param args: Argparse args.
    :param config: Dict.
    :return: Nothing.
    """
    config['bilateral_range_stddev'] = args.bilateral_range_stddev
    config['use_flcon'] = args.use_flcon
    config['residual_estimators'] = args.residual_estimators
    config['norm'] = args.norm
    config['deeper_feature_pyramid'] = args.deeper_feature_pyramid
    config['upres'] = args.upres
    config['hinted'] = args.hinted
    config['bilateral_hint_splat_range'] = args.bilateral_hint_splat_range
    config['residual_hints'] = args.residual_hints
