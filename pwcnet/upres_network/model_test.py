import numpy as np
import tensorflow as tf
import unittest
from pwcnet.upres_network.model import UpResNetwork
from tensorflow.contrib.layers import l2_regularizer


class TestUpResNetwork(unittest.TestCase):
    def setUp(self):
        self.upres_network = UpResNetwork(regularizer=l2_regularizer(1e-4))

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_network(self):
        """
        Sets up the network's forward pass and ensures that all shapes are expected.
        """
        height = 4
        width = 4
        num_features = 8
        batch_size = 2

        # Create the graph.
        input_flow_tensor = tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32)
        input_quarter_features_tensor = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        input_half_features_tensor = tf.placeholder(shape=[None, height * 2, width * 2, num_features], dtype=tf.float32)
        final_flow_tensor = self.upres_network.get_forward(input_flow_tensor, input_quarter_features_tensor,
                                                    input_half_features_tensor)

        input_flow = np.ones((batch_size, height, width, 2), dtype=np.float32)
        input_quarter_features = np.ones((batch_size, height, width, num_features), dtype=np.float32)
        input_half_features = np.ones((batch_size, height * 2, width * 2, num_features), dtype=np.float32)

        self.sess.run(tf.global_variables_initializer())
        final_flow = self.sess.run(final_flow_tensor, feed_dict={input_flow_tensor: input_flow,
                                                                 input_quarter_features_tensor: input_quarter_features,
                                                                 input_half_features_tensor: input_half_features})

        self.assertTupleEqual((batch_size, height * 4, width * 4, 2), final_flow.shape)


if __name__ == '__main__':
    unittest.main()
