import tensorflow as tf
from common.models import ConvNetwork, RestorableNetwork
from common.utils.tf import leaky_relu


class UpResNetwork(RestorableNetwork):
    def __init__(self, name='upres_network', activation_fn=leaky_relu, regularizer=None, kernel_initializer=None):
        """
        Network for increasing the resolution of a quarter-resolution optical flow to full resolution.
        :param name: Str. For variable scoping.
        :param layer_specs: See parent class.
        :param activation_fn: Tensorflow activation function.
        :param regularizer: Tf regularizer such as tf.contrib.layers.l2_regularizer.
        :param kernel_initializer: Tensorflow kernel initializer.
        """
        super().__init__(name=name)
        layer_specs_1 = [[3, 128, 1, 1],
                         [3, 128, 2, 1],
                         [3, 128, 4, 1],
                         [3, 96, 8, 1],
                         [3, 64, 16, 1],
                         [4, 32, 1, 0.5]]  # Upsample.
        layer_specs_2 = [[3, 48, 1, 1],
                         [4, 32, 1, 0.5]]  # Upsample.
        layer_specs_3 = [[3, 32, 1, 1],
                         [3, 16, 1, 1],
                         [3, 2, 1, 1]]  # last_activation_fn is linear.
        self.upsample_1 = ConvNetwork(name='upsample_1', layer_specs=layer_specs_1,
                                      activation_fn=activation_fn, regularizer=regularizer, padding='SAME',
                                      dense_net=False, kernel_initializer=kernel_initializer)
        self.upsample_2 = ConvNetwork(name='upsample_2', layer_specs=layer_specs_2,
                                      activation_fn=activation_fn, regularizer=regularizer, padding='SAME',
                                      dense_net=False, kernel_initializer=kernel_initializer)
        self.process = ConvNetwork(name='process', layer_specs=layer_specs_3,
                                   activation_fn=activation_fn, last_activation_fn=None,
                                   regularizer=regularizer, padding='SAME', dense_net=False,
                                   kernel_initializer=kernel_initializer)

    def get_forward(self, quarter_flow, quarter_features, half_features, reuse_variables=tf.AUTO_REUSE, training=False):
        """
        :param quarter_flow: Tensor. Optical flow of shape [batch_size, H / 4, W / 4, 2].
        :param quarter_features: Tensor. Feature map of shape [batch_size, H / 4, W / 4, num_features].
        :param half_features: Tensor. Feature map of shape [batch_size, H / 2, W / 2, num_features].
        :param reuse_variables: tf reuse option. i.e. tf.AUTO_REUSE.
        :param training: Python bool or Tensorflow bool.
        :return: Tensor. Optical flow of shape [batch_size, H, W, 2].
        """
        with tf.variable_scope(self.name, reuse=reuse_variables):
            with tf.name_scope('resized_flows'):
                _, height, width, _ = tf.unstack(tf.shape(quarter_flow))
                half_flow = tf.image.resize_bilinear(quarter_flow, [height * 2, width * 2], align_corners=True,
                                                     name='half_flow')
                full_flow = tf.image.resize_bilinear(quarter_flow, [height * 4, width * 4], align_corners=True,
                                                     name='full_flow')

            input_stack_1 = tf.concat([quarter_flow, quarter_features], axis=-1)
            upsampled_1, _, _ = self.upsample_1.get_forward_conv(input_stack_1, reuse_variables=reuse_variables,
                                                                 training=training)

            input_stack_2 = tf.concat([upsampled_1, half_flow, half_features], axis=-1)
            upsampled_2, _, _ = self.upsample_2.get_forward_conv(input_stack_2, reuse_variables=reuse_variables,
                                                                 training=training)

            input_stack_3 = tf.concat([upsampled_2, full_flow], axis=-1)
            processed, _, _ = self.process.get_forward_conv(input_stack_3, reuse_variables=reuse_variables,
                                                            training=training)

            return processed + full_flow
