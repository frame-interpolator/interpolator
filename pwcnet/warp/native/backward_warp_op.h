#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"

using namespace tensorflow;

void BackwardWarp(const Eigen::GpuDevice& d,
	typename TTypes<float, 4>::ConstTensor images,
	typename TTypes<float, 4>::ConstTensor flows,
	typename TTypes<float, 4>::Tensor output);

void BackwardWarpGrad(const Eigen::GpuDevice& d,
	typename TTypes<float, 4>::ConstTensor input_grad,
	typename TTypes<float, 4>::ConstTensor input_images,
	typename TTypes<float, 4>::ConstTensor flows,
	typename TTypes<float, 4>::Tensor output_image_grad,
	typename TTypes<float, 4>::Tensor output_flow_grad);
