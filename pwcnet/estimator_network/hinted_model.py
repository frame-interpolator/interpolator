import tensorflow as tf
from common.utils.tf import leaky_relu
from common.vector_max_pool.vector_max_pool import vector_max_pool
from pwcnet.estimator_network.model import EstimatorNetwork
from pwcnet.hint_splat.hint_splat import hint_splat


class HintedEstimatorNetwork(EstimatorNetwork):
    def __init__(self, name='estimator_network', layer_specs=None,
                 activation_fn=leaky_relu,
                 regularizer=None, search_range=4, dense_net=True, cost_volume_activation=True,
                 kernel_initializer=None, level=-1, parent=None, residual=True, bilateral_hint_splat_range=0.5,
                 residual_hints=True):
        """
        :param name: Str. For variable scoping.
        :param layer_specs: See parent class.
        :param activation_fn: Tensorflow activation function.
        :param regularizer: Tf regularizer such as tf.contrib.layers.l2_regularizer.
        :param dense_net: Bool. Default for PWC-Net is true.
        :param cost_volume_activation: Bool. Whether to put an activation function on the cost volume.
        :param kernel_initializer: Tensorflow kernel initializer.
        :param level: Int. Value between 0 and 5. 5 indicates the lowest resolution. 0 is half resolution.
        :param parent: Parent HintedPWCNet.
        :param residual: Bool. Whether the network should be residual.
        :param bilateral_hint_splat_range: Float, If negative, hints will not be splatted. Otherwise, this value
            determines how much to splat the hint over a flow edge. Use a large value (i.e. 1e3) to ignore edges.
        :param residual_hints: Bool. Whether to feed in residual hints (difference between predicted and expected) or
            the "ground truth" hint directly.
        """
        assert parent is not None
        assert level >= 0
        super().__init__(name=name, layer_specs=layer_specs, activation_fn=activation_fn, regularizer=regularizer,
                         search_range=search_range, dense_net=dense_net, cost_volume_activation=cost_volume_activation,
                         kernel_initializer=kernel_initializer, residual=residual)
        self.parent = parent
        self.level = level
        self.bilateral_hint_splat_range = bilateral_hint_splat_range
        self.residual_hints = residual_hints
        if self.bilateral_hint_splat_range > 0.0:
            assert self.residual_hints, 'Cannot splat hints if hints are not residual.'

    def get_forward(self, features1, features2, optical_flow, previous_estimator_feature,
                    pre_warp_scaling=1.0, reuse_variables=tf.AUTO_REUSE, training=False):
        """
        Overloads get_forward by concatenating a hint onto the previous_estimator_features.
        :param features1: Tensor. Feature map of shape [batch_size, H, W, num_features]. Time = 0.
        :param features2: Tensor. Feature map of shape [batch_size, H, W, num_features]. Time = 1.
        :param optical_flow: Tensor or None. Optical flow of shape [batch_size, H, W, 2].
        :param previous_estimator_feature: Tensor or None. Feature map of shape [batch_size, H, W, num_features].
        :param pre_warp_scaling: Tensor or scalar. Scaling to be applied right before warping.
        :param reuse_variables: Tensorflow reuse option. i.e. tf.AUTO_REUSE.
        :param training: Python bool or Tensorflow bool.
        :return: final_flow: Tensor. Optical flow of shape [batch_size, H, W, 2].
                 layer_outputs: List of all convolution outputs of the network. The last item is the final_flow.
                 dense_outputs: List of dense outputs from the convolution tower. List is empty if network is not dense.
        """
        with tf.name_scope(self.name + '_hint'):
            assert self.parent.flow_hints_and_masks is not None
            pooled_hints = self._get_hints(optical_flow)
            if self.residual_hints:
                # The mask does not need to be fed in for a residual hint.
                pooled_hints = [pooled_hints[0]]
            if previous_estimator_feature is None:
                previous_estimator_feature = tf.concat(pooled_hints, axis=-1)
            else:
                previous_estimator_feature = tf.concat([previous_estimator_feature] + pooled_hints, axis=-1)

        return super().get_forward(features1, features2, optical_flow, previous_estimator_feature,
                                   pre_warp_scaling=pre_warp_scaling, reuse_variables=reuse_variables,
                                   training=training)

    def _get_hints(self, prev_flow):
        with tf.name_scope('get_hints'):
            hints, mask = self.parent.flow_hints_and_masks[self.level]
            if prev_flow is None:
                # Residual for the first estimator is just the hint itself.
                return [hints, mask]
            if self.residual_hints:
                with tf.name_scope('compute_residual'):
                    # Use full resolution hints to compute the residual, then max-pool the residuals.
                    hints = self.parent.flow_hint
                    _, H, W, _ = tf.unstack(tf.shape(hints))
                    resized_prev_flow = tf.image.resize_bilinear(prev_flow, (H, W), align_corners=True)
                    # Hints already have flow scaling applied to them.
                    hints = hints - (self.parent.flow_hint_mask * resized_prev_flow)
                    hints = vector_max_pool(hints, kernel_size=2 ** (self.level + 1))
            if self.bilateral_hint_splat_range > 0.0:
                kernel_size = 3 if self.level >= 4 else 5 if self.level >= 3 else 7
                spacial_std_dev = 0.5 if self.level >= 4 else 1.0 if self.level >= 3 else 2.0
                range_std_dev = self.bilateral_hint_splat_range
                splatted_residual, splatted_mask = hint_splat(hints, mask, prev_flow, kernel_size=kernel_size,
                                                              range_std_dev=range_std_dev * self.parent.flow_scaling,
                                                              spacial_std_dev=spacial_std_dev)
                return [splatted_residual, splatted_mask]
            else:
                return [hints, mask]
