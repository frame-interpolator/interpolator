# Mostly copied from https://github.com/nameless-Chatoyant/PWC-Net_pytorch/blob/master/modules.py.
# Master branch commit 2225ad2082371126cc9c8e57a8b962a88933a8c0.
import tensorflow as tf
from common.utils.tf import load_op_library
from tensorflow.python.framework import ops


# Load op library.
mod = load_op_library('correlation_op', 'build')


def cost_volume(c1, c2, search_range=4, do_normalize=False):
    """
    See https://arxiv.org/pdf/1709.02371.pdf.
    For each pixel in c1, we will compute correlations with its spatial neighbors in c2.
    :param c1: Tensor. Feature map of shape [batch_size, H, W, num_features].
    :param c2: Input tensor with the exact same shape as c1.
    :param search_range: The search square's side length is equal to 2 * search_range + 1.
    :param do_normalize: Whether to normalize the feature inner products, as in https://arxiv.org/pdf/1810.10510.pdf.
    :return: Tensor. Cost volume of shape [batch_size, H, W, s * s], where s is equal to 2 * search_range + 1.
    """
    if do_normalize:
        eps = 1E-10
        num_channels = tf.shape(c1)[-1]
        c1_norms = tf.reduce_sum(c1 * c1, axis=-1, keep_dims=True)
        c2_norms = tf.reduce_sum(c2 * c2, axis=-1, keep_dims=True)
        c1 = c1 / tf.sqrt(c1_norms + eps)
        c2 = c2 / tf.sqrt(c2_norms + eps)
    if mod is not None:
        results = mod.correlation(
            c1, c2,
            max_displacement=search_range,
            pad=search_range,
            stride_1=1,
            stride_2=1
        )
        cv = results[0]
    else:
        cv = cost_volume_tensorflow(c1, c2, search_range=search_range)

    # The not-normalized version uses a reduce mean so we want to undo that (to pass the tests).
    if do_normalize:
        cv = cv * tf.cast(num_channels, tf.float32)
    return cv


if mod is not None:
    @ops.RegisterGradient('Correlation')
    def _CorrelationGrad(op, in_grad, in_grad1, in_grad2):
        grad0, grad1 = mod.correlation_grad(
            in_grad, op.inputs[0], op.inputs[1],
            op.outputs[1], op.outputs[2],
            kernel_size=op.get_attr('kernel_size'),
            max_displacement=op.get_attr('max_displacement'),
            pad=op.get_attr('pad'),
            stride_1=op.get_attr('stride_1'),
            stride_2=op.get_attr('stride_2'))
        return [grad0, grad1]


def cost_volume_tensorflow(c1, c2, search_range=4):
    """
    Copied from: https://github.com/philferriere/tfoptflow/blob/master/tfoptflow/core_costvol.py
    Note that the leaky ReLU has been separated out from this operation.
    """
    with tf.name_scope('cost_volume'):
        padded_lvl = tf.pad(c2, [[0, 0], [search_range, search_range], [search_range, search_range], [0, 0]])
        _, h, w, _ = tf.unstack(tf.shape(c1))
        max_offset = search_range * 2 + 1

        cost_vol = []
        for y in range(0, max_offset):
            for x in range(0, max_offset):
                slice = tf.slice(padded_lvl, [0, y, x, 0], [-1, h, w, -1])
                cost = tf.reduce_mean(c1 * slice, axis=3, keepdims=True)
                cost_vol.append(cost)
        cost_vol = tf.concat(cost_vol, axis=3)
        return cost_vol
