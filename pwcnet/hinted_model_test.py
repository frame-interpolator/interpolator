import os
import numpy as np
import tensorflow as tf
import unittest
from common.utils.data import silently_remove_directory
from pwcnet.hinted_model import HintedPWCNet
from tensorflow.contrib.layers import l2_regularizer


class TestHintedPWCModel(unittest.TestCase):
    def setUp(self):
        self.pwc_net = HintedPWCNet(name='hinted_pwc_net', regularizer=l2_regularizer(1e-4))

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_hint(self):
        shape = (2, 128, 128, 2)
        mask_shape = (2, 128, 128, 1)
        flow = tf.ones(shape, dtype=tf.float32)
        mask = tf.ones(mask_shape, dtype=tf.float32)
        self.pwc_net.set_flow_hint(flow, mask)
        flow_hint = self.pwc_net.flow_hint
        self.assertTrue(np.allclose(np.ones(shape, dtype=np.float32) * self.pwc_net.flow_scaling,
                                    self.sess.run(flow_hint)))

    def test_network(self):
        """
        Sets up the network's forward pass and ensures that all shapes are expected.
        """
        height = 128
        width = 128
        num_features = 3
        batch_size = 2

        # Create the graph.
        input_image_a = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        input_image_b = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        input_flow_hint = tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32)
        mask_tensor = tf.ones([batch_size, height, width, 1], dtype=tf.float32)
        self.pwc_net.set_flow_hint(input_flow_hint, mask_tensor)
        final_flow, previous_flows, extras = self.pwc_net.get_forward(input_image_a, input_image_b)

        grad = tf.gradients(final_flow, input_image_a)[0]

        self.assertEqual(6, len(extras['features']))

        image_a = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        image_a[:, 10:height - 10, 10:width - 10, :] = 1.0
        image_b = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        image_b[:, 5:height - 5, 5:width - 5, :] = 1.0
        dummy_flow = np.ones(shape=[batch_size, height, width, 2], dtype=np.float32)

        self.sess.run(tf.global_variables_initializer())

        query = [final_flow] + previous_flows + [grad]
        results = self.sess.run(query, feed_dict={input_image_a: image_a, input_image_b: image_b,
                                                  input_flow_hint: dummy_flow})

        self.assertEqual(len(results), 8)

        # Test that the default values are working.
        self.assertTupleEqual(results[0].shape, (batch_size, height, width, 2))
        self.assertTupleEqual(results[1].shape, (batch_size, height/64, width/64, 2))
        self.assertTupleEqual(results[2].shape, (batch_size, height/32, width/32, 2))
        self.assertTupleEqual(results[3].shape, (batch_size, height/16, width/16, 2))
        self.assertTupleEqual(results[4].shape, (batch_size, height/8, width/8, 2))
        self.assertTupleEqual(results[5].shape, (batch_size, height/4, width/4, 2))
        self.assertTupleEqual(results[6].shape, (batch_size, height/4, width/4, 2))

        grad = results[7]
        self.assertGreater(float(np.sum(np.abs(grad))), 0.0)
        self.assertAlmostEqual(0.0, float(np.sum(np.isnan(grad).astype(np.float32))))


class TestHintedNonResidualPWCModel(unittest.TestCase):
    def setUp(self):
        self.pwc_net = HintedPWCNet(name='hinted_non_residual_pwc_net', regularizer=l2_regularizer(1e-4),
                                    residual_hints=False, bilateral_hint_splat_range=-1.0)

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_network(self):
        """
        Sets up the network's forward pass and ensures that all shapes are expected.
        """
        height = 128
        width = 128
        num_features = 3
        batch_size = 2

        # Create the graph.
        input_image_a = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        input_image_b = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        input_flow_hint = tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32)
        mask_tensor = tf.ones([batch_size, height, width, 1], dtype=tf.float32)
        self.pwc_net.set_flow_hint(input_flow_hint, mask_tensor)
        final_flow, previous_flows, extras = self.pwc_net.get_forward(input_image_a, input_image_b)

        self.assertEqual(6, len(extras['features']))

        image_a = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        image_a[:, 10:height - 10, 10:width - 10, :] = 1.0
        image_b = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        image_b[:, 5:height - 5, 5:width - 5, :] = 1.0
        dummy_flow = np.ones(shape=[batch_size, height, width, 2], dtype=np.float32)

        self.sess.run(tf.global_variables_initializer())

        query = [final_flow] + previous_flows
        results = self.sess.run(query, feed_dict={input_image_a: image_a, input_image_b: image_b,
                                                  input_flow_hint: dummy_flow})

        self.assertEqual(len(results), 7)

        # Test that the default values are working.
        self.assertTupleEqual(results[0].shape, (batch_size, height, width, 2))
        self.assertTupleEqual(results[1].shape, (batch_size, height/64, width/64, 2))
        self.assertTupleEqual(results[2].shape, (batch_size, height/32, width/32, 2))
        self.assertTupleEqual(results[3].shape, (batch_size, height/16, width/16, 2))
        self.assertTupleEqual(results[4].shape, (batch_size, height/8, width/8, 2))
        self.assertTupleEqual(results[5].shape, (batch_size, height/4, width/4, 2))
        self.assertTupleEqual(results[6].shape, (batch_size, height/4, width/4, 2))


class TestFrozenHintedPWCModel(unittest.TestCase):
    def setUp(self):
        self.pwc_net = None
        self.config = tf.ConfigProto()
        self.config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=self.config)
        self.test_data_dir = os.path.join('pwcnet', 'test_data')

    def tearDown(self):
        silently_remove_directory(self.test_data_dir)

    def test_freeze_and_restore(self):
        self.pwc_net = HintedPWCNet(name='frozen_hinted_pwcnet', regularizer=l2_regularizer(1e-4),
                                    bilateral_range_stddev=-1.0, use_flcon=False, search_range=1)

        height = 128
        width = 128
        num_features = 3
        batch_size = 1

        # Create the graph.
        input_image_a = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        input_image_b = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        input_flow_hint = tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32)
        input_mask_tensor = tf.ones([batch_size, height, width, 1], dtype=tf.float32)
        self.pwc_net.set_flow_hint(input_flow_hint, input_mask_tensor)
        final_flow_tensor, _, extras = self.pwc_net.get_forward(input_image_a, input_image_b)

        image_a = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        image_a[:, 10:height - 10, 10:width - 10, :] = 1.0
        image_b = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        image_b[:, 5:height - 5, 5:width - 5, :] = 1.0
        hints = np.ones(shape=[batch_size, height, width, 2], dtype=np.float32)
        hint_masks = np.ones(shape=[batch_size, height, width, 1], dtype=np.float32)

        self.sess.run(tf.global_variables_initializer())
        final_flow_1 = self.sess.run(final_flow_tensor, feed_dict={input_image_a: image_a, input_image_b: image_b,
                                                                   input_flow_hint: hints,
                                                                   input_mask_tensor: hint_masks})
        final_flow_2 = self.sess.run(final_flow_tensor, feed_dict={input_image_a: image_a, input_image_b: image_b,
                                                                   input_flow_hint: np.zeros_like(hints),
                                                                   input_mask_tensor: np.zeros_like(hint_masks)})
        features_1 = self.sess.run(extras['features'], feed_dict={input_image_a: image_a, input_image_b: image_b,
                                                                  input_flow_hint: hints,
                                                                  input_mask_tensor: hint_masks})

        frozen_model = HintedPWCNet.create_frozen_model(self.test_data_dir)
        frozen_model.freeze(self.pwc_net, self.sess)
        frozen_model.load(session_config_proto=self.config)

        final_flow_saved, frozen_extras_1 = frozen_model.run((image_a, image_b, hints, hint_masks))
        self.assertTupleEqual(final_flow_1.shape, final_flow_saved.shape)
        self.assertTrue(np.allclose(final_flow_1, final_flow_saved, rtol=1e-3, atol=1e-3))

        final_flow_saved, frozen_extras_2 = frozen_model.run((image_a, image_b))
        self.assertTupleEqual(final_flow_2.shape, final_flow_saved.shape)
        self.assertTrue(np.allclose(final_flow_2, final_flow_saved, rtol=1e-3, atol=1e-3))

        final_flow_saved, frozen_extras_2 = frozen_model.run(([image_a[0]], image_b))
        self.assertTupleEqual(final_flow_2.shape, final_flow_saved.shape)
        self.assertTrue(np.allclose(final_flow_2, final_flow_saved, rtol=1e-3, atol=1e-3))

        for i, feature in enumerate(frozen_extras_1['features']):
            original_feature = features_1[i]
            self.assertTupleEqual(original_feature.shape, feature.shape)
            self.assertTrue(np.allclose(original_feature, feature, rtol=1e-2, atol=1e-3))

    def test_freeze_and_restore_enclosed_scope(self):
        self.pwc_net = HintedPWCNet(name='frozen_hinted_pwcnet_enclosed', regularizer=l2_regularizer(1e-4),
                                    bilateral_range_stddev=-1.0, use_flcon=False, search_range=1)
        height = 128
        width = 128
        num_features = 3
        batch_size = 1

        # Create the graph.
        input_image_a = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        input_image_b = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        input_flow_hint = tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32)
        input_mask_tensor = tf.ones([batch_size, height, width, 1], dtype=tf.float32)
        self.pwc_net.set_flow_hint(input_flow_hint, input_mask_tensor)
        with tf.variable_scope('outer_scope', reuse=tf.AUTO_REUSE):
            final_flow_tensor, _, extras = self.pwc_net.get_forward(input_image_a, input_image_b)

        image_a = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        image_a[:, 10:height - 10, 10:width - 10, :] = 1.0
        image_b = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        image_b[:, 5:height - 5, 5:width - 5, :] = 1.0
        hints = np.ones(shape=[batch_size, height, width, 2], dtype=np.float32)
        hint_masks = np.ones(shape=[batch_size, height, width, 1], dtype=np.float32)

        self.sess.run(tf.global_variables_initializer())
        final_flow_1 = self.sess.run(final_flow_tensor, feed_dict={input_image_a: image_a, input_image_b: image_b,
                                                                   input_flow_hint: hints,
                                                                   input_mask_tensor: hint_masks})
        final_flow_2 = self.sess.run(final_flow_tensor, feed_dict={input_image_a: image_a, input_image_b: image_b,
                                                                   input_flow_hint: np.zeros_like(hints),
                                                                   input_mask_tensor: np.zeros_like(hint_masks)})
        features_1 = self.sess.run(extras['features'], feed_dict={input_image_a: image_a, input_image_b: image_b,
                                                                  input_flow_hint: hints,
                                                                  input_mask_tensor: hint_masks})

        frozen_model = HintedPWCNet.create_frozen_model(self.test_data_dir)
        frozen_model.freeze(self.pwc_net, self.sess)
        frozen_model.load(session_config_proto=self.config)

        final_flow_saved, frozen_extras_1 = frozen_model.run((image_a, image_b, hints, hint_masks))
        self.assertTupleEqual(final_flow_1.shape, final_flow_saved.shape)
        self.assertTrue(np.allclose(final_flow_1, final_flow_saved, rtol=1e-3, atol=1e-3))

        final_flow_saved, frozen_extras_2 = frozen_model.run((image_a, image_b))
        self.assertTupleEqual(final_flow_2.shape, final_flow_saved.shape)
        self.assertTrue(np.allclose(final_flow_2, final_flow_saved, rtol=1e-3, atol=1e-3))

        final_flow_saved, frozen_extras_2 = frozen_model.run(([image_a[0]], image_b))
        self.assertTupleEqual(final_flow_2.shape, final_flow_saved.shape)
        self.assertTrue(np.allclose(final_flow_2, final_flow_saved, rtol=1e-3, atol=1e-3))

        for i, feature in enumerate(frozen_extras_1['features']):
            original_feature = features_1[i]
            self.assertTupleEqual(original_feature.shape, feature.shape)
            self.assertTrue(np.allclose(original_feature, feature, rtol=1e-2, atol=1e-3))


if __name__ == '__main__':
    unittest.main()
