import cv2
import numpy as np
import os
import unittest
import tensorflow as tf
from common.utils.img import read_image, write_image
from pwcnet.bilateral_filter.bilateral_filter import bilateral_filter, tf_bilateral_filter
from tensorflow.python.ops import gradient_checker


VISUALIZE = False


class TestBilateralFilter(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        self.image_path = os.path.join('pwcnet', 'warp', 'test_data', 'image_a.png')
        self.image = read_image(self.image_path, as_float=True)

    def test_filter(self):
        kernel_size = 5
        sigma_color = 0.1
        sigma_space = 1.5

        shape = self.image.shape
        shape = (1, shape[0], shape[1], shape[2])
        image_tensor = tf.placeholder(shape=shape, dtype=tf.float32)
        filtered_image_tensor = tf.clip_by_value(
            bilateral_filter(image_tensor, kernel_size=kernel_size, range_std_dev=sigma_color,
                             spacial_std_dev=sigma_space), 0, 1)
        filtered_tf_image_tensor = tf.clip_by_value(
            tf_bilateral_filter(image_tensor, kernel_size=kernel_size, range_std_dev=sigma_color,
                                spacial_std_dev=sigma_space), 0, 1)
        filtered_image, tf_filtered_image = self.sess.run([filtered_image_tensor, filtered_tf_image_tensor],
                                                          feed_dict={image_tensor: [self.image]})
        filtered_image = filtered_image[0]
        tf_filtered_image = tf_filtered_image[0]

        self.assertTrue(np.allclose(filtered_image, tf_filtered_image))

        if VISUALIZE:
            cv2_bilateral_filter = cv2.bilateralFilter(self.image, d=kernel_size, sigmaColor=sigma_color,
                                                       sigmaSpace=sigma_space)
            write_image('outputs/unfiltered.png', self.image)
            write_image('outputs/bilateral_filtered.png', filtered_image)
            write_image('outputs/cv2_bilateral_filtered.png', cv2_bilateral_filter)


class TestForwardWarpBilateralIdentity(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        img_shape = (16, 16, 3)
        self.image = np.random.rand(*img_shape)

    def test_filter_identity_kernel(self):
        kernel_size = 1
        sigma_color = 0.1
        sigma_space = 2.5

        shape = self.image.shape
        shape = (1, shape[0], shape[1], shape[2])
        image_tensor = tf.placeholder(shape=shape, dtype=tf.float32)
        filtered_image_tensor = tf.clip_by_value(
            bilateral_filter(image_tensor, kernel_size=kernel_size, range_std_dev=sigma_color,
                             spacial_std_dev=sigma_space), 0, 1)
        filtered_image = self.sess.run(filtered_image_tensor, feed_dict={image_tensor: [self.image]})[0]

        self.assertTrue(np.allclose(filtered_image, self.image))

    def test_filter_identity_sigma_space(self):
        kernel_size = 5
        sigma_color = 0.1
        sigma_space = 1e-5

        shape = self.image.shape
        shape = (1, shape[0], shape[1], shape[2])
        image_tensor = tf.placeholder(shape=shape, dtype=tf.float32)
        filtered_image_tensor = tf.clip_by_value(
            bilateral_filter(image_tensor, kernel_size=kernel_size, range_std_dev=sigma_color,
                             spacial_std_dev=sigma_space), 0, 1)
        filtered_image = self.sess.run(filtered_image_tensor, feed_dict={image_tensor: [self.image]})[0]

        self.assertTrue(np.allclose(filtered_image, self.image))

    def test_filter_identity_sigma_color(self):
        kernel_size = 5
        sigma_color = 1e-5
        sigma_space = 2.5

        shape = self.image.shape
        shape = (1, shape[0], shape[1], shape[2])
        image_tensor = tf.placeholder(shape=shape, dtype=tf.float32)
        filtered_image_tensor = tf.clip_by_value(
            bilateral_filter(image_tensor, kernel_size=kernel_size, range_std_dev=sigma_color,
                             spacial_std_dev=sigma_space), 0, 1)
        filtered_image = self.sess.run(filtered_image_tensor, feed_dict={image_tensor: [self.image]})[0]

        self.assertTrue(np.allclose(filtered_image, self.image))


class TestBilateralFilterGrads(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        self.max_allowable_grad_err = 1.5e-3

    def test_consistent_implementations(self):
        kernel_size = 5
        sigma_color = 0.4
        sigma_space = 1.5

        img = [
            [[1.0], [1.0], [1.0], [1.0], [0.9]],
            [[0.8], [1.0], [1.0], [1.0], [1.0]],
            [[0.8], [1.0], [1.5], [1.0], [1.0]],
            [[0.8], [1.0], [1.5], [1.0], [1.0]],
            [[1.0], [1.0], [1.0], [1.0], [1.0]],
            [[1.0], [1.0], [1.0], [1.0], [1.0]]
        ]
        img = np.asarray(img, dtype=np.float32)

        image_tensor = tf.placeholder(shape=[None, 6, 5, 1], dtype=tf.float32)
        filtered_image_tensor = bilateral_filter(image_tensor, kernel_size=kernel_size, range_std_dev=sigma_color,
                                                 spacial_std_dev=sigma_space)
        filtered_tf_image_tensor = tf_bilateral_filter(image_tensor, kernel_size=kernel_size, range_std_dev=sigma_color,
                                                       spacial_std_dev=sigma_space)

        grads_tensor = tf.gradients(filtered_image_tensor, image_tensor)[0]
        tf_grads_tensor = tf.gradients(filtered_tf_image_tensor, image_tensor)[0]

        filtered_image, filtered_tf_image, grads, tf_grads = self.sess.run(
            [filtered_image_tensor, filtered_tf_image_tensor, grads_tensor, tf_grads_tensor],
            feed_dict={image_tensor: [img]})

        self.assertTrue(np.allclose(filtered_image, filtered_tf_image))
        self.assertTrue(np.allclose(grads, tf_grads))

    def test_gradients_errors_single_channel(self):
        kernel_size = 5
        sigma_color = 0.2
        sigma_space = 1.5

        with self.sess:
            # This test is flaky, so retry if fail.
            num_tries = 2
            error = 0
            for i in range(num_tries):
                img_shape = (4, 16, 16, 1)
                img = np.random.rand(*img_shape)
                input = tf.placeholder(shape=img.shape, dtype=tf.float32)
                filtered_input = bilateral_filter(input, kernel_size=kernel_size, range_std_dev=sigma_color,
                                                  spacial_std_dev=sigma_space)

                error = gradient_checker.compute_gradient_error(input, img.shape, filtered_input, img.shape,
                                                                x_init_value=img, delta=2e-4)
                if error <= self.max_allowable_grad_err:
                    return
            self.assertLessEqual(error, self.max_allowable_grad_err,
                                 'Exceeded the error threshold. Note that this test may be flaky.')

    def test_gradients_errors(self):
        kernel_size = 5
        sigma_color = 0.2
        sigma_space = 1.5

        with self.sess:
            # This test is flaky, so retry if fail.
            num_tries = 3
            error = 0
            for i in range(num_tries):
                img_shape = (4, 16, 16, 3)
                img = np.random.rand(*img_shape)
                input = tf.placeholder(shape=img.shape, dtype=tf.float32)
                filtered_input = bilateral_filter(input, kernel_size=kernel_size, range_std_dev=sigma_color,
                                                  spacial_std_dev=sigma_space)

                error = gradient_checker.compute_gradient_error(input, img.shape, filtered_input, img.shape,
                                                                x_init_value=img, delta=2e-4)
                if error <= self.max_allowable_grad_err:
                    return
            self.assertLessEqual(error, self.max_allowable_grad_err,
                                 'Exceeded the error threshold. Note that this test may be flaky.')


if __name__ == '__main__':
    unittest.main()
