cmake_minimum_required(VERSION 3.5)

add_op_library(NAME bilateral_filter_op SOURCES
    "bilateral_filter_op.cc"
    "bilateral_filter_op.cc.cu"
)
