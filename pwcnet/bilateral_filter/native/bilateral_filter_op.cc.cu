#if GOOGLE_CUDA

#define EIGEN_USE_GPU

#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/util/cuda_kernel_helper.h"

using namespace tensorflow;

typedef Eigen::GpuDevice GPUDevice;

// Implements a bilateral filter as described by Wikipedia: https://en.wikipedia.org/wiki/Bilateral_filter
template<int channels>
__global__ void BilateralFilterKernel(const int32 nthreads,
    const float* images, int batch, int height, int width,
    float* output, int kernel_size, float range_std_dev, float spacial_std_dev) {
	CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
		// out_idx = src_x + width * (src_y + height * b).
		int idx = out_idx;
		const int src_x = idx % width;
		idx /= width;
		const int src_y = idx % height;
		const int b = idx / height;

		const int half_kernel = kernel_size / 2;
		const float range_variance = range_std_dev * range_std_dev;
		const float spacial_variance = spacial_std_dev * spacial_std_dev;

        const float range_gauss_divisor = 2.0f * range_variance;
        const float spacial_gauss_divisor = 2.0f * spacial_variance;
        float total_normalizer = 0.0f;

        // Cache image values in registers.
        float images_reg[channels];

#define IMG_OFFSET(iy, ix, c) ((c) + channels * ((ix) + width * ((iy) + height * b)))
        // Cache src image values in registers.
        float src_image_reg[channels];
#pragma unroll
        for (int c = 0; c < channels; ++c) {
            src_image_reg[c] = images[out_idx * channels + c];
        }

        float output_sum[channels] = {0.0f};
		for (int y = src_y - half_kernel; y <= src_y + half_kernel; ++y) {
		    for (int x = src_x - half_kernel; x <= src_x + half_kernel; ++x) {
                if (x >= 0 && x < width && y >= 0 && y < height) {
                    const int x_diff = x - src_x;
                    const int y_diff = y - src_y;
                    const float spacial_weight_log = -(x_diff * x_diff + y_diff * y_diff) / spacial_gauss_divisor;

                    float range_diff = 0.0f;
#pragma unroll
                    for (int c = 0; c < channels; ++c) {
                        images_reg[c] = images[IMG_OFFSET(y, x, c)];
                        const float diff = src_image_reg[c] - images_reg[c];
                        range_diff += diff * diff;
                    }
                    const float range_weight_log = -range_diff / range_gauss_divisor;
                    const float weight = expf(spacial_weight_log + range_weight_log);
                    total_normalizer += weight;

#pragma unroll
                    for (int c = 0; c < channels; ++c) {
                        output_sum[c] += weight * images_reg[c];
                    }
                }
		    }
		}

#pragma unroll
        for (int c = 0; c < channels; ++c) {
            output[out_idx * channels + c] = output_sum[c] / total_normalizer;
        }
#undef IMG_OFFSET
	}
}

// Implements the bilateral filter gradient.
// Note that it recomputes the denominator. Experimentally, this is actually faster than saving the denominator
// from the forward pass.
template<int channels>
__global__ void BilateralFilterGradKernel(const int32 nthreads,
    const float* input_grad, const float* images, const float* original_outputs,
    int batch, int height, int width,
    float* output, int kernel_size, float range_std_dev, float spacial_std_dev) {
	CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
		// out_idx = c + channels * (src_x + width * (src_y + height * b)).
		int idx = out_idx;
		const int c = idx % channels;
		idx /= channels;
		const int src_x = idx % width;
		idx /= width;
		const int src_y = idx % height;
		const int b = idx / height;

        float d_lo_c[channels];
        float d_hi_c[channels];

		const int half_kernel = kernel_size / 2;
		const float range_variance = range_std_dev * range_std_dev;
		const float spacial_variance = spacial_std_dev * spacial_std_dev;

        const float range_gauss_divisor = 2.0f * range_variance;
        const float spacial_gauss_divisor = 2.0f * spacial_variance;

        // Cache image values in registers.
        float images_reg[channels];

#define IMG_OFFSET(iy, ix, ic) ((ic) + channels * ((ix) + width * ((iy) + height * b)))
        // Cache src image values in registers.
        float src_image_reg[channels];
#pragma unroll
        for (int c = 0; c < channels; ++c) {
            src_image_reg[c] = images[IMG_OFFSET(src_y, src_x, c)];
        }

        // Compute the normalizer (denominator).
        float denominator = 0.0f;
        for (int y = src_y - half_kernel; y <= src_y + half_kernel; ++y) {
		    for (int x = src_x - half_kernel; x <= src_x + half_kernel; ++x) {
                if (x >= 0 && x < width && y >= 0 && y < height) {
                    const int x_diff = x - src_x;
                    const int y_diff = y - src_y;
                    const float spacial_weight_log = -(x_diff * x_diff + y_diff * y_diff) / spacial_gauss_divisor;

                    float range_diff = 0.0f;
#pragma unroll
                    for (int c_i = 0; c_i < channels; ++c_i) {
                        const float diff = src_image_reg[c_i] - images[IMG_OFFSET(y, x, c_i)];
                        range_diff += diff * diff;
                    }
                    const float range_weight_log = -range_diff / range_gauss_divisor;
                    const float weight = expf(spacial_weight_log + range_weight_log);
                    denominator += weight;
                }
		    }
		}

		const float din = input_grad[out_idx];
		const float original_input = original_outputs[out_idx];

#pragma unroll
        for (int c_i = 0; c_i < channels; ++c_i) {
            d_lo_c[c_i] = 0.0f;
            d_hi_c[c_i] = 0.0f;
        }

        // Compute derivatives with respect to each x, y for this given c.
        for (int y = src_y - half_kernel; y <= src_y + half_kernel; ++y) {
            for (int x = src_x - half_kernel; x <= src_x + half_kernel; ++x) {
                if (x >= 0 && x < width && y >= 0 && y < height) {
                    const int x_diff = x - src_x;
                    const int y_diff = y - src_y;
                    const float spacial_weight_log = -(x_diff * x_diff + y_diff * y_diff) / spacial_gauss_divisor;

                    float range_diff = 0.0f;
#pragma unroll
                    for (int c_i = 0; c_i < channels; ++c_i) {
                        images_reg[c_i] = images[IMG_OFFSET(y, x, c_i)];
                        const float diff = src_image_reg[c_i] - images_reg[c_i];
                        range_diff += diff * diff;
                    }
                    const float range_weight_log = -range_diff / range_gauss_divisor;
                    const float weight = expf(spacial_weight_log + range_weight_log);

                    const float i_curr = images_reg[c];
#pragma unroll
                    for (int c_i = 0; c_i < channels; ++c_i) {
                        const float i_val = images_reg[c_i];
                        const float i_src_val = src_image_reg[c_i];

                        bool curr_channel = c == c_i;
                        if (x == src_x && y == src_y) {
                            if (curr_channel) {
                                d_hi_c[c_i] += weight;
                            }
                        }
                        else {
                            const float d_exp = (i_src_val - i_val) / range_variance;
                            // Partial derivative of the denominator.
                            const float d_lo = weight * d_exp;
                            // Partial derivative of the numerator.
                            const float d_hi = curr_channel ?
                                weight + d_lo * i_val : d_lo * i_curr;
                            // Quotient rule (denominator * d_hi - numerator * d_lo) / (denominator * denominator).
                            // Note that numerator / (denominator * denominator) = original_input / denominator
                            // because numerator / denominator = original_input.
                            const float d_frac = (d_hi - original_input * d_lo) / denominator;
                            // Applies the gradient.
                            CudaAtomicAdd(output + IMG_OFFSET(y, x, c_i), din * d_frac);

                            // Accumulate the gradient for the middle pixel.
                            d_hi_c[c_i] -= d_lo * i_curr;
                            d_lo_c[c_i] -= d_lo;
                        }
                    }

                }
            }
        }

#pragma unroll
        for (int c_i = 0; c_i < channels; ++c_i) {
            // Quotient rule (denominator * d_hi_c[c_i] - numerator * d_lo_c[c_i]) / (denominator * denominator).
            // Note that numerator / (denominator * denominator) = original_input / denominator because
            // numerator / denominator = original_input.
            float d_frac_src =  (d_hi_c[c_i] - original_input * d_lo_c[c_i]) / denominator;
            CudaAtomicAdd(output + IMG_OFFSET(src_y, src_x, c_i), din * d_frac_src);
        }
#undef IMG_OFFSET
	}
}

void BilateralFilter(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor images,
    typename TTypes<float, 4>::Tensor output,
    int kernel_size, float range_std_dev, float spacial_std_dev) {
	const int batch = images.dimension(0);
	const int height = images.dimension(1);
	const int width = images.dimension(2);
	const int channels = images.dimension(3);

    int total_count = batch * height * width * channels;
	if (total_count == 0) return;

    // Initialize with all zeros.
	CudaLaunchConfig config = GetCudaLaunchConfig(total_count, d);
	SetZero << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
		config.virtual_thread_count, output.data());

	total_count = batch * height * width;
	config = GetCudaLaunchConfig(total_count, d);

#define BilateralFilterKernelC(ic)\
    BilateralFilterKernel<ic>\
        <<<config.block_count, config.thread_per_block, 0, d.stream()>>>(\
            config.virtual_thread_count, images.data(),\
            batch, height, width,\
            output.data(), kernel_size, range_std_dev, spacial_std_dev)

	switch (channels) {
    case 1:
        BilateralFilterKernelC(1);
        break;
    case 2:
        BilateralFilterKernelC(2);
        break;
    case 3:
        BilateralFilterKernelC(3);
        break;
    case 4:
        BilateralFilterKernelC(4);
        break;
    default:
        break;
	}
}

void BilateralFilterGrad(const Eigen::GpuDevice& d,
    typename TTypes<float, 4>::ConstTensor grads,
	typename TTypes<float, 4>::ConstTensor original_images,
	typename TTypes<float, 4>::ConstTensor original_outputs,
	typename TTypes<float, 4>::Tensor output_image_grad,
	int kernel_size, float range_std_dev, float spacial_std_dev) {
	const int batch = original_images.dimension(0);
	const int height = original_images.dimension(1);
	const int width = original_images.dimension(2);
	const int channels = original_images.dimension(3);

	int total_count = batch * height * width * channels;
	if (total_count == 0) return;
	CudaLaunchConfig config = GetCudaLaunchConfig(total_count, d);

	// Initialize with all zeros.
	SetZero << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
		config.virtual_thread_count, output_image_grad.data());

#define BilateralFilterGradKernelC(ic)\
    BilateralFilterGradKernel<ic>\
        <<<config.block_count, config.thread_per_block, 0, d.stream()>>>(\
            config.virtual_thread_count, grads.data(), original_images.data(), original_outputs.data(),\
            batch, height, width,\
            output_image_grad.data(), kernel_size, range_std_dev, spacial_std_dev)

    switch (channels) {
    case 1:
        BilateralFilterGradKernelC(1);
        break;
    case 2:
        BilateralFilterGradKernelC(2);
        break;
    case 3:
        BilateralFilterGradKernelC(3);
        break;
    case 4:
        BilateralFilterGradKernelC(4);
        break;
    default:
        break;
	}
}

#endif  // GOOGLE_CUDA
