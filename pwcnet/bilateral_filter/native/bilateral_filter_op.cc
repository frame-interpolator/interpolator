#define EIGEN_USE_THREADS

#include <memory>
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/framework/types.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/common_shape_fns.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"

using namespace tensorflow;

typedef Eigen::GpuDevice GPUDevice;

void BilateralFilter(const Eigen::GpuDevice& d,
	typename TTypes<float, 4>::ConstTensor images,
	typename TTypes<float, 4>::Tensor output,
	int kernel_size, float range_std_dev, float spacial_std_dev);

void BilateralFilterGrad(const Eigen::GpuDevice& d,
    typename TTypes<float, 4>::ConstTensor grads,
	typename TTypes<float, 4>::ConstTensor original_images,
	typename TTypes<float, 4>::ConstTensor original_outputs,
	typename TTypes<float, 4>::Tensor output_image_grad,
	int kernel_size, float range_std_dev, float spacial_std_dev);

class BilateralFilterOp : public OpKernel {
public:
	explicit BilateralFilterOp(OpKernelConstruction* context) : OpKernel(context) {
	    OP_REQUIRES_OK(context, context->GetAttr("kernel_size", &kernel_size_));
		OP_REQUIRES(context, kernel_size_ > 0,
			errors::InvalidArgument("Need kernel_size > 0, got ", kernel_size_));
        OP_REQUIRES(context, (kernel_size_ % 2) == 1,
			errors::InvalidArgument("Need kernel_size to be odd, got ", kernel_size_));

        OP_REQUIRES_OK(context, context->GetAttr("range_std_dev", &range_std_dev_));
        OP_REQUIRES(context, range_std_dev_ > 0,
			errors::InvalidArgument("Need range_std_dev > 0, got ", range_std_dev_));

        OP_REQUIRES_OK(context, context->GetAttr("spacial_std_dev", &spacial_std_dev_));
        OP_REQUIRES(context, spacial_std_dev_ > 0,
			errors::InvalidArgument("Need spacial_std_dev > 0, got ", spacial_std_dev_));
	}

	void Compute(OpKernelContext* context) override {
		const Tensor& input_images = context->input(0);

        // Assert correct number of dimensions.
        OP_REQUIRES(context, input_images.dims() == 4,
            errors::InvalidArgument("input_image expecting a 4-D vector."));
        OP_REQUIRES(context, input_images.dim_size(3) <= 4,
            errors::InvalidArgument("This operation does not support more than 4 input channels."));

		Tensor* output_images = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(0, input_images.shape(),
			&output_images));

		typename TTypes<float, 4>::ConstTensor image_data = input_images.tensor<float, 4>();
		typename TTypes<float, 4>::Tensor output_data = output_images->tensor<float, 4>();

		BilateralFilter(context->eigen_device<GPUDevice>(),
			image_data, output_data, kernel_size_, range_std_dev_, spacial_std_dev_);
	}

private:
    int kernel_size_;
    float range_std_dev_;
    float spacial_std_dev_;
};

class BilateralFilterOpGrad : public OpKernel {
public:
	explicit BilateralFilterOpGrad(OpKernelConstruction* context) : OpKernel(context) {
	    OP_REQUIRES_OK(context, context->GetAttr("kernel_size", &kernel_size_));
		OP_REQUIRES(context, kernel_size_ > 0,
			errors::InvalidArgument("Need kernel_size > 0, got ", kernel_size_));
        OP_REQUIRES(context, (kernel_size_ % 2) == 1,
			errors::InvalidArgument("Need kernel_size to be odd, got ", kernel_size_));

        OP_REQUIRES_OK(context, context->GetAttr("range_std_dev", &range_std_dev_));
        OP_REQUIRES(context, range_std_dev_ > 0,
			errors::InvalidArgument("Need range_std_dev > 0, got ", range_std_dev_));

        OP_REQUIRES_OK(context, context->GetAttr("spacial_std_dev", &spacial_std_dev_));
        OP_REQUIRES(context, spacial_std_dev_ > 0,
			errors::InvalidArgument("Need spacial_std_dev > 0, got ", spacial_std_dev_));
	}

	void Compute(OpKernelContext* context) override {
		const Tensor& grads = context->input(0);
		const Tensor& original_images = context->input(1);
		const Tensor& original_outputs = context->input(2);

        // Assert correct number of dimensions.
        OP_REQUIRES(context, original_images.dims() == 4,
            errors::InvalidArgument("original_images expecting a 4-D vector."));
        OP_REQUIRES(context, original_outputs.dims() == 4,
            errors::InvalidArgument("original_outputs expecting a 4-D vector."));
        for (int d = 0; d < 4; ++d) {
            OP_REQUIRES(context, original_images.dim_size(d) == original_outputs.dim_size(d),
                errors::InvalidArgument("input and output dimensions did not match."));
        }
        OP_REQUIRES(context, original_images.dim_size(3) <= 4,
            errors::InvalidArgument("This operation does not support more than 4 input channels."));

		Tensor* output_image_grad = NULL;
		OP_REQUIRES_OK(context, context->allocate_output(0, original_images.shape(),
			&output_image_grad));

		typename TTypes<float, 4>::ConstTensor grads_data = grads.tensor<float, 4>();
		typename TTypes<float, 4>::ConstTensor original_images_data = original_images.tensor<float, 4>();
		typename TTypes<float, 4>::ConstTensor original_outputs_data = original_outputs.tensor<float, 4>();
		typename TTypes<float, 4>::Tensor output_image_grad_data = output_image_grad->tensor<float, 4>();

		BilateralFilterGrad(context->eigen_device<GPUDevice>(),
			grads_data, original_images_data, original_outputs_data,
			output_image_grad_data, kernel_size_, range_std_dev_, spacial_std_dev_);
	}

private:
    int kernel_size_;
    float range_std_dev_;
    float spacial_std_dev_;
};

REGISTER_OP("BilateralFilter")
.Input("images: float")
.Output("filtered_images: float")
.Attr("kernel_size: int = 5")
.Attr("range_std_dev: float = 0.1")
.Attr("spacial_std_dev: float = 1.5")
.SetShapeFn(shape_inference::UnchangedShape);

REGISTER_OP("BilateralFilterGrad")
.Input("grads: float")
.Input("original_images: float")
.Input("original_outputs: float")
.Output("output_image_grad: float")
.Attr("kernel_size: int = 5")
.Attr("range_std_dev: float = 0.1")
.Attr("spacial_std_dev: float = 1.5")
.SetShapeFn(shape_inference::UnchangedShape);

#if GOOGLE_CUDA

REGISTER_KERNEL_BUILDER(Name("BilateralFilter").Device(DEVICE_GPU), BilateralFilterOp);
REGISTER_KERNEL_BUILDER(Name("BilateralFilterGrad").Device(DEVICE_GPU), BilateralFilterOpGrad);

#endif // GOOGLE_CUDA
