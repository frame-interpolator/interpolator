import numpy as np
import tensorflow as tf
from common.utils.profile import run_profiler
from pwcnet.bilateral_filter.bilateral_filter import bilateral_filter


if __name__ == '__main__':
    height = 512
    width = 512
    im_channels = 3
    batch_size = 8

    # Create the graph.
    image_shape = [batch_size, height, width, im_channels]
    image_placeholder = tf.placeholder(shape=image_shape, dtype=tf.float32)
    filtered_img = bilateral_filter(image_placeholder, kernel_size=5, range_std_dev=0.1, spacial_std_dev=2.5)
    grads = tf.gradients(filtered_img, image_placeholder)

    # Create dummy images.
    image = np.zeros(shape=[batch_size, height, width, im_channels], dtype=np.float32)
    image[:, 2:height - 2, 2:width - 2, :] = 1.0

    query = [filtered_img, grads]
    feed_dict = {image_placeholder: image}

    run_profiler(query, feed_dict, name='bilateral-filter')
