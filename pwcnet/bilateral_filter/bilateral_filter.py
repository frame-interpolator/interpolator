import numpy as np
import tensorflow as tf
from common.extract_patches.extract_patches import extract_patches
from common.utils.tf import load_op_library
from tensorflow.python.framework import ops


# Load op library.
mod = load_op_library('bilateral_filter_op', 'build')


def bilateral_filter(images, kernel_size=5, range_std_dev=0.1, spacial_std_dev=1.5):
    """
    :param images: Image tensor of shape [B, H, W, C].
    :param kernel_size: Diameter of the kernel.
    :param range_std_dev: Standard deviation of the color.
    :param spacial_std_dev: Standard deviation of the kernel.
    :return: Image tensor of shape [B, H, W, C].
    """
    with tf.name_scope('bilateral_filter'):
        if mod is not None:
            return mod.bilateral_filter(images, kernel_size=kernel_size,
                                        range_std_dev=range_std_dev, spacial_std_dev=spacial_std_dev)
        else:
            return tf_bilateral_filter(images, kernel_size=kernel_size,
                                       range_std_dev=range_std_dev, spacial_std_dev=spacial_std_dev)


if mod is not None:
    @ops.RegisterGradient('BilateralFilter')
    def _BilateralFilterGrad(op, grad):
        image_grad = mod.bilateral_filter_grad(grad, op.inputs[0], op.outputs[0],
                                               kernel_size=op.get_attr('kernel_size'),
                                               range_std_dev=op.get_attr('range_std_dev'),
                                               spacial_std_dev=op.get_attr('spacial_std_dev'))
        return image_grad


def _np_create_gaussian_kernel(kernel_size, spacial_std_dev):
    """
    :param kernel_size: Int. Must be an odd number greater than 0.
    :param spacial_std_dev: Std-dev of the kernel.
    :return: Un-normalized gaussian kernel with shape [kernel_size, kernel_size].
    """
    spacial_variance = spacial_std_dev * spacial_std_dev
    half_kernel = int(kernel_size / 2)
    assert half_kernel * 2 + 1 == kernel_size
    x = np.linspace(-half_kernel, half_kernel, kernel_size)
    y = np.linspace(-half_kernel, half_kernel, kernel_size)
    xs, yx = np.meshgrid(x, y, sparse=False, indexing='xy')
    xs, yx = xs[..., None], yx[..., None]
    kernel = np.concatenate((yx, xs), axis=-1)
    kernel = np.exp(-np.sum(np.square(kernel), axis=-1) / (2.0 * spacial_variance))
    return kernel


def tf_bilateral_filter(images, kernel_size=5, range_std_dev=0.1, spacial_std_dev=1.5):
    """
    Implemented using only existing Tensorflow ops.
    :param images: Image tensor of shape [B, H, W, C].
    :param kernel_size: Diameter of the kernel.
    :param range_std_dev: Standard deviation of the color.
    :param spacial_std_dev: Standard deviation of the kernel.
    :return: Image tensor of shape [B, H, W, C].
    """
    range_variance = range_std_dev * range_std_dev
    range_gauss_divisor = 2.0 * range_variance

    B, H, W, C = tf.unstack(tf.shape(images))
    # Get the patches.
    patches = extract_patches(images, kernel_size=kernel_size)
    patches = tf.reshape(patches, [B, H, W, kernel_size * kernel_size, C])
    # Create mask for boundary masking.
    ones = tf.ones(shape=[B, H, W, 1], dtype=tf.float32)
    mask = extract_patches(ones, kernel_size=kernel_size)
    mask = tf.reshape(mask, [B, H, W, kernel_size * kernel_size, 1])

    # Subtract from the center value of each patch.
    center_values = tf.reshape(images, [B, H, W, 1, C])
    # Shape of diff_squared is [B, H, W, kernel_size * kernel_size, 1].
    diff_squared = tf.reduce_sum(tf.square(patches - center_values), axis=-1, keepdims=True)
    range_weight = tf.exp(-diff_squared / range_gauss_divisor) * mask

    # Create gaussian kernel.
    spacial_weight = tf.constant(_np_create_gaussian_kernel(kernel_size, spacial_std_dev), dtype=tf.float32)
    spacial_weight = tf.reshape(spacial_weight, [1, 1, 1, kernel_size * kernel_size, 1])

    # Weight has shape [B, H, W, kernel_size * kernel_size, 1].
    weight = range_weight * spacial_weight

    filtered = tf.reduce_sum(patches * weight, axis=-2) / tf.reduce_sum(weight, axis=-2)
    return filtered
