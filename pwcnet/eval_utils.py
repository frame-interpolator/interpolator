import numpy as np
import os
import random
from common.models import FrozenModel
from common.utils.flow import read_flow_file, stretch_flow_for_divisibility, get_flow_visualization,\
    write_flow_file, resize_flow
from common.utils.img import stretch_image_for_divisibility, read_image, write_image
from common.utils.misc import print_progress_bar
from common.utils.tf import sample_n_from_weights_py
from pwcnet.model import PWCNet


def hint_eval_results_to_csv(totals, low_speeds, med_speeds, hi_speeds, min_num_hints=0):
    """
    Inputs are the outputs of eval_sintel_with_hints.
    :param min_num_hints: Int. Minimum number of hints evaluated.
    :return: Str. CSV text.
    """
    # print('EPE all: %.3f' % total)
    # print('s0-10: %.3f' % low_speed)
    # print('s10-40: %.3f' % med_speed)
    # print('s40+: %.3f' % hi_speed)
    csv_str = 'num_hints, EPE all, s0-10, s10-40, s40+\n'
    for i, (total, low, med, hi) in enumerate(zip(totals, low_speeds, med_speeds, hi_speeds)):
        csv_str += str(i + min_num_hints) + ', '
        csv_str += '%.3f, ' % total
        csv_str += '%.3f, ' % low
        csv_str += '%.3f, ' % med
        csv_str += '%.3f\n' % hi
    return csv_str


def _sample_deterministic_weights(weights, num_points):
    """
    :param weights: Numpy array of shape [B, H, W, 1]. Float values.
    :param num_points: Int. Number of points to sample.
    :return: Numpy array of shape [B, H, W, 1]. Values are either 0.0 or 1.0.
    """
    batch_size, height, width = weights.shape[0], weights.shape[1], weights.shape[2]
    sampled_masks = np.zeros((batch_size, height, width, 1), dtype=np.float32)
    for batch_idx in range(batch_size):
        curr_weights = weights[batch_idx].reshape((-1,))
        # Sort from low to high. argsort returns indices.
        curr_weights = np.argsort(curr_weights)
        for i in range(num_points):
            # index = col + width * row
            index = curr_weights[-(i + 1)]  # Highest index is at the end of the list.
            col = int(index % width)
            row = int(index / width)
            sampled_masks[batch_idx, row, col, 0] = 1.0
    return sampled_masks


def create_eval_hint_input(gt, num_hints, valid_mask=None, prev_hint_mask=None, prev_flow=None,
                           sample_percent_error=False, probabilistic=True):
    """
    Samples hints based on ground truth magnitude (by default).
    :param gt: Numpy array of shape [H, W, 2].
    :param num_hints: Int.
    :param valid_mask: Optional array of shape [H, W, 1]. Values are 0.0 or 1.0. 1.0 denotes a valid place to sample.
    :param prev_hint_mask: Optional hint mask to accumulate the hints with. If used, the output hint mask will be added
        to the previous hint mask.
    :param prev_flow: Optional previous flow of shape [H, W, 2]. If provided, the sampling will be done based on flow
        error and not gt magnitude.
    :param sample_percent_error: Bool. If a previous flow is provided, then sampling is either based on magnitude
        of the error, or the percent error.
    :param probabilistic: Bool. Whether to sample based on distribution or deterministically based on sorted magnitude.
    :return: Numpy array of shape [1, fitted_H, fitted_W, 2] - hint.
            Numpy array of shape [1, fitted_H, fitted_W, 1] - mask.
    """
    # Reseed both numpy.random and random for sanity.
    np.random.seed(0)
    random.seed(0)
    # Stretch and add the batch dimension.
    gt = np.expand_dims(stretch_flow_for_divisibility(gt, PWCNet.NUM_FEATURE_LEVELS), axis=0)
    assert len(gt.shape) == 4
    if prev_flow is not None:
        prev_flow = np.expand_dims(stretch_flow_for_divisibility(prev_flow, PWCNet.NUM_FEATURE_LEVELS), axis=0)
        sample_weights = np.linalg.norm(gt - prev_flow, axis=-1, keepdims=True)
        if sample_percent_error:
            sample_weights = sample_weights / (np.linalg.norm(gt, axis=-1, keepdims=True) + 1e-3)
    else:
        sample_weights = np.linalg.norm(gt, axis=-1, keepdims=True)
    if valid_mask is not None:
        valid_mask = valid_mask.astype(np.float32)
        valid_mask = np.expand_dims(stretch_image_for_divisibility(valid_mask, PWCNet.NUM_FEATURE_LEVELS), axis=0)
        assert len(valid_mask.shape) == 4
        valid_mask = (valid_mask > 0.95).astype(np.float32)
        sample_weights *= valid_mask
    prev_count = 0
    if prev_hint_mask is not None:
        prev_count = float(np.sum(prev_hint_mask))
    sample_func = _sample_deterministic_weights if not probabilistic else sample_n_from_weights_py
    while True:
        hint_mask = sample_func(sample_weights, num_hints)
        if prev_hint_mask is not None:
            hint_mask = np.clip(hint_mask + prev_hint_mask, 0.0, 1.0)
        # Ensure we have exactly num_hints, or try again. If deterministic, no point in trying again.
        if np.sum(hint_mask) == (float(num_hints) + prev_count) or not probabilistic:
            break
    hint = gt * hint_mask
    return hint, hint_mask


def eval_file_list_with_hints(model, file_lists, min_num_hints=0, max_num_hints=12, print_output=False,
                              preprocess_valid_mask=None, percent_error=False, error_sampling=False,
                              probabilistic=True):
    """
    :param model: FrozenModel.
    :param file_lists: Tuple of 3 lists of file paths: images_a, images_b, flows.
    :param min_num_hints: Int. Evaluates from [min_num_hints, max_num_hints] hints.
    :param max_num_hints: Int. Evaluates from [min_num_hints, max_num_hints] hints.
    :param print_output: Bool.
    :param preprocess_valid_mask: Optional function. Inputs are (idx, valid_mask). Outputs are (valid_mask).
        Used to preprocess a valid mask or even add a valid mask if there wasn't one to begin with.
        idx is the index of the current file_list element.
        Output shape should be [H, W, 1].
    :param percent_error: Bool. Whether the EPE is expressed as an error percentage.
    :param print_output: Bool. Whether to sample based on error of the previous flow.
    :param error_sampling: Bool. Whether to sample hints based on the error of the previous flow.
    :param probabilistic: Bool. Whether the hints are sampled pseudo-randomly based on a fixed seed or whether it's
        sampled by sorting the error/magnitudes.
    :return: List of average endpoint errors of the flows for each number of hints.
             List of average EPEs of speeds between 0-10.
             List of average EPEs of speeds between 10-40.
             List of average EPEs of speeds between 40+.
    """
    assert max_num_hints > min_num_hints
    num_hints_range = range(min_num_hints, max_num_hints + 1)
    total_aepe = [0.0 for _ in num_hints_range]
    total_low_epe = [0.0 for _ in num_hints_range]
    total_med_epe = [0.0 for _ in num_hints_range]
    total_hi_epe = [0.0 for _ in num_hints_range]
    low_count = [0.0 for _ in num_hints_range]
    med_count = [0.0 for _ in num_hints_range]
    hi_count = [0.0 for _ in num_hints_range]
    images_a, images_b, flows = file_lists

    enumerated = enumerate(zip(images_a, images_b, flows))
    for i, (image_a_path, image_b_path, flow_path) in enumerated:
        extras = {}
        gt = read_flow_file(flow_path, extras=extras, use_default_valid=True)
        valid_mask = extras['valid'].astype(np.float32)
        if preprocess_valid_mask is not None:
            valid_mask = preprocess_valid_mask(i, valid_mask)
        flow = None
        hint_mask = None
        for num_hints in num_hints_range:
            if error_sampling:
                next_num_hints = 0 if num_hints == 0 else 1
                hint_inputs = create_eval_hint_input(gt, next_num_hints, valid_mask=valid_mask,
                                                     sample_percent_error=percent_error, prev_flow=flow,
                                                     prev_hint_mask=hint_mask, probabilistic=probabilistic)
                hint_mask = hint_inputs[1][0, ...]  # hint_inputs[1] has shape [1, H, W, 1].
            else:
                hint_inputs = create_eval_hint_input(gt, num_hints, valid_mask=valid_mask,
                                                     sample_percent_error=percent_error, probabilistic=probabilistic)
            flow = eval_net(model, image_a_path, image_b_path, None,
                            additional_inputs=hint_inputs)
            aepe, low_speed, med_speed, hi_speed = calculate_aepe_stats(gt, flow, valid_mask=valid_mask,
                                                                        percent_error=percent_error)
            total_aepe[num_hints - min_num_hints] += aepe
            total_low_epe[num_hints - min_num_hints] += low_speed[0]
            total_med_epe[num_hints - min_num_hints] += med_speed[0]
            total_hi_epe[num_hints - min_num_hints] += hi_speed[0]
            low_count[num_hints - min_num_hints] += low_speed[1]
            med_count[num_hints - min_num_hints] += med_speed[1]
            hi_count[num_hints - min_num_hints] += hi_speed[1]
        if print_output:
            # Small preview with one hint.
            no_hints_str = 'AEPE (' + str(min_num_hints) + ' hints): ' + '%.3f' % (total_aepe[0] / (i + 1))
            one_hint_str = 'AEPE (' + str(min_num_hints + 1) + ' hints): ' + '%.3f' % (total_aepe[1] / (i + 1))
            print_progress_bar(i + 1, len(images_a),
                               prefix=no_hints_str + ', ' + one_hint_str,
                               use_percentage=False)
    # Average the accumulations.
    total_aepes = [aepe / len(images_a) for aepe in total_aepe]
    total_low_aepes = [aepe / count for aepe, count in zip(total_low_epe, low_count)]
    total_med_aepes = [aepe / count for aepe, count in zip(total_med_epe, med_count)]
    total_hi_aepes = [aepe / count for aepe, count in zip(total_hi_epe, hi_count)]
    return total_aepes, total_low_aepes, total_med_aepes, total_hi_aepes


def eval_file_list(model, file_lists, input_dir=None, output_dir=None, print_output=False, preprocess_valid_mask=None,
                   percent_error=False):
    """
    :param model: FrozenModel.
    :param file_lists: Tuple of 3 lists of file paths: images_a, images_b, flows.
    :param input_dir: Str. Input directory.
    :param output_dir: Str. Directory to output flow images. If None then no images will be saved.
    :param print_output: Bool.
    :param preprocess_valid_mask: Optional function. Inputs are (idx, valid_mask). Outputs are (valid_mask).
        Used to preprocess a valid mask or even add a valid mask if there wasn't one to begin with.
        idx is the index of the current file_list element.
        Output shape should be [H, W, 1].
    :param percent_error: Bool. Whether the EPE is expressed as an error percentage.
    :return: The average endpoint error of the flows.
             Average EPE of speeds between 0-10.
             Average EPE of speeds between 10-40.
             Average EPE of speeds between 40+.
    """
    total_aepe = 0.0
    total_low_epe, total_med_epe, total_hi_epe = 0.0, 0.0, 0.0
    low_count, med_count, hi_count = 0.0, 0.0, 0.0
    images_a, images_b, flows = file_lists

    enumerated = enumerate(zip(images_a, images_b, flows))
    for i, (image_a_path, image_b_path, flow_path) in enumerated:
        if output_dir is not None and input_dir is not None:
            output_flow_path = os.path.join(output_dir, os.path.relpath(flow_path, input_dir))
        else:
            output_flow_path = None
        extras = {}
        gt = read_flow_file(flow_path, extras=extras, use_default_valid=True)
        valid_mask = extras['valid'].astype(np.float32)
        if preprocess_valid_mask is not None:
            valid_mask = preprocess_valid_mask(i, valid_mask)
        flow = eval_net(model, image_a_path, image_b_path, output_flow_path)
        aepe, low_speed, med_speed, hi_speed = calculate_aepe_stats(gt, flow, valid_mask=valid_mask,
                                                                    percent_error=percent_error)
        total_aepe += aepe
        total_low_epe += low_speed[0]
        total_med_epe += med_speed[0]
        total_hi_epe += hi_speed[0]
        low_count += low_speed[1]
        med_count += med_speed[1]
        hi_count += hi_speed[1]
        if print_output:
            cur_aepe = total_aepe / (i + 1)
            print_progress_bar(i + 1, len(images_a),
                               prefix='AEPE: ' + '%.3f' % cur_aepe,
                               use_percentage=False)
    return total_aepe / len(images_a), total_low_epe / low_count, total_med_epe / med_count, total_hi_epe / hi_count


def eval_net(model, image_a_path, image_b_path, output_flow_path, additional_inputs=()):
    """
    Evaluates a network for the given input files.
    :param model: FrozenModel.
    :param model_io: Tuple. (image_a_tensor, image_b_tensor, flow_tensor).
    :param session: tf.Session.
    :param image_a_path: Str.
    :param image_b_path: Str.
    :param output_flow_path: Str.
    :param additional_inputs: Tuple of additional numpy array inputs.
    :return: Final flow of shape [H, W, 2].
    """
    assert isinstance(model, FrozenModel)

    # Read images.
    try:
        image_a = read_image(image_a_path, as_float=True)
        image_b = read_image(image_b_path, as_float=True)
    except Exception as e:
        print('Reading images failed.')
        print(e)
        return

    # Check shapes.
    if image_a.shape != image_b.shape:
        print('Input image shapes do not match.')
        return
    if image_a.shape[2] != 3:
        print('Input images should have 3 channels (RGB).')
        return
    height, width = image_a.shape[0], image_a.shape[1]
    image_a = stretch_image_for_divisibility(image_a, PWCNet.NUM_FEATURE_LEVELS)
    image_b = stretch_image_for_divisibility(image_b, PWCNet.NUM_FEATURE_LEVELS)

    # Run network.
    final_flow, _ = model.run(([image_a], [image_b]) + additional_inputs)
    final_flow = resize_flow(final_flow[0], height, width)

    # Write output.
    if output_flow_path is not None:
        try:
            os.makedirs(os.path.dirname(output_flow_path), exist_ok=True)
            if output_flow_path.endswith('.png'):
                write_image(output_flow_path, get_flow_visualization(final_flow))
            else:
                write_flow_file(output_flow_path, final_flow)
        except Exception as e:
            print('Writing flow file failed.')
            print(e)

    return final_flow


def _epe_speed_range(lower, upper, speed, epe, valid_mask):
    """
    Segments EPE within a speed range.
    :param lower: Lower speed value (inclusive).
    :param upper: Upper speed value (exclusive).
    :param speed: Speed matrix.
    :param epe: EPE matrix.
    :param valid_mask: Mask to denote pixels with valid gt.
    :return: EPE sum in the speed range.
             Number of pixels in the speed range.
    """
    mask = (speed >= lower) & (speed < upper)
    mask = mask * valid_mask
    count = np.sum(mask)
    epe_sum = np.sum(epe * mask)
    return epe_sum, count


def calculate_aepe_stats(gt, flow, valid_mask=None, percent_error=False):
    """
    Computes the AEPE between gt and flow.
    :param gt: Numpy array.
    :param flow: Numpy array. Same shape as gt.
    :param valid_mask: Optional mask to denote pixels with valid gt. Values are either 0.0 or 1.0.
        All shape dimensions should be the same as gt except the last one, which should be 1.
    :param percent_error: Bool. Whether the EPE is expressed as an error percentage.
    :return: Total AEPE.
             (EPE sum, count) of speeds 0-10.
             (EPE sum, count) of speeds 10-40.
             (EPE sum, count) of speeds 40+.
    """
    if valid_mask is None:
        valid_shape = list(gt.shape)
        valid_shape[-1] = 1
        valid_mask = np.ones(shape=valid_shape, dtype=np.float32)

    speed = np.linalg.norm(gt, axis=-1, keepdims=True)
    epe = np.linalg.norm(gt - flow, axis=-1, keepdims=True)
    if percent_error:
        # The minimum speed to compare to is 0.5. Anything within half a pixel is considered correct.
        epe = epe / np.maximum(speed, 0.5)

    low_speed = _epe_speed_range(0.0, 10.0, speed, epe, valid_mask)
    medium_speed = _epe_speed_range(10.0, 40.0, speed, epe, valid_mask)
    high_speed = _epe_speed_range(40.0, np.inf, speed, epe, valid_mask)
    aepe_all = np.sum(epe * valid_mask) / (np.sum(valid_mask) + 1e-10)

    return aepe_all, low_speed, medium_speed, high_speed
