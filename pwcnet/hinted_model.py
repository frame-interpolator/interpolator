import numpy as np
import os
import shutil
import tensorflow as tf
from common.models import FrozenModel
from common.utils.tf import he_normal
from common.vector_max_pool.vector_max_pool import vector_max_pool
from pwcnet.estimator_network.hinted_model import HintedEstimatorNetwork
from pwcnet.model import PWCNet
from tensorflow.contrib.layers import l2_regularizer


VERBOSE = False


class HintedPWCNet(PWCNet):
    def __init__(self, name='hinted_pwc_net', regularizer=l2_regularizer(4e-4), flow_scaling=0.05, search_range=4,
                 kernel_initializer=he_normal(), residual=True, use_flcon=True,
                 bilateral_range_stddev=0.5, norm=None, deeper_feature_pyramid=True,
                 bilateral_hint_splat_range=0.5, residual_hints=True, upres=False):
        """
        :param name: Str.
        :param regularizer: Tf regularizer.
        :param flow_layer_loss_weights: List of floats. Corresponds to the weight of a loss for a flow at some layer.
                                        i.e. flow_layer_loss_weights[0] corresponds to previous_flows[0].
        :param flow_scaling: In the PWC-Net paper, ground truth is scaled by this amount to normalize the flows.
        :param search_range: The search range to use for the cost volume layer.
        :param kernel_initializer: Tensorflow kernel initializer.
        :param residual: Bool. Whether the estimator networks should be residual.
        :param use_flcon: Bool.
        :param bilateral_range_stddev: Float. Bilateral filter range/value standard deviation.
                                       No filter if less than 0. Units are in pixels.
        :param norm: Str. None, ConvNetwork.Norm.BATCH_NORM, or ConvNetwork.Norm.GROUP_NORM.
        :param deeper_feature_pyramid: Bool. Whether to use a deeper feature pyramid architecture.
        :param bilateral_hint_splat_range: Float, If negative, hints will not be splatted. Otherwise, this value
            determines how much to splat the hint over a flow edge. Use a large value (i.e. 1e3) to ignore edges.
        :param residual_hints: Bool. Whether to feed in residual hints (difference between predicted and expected) or
            the "ground truth" hint directly.
        :param upres: Bool. Whether to use an upresing context network instead of the context network.
        """
        super().__init__(name=name, regularizer=regularizer, flow_scaling=flow_scaling, search_range=search_range,
                         kernel_initializer=kernel_initializer, residual=residual, use_flcon=use_flcon,
                         bilateral_range_stddev=bilateral_range_stddev, norm=norm,
                         deeper_feature_pyramid=deeper_feature_pyramid, upres=upres)

        self.estimator_networks = [HintedEstimatorNetwork(name='estimator_network_' + str(i),
                                                          regularizer=self.regularizer,
                                                          search_range=search_range,
                                                          kernel_initializer=self.kernel_initializer,
                                                          level=i - 1, parent=self,
                                                          residual=self.residual,
                                                          bilateral_hint_splat_range=bilateral_hint_splat_range,
                                                          residual_hints=residual_hints)
                                   for i in self.iter_range]

        # Full resolution hint.
        self.flow_hint = None
        # Full resolution hint mask. Corresponds to values in self.flow_hint.
        self.flow_hint_mask = None
        # Max pooled hints and the corresponding masks. Indices correspond to the estimator's 'level'.
        # Value is a pair of [hint, and hint_mask].
        self.flow_hints_and_masks = None

    def _create_max_pool_flow_hints(self):
        """
        Does max pooling on flow_hint and stores it in flow_hints.
        :return: Nothing.
        """
        self.flow_hints_and_masks = []
        curr_hint = self.flow_hint
        curr_hint_mask = self.flow_hint_mask
        for i in range(self.NUM_FEATURE_LEVELS):
            pooled_hint = vector_max_pool(curr_hint)
            pooled_mask = tf.layers.max_pooling2d(curr_hint_mask, 2, 2)
            self.flow_hints_and_masks.append([pooled_hint, pooled_mask])
            curr_hint = pooled_hint
            curr_hint_mask = pooled_mask

    def set_flow_hint(self, hint, hint_mask):
        """
        Sets the hint. This function must be called before get_forward().
        :param hint: Flow tensor of shape [batch_size, H, W, 3]. Units are in pixels.
        :param hint_mask: Mask describing where hints are defined. Shape is [batch_size, H, W, 1]. If training, this
            can be left as None. Values should be either 0 or 1.
        :param training: Bool. If training, then it is assumed that hint is the ground truth. This will apply some
            additional graph nodes to randomize the hint.
        :param hint_dropout_rate: Float. Rate of dropping out the hint during training. If 0.5, then 50% of the time
            there will be no hint.
        :param num_hints: Int. Number of hints to sample during training.
        :param hint_noise_std_dev: Float. Standard deviation of the hint noise. Units are a percentage of the hint
            magnitude. Used for training.
        :return: Nothing.
        """
        with tf.name_scope(self.name + '_hint'):
            # Apply flow scaling to the hint.
            hint = hint * self.flow_scaling
            self.flow_hint = hint
            self.flow_hint_mask = hint_mask
            self._create_max_pool_flow_hints()

    @staticmethod
    def create_frozen_model(directory):
        """
        Creates an immutable model that can't be trained.
        :param directory: Str. Directory of the saved/frozen model.
        :return: FrozenModel
        """
        return HintedPWCNet._HintedFrozenModelImpl(directory)

    class _HintedFrozenModelImpl(FrozenModel):
        def __init__(self, directory):
            super().__init__(directory)
            self.inputs = None
            self.outputs = None

        def freeze(self, model, session):
            """
            Overridden.
            """
            assert isinstance(model, HintedPWCNet)
            assert isinstance(session, tf.Session)
            if self.inputs is None or self.outputs is None:
                hints_ph = tf.placeholder(shape=[None, None, None, 2], dtype=tf.float32)
                hint_masks_ph = tf.placeholder(shape=[None, None, None, 1], dtype=tf.float32)
                model.set_flow_hint(hints_ph, hint_masks_ph)
                with tf.variable_scope(model.enclosing_scope):
                    images_0_ph, images_1_ph, flow_tensor, extras = model.init_forward_with_placeholders(training=False)
                self.inputs = {'images_0': images_0_ph, 'images_1': images_1_ph,
                               'hints': hints_ph, 'hint_masks': hint_masks_ph}
                self.outputs = {'output': flow_tensor}
                # Add extras.
                for i, feature in enumerate(extras['features']):
                    self.outputs['feature_' + str(i)] = feature
            if os.path.exists(self.directory) and len(os.listdir(self.directory)) > 0:
                shutil.rmtree(self.directory)
            tf.saved_model.simple_save(session, self.directory, self.inputs, self.outputs)
            print('Saved frozen model at:', self.directory)

        def run(self, inputs):
            """
            Overridden.
            :param inputs: Tuple of 2 or 4 elements:
                    Numpy array. Image of shape [B, H, W, C]. The image at t = 0.
                    Numpy array. Image of shape [B, H, W, C]. The image at t = 1.
                    Numpy array. Flow hints of shape [B, H, W, 2]. Optional.
                    Numpy array. Flow hint masks of shape [B, H, W, 1]. Optional.
            :return: Numpy array of shape [B, H, W, 2].
                     extras: A dictionary of extra outputs including feature pyramid features.
            """
            assert self.predictor is not None, 'load() must be called before run().'
            assert isinstance(inputs, tuple), 'inputs must be a tuple.'
            assert len(inputs) == 2 or len(inputs) == 4, 'inputs must have either 2 or 4 elements.'
            images_0 = np.asarray(inputs[0])
            images_1 = inputs[1]
            if len(inputs) == 4:
                hints = inputs[2]
                hint_masks = inputs[3]
            else:
                batch, height, width = images_0.shape[0], images_0.shape[1], images_0.shape[2]
                hints = np.zeros((batch, height, width, 2), dtype=np.float32)
                hint_masks = np.zeros((batch, height, width, 1), dtype=np.float32)
            outputs = self.predictor({
                'images_0': images_0,
                'images_1': images_1,
                'hints': hints,
                'hint_masks': hint_masks
            })
            extras = {'features': []}
            for i in range(PWCNet.NUM_FEATURE_LEVELS):
                key = 'feature_' + str(i)
                if key in outputs:
                    extras['features'].append(outputs[key])
            return outputs['output'], extras
