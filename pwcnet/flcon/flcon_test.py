import numpy as np
import tensorflow as tf
import unittest
from pwcnet.flcon.flcon import FLocalConv
from tensorflow.contrib.layers import l2_regularizer


class TestSpacialTransformTranslate(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

        self.flcon = FLocalConv(name='flcon_test', regularizer=l2_regularizer(4e-4))

    def test_network(self):
        H = 16
        W = 16
        F = 8
        flow_tensor = tf.placeholder(shape=[1, H, W, 2], dtype=tf.float32)
        image_a_features_tensor = tf.placeholder(shape=[1, H, W, F], dtype=tf.float32)
        image_a_tensor = tf.placeholder(shape=[1, H * 2, W * 2, 3], dtype=tf.float32)
        image_b_tensor = tf.placeholder(shape=[1, H * 2, W * 2, 3], dtype=tf.float32)
        filtered_flow_tensor = self.flcon.get_forward(flow_tensor, image_a_features_tensor, image_a_tensor,
                                                      image_b_tensor)

        self.sess.run(tf.global_variables_initializer())

        # Create numpy inputs.
        flow = np.ones(shape=[1, H, W, 2], dtype=np.float32)
        image_a_features = np.ones(shape=[1, H, W, F], dtype=np.float32)
        image_a = np.ones(shape=[1, H * 2, W * 2, 3], dtype=np.float32)
        image_b = np.ones(shape=[1, H * 2, W * 2, 3], dtype=np.float32)

        filtered_flow = self.sess.run(filtered_flow_tensor, feed_dict={
            flow_tensor: flow,
            image_a_features_tensor: image_a_features,
            image_a_tensor: image_a,
            image_b_tensor: image_b
        })

        self.assertTupleEqual((1, H, W, 2), filtered_flow.shape)
        # Test that the normalization is working.
        self.assertAlmostEqual(1.0, np.mean(filtered_flow[:, 1:H-1, 1:W-1, :]))
        self.assertLess(np.mean(filtered_flow), 1.0)

        # There are 6 convolutions, so 6 losses.
        reg_losses = tf.losses.get_regularization_losses(scope=self.flcon.name)
        self.assertEqual(len(reg_losses), 6)

    def test_network_gradients(self):
        H = 16
        W = 16
        F = 8
        flow_tensor = tf.placeholder(shape=[1, H, W, 2], dtype=tf.float32)
        image_a_features_tensor = tf.placeholder(shape=[1, H, W, F], dtype=tf.float32)
        image_a_tensor = tf.placeholder(shape=[1, H, W, 3], dtype=tf.float32)
        image_b_tensor = tf.placeholder(shape=[1, H, W, 3], dtype=tf.float32)
        filtered_flow_tensor = self.flcon.get_forward(flow_tensor, image_a_features_tensor, image_a_tensor,
                                                      image_b_tensor)

        self.sess.run(tf.global_variables_initializer())

        # Create numpy inputs.
        flow = np.random.rand(1, H, W, 2)
        image_a_features = np.random.rand(1, H, W, F)
        image_a = np.random.rand(1, H, W, 3)
        image_b = np.random.rand(1, H, W, 3)

        trainable_vars = tf.trainable_variables(scope=self.flcon.name)

        # Check that the gradients are flowing.
        grad_op = tf.gradients(filtered_flow_tensor, trainable_vars +
                               [flow_tensor, image_a_features_tensor, image_a_tensor, image_b_tensor])
        for grad in grad_op:
            self.assertNotEqual(grad, None)
        grads = self.sess.run(grad_op, feed_dict={
            flow_tensor: flow,
            image_a_features_tensor: image_a_features,
            image_a_tensor: image_a,
            image_b_tensor: image_b
        })
        for grad in grads:
            self.assertNotAlmostEqual(0.0, np.sum(grad))


if __name__ == '__main__':
    unittest.main()
