import tensorflow as tf
from common.extract_patches.extract_patches import extract_patches
from common.models import ConvNetwork
from common.utils.tf import leaky_relu
from pwcnet.losses.loss import l2_diff
from pwcnet.warp.warp import backward_warp


class FLocalConv(ConvNetwork):
    def __init__(self, name='flcon', layer_specs=None, activation_fn=leaky_relu, regularizer=None,
                 kernel_initializer=None, lconv_kernel_size=3, flow_scaling=0.05):
        """
        Implements the flcon flow regularizer in https://arxiv.org/pdf/1805.07036.pdf.
        :param name: Str. For variable scoping.
        :param layer_specs: Array of shape [num_layers, 4].
                            The second dimension consists of [kernel_size, num_output_features, dilation, stride].
                            If None, the default will be used.
        :param activation_fn: Tensorflow activation function.
        :param regularizer: Tf regularizer such as tf.contrib.layers.l2_regularizer.
        :param kernel_initializer: Tensorflow initializer. Defaults to glorot (xavier) initialization if None.
        :param lconv_kernel_size: Int. Odd number. Diameter of the local convolution kernel.
        :param flow_scaling: Float. Scaling of the flow. I.e. flow / flow_scaling returns flow to full magnitude.
        """
        self.lconv_kernel_size = lconv_kernel_size
        self.flow_scaling = flow_scaling

        if layer_specs is None:
            # [kernel_size, num_output_features, dilation, stride].
            layer_specs = [[3, 128, 1, 1],
                           [3, 96, 1, 1],
                           [3, 64, 1, 1],
                           [3, 32, 1, 1]]

        super().__init__(name=name, layer_specs=layer_specs,
                         activation_fn=activation_fn, regularizer=regularizer, padding='SAME', dense_net=False,
                         kernel_initializer=kernel_initializer)

    def get_forward(self, flow, image_a_features, image_a, image_b, reuse_variables=tf.AUTO_REUSE, training=False):
        """
        :param flow: Flow tensor of shape [B, target_height, target_width, 2]. Flow from image_a to image_b.
                     Units are in (pixels * flow_scaling).
        :param image_a_features: Tensor of shape [B, target_height, target_width, C].
        :param image_a: Full size image_a of shape [B, H, W, 3].
        :param image_b: Full size image_a of shape [B, H, W, 3].
        :param reuse_variables: Tensorflow reuse option. i.e. tf.AUTO_REUSE.
        :param training: Python bool or Tensorflow bool. Switches between training and test mode (i.e. for batch norm).
        :return:
        """
        with tf.variable_scope(self.name, reuse=reuse_variables):
            with tf.name_scope('dimensions'):
                _, target_height, target_width, _ = tf.unstack(tf.shape(flow))
                _, image_height, image_width, _ = tf.unstack(tf.shape(image_a))
                size_ratio = tf.cast(target_height, dtype=tf.float32) / tf.cast(image_height, dtype=tf.float32)

            with tf.name_scope('resized_images'):
                image_a_resized = tf.image.resize_bilinear(image_a, [target_height, target_width],
                                                           align_corners=True)
                image_b_resized = tf.image.resize_bilinear(image_b, [target_height, target_width],
                                                           align_corners=True)

            with tf.name_scope('warp_diff'):
                unscaled_flow = flow * size_ratio / self.flow_scaling
                image_b_warped = backward_warp(image_b_resized, unscaled_flow)
                with tf.name_scope('norm'):
                    warp_diff = l2_diff(image_a_resized, image_b_warped)

            with tf.name_scope('flow_components'):
                # Flow x and y have shape [B, H, W, 1].
                flow_x = tf.expand_dims(flow[:, :, :, 0], axis=-1)
                flow_y = tf.expand_dims(flow[:, :, :, 1], axis=-1)
                flow_x_patches, flow_y_patches = self._get_flow_component_patches(flow_x, flow_y)

            with tf.name_scope('remove_mean'):
                flow_x_no_mean, flow_y_no_mean = self._remove_mean(flow_x, flow_y)

            with tf.name_scope('input_stack'):
                input_stack = tf.concat([flow_x_no_mean, flow_y_no_mean, image_a_features, warp_diff], axis=-1)

            with tf.variable_scope('conv_tower', reuse=reuse_variables):
                conv_out, _, _ = self._get_conv_tower(input_stack, training=training,
                                                      reuse_variables=reuse_variables)
                # lcon_kernels should have shape [B, H, W, self.lconv_kernel_size * self.lconv_kernel_size].
                # lcon_kernels_normalizer should have shape [B, H, W, 1].
                lcon_kernels, lcon_kernels_normalizer = self._create_lcon_kernels(conv_out)

            with tf.name_scope('local_conv'):
                local_conv_x = tf.reduce_sum(lcon_kernels * flow_x_patches, axis=-1, keepdims=True)
                local_conv_x = local_conv_x / lcon_kernels_normalizer
                local_conv_y = tf.reduce_sum(lcon_kernels * flow_y_patches, axis=-1, keepdims=True)
                local_conv_y = local_conv_y / lcon_kernels_normalizer
                filtered_flow = tf.concat([local_conv_x, local_conv_y], axis=-1)

            return filtered_flow

    def _remove_mean(self, flow_x, flow_y):
        """
        Subtracts the instance mean from the components separately.
        :param flow_x: Tensor of shape [B, H, W, 1].
        :param flow_y: Tensor of shape [B, H, W, 1].
        :return: Tensor of shape [B, H, W, 1], Tensor of shape [B, H, W, 1].
        """
        mean_x = tf.reduce_mean(flow_x, axis=[1, 2, 3], keepdims=True)
        mean_y = tf.reduce_mean(flow_y, axis=[1, 2, 3], keepdims=True)
        no_mean_x = flow_x - mean_x
        no_mean_y = flow_y - mean_y
        return no_mean_x, no_mean_y

    def _get_flow_component_patches(self, flow_x, flow_y):
        """
        :param flow_x: Tensor of shape [B, H, W, 1].
        :param flow_y: Tensor of shape [B, H, W, 1].
        :return: Tensor of shape [B, H, W, self.lconv_kernel_size * self.lconv_kernel_size] (x component),
                 Tensor of shape [B, H, W, self.lconv_kernel_size * self.lconv_kernel_size] (y component).
        """
        flow_x_patches = extract_patches(flow_x, kernel_size=self.lconv_kernel_size)
        flow_y_patches = extract_patches(flow_y, kernel_size=self.lconv_kernel_size)
        return flow_x_patches, flow_y_patches

    def _create_lcon_kernels(self, conv_out):
        """
        As seen in the official implementation, a vertical kernel and a horizontal kernel are applied separately.
        There is no bias nor activation on the convolutions.
        :param conv_out: Tensor of shape [B, H, W, C]. Output of the conv tower.
        :return: Tensor of shape [B, H, W, self.lconv_kernel_size * self.lconv_kernel_size] (softmax numerator),
                 Tensor of shape [B, H, W, 1] (softmax denominator).
        """
        lcon_kernels = tf.layers.conv2d(inputs=conv_out,
                                        filters=self.lconv_kernel_size * self.lconv_kernel_size,
                                        kernel_size=[self.lconv_kernel_size, 1],
                                        strides=(1, 1), padding='SAME', activation=None,
                                        kernel_regularizer=self.regularizer,
                                        kernel_initializer=self.kernel_initializer,
                                        use_bias=False, name='ver')
        lcon_kernels = tf.layers.conv2d(inputs=lcon_kernels,
                                        filters=self.lconv_kernel_size * self.lconv_kernel_size,
                                        kernel_size=[1, self.lconv_kernel_size],
                                        strides=(1, 1), padding='SAME', activation=None,
                                        kernel_regularizer=self.regularizer,
                                        kernel_initializer=self.kernel_initializer,
                                        use_bias=False, name='hor')

        with tf.name_scope('normalize'):
            lcon_kernels = tf.exp(-tf.square(lcon_kernels))
            lcon_kernels_normalizer = tf.reduce_sum(lcon_kernels, axis=-1, keepdims=True) + 1e-10

        return lcon_kernels, lcon_kernels_normalizer
