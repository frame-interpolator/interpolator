import numpy as np
import tensorflow as tf
import unittest
from pwcnet.losses.loss import create_multi_level_loss, create_multi_level_unflow_loss, create_training_hints


class TestPWCNetLosses(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_multi_level_loss(self):
        """
        Sets up the network's forward pass and ensures that all shapes are expected.
        """
        height = 2
        width = 2
        batch_size = 2
        scaling = 2.0

        # Create the graph.
        expected_tensor = tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32)
        flow_tensors = [tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32),
                        tf.placeholder(shape=[None, height / 2, width / 2, 2], dtype=tf.float32)]
        flow_layer_loss_weights = [1.0, 0.5]

        expected = np.ones(shape=[batch_size, height, width, 2], dtype=np.float32)
        flows = [np.ones(shape=[batch_size, height, width, 2], dtype=np.float32) * scaling,
                 np.ones(shape=[batch_size, int(height / 2), int(width / 2), 2], dtype=np.float32) * scaling]
        flows[0][:, 0, 0, 0] = 0.0
        flows[1][:, 0, 0, 1] = 0.0

        total_loss, layer_losses = create_multi_level_loss(
            expected_tensor, flow_tensors, scaling, flow_layer_loss_weights)

        # Test loss values.
        results = self.sess.run([total_loss] + layer_losses, feed_dict={expected_tensor: expected,
                                                                        flow_tensors[0]: flows[0],
                                                                        flow_tensors[1]: flows[1]})
        self.assertEqual(3, len(results))
        self.assertAlmostEqual(1.5, results[0], places=4)
        self.assertAlmostEqual(0.5, results[1], places=4)
        self.assertAlmostEqual(1.0, results[2], places=4)

        # Test gradients.
        grad_ops = tf.gradients(total_loss, [expected_tensor, flow_tensors[0], flow_tensors[1]])
        for grad_op in grad_ops:
            self.assertNotEqual(None, grad_op)
        grads = self.sess.run(grad_ops, feed_dict={expected_tensor: expected,
                                                   flow_tensors[0]: flows[0],
                                                   flow_tensors[1]: flows[1]})
        self.assertTrue(np.allclose(np.asarray([[[[[0.25, 0.125], [0., 0.125]],
                                                  [[0., 0.125], [0., 0.125]]],
                                                 [[[0.25, 0.125], [0., 0.125]],
                                                  [[0., 0.125], [0., 0.125]]]]]), grads[0]))
        self.assertTrue(np.allclose(np.asarray([[[[[-0.125, 0.], [0., 0.]],
                                                  [[0., 0.], [0., 0.]]],
                                                 [[[-0.125, 0.], [0., 0.]],
                                                  [[0., 0.], [0., 0.]]]]]), grads[1]))
        self.assertTrue(np.allclose(np.asarray([[[[0., -0.25]]],
                                                [[[0., -0.25]]]]), grads[2]))

    def test_multi_level_loss_mask(self):
        """
        Sets up the network's forward pass and ensures that all shapes are expected.
        """
        height = 2
        width = 2
        batch_size = 2
        scaling = 2.0

        # Create the graph.
        expected_tensor = tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32)
        flow_tensors = [tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32),
                        tf.placeholder(shape=[None, height / 2, width / 2, 2], dtype=tf.float32)]
        flow_layer_loss_weights = [1.0, 0.5]

        expected = np.ones(shape=[batch_size, height, width, 2], dtype=np.float32)
        flows = [np.ones(shape=[batch_size, height, width, 2], dtype=np.float32) * scaling,
                 np.ones(shape=[batch_size, int(height / 2), int(width / 2), 2], dtype=np.float32) * scaling]
        flows[0][:, 0, 0, 0] = 0.0
        flows[1][:, 0, 0, 1] = 0.0
        loss_mask = np.zeros(shape=[batch_size, height, width, 1], dtype=np.float32)
        loss_mask_tensor = tf.constant(loss_mask, dtype=tf.float32)

        total_loss, layer_losses = create_multi_level_loss(
            expected_tensor, flow_tensors, scaling, flow_layer_loss_weights, loss_mask=loss_mask_tensor)

        # Test loss values.
        results = self.sess.run([total_loss] + layer_losses, feed_dict={expected_tensor: expected,
                                                                        flow_tensors[0]: flows[0],
                                                                        flow_tensors[1]: flows[1]})
        self.assertEqual(3, len(results))
        self.assertAlmostEqual(0.0, results[0], places=4)
        self.assertAlmostEqual(0.0, results[1], places=4)
        self.assertAlmostEqual(0.0, results[2], places=4)

    def test_multi_level_loss_mask_identity(self):
        """
        Sets up the network's forward pass and ensures that all shapes are expected.
        """
        height = 2
        width = 2
        batch_size = 2
        scaling = 2.0

        # Create the graph.
        expected_tensor = tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32)
        flow_tensors = [tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32),
                        tf.placeholder(shape=[None, height / 2, width / 2, 2], dtype=tf.float32)]
        flow_layer_loss_weights = [1.0, 0.5]

        expected = np.ones(shape=[batch_size, height, width, 2], dtype=np.float32)
        flows = [np.ones(shape=[batch_size, height, width, 2], dtype=np.float32) * scaling,
                 np.ones(shape=[batch_size, int(height / 2), int(width / 2), 2], dtype=np.float32) * scaling]
        flows[0][:, 0, 0, 0] = 0.0
        flows[1][:, 0, 0, 1] = 0.0
        loss_mask = np.ones(shape=[batch_size, height, width, 1], dtype=np.float32)
        loss_mask_tensor = tf.constant(loss_mask, dtype=tf.float32)

        total_loss, layer_losses = create_multi_level_loss(
            expected_tensor, flow_tensors, scaling, flow_layer_loss_weights, loss_mask=loss_mask_tensor)

        # Test loss values.
        results = self.sess.run([total_loss] + layer_losses, feed_dict={expected_tensor: expected,
                                                                        flow_tensors[0]: flows[0],
                                                                        flow_tensors[1]: flows[1]})
        self.assertEqual(3, len(results))
        self.assertAlmostEqual(1.5, results[0], places=4)
        self.assertAlmostEqual(0.5, results[1], places=4)
        self.assertAlmostEqual(1.0, results[2], places=4)

    def test_multi_level_unflow_loss(self):
        height = 8
        width = 8
        batch_size = 2
        scaling = 0.01

        # Create the graph.
        image_a_tensor = tf.placeholder(shape=[None, height, width, 3], dtype=tf.float32)
        image_b_tensor = tf.placeholder(shape=[None, height, width, 3], dtype=tf.float32)
        forward_flow_tensors = [tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32),
                                tf.placeholder(shape=[None, height / 2, width / 2, 2], dtype=tf.float32)]
        backward_flow_tensors = [tf.placeholder(shape=[None, height, width, 2], dtype=tf.float32),
                                 tf.placeholder(shape=[None, height / 2, width / 2, 2], dtype=tf.float32)]
        flow_layer_loss_weights = [1.0, 0.5]
        layer_patch_distances = [2, 1]

        total_loss, layer_losses, _, _, layer_losses_detailed = create_multi_level_unflow_loss(
            image_a_tensor, image_b_tensor, forward_flow_tensors, backward_flow_tensors, scaling,
            flow_layer_loss_weights, layer_patch_distances)

        self.assertEqual(2, len(layer_losses_detailed))
        self.assertEqual(4, len(layer_losses_detailed[0].keys()))
        self.assertEqual(4, len(layer_losses_detailed[1].keys()))

        image_a = np.ones(shape=[batch_size, height, width, 3], dtype=np.float32)
        image_b = np.ones(shape=[batch_size, height, width, 3], dtype=np.float32)
        image_a[:, :, 0, :] = 0.1
        image_b[:, 0, :, :] = 0.1
        forward_flows = [np.ones(shape=[batch_size, height, width, 2], dtype=np.float32) * scaling,
                         np.ones(shape=[batch_size, int(height / 2), int(width / 2), 2], dtype=np.float32) * scaling]
        backward_flows = [-np.ones(shape=[batch_size, height, width, 2], dtype=np.float32) * scaling * 0.5,
                          -np.ones(shape=[batch_size, int(height / 2), int(width / 2), 2], dtype=np.float32) * scaling]
        forward_flows[0][:, :2, 0, 0] = 0.1
        forward_flows[1][:, 0, 1:, 1] = 0.05
        backward_flows[0][:, 0, :3, 0] = 0.03
        backward_flows[1][:, :1, 0, 1] = 0.1
        feed_dict = {
            image_a_tensor: image_a, image_b_tensor: image_b,
            forward_flow_tensors[0]: forward_flows[0], forward_flow_tensors[1]: forward_flows[1],
            backward_flow_tensors[0]: backward_flows[0], backward_flow_tensors[1]: backward_flows[1]
        }

        # Test loss values.
        results = self.sess.run([total_loss] + layer_losses, feed_dict=feed_dict)
        self.assertEqual(3, len(results))
        self.assertEqual(results[0], results[1] + results[2])

        # Test gradients.
        grad_ops = tf.gradients(total_loss, [forward_flow_tensors[0], forward_flow_tensors[1],
                                             backward_flow_tensors[0], backward_flow_tensors[1],
                                             image_a_tensor, image_b_tensor])
        for grad_op in grad_ops:
            self.assertNotEqual(None, grad_op)
        grads = self.sess.run(grad_ops, feed_dict=feed_dict)
        for grad in grads:
            self.assertGreater(np.sum(np.abs(grad)), 0.0)


class TestHintedLosses(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_no_keep_mask_perfect_estimate(self):
        B = 1
        H = 8
        W = 16
        C = 2
        last_flow_unscaled = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        gt_resized = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        flows = tf.ones(shape=[B, H, W, C], dtype=tf.float32)
        valid = tf.ones(shape=[B, H, W, 1], dtype=tf.float32)
        keep_mask = None
        flow_error_hint_thresh = 0.05
        num_hints = tf.constant(10, dtype=tf.int32)
        hint_noise_std_dev = 0.05
        keep_mask, hints, loss_mask, flow_error = create_training_hints(last_flow_unscaled, gt_resized, flows,
                                                                        keep_mask, flow_error_hint_thresh, num_hints,
                                                                        hint_noise_std_dev, valid)
        keep_mask, hints, loss_mask, flow_error = self.sess.run([keep_mask, hints, loss_mask, flow_error])
        self.assertTupleEqual((B, H, W, 1), keep_mask.shape)
        self.assertTupleEqual((B, H, W, 2), hints.shape)
        self.assertTupleEqual((B, H, W, 1), loss_mask.shape)
        self.assertAlmostEqual(0.0, float(np.sum(keep_mask)))
        self.assertAlmostEqual(0.0, float(np.sum(hints)))
        self.assertAlmostEqual(0.0, float(np.sum(flow_error)), places=2)
        self.assertAlmostEqual(0.0, float(np.sum(loss_mask)))

    def test_no_keep_mask_perfect_estimate_probablistic_sampling(self):
        B = 1
        H = 8
        W = 16
        C = 2
        last_flow_unscaled = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        gt_resized = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        flows = tf.ones(shape=[B, H, W, C], dtype=tf.float32)
        valid = tf.ones(shape=[B, H, W, 1], dtype=tf.float32)
        keep_mask = None
        flow_error_hint_thresh = 0.05
        num_hints = tf.constant(10, dtype=tf.int32)
        hint_noise_std_dev = 0.05
        keep_mask, hints, loss_mask, flow_error = create_training_hints(last_flow_unscaled, gt_resized, flows,
                                                                        keep_mask, flow_error_hint_thresh, num_hints,
                                                                        hint_noise_std_dev, valid,
                                                                        probablistic_sampling=True)
        keep_mask, hints, loss_mask, flow_error = self.sess.run([keep_mask, hints, loss_mask, flow_error])
        self.assertTupleEqual((B, H, W, 1), keep_mask.shape)
        self.assertTupleEqual((B, H, W, 2), hints.shape)
        self.assertTupleEqual((B, H, W, 1), loss_mask.shape)
        self.assertAlmostEqual(0.0, float(np.sum(flow_error)), places=2)
        self.assertAlmostEqual(0.0, float(np.sum(loss_mask)))
        self.assertAlmostEqual(0.0, float(np.sum(keep_mask)))
        self.assertAlmostEqual(0.0, float(np.sum(hints)))

    def test_with_keep_mask_perfect_estimate(self):
        B = 1
        H = 8
        W = 16
        C = 2
        last_flow_unscaled = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        gt_resized = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        flows = tf.ones(shape=[B, H, W, C], dtype=tf.float32)
        valid = tf.ones(shape=[B, H, W, 1], dtype=tf.float32)
        keep_mask = np.zeros((B, H, W, 1), dtype=np.float32)
        keep_mask[0, 0, 0, 0] = 1.0
        keep_mask = tf.constant(keep_mask, dtype=tf.float32)
        flow_error_hint_thresh = 0.05
        num_hints = tf.constant(10, dtype=tf.int32)
        hint_noise_std_dev = 0.05
        keep_mask, hints, loss_mask, _ = create_training_hints(last_flow_unscaled, gt_resized, flows, keep_mask,
                                                               flow_error_hint_thresh, num_hints, hint_noise_std_dev,
                                                               valid)
        keep_mask, hints, loss_mask = self.sess.run([keep_mask, hints, loss_mask])
        self.assertTupleEqual((B, H, W, 1), keep_mask.shape)
        self.assertTupleEqual((B, H, W, 2), hints.shape)
        self.assertTupleEqual((B, H, W, 1), loss_mask.shape)
        self.assertAlmostEqual(1.0, float(np.sum(keep_mask)))
        self.assertAlmostEqual(2.0, float(np.sum(hints)), delta=0.2)
        self.assertNotAlmostEqual(2.0, float(np.sum(hints)))
        self.assertNotAlmostEqual(0.0, float(np.sum(loss_mask)))

    def test_with_keep_mask_perfect_estimate_probablistic_sampling(self):
        B = 1
        H = 8
        W = 16
        C = 2
        last_flow_unscaled = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        gt_resized = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        flows = tf.ones(shape=[B, H, W, C], dtype=tf.float32)
        valid = tf.ones(shape=[B, H, W, 1], dtype=tf.float32)
        keep_mask = np.zeros((B, H, W, 1), dtype=np.float32)
        keep_mask[0, 0, 0, 0] = 1.0
        keep_mask = tf.constant(keep_mask, dtype=tf.float32)
        flow_error_hint_thresh = 0.05
        num_hints = tf.constant(10, dtype=tf.int32)
        hint_noise_std_dev = 0.0
        keep_mask, hints, loss_mask, _ = create_training_hints(last_flow_unscaled, gt_resized, flows, keep_mask,
                                                               flow_error_hint_thresh, num_hints, hint_noise_std_dev,
                                                               valid, probablistic_sampling=True)
        keep_mask, hints, loss_mask = self.sess.run([keep_mask, hints, loss_mask])
        self.assertTupleEqual((B, H, W, 1), keep_mask.shape)
        self.assertTupleEqual((B, H, W, 2), hints.shape)
        self.assertTupleEqual((B, H, W, 1), loss_mask.shape)
        self.assertAlmostEqual(1.0, float(np.sum(keep_mask)))
        self.assertAlmostEqual(2.0, float(np.sum(hints)))
        self.assertNotAlmostEqual(0.0, float(np.sum(loss_mask)))

    def test_no_keep_mask_one_error(self):
        B = 1
        H = 64
        W = 64
        C = 2
        last_flow_unscaled = np.ones((B, int(H / 4), int(W / 4), C), dtype=np.float32)
        last_flow_unscaled[0, 8, 8, 0] = 0.0
        last_flow_unscaled = tf.constant(last_flow_unscaled, dtype=tf.float32)
        gt_resized = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        flows = tf.ones(shape=[B, H, W, C], dtype=tf.float32)
        valid = tf.ones(shape=[B, H, W, 1], dtype=tf.float32)
        keep_mask = None
        flow_error_hint_thresh = 0.5
        num_hints = tf.constant(7, dtype=tf.int32)
        hint_noise_std_dev = 0.0
        keep_mask, hints, loss_mask, _ = create_training_hints(last_flow_unscaled, gt_resized, flows, keep_mask,
                                                               flow_error_hint_thresh, num_hints, hint_noise_std_dev,
                                                               valid)
        keep_mask, hints, loss_mask = self.sess.run([keep_mask, hints, loss_mask])
        self.assertTupleEqual((B, H, W, 1), keep_mask.shape)
        self.assertTupleEqual((B, H, W, 2), hints.shape)
        self.assertTupleEqual((B, H, W, 1), loss_mask.shape)
        self.assertAlmostEqual(7.0, float(np.sum(keep_mask)))
        self.assertAlmostEqual(14.0, float(np.sum(hints)))
        self.assertNotAlmostEqual(0.0, float(np.sum(loss_mask)))

    def test_no_keep_mask_one_error_probablistic_sampling(self):
        B = 1
        H = 32
        W = 32
        C = 2
        last_flow_unscaled = np.ones((B, int(H / 4), int(W / 4), C), dtype=np.float32)
        last_flow_unscaled[0, 3, 3, 0] = 0.0
        last_flow_unscaled = tf.constant(last_flow_unscaled, dtype=tf.float32)
        gt_resized = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        flows = tf.ones(shape=[B, H, W, C], dtype=tf.float32)
        valid = tf.ones(shape=[B, H, W, 1], dtype=tf.float32)
        keep_mask = None
        flow_error_hint_thresh = 0.5
        num_hints = tf.constant(16, dtype=tf.int32)
        hint_noise_std_dev = 0.0
        keep_mask, hints, loss_mask, _ = create_training_hints(last_flow_unscaled, gt_resized, flows, keep_mask,
                                                               flow_error_hint_thresh, num_hints, hint_noise_std_dev,
                                                               valid, probablistic_sampling=True)
        keep_mask, hints, loss_mask = self.sess.run([keep_mask, hints, loss_mask])
        self.assertTupleEqual((B, H, W, 1), keep_mask.shape)
        self.assertTupleEqual((B, H, W, 2), hints.shape)
        self.assertTupleEqual((B, H, W, 1), loss_mask.shape)
        num_hints = np.sum(keep_mask)
        self.assertGreaterEqual(16.0, num_hints)
        self.assertAlmostEqual(num_hints * 2, float(np.sum(hints)))
        self.assertNotAlmostEqual(0.0, float(np.sum(loss_mask)))

    def test_errors(self):
        B = 1
        H = 16
        W = 16
        C = 2
        last_flow_unscaled = np.zeros((B, int(H / 4), int(W / 4), C), dtype=np.float32)
        last_flow_unscaled[0, 0, 0, 0] = 1.0
        last_flow_unscaled = tf.constant(last_flow_unscaled, dtype=tf.float32)
        gt_resized = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        flows = tf.ones(shape=[B, H, W, C], dtype=tf.float32)
        valid = tf.ones(shape=[B, H, W, 1], dtype=tf.float32)
        keep_mask = np.zeros((B, H, W, 1), dtype=np.float32)
        keep_mask[0, 0, 0, 0] = 1.0
        keep_mask = tf.constant(keep_mask, dtype=tf.float32)
        flow_error_hint_thresh = 0.5
        num_hints = tf.constant(7, dtype=tf.int32)
        hint_noise_std_dev = 0.0
        keep_mask, hints, loss_mask, _ = create_training_hints(last_flow_unscaled, gt_resized, flows, keep_mask,
                                                               flow_error_hint_thresh, num_hints, hint_noise_std_dev,
                                                               valid)
        keep_mask, hints, loss_mask = self.sess.run([keep_mask, hints, loss_mask])
        self.assertTupleEqual((B, H, W, 1), keep_mask.shape)
        self.assertTupleEqual((B, H, W, 2), hints.shape)
        self.assertTupleEqual((B, H, W, 1), loss_mask.shape)
        self.assertAlmostEqual(8.0, float(np.sum(keep_mask)))
        self.assertAlmostEqual(16.0, float(np.sum(hints)))
        self.assertNotAlmostEqual(0.0, float(np.sum(loss_mask)))

    def test_valid_mask(self):
        B = 1
        H = 16
        W = 16
        C = 2
        last_flow_unscaled = tf.zeros(shape=[B, int(H / 4), int(W / 4), C], dtype=tf.float32)
        gt_resized = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        flows = tf.ones(shape=[B, H, W, C], dtype=tf.float32)
        valid = np.ones((B, H, W, 1), dtype=np.float32)
        valid[:, :int(H/2), ...] = 0.0
        valid = tf.constant(valid, dtype=tf.float32)
        keep_mask = None
        flow_error_hint_thresh = 0.5
        num_hints = tf.constant(7, dtype=tf.int32)
        hint_noise_std_dev = 0.0
        keep_mask, hints, loss_mask, _ = create_training_hints(last_flow_unscaled, gt_resized, flows, keep_mask,
                                                               flow_error_hint_thresh, num_hints, hint_noise_std_dev,
                                                               valid)
        keep_mask, hints, loss_mask = self.sess.run([keep_mask, hints, loss_mask])
        self.assertTupleEqual((B, H, W, 1), keep_mask.shape)
        self.assertTupleEqual((B, H, W, 2), hints.shape)
        self.assertTupleEqual((B, H, W, 1), loss_mask.shape)
        self.assertAlmostEqual(7.0, float(np.sum(keep_mask)))
        self.assertAlmostEqual(14.0, float(np.sum(hints)))
        self.assertNotAlmostEqual(0.0, float(np.sum(loss_mask)))
        self.assertAlmostEqual(0.0, float(np.sum(keep_mask[:, :int(H/2), ...])))

    def test_valid_mask_probablistic(self):
        B = 1
        H = 16
        W = 16
        C = 2
        last_flow_unscaled = tf.zeros(shape=[B, int(H / 4), int(W / 4), C], dtype=tf.float32)
        gt_resized = tf.ones(shape=[B, H / 4, W / 4, C], dtype=tf.float32)
        flows = tf.ones(shape=[B, H, W, C], dtype=tf.float32)
        valid = np.ones((B, H, W, 1), dtype=np.float32)
        valid[:, :int(H/2), ...] = 0.0
        valid = tf.constant(valid, dtype=tf.float32)
        keep_mask = None
        flow_error_hint_thresh = 0.5
        num_hints = tf.constant(7, dtype=tf.int32)
        hint_noise_std_dev = 0.0
        keep_mask, hints, loss_mask, _ = create_training_hints(last_flow_unscaled, gt_resized, flows, keep_mask,
                                                               flow_error_hint_thresh, num_hints, hint_noise_std_dev,
                                                               valid, probablistic_sampling=True)
        keep_mask, hints, loss_mask = self.sess.run([keep_mask, hints, loss_mask])
        self.assertTupleEqual((B, H, W, 1), keep_mask.shape)
        self.assertTupleEqual((B, H, W, 2), hints.shape)
        self.assertTupleEqual((B, H, W, 1), loss_mask.shape)
        self.assertGreater(float(np.sum(keep_mask)), 0.0)
        self.assertLessEqual(float(np.sum(hints)), 14.0)
        self.assertNotAlmostEqual(0.0, float(np.sum(loss_mask)))
        self.assertAlmostEqual(0.0, float(np.sum(keep_mask[:, :int(H/2), ...])))
        self.assertAlmostEqual(float(np.sum(keep_mask)), float(np.sum(keep_mask[:, int(H / 2):, ...])))


if __name__ == '__main__':
    unittest.main()
