import tensorflow as tf
from common.utils.tf import convolve_with_gaussian, norm, sample_n_from_mask, sample_n_from_weights
from pwcnet.losses.unflow import compute_losses
from pwcnet.downsample.downsample import downsample


def l2_diff(a, b):
    """
    Calculates the L2 norm of the difference between a and b.
    :param a: Tensor.
    :param b: Tensor.
    :return: Tensor.
    """
    return norm(a - b, epsilon=1e-10)


def lq_diff(a, b, q=0.4, epsilon=0.01):
    """
    Calculates the diffing scheme mentioned in the "robust training loss" from https://arxiv.org/pdf/1709.02371.pdf.
    :param a: Tensor
    :param b: Tensor
    :param q: Number or scalar tensor.
    :param epsilon: Number or scalar tensor.
    :return: Tensor.
    """
    l1_norm = tf.reduce_sum(tf.abs(a - b), axis=-1, keepdims=True)
    return tf.pow(l1_norm + epsilon, q)


def create_multi_level_loss(expected_flow, flows, flow_scaling, flow_layer_loss_weights=None, diff_fn=l2_diff,
                            loss_mask=None):
    """
    Creates the PWC Net multi-level loss.
    :param expected_flow: Tensor of shape [B, H, W, 2]. Ground truth.
    :param flows: List of flow tensors. Flows at different resolutions.
    :param flow_scaling: Float. Scales the expected_flow before comparing it to the flows.
    :param flow_layer_loss_weights: List of floats. Loss weights that correspond to the flows list.
    :param diff_fn: Function.
    :param loss_mask: Tensor of shape [B, H, W, 1] to mask the loss. Magnitude of the values will be reweighted so that
                      the overall loss magnitude will be as if there was no mask applied.
    :return: total_loss: Scalar tensor. Sum off all weighted level losses.
             layer_losses: List of scalar tensors. Weighted loss at each level.
    """
    if flow_layer_loss_weights is None:
        flow_layer_loss_weights = [12.5, 12.5, 12.5, 25, 25, 50]

    with tf.name_scope('shape'):
        B, H, W, _ = tf.unstack(tf.shape(expected_flow))

    if loss_mask is not None:
        # Reweight the loss mask to account for any changes in overall magnitude.
        with tf.name_scope('mask_reweight'):
            loss_mask = loss_mask * (tf.cast(H * W, tf.float32) /
                                     (tf.reduce_sum(loss_mask, axis=[1, 2, 3], keepdims=True) + 1e-10))

    scaled_gt = expected_flow * flow_scaling

    layer_losses = []
    for i, flow in enumerate(flows):
        if flow_layer_loss_weights[i] == 0:
            continue
        with tf.name_scope('layer_' + str(i) + '_loss'):
            resized_flow = tf.image.resize_bilinear(flow, [H, W], align_corners=True)
            # diff has the shape [batch_size, H, W, 1].
            diff = diff_fn(scaled_gt, resized_flow)
            if loss_mask is not None:
                diff = diff * loss_mask
            # Reduce sum, average over all pixels, and apply the weight.
            weight = flow_layer_loss_weights[i]
            layer_loss = weight * (tf.reduce_sum(diff) / tf.cast(B * H * W, dtype=tf.float32))
            layer_losses.append(layer_loss)

    total_loss = tf.add_n(layer_losses, name='total_loss')
    return total_loss, layer_losses


def create_multi_level_unflow_loss(image_a, image_b, forward_flows, backward_flows, flow_scaling,
                                   flow_layer_loss_weights=None, layer_patch_distances=None, loss_weights=None):
    """
    :param image_a: Tensor of shape [B, H, W, 3].
    :param image_b: Tensor of shape [B, H, W, 3].
    :param forward_flows: List of tensors of shape [B, H_i, W_i, 2]. Flows at different resolutions (i.e. different
                          H_i, W_i for different resolutions). By default the resolutions should be increasing.
                          These should be in in the same order as flow_layer_loss_weights.
    :param backward_flows: List of tensors of shape [B, H_i, W_i, 2]. Flows at different resolutions (i.e. different
                           H_i, W_i for different resolutions). By default the resolutions should be increasing.
                           These should be in in the same order as flow_layer_loss_weights.
    :param flow_scaling: Float. Magnitude scale of the forward/backward flows with respect to the final flow. The
                         forward/backward flows will be divided by this before they are used in this function.
    :param flow_layer_loss_weights: List of floats. Loss weights that correspond to the flows list.
    :param layer_patch_distances: List of ints. These should be in the same order as flow_layer_loss_weights.
    :param loss_weights: Dict. Key is the loss name and the value is the weight.
    :return: total_loss: Scalar tensor. Sum off all weighted level losses.
             layer_losses: List of scalar tensors. Weighted loss at each level.
             forward_occlusion_masks: List of tensors of shape [B, H, W, 1].
             backward_occlusion_masks: List of tensors of shape [B, H, W, 1].
             layer_losses_detailed: List of dicts. Each dict contains the individual fully weighted losses.
    """
    # Set defaults.
    if flow_layer_loss_weights is None:
        flow_layer_loss_weights = [1.1, 3.4, 3.9, 4.35, 4.35, 12.7]
    if layer_patch_distances is None:
        layer_patch_distances = [1, 1, 2, 2, 3, 3]
    if loss_weights is None:
        loss_weights = {
            'ternary': 1.0,  # E_D data loss term in the paper.
            'occ': 12.4,  # Lambda_p. This is part of the E_D term to penalize the trivial occlusion mask.
            'smooth_2nd': 3.0,  # E_S second order smoothness constraint in the paper.
            'fb': 0.2  # E_C forward-backward consistency term in the paper.
        }

    # Length assertions.
    assert len(flow_layer_loss_weights) == len(layer_patch_distances)
    assert len(forward_flows) == len(layer_patch_distances)
    assert len(forward_flows) == len(backward_flows)

    layer_losses = []
    forward_occlusion_masks = []
    backward_occlusion_masks = []
    layer_losses_detailed = []

    _, image_height, _, _ = tf.unstack(tf.shape(image_a))
    for i, (forward_flow, backward_flow) in enumerate(zip(forward_flows, backward_flows)):
        if flow_layer_loss_weights[i] == 0.0:
            continue
        with tf.name_scope('layer_' + str(i) + '_loss'):
            _, flow_height, flow_width, _ = tf.unstack(tf.shape(forward_flow))

            # Resize the input images to match the corresponding flow size.
            resize_image_a = tf.image.resize_bilinear(image_a, (flow_height, flow_width), align_corners=True)
            resize_image_b = tf.image.resize_bilinear(image_b, (flow_height, flow_width), align_corners=True)

            # We must do this to interop with the rest of the network that uses a scaled flow due to the regular PWC Net
            # loss function.
            unscaled_forward_flow = forward_flow / flow_scaling
            unscaled_backward_flow = backward_flow / flow_scaling

            # The res_scaling is only used directly before using a flow to warp an image.
            # The reason we don't just apply the flow scaling here is because part of the UnFlow loss diffs the flow
            # magnitudes, and it's best to keep the loss magnitudes the same at all levels.
            res_scaling = tf.cast(flow_height, dtype=tf.float32) / tf.cast(image_height, dtype=tf.float32)
            losses, occ_fw, occ_bw = compute_losses(resize_image_a, resize_image_b, unscaled_forward_flow,
                                                    unscaled_backward_flow, prewarp_scaling=res_scaling,
                                                    data_max_distance=layer_patch_distances[i])
            forward_occlusion_masks.append(occ_fw)
            backward_occlusion_masks.append(occ_bw)

            # Get losses for this layer.
            this_layer_losses = []
            this_layer_losses_dict = {}
            for key in loss_weights.keys():
                assert key in losses
                loss = loss_weights[key] * losses[key]
                this_layer_losses.append(loss)
                this_layer_losses_dict[key] = flow_layer_loss_weights[i] * loss
            if len(this_layer_losses) > 0:
                # Sum all losses for this layer and apply the loss weight.
                layer_losses.append(flow_layer_loss_weights[i] * tf.add_n(this_layer_losses))
                layer_losses_detailed.append(this_layer_losses_dict)

    # Sum up the total loss.
    if len(layer_losses) > 0:
        total_loss = tf.add_n(layer_losses, name='total_unflow_loss')
    else:
        total_loss = tf.constant(0.0, dtype=tf.float32)

    return total_loss, layer_losses, forward_occlusion_masks, backward_occlusion_masks, layer_losses_detailed


def create_training_hints(last_flow_unscaled, gt_resized, flows, keep_mask, flow_error_hint_thresh, num_hints,
                          hint_noise_std_dev, valid, probablistic_sampling=False):
    """
    Creates hints for training the hinted network. The hints are sampled from areas that are incorrect. Sampling only
    occurs in regions specified by the valid tensor.
    This function considers the full resolution image to be [B, H, W, 2].
    :param last_flow_unscaled: Previous flow unscaled. Shape is [B, H / 4, W / 4, 2].
    :param gt_resized: Ground truth resized to quarter resolution. Shape is [B, H / 4, W / 4, 2].
    :param flows: Ground truth at full resolution. Shape is [B, H, W, 2].
    :param keep_mask: None if no keep mask. Values are 0 or 1. Shape is [B, H, W, 2].
            This keep_mask defines areas where hints should be provided regardless of the error threshold and sampling.
            If provided, hints will be generated at the locations specified in the keep_mask.
    :param flow_error_hint_thresh: Float. This is the absolute threshold for which the predicted optical flow
            is considered erroneous. Hints will only be sampled in these erroneous areas.
            Ignored if probablistic_sampling is True.
    :param num_hints: Scalar. Number of hints.
    :param hint_noise_std_dev: Scalar. Std dev of the hint accuracy.
    :param valid: Tensor of shape [B, H, W, 1]. Values should be either 0.0 or 1.0. 1.0 means valid ground truth.
    :param probablistic_sampling: Bool. Samples the hints with a higher probability where the error is higher.
    :return: New keep_mask, hints, loss_mask, flow_error.
            loss_mask is to be used for masking the multi-level loss.
    """
    with tf.name_scope('shape'):
        B, H, W, C = tf.unstack(tf.shape(flows))
    with tf.name_scope('diff'):
        # Threshold the end point error.
        flow_error = l2_diff(last_flow_unscaled, gt_resized)
        # Adjust for the epsilon used in the norm.
        flow_error = tf.maximum(flow_error - 1e-5, 0.0)
    with tf.name_scope('sample_keep_mask'):
        if probablistic_sampling:
            # Gaussian blur to help smooth outliers.
            flow_error = convolve_with_gaussian(flow_error, 2.0, 3, normalized=True)
        flow_error = tf.image.resize_nearest_neighbor(flow_error, [H, W], align_corners=True)
        flow_error_thresh = None
        if probablistic_sampling:
            curr_keep_mask = sample_n_from_weights(flow_error * valid, num_hints)
        else:
            flow_error_thresh = tf.cast(flow_error > flow_error_hint_thresh, dtype=tf.float32)
            curr_keep_mask = sample_n_from_mask(flow_error_thresh * valid, num_hints)
        if keep_mask is None:
            keep_mask = curr_keep_mask
        else:  # Accumulate hints.
            keep_mask = tf.clip_by_value(curr_keep_mask + keep_mask, 0, 1)
    with tf.name_scope('get_hints'):
        hints = flows
        if hint_noise_std_dev > 0.0:
            # hint_noise_std_dev is measured as a percentage of the hint magnitude.
            hints = hints + norm(hints) * tf.truncated_normal((B, H, W, C), mean=0.0, stddev=hint_noise_std_dev)
        hints = hints * keep_mask
    with tf.name_scope('loss_mask'):
        if probablistic_sampling:
            # Use a normalized flow_error where the max value is 1.0.
            flow_error_component = flow_error / (tf.reduce_max(flow_error, axis=[1, 2, 3], keepdims=True) + 1e-10)
        else:
            flow_error_component = flow_error_thresh
        # Loss mask is the region of flow error plus the keep mask to add extra weight to the hinted areas.
        keep_mask_component = convolve_with_gaussian(keep_mask, 4.0, 15, normalized=False)
        flow_error_component = convolve_with_gaussian(flow_error_component, 4.0, 7, normalized=True)
        # Sum the loss weightings. We don't need to reweight them here because the create_multi_level_loss function will
        # do that for us.
        loss_mask = keep_mask_component + flow_error_component
    with tf.name_scope('stop_gradients'):
        # No gradients.
        keep_mask = tf.stop_gradient(keep_mask)
        hints = tf.stop_gradient(hints)
        loss_mask = tf.stop_gradient(loss_mask)
        flow_error = tf.stop_gradient(flow_error)
    return keep_mask, hints, loss_mask, flow_error
