import numpy as np
import unittest
import tensorflow as tf
from pwcnet.hint_splat.hint_splat import hint_splat
from tensorflow.python.ops import gradient_checker


class TestHintSplat(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_zeros(self):
        batch = 3
        height = 4
        width = 6
        hints_tensor = tf.zeros((batch, height, width, 2), dtype=tf.float32)
        hints_mask_tensor = tf.zeros((batch, height, width, 1), dtype=tf.float32)
        flow_tensor = tf.zeros((batch, height, width, 2), dtype=tf.float32)
        splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor)
        splatted_hints, splat_weights = self.sess.run([splatted_hints, splat_weights])
        self.assertTupleEqual((batch, height, width, 2), splatted_hints.shape)
        self.assertTupleEqual((batch, height, width, 1), splat_weights.shape)
        self.assertTrue(np.allclose(np.zeros((batch, height, width, 2), dtype=np.float32), splatted_hints))
        self.assertTrue(np.allclose(np.zeros((batch, height, width, 1), dtype=np.float32), splat_weights))

    def test_zeros_large_kernel(self):
        batch = 3
        height = 4
        width = 6
        hints_tensor = tf.zeros((batch, height, width, 2), dtype=tf.float32)
        hints_mask_tensor = tf.zeros((batch, height, width, 1), dtype=tf.float32)
        flow_tensor = tf.zeros((batch, height, width, 2), dtype=tf.float32)
        splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor, kernel_size=17)
        splatted_hints, splat_weights = self.sess.run([splatted_hints, splat_weights])
        self.assertTupleEqual((batch, height, width, 2), splatted_hints.shape)
        self.assertTupleEqual((batch, height, width, 1), splat_weights.shape)
        self.assertTrue(np.allclose(np.zeros((batch, height, width, 2), dtype=np.float32), splatted_hints))
        self.assertTrue(np.allclose(np.zeros((batch, height, width, 1), dtype=np.float32), splat_weights))

    def test_single_hint_spread(self):
        batch = 1
        height = 4
        width = 4
        hints_mask = np.asarray([[[[0.0], [0.0], [0.0], [0.0]],
                                  [[0.0], [0.0], [1.0], [0.0]],
                                  [[0.0], [0.0], [0.0], [0.0]],
                                  [[0.0], [0.0], [0.0], [0.0]]]], dtype=np.float32)
        hints_mask_tensor = tf.constant(hints_mask, dtype=tf.float32)
        hints_tensor = tf.ones((batch, height, width, 2), dtype=tf.float32) * hints_mask_tensor
        flow_tensor = tf.zeros((batch, height, width, 2), dtype=tf.float32)
        splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor,
                                                   kernel_size=5, range_std_dev=0.1, spacial_std_dev=1.5)
        splatted_hints, splat_weights = self.sess.run([splatted_hints, splat_weights])
        self.assertTupleEqual((batch, height, width, 2), splatted_hints.shape)
        self.assertTupleEqual((batch, height, width, 1), splat_weights.shape)
        expected = np.asarray([[[[0.32919297], [0.64118034], [0.8007374], [0.64118034]],
                                [[0.41111228], [0.8007374], [1.], [0.8007374]],
                                [[0.32919297], [0.64118034], [0.8007374], [0.64118034]],
                                [[0.1690133], [0.32919297], [0.41111228], [0.32919297]]]], dtype=np.float32)
        self.assertTrue(np.allclose(expected, splat_weights, atol=1e-4))
        self.assertTrue(np.allclose(expected * np.ones((batch, height, width, 2), dtype=np.float32), splatted_hints,
                                    atol=1e-4))

    def test_single_hint_spread_with_boundary(self):
        batch = 1
        height = 4
        width = 4
        hints_mask = np.asarray([[[[0.0], [0.0], [0.0], [0.0]],
                                  [[0.0], [0.0], [1.0], [0.0]],
                                  [[0.0], [0.0], [0.0], [0.0]],
                                  [[0.0], [0.0], [0.0], [0.0]]]], dtype=np.float32)
        hints_mask_tensor = tf.constant(hints_mask, dtype=tf.float32)
        hints_tensor = tf.ones((batch, height, width, 2), dtype=tf.float32) * hints_mask_tensor
        flow = np.asarray([[[[0.0, 0.0], [0.0, 0.0], [0.0, 0.0], [0.0, 5.0]],
                            [[0.0, 0.0], [0.0, 0.0], [0.0, 0.0], [0.0, 0.0]],
                            [[1.0, 0.0], [0.0, 0.0], [0.0, 0.0], [0.0, 0.0]],
                            [[0.0, 1.0], [1.0, 0.0], [1.0, 1.0], [0.0, 0.0]]]], dtype=np.float32)
        flow_tensor = tf.constant(flow, dtype=tf.float32)
        splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor,
                                                   kernel_size=5, range_std_dev=0.01, spacial_std_dev=1.5)
        splatted_hints, splat_weights = self.sess.run([splatted_hints, splat_weights])
        self.assertTupleEqual((batch, height, width, 2), splatted_hints.shape)
        self.assertTupleEqual((batch, height, width, 1), splat_weights.shape)
        expected = np.asarray([[[[0.32919297], [0.64118034], [0.8007374], [0.0]],
                                [[0.41111228], [0.8007374], [1.], [0.8007374]],
                                [[0.0], [0.64118034], [0.8007374], [0.64118034]],
                                [[0.0], [0.0], [0.0], [0.32919297]]]], dtype=np.float32)
        self.assertTrue(np.allclose(expected, splat_weights, atol=1e-4))
        self.assertTrue(np.allclose(expected * np.ones((batch, height, width, 2), dtype=np.float32), splatted_hints,
                                    atol=1e-4))

    def test_single_hint_spread_with_soft_boundary(self):
        batch = 1
        height = 4
        width = 4
        hints_mask = np.asarray([[[[0.0], [0.0], [0.0], [0.0]],
                                  [[0.0], [0.0], [0.0], [1.0]],
                                  [[0.0], [0.0], [0.0], [1.0]],
                                  [[0.0], [0.0], [0.0], [0.0]]]], dtype=np.float32)
        hints_mask_tensor = tf.constant(hints_mask, dtype=tf.float32)
        hints_tensor = tf.ones((batch, height, width, 2), dtype=tf.float32) * hints_mask_tensor
        flow = np.asarray([[[[0.0, 0.3], [0.0, 0.2], [0.0, 0.0], [0.0, 0.0]],
                            [[0.0, 0.3], [0.0, 0.2], [10.0, 10.0], [0.0, 0.0]],
                            [[0.0, 0.3], [0.0, 0.2], [0.0, 0.1], [0.0, 0.0]],
                            [[0.0, 0.3], [0.0, 0.2], [0.0, 0.1], [0.0, 0.0]]]], dtype=np.float32)
        flow_tensor = tf.constant(flow, dtype=tf.float32)
        splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor,
                                                   kernel_size=5, range_std_dev=0.2, spacial_std_dev=1.5)
        splatted_hints, splat_weights = self.sess.run([splatted_hints, splat_weights])
        self.assertTupleEqual((batch, height, width, 2), splatted_hints.shape)
        self.assertTupleEqual((batch, height, width, 1), splat_weights.shape)
        expected = np.asarray([[[[0.], [0.19966564], [0.6411804], [0.8007374]],
                                [[0.], [0.24935222], [0.0], [1.]],
                                [[0.], [0.24935222], [0.7066483], [1.]],
                                [[0.], [0.19966564], [0.56583965], [0.8007374]]]], dtype=np.float32)
        self.assertTrue(np.allclose(expected, splat_weights, atol=1e-4))
        self.assertTrue(np.allclose(expected * np.ones((batch, height, width, 2), dtype=np.float32), splatted_hints,
                                    atol=1e-4))

    def test_two_hint_spread(self):
        batch = 1
        height = 3
        width = 4
        hints_mask = np.asarray([[[[0.0], [0.0], [0.0], [0.0]],
                                  [[1.0], [0.0], [0.0], [1.0]],
                                  [[0.0], [0.0], [0.0], [0.0]]]], dtype=np.float32)
        hints_mask_tensor = tf.constant(hints_mask, dtype=tf.float32)
        hints_tensor = tf.ones((batch, height, width, 2), dtype=tf.float32) * hints_mask_tensor
        flow_tensor = tf.zeros((batch, height, width, 2), dtype=tf.float32)
        splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor,
                                                   kernel_size=5, range_std_dev=0.1, spacial_std_dev=1.5)
        splatted_hints, splat_weights = self.sess.run([splatted_hints, splat_weights])
        self.assertTupleEqual((batch, height, width, 2), splatted_hints.shape)
        self.assertTupleEqual((batch, height, width, 1), splat_weights.shape)
        expected = np.asarray([[[[0.8007374], [0.64118034], [0.64118034], [0.8007374]],
                                [[1.], [0.8007374], [0.8007374], [1.]],
                                [[0.8007374], [0.64118034],[0.64118034], [0.8007374]]]], dtype=np.float32)
        self.assertTrue(np.allclose(expected, splat_weights, atol=1e-4))
        self.assertTrue(np.allclose(expected * np.ones((batch, height, width, 2), dtype=np.float32), splatted_hints,
                                    atol=1e-4))

    def test_two_hints_nearby(self):
        batch = 1
        height = 3
        width = 4
        hints_mask = np.asarray([[[[0.0], [0.0], [0.0], [0.0]],
                                  [[0.0], [1.0], [1.0], [0.0]],
                                  [[0.0], [0.0], [0.0], [0.0]]]], dtype=np.float32)
        hints_mask_tensor = tf.constant(hints_mask, dtype=tf.float32)
        hints_tensor = tf.ones((batch, height, width, 2), dtype=tf.float32) * hints_mask_tensor
        flow_tensor = tf.zeros((batch, height, width, 2), dtype=tf.float32)
        splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor,
                                                   kernel_size=5, range_std_dev=0.1, spacial_std_dev=1.5)
        splatted_hints, splat_weights = self.sess.run([splatted_hints, splat_weights])
        self.assertTupleEqual((batch, height, width, 2), splatted_hints.shape)
        self.assertTupleEqual((batch, height, width, 1), splat_weights.shape)
        expected = np.asarray([[[[0.64118034], [0.8007374], [0.8007374], [0.64118034]],
                                [[0.8007374], [1.], [1.], [0.8007374]],
                                [[0.64118034], [0.8007374], [0.8007374], [0.64118034]]]], dtype=np.float32)
        self.assertTrue(np.allclose(expected, splat_weights, atol=1e-4), str(splat_weights))
        self.assertTrue(np.allclose(expected * np.ones((batch, height, width, 2), dtype=np.float32), splatted_hints,
                                    atol=1e-4))

    def test_two_hint_spread_boundary(self):
        batch = 1
        height = 3
        width = 4
        hints_mask = np.asarray([[[[0.0], [0.0], [0.0], [0.0]],
                                  [[1.0], [0.0], [0.0], [1.0]],
                                  [[0.0], [0.0], [0.0], [0.0]]]], dtype=np.float32)
        hints_mask_tensor = tf.constant(hints_mask, dtype=tf.float32)
        hints_tensor = tf.ones((batch, height, width, 2), dtype=tf.float32) * hints_mask_tensor
        flow = np.asarray([[[[0.0, 1.0], [1.0, 0.0], [0.0, 0.0], [0.0, 0.0]],
                            [[1.0, 0.0], [1.0, 0.0], [0.0, 0.0], [0.0, 0.0]],
                            [[1.0, 0.0], [1.0, 0.0], [0.0, 0.0], [0.0, 1.0]]]], dtype=np.float32)
        flow_tensor = tf.constant(flow, dtype=tf.float32)
        splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor,
                                                   kernel_size=5, range_std_dev=0.01, spacial_std_dev=1.5)
        splatted_hints, splat_weights = self.sess.run([splatted_hints, splat_weights])
        self.assertTupleEqual((batch, height, width, 2), splatted_hints.shape)
        self.assertTupleEqual((batch, height, width, 1), splat_weights.shape)
        expected = np.asarray([[[[0.0], [0.64118034], [0.64118034], [0.8007374]],
                                [[1.], [0.8007374], [0.8007374], [1.]],
                                [[0.8007374], [0.64118034],[0.64118034], [0.0]]]], dtype=np.float32)
        self.assertTrue(np.allclose(expected, splat_weights, atol=1e-4))
        self.assertTrue(np.allclose(expected * np.ones((batch, height, width, 2), dtype=np.float32), splatted_hints,
                                    atol=1e-4))

    def test_two_hint_values(self):
        batch = 1
        height = 3
        width = 4
        hints_mask = np.asarray([[[[0.0], [0.0], [0.0], [0.0]],
                                  [[1.0], [0.0], [0.0], [1.0]],
                                  [[0.0], [0.0], [0.0], [0.0]]]], dtype=np.float32)
        hints_mask_tensor = tf.constant(hints_mask, dtype=tf.float32)
        hints = np.asarray([[[[0.0, 0.0], [0.0, 0.0], [0.0, 0.0], [0.0, 0.0]],
                             [[1.0, 2.0], [0.0, 0.0], [0.0, 0.0], [2.0, 1.0]],
                             [[0.0, 0.0], [0.0, 0.0], [0.0, 0.0], [0.0, 0.0]]]], dtype=np.float32)
        hints_tensor = tf.constant(hints, dtype=tf.float32) * hints_mask_tensor
        flow_tensor = tf.zeros((batch, height, width, 2), dtype=tf.float32)
        splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor,
                                                   kernel_size=5, range_std_dev=0.1, spacial_std_dev=1.5)
        splatted_hints, splat_weights = self.sess.run([splatted_hints, splat_weights])
        self.assertTupleEqual((batch, height, width, 2), splatted_hints.shape)
        self.assertTupleEqual((batch, height, width, 1), splat_weights.shape)
        expected = np.asarray([[[[0.8007374], [0.64118034], [0.64118034], [0.8007374]],
                                [[1.], [0.8007374], [0.8007374], [1.]],
                                [[0.8007374], [0.64118034], [0.64118034], [0.8007374]]]], dtype=np.float32)
        self.assertTrue(np.allclose(expected, splat_weights, atol=1e-4))
        expected_hint = np.asarray([[[[0.80073, 1.60147], [0.85869, 1.06484], [1.06484, 0.85869], [1.60147, 0.80073]],
                                     [[1., 2.], [1.0723825, 1.3298297], [1.3298297, 1.0723825], [2., 1.]],
                                     [[0.80073, 1.60147], [0.85869, 1.06484], [1.06484, 0.85869], [1.60147, 0.80073]]]],
                                   dtype=np.float32)
        self.assertTrue(np.allclose(expected_hint, splatted_hints, atol=1e-4))

    def test_two_hint_values_boundary(self):
        batch = 1
        height = 3
        width = 4
        hints_mask = np.asarray([[[[0.0], [0.0], [0.0], [0.0]],
                                  [[1.0], [0.0], [0.0], [1.0]],
                                  [[0.0], [0.0], [0.0], [0.0]]]], dtype=np.float32)
        hints_mask_tensor = tf.constant(hints_mask, dtype=tf.float32)
        hints = np.asarray([[[[0.0, 0.0], [0.0, 0.0], [0.0, 0.0], [0.0, 0.0]],
                             [[1.0, 2.0], [0.0, 0.0], [0.0, 0.0], [2.0, 1.0]],
                             [[0.0, 0.0], [0.0, 0.0], [0.0, 0.0], [0.0, 0.0]]]], dtype=np.float32)
        hints_tensor = tf.constant(hints, dtype=tf.float32) * hints_mask_tensor
        flow = np.asarray([[[[1.0, 0.0], [1.0, 0.0], [0.0, 0.0], [0.0, 0.0]],
                            [[1.0, 0.0], [1.0, 0.0], [0.0, 0.0], [0.0, 0.0]],
                            [[1.0, 0.0], [1.0, 0.0], [0.0, 0.0], [0.0, 0.0]]]], dtype=np.float32)
        flow_tensor = tf.constant(flow, dtype=tf.float32)
        splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor,
                                                   kernel_size=5, range_std_dev=0.01, spacial_std_dev=1.5)
        splatted_hints, splat_weights = self.sess.run([splatted_hints, splat_weights])
        self.assertTupleEqual((batch, height, width, 2), splatted_hints.shape)
        self.assertTupleEqual((batch, height, width, 1), splat_weights.shape)
        expected = np.asarray([[[[0.8007374], [0.64118034], [0.64118034], [0.8007374]],
                                [[1.], [0.8007374], [0.8007374], [1.]],
                                [[0.8007374], [0.64118034], [0.64118034], [0.8007374]]]], dtype=np.float32)
        self.assertTrue(np.allclose(expected, splat_weights, atol=1e-4))
        expected_hint = np.asarray([[[[0.80073, 1.60147], [0.64118, 1.28236], [1.28236, 0.64118], [1.60147, 0.80073]],
                                     [[1., 2.], [0.80073, 1.60147], [1.60147, 0.80073], [2., 1.]],
                                     [[0.80073, 1.60147], [0.64118, 1.28236], [1.28236, 0.64118], [1.60147, 0.80073]]]],
                                   dtype=np.float32)
        self.assertTrue(np.allclose(expected_hint, splatted_hints, atol=1e-4), str(splatted_hints))


class TestHintSplatGrads(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        self.max_allowable_grad_err = 1.5e-3

    def test_gradients_error_splatted_hints(self):
        kernel_size = 5
        sigma_color = 0.2
        sigma_space = 1.5

        with self.sess:
            # This test is flaky, so retry if fail.
            num_tries = 3
            error = 0
            for i in range(num_tries):
                flow_shape = (2, 16, 16, 2)
                mask_shape = (2, 16, 16, 1)
                hints = np.random.rand(*flow_shape)
                flow = np.random.rand(*flow_shape)
                hints_tensor = tf.placeholder(shape=flow.shape, dtype=tf.float32)
                hints_mask_tensor = tf.ones(shape=mask_shape, dtype=tf.float32)
                flow_tensor = tf.constant(flow, dtype=tf.float32)
                splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor,
                                                           kernel_size=kernel_size, range_std_dev=sigma_color,
                                                           spacial_std_dev=sigma_space)

                grad = tf.gradients(splatted_hints, hints_tensor)[0]
                grad = self.sess.run(grad, feed_dict={hints_tensor: hints})
                self.assertGreater(np.sum(grad), 0)

                error = gradient_checker.compute_gradient_error(hints_tensor, flow.shape, splatted_hints, flow.shape,
                                                                x_init_value=hints, delta=2e-4)
                if error <= self.max_allowable_grad_err:
                    return
            self.assertLessEqual(error, self.max_allowable_grad_err,
                                 'Exceeded the error threshold. Note that this test may be flaky.')

    def test_gradients_error_splatted_hints_mask(self):
        kernel_size = 5
        sigma_color = 0.2
        sigma_space = 1.5

        with self.sess:
            # This test is flaky, so retry if fail.
            num_tries = 3
            error = 0
            for i in range(num_tries):
                flow_shape = (2, 16, 16, 2)
                mask_shape = (2, 16, 16, 1)
                mask = np.random.rand(*mask_shape)
                flow = np.random.rand(*flow_shape)
                hints_tensor = tf.ones(shape=flow_shape, dtype=tf.float32)
                hints_mask_tensor = tf.placeholder(shape=mask_shape, dtype=tf.float32)
                flow_tensor = tf.constant(flow, dtype=tf.float32)
                splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor,
                                                           kernel_size=kernel_size, range_std_dev=sigma_color,
                                                           spacial_std_dev=sigma_space)

                grad = tf.gradients(splat_weights, hints_mask_tensor)[0]
                grad = self.sess.run(grad, feed_dict={hints_mask_tensor: mask})
                self.assertGreater(np.sum(grad), 0)

                error = gradient_checker.compute_gradient_error(hints_mask_tensor, mask.shape,
                                                                splat_weights, mask.shape,
                                                                x_init_value=mask, delta=2e-4)
                if error <= self.max_allowable_grad_err:
                    return
            self.assertLessEqual(error, self.max_allowable_grad_err,
                                 'Exceeded the error threshold. Note that this test may be flaky.')

    def test_gradients_error_splatted_hints_and_hints_mask(self):
        kernel_size = 5
        sigma_color = 0.2
        sigma_space = 1.5

        with self.sess:
            # This test is flaky, so retry if fail.
            num_tries = 3
            error = 0
            for i in range(num_tries):
                flow_shape = (2, 16, 16, 2)
                mask_shape = (2, 16, 16, 1)
                mask = np.random.rand(*mask_shape)
                flow = np.random.rand(*flow_shape)
                hints_tensor = tf.constant(flow, dtype=tf.float32)
                hints_mask_tensor = tf.constant(mask, dtype=tf.float32)
                flow_tensor = tf.constant(flow, dtype=tf.float32)
                splatted_hints, splat_weights = hint_splat(hints_tensor, hints_mask_tensor, flow_tensor,
                                                           kernel_size=kernel_size, range_std_dev=sigma_color,
                                                           spacial_std_dev=sigma_space)

                error = gradient_checker.compute_gradient_error([hints_tensor, hints_mask_tensor],
                                                                [flow.shape, mask.shape],
                                                                splat_weights, mask.shape, delta=2e-4)
                if error <= self.max_allowable_grad_err:
                    return
            self.assertLessEqual(error, self.max_allowable_grad_err,
                                 'Exceeded the error threshold. Note that this test may be flaky.')


if __name__ == '__main__':
    unittest.main()
