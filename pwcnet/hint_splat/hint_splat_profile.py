import numpy as np
import tensorflow as tf
from common.utils.profile import run_profiler
from pwcnet.hint_splat.hint_splat import hint_splat


if __name__ == '__main__':
    height = 512
    width = 512
    batch_size = 8

    # Create the graph.
    hints_placeholder = tf.placeholder(shape=[batch_size, height, width, 2], dtype=tf.float32)
    hints_mask_placeholder = tf.placeholder(shape=[batch_size, height, width, 1], dtype=tf.float32)
    flow_placeholder = tf.placeholder(shape=[batch_size, height, width, 2], dtype=tf.float32)
    splatted_flow, splat_weights = hint_splat(hints_placeholder, hints_mask_placeholder, flow_placeholder,
                                              kernel_size=5, range_std_dev=0.1, spacial_std_dev=2.5)
    grads = tf.gradients(splatted_flow, hints_placeholder)
    grads += tf.gradients(splat_weights, hints_mask_placeholder)

    # Create inputs.
    hints = np.zeros(shape=[batch_size, height, width, 2], dtype=np.float32)
    hints_mask = np.zeros(shape=[batch_size, height, width, 1], dtype=np.float32)
    flow = np.zeros(shape=[batch_size, height, width, 2], dtype=np.float32)
    hints[:, 10, 10, :] = 1.0
    hints_mask[:, 10, 10, :] = 1.0
    hints[:, 14, 14, :] = 1.0
    hints_mask[:, 14, 14, :] = 1.0
    hints[:, 100, 100, :] = 1.0
    hints_mask[:, 100, 100, :] = 1.0

    feed_dict = {
        hints_placeholder: hints,
        hints_mask_placeholder: hints_mask,
        flow_placeholder: flow
    }
    run_profiler([splatted_flow, splat_weights, grads], feed_dict, name='hint_splat')
