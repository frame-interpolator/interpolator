#if GOOGLE_CUDA

#define EIGEN_USE_GPU

#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/util/cuda_kernel_helper.h"

using namespace tensorflow;

typedef Eigen::GpuDevice GPUDevice;

static const int channels = 2;
static const int small_kernel_thresh = 11;

template<bool small_kernel>
__global__ void HintSplatKernel(const int32 nthreads,
    const float* hints, const float* hints_mask, const float* flow, int batch, int height, int width, float* temp,
    float* splatted_hints, float* splat_weights, int kernel_size, float range_std_dev, float spacial_std_dev) {
    // Setup the weights cache. For small weights, use local registers.
    float* weights = nullptr;
    float local_data[small_kernel ? (small_kernel_thresh * small_kernel_thresh) : 1];
    if (small_kernel) {
        weights = local_data;
    }
    CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
        // Deconstruct using out_idx = src_x + width * (src_y + height * b).
        int idx = out_idx;
        const int src_x = idx % width;
        idx /= width;
        const int src_y = idx % height;
        const int b = idx / height;

        const int half_kernel = kernel_size / 2;
        const float range_variance = range_std_dev * range_std_dev;
        const float spacial_variance = spacial_std_dev * spacial_std_dev;

        const float range_gauss_divisor = 2.0f * range_variance;
        const float spacial_gauss_divisor = 2.0f * spacial_variance;

        const int src_mask_offset = out_idx;
        const int src_flow_offset_x = out_idx * 2;
        const int src_flow_offset_y = out_idx * 2 + 1;

        // Cache src flow values in registers.
        const float src_flow_reg[channels] = {
            flow[src_flow_offset_x],
            flow[src_flow_offset_y]
        };

        if (!small_kernel) {
            weights = temp + out_idx * kernel_size * kernel_size;
        }

#define FLOW_OFFSET(iy, ix, c) ((c) + channels * ((ix) + width * ((iy) + height * b)))
#define MASK_OFFSET(iy, ix) ((ix) + width * ((iy) + height * b))
        float max_weight = 0.0f;
        float weight_sum = 0.0f;
        int weight_idx = 0;
        for (int y = src_y - half_kernel; y <= src_y + half_kernel; ++y) {
            for (int x = src_x - half_kernel; x <= src_x + half_kernel; ++x, ++weight_idx) {
                if (x >= 0 && x < width && y >= 0 && y < height &&
                    hints_mask[MASK_OFFSET(y, x)] >= 1e-6) {
                    const int flow_offset_x = FLOW_OFFSET(y, x, 0);
                    const int flow_offset_y = FLOW_OFFSET(y, x, 1);

                    const int x_diff = x - src_x;
                    const int y_diff = y - src_y;
                    const float spacial_weight_log = -(x_diff * x_diff + y_diff * y_diff) / spacial_gauss_divisor;

                    const float flow_x_diff = src_flow_reg[0] - flow[flow_offset_x];
                    const float flow_y_diff = src_flow_reg[1] - flow[flow_offset_y];
                    const float range_weight_log = -(flow_x_diff * flow_x_diff + flow_y_diff * flow_y_diff) /
                        range_gauss_divisor;

                    const float weight = expf(spacial_weight_log + range_weight_log);
                    if (weight > max_weight) {
                        max_weight = weight;
                    }
                    weight_sum += weight;
                    weights[weight_idx] = weight;
                } else {
                    weights[weight_idx] = -1.0f;
                }
            }
        }

        float x_sum = 0.0f;
        float y_sum = 0.0f;
        float mask_sum = 0.0f;
        if (weight_sum > 1e-10) {
            weight_idx = 0;
            for (int y = src_y - half_kernel; y <= src_y + half_kernel; ++y) {
                for (int x = src_x - half_kernel; x <= src_x + half_kernel; ++x, ++weight_idx) {
                    const float weight = weights[weight_idx];
                    if (weight > 0.0f) {
                        const float effective_weight = weight / weight_sum * max_weight;
                        x_sum += effective_weight * hints[FLOW_OFFSET(y, x, 0)];
                        y_sum += effective_weight * hints[FLOW_OFFSET(y, x, 1)];
                        mask_sum += effective_weight * hints_mask[MASK_OFFSET(y, x)];
                    }
                }
            }
        }
        splatted_hints[src_flow_offset_x] = x_sum;
        splatted_hints[src_flow_offset_y] = y_sum;
        // The max weight should be the total weight if all mask values were 1.
        splat_weights[src_mask_offset] = mask_sum;
#undef MASK_OFFSET
#undef FLOW_OFFSET
    }
}

template<bool small_kernel>
__global__ void HintSplatGradKernel(const int32 nthreads,
    const float* splatted_hints_grad, const float* splat_weights_grad,
    const float* hints, const float* hints_mask, const float* flow,
    int batch, int height, int width, float* temp,
    float* hints_grad, float* hints_mask_grad,
    int kernel_size, float range_std_dev, float spacial_std_dev) {
    // Setup the weights cache. For small weights, use local registers.
    float* weights = nullptr;
    float local_data[small_kernel ? (small_kernel_thresh * small_kernel_thresh) : 1];
    if (small_kernel) {
        weights = local_data;
    }
    CUDA_1D_KERNEL_LOOP(out_idx, nthreads) {
        // Deconstruct using out_idx = src_x + width * (src_y + height * b).
        int idx = out_idx;
        const int src_x = idx % width;
        idx /= width;
        const int src_y = idx % height;
        const int b = idx / height;

        const int half_kernel = kernel_size / 2;
        const float range_variance = range_std_dev * range_std_dev;
        const float spacial_variance = spacial_std_dev * spacial_std_dev;

        const float range_gauss_divisor = 2.0f * range_variance;
        const float spacial_gauss_divisor = 2.0f * spacial_variance;

        const int src_mask_offset = out_idx;
        const int src_flow_offset_x = out_idx * 2;
        const int src_flow_offset_y = out_idx * 2 + 1;

        // Cache src flow values in registers.
        const float src_flow_reg[channels] = {
            flow[src_flow_offset_x],
            flow[src_flow_offset_y]
        };

        if (!small_kernel) {
            weights = temp + out_idx * kernel_size * kernel_size;
        }

#define FLOW_OFFSET(iy, ix, c) ((c) + channels * ((ix) + width * ((iy) + height * b)))
#define MASK_OFFSET(iy, ix) ((ix) + width * ((iy) + height * b))
        float max_weight = 0.0f;
        float weight_sum = 0.0f;
        int weight_idx = 0;
        for (int y = src_y - half_kernel; y <= src_y + half_kernel; ++y) {
            for (int x = src_x - half_kernel; x <= src_x + half_kernel; ++x, ++weight_idx) {
                if (x >= 0 && x < width && y >= 0 && y < height &&
                    hints_mask[MASK_OFFSET(y, x)] >= 1e-6) {
                    const int flow_offset_x = FLOW_OFFSET(y, x, 0);
                    const int flow_offset_y = FLOW_OFFSET(y, x, 1);

                    const int x_diff = x - src_x;
                    const int y_diff = y - src_y;
                    const float spacial_weight_log = -(x_diff * x_diff + y_diff * y_diff) / spacial_gauss_divisor;

                    const float flow_x_diff = src_flow_reg[0] - flow[flow_offset_x];
                    const float flow_y_diff = src_flow_reg[1] - flow[flow_offset_y];
                    const float range_weight_log = -(flow_x_diff * flow_x_diff + flow_y_diff * flow_y_diff) /
                        range_gauss_divisor;

                    const float weight = expf(spacial_weight_log + range_weight_log);
                    if (weight > max_weight) {
                        max_weight = weight;
                    }
                    weight_sum += weight;
                    weights[weight_idx] = weight;
                } else {
                    weights[weight_idx] = -1.0f;
                }
            }
        }

        if (weight_sum < 1e-10) {
            return;
        }

        const float din_splatted_hints_x = splatted_hints_grad[src_flow_offset_x];
        const float din_splatted_hints_y = splatted_hints_grad[src_flow_offset_y];
        const float din_splat_weights = splat_weights_grad[src_mask_offset];

        weight_idx = 0;
        for (int y = src_y - half_kernel; y <= src_y + half_kernel; ++y) {
            for (int x = src_x - half_kernel; x <= src_x + half_kernel; ++x, ++weight_idx) {
                const float weight = weights[weight_idx];
                if (weight > 0.0f) {
                    const int flow_offset_x = FLOW_OFFSET(y, x, 0);
                    const int flow_offset_y = FLOW_OFFSET(y, x, 1);
                    const int mask_offset = MASK_OFFSET(y, x);
                    const float effective_weight = weight / weight_sum * max_weight;
                    CudaAtomicAdd(hints_grad + flow_offset_x, din_splatted_hints_x * effective_weight);
                    CudaAtomicAdd(hints_grad + flow_offset_y, din_splatted_hints_y * effective_weight);
                    CudaAtomicAdd(hints_mask_grad + mask_offset, din_splat_weights * effective_weight);
                }
            }
        }
#undef MASK_OFFSET
#undef FLOW_OFFSET
    }
}

void HintSplat(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor hints,
    typename TTypes<float, 4>::ConstTensor hints_mask,
    typename TTypes<float, 4>::ConstTensor flow,
    typename TTypes<float, 4>::Tensor splatted_hints,
    typename TTypes<float, 4>::Tensor splat_weights,
    int kernel_size, float range_std_dev, float spacial_std_dev) {
    const int batch = hints.dimension(0);
    const int height = hints.dimension(1);
    const int width = hints.dimension(2);
    const int channels = hints.dimension(3);

    int total_count = batch * height * width * channels;
    if (total_count == 0) return;

    // Initialize splatted_hints with all zeros.
    CudaLaunchConfig config = GetCudaLaunchConfig(total_count, d);
    SetZero << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
        config.virtual_thread_count, splatted_hints.data());

    total_count = batch * height * width;
    config = GetCudaLaunchConfig(total_count, d);

    // Initialize splat_weights with all zeros.
    SetZero << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
        config.virtual_thread_count, splat_weights.data());

    if (kernel_size > small_kernel_thresh) {
        float* temp = nullptr;
        cudaMalloc((void**)&temp, total_count * kernel_size * kernel_size * sizeof(float));
        HintSplatKernel<false>
            <<<config.block_count, config.thread_per_block, 0, d.stream()>>>(
                config.virtual_thread_count, hints.data(), hints_mask.data(), flow.data(),
                batch, height, width, temp,
                splatted_hints.data(), splat_weights.data(), kernel_size, range_std_dev, spacial_std_dev);
        cudaFree((void*)temp);
    } else {
        HintSplatKernel<true>
            <<<config.block_count, config.thread_per_block, 0, d.stream()>>>(
                config.virtual_thread_count, hints.data(), hints_mask.data(), flow.data(),
                batch, height, width, nullptr,
                splatted_hints.data(), splat_weights.data(), kernel_size, range_std_dev, spacial_std_dev);
    }
}

void HintSplatGrad(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor splatted_hints_grad,
    typename TTypes<float, 4>::ConstTensor splat_weights_grad,
    typename TTypes<float, 4>::ConstTensor hints,
    typename TTypes<float, 4>::ConstTensor hints_mask,
    typename TTypes<float, 4>::ConstTensor flow,
    typename TTypes<float, 4>::Tensor hints_grad,
    typename TTypes<float, 4>::Tensor hints_mask_grad,
    int kernel_size, float range_std_dev, float spacial_std_dev) {
    const int batch = hints.dimension(0);
    const int height = hints.dimension(1);
    const int width = hints.dimension(2);
    const int channels = hints.dimension(3);

    int total_count = batch * height * width * channels;
    if (total_count == 0) return;

    CudaLaunchConfig config = GetCudaLaunchConfig(total_count, d);
    SetZero << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
        config.virtual_thread_count, hints_grad.data());

    total_count = batch * height * width;
    config = GetCudaLaunchConfig(total_count, d);

    SetZero << <config.block_count, config.thread_per_block, 0, d.stream() >> >(
        config.virtual_thread_count, hints_mask_grad.data());

    if (kernel_size > small_kernel_thresh) {
        float* temp = nullptr;
        cudaMalloc((void**)&temp, total_count * kernel_size * kernel_size * sizeof(float));
        HintSplatGradKernel<false>
            <<<config.block_count, config.thread_per_block, 0, d.stream()>>>(
                config.virtual_thread_count, splatted_hints_grad.data(), splat_weights_grad.data(),
                hints.data(), hints_mask.data(), flow.data(),
                batch, height, width, nullptr,
                hints_grad.data(), hints_mask_grad.data(),
                kernel_size, range_std_dev, spacial_std_dev);
        cudaFree((void*)temp);
    } else {
        HintSplatGradKernel<true>
            <<<config.block_count, config.thread_per_block, 0, d.stream()>>>(
                config.virtual_thread_count, splatted_hints_grad.data(), splat_weights_grad.data(),
                hints.data(), hints_mask.data(), flow.data(),
                batch, height, width, nullptr,
                hints_grad.data(), hints_mask_grad.data(),
                kernel_size, range_std_dev, spacial_std_dev);
    }
}

#endif  // GOOGLE_CUDA
