#define EIGEN_USE_THREADS

#include <memory>
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/framework/types.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/common_shape_fns.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"

using namespace tensorflow;

typedef Eigen::GpuDevice GPUDevice;

void HintSplat(const Eigen::GpuDevice& d,
    typename TTypes<float, 4>::ConstTensor hints,
    typename TTypes<float, 4>::ConstTensor hints_mask,
    typename TTypes<float, 4>::ConstTensor flow,
    typename TTypes<float, 4>::Tensor splatted_hints,
    typename TTypes<float, 4>::Tensor splat_weights,
    int kernel_size, float range_std_dev, float spacial_std_dev);

void HintSplatGrad(const Eigen::GpuDevice& d,
    typename TTypes<float, 4>::ConstTensor splatted_hints_grad,
    typename TTypes<float, 4>::ConstTensor splat_weights_grad,
    typename TTypes<float, 4>::ConstTensor hints,
    typename TTypes<float, 4>::ConstTensor hints_mask,
    typename TTypes<float, 4>::ConstTensor flow,
    typename TTypes<float, 4>::Tensor hints_grad,
    typename TTypes<float, 4>::Tensor hints_mask_grad,
    int kernel_size, float range_std_dev, float spacial_std_dev);

class HintSplatOp : public OpKernel {
public:
    explicit HintSplatOp(OpKernelConstruction* context) : OpKernel(context) {
        OP_REQUIRES_OK(context, context->GetAttr("kernel_size", &kernel_size_));
        OP_REQUIRES(context, kernel_size_ > 0,
            errors::InvalidArgument("Need kernel_size > 0, got ", kernel_size_));
        OP_REQUIRES(context, (kernel_size_ % 2) == 1,
            errors::InvalidArgument("Need kernel_size to be odd, got ", kernel_size_));

        OP_REQUIRES_OK(context, context->GetAttr("range_std_dev", &range_std_dev_));
        OP_REQUIRES(context, range_std_dev_ > 0,
            errors::InvalidArgument("Need range_std_dev > 0, got ", range_std_dev_));

        OP_REQUIRES_OK(context, context->GetAttr("spacial_std_dev", &spacial_std_dev_));
        OP_REQUIRES(context, spacial_std_dev_ > 0,
            errors::InvalidArgument("Need spacial_std_dev > 0, got ", spacial_std_dev_));
    }

    void Compute(OpKernelContext* context) override {
        const Tensor& input_hints = context->input(0);
        const Tensor& input_hints_mask = context->input(1);
        const Tensor& input_flow = context->input(2);

        // Assert correct number of dimensions.
        OP_REQUIRES(context, input_hints.dims() == 4,
            errors::InvalidArgument("input_hints expecting a 4-D vector."));
        OP_REQUIRES(context, input_hints.dim_size(3) == 2,
            errors::InvalidArgument("input_hints must have 2 channels."));
        OP_REQUIRES(context, input_flow.dims() == 4,
            errors::InvalidArgument("input_flow expecting a 4-D vector."));
        OP_REQUIRES(context, input_flow.dim_size(3) == 2,
            errors::InvalidArgument("input_flow must have 2 channels."));
        OP_REQUIRES(context, input_hints_mask.dims() == 4,
            errors::InvalidArgument("input_hints_mask expecting a 4-D vector."));
        OP_REQUIRES(context, input_hints_mask.dim_size(3) == 1,
            errors::InvalidArgument("input_hints_mask must have 1 channels."));

        TensorShape mask_shape = input_hints.shape();
        mask_shape.set_dim(3, 1);

        Tensor* splatted_hints = NULL;
        Tensor* splat_weights = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(0, input_hints.shape(), &splatted_hints));
        OP_REQUIRES_OK(context, context->allocate_output(1, mask_shape, &splat_weights));

        typename TTypes<float, 4>::ConstTensor hints_data = input_hints.tensor<float, 4>();
        typename TTypes<float, 4>::ConstTensor hints_mask_data = input_hints_mask.tensor<float, 4>();
        typename TTypes<float, 4>::ConstTensor flow_data = input_flow.tensor<float, 4>();
        typename TTypes<float, 4>::Tensor splatted_hints_data = splatted_hints->tensor<float, 4>();
        typename TTypes<float, 4>::Tensor splat_weights_data = splat_weights->tensor<float, 4>();

        HintSplat(context->eigen_device<GPUDevice>(),
            hints_data, hints_mask_data, flow_data, splatted_hints_data, splat_weights_data,
            kernel_size_, range_std_dev_, spacial_std_dev_);
    }

private:
    int kernel_size_;
    float range_std_dev_;
    float spacial_std_dev_;
};

class HintSplatOpGrad : public OpKernel {
public:
    explicit HintSplatOpGrad(OpKernelConstruction* context) : OpKernel(context) {
        OP_REQUIRES_OK(context, context->GetAttr("kernel_size", &kernel_size_));
        OP_REQUIRES(context, kernel_size_ > 0,
            errors::InvalidArgument("Need kernel_size > 0, got ", kernel_size_));
        OP_REQUIRES(context, (kernel_size_ % 2) == 1,
            errors::InvalidArgument("Need kernel_size to be odd, got ", kernel_size_));

        OP_REQUIRES_OK(context, context->GetAttr("range_std_dev", &range_std_dev_));
        OP_REQUIRES(context, range_std_dev_ > 0,
            errors::InvalidArgument("Need range_std_dev > 0, got ", range_std_dev_));

        OP_REQUIRES_OK(context, context->GetAttr("spacial_std_dev", &spacial_std_dev_));
        OP_REQUIRES(context, spacial_std_dev_ > 0,
            errors::InvalidArgument("Need spacial_std_dev > 0, got ", spacial_std_dev_));
    }

    void Compute(OpKernelContext* context) override {
        const Tensor& input_splatted_hints_grad = context->input(0);
        const Tensor& input_splat_weights_grad = context->input(1);
        const Tensor& input_hints = context->input(2);
        const Tensor& input_hints_mask = context->input(3);
        const Tensor& input_flow = context->input(4);

        // Assert correct number of dimensions.
        OP_REQUIRES(context, input_splatted_hints_grad.dims() == 4,
            errors::InvalidArgument("input_splatted_hints_grad expecting a 4-D vector."));
        OP_REQUIRES(context, input_splatted_hints_grad.dim_size(3) == 2,
            errors::InvalidArgument("input_splatted_hints_grad must have 2 channels."));
        OP_REQUIRES(context, input_splat_weights_grad.dims() == 4,
            errors::InvalidArgument("input_splat_weights_grad expecting a 4-D vector."));
        OP_REQUIRES(context, input_splat_weights_grad.dim_size(3) == 1,
            errors::InvalidArgument("input_splat_weights_grad must have 1 channels."));
        OP_REQUIRES(context, input_hints.dims() == 4,
            errors::InvalidArgument("input_hints expecting a 4-D vector."));
        OP_REQUIRES(context, input_hints.dim_size(3) == 2,
            errors::InvalidArgument("input_hints must have 2 channels."));
        OP_REQUIRES(context, input_flow.dims() == 4,
            errors::InvalidArgument("input_flow expecting a 4-D vector."));
        OP_REQUIRES(context, input_flow.dim_size(3) == 2,
            errors::InvalidArgument("input_flow must have 2 channels."));
        OP_REQUIRES(context, input_hints_mask.dims() == 4,
            errors::InvalidArgument("input_hints_mask expecting a 4-D vector."));
        OP_REQUIRES(context, input_hints_mask.dim_size(3) == 1,
            errors::InvalidArgument("input_hints_mask must have 1 channels."));

        TensorShape mask_shape = input_hints.shape();
        mask_shape.set_dim(3, 1);

        Tensor* output_hints_grad = NULL;
        Tensor* output_hints_mask_grad = NULL;
        OP_REQUIRES_OK(context, context->allocate_output(0, input_hints.shape(), &output_hints_grad));
        OP_REQUIRES_OK(context, context->allocate_output(1, mask_shape, &output_hints_mask_grad));

        typename TTypes<float, 4>::ConstTensor splatted_hints_grad_data = input_splatted_hints_grad.tensor<float, 4>();
        typename TTypes<float, 4>::ConstTensor splat_weights_grad_data = input_splat_weights_grad.tensor<float, 4>();
        typename TTypes<float, 4>::ConstTensor hints_data = input_hints.tensor<float, 4>();
        typename TTypes<float, 4>::ConstTensor hints_mask_data = input_hints_mask.tensor<float, 4>();
        typename TTypes<float, 4>::ConstTensor flow_data = input_flow.tensor<float, 4>();
        typename TTypes<float, 4>::Tensor hints_grad_data = output_hints_grad->tensor<float, 4>();
        typename TTypes<float, 4>::Tensor hints_mask_grad_data = output_hints_mask_grad->tensor<float, 4>();

        HintSplatGrad(context->eigen_device<GPUDevice>(),
            splatted_hints_grad_data, splat_weights_grad_data,
            hints_data, hints_mask_data, flow_data,
            hints_grad_data, hints_mask_grad_data,
            kernel_size_, range_std_dev_, spacial_std_dev_);
    }

private:
    int kernel_size_;
    float range_std_dev_;
    float spacial_std_dev_;
};

REGISTER_OP("HintSplat")
.Input("hints: float")
.Input("hints_mask: float")
.Input("flow: float")
.Output("splatted_hints: float")
.Output("splat_weights: float")
.Attr("kernel_size: int = 5")
.Attr("range_std_dev: float = 0.1")
.Attr("spacial_std_dev: float = 1.5")
.SetShapeFn([](shape_inference::InferenceContext* c) {
    c->set_output(0, c->input(0));
    c->set_output(1, c->input(1));
    return Status::OK();
});

REGISTER_OP("HintSplatGrad")
.Input("splatted_hints_grad: float")
.Input("splat_weights_grad: float")
.Input("hints: float")
.Input("hints_mask: float")
.Input("flow: float")
.Output("output_hints_grad: float")
.Output("output_hints_mask_grad: float")
.Attr("kernel_size: int = 5")
.Attr("range_std_dev: float = 0.1")
.Attr("spacial_std_dev: float = 1.5")
.SetShapeFn([](shape_inference::InferenceContext* c) {
    c->set_output(0, c->input(2));
    c->set_output(1, c->input(3));
    return Status::OK();
});

#if GOOGLE_CUDA

REGISTER_KERNEL_BUILDER(Name("HintSplat").Device(DEVICE_GPU), HintSplatOp);
REGISTER_KERNEL_BUILDER(Name("HintSplatGrad").Device(DEVICE_GPU), HintSplatOpGrad);

#endif // GOOGLE_CUDA
