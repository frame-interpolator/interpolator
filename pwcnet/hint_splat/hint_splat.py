import tensorflow as tf
from common.utils.tf import load_op_library
from tensorflow.python.framework import ops


# Load op library.
mod = load_op_library('hint_splat_op', 'build')


def hint_splat(hints, hints_mask, flow, kernel_size=5, range_std_dev=0.1, spacial_std_dev=1.5):
    """
    Splats the hint in a gaussian radius of spacial_std_dev pixels.
    The hints will not be splatted across edges where the flow magnitude changes more than range_std_dev pixels.
    The gaussian splat is modified like a bilateral filter to prevent splat over edges.
    Additionally, the gaussian filter is not normalized (the max value of the filter is 1.0) to preserve the magnitude
    of the hints. To prevent the overall magnitude of hints exceeding 1.0 when hints are close to each other, the
    max magnitude of gaussian filters is used instead of the sum of gaussian filters.
    :param hints: Flow tensor of shape [B, H, W, 2].
    :param hints_mask: Mask tensor of shape [B, H, W, 1]. Values of 1.0 indicate there is a hint.
    :param flow: Flow tensor of shape [B, H, W, 2].
    :param kernel_size: Diameter of the kernel.
    :param range_std_dev: Standard deviation of the color.
    :param spacial_std_dev: Standard deviation of the kernel.
    :return: Flow tensor of shape [B, H, W, 2].
             Weights tensor of shape [B, H, W, 1].
    """
    with tf.name_scope('hint_splat'):
        assert mod is not None, 'hint_splat_op is not compiled.'
        flow = tf.stop_gradient(flow)  # Gradient of the flow not implemented.
        splatted_hints, splat_weights = mod.hint_splat(hints, hints_mask, flow, kernel_size=kernel_size,
                                                       range_std_dev=range_std_dev, spacial_std_dev=spacial_std_dev)
        return splatted_hints, splat_weights


if mod is not None:
    @ops.RegisterGradient('HintSplat')
    def _HintSplatGrad(op, grad0, grad1):
        grads = mod.hint_splat_grad(grad0, grad1, op.inputs[0], op.inputs[1], op.inputs[2],
                                    kernel_size=op.get_attr('kernel_size'),
                                    range_std_dev=op.get_attr('range_std_dev'),
                                    spacial_std_dev=op.get_attr('spacial_std_dev'))
        hints_grad, hints_mask_grad = grads
        return hints_grad, hints_mask_grad, None
