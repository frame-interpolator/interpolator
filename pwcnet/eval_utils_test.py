import numpy as np
import unittest
from pwcnet.eval_utils import create_eval_hint_input, hint_eval_results_to_csv, calculate_aepe_stats


class TestHintUtils(unittest.TestCase):
    def test_create_eval_hint_input_same(self):
        height = 10
        width = 10
        num_hints = 10
        gt = np.ones(shape=[height, width, 2], dtype=np.float32)
        hint1, hint_mask1 = create_eval_hint_input(gt, num_hints)
        hint2, hint_mask2 = create_eval_hint_input(gt, num_hints)
        hint3, hint_mask3 = create_eval_hint_input(gt, num_hints)
        self.assertTupleEqual((1, 64, 64, 1), hint_mask3.shape)
        self.assertListEqual(hint1.tolist(), hint2.tolist())
        self.assertListEqual(hint2.tolist(), hint3.tolist())
        self.assertEqual(num_hints, int(np.sum(hint_mask1)))

    def test_create_eval_hint_input_gt_magnitude(self):
        height = 64
        width = 64
        num_hints = 20
        gt = np.ones(shape=[height, width, 2], dtype=np.float32)
        gt[:int(height / 2), ...] = 0.0
        hint, hint_mask = create_eval_hint_input(gt, num_hints)
        self.assertAlmostEqual(0.0, float(np.sum(hint_mask[0, :int(height / 2), ...])))

    def test_create_eval_hint_input_valid_mask(self):
        height = 64
        width = 64
        num_hints = 25
        gt = np.ones(shape=[height, width, 2], dtype=np.float32)
        valid_mask = np.ones(shape=[height, width, 1], dtype=np.float32)
        valid_mask[:int(height / 2), ...] = 0.0
        hint, hint_mask = create_eval_hint_input(gt, num_hints, valid_mask=valid_mask)
        self.assertEqual(num_hints, int(np.sum(hint_mask)))
        self.assertAlmostEqual(0.0, float(np.sum(hint_mask[0, :int(height / 2), ...])))

    def test_create_eval_hint_input_gt_magnitude_with_previous_mask(self):
        height = 64
        width = 64
        num_hints = 20
        gt = np.ones(shape=[height, width, 2], dtype=np.float32)
        prev = np.zeros(shape=[height, width, 1], dtype=np.float32)
        prev[0, 0, :] = 1.0
        hint, hint_mask = create_eval_hint_input(gt, num_hints, prev_hint_mask=prev)
        self.assertAlmostEqual(21.0, float(np.sum(hint_mask)))
        self.assertAlmostEqual(1.0, float(hint_mask[0, 0, 0, :]))

    def test_create_eval_hint_input_prev_flow(self):
        height = 64
        width = 64
        num_hints = 20
        gt = np.ones(shape=[height, width, 2], dtype=np.float32)
        prev_flow = np.zeros(shape=[height, width, 2], dtype=np.float32)
        prev_flow[:int(height / 2), ...] = 1.0
        hint, hint_mask = create_eval_hint_input(gt, num_hints, prev_flow=prev_flow)
        self.assertAlmostEqual(0.0, float(np.sum(hint_mask[0, :int(height / 2), ...])))

    def test_create_eval_hint_input_prev_flow_percent_error(self):
        height = 64
        width = 64
        num_hints = 20
        gt = np.ones(shape=[height, width, 2], dtype=np.float32)
        prev_flow = np.zeros(shape=[height, width, 2], dtype=np.float32)
        prev_flow[:int(height / 2), ...] = 1.0
        hint, hint_mask = create_eval_hint_input(gt, num_hints, prev_flow=prev_flow, sample_percent_error=True)
        self.assertAlmostEqual(0.0, float(np.sum(hint_mask[0, :int(height / 2), ...])))

    def test_create_eval_hint_input_deterministic(self):
        height = 64
        width = 64
        num_hints = 2
        gt = np.ones(shape=[height, width, 2], dtype=np.float32)
        gt[0, 0, :] = 2.0
        hint, hint_mask = create_eval_hint_input(gt, num_hints, probabilistic=False)
        self.assertAlmostEqual(1.0, float(np.sum(hint_mask[0, 0, 0, ...])))
        self.assertAlmostEqual(2.0, float(np.sum(hint_mask)))

    def test_create_eval_hint_input_prev_flow_empty(self):
        height = 64
        width = 64
        num_hints = 0
        gt = np.ones(shape=[height, width, 2], dtype=np.float32)
        prev_flow = np.zeros(shape=[height, width, 2], dtype=np.float32)
        prev_flow[:int(height / 2), ...] = 1.0
        hint, hint_mask = create_eval_hint_input(gt, num_hints, prev_flow=prev_flow, probabilistic=False)
        self.assertAlmostEqual(0.0, float(np.sum(hint)))
        self.assertAlmostEqual(0.0, float(np.sum(hint_mask)))

    def test_create_eval_hint_input_valid_mask_empty(self):
        height = 64
        width = 64
        num_hints = 0
        gt = np.ones(shape=[height, width, 2], dtype=np.float32)
        valid_mask = np.ones(shape=[height, width, 1], dtype=np.float32)
        valid_mask[:int(height / 2), ...] = 0.0
        hint, hint_mask = create_eval_hint_input(gt, num_hints, valid_mask=valid_mask, probabilistic=True)
        self.assertAlmostEqual(0.0, float(np.sum(hint)))
        self.assertAlmostEqual(0.0, float(np.sum(hint_mask)))

    def test_hint_eval_results_to_csv(self):
        totals = [1.0, 2.0, 3.0]
        low_speeds = [0.0, 1.0, 2.0]
        med_speeds = [3.0, 4.0, 5.0]
        hi_speeds = [6.0, 7.0, 8.0]
        csv_str = hint_eval_results_to_csv(totals, low_speeds, med_speeds, hi_speeds)
        expected = 'num_hints, EPE all, s0-10, s10-40, s40+\n'
        expected += '0, 1.000, 0.000, 3.000, 6.000\n'
        expected += '1, 2.000, 1.000, 4.000, 7.000\n'
        expected += '2, 3.000, 2.000, 5.000, 8.000\n'
        self.assertEqual(expected, csv_str)

    def test_hint_eval_results_to_csv_min_hints(self):
        totals = [1.0, 2.0, 3.0]
        low_speeds = [0.0, 1.0, 2.0]
        med_speeds = [3.0, 4.0, 5.0]
        hi_speeds = [6.0, 7.0, 8.0]
        csv_str = hint_eval_results_to_csv(totals, low_speeds, med_speeds, hi_speeds, min_num_hints=2)
        expected = 'num_hints, EPE all, s0-10, s10-40, s40+\n'
        expected += '2, 1.000, 0.000, 3.000, 6.000\n'
        expected += '3, 2.000, 1.000, 4.000, 7.000\n'
        expected += '4, 3.000, 2.000, 5.000, 8.000\n'
        self.assertEqual(expected, csv_str)


class TestCalculateAEPE(unittest.TestCase):
    def test_zero_aepe(self):
        gt = np.asarray([[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0], [1.0, -1.0]])
        flow = np.asarray([[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0], [1.0, -1.0]])
        aepe, low_speed, med_speed, hi_speed = calculate_aepe_stats(gt, flow)
        self.assertAlmostEqual(0.0, aepe)
        self.assertAlmostEqual(0.0, low_speed[0])
        self.assertAlmostEqual(0.0, med_speed[0])
        self.assertAlmostEqual(0.0, hi_speed[0])
        self.assertAlmostEqual(5.0, low_speed[1])
        self.assertAlmostEqual(0.0, med_speed[1])
        self.assertAlmostEqual(0.0, hi_speed[1])

    def test_low_aepe(self):
        gt = np.asarray([[0.0, 0.0], [1.0, 0.0]])
        flow = np.asarray([[0.0, 0.0], [-3.0, 3.0]])
        aepe, low_speed, med_speed, hi_speed = calculate_aepe_stats(gt, flow)
        self.assertAlmostEqual(2.5, aepe)
        self.assertAlmostEqual(5.0, low_speed[0])
        self.assertAlmostEqual(0.0, med_speed[0])
        self.assertAlmostEqual(0.0, hi_speed[0])
        self.assertAlmostEqual(2.0, low_speed[1])
        self.assertAlmostEqual(0.0, med_speed[1])
        self.assertAlmostEqual(0.0, hi_speed[1])

    def test_medium_aepe(self):
        gt = np.asarray([[0.0, 20.0], [1.0, 0.0]])
        flow = np.asarray([[0.0, 0.0], [-3.0, 3.0]])
        aepe, low_speed, med_speed, hi_speed = calculate_aepe_stats(gt, flow)
        self.assertAlmostEqual((5 + 20) / 2, aepe)
        self.assertAlmostEqual(5.0, low_speed[0])
        self.assertAlmostEqual(20.0, med_speed[0])
        self.assertAlmostEqual(0.0, hi_speed[0])
        self.assertAlmostEqual(1.0, low_speed[1])
        self.assertAlmostEqual(1.0, med_speed[1])
        self.assertAlmostEqual(0.0, hi_speed[1])

    def test_high_aepe(self):
        gt = np.asarray([[0.0, 20.0], [1.0, 0.0], [40.0, -20.0]])
        flow = np.asarray([[0.0, 0.0], [-3.0, 3.0], [0.0, 10.0]])
        aepe, low_speed, med_speed, hi_speed = calculate_aepe_stats(gt, flow)
        self.assertAlmostEqual((5 + 20 + 50) / 3, aepe)
        self.assertAlmostEqual(5.0, low_speed[0])
        self.assertAlmostEqual(20.0, med_speed[0])
        self.assertAlmostEqual(50.0, hi_speed[0])
        self.assertAlmostEqual(1.0, low_speed[1])
        self.assertAlmostEqual(1.0, med_speed[1])
        self.assertAlmostEqual(1.0, hi_speed[1])

    def test_multi_aepe(self):
        gt = np.asarray([[0.0, 20.0], [1.0, 0.0], [0.0, -1.0], [40.0, -20.0], [40.0, -30.0]])
        flow = np.asarray([[0.0, 0.0], [-3.0, 3.0], [-3.0, 3.0], [0.0, 10.0], [0.0, 0.0]])
        aepe, low_speed, med_speed, hi_speed = calculate_aepe_stats(gt, flow)
        self.assertAlmostEqual((5 + 5 + 20 + 50 + 50) / 5, aepe)
        self.assertAlmostEqual(10.0, low_speed[0])
        self.assertAlmostEqual(20.0, med_speed[0])
        self.assertAlmostEqual(100.0, hi_speed[0])
        self.assertAlmostEqual(2.0, low_speed[1])
        self.assertAlmostEqual(1.0, med_speed[1])
        self.assertAlmostEqual(2.0, hi_speed[1])

    def test_multi_aepe_valid_mask(self):
        gt = np.asarray([[0.0, 5.0], [0.0, 20.0], [1.0, 0.0], [0.0, -1.0], [40.0, -20.0], [40.0, -20.0], [40.0, -30.0]])
        flow = np.asarray([[0.0, 0.0], [0.0, 0.0], [-3.0, 3.0], [-3.0, 3.0], [0.0, 10.0], [0.0, 10.0], [0.0, 0.0]])
        valid = np.asarray([[0.0], [1.0], [1.0], [1.0], [1.0], [0.0], [1.0]])
        aepe, low_speed, med_speed, hi_speed = calculate_aepe_stats(gt, flow, valid)
        self.assertAlmostEqual((5 + 5 + 20 + 50 + 50) / 5, aepe)
        self.assertAlmostEqual(10.0, low_speed[0])
        self.assertAlmostEqual(20.0, med_speed[0])
        self.assertAlmostEqual(100.0, hi_speed[0])
        self.assertAlmostEqual(2.0, low_speed[1])
        self.assertAlmostEqual(1.0, med_speed[1])
        self.assertAlmostEqual(2.0, hi_speed[1])

    def test_multi_percent_error(self):
        gt = np.asarray([[0.0, 20.0], [1.0, 0.0], [0.0, -1.0], [40.0, -30.0], [-30.0, 40.0]])
        flow = np.asarray([[0.0, 0.0], [-3.0, 3.0], [-3.0, 3.0], [0.0, 0.0], [0.0, 0.0]])
        aepe, low_speed, med_speed, hi_speed = calculate_aepe_stats(gt, flow, percent_error=True)
        self.assertAlmostEqual((1.0 + 5.0 + 5.0 + 1.0 + 1.0) / 5, aepe)
        self.assertAlmostEqual(10.0, low_speed[0])
        self.assertAlmostEqual(1.0, med_speed[0])
        self.assertAlmostEqual(2.0, hi_speed[0])
        self.assertAlmostEqual(2.0, low_speed[1])
        self.assertAlmostEqual(1.0, med_speed[1])
        self.assertAlmostEqual(2.0, hi_speed[1])


if __name__ == '__main__':
    unittest.main()
