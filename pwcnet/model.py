import os
import shutil
import tensorflow as tf
from common.models import RestorableNetwork, FrozenModel
from common.utils.tf import he_normal
from pwcnet.bilateral_filter.bilateral_filter import bilateral_filter
from pwcnet.estimator_network.model import EstimatorNetwork
from pwcnet.flcon.flcon import FLocalConv
from pwcnet.context_network.model import ContextNetwork
from pwcnet.feature_pyramid_network.model import FeaturePyramidNetwork
from pwcnet.upres_network.model import UpResNetwork
from pwcnet.losses.loss import create_multi_level_loss, l2_diff, lq_diff, create_multi_level_unflow_loss
from tensorflow.contrib.layers import l2_regularizer


VERBOSE = False


class PWCNet(RestorableNetwork):
    # Number of times the flow is estimated and refined.
    # If this number changes, then the feature_pyramid needs to be reconfigured.
    NUM_FEATURE_LEVELS = 6
    # This is the output feature level. This can be anywhere between [1, self.num_feature_levels].
    OUTPUT_LEVEL = 2

    def __init__(self, name='pwc_net', regularizer=l2_regularizer(4e-4), flow_scaling=0.05, search_range=4,
                 kernel_initializer=he_normal(), residual=True, use_flcon=True, bilateral_range_stddev=0.5,
                 norm=None, deeper_feature_pyramid=True, upres=False):
        """
        :param name: Str.
        :param regularizer: Tf regularizer.
        :param flow_layer_loss_weights: List of floats. Corresponds to the weight of a loss for a flow at some layer.
                                        i.e. flow_layer_loss_weights[0] corresponds to previous_flows[0].
        :param flow_scaling: In the PWC-Net paper, ground truth is scaled by this amount to normalize the flows.
        :param search_range: The search range to use for the cost volume layer.
        :param kernel_initializer: Tensorflow kernel initializer.
        :param residual: Bool. Whether the estimator networks should be residual.
        :param use_flcon: Bool.
        :param bilateral_range_stddev: Float. Bilateral filter range/value standard deviation.
                                       No filter if less than 0. Units are in pixels.
        :param norm: Str. None, ConvNetwork.Norm.BATCH_NORM, or ConvNetwork.Norm.GROUP_NORM.
        :param deeper_feature_pyramid: Bool. Whether to use a deeper feature pyramid architecture.
        :param upres: Bool. Whether to use an upresing context network instead of the context network.
        """
        super().__init__(name=name)

        self.regularizer = regularizer
        self.flow_scaling = flow_scaling
        self.kernel_initializer = kernel_initializer
        self.residual = residual
        self.use_flcon = use_flcon
        self.bilateral_range_stddev = bilateral_range_stddev * self.flow_scaling
        self.norm = norm
        self.deeper_feature_pyramid = deeper_feature_pyramid
        self.enclosing_scope = tf.get_variable_scope()
        self.upres = upres

        # A range that counts down from [self.num_flow_estimates, self.output_level] inclusive.
        self.iter_range = range(self.NUM_FEATURE_LEVELS, self.OUTPUT_LEVEL - 1, -1)

        self.feature_pyramid = FeaturePyramidNetwork(regularizer=self.regularizer,
                                                     kernel_initializer=self.kernel_initializer,
                                                     norm=self.norm, deeper_feature_pyramid=self.deeper_feature_pyramid)
        self.estimator_networks = [EstimatorNetwork(name='estimator_network_' + str(i),
                                                    regularizer=self.regularizer,
                                                    search_range=search_range,
                                                    kernel_initializer=self.kernel_initializer,
                                                    residual=self.residual)
                                   for i in self.iter_range]
        self.context_network = ContextNetwork(regularizer=self.regularizer, kernel_initializer=self.kernel_initializer)
        self.upres_network = UpResNetwork(name='upres_network', regularizer=self.regularizer,
                                          kernel_initializer=self.kernel_initializer)
        self.flcon_network = FLocalConv(name='flcon', regularizer=self.regularizer,
                                        kernel_initializer=self.kernel_initializer, flow_scaling=self.flow_scaling,
                                        lconv_kernel_size=7)
        if self.upres or self.use_flcon or self.bilateral_range_stddev > 0:
            self.flow_layer_loss_weights = [12.5, 12.5, 12.5, 25, 25, 50]
        else:
            self.flow_layer_loss_weights = [12.5, 12.5, 12.5, 25, 0, 50]
        if self.upres:
            assert not self.use_flcon, 'Flcon is currently not supported with the upres network.'

    def get_forward(self, image_a, image_b, reuse_variables=tf.AUTO_REUSE, training=False):
        """
        :param image_a: Tensor of shape [batch_size, H, W, 3].
        :param image_b: Tensor of shape [batch_size, H, W, 3].
        :param reuse_variables: tf reuse option. i.e. tf.AUTO_REUSE.
        :param training: Python bool or Tensorflow bool. Switches between training and test mode (i.e. for batch norm).
        :return: final_flow: up-sampled final flow.
                 previous_flows: all previous flow outputs of the estimator networks and the context network.
                 extras: A dictionary of extra outputs including feature pyramid features.
        """
        self.enclosing_scope = tf.get_variable_scope()
        with tf.variable_scope(self.name, reuse=reuse_variables):
            with tf.name_scope('input_normalization'):
                mean_a, var_a = tf.nn.moments(image_a, axes=[1, 2], keep_dims=True)
                mean_b, var_b = tf.nn.moments(image_b, axes=[1, 2], keep_dims=True)
                image_a = (image_a - mean_a) / tf.sqrt(var_a + 1e-8)
                image_b = (image_b - mean_b) / tf.sqrt(var_b + 1e-8)

            batch_size, img_height, img_width, _ = self._get_dimensions(image_a)
            # Siamese networks (i.e. image_a and image_b are fed through the same network with shared weights).
            # Implemented by combining the the image_a and image_b batches.
            images_a_b = tf.concat([image_a, image_b], axis=0)
            _, features = self.feature_pyramid.get_forward(images_a_b, reuse_variables=reuse_variables,
                                                           training=training)

            # The initial flow is None. The estimator not do warping if flow is None.
            # It is refined at each feature level.
            previous_flow = None
            previous_flows = []
            # Intermediate features from the previous estimator network.
            previous_estimator_features = None

            # Counts down from [self.num_flow_estimates, self.output_level] inclusive.
            features_a_n = None
            for i in self.iter_range:
                if VERBOSE:
                    print('Creating estimator at level', i)
                # Get the features at this level.
                features_a_n, features_b_n = self._get_image_features_for_level(features, i, batch_size)

                # Setup the previous flow and feature map for input into the estimator network at this level.
                with tf.name_scope('resize_' + str(i)):
                    resized_flow, pre_warp_scaling = self._create_upsampled_flow_for_next_estimator(
                        previous_flow, img_height, name='resize_previous_flow' + str(i))
                    upsampled_previous_features = self._create_upsampled_features_for_next_estimator(
                        previous_estimator_features, name='deconv_estimator_features_' + str(i))

                # Get the estimator network output for this level.
                estimator_network = self.estimator_networks[self.NUM_FEATURE_LEVELS - i]
                if VERBOSE:
                    print('Getting forward ops for', estimator_network.name)
                previous_flow, estimator_outputs, dense_outputs = estimator_network.get_forward(
                    features_a_n, features_b_n, resized_flow, upsampled_previous_features,
                    pre_warp_scaling=pre_warp_scaling, reuse_variables=reuse_variables, training=training)
                if not self.residual:
                    assert estimator_outputs[-1] == previous_flow
                if self.bilateral_range_stddev > 0 and i <= 4:
                    previous_flow = bilateral_filter(previous_flow, kernel_size=5,
                                                     range_std_dev=self.bilateral_range_stddev, spacial_std_dev=1.5)
                previous_flows.append(previous_flow)

                # Get the previous_estimator_features depending on whether the estimator is dense.
                if estimator_network.dense_net:
                    assert len(dense_outputs) > 1
                    previous_estimator_features = dense_outputs[-2]
                else:
                    assert len(estimator_outputs) > 1
                    previous_estimator_features = estimator_outputs[-2]

            # Last level gets the context-network treatment.
            if not self.upres:
                if VERBOSE:
                    print('Getting forward ops for context network.')
                # Features are the second to last output of the estimator network.
                previous_flow, _ = self.context_network.get_forward(
                    previous_estimator_features, previous_flow, reuse_variables=reuse_variables, training=training)
            else:
                if VERBOSE:
                    print('Getting forward ops for up-res network.')
                # Increase the resolution of the final flow.
                quarter_features = previous_estimator_features
                half_features = self._get_image_features_for_level(features, 1, batch_size)[0]
                previous_flow = self.upres_network.get_forward(previous_flow, quarter_features, half_features,
                                                               reuse_variables=reuse_variables, training=training)
            # Apply flow regularization to the output.
            if self.use_flcon and not self.upres:
                previous_flow = self.flcon_network.get_forward(previous_flow, features_a_n, image_a, image_b,
                                                               reuse_variables=reuse_variables, training=training)
            elif self.bilateral_range_stddev > 0:
                previous_flow = bilateral_filter(previous_flow, kernel_size=9 if self.upres else 7,
                                                 range_std_dev=self.bilateral_range_stddev,
                                                 spacial_std_dev=3.5 if self.upres else 2.5)

            previous_flows.append(previous_flow)

            # Un-scale the final flow.
            if not self.upres:
                previous_flow = tf.image.resize_bilinear(previous_flow, [img_height, img_width], align_corners=True)
            final_flow = tf.divide(previous_flow, self.flow_scaling, name='final_flow')
            return final_flow, previous_flows, {'features': self._get_all_features(features)}

    def get_bidirectional(self, image_a, image_b, reuse_variables=tf.AUTO_REUSE, training=False):
        """
        Gets the bidirectional flow using a siamese PWC Net.
        :param image_a: Tensor of shape [batch_size, H, W, C].
        :param image_b: Tensor of shape [batch_size, H, W, C].
        :param reuse_variables: Tensorflow reuse variable parameter. Can be False, True, or tf.AUTO_REUSE.
        :param training: Python bool or Tensorflow bool. Switches between training and test mode (i.e. for batch norm).
        :return: final_forward_flow: up-sampled final forward flow.
                 final_backward_flow: up-sampled final backward flow.
                 previous_forward_flows: all previous flow outputs of the estimator networks and the context network.
                 previous_backward_flows: all previous flow outputs of the estimator networks and the context network.
                 extras_forward: Dictionary of extras from the forward pass.
                 extras_backward: Dictionary of extras from the backward pass.
        """
        with tf.name_scope('preprocess_bidirectional'):
            batch_size = tf.shape(image_a)[0]
            double_batch_size = batch_size * 2
            input_stack_a = tf.concat([image_a, image_b], axis=0)
            input_stack_b = tf.concat([image_b, image_a], axis=0)

        final_flow, previous_flows, extras = self.get_forward(input_stack_a, input_stack_b,
                                                              reuse_variables=reuse_variables, training=training)

        with tf.name_scope('postprocess_bidirectional'):
            final_forward_flow = final_flow[0:batch_size, ...]
            final_backward_flow = final_flow[batch_size:, ...]
            previous_forward_flows = []
            previous_backward_flows = []
            for previous_flow in previous_flows:
                previous_forward_flows.append(previous_flow[:batch_size, ...])
                previous_backward_flows.append(previous_flow[batch_size:, ...])
            extras_forward = {'features': []}
            extras_backward = {'features': []}
            features = extras['features']
            for feature in features:
                extras_forward['features'].append(feature[:double_batch_size, ...])
                extras_backward['features'].append(feature[double_batch_size:, ...])

        return (final_forward_flow, final_backward_flow, previous_forward_flows, previous_backward_flows,
                extras_forward, extras_backward)

    def _get_dimensions(self, tensor, name='dimensions'):
        """
        :param tensor: 4D tensor.
        :param name: Str.
        :return: B, H, W, C dimensions.
        """
        with tf.name_scope(name):
            return tf.unstack(tf.shape(tensor))

    def _get_image_features_for_level(self, feature_levels, level, batch_size):
        """
        Extracts the features for image_a and image_b from the feature pyramid.
        :param feature_levels: List of features from the feature pyramid.
        :param level: Int.
        :param batch_size: Scalar tensor.
        :return: features_a_n, features_b_n: Tensors of shape [batch_size, height, width, channels].
        """
        features_n = feature_levels[self.feature_pyramid.get_c_n_idx(level)]
        with tf.name_scope('features_a_' + str(level)):
            features_a_n = features_n[0:batch_size, ...]
        with tf.name_scope('features_b_' + str(level)):
            features_b_n = features_n[batch_size:, ...]
        return features_a_n, features_b_n

    def _create_upsampled_flow_for_next_estimator(self, previous_flow, img_height, name):
        """
        Scales the flow for the next estimator network. Also computes the pre-warp scaling to denormalize the flow.
        :param previous_flow: Tensor of shape [batch, height, width, 2] or None.
        :param img_height: Int or tensor. Height of the network's input image.
        :param name: Str.
        :return: resized_flow: Tensor of shape [batch, height/2, width/2, 2]. None if previous_flow is None.
                 pre_warp_scaling: Scalar tensor. 1.0 if previous_flow is None.
        """
        with tf.name_scope(name):
            pre_warp_scaling = 1.0
            resized_flow = None
            if previous_flow is not None:
                # Official PWC Net Caffe model disables the bias.
                resized_flow = tf.layers.conv2d_transpose(
                    previous_flow, filters=2, kernel_size=4, strides=2, padding='same', use_bias=False,
                    kernel_regularizer=None, name=name, kernel_initializer=self.kernel_initializer)
                # The original scale flows at all layers is the same as the scale of the ground truth.
                desired_height = tf.shape(resized_flow)[1]
                dimension_scaling = tf.cast(desired_height, tf.float32) / tf.cast(img_height, tf.float32)
                pre_warp_scaling = dimension_scaling / self.flow_scaling
        return resized_flow, pre_warp_scaling

    def _create_upsampled_features_for_next_estimator(self, features, name):
        """
        Upsamples a feature map and encodes it into 2 channels.
        :param features: Tensor of shape [batch, height, width, channels] or None.
        :param name: Str.
        :return: Tensor of shape [batch, height * 2, width * 2, 2] or None if features is none.
        """
        upsampled_features = None
        if features is not None:
            # Official PWC Net Caffe model disables the bias.
            upsampled_features = tf.layers.conv2d_transpose(
                features, filters=2, kernel_size=4, strides=2, padding='same', use_bias=False,
                kernel_regularizer=None, name=name, kernel_initializer=self.kernel_initializer)
        return upsampled_features

    def _get_all_features(self, feature_levels):
        """
        :param feature_levels: List of features from the feature pyramid.
        :return: List of features [C1, C2, C3, C4, C5, C6] (see the pyramid implementation) from the feature pyramid.
        """
        c = []
        for level in range(1, PWCNet.NUM_FEATURE_LEVELS + 1):
            c.append(feature_levels[self.feature_pyramid.get_c_n_idx(level)])
        return c

    def _get_loss(self, previous_flows, expected_flow, diff_fn, loss_mask, add_regularization):
        """
        :param previous_flows: List of previous outputs from the PWC-Net forward pass.
        :param expected_flow: Tensor of shape [batch_size, H, W, 2]. These are the ground-truth labels.
        :param diff_fn: Function to diff 2 tensors.
        :param loss_mask: Tensor of shape [B, H, W, 1] to mask the loss.
        :param add_regularization: Whether to add regularization.
        :return: Tf scalar loss term, an array of all the inidividual loss terms, and a regularization loss scalar.
        """

        total_loss, layer_losses = create_multi_level_loss(
            expected_flow=expected_flow, flows=previous_flows, flow_scaling=self.flow_scaling, diff_fn=diff_fn,
            loss_mask=loss_mask, flow_layer_loss_weights=self.flow_layer_loss_weights)

        # Add the regularization loss.
        regularization_loss = 0
        if add_regularization:
            regularization_losses = tf.losses.get_regularization_losses(scope='.*' + self.name)
            if regularization_losses is not None and len(regularization_losses) > 0:
                regularization_loss = tf.add_n(regularization_losses)
                total_loss += regularization_loss

        return total_loss, layer_losses, regularization_loss

    def get_training_loss(self, previous_flows, expected_flow, loss_mask=None, add_regularization=True):
        """
        Uses an L2 diffing loss.
        :param previous_flows: Flows at different levels.
        :param expected_flow: Ground truth.
        :param loss_mask: Tensor of shape [B, H, W, 1] to mask the loss.
        :param add_regularization: Whether to add regularization.
        :return: Tf scalar loss term, an array of all the inidividual loss terms, and a regularization loss scalar.
        """
        with tf.name_scope('training_loss'):
            return self._get_loss(previous_flows, expected_flow, l2_diff, loss_mask, add_regularization)

    def get_fine_tuning_loss(self, previous_flows, expected_flow, q=0.4, epsilon=0.01, loss_mask=None,
                             add_regularization=True):
        """
        Uses an Lq diffing loss.
        :param previous_flows: Flows at different levels.
        :param expected_flow: Ground truth.
        :param q: Float.
        :param epsilon: Float.
        :param loss_mask: Tensor of shape [B, H, W, 1] to mask the loss.
        :param add_regularization: Whether to add regularization.
        :return: Tf scalar loss term, an array of all the inidividual loss terms, and a regularization loss scalar.
        """
        with tf.name_scope('fine_tuning_loss'):
            def lq_diff_with_args(a, b):
                return lq_diff(a, b, q=q, epsilon=epsilon)
            return self._get_loss(previous_flows, expected_flow, lq_diff_with_args, loss_mask, add_regularization)

    def get_unflow_training_loss(self, image_a, image_b, previous_forward_flows, previous_backward_flows):
        """
        :param image_a: Tensor of shape [B, H, W, 3].
        :param image_b: Tensor of shape [B, H, W, 3].
        :param previous_forward_flows: List of flow tensors at different resolution levels.
        :param previous_backward_flows: List of flow tensors at different resolution levels.
        :return: total_loss: Scalar tensor. Sum off all weighted level losses.
                 layer_losses: List of scalar tensors. Weighted loss at each level.
                 forward_occlusion_masks: List of tensors of shape [B, H, W, 1].
                 backward_occlusion_masks: List of tensors of shape [B, H, W, 1].
                 layer_losses_detailed: List of dicts. Each dict contains the individual fully weighted losses.
        """
        with tf.name_scope('unflow_training_loss'):
            losses = create_multi_level_unflow_loss(image_a, image_b, previous_forward_flows, previous_backward_flows,
                                                    self.flow_scaling)
            total_loss, layer_losses, forward_occlusion_masks, backward_occlusion_masks, layer_losses_detailed = losses

            # Add the regularization loss.
            regularization_losses = tf.losses.get_regularization_losses(scope='.*' + self.name)
            if regularization_losses is not None and len(regularization_losses) > 0:
                total_loss += tf.add_n(regularization_losses)

            return total_loss, layer_losses, forward_occlusion_masks, backward_occlusion_masks, layer_losses_detailed

    def init_forward_with_placeholders(self, training=False):
        """
        Convenience function to create input and output tensors for getting the forward flow.
        :param training: Python bool or Tensorflow bool. Switches between training and test mode (i.e. for batch norm).
        :return: image_a_tensor: Tensor of shape [None, None, None, 3].
                 image_b_tensor: Tensor of shape [None, None, None, 3].
                 flow_tensor: Tensor of shape [None, None, None, 2].
                 extras: A dictionary of extra outputs including feature pyramid features.
        """
        image_a_tensor = tf.placeholder(shape=[None, None, None, 3], dtype=tf.float32)
        image_b_tensor = tf.placeholder(shape=[None, None, None, 3], dtype=tf.float32)
        flow_tensor, _, extras = self.get_forward(image_a_tensor, image_b_tensor, training=training)
        return image_a_tensor, image_b_tensor, flow_tensor, extras

    @staticmethod
    def create_frozen_model(directory):
        """
        Creates an immutable model that can't be trained.
        :param directory: Str. Directory of the saved/frozen model.
        :return: FrozenModel
        """
        return PWCNet._FrozenModelImpl(directory)

    class _FrozenModelImpl(FrozenModel):
        def __init__(self, directory):
            super().__init__(directory)
            self.inputs = None
            self.outputs = None

        def freeze(self, model, session):
            """
            Overridden.
            """
            assert isinstance(model, PWCNet)
            assert isinstance(session, tf.Session)
            if self.inputs is None or self.outputs is None:
                with tf.variable_scope(model.enclosing_scope):
                    images_0_ph, images_1_ph, flow_tensor, extras = model.init_forward_with_placeholders(training=False)
                self.inputs = {'images_0': images_0_ph, 'images_1': images_1_ph}
                self.outputs = {'output': flow_tensor}
                # Add extras.
                for i, feature in enumerate(extras['features']):
                    self.outputs['feature_' + str(i)] = feature
            if os.path.exists(self.directory) and len(os.listdir(self.directory)) > 0:
                shutil.rmtree(self.directory)
            tf.saved_model.simple_save(session, self.directory, self.inputs, self.outputs)
            print('Saved frozen model at:', self.directory)

        def run(self, inputs):
            """
            Overridden.
            :param inputs: Tuple of 2 elements:
                    Numpy array. Image of shape [B, H, W, C]. The image at t = 0.
                    Numpy array. Image of shape [B, H, W, C]. The image at t = 1.
            :return: Numpy array of shape [B, H, W, 2].
                     extras: A dictionary of extra outputs including feature pyramid features.
            """
            assert self.predictor is not None, 'load() must be called before run().'
            assert isinstance(inputs, tuple) and len(inputs) == 2, 'inputs must be a tuple of 2 numpy arrays.'
            outputs = self.predictor({
                'images_0': inputs[0],
                'images_1': inputs[1]
            })
            extras = {'features': []}
            for i in range(PWCNet.NUM_FEATURE_LEVELS):
                key = 'feature_' + str(i)
                if key in outputs:
                    extras['features'].append(outputs[key])
            return outputs['output'], extras
