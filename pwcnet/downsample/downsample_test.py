import numpy as np
import tensorflow as tf
import unittest
import os
from pwcnet.downsample.downsample import downsample
from common.utils.flow import read_flow_file, get_flow_visualization
from common.utils.img import write_image


class TestDownsample(unittest.TestCase):
    def setUp(self):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)
        self.flow_a_b_path = os.path.join('common', 'forward_warp', 'test_data', 'flow_ab.flo')
        self.test_output_folder = os.path.join('output_test_images', 'test_flow_downsample')
        if not os.path.exists(self.test_output_folder):
            os.makedirs(self.test_output_folder, exist_ok=True)
        np.set_printoptions(precision=3)

    def test_identity(self):
        image_shape = (1, 6, 4, 1)
        image = np.ones(image_shape)
        image_tensor = tf.placeholder(shape=image_shape, dtype=tf.float32)
        output_height_tensor = tf.shape(image_tensor)[1]
        output_width_tensor = tf.shape(image_tensor)[2]
        resized_tensor = downsample(image_tensor, output_height_tensor, output_width_tensor)
        resized = self.sess.run(resized_tensor, feed_dict={image_tensor: image})
        self.assertEqual(resized.tolist(), image.tolist())

    def test_zeros_image(self):
        image_shape = (1, 6, 4, 1)
        output_height = 3
        output_width = 2
        output_shape = (1, output_height, output_width, 1)
        image = np.zeros(image_shape)
        expected = np.zeros(output_shape)
        image_tensor = tf.placeholder(shape=image_shape, dtype=tf.float32)
        output_height_tensor = tf.shape(image_tensor)[1] / 2
        output_width_tensor = tf.shape(image_tensor)[2] / 2
        resized_tensor = downsample(image_tensor, output_height_tensor, output_width_tensor)
        resized = self.sess.run(resized_tensor, feed_dict={image_tensor: image})
        self.assertEqual(resized.tolist(), expected.tolist())

    def test_ones_image(self):
        image_shape = (1, 6, 4, 1)
        output_height = 3
        output_width = 2
        output_shape = (1, output_height, output_width, 1)
        image = np.ones(image_shape)
        expected = np.ones(output_shape)
        image_tensor = tf.placeholder(shape=image_shape, dtype=tf.float32)
        output_height_tensor = tf.shape(image_tensor)[1] / 2
        output_width_tensor = tf.shape(image_tensor)[2] / 2
        resized_tensor = downsample(image_tensor, output_height_tensor, output_width_tensor)
        resized = self.sess.run(resized_tensor, feed_dict={image_tensor: image})
        self.assertEqual(resized.tolist(), expected.tolist())

    def test_mixed_image(self):
        image_shape = (1, 4, 8, 1)
        image = np.ones(image_shape)
        image[:, 2, 2, :] = 0.0

        # To be honest I don't know why it's this value, it's just a regression test.
        expected = [[[[0.97], [0.988]], [[0.94], [0.976]]]]
        image_tensor = tf.placeholder(shape=image_shape, dtype=tf.float32)
        output_height_tensor = tf.shape(image_tensor)[1] / 2
        output_width_tensor = tf.shape(image_tensor)[2] / 4
        resized_tensor = downsample(image_tensor, output_height_tensor, output_width_tensor)
        resized = self.sess.run(resized_tensor, feed_dict={image_tensor: image})
        self.assertTrue(np.max(expected - resized) < 1E-10)

    def test_mixed_image_channels(self):
        image_shape = (1, 4, 6, 3)
        image = np.ones(image_shape)
        image[:, 1, 1, 1] = 0.0

        # To be honest I don't know why it's this value, it's just a regression test.
        expected = [[[[1.0, 0.8888888359069824, 1.0], [1.0, 0.944444477558136, 1.0], [1.0, 1.0, 1.0]],
                     [[1.0, 0.9444443583488464, 1.0], [1.0, 0.9722222685813904, 1.0], [1.0, 1.0, 1.0]]]]
        image_tensor = tf.placeholder(shape=image_shape, dtype=tf.float32)
        output_height_tensor = tf.shape(image_tensor)[1] / 2
        output_width_tensor = tf.shape(image_tensor)[2] / 2
        resized_tensor = downsample(image_tensor, output_height_tensor, output_width_tensor)
        resized = self.sess.run(resized_tensor, feed_dict={image_tensor: image})
        self.assertTrue(np.max(expected - resized) < 1E-10)

    def test_mixed_image_batch(self):
        image_shape = (2, 4, 6, 3)
        image = np.ones(image_shape)
        image[0, 1, 1, 1] = 2.0

        # To be honest I don't know why it's this value, it's just a regression test.
        expected = [[[[1.0, 1.111111044883728, 1.0], [1.0, 1.0555554628372192, 1.0], [1.0, 1.0, 1.0]],
                     [[1.0, 1.0555554628372192, 1.0], [1.0, 1.0277777910232544, 1.0], [1.0, 1.0, 1.0]]],
                    [[[1.0, 1.0, 1.0], [1.0, 1.0, 1.0], [1.0, 1.0, 1.0]],
                     [[1.0, 1.0, 1.0], [1.0, 1.0, 1.0], [1.0, 1.0, 1.0]]]]
        image_tensor = tf.placeholder(shape=image_shape, dtype=tf.float32)
        output_height_tensor = tf.shape(image_tensor)[1] / 2
        output_width_tensor = tf.shape(image_tensor)[2] / 2
        resized_tensor = downsample(image_tensor, output_height_tensor, output_width_tensor)
        resized = self.sess.run(resized_tensor, feed_dict={image_tensor: image})
        self.assertTrue(np.max(expected - resized) < 1E-10)

    def test_resize_flow(self):
        # Downsample it by a factor of 2^5 = 32.
        factor = 32
        flow_ab = [read_flow_file(self.flow_a_b_path)]
        flow_tensor = tf.placeholder(shape=np.shape(flow_ab), dtype=tf.float32)
        output_height = np.shape(flow_ab)[1] / factor
        output_width = np.shape(flow_ab)[2] / factor
        resized_tensor = downsample(flow_tensor, output_height, output_width)
        size_tensor = [tf.cast(output_height, tf.int32), tf.cast(output_width, tf.int32)]
        resized_bilinear = tf.image.resize_bilinear(flow_tensor, size_tensor, align_corners=True)
        resized_area = tf.image.resize_area(flow_tensor, size_tensor, align_corners=True)
        resized_images = self.sess.run([resized_tensor, resized_bilinear, resized_area],
                                       feed_dict={flow_tensor: flow_ab})
        resized, resized_bilinear, resized_area = resized_images
        flow_visualization = get_flow_visualization(resized[0])
        flow_visualization_bilinear = get_flow_visualization(resized_bilinear[0])
        flow_visualization_area = get_flow_visualization(resized_area[0])
        write_image(os.path.join(self.test_output_folder, 'full.png'), get_flow_visualization(flow_ab[0]))
        write_image(os.path.join(self.test_output_folder, 'resize_down_32.png'), flow_visualization)
        write_image(os.path.join(self.test_output_folder, 'resize_down_bilinear_32.png'), flow_visualization_bilinear)
        write_image(os.path.join(self.test_output_folder, 'resize_down_area_32.png'), flow_visualization_area)


if __name__ == '__main__':
    unittest.main()
