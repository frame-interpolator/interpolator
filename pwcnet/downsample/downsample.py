from common.utils.tf import load_op_library
import tensorflow as tf


# Load op library.
mod = load_op_library('downsample_op', 'build')


def downsample(image, output_height, output_width):
    """
    Downsamples image using a weighting scheme similar to bilinear interpolation but that takes into account
    all pixels in the "footprint" area.
    This operation is actually really sketchily written and breaks if output height or width has a 1.
    See: https://github.com/lmb-freiburg/flownet2/blob/master/src/caffe/layers/downsample_layer.cu
    commit 476d400834d96337165d8561a8ef469ecfea77cd.
    :param image: Tensor of shape [B, H, W, C].
    :param output_height: Int. Height of the output image. Must be >= H.
    :param output_width: Int. Width of the output image. Must be >= W.
    :return: Resized image of shape [B, output_height, output_width, C].
    """
    if mod is not None:
        size_tensor = tf.convert_to_tensor([output_height, output_width])
        size_tensor = tf.cast(size_tensor, tf.int32)
        resized = mod.downsample(image, size_tensor)
        return resized
    else:
        raise NotImplementedError('This operation was not implemented for the CPU.')
