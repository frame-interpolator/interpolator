#include "downsample_op.h"

#define EIGEN_USE_THREADS

#include <memory>
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/framework/types.h"
#include "tensorflow/core/lib/core/status.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/common_shape_fns.h"

typedef Eigen::GpuDevice GPUDevice;

class DownsampleOp : public OpKernel {
public:
    explicit DownsampleOp(OpKernelConstruction* context) : OpKernel(context) {}

    void Compute(OpKernelContext* context) override {
        const Tensor& input_image = context->input(0);
        const Tensor& output_size = context->input(1);

        // Assert correct number of dimensions.
        OP_REQUIRES(context, input_image.dims() == 4,
                    errors::InvalidArgument("input_image expecting a 4-D vector."));

        typename TTypes<float, 4>::ConstTensor input_data = input_image.tensor<float, 4>();
        typename TTypes<int, 1>::ConstTensor size_data = output_size.tensor<int, 1>();
        const int batch = input_data.dimension(0);
        const int in_height = input_data.dimension(1);
        const int in_width = input_data.dimension(2);
        const int in_channels = input_data.dimension(3);

        // Copy output size.
        auto size_vec = output_size.vec<int32>();
        const int output_height = size_vec(0);
        const int output_width = size_vec(1);

        // The input height and width should be greater than the output.
        OP_REQUIRES(context, in_height >= output_height,
                    errors::InvalidArgument("Input image height must be greater than or equal to output height."));
        OP_REQUIRES(context, in_width >= output_width,
                    errors::InvalidArgument("Input image width must be greater than or equal to output width."));

        // Set output data and shape, and allocate memory for output tensor.
        Tensor* output_image = NULL;
        TensorShape output_shape({ batch, output_height, output_width, in_channels});
        OP_REQUIRES_OK(context, context->allocate_output(0, output_shape, &output_image));

        // Run the op.
        typename TTypes<float, 4>::Tensor output_data = output_image->tensor<float, 4>();
        Downsample(context->eigen_device<GPUDevice>(), input_data, output_data);
    }
};

REGISTER_OP("Downsample")
.Input("image: float")
.Input("size: int32")
.Output("resized_images: float")
.SetShapeFn([](shape_inference::InferenceContext* c) {
    shape_inference::DimensionHandle batch = c->Dim(c->input(0), 0);
    shape_inference::DimensionHandle channels = c->Dim(c->input(0), 3);
    c->set_output(0, c->MakeShape({ batch, c->UnknownDim(), c->UnknownDim(), channels }));
    return Status::OK();
});

#if GOOGLE_CUDA

REGISTER_KERNEL_BUILDER(Name("Downsample")
.Device(DEVICE_GPU)
.HostMemory("size"), DownsampleOp);

#endif // GOOGLE_CUDA
