#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"

using namespace tensorflow;

void Downsample(const Eigen::GpuDevice& d,
    typename TTypes<float, 4>::ConstTensor images,
    typename TTypes<float, 4>::Tensor output);
