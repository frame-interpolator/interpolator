// See https://github.com/lmb-freiburg/flownet2/blob/master/src/caffe/layers/downsample_layer.cu
// commit 476d400834d96337165d8561a8ef469ecfea77cd
#if GOOGLE_CUDA

#define CUDART_NAN_F  __int_as_float(0x7fffffff)

#include "downsample_op.h"

#define EIGEN_USE_GPU

#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor_types.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/util/cuda_kernel_helper.h"

typedef Eigen::GpuDevice GPUDevice;

template <typename Dtype>
__global__ void DownsampleKernel(const int nthreads, const int num, const int channels, const int bottomwidth, const int bottomheight,
            const int topheight, const int topwidth, const int bot_countpernum, const int bot_numstride, const float widthScale, const float heightScale, const int wradius, const int hradius, const Dtype* src_data, Dtype* dest_data) {
  CUDA_1D_KERNEL_LOOP(index, nthreads) {
    // Note that the change from the original Caffe CUDA code is the array indexing (from NCHW to NHWC).
    // From top (large,src_data) to bottom (small,dst_data)
    int cn = (index / channels / topwidth);
    int desty = cn % topheight;
    int n = cn / topheight;

    int c = index % channels;
    int destx = (index / channels) % topwidth;
    
    //Compute source center pos in topdiff
    float botx = ((float)destx/(float)(topwidth-1)) * (float)(bottomwidth-1); // \in [0.0, (topwidth-1)]
    float boty = ((float)desty/(float)(topheight-1)) * (float)(bottomheight-1);
    
    int ibotx = round(botx);
    int iboty = round(boty);
    
    // Accumulate in range around that point:
    int botidxoff = bot_numstride*n;
    
    float accum_value = 0;
    float accum_weight = 0;
    float accum_nan = 0;

    for(int yoff = -hradius; yoff <= hradius; yoff++)  {
        int by = iboty + yoff;
        int botidxoffycn = by*bottomwidth*channels + botidxoff;
        for(int xoff = -wradius; xoff <= wradius; xoff++)  {
            int bx = ibotx + xoff;
            
            if(bx >= 0 && by >= 0 && bx < bottomwidth && by < bottomheight) {
                float sample = src_data[bx * channels + botidxoffycn + c];
                float weight = max(0.0f,1.0f-(abs((float)bx - botx)/widthScale)) * max(0.0f,1.0f- (abs((float)by - boty)/heightScale) );
                if(sample != sample) { //isnan
                  accum_nan += weight;
                  sample = 0;
                  weight = 0;
                }
                
                accum_value += sample * weight;
                accum_weight += weight;
            }
        }
    }
    if(accum_nan / accum_weight > 0.5) {
      dest_data[index] = CUDART_NAN_F;
    } else {
      dest_data[index] = accum_value / accum_weight;
    }

    //printf("dest x,y[%d,%d] n:%d c:%d = idx:%d | bot x,y[%d,%d] (int)val %d\n", destx, desty, n, c, index, ibotx, iboty, (int)src_data[ibotx + (iboty*bottomwidth + botidxoffcn)]);    
    //dest_data[index] = src_data[ibotx + (iboty*bottomwidth + botidxoffcn)];
  }
}

void Downsample(const GPUDevice& d,
    typename TTypes<float, 4>::ConstTensor image,
    typename TTypes<float, 4>::Tensor output) {

// Get output dimensions and count.
int topnum = output.dimension(0);
int topheight = output.dimension(1);
int topwidth = output.dimension(2);
int topchannels = output.dimension(3);
int topcount = topnum * topwidth * topheight * topchannels;

// Get input dimensions and count.
int bottomnum = image.dimension(0);
int bottomheight = image.dimension(1);
int bottomwidth = image.dimension(2);
int bottomchannels = image.dimension(3);
int bottomcount = bottomnum * bottomheight * bottomwidth * bottomchannels;

// Check if there's no resizing to do.
if (bottomwidth == topwidth && bottomheight == topheight) {
    cudaMemcpy(output.data(), image.data(), bottomcount * sizeof(float), cudaMemcpyDeviceToDevice);
    return;
}

// Get a bunch of other parameters for the actual downsampling operation.
// From bottom to top.
int bot_countpernum = bottomwidth * bottomheight * bottomchannels;
int bot_numstride = bottomwidth * bottomheight * bottomchannels;

float widthScale = (float)(bottomwidth-1) / (float)(topwidth-1); // e.g. 2.0 if bottom pixeldist half compared to top. 
float heightScale = (float)(bottomheight-1) / (float)(topheight-1);

const int wradius = ceil(widthScale); //One pixel from bottom is incfluenced by +- widthScale or heightScale pixels around that in top 
const int hradius = ceil(heightScale);

// Loop over: bottomwidth,bottomheight,bottomlayers. (x,y,l)
// Accumulate data from top_diff_chanoffsetptr, at
//  topx = (x/(bottomwidth-1)) [0.0, 1.0]  * (topwidth-1) = [0.0, (topwidth-1)]
//  topy = analogously
// in a rectangle around that point width range [-wradius,+wradius][-hradius,+hradius]
// and weight each toppixel with "closeness" (=max(0,1-xdist)*max(0,1-ydist)) to [topx,topy] but xdist and ydist scaled by widthScale and heightScale.

CudaLaunchConfig config = GetCudaLaunchConfig(topcount, d);
DownsampleKernel
<<<config.block_count, config.thread_per_block, 0, d.stream()>>>(
    topcount,
    bottomnum, bottomchannels, bottomwidth, bottomheight,
    topheight, topwidth, bot_countpernum, bot_numstride, widthScale, heightScale, wradius, hradius, image.data(), output.data());
}

#endif  // GOOGLE_CUDA
