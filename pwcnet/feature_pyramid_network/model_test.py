import numpy as np
import tensorflow as tf
import unittest
from pwcnet.feature_pyramid_network.model import FeaturePyramidNetwork
from tensorflow.contrib.layers import l2_regularizer


class TestFeaturePyramid(unittest.TestCase):
    def setUp(self):
        self.feature_pyr_net = FeaturePyramidNetwork(regularizer=l2_regularizer(1e-4))

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_network(self):
        """
        Sets up the network's forward pass and ensures that all shapes are expected.
        """
        height = 128
        width = 128
        num_features = 3
        batch_size = 3

        # Create the graph.
        input_image = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        final_features, layer_outputs = self.feature_pyr_net.get_forward(input_image)

        input_features = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        input_features[:, 10:height-10, 10:width-10, :] = 1.0

        self.sess.run(tf.global_variables_initializer())

        query = [final_features] + layer_outputs
        results = self.sess.run(query, feed_dict={input_image: input_features})

        self.assertEqual(len(results), 19)

        # Test that the default values are working.
        self.assertTrue(np.allclose(results[2].shape, np.asarray([batch_size, height/2, width/2, 16])))
        self.assertTrue(np.allclose(results[5].shape, np.asarray([batch_size, height/4, width/4, 32])))
        self.assertTrue(np.allclose(results[8].shape, np.asarray([batch_size, height/8, width/8, 64])))
        self.assertTrue(np.allclose(results[11].shape, np.asarray([batch_size, height/16, width/16, 96])))
        self.assertTrue(np.allclose(results[14].shape, np.asarray([batch_size, height/32, width/32, 128])))
        self.assertTrue(np.allclose(results[17].shape, np.asarray([batch_size, height/64, width/64, 192])))

        for i in range(1, 13):
            self.assertNotEqual(np.sum(results[i]), 0.0)

        # Test regularization losses.
        # 7 conv layers (kernels only no bias).
        reg_losses = tf.losses.get_regularization_losses(scope='feature_pyramid_network')
        self.assertEqual(len(reg_losses), 18)
        # Make sure the reg losses aren't 0.
        reg_loss_sum_tensor = tf.add_n(reg_losses)
        reg_loss_sum = self.sess.run(reg_loss_sum_tensor)
        self.assertNotEqual(reg_loss_sum, 0.0)

        # Test that we have all the trainable variables.
        trainable_vars = tf.trainable_variables(scope='feature_pyramid_network')
        self.assertEqual(len(trainable_vars), 36)

        # Check that the gradients are flowing.
        grad_op = tf.gradients(tf.reduce_mean(final_features), trainable_vars + [input_image])
        gradients = self.sess.run(grad_op, feed_dict={input_image: input_features})
        for gradient in gradients:
            self.assertNotAlmostEqual(np.sum(gradient), 0.0)

        c_3_tensor = layer_outputs[self.feature_pyr_net.get_c_n_idx(3)]
        c_3 = self.sess.run(c_3_tensor, feed_dict={input_image: input_features})
        self.assertTrue(np.allclose(c_3.shape, [batch_size, height/8, width/8, 64]))

        c_5_tensor = layer_outputs[self.feature_pyr_net.get_c_n_idx(5)]
        c_5 = self.sess.run(c_5_tensor, feed_dict={input_image: input_features})
        self.assertTrue(np.allclose(c_5.shape, [batch_size, height/32, width/32, 128]))


class TestShallowerFeaturePyramid(unittest.TestCase):
    def setUp(self):
        self.feature_pyr_net = FeaturePyramidNetwork(name='shallow_feature_pyramid', regularizer=l2_regularizer(1e-4),
                                                     deeper_feature_pyramid=False)

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_network(self):
        """
        Sets up the network's forward pass and ensures that all shapes are expected.
        """
        height = 128
        width = 128
        num_features = 3
        batch_size = 3

        # Create the graph.
        input_image = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        final_features, layer_outputs = self.feature_pyr_net.get_forward(input_image)

        input_features = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        input_features[:, 10:height-10, 10:width-10, :] = 1.0

        self.sess.run(tf.global_variables_initializer())

        query = [final_features] + layer_outputs
        results = self.sess.run(query, feed_dict={input_image: input_features})

        self.assertEqual(len(results), 13)

        # Test that the default values are working.
        self.assertTupleEqual((batch_size, height / 2, width / 2, 16), results[1].shape)
        self.assertTupleEqual((batch_size, height / 4, width / 4, 32), results[3].shape)
        self.assertTupleEqual((batch_size, height / 8, width / 8, 64), results[5].shape)
        self.assertTupleEqual((batch_size, height / 16, width / 16, 96), results[7].shape)
        self.assertTupleEqual((batch_size, height / 32, width / 32, 128), results[9].shape)
        self.assertTupleEqual((batch_size, height / 64, width / 64, 192), results[11].shape)

        for i in range(1, 13):
            self.assertNotEqual(np.sum(results[i]), 0.0)

        # Test regularization losses.
        reg_losses = tf.losses.get_regularization_losses(scope='shallow_feature_pyramid')
        self.assertEqual(len(reg_losses), 12)
        # Make sure the reg losses aren't 0.
        reg_loss_sum_tensor = tf.add_n(reg_losses)
        reg_loss_sum = self.sess.run(reg_loss_sum_tensor)
        self.assertNotEqual(reg_loss_sum, 0.0)

        # Test that we have all the trainable variables.
        trainable_vars = tf.trainable_variables(scope='shallow_feature_pyramid')
        self.assertEqual(len(trainable_vars), 24)

        c_3_tensor = layer_outputs[self.feature_pyr_net.get_c_n_idx(3)]
        c_3 = self.sess.run(c_3_tensor, feed_dict={input_image: input_features})
        self.assertTupleEqual((batch_size, height / 8, width / 8, 64), c_3.shape)

        c_5_tensor = layer_outputs[self.feature_pyr_net.get_c_n_idx(5)]
        c_5 = self.sess.run(c_5_tensor, feed_dict={input_image: input_features})
        self.assertTupleEqual((batch_size, height / 32, width / 32, 128), c_5.shape)


class TestFeaturePyramidNormalizations(unittest.TestCase):
    def setUp(self):
        self.feature_pyr_net = None

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config)

    def test_batch_norm(self):
        self.feature_pyr_net = FeaturePyramidNetwork(name='batch_norm_features', regularizer=l2_regularizer(1e-4),
                                                     norm='batch_norm')
        height = 128
        width = 128
        num_features = 3
        batch_size = 3

        # Create the graph.
        input_image = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        final_features, layer_outputs = self.feature_pyr_net.get_forward(input_image, training=True)

        input_features = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        input_features[:, 10:height-10, 10:width-10, :] = 1.0

        self.sess.run(tf.global_variables_initializer())

        query = [final_features] + layer_outputs
        results = self.sess.run(query, feed_dict={input_image: input_features})

        self.assertEqual(len(results), 19)

        # Test that the default values are working.
        self.assertTrue(np.allclose(results[2].shape, np.asarray([batch_size, height/2, width/2, 16])))
        self.assertTrue(np.allclose(results[5].shape, np.asarray([batch_size, height/4, width/4, 32])))
        self.assertTrue(np.allclose(results[8].shape, np.asarray([batch_size, height/8, width/8, 64])))
        self.assertTrue(np.allclose(results[11].shape, np.asarray([batch_size, height/16, width/16, 96])))
        self.assertTrue(np.allclose(results[14].shape, np.asarray([batch_size, height/32, width/32, 128])))
        self.assertTrue(np.allclose(results[17].shape, np.asarray([batch_size, height/64, width/64, 192])))

        for i in range(1, 13):
            self.assertNotEqual(np.sum(results[i]), 0.0)

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope='batch_norm_features')
        self.assertEqual(2, len(update_ops))

        # Test that we have all the trainable variables.
        trainable_vars = tf.trainable_variables(scope='batch_norm_features')
        self.assertEqual(len(trainable_vars), 38)

    def test_group_norm(self):
        self.feature_pyr_net = FeaturePyramidNetwork(name='group_norm_features', regularizer=l2_regularizer(1e-4),
                                                     norm='group_norm')
        height = 128
        width = 128
        num_features = 3
        batch_size = 3

        # Create the graph.
        input_image = tf.placeholder(shape=[None, height, width, num_features], dtype=tf.float32)
        final_features, layer_outputs = self.feature_pyr_net.get_forward(input_image, training=True)

        input_features = np.zeros(shape=[batch_size, height, width, num_features], dtype=np.float32)
        input_features[:, 10:height-10, 10:width-10, :] = 1.0

        self.sess.run(tf.global_variables_initializer())

        query = [final_features] + layer_outputs
        results = self.sess.run(query, feed_dict={input_image: input_features})

        self.assertEqual(len(results), 19)

        # Test that the default values are working.
        self.assertTrue(np.allclose(results[2].shape, np.asarray([batch_size, height/2, width/2, 16])))
        self.assertTrue(np.allclose(results[5].shape, np.asarray([batch_size, height/4, width/4, 32])))
        self.assertTrue(np.allclose(results[8].shape, np.asarray([batch_size, height/8, width/8, 64])))
        self.assertTrue(np.allclose(results[11].shape, np.asarray([batch_size, height/16, width/16, 96])))
        self.assertTrue(np.allclose(results[14].shape, np.asarray([batch_size, height/32, width/32, 128])))
        self.assertTrue(np.allclose(results[17].shape, np.asarray([batch_size, height/64, width/64, 192])))

        for i in range(1, 13):
            self.assertNotEqual(np.sum(results[i]), 0.0)

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope='group_norm_features')
        self.assertEqual(0, len(update_ops))

        # Test that we have all the trainable variables.
        trainable_vars = tf.trainable_variables(scope='batch_norm_features')
        self.assertEqual(len(trainable_vars), 38)


if __name__ == '__main__':
    unittest.main()
