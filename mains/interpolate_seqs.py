import argparse
import os
import glob
from common.utils import img
from common.utils.video import VideoWriter
from common.utils.misc import print_progress_bar
from common.utils.data import get_sequence_dict
from common.utils.interp_eval import load_saved_models, Interpolator


def main():
    """
    Images in the same shot should have the same name, followed by their index.
    For example, the input directory may look like this:
        foo_0.jpg
        foo_1.jpg
        foo_2.jpg
        lmao_0.jpg
        lmao_1.jpg
    """
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()
    assert args.every_other >= 0

    if not os.path.exists(args.output_directory):
        os.makedirs(args.output_directory)

    # Load model for inference.
    synthesizer_model, flow_model = load_saved_models(args.synthesis_model_dir, args.flow_model_dir)
    interpolator = Interpolator(synthesizer_model, flow_model)

    # Evaluate on each subdirectory.
    subdirs = [x[0] for x in os.walk(args.input_directory)]

    for subdir in subdirs:
        # Group files according to their prefix.
        groups = get_sequence_dict(subdir, recursive=False, use_underscore_sep=args.underscore_sep)
        cur_group = None
        writer = None

        # Form pair infos from groups.
        pair_infos = []
        for key, value in groups.items():
            for i in range(1 + args.every_other, len(value), 1 + args.every_other):
                pair_infos.append({
                    'group': key,
                    'start': int(value[i - 1 - args.every_other]),
                    'end': int(value[i])
                })

        for pair in pair_infos:
            name_sep = '_' if args.underscore_sep else ''
            path_0 = glob.glob(os.path.join(subdir, pair['group'] + name_sep + str(pair['start']) + '*'))
            path_1 = glob.glob(os.path.join(subdir, pair['group'] + name_sep + str(pair['end']) + '*'))
            assert len(path_0) == 1
            assert len(path_1) == 1
            img_0 = img.read_image(path_0[0], as_float=True)
            img_1 = img.read_image(path_1[0], as_float=True)
            interpolator.set_end_frames_and_compute_flow(img_0, img_1, use_spline=False)

            if cur_group != pair['group']:
                cur_group = pair['group']
                video_name = '_'.join(os.path.join(subdir, cur_group).split(os.path.sep)[1:])
                video_path = os.path.join(args.output_directory, video_name + '.avi')
                if writer is not None:
                    writer.release()
                writer = VideoWriter(video_path, fps=20)

            # Interpolate at each time step and save with appropriate timestamp.
            print('Interpolating between key-frames to get a sequence for ' + pair['group'] + '...')
            writer.write(img_0)
            for i in range(args.interpolation_steps):
                t = (i + 1) / (args.interpolation_steps + 1)
                interpolated = interpolator.get_interpolated(t)
                time_stamp = float(pair['start']) + t * (float(pair['end']) - float(pair['start']))
                time_stamp_str = ''.join(('%.2f' % time_stamp).split('.'))

                do_save_images = False
                if do_save_images:
                    # Save sequence of images to its own group folder.
                    image_name = '_'.join(os.path.join(subdir, cur_group).split(os.path.sep)[1:])
                    image_name += '_' + time_stamp_str + '.png'
                    images_folder = os.path.join(args.output_directory, pair['group'] + '_images')
                    if not os.path.exists(images_folder):
                        os.makedirs(images_folder)
                    out_path = os.path.join(images_folder, image_name)
                    img.write_image(out_path, interpolated)

                print_progress_bar(i + 1, args.interpolation_steps,
                                   prefix='Generating',
                                   suffix='Frames',
                                   use_percentage=False)

                writer.write(interpolated)
            writer.write(img_1)

        if writer is not None:
            writer.release()


def add_args(parser):
    parser.add_argument('-d', '--input_directory', type=str,
                        help='Input directory.')
    parser.add_argument('-s', '--synthesis_model_dir', type=str,
                        help='Path to the synthesizer SavedModel that we will load.')
    parser.add_argument('-f', '--flow_model_dir', type=str, default=None,
                        help='Path to the optical flow SavedModel that we will load.' +
                             'If not provided, we will use the flow model associated with the synthesizer, ' +
                             'if it is available')
    parser.add_argument('-o', '--output_directory', type=str,
                        help='Directory that we output into.')
    parser.add_argument('-i', '--interpolation_steps', type=int, default=12,
                        help='E.g For i = 2, we interpolate at t = 0.333 and t = 0.667')
    parser.add_argument('-u', '--underscore_sep', type=int, default=1,
                        help='Whether file names are formatted with an underscore separation ' +
                             'between the sequence name and the sequence idx.')
    parser.add_argument('-e', '--every_other', type=int, default=0,
                        help='The number of input images between keyframes. ' +
                             'E.g for every_other = 1, we use the 1st and 3rd, 2nd and 4th keyframes.')


if __name__ == '__main__':
    main()
