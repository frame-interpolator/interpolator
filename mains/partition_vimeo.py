"""
Script to create symlinks of all the test (or train) sequences from the vimeo septuplet dataset into their own folder.
"""
import argparse
import os
from shutil import copytree
import multiprocessing
from joblib import Parallel, delayed


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()
    input_dir = args.input_directory
    output_dir = args.output_directory
    use_symlink = bool(args.symlink)
    test_list_path = args.file_list_path
    if not os.path.exists(output_dir):
        os.makedirs(output_dir, exist_ok=True)

    # Each line in the test list is a sequence directory relative path that belongs to the test set.
    def _process_line(line):
        line = line.strip()
        print('Copying sequence ' + line + '...')
        src_sequence_dir = os.path.join(input_dir, 'sequences', line)
        dst_sequence_dir = os.path.join(output_dir, line)
        parent_dir = os.path.dirname(dst_sequence_dir)
        if not os.path.isdir(parent_dir):
            os.makedirs(parent_dir, exist_ok=True)
        if use_symlink:
            os.symlink(src_sequence_dir, dst_sequence_dir)
        else:
            copytree(src_sequence_dir, dst_sequence_dir)

    with open(test_list_path, 'r') as f:
        lines = f.readlines()
        total_len = len(lines)
        print('There are %d sequences in total to copy.' % total_len)
        n_jobs = multiprocessing.cpu_count()
        Parallel(n_jobs=n_jobs, backend="threading")(delayed(_process_line)(lines[i]) for i in range(total_len))


def add_args(parser):
    parser.add_argument('-i', '--input_directory', type=str,
                        help='Path to the raw vimeo folder.')
    parser.add_argument('-f', '--file_list_path', type=str,
                        help='Path to the file list specifying which sequences we will copy.')
    parser.add_argument('-o', '--output_directory', type=str,
                        help='Output directory that will host all the symlinks.')
    parser.add_argument('-s', '--symlink', type=int, default=1,
                        help='Whether to copy the files (0) or to use symlinks (1).')


if __name__ == "__main__":
    main()
