import argparse
import sys
import tensorflow as tf
from pwcnet.eval_utils import eval_net
from pwcnet.model import PWCNet


def print_help():
    print('Format is <Path to Image A>, <Path to Image B>, <Output Path (.flo or .png extension)>')


def run_loop(model):
    """
    Runs the main program loop.
    :param model: PWCNet frozen model.
    :return: Nothing.
    """
    sys.stdout.write('> ')
    sys.stdout.flush()
    line = sys.stdin.readline()
    while line:
        args = [arg.strip() for arg in line.split(',')]
        if len(args) > 0 and (args[0] == 'h' or args[0] == 'help'):
            print_help()
        elif len(args) == 3:
            image_a_path = args[0]
            image_b_path = args[1]
            output_flow_path = args[2]
            try:
                eval_net(model, image_a_path, image_b_path, output_flow_path)
            except Exception as e:
                print('Running the model failed for', image_a_path, image_b_path)
                print(e)
        else:
            print_help()
        sys.stdout.write('> ')
        sys.stdout.flush()
        line = sys.stdin.readline()
    print('')


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True

    print('Loading model...')
    model = PWCNet.create_frozen_model(args.directory)
    model.load(session_config_proto=config_proto)

    print('Enter commands:')
    run_loop(model)


def add_args(parser):
    parser.add_argument('-d', '--directory', type=str,
                        help='Path to the frozen model.')


if __name__ == "__main__":
    main()
