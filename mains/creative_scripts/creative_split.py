import argparse
import os
import os.path
from common.utils.config import import_json
from data.flow.creative.creative_preprocessor import CreativeFlowDataPreprocessor
from random import shuffle


def read_scenes(filelist_paths):
    # Dictionary of lists.
    # First key indexes the scene, which returns the list of path tuples.
    scenes = {}

    frame_count = 0
    for filelist_path in filelist_paths:
        # Read file line by line.
        f = open(filelist_path, 'r')
        while True:
            line = f.readline()
            if not line:
                break
            if line == '':
                break
            # Parse the line.
            # Format is "mixamo/<path/to>/frame_a.png mixamo/<path/to>/frame_b.png mixamo/<path/to>/flow_ab.flo"
            # There is an optional 4th field for objectids.txt.
            paths = line.split()
            assert len(paths) == 3 or len(paths) == 4
            dirs = paths[0].split('/')
            scene = os.path.join(dirs[0], dirs[1])
            if scene not in scenes:
                scenes[scene] = []
            scenes[scene].append(line)
            frame_count += 1
        f.close()
    print(frame_count, 'frame(s) in total.')
    return scenes


def write_output(scenes_dict, scenes, output_dir, file_name):
    output_file = os.path.join(output_dir, file_name)
    try:
        os.remove(output_file)
    except OSError:
        pass
    with open(output_file, 'w') as file:
        for scene in scenes:
            path_tuples = scenes_dict[scene]
            for line in path_tuples:
                file.write(line)


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    config = import_json('mains/configs/creative_flow_all.json')
    output_dir = args.output_dir

    scenes_dict = read_scenes(config['filelists'])
    scenes = list(scenes_dict.keys())
    shuffle(scenes)
    split_index = int(len(scenes) * args.split_ratio)
    scenes_train = scenes[:split_index]
    scenes_test = scenes[split_index:]
    # Create white list files that split the dataset into train and test files.
    write_output(scenes_dict, scenes_train, output_dir, 'train_ALL.txt')
    write_output(scenes_dict, scenes_test, output_dir, 'test_ALL.txt')


def add_args(parser):
    parser.add_argument('-o', '--output_dir', type=str, default='mains/creative_scripts',
                        help='Output directory.')
    parser.add_argument('-s', '--split_ratio', type=float, default=0.66,
                        help='Ratio of train scenes to test scenes.')


if __name__ == "__main__":
    main()
