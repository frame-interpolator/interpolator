import argparse
import cv2
import tensorflow as tf
from common.utils.config import import_json
from data.flow.creative.creative_preprocessor import CreativeFlowDataPreprocessor
from data.flow.creative.utils import parse_objectids_foreground
from pwcnet.eval_utils import eval_file_list
from pwcnet.model import PWCNet


def eval_creative(model, output_dir, input_dir, config_path, print_output=False, percent_error=False):
    """
    Runs the main program loop.
    :param model: FrozenModel.
    :param output_dir: Str.
    :param input_dir: Str. Input directory.
    :param config_path: Str. Path to the config json.
    :param print_output: Bool.
    :param percent_error: Bool. Whether the EPE is expressed as an error percentage.
    :return: The average endpoint error of the flows.
             Average EPE of speeds between 0-10.
             Average EPE of speeds between 10-40.
             Average EPE of speeds between 40+.
    """
    config = import_json(config_path)
    preprocessor = CreativeFlowDataPreprocessor(input_dir, '', config=config)
    images_a, images_b, flows, objids = preprocessor.get_data_paths()

    def get_foreground(objid_path):
        """
        :param objid_path: Str.
        :return: Foreground mask of shape [H, W, 1]. Values are 0.0 or 1.0.
        """
        objectids = cv2.imread(objid_path, cv2.IMREAD_UNCHANGED)
        foreground = parse_objectids_foreground(objectids)  # Shape is [H, W, 1].
        return foreground

    def preprocess_valid_mask(i, valid_mask):
        assert valid_mask is None
        return get_foreground(objids[i])

    return eval_file_list(model, (images_a, images_b, flows), input_dir=input_dir, output_dir=output_dir,
                          print_output=print_output, preprocess_valid_mask=preprocess_valid_mask,
                          percent_error=percent_error)


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True

    print('Loading frozen model...')
    model = PWCNet.create_frozen_model(args.directory)
    model.load(session_config_proto=config_proto)

    print('Evaluating...')
    total, low_speed, med_speed, hi_speed = eval_creative(model, args.output_dir, args.input_dir, args.config,
                                                          percent_error=bool(args.percent_error))
    print('EPE all: %.2f' % total)
    print('s0-10: %.2f' % low_speed)
    print('s10-40: %.2f' % med_speed)
    print('s40+: %.2f' % hi_speed)


def add_args(parser):
    parser.add_argument('-d', '--directory', type=str,
                        help='Path to the frozen model..')
    parser.add_argument('-o', '--output_dir', type=str,
                        help='Output directory.')
    parser.add_argument('-i', '--input_dir', type=str, default='/ais/sf1/abstractvision/dataset_decompressed/test',
                        help='Input directory.')
    parser.add_argument('-c', '--config', type=str, default='mains/creative_scripts/creative_flow_test.json',
                        help='Config json containing the file list.')
    parser.add_argument('-p', '--percent_error', type=int, default=0,
                        help='Whether to record percent error on the EPE (1) or the raw EPE (0).')


if __name__ == "__main__":
    main()
