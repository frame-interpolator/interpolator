import argparse
import cv2
import tensorflow as tf
import time
from common.utils.config import import_json
from data.flow.creative.creative_preprocessor import CreativeFlowDataPreprocessor
from data.flow.creative.utils import parse_objectids_foreground
from pwcnet.eval_utils import hint_eval_results_to_csv, eval_file_list_with_hints
from pwcnet.hinted_model import HintedPWCNet


def eval_creative_with_hints(model, input_dir, config_path, min_num_hints=0, max_num_hints=12, percent_error=False,
                             error_sampling=False, probabilistic=True):
    """
    Runs the main program loop.
    :param model: FrozenModel.
    :param input_dir: Str. Input directory.
    :param config_path: Str. Path to the config json.
    :param min_num_hints: Int. Evaluates from [min_num_hints, max_num_hints] hints.
    :param max_num_hints: Int. Evaluates from [min_num_hints, max_num_hints] hints.
    :param percent_error: Bool. Whether the EPE is expressed as an error percentage.
    :param error_sampling: Bool. Whether to sample hints based on the error of the previous flow.
    :param probabilistic: Bool. Whether the hints are sampled pseudo-randomly based on a fixed seed or whether it's
        sampled by sorting the error/magnitudes.
    :return: Nothing.
    """
    config = import_json(config_path)

    preprocessor = CreativeFlowDataPreprocessor(input_dir, '', config=config)
    images_a, images_b, flows, objids = preprocessor.get_data_paths()
    assert len(objids) == len(images_a)

    def get_foreground(objid_path):
        """
        :param objid_path: Str.
        :return: Foreground mask of shape [H, W, 1]. Values are 0.0 or 1.0.
        """
        objectids = cv2.imread(objid_path, cv2.IMREAD_UNCHANGED)
        foreground = parse_objectids_foreground(objectids)  # Shape is [H, W, 1].
        return foreground

    def preprocess_valid_mask(i, valid_mask):
        assert valid_mask is None
        return get_foreground(objids[i])

    return eval_file_list_with_hints(model, (images_a, images_b, flows),
                                     min_num_hints=min_num_hints, max_num_hints=max_num_hints,
                                     preprocess_valid_mask=preprocess_valid_mask, percent_error=percent_error,
                                     error_sampling=error_sampling, probabilistic=probabilistic)


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True

    print('Loading model...')
    model = HintedPWCNet.create_frozen_model(args.directory)
    model.load(session_config_proto=config_proto)

    print('Evaluating...')
    start = time.time()
    results = eval_creative_with_hints(model, args.input_dir, args.config, min_num_hints=args.min_num_hints,
                                       max_num_hints=args.max_num_hints, percent_error=bool(args.percent_error),
                                       error_sampling=bool(args.error_sampling),
                                       probabilistic=bool(args.probabilistic))
    elapsed = time.time() - start
    print('Evaluation took: %.2f seconds' % elapsed)
    csv_str = hint_eval_results_to_csv(*results, min_num_hints=args.min_num_hints)
    print(csv_str)
    if args.csv_out != '':
        with open(args.csv_out, 'w') as file:
            file.write(csv_str)


def add_args(parser):
    parser.add_argument('-w', '--directory', type=str,
                        help='Path to the frozen model.')
    parser.add_argument('-d', '--input_dir', type=str,
                        help='Input directory of the creative dataset. I.e. .../dataset_decompressed/test')
    parser.add_argument('-c', '--config', type=str, default='mains/creative_scripts/creative_flow_test.json',
                        help='Config json containing the file list.')
    parser.add_argument('-m', '--min_num_hints', type=int, default=0,
                        help='Min number of hints to evaluate to.')
    parser.add_argument('-n', '--max_num_hints', type=int, default=12,
                        help='Max number of hints to evaluate to.')
    parser.add_argument('-s', '--csv_out', type=str, default='output.csv',
                        help='Save file to dump csv into.')
    parser.add_argument('-p', '--percent_error', type=int, default=0,
                        help='Whether to record percent error on the EPE (1) or the raw EPE (0).')
    parser.add_argument('-e', '--error_sampling', type=int, default=0,
                        help='Whether to sample hints based on the error of the previous flow (1) .' +
                             'or the gt magnitude (0)')
    parser.add_argument('-pr', '--probabilistic', type=int, default=1,
                        help='Whether the hints are sampled pseudo-randomly based on a fixed seed (1) or ' +
                             'sampled by sorting the error/magnitudes (0).')


if __name__ == "__main__":
    main()
