import argparse
import tensorflow as tf
import time
from data.flow.sintel.sintel_preprocessor import SintelFlowDataPreprocessor
from pwcnet.eval_utils import eval_file_list
from pwcnet.model import PWCNet


def eval_sintel(model, output_dir, input_dir, print_output=False, percent_error=False):
    """
    :param model: FrozenModel.
    :param output_dir: Str. Directory to output flow images. If None then no images will be saved.
    :param input_dir: Str. Input directory.
    :param print_output: Bool.
    :param percent_error: Bool. Whether the EPE is expressed as an error percentage.
    :return: The average endpoint error of the flows.
             Average EPE of speeds between 0-10.
             Average EPE of speeds between 10-40.
             Average EPE of speeds between 40+.
    """
    preprocessor = SintelFlowDataPreprocessor(input_dir, '')
    images_a, images_b, flows, _ = preprocessor.get_data_paths()
    return eval_file_list(model, (images_a, images_b, flows), input_dir=input_dir, output_dir=output_dir,
                          print_output=print_output, percent_error=percent_error)


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True

    print('Loading frozen model...')
    model = PWCNet.create_frozen_model(args.directory)
    model.load(session_config_proto=config_proto)

    print('Evaluating...')
    start = time.time()
    total, low_speed, med_speed, hi_speed = eval_sintel(model, args.output_dir, args.input_dir, print_output=True,
                                                        percent_error=bool(args.percent_error))
    elapsed = time.time() - start
    print('Evaluation took: %.2f seconds' % elapsed)
    print('EPE all: %.2f' % total)
    print('s0-10: %.2f' % low_speed)
    print('s10-40: %.2f' % med_speed)
    print('s40+: %.2f' % hi_speed)


def add_args(parser):
    parser.add_argument('-d', '--directory', type=str,
                        help='Path to the frozen model..')
    parser.add_argument('-o', '--output_dir', type=str, default=None,
                        help='Output directory to which output flows will be written.')
    parser.add_argument('-i', '--input_dir', type=str,
                        default='/ais/gobi6/interpolation/datasets/flow/sintel_raw/training/clean/',
                        help='Input directory.')
    parser.add_argument('-p', '--percent_error', type=int, default=0,
                        help='Whether to record percent error on the EPE (1) or the raw EPE (0).')


if __name__ == "__main__":
    main()
