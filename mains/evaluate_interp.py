import argparse
import numpy as np
import os
import tensorflow as tf
from common.utils.interp_eval import load_saved_models
from common.utils.metrics import get_metrics


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()
    k = args.num_ranked_images
    n = args.num_steps
    assert n > 0
    assert args.percentage_eval > 0.0
    assert args.percentage_eval <= 100.0

    if args.out_directory is not None and not os.path.exists(args.out_directory):
        os.makedirs(args.out_directory)

    print('Starting session...')
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    # Load models for inference.
    synthesizer_model, flow_model = load_saved_models(args.synthesis_model_dir, args.flow_model_dir)
    metrics, ranked_tuples_dict = get_metrics(synthesizer_model, flow_model, args.input_directory,
                                              args.percentage_eval,
                                              sess=sess,
                                              num_inbetween_steps=n,
                                              use_underscore_sep=bool(args.underscore_sep),
                                              use_endpoints=bool(args.endpoints),
                                              max_ranked=args.num_ranked_images,
                                              use_lpips=True,
                                              verbose=True,
                                              use_flow_spline=False)

    if np.sum(metrics.num_evaluated) > 0:
        if args.out_directory is not None:
            metrics.write_to_json(os.path.join(args.out_directory, 'all-eval.json'))

        # This will print the results to stdout.
        metrics.write_to_json(None)

    if args.out_directory is not None:
        if k >= np.sum(metrics.num_evaluated):
            ranked_tuples_dict['best_ssim'].write_to_folder(os.path.join(args.out_directory, 'all'))
        else:
            # Write the representative images.
            num_to_write = 4 * len(ranked_tuples_dict['best_ssim'].sorted_list)
            if num_to_write > 0:
                print('Writing %d representative evaluation images...' % num_to_write)
                ranked_tuples_dict['best_ssim'].write_to_folder(os.path.join(args.out_directory, 'best_ssim'))
                ranked_tuples_dict['worst_ssim'].write_to_folder(os.path.join(args.out_directory, 'worst_ssim'))
                ranked_tuples_dict['best_psnr'].write_to_folder(os.path.join(args.out_directory, 'best_psnr'))
                ranked_tuples_dict['worst_psnr'].write_to_folder(os.path.join(args.out_directory, 'worst_psnr'))


def add_args(parser):
    parser.add_argument('-s', '--synthesis_model_dir', type=str,
                        help='Path to the synthesis SavedModel that we will load.')
    parser.add_argument('-f', '--flow_model_dir', type=str, default=None,
                        help='Path to the optical flow SavedModel that we will load.' +
                             'If not provided, we will use the flow model associated with the synthesizer, ' +
                             'if it is available')
    parser.add_argument('-d', '--input_directory', type=str,
                        help='Input directory.')
    parser.add_argument('-o', '--out_directory', type=str,
                        help='Output directory.')
    parser.add_argument('-k', '--num_ranked_images', type=int, default=5,
                        help='The number of best / worst images to save for each of PSNR and SSIM. ' +
                             'This is ignored for values of num_steps greater than 1.')
    parser.add_argument('-n', '--num_steps', type=int, default=1,
                        help='The number of inbetween times to evaluate at, spaced uniformly. ' +
                             'A value of 1 means that we are evaluating triplet by triplet.')
    parser.add_argument('-p', '--percentage_eval', type=float, default=100.0,
                        help='The approximate percentage of sub-folders to do evaluation on.')
    parser.add_argument('-u', '--underscore_sep', type=int, default=1,
                        help='Whether file names are formatted with an underscore separation ' +
                             'between the sequence name and the sequence idx.')
    parser.add_argument('-e', '--endpoints', type=int, default=1,
                        help='Whether to include the endpoint frames for evaluation (i.e use them as gt).')


if __name__ == '__main__':
    main()
