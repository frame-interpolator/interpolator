import argparse
import os
import os.path
import tensorflow as tf
from pwcnet.model import PWCNet
from data.interp.video_processing.frame_diff import get_pwcnet_diff_graph
from data.interp.video_processing.processing import process_video


def get_compute_stats_fn(model, session):
    assert isinstance(model, PWCNet)
    image_a_input, image_b_input, avg_flow_mag_t, max_flow_mag_t, diff_warp_t, diff_t = get_pwcnet_diff_graph(model)

    def run(images_a, images_b):
        return session.run([avg_flow_mag_t, max_flow_mag_t, diff_warp_t, diff_t], feed_dict={
            image_a_input: images_a,
            image_b_input: images_b
        })

    return run


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    print('Starting session...')
    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True
    session = tf.Session(config=config_proto)

    print('Initializing...')
    model = PWCNet(bilateral_range_stddev=args.bilateral_range_stddev, use_flcon=args.use_flcon)
    compute_stats = get_compute_stats_fn(model, session)

    print('Loading PWC Net weights...')
    if os.path.isfile(args.weights):
        print('Restoring weights from npz...')
        model.restore_from(args.weights, session)
    else:
        print(args.weights, 'does not exist.')
        return

    print('Processing...')
    process_video(compute_stats, model.NUM_FEATURE_LEVELS, args.video_path, args.output_dir, batch_size=args.batch_size,
                  desired_height=args.height, desired_width=args.width, min_clip_length=args.min_clip_length,
                  min_diff=args.min_diff)


def add_args(parser):
    parser.add_argument('-w', '--weights', type=str,
                        help='PWC Net weights in npz format.')
    parser.add_argument('-v', '--video_path', type=str,
                        help='Path to a video file.')
    parser.add_argument('-o', '--output_dir', type=str,
                        help='Output directory path.')
    parser.add_argument('-b', '--batch_size', type=int, default=4,
                        help='Number of images to process in parallel.')
    parser.add_argument('-dh', '--height', type=int, default=720,
                        help='Desired height to resize the frames to.')
    parser.add_argument('-dw', '--width', type=int, default=1280,
                        help='Desired width to resize the frames to.')
    parser.add_argument('-mc', '--min_clip_length', type=int, default=6,
                        help='Minimum length for a clip.')
    parser.add_argument('-md', '--min_diff', type=float, default=0.0005,
                        help='Minimum pixel difference between 2 frames.')
    # PWCNet config options.
    parser.add_argument('-bi', '--bilateral_range_stddev', type=float, default=0.5,
                        help='Standard deviation over which the bilateral filter should average.')
    parser.add_argument('-fl', '--use_flcon', type=bool, default=True,
                        help='Whether to use an flcon at the output layer.')


if __name__ == "__main__":
    main()
