import argparse
import os
import os.path
import shutil

def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    files = [f for f in os.listdir(args.input_dir) if \
            os.path.isfile(os.path.join(args.input_dir, f))\
            and not f.startswith('.')]

    if os.path.exists(args.output_dir):
        shutil.rmtree(args.output_dir)
    clip_dir = os.path.join(args.output_dir, "clips")
    tuple_dir = os.path.join(args.output_dir, "tuples")

    # I'm too lazy to figure out how to fiddle with the argparser, so:
    for fname in files:
        # Video to clips.
        os.makedirs(os.path.join(clip_dir, fname))
        os.system("python -m mains.video_to_clips -w %s -v %s -o %s" % \
                    (args.weights,
                    os.path.join(args.input_dir, fname),
                    os.path.join(clip_dir, fname)))
        # Clips to tuples.
        os.makedirs(os.path.join(tuple_dir, fname))
        os.system("python -m mains.clips_to_tuples -v %s -o %s -ch %s -cw %s" %\
                    (os.path.join(clip_dir, fname),
                    os.path.join(tuple_dir, fname),
                    args.crop_height,
                    args.crop_width))
        # Delete temporary clips directory
        # shutil.rmtree(os.path.join(clip_dir, fname))


def add_args(parser):
    parser.add_argument('-w', '--weights', type=str,
                        help='PWC Net weights in npz format.')
    parser.add_argument('-i', '--input_dir', type=str,
                        help='Directory containing videos to process.')
    parser.add_argument('-o', '--output_dir', type=str,
                        help='Directory to output clips and triplets.')
    parser.add_argument('-ch', '--crop_height', type=int,
                        help='Height to crop the tuples to.')
    parser.add_argument('-cw', '--crop_width', type=int,
                        help='Height to crop the tuples to.')
if __name__ == "__main__":
    main()
