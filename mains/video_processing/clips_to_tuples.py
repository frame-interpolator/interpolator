import argparse
import glob
import multiprocessing
import os.path
from common.utils import data, img
from data.interp.video_processing.tuple_cropping import crop_tuples_for_flow
from joblib import Parallel, delayed


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    images = glob.glob(os.path.join(args.input_dir, '**', '*.png'), recursive=True)
    images.sort()
    images = images[::args.frame_skip]
    clips = data.group_files_by_directory(images)

    # Process clips by the tuple.
    tuple_data = []
    for clip_index, clip in enumerate(clips):
        tuples = data.shard_list(clip, args.tuple_size)
        if len(tuples[-1]) != args.tuple_size:
            tuples.pop()

        if len(tuples) == 0:
            continue

        clip_dir = os.path.join(args.output_dir, str(clip_index))
        os.makedirs(clip_dir, exist_ok=True)

        for tuple_index, path_tuple in enumerate(tuples):
            tuple_data.append([path_tuple, tuple_index, clip_dir])

    Parallel(n_jobs=multiprocessing.cpu_count(), backend='threading')(
        delayed(process_tuple)(path_tuple, tuple_index, clip_dir, args.crop_height, args.crop_width, args.tuple_size)
        for path_tuple, tuple_index, clip_dir in tuple_data
    )


def process_tuple(path_tuple, clip_index, clip_dir, crop_height, crop_width, tuple_size):
    """
    Processes a tuple of images (given as file paths) and saves them to the output clip_dir.
    :param path_tuple: Tuple/list containing tuple_size image paths (str).
    :param clip_index: Int.
    :param clip_dir: Str. Output directory.
    :param crop_height: Int.
    :param crop_width: Int.
    :param tuple_size: Int. Number of frames per tuple.
    :return: Nothing.
    """
    assert len(path_tuple) == tuple_size
    images = [img.read_image(image_path) for image_path in path_tuple]
    crops = crop_tuples_for_flow(images, crop_height, crop_width)
    if crops is None:
        return
    for i, cropped_image in enumerate(crops):
        img.write_image(os.path.join(clip_dir, '{0:0>7}'.format(clip_index) + '_' + str(i) + '.png'), cropped_image)


def add_args(parser):
    parser.add_argument('-v', '--input_dir', type=str,
                        help='Path to a video file.')
    parser.add_argument('-o', '--output_dir', type=str,
                        help='Output directory path.')
    parser.add_argument('-ch', '--crop_height', type=int,
                        help='Height to crop the tuples to.')
    parser.add_argument('-cw', '--crop_width', type=int,
                        help='Height to crop the tuples to.')
    parser.add_argument('-fs', '--frame_skip', type=int, default=2,
                        help='Number of frames to skip by when generating tuples.' +
                             ' The higher this is, the more time between frames.')
    parser.add_argument('-ts', '--tuple_size', type=int, default=4,
                        help='Number of frames per tuple.')


if __name__ == "__main__":
    main()
