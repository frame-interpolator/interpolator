import argparse
import os
import tensorflow as tf
from common.utils.video import VideoWriter
from common.utils import img, flow
from common.utils.warper import Warper
from common.utils.misc import print_progress_bar
from common.utils.interp_eval import config_readline, load_saved_models, Interpolator
import ntpath


def main():
    img_help_str = 'Usage (.jpg or .png): <path/to/im0.png>, <path/to/im1.png>, <out/img.png>, [0 < t < 1]'
    vid_help_str = 'Usage (.jpg or .png): <path/to/im0.png>, <path/to/im1.png>, <out/dir>'
    config_readline()
    parser = argparse.ArgumentParser()
    add_args(parser)
    argparse_args = parser.parse_args()

    # Create session.
    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True
    sess = tf.Session(config=config_proto)
    synthesizer_model, flow_model = load_saved_models(argparse_args.synthesizer_model_dir, argparse_args.flow_model_dir)
    interpolator = Interpolator(synthesizer_model, flow_model, sess=sess)

    if argparse_args.debug:
        print('Creating TensorFlow graph for debug warping operations...', flush=True)
        warper = Warper(use_middlebury=argparse_args.use_middlebury_warp, fill_holes=False, sess=sess)

    # The fire emoji might not display correctly in non-bash environments.
    fire_emoji = '\xf0\x9f\x94\xa5'
    print(fire_emoji + ' Happy in-betweenings ! ' + fire_emoji, flush=True)
    if argparse_args.video:
        print(vid_help_str)
    else:
        print(img_help_str)
    line = input()
    while line:
        args = [x.strip() for x in line.split(',')]
        invalid_vid_arglen = argparse_args.video and len(args) != 3
        invalid_img_arglen = not argparse_args.video and (len(args) < 3 or len(args) > 4)
        if invalid_img_arglen or invalid_vid_arglen or args[0] == 'h' or args[0] == '--help':
            print('Input format is incorrect.', flush=True)
            if argparse_args.video:
                print(vid_help_str)
            else:
                print(img_help_str)
        else:
            try:
                if not os.path.exists(args[0]):
                    print('Provided path %s does not exist.' % args[0])
                if not os.path.exists(args[1]):
                    print('Provided path %s does not exist.' % args[1])
                if not os.path.exists(os.path.dirname(args[2])):
                    os.makedirs(os.path.dirname(args[2]))

                img_0 = img.read_image(args[0], as_float=True)
                img_1 = img.read_image(args[1], as_float=True)
                t = float(args[3]) if len(args) > 3 else 0.5
                assert 0 <= t <= 1

                # Create specified directory if it doesn't exist.
                out_directory = os.path.dirname(args[2])
                if not os.path.exists(out_directory):
                    os.makedirs(out_directory)

                interpolator.set_end_frames_and_compute_flow(img_0, img_1, use_spline=False)

                if argparse_args.video:
                    # Interpolate at a sequence of times between the 2 keyframes to create a video.
                    outdir_name = args[2]
                    if not os.path.exists(outdir_name):
                        os.makedirs(outdir_name)
                    interpolated_video_path = os.path.join(outdir_name, 'interpolated.avi')
                    warp_overlay_video_path = os.path.join(outdir_name, 'warp_overlay.avi')
                    warp_0_1_video_path = os.path.join(outdir_name, 'warp_0_1.avi')
                    warp_1_0_video_path = os.path.join(outdir_name, 'warp_1_0.avi')

                    # Create video writers and set video params.
                    fps = 12
                    interpolated_video = VideoWriter(interpolated_video_path, fps=fps)
                    warp_overlay_video = VideoWriter(warp_overlay_video_path, fps=fps)
                    warp_0_1_video = VideoWriter(warp_0_1_video_path, fps=fps)
                    warp_1_0_video = VideoWriter(warp_1_0_video_path, fps=fps)
                    num_frames = 24
                    dt = 1.0 / num_frames

                    # Interpolate and write the videos.
                    for i in range(num_frames):
                        t = dt * i
                        interpolated = interpolator.get_interpolated(t)
                        interpolated_video.write(interpolated)
                        if argparse_args.debug:
                            flows_0_1 = interpolator.flow_0_1
                            flows_1_0 = interpolator.flow_1_0
                            warp_0_1, warp_1_0 = warper.get_bidir_warps(img_0, img_1,
                                                                        flows_0_1, flows_1_0, t)
                            warp_overlay_video.write(0.5 * (warp_0_1 + warp_1_0))
                            warp_0_1_video.write(warp_0_1)
                            warp_1_0_video.write(warp_1_0)
                        print_progress_bar(i + 1, num_frames,
                                           prefix='Generating',
                                           suffix='Frames',
                                           use_percentage=False)

                    if argparse_args.debug:
                        # Add useful images.
                        img.write_image(os.path.join(outdir_name, 'overlay.png'), 0.6 * img_0 + 0.4 * img_1)
                        img.write_image(os.path.join(outdir_name, 'img_0.png'), img_0)
                        img.write_image(os.path.join(outdir_name, 'img_1.png'), img_1)
                        img.write_image(os.path.join(outdir_name, 'flow_0_1.png'),
                                        flow.get_flow_visualization(interpolator.flow_0_1))
                        img.write_image(os.path.join(outdir_name, 'flow_1_0.png'),
                                        flow.get_flow_visualization(interpolator.flow_1_0))

                    # Free video writers.
                    interpolated_video.release()
                    warp_overlay_video.release()
                    warp_0_1_video.release()
                    warp_1_0_video.release()
                else:
                    # Interpolate at a single specified target time.
                    interpolated = interpolator.get_interpolated(t)
                    if argparse_args.debug:
                        flows_0_1 = interpolator.flow_0_1
                        flows_1_0 = interpolator.flow_1_0
                        warp_0_1, warp_1_0 = warper.get_bidir_warps(img_0, img_1, flows_0_1, flows_1_0, t)
                        outdir_name = os.path.join(os.path.dirname(args[2]), ntpath.basename(args[2])[:-4])
                        if not os.path.exists(outdir_name):
                            os.makedirs(outdir_name)
                        img.write_image(os.path.join(outdir_name, 'interpolated.png'), interpolated)
                        img.write_image(os.path.join(outdir_name, 'overlay.png'), 0.6 * img_0 + 0.4 * img_1)
                        img.write_image(os.path.join(outdir_name, 'warp_overlay.png'), 0.5 * (warp_0_1 + warp_1_0))
                        img.write_image(os.path.join(outdir_name, 'warp_0_1.png'), warp_0_1)
                        img.write_image(os.path.join(outdir_name, 'warp_1_0.png'), warp_1_0)
                        img.write_image(os.path.join(outdir_name, 'flow_0_1.png'),
                                        flow.get_flow_visualization(flows_0_1))
                        img.write_image(os.path.join(outdir_name, 'flow_1_0.png'),
                                        flow.get_flow_visualization(flows_1_0))
                    else:
                        img.write_image(args[2], interpolated)
            except Exception as e:
                print('Failed to run the model.')
                print(e)
        print('Enter command:', flush=True)
        line = input()
    print('')


def add_args(parser):
    parser.add_argument('-s', '--synthesizer_model_dir', type=str,
                        help='Path to the synthesizer SavedModel that we will load.')
    parser.add_argument('-f', '--flow_model_dir', type=str, default=None,
                        help='Path to the optical flow SavedModel that we will load.' +
                             'If not provided, we will use the flow model associated with the synthesizer, ' +
                             'if it is available')

    # TODO: These might be better off as options within the command line app.
    parser.add_argument('-g', '--debug', dest='debug', action='store_true',
                        help='Whether to output additional debug information for the interpolation.')
    parser.add_argument('-v', '--video', dest='video', action='store_true',
                        help='Whether to use video mode. This will output interpolated videos instead of single frames.')
    parser.add_argument('-m', '--use_middlebury_warp', type=int, default=0,
                        help='Relevant only if -g is enabled.')
    parser.set_defaults(feature=True)


if __name__ == '__main__':
    main()
