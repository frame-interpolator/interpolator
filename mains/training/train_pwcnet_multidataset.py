import argparse
import os
import tensorflow as tf
from common.utils.config import preprocess_var_refs, import_json, dump_json
from data.flow.flow_data import FlowDataSet
from pwcnet.factory import create_pwcnet, add_pwcnet_config_to_argparser, add_pwcnet_config_args_to_dict
from train.pwcnet.simulated_hint_trainer import PWCNetSimulatedHintTrainer
from train.pwcnet.trainer import PWCNetTrainer


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    if not os.path.exists(args.checkpoint_directory):
        os.makedirs(args.checkpoint_directory)

    print('Creating session...')
    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True
    config_proto.allow_soft_placement = True
    session = tf.Session(config=config_proto)

    print('Creating datasets...')
    datasets = []
    config_paths = []
    directories = []
    if args.directory_flyingchairs is not None:
        config = import_json(args.config_flyingchairs)
        preprocess_var_refs(config)
        if args.learning_rate is not None:
            config['learning_rate'] = args.learning_rate
        dataset = FlowDataSet(args.directory_flyingchairs, batch_size=config['batch_size'],
                              crop_size=(config['crop_height'], config['crop_width']),
                              training_augmentations=config['training_augmentations'],
                              config=config, parent=None if len(datasets) == 0 else datasets[-1])
        datasets.append(dataset)
        config_paths.append(args.config_flyingchairs)
        directories.append(dataset.directory)
    if args.directory_flyingthings is not None:
        config = import_json(args.config_flyingthings)
        preprocess_var_refs(config)
        if args.learning_rate is not None:
            config['learning_rate'] = args.learning_rate
        dataset = FlowDataSet(args.directory_flyingthings, batch_size=config['batch_size'],
                              crop_size=(config['crop_height'], config['crop_width']),
                              training_augmentations=config['training_augmentations'],
                              config=config, parent=None if len(datasets) == 0 else datasets[-1])
        datasets.append(dataset)
        config_paths.append(args.config_flyingthings)
        directories.append(dataset.directory)
    if args.directory_sintel is not None:
        config = import_json(args.config_sintel)
        preprocess_var_refs(config)
        if args.learning_rate is not None:
            config['learning_rate'] = args.learning_rate
        dataset = FlowDataSet(args.directory_sintel, batch_size=config['batch_size'],
                              crop_size=(config['crop_height'], config['crop_width']),
                              training_augmentations=config['training_augmentations'],
                              config=config, parent=None if len(datasets) == 0 else datasets[-1])
        datasets.append(dataset)
        config_paths.append(args.config_sintel)
        directories.append(dataset.directory)
    if args.directory_creative is not None:
        config = import_json(args.config_creative)
        preprocess_var_refs(config)
        if args.learning_rate is not None:
            config['learning_rate'] = args.learning_rate
        dataset = FlowDataSet(args.directory_creative, batch_size=config['batch_size'],
                              crop_size=(config['crop_height'], config['crop_width']),
                              training_augmentations=config['training_augmentations'],
                              config=config, parent=None if len(datasets) == 0 else datasets[-1])
        datasets.append(dataset)
        config_paths.append(args.config_creative)
        directories.append(dataset.directory)
    if args.directory_kitti is not None:
        config = import_json(args.config_kitti)
        preprocess_var_refs(config)
        if args.learning_rate is not None:
            config['learning_rate'] = args.learning_rate
        dataset = FlowDataSet(args.directory_kitti, batch_size=config['batch_size'],
                              crop_size=(config['crop_height'], config['crop_width']),
                              training_augmentations=config['training_augmentations'],
                              config=config, parent=None if len(datasets) == 0 else datasets[-1])
        datasets.append(dataset)
        config_paths.append(args.config_kitti)
        directories.append(dataset.directory)
    if len(datasets) == 0:
        print('Insufficient number of datasets provided.')
        return

    # Use the master config.
    # Add extra fields to the config from argparse.
    config = import_json(args.config)
    preprocess_var_refs(config)
    config['checkpoint_directory'] = args.checkpoint_directory
    config['directory'] = ' '.join(directories)
    config['config'] = ' '.join(config_paths)
    config['lr_half_life'] = args.lr_half_life
    config['sintel_clean_dir'] = args.sintel_clean_dir
    config['sintel_final_dir'] = args.sintel_final_dir
    add_pwcnet_config_args_to_dict(args, config)
    if args.learning_rate is not None:
        config['learning_rate'] = args.learning_rate

    print('Creating network...')
    model, model_config = create_pwcnet(config)
    dump_json(os.path.join(config['checkpoint_directory'], 'model_config.json'), model_config)

    print('Initializing trainer and model ops...')
    if args.loss == 'sim_hint':
        if not config['hinted']:
            raise Exception('If loss is sim_hint, then the model must be hinted.')
        trainer = PWCNetSimulatedHintTrainer(model, datasets, session, config, lr_half_life=args.lr_half_life)
    else:
        if config['hinted']:
            raise Exception('Must use sim_hint loss with a hinted model.')
        trainer = PWCNetTrainer(model, datasets, session, config, lr_half_life=args.lr_half_life)

    print('Initializing variables...')
    session.run(tf.global_variables_initializer())
    trainer.restore()

    trainer.train(validate_every=config['validate_every'], iterations=args.iterations)


def add_args(parser):
    parser.add_argument('-dfc', '--directory_flyingchairs', type=str, default=None,
                        help='Directory of the flyingchairs tf records.')
    parser.add_argument('-dft', '--directory_flyingthings', type=str, default=None,
                        help='Directory of the flyingthings tf records.')
    parser.add_argument('-ds', '--directory_sintel', type=str, default=None,
                        help='Directory of the sintel tf records.')
    parser.add_argument('-dc', '--directory_creative', type=str, default=None,
                        help='Directory of the creative tf records.')
    parser.add_argument('-dk', '--directory_kitti', type=str, default=None,
                        help='Directory of the kitti tf records.')

    parser.add_argument('-jfc', '--config_flyingchairs', type=str, default=None,
                        help='Config json file path.')
    parser.add_argument('-jft', '--config_flyingthings', type=str, default=None,
                        help='Config json file path.')
    parser.add_argument('-js', '--config_sintel', type=str, default=None,
                        help='Config json file path.')
    parser.add_argument('-jc', '--config_creative', type=str, default=None,
                        help='Config json file path.')
    parser.add_argument('-jk', '--config_kitti', type=str, default=None,
                        help='Config json file path.')

    parser.add_argument('-j', '--config', type=str, default=None,
                        help='Master config json file path.')

    parser.add_argument('-c', '--checkpoint_directory', type=str,
                        help='Directory of saved checkpoints.')
    parser.add_argument('-i', '--iterations', type=int, default=1000000,
                        help='Number of iterations to train for.')
    parser.add_argument('-l', '--loss', type=str, default='supervised',
                        help='Loss type. Can be "supervised" or "sim_hint". Defaults to "supervised".')

    parser.add_argument('-hl', '--lr_half_life', type=int, default=-1,
                        help='Iteration at which the learning rate will be halved.')
    parser.add_argument('-lr', '--learning_rate', type=float, default=None,
                        help='If specified, this will overwrite the learning rate in the config.')

    parser.add_argument('-sc', '--sintel_clean_dir', type=str, default=None,
                        help='Path to Sintel Clean, with images and flows as sub-folders.')
    parser.add_argument('-sf', '--sintel_final_dir', type=str, default=None,
                        help='Path to Sintel Final, with images and flows as sub-folders.')

    add_pwcnet_config_to_argparser(parser)


if __name__ == "__main__":
    main()
