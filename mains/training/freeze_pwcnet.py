import argparse
import os
import tensorflow as tf
from pwcnet.factory import create_pwcnet


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    print('Creating session...')
    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True
    config_proto.allow_soft_placement = True
    session = tf.Session(config=config_proto)

    print('Creating network...')
    model, config = create_pwcnet(args.pwcnet_config)
    assert 'hinted' in config
    if config['hinted']:
        hints_ph = tf.placeholder(shape=[None, None, None, 2], dtype=tf.float32)
        hint_masks_ph = tf.placeholder(shape=[None, None, None, 1], dtype=tf.float32)
        model.set_flow_hint(hints_ph, hint_masks_ph)
    model.init_forward_with_placeholders()

    print('Initializing variables...')
    session.run(tf.global_variables_initializer())

    print('Restoring...')
    model.restore_from(args.weights, session)

    print('Freezing...')
    frozen_model = model.create_frozen_model(os.path.join(args.output_dir, 'frozen'))
    frozen_model.freeze(model, session)


def add_args(parser):
    parser.add_argument('-w', '--weights', type=str,
                        help='Path to pwcnet_weights.npz.')
    parser.add_argument('-c', '--pwcnet_config', type=str,
                        help='Path to the pwcnet model_config.json.')
    parser.add_argument('-o', '--output_dir', type=str,
                        help='Directory to output the frozen model.')


if __name__ == "__main__":
    main()
