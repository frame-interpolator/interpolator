import argparse
import os
import tensorflow as tf
from common.utils.config import preprocess_var_refs, import_json, dump_json
from data.flow.flow_data import FlowDataSet
from pwcnet.factory import create_pwcnet, add_pwcnet_config_to_argparser, add_pwcnet_config_args_to_dict
from train.pwcnet.trainer import PWCNetTrainer
from train.pwcnet.simulated_hint_trainer import PWCNetSimulatedHintTrainer


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    # Read the JSON config.
    print('Loading configurations...', flush=True)
    config = import_json(args.config)
    preprocess_var_refs(config)
    print(config, flush=True)
    print('', flush=True)
    # Add extra fields to the config from argparse.
    config['checkpoint_directory'] = args.checkpoint_directory
    config['directory'] = args.directory
    config['config'] = args.config
    config['sintel_clean_dir'] = args.sintel_clean_dir
    config['sintel_final_dir'] = args.sintel_final_dir
    config['lr_half_life'] = args.lr_half_life
    add_pwcnet_config_args_to_dict(args, config)
    if args.learning_rate is not None:
        config['learning_rate'] = args.learning_rate

    if not os.path.exists(args.checkpoint_directory):
        os.makedirs(args.checkpoint_directory)

    print('Creating session...', flush=True)
    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True
    config_proto.allow_soft_placement = True
    session = tf.Session(config=config_proto)

    print('Creating network...', flush=True)
    model, model_config = create_pwcnet(config)
    dump_json(os.path.join(config['checkpoint_directory'], 'model_config.json'), model_config)

    print('Creating dataset...', flush=True)
    dataset = FlowDataSet(args.directory, batch_size=config['batch_size'],
                          crop_size=(config['crop_height'], config['crop_width']),
                          training_augmentations=config['training_augmentations'], config=config)

    print('Initializing trainer and model ops...', flush=True)
    if args.loss == 'sim_hint':
        if not config['hinted']:
            raise Exception('If loss is sim_hint, then the model must be hinted.')
        trainer = PWCNetSimulatedHintTrainer(model, [dataset], session, config, lr_half_life=args.lr_half_life)
    else:
        if config['hinted']:
            raise Exception('Must use sim_hint loss with a hinted model.')
        trainer = PWCNetTrainer(model, [dataset], session, config, lr_half_life=args.lr_half_life)

    print('Initializing variables...', flush=True)
    session.run(tf.global_variables_initializer())
    trainer.restore()

    trainer.train(validate_every=config['validate_every'], iterations=args.iterations)


def add_args(parser):
    parser.add_argument('-d', '--directory', type=str,
                        help='Directory of the tf records.')
    parser.add_argument('-c', '--checkpoint_directory', type=str,
                        help='Directory of saved checkpoints.')
    parser.add_argument('-j', '--config', type=str,
                        default='mains/configs/pwcnet_schedule/train_pwcnet_flyingchairs_0.json',
                        help='Config json file path.')
    parser.add_argument('-i', '--iterations', type=int, default=1000000,
                        help='Number of iterations to train for.')
    parser.add_argument('-l', '--loss', type=str, default='supervised',
                        help='Loss type. Can be "supervised" or "sim_hint". Defaults to "supervised".')

    parser.add_argument('-sc', '--sintel_clean_dir', type=str, default=None,
                        help='Path to Sintel Clean, with images and flows as sub-folders.')
    parser.add_argument('-sf', '--sintel_final_dir', type=str, default=None,
                        help='Path to Sintel Final, with images and flows as sub-folders.')

    parser.add_argument('-hl', '--lr_half_life', type=int, default=-1,
                        help='Iteration at which the learning rate will be halved.')
    parser.add_argument('-lr', '--learning_rate', type=float, default=None,
                        help='If specified, this will overwrite the learning rate in the config.')

    add_pwcnet_config_to_argparser(parser)


if __name__ == "__main__":
    main()
