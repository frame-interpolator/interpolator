import argparse
import os
import tensorflow as tf
from common.utils.config import preprocess_var_refs, import_json
from data.interp.interp_data import InterpDataSet, InterpAugmentations
from interp.context_interp.model import ContextInterp
from interp.context_interp.deep_model import DeepContextInterp
from pwcnet.factory import create_pwcnet
from train.interp.context_interp_trainer import ContextInterpTrainer


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True
    session = tf.Session(config=config_proto)

    # Read the JSON config.
    print('Loading configurations...')
    config = import_json(args.config)
    preprocess_var_refs(config)
    print(config)
    print('')

    # Add extra fields to the config from argparse.
    config['checkpoint_directory'] = args.checkpoint_directory
    config['iterations'] = args.iterations
    config['vimeo_train_raw_dir'] = args.raw_vimeo_train_directory
    config['fine_tune'] = bool(args.fine_tune)
    config['use_middlebury_warp'] = bool(args.use_middlebury_warp)

    msg = 'There must be either an existing checkpoint or PWCNet weights that we can load.'
    assert os.path.exists(args.checkpoint_directory) or args.pwcnet_weights_path is not None, msg

    if not os.path.exists(args.checkpoint_directory):
        os.makedirs(args.checkpoint_directory)

    # Repeat important input argument information back to user.
    stop_grads = not ('gradients_to_pwcnet' in config and config['gradients_to_pwcnet'])
    if stop_grads:
        print('Gradients will not flow back to PWCNet.')
    else:
        print('Gradients will flow back to PWCNet.')

    if config['use_middlebury_warp']:
        print('Will use the hole-filling Middlebury forward warp.')
    else:
        print('Will use a differentiable forward warp.')

    if 'fine_tune' in config and config['fine_tune']:
        print('Will use perceptual fine-tuning loss.')
    else:
        print('Will use Laplacian pyramid loss.')

    print('Creating network...')
    pwcnet, _ = create_pwcnet(args.pwcnet_config if args.pwcnet_config is not None else {})
    if config['extract_deep_context']:
        assert not config['time_channel_input'], 'This model currently does not support time channel input.'
        model = DeepContextInterp(use_instance_norm=True,
                                  stop_gradient_to_pwcnet=stop_grads,
                                  use_differentiable_warp=not config['use_middlebury_warp'], pwcnet=pwcnet)
    else:
        model = ContextInterp(use_instance_norm=True,
                              stop_gradient_to_pwcnet=stop_grads,
                              use_differentiable_warp=not config['use_middlebury_warp'],
                              use_time_channel=config['use_time_channel'], pwcnet=pwcnet)

    print('Creating dataset...')
    augmentations = InterpAugmentations(flip_prob=0.5,
                                        duplicate_endpoint_frames_prob=0.0,
                                        do_center_crop=False,
                                        crop_size=(config['crop_height'], config['crop_width']))

    inbetween_locs = config['inbetween_times']
    dataset = InterpDataSet(args.directory, inbetween_locs,
                            batch_size=config['batch_size'],
                            augmentations=augmentations,
                            extra_validation_dir=args.additional_val_directory)

    print('Initializing trainer...')
    trainer = ContextInterpTrainer(model, [dataset], session, config)

    print('Initializing variables...')
    session.run(tf.global_variables_initializer())
    trainer.restore()

    # Possibly overwrite existing checkpoint PWCNet weights with new ones:
    # This is useful in the case that a newer PWCNet architecture is available
    # (in which case trainer.restore() would have left them as garbage / newly randomly initialized),
    # but you would like the synthesis network variables to continue to be used.
    if args.pwcnet_weights_path is not None:
        print('Would you like to over-write PWCNet checkpoint variables with weights from %s? (y/n)' %
              args.pwcnet_weights_path, flush=True)
        response = input()
        if response == 'y':
            print('Loading pre-trained PWCNet...', flush=True)
            model.load_pwcnet_weights(args.pwcnet_weights_path, session)
        else:
            print('Will not overwrite PWCNet checkpoint variables.', flush=True)

    trainer.train(validate_every=config['validate_every'], iterations=config['iterations'])


def add_args(parser):
    parser.add_argument('-d', '--directory', type=str,
                        help='Directory of the TFRecords.')
    parser.add_argument('-a', '--additional_val_directory', type=str, default=None,
                        help='An extra validation directory.')
    parser.add_argument('-r', '--raw_vimeo_train_directory', type=str, default=None,
                        help='An extra optional directory for frame consistency validation.')
    parser.add_argument('-c', '--checkpoint_directory', type=str,
                        help='Directory of saved checkpoints.')
    parser.add_argument('-w', '--pwcnet_weights_path', type=str,
                        help='Path to the .npz weights for a pre-trained PWCNet.')
    parser.add_argument('-l', '--validation_logging_period', type=int, default=1000,
                        help='Every validation_logging_period samples we log 1 validation sample to Tensorboard.')
    parser.add_argument('-f', '--fine_tune', type=int, default=0,
                        help='Whether to use the fine tuning loss.')
    parser.add_argument('-m', '--use_middlebury_warp', type=int, default=0,
                        help='Whether to use the Middlebury warp.')
    parser.add_argument('-i', '--iterations', type=int, default=1000000,
                        help='Number of iterations to train for.')
    parser.add_argument('-j', '--config', type=str,
                        default='mains/configs/context_interp_schedule/train_context_interp_base.json',
                        help='Config json file path.')
    parser.add_argument('-pc', '--pwcnet_config', type=str, default=None,
                        help='PWCNet config json file path. If None, then the default config options will be used.')


if __name__ == "__main__":
    main()
