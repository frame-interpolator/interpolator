import argparse
import os
import tensorflow as tf
import json
from common.utils.config import preprocess_var_refs, import_json
from data.interp.interp_data import InterpDataSet, InterpAugmentations
from interp.inpaint_interp.model import InpaintInterp
from pwcnet.factory import create_pwcnet
from train.interp.inpaint_interp_trainer import InpaintInterpTrainer


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True
    session = tf.Session(config=config_proto)

    # Read the JSON config.
    print('Loading configurations...')
    config = import_json(args.config)
    preprocess_var_refs(config)

    # Add extra fields to the config from argparse.
    config['checkpoint_directory'] = args.checkpoint_directory
    config['iterations'] = args.iterations
    config['vimeo_train_raw_dir'] = args.raw_vimeo_train_directory

    msg = 'There must be either an existing checkpoint or PWCNet weights that we can load.'
    assert os.path.exists(args.checkpoint_directory) or args.pwcnet_weights_path is not None, msg
    if not os.path.exists(args.checkpoint_directory):
        os.makedirs(args.checkpoint_directory)

    # Repeat important input argument information back to user.
    print('Will train using these configurations: ')
    print(config)
    print('')

    print('Creating network...')
    pwcnet, _ = create_pwcnet(args.pwcnet_config if args.pwcnet_config is not None else {})
    model = InpaintInterp(pwcnet,
                          context_type=config['context_type'],
                          loss_weights=config['loss_weights'],
                          stop_gradient_to_pwcnet=not config['gradients_to_pwcnet'],
                          use_time_channel=config['use_time_channel'],
                          use_confidence=config['use_confidence'],
                          use_gridnet=config['use_gridnet'])

    print('Creating dataset...')
    augmentations = InterpAugmentations(flip_prob=0.5,
                                        duplicate_endpoint_frames_prob=config['endpoint_dup_prob'],
                                        do_center_crop=False,
                                        crop_size=(config['crop_height'], config['crop_width']))
    inbetween_locs = config['inbetween_times']
    dataset = InterpDataSet(args.directory, inbetween_locs,
                            batch_size=config['batch_size'],
                            augmentations=augmentations,
                            extra_validation_dir=args.additional_val_directory)

    print('Initializing trainer...')
    trainer = InpaintInterpTrainer(model, [dataset], session, config)

    print('Initializing variables...')
    session.run(tf.global_variables_initializer())
    trainer.restore()

    # Possibly overwrite existing checkpoint PWCNet weights with new ones:
    # This is useful in the case that a newer PWCNet architecture is available
    # (in which case trainer.restore() would leave them as garbage / newly randomly initialized),
    # but you would like the synthesis network variables to continue to be used.
    if args.pwcnet_weights_path is not None:
        print('Would you like to over-write PWCNet checkpoint variables with weights from %s? (y/n)' %
              args.pwcnet_weights_path, flush=True)
        response = input()
        if response == 'y':
            print('Loading pre-trained PWCNet...', flush=True)
            model.load_pwcnet_weights(args.pwcnet_weights_path, session)
        else:
            print('Will not overwrite PWCNet checkpoint variables.', flush=True)

    trainer.train(validate_every=config['validate_every'], iterations=config['iterations'])


def add_args(parser):
    parser.add_argument('-d', '--directory', type=str,
                        help='Directory of the TFRecords.')
    parser.add_argument('-a', '--additional_val_directory', type=str, default=None,
                        help='An optional extra validation directory.')
    parser.add_argument('-r', '--raw_vimeo_train_directory', type=str, default=None,
                        help='An optional extra validation directory for frame consistency validation.')
    parser.add_argument('-c', '--checkpoint_directory', type=str,
                        help='Directory of saved checkpoints.')
    parser.add_argument('-w', '--pwcnet_weights_path', type=str,
                        help='Path to the .npz weights for a pre-trained PWCNet.')
    parser.add_argument('-i', '--iterations', type=int, default=1000000,
                        help='Number of iterations to train for.')
    parser.add_argument('-j', '--config', type=str,
                        default='mains/configs/inpaint_interp_configs/base.json',
                        help='Config json file path.')
    parser.add_argument('-pc', '--pwcnet_config', type=str, default=None,
                        help='PWCNet config json file path. If None, then the default config options will be used.')


if __name__ == "__main__":
    main()
