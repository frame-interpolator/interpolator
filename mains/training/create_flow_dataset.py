import argparse
from common.utils.config import preprocess_var_refs, import_json
from data.flow.creative.creative_preprocessor import CreativeFlowDataPreprocessor
from data.flow.kitti.kitti_preprocessor import KittiFlowDataPreprocessor
from data.flow.sintel.sintel_preprocessor import SintelFlowDataPreprocessor
from data.flow.flyingchairs.flyingchairs_preprocessor import FlyingChairsFlowDataPreprocessor
from data.flow.flyingthings.flyingthings_preprocessor import FlyingThingsFlowDataPreprocessor


def main():
    """
    This program requires the following data structure at the root directory:

    For Sintel:
        <directory to images>
            <set_0>
                image_<ID>.png
                ...
            <set_1>
            ...
            <set_n>
        <directory to flows>
            <set_0>
                flow_<ID>.fo
                ...
            <set_1>
            ...
            <set_n>

    For FlyingChairs:
        <data directory>
            <ID>_img1.ppm
            <ID>_img2.ppm
            <ID>_flow.flo
            ...

    For FlyingThings:
        frames
            TEST
            TRAIN
                <set>
                    <clip>
                        left
                            0000.png
                            ...
                        right
        optical_flow
            TEST
            TRAIN
                <set>
                    <clip>
                        into_future
                            left
                                OpticalFlowIntoFuture_0000_L.pfm
                                ...
                            right
                        into_past
    For Creative Flow+:
        mixamo
        shapenet
        web
        ...
    For Kitti:
        2015
            training
                flow_occ
                    000000_10.png
                    ...
                image_2
                    000000_10.png
                    000000_11.png
                    ...
            ...
        2012
            training
                flow_occ
                    000000_10.png
                    ...
                colored_0
                    000000_10.png
                    000000_11.png
                    ...
            ...
    """
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()
    if args.output_directory == '':
        args.output_directory = args.directory

    config = {}
    if args.config != '':
        config = import_json(args.config)
        preprocess_var_refs(config)
        print('Using config:')
        print(config)

    preprocessor_constructor = SintelFlowDataPreprocessor  # Sintel by default.
    if args.data_source == 'sintel':
        preprocessor_constructor = SintelFlowDataPreprocessor
    elif args.data_source == 'flyingchairs':
        preprocessor_constructor = FlyingChairsFlowDataPreprocessor
    elif args.data_source == 'flyingthings':
        preprocessor_constructor = FlyingThingsFlowDataPreprocessor
    elif args.data_source == 'creative':
        preprocessor_constructor = CreativeFlowDataPreprocessor
    elif args.data_source == 'kitti':
        preprocessor_constructor = KittiFlowDataPreprocessor

    preprocessor = preprocessor_constructor(args.directory, args.output_directory, validation_size=args.num_validation,
                                            shard_size=args.shard_size, verbose=True, config=config)
    preprocessor.preprocess_raw()


def add_args(parser):
    parser.add_argument('-d', '--directory', type=str,
                        help='Directory of the raw dataset.')
    parser.add_argument('-v', '--num_validation', type=int, default=100,
                        help='Number of data examples to use for validation.')
    parser.add_argument('-s', '--shard_size', type=int, default=1,
                        help='Maximum number of data examples in a shard.')
    parser.add_argument('-src', '--data_source', type=str, default='sintel',
                        help='Data source can be sintel, flyingchairs, flyingthings, creative, or kitti.')
    parser.add_argument('-o', '--output_directory', type=str, default='',
                        help='Directory of the output. Defaults to be the directory of the input.')
    parser.add_argument('-c', '--config', type=str, default='',
                        help='Config file input if the preprocessor requires it.')


if __name__ == "__main__":
    main()
