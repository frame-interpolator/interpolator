import argparse
import json
import os
import subprocess
from subprocess import CalledProcessError
from common.utils.config import compile_args, preprocess_var_refs, import_json


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    cwd = os.getcwd()
    print('CWD:', os.getcwd())

    # Read the JSON schedule.
    print('Loading schedule...')
    schedule = import_json(args.schedule)
    preprocess_var_refs(schedule)
    print(json.dumps(schedule, sort_keys=True, indent=4))
    print('')

    runs = schedule['runs']
    end_idx = len(runs) - 1 if args.end_at == -1 else args.end_at
    assert args.start_from < len(runs)
    assert isinstance(runs, list)
    for i in range(args.start_from, end_idx + 1):
        run = runs[i]
        assert isinstance(run, dict)
        assert 'executable' in run
        assert 'args' in run
        script = run['executable']
        assert isinstance(script, str)
        args_dict = run['args']
        assert isinstance(args_dict, dict)

        arg_str = ' '.join([script] + compile_args(args_dict))
        print('\nRUNNING:', arg_str, '\n')
        process = subprocess.Popen(arg_str, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                   cwd=cwd, shell=True,
                                   bufsize=1, universal_newlines=True)
        for line in process.stdout:
            print(line, end='')
        print('Waiting for process to end.')
        process.wait()
        if process.returncode != 0:
            raise CalledProcessError(process.returncode, process.args)


def add_args(parser):
    parser.add_argument('-s', '--schedule', type=str,
                        help='Json formatted schedule path.')
    parser.add_argument('-sf', '--start_from', type=int, default=0,
                        help='Run index to start from.')
    parser.add_argument('-e', '--end_at', type=int, default=-1,
                        help='Run index to end at, inclusive.')


if __name__ == "__main__":
    main()
