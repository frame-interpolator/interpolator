import argparse
import os
import tensorflow as tf
from interp.context_interp.model import ContextInterp
from common.utils.tf import optimistic_restore
from interp.interp_saved_model.synth_saved_model import SynthSavedModel


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    print('Creating session...')
    config_proto = tf.ConfigProto()
    config_proto.gpu_options.allow_growth = True
    config_proto.allow_soft_placement = True
    session = tf.Session(config=config_proto)

    print('Creating network...')
    model = ContextInterp(use_differentiable_warp=not bool(args.use_middlebury_warp))
    synth_model_saver = SynthSavedModel(os.path.join(args.output_dir, 'synthesis'))
    synth_model_saver.create_graph(model)

    print('Initializing variables...')
    session.run(tf.global_variables_initializer())

    saver = tf.train.Saver()
    if tf.train.latest_checkpoint(args.checkpoint_dir) is not None:
        print('Restoring checkpoint...')
        checkpoint_file = tf.train.latest_checkpoint(args.checkpoint_dir)
        try:
            saver.restore(session, checkpoint_file)
        except Exception as e:
            print('')
            print(e.message)
            print('')
            print('Will attempt optimistic restore instead...')
            optimistic_restore(session, checkpoint_file)

    print('Freezing...')
    synth_model_saver.freeze(model, session)


def add_args(parser):
    parser.add_argument('-c', '--checkpoint_dir', type=str,
                        help='Checkpoint directory.')
    parser.add_argument('-o', '--output_dir', type=str,
                        help='Directory to output the frozen model.')
    parser.add_argument('-m', '--use_middlebury_warp', type=int, default=0,
                        help='Whether to use the Middlebury warp.')


if __name__ == "__main__":
    main()
