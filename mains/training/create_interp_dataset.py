import argparse
import os
from data.interp.interp_data_preprocessor import InterpDataPreprocessor


def main():
    """
    Each video sequence must be in its own folder, and sorting the images in the folder by name should
    create a valid video sequence. DAVIS, Creative Flow+, and Vimeo-90k all follow this structure.
    For Vimeo-90k, the directory should be the root of the unzipped dataset (important if using whitelist).
    If provided, the whitelist file should look similar to the following example:
    valid_path1
    valid_path2/foo
    """
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    tf_records_directory = args.out_directory
    if not os.path.exists(tf_records_directory):
        os.mkdir(tf_records_directory)

    # Process whitelist if there is one.
    if args.whitelist is not None:
        assert args.data_source == 'vimeo'
        with open(args.whitelist, 'r') as f:
            # I can't ever remember how to do this:
            # https://stackoverflow.com/questions/3277503/how-to-read-a-file-line-by-line-into-a-list
            whitelist = f.readlines()
        whitelist = [os.path.join('sequences', x.strip()) for x in whitelist]
        print('There are %d whitelisted folders.' % len(whitelist))
    else:
        whitelist = None

    # We will want to mix up the order of sequences in Creative Flow
    # (i.e don't want validation split on sorted sequences, since it'll all be 1 rendering style).
    if args.data_source == 'creative':
        do_randomize = True
        crop_size = (750, 750)
        print('Frames will be center-cropped to %dx%d before saving.' % (crop_size[0], crop_size[1]))
    else:
        do_randomize = False
        crop_size = None

    input_directory = args.directory
    dataset = InterpDataPreprocessor(tf_records_directory, [[1]],
                                     shard_size=args.shard_size,
                                     validation_size=args.num_validation,
                                     verbose=True,
                                     crop_size=crop_size)

    dataset.preprocess_raw(input_directory, whitelist=whitelist, do_randomize=do_randomize)


def add_args(parser):
    parser.add_argument('-d', '--directory', type=str,
                        help='Directory containing the sequences to process.')
    parser.add_argument('-o', '--out_directory', type=str,
                        help='Directory that will host the output TFRecords.')
    parser.add_argument('-v', '--num_validation', type=int, default=100,
                        help='Minimum number of data examples to use for validation.')
    parser.add_argument('-s', '--shard_size', type=int, default=2,
                        help='Maximum number of data examples in a shard.')
    parser.add_argument('-src', '--data_source', type=str, default='vimeo',
                        help='Data source can be vimeo or creative.')
    parser.add_argument('-w', '--whitelist', type=str, default=None,
                        help='Path to the whitelist file. Only supported for vimeo.')


if __name__ == "__main__":
    main()
