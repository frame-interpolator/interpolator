import argparse
import glob
import os.path


def main():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    images = glob.glob(os.path.join(args.directory, '**', '*.png'), recursive=True)
    images.sort()
    images_a = []
    images_b = []
    output_flows = []
    for i in range(len(images) - 1):
        directory_a = os.path.dirname(images[i])
        directory_b = os.path.dirname(images[i + 1])
        if directory_a == directory_b:
            images_a.append(images[i])
            images_b.append(images[i + 1])
            frame_file_name = os.path.splitext(os.path.relpath(images[i], args.directory))[0]
            output_flows.append(os.path.join(args.output, frame_file_name + '.' + args.extension))
    assert len(images_a) == len(images_b)
    assert len(images_a) == len(output_flows)

    for i in range(len(images_a)):
        print(images_a[i] + ', ' + images_b[i] + ', ' + output_flows[i])


def add_args(parser):
    parser.add_argument('-d', '--directory', type=str,
                        help='Path to the Sintel test directory.')
    parser.add_argument('-o', '--output', type=str,
                        help='Path to the output flow directory.')
    parser.add_argument('-e', '--extension', type=str, default='flo',
                        help='Output extension (flo or png).')


if __name__ == "__main__":
    main()
