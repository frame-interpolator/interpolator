import tensorflow as tf
from data.dataset import DataSet


class Trainer:
    def __init__(self, model, datasets, session, config, verbose=True):
        """
        :param model: A PWCNet object.
        :param datasets: A list of flow datasets.
        :param session: Tensorflow session.
        :param config: Dictionary.
        :param verbose: Bool.
        """
        assert isinstance(session, tf.Session)
        assert isinstance(datasets, list)
        assert len(datasets) > 0
        assert isinstance(datasets[0], DataSet)
        self.model = model
        self.datasets = datasets
        self.session = session
        self.config = config
        self.verbose = verbose

    def restore(self):
        raise NotImplementedError('restore() is not implemented.')

    def train(self, validate_every=10000, iterations=1000000):
        """
        Runs a training loop that validates every so often.
        :param validate_every: Perform validation at this interval.
        :param iterations: Number of iterations to train for. This is expected to be a multiple of validate_every.
        :return: Nothing.
        """
        num_iterations = 0
        while True:
            if self.verbose:
                print('Training.')
            self.train_for(min(validate_every, iterations))
            if self.verbose:
                print('Validating.')
            self.validate()
            num_iterations += validate_every
            if num_iterations >= iterations:
                return

    def train_for(self, iterations):
        """
        :param iterations: Number of iterations to train for.
        :return: Nothing.
        """
        raise NotImplementedError('train_for() is not implemented.')

    def validate(self):
        raise NotImplementedError('validate() is not implemented.')

    def _write_config_summary(self, summary_writer):
        """
        Writes the config summary to a given writer.
        :param summary_writer: Tensorflow summary FileWriter (tf.summary.FileWriter).
        :return: Nothing.
        """
        assert isinstance(summary_writer, tf.summary.FileWriter)

        # Config summary.
        text = []
        for key in sorted(self.config.keys()):
            text.append([key, str(self.config[key])])
        config_summary = tf.summary.text('Configurations', tf.convert_to_tensor(text))

        # Write the config summary.
        summary_writer.add_summary(self.session.run(config_summary), global_step=0)
        summary_writer.flush()
