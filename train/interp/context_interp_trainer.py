import tensorflow as tf
from common.utils.tf import AdamaxOptimizer
from common.utils.flow import get_tf_flow_visualization
from interp.context_interp.model import ContextInterp
from data.interp.interp_data import InterpDataSet
from train.interp.interp_trainer import InterpTrainer


class ContextInterpTrainer(InterpTrainer):
    def __init__(self, model, datasets, session, config, verbose=True):
        assert isinstance(model, ContextInterp)
        assert isinstance(datasets[0], InterpDataSet)

        # Variables needed for Tensorboard logging.
        self.images_a = None
        self.images_b = None
        self.images_c = None
        self.images_b_pred = None
        self.warped_a_b_bidir = None
        self.warped_c_b_bidir = None
        self.warped_a_b_interp = None
        self.warped_c_b_interp = None
        self.warped_a_b_gaussian = None
        self.warped_c_b_gaussian = None
        self.flow_a_c = None
        self.flow_c_a = None
        self.loss = None
        super().__init__(model, datasets, session, config, verbose)

    def build_interp_train_graph(self, next_sequence_tensor, next_sequence_timing_tensor, global_step):
        """
        Overriden.
        """
        self.images_a = next_sequence_tensor[:, 0]
        self.images_b = next_sequence_tensor[:, 1]
        self.images_c = next_sequence_tensor[:, 2]

        # Get the train network.
        self.images_b_pred, tensors_dict = self.model.get_forward(self.images_a, self.images_c,
                                                                  self.next_sequence_timing_tensor[:, 1],
                                                                  reuse_variables=tf.AUTO_REUSE)

        self.warped_a_b_gaussian = tensors_dict['warped_0_1_gaussian']
        self.warped_c_b_gaussian = tensors_dict['warped_1_0_gaussian']
        self.flow_a_c = tensors_dict['flow_0_1']
        self.flow_c_a = tensors_dict['flow_1_0']
        self.warped_a_b_interp = tensors_dict['warped_0_1_interp']
        self.warped_c_b_interp = tensors_dict['warped_1_0_interp']
        self.warped_a_b_bidir = tensors_dict['warped_0_1_bidir']
        self.warped_c_b_bidir = tensors_dict['warped_1_0_bidir']

        # Get the loss function.
        if self.config['fine_tune']:
            self.loss = self.model.get_fine_tuning_loss(self.images_b_pred, self.images_b)
        else:
            self.loss = self.model.get_training_loss(self.images_b_pred, self.images_b)

        # Get the optimizer.
        with tf.variable_scope('train'):
            self.train_op = AdamaxOptimizer(self.config['learning_rate'], beta1=0.9, beta2=0.999).minimize(
                self.loss, global_step=global_step
            )
        return self.train_op

    def get_summaries_dicts(self):
        """
        Overriden.
        """
        # Get overlays.
        weight = 0.60
        image_overlays = self.images_a * weight + self.images_c * (1 - weight)

        # Images will appear 'washed out' otherwise, due to tensorboard's own normalization scheme.
        clipped_preds = tf.clip_by_value(self.images_b_pred, 0.0, 1.0)
        clipped_warp_a_b = tf.clip_by_value(self.warped_a_b_bidir, 0.0, 1.0)
        clipped_warp_b_a = tf.clip_by_value(self.warped_c_b_bidir, 0.0, 1.0)
        clipped_warp_a_b_interp = tf.clip_by_value(self.warped_a_b_interp, 0.0, 1.0)
        clipped_warp_c_b_interp = tf.clip_by_value(self.warped_c_b_interp, 0.0, 1.0)
        clipped_warp_a_b_gaussian = tf.clip_by_value(self.warped_a_b_gaussian, 0.0, 1.0)
        clipped_warp_c_b_gaussian = tf.clip_by_value(self.warped_c_b_gaussian, 0.0, 1.0)

        images_summaries_dict = {
            'overlay': image_overlays,
            'inbetween_gt': self.images_b,
            'inbetween_pred': clipped_preds,
            'warped_a_b_bidir': clipped_warp_a_b,
            'warped_c_b_bidir': clipped_warp_b_a,
            'warped_a_b_interp': clipped_warp_a_b_interp,
            'warped_c_b_interp': clipped_warp_c_b_interp,
            'warped_a_b_gaussian': clipped_warp_a_b_gaussian,
            'warped_c_b_gaussian': clipped_warp_c_b_gaussian,
            'flow_a_c': get_tf_flow_visualization(self.flow_a_c),
            'flow_c_a': get_tf_flow_visualization(self.flow_c_a),
            'image_a': self.images_a,
            'image_c': self.images_c,
        }

        scalars_summaries_dict = {
            self.loss_key: self.loss,
            'ssim': tf.reduce_mean(tf.image.ssim(self.images_b, clipped_preds, max_val=1.0)),
            'ms_ssim': tf.reduce_mean(tf.image.ssim_multiscale(self.images_b, clipped_preds, max_val=1.0)),
            'psnr': tf.reduce_mean(tf.image.psnr(self.images_b, clipped_preds, max_val=1.0)),
        }

        histograms_summaries_dict = {}
        return images_summaries_dict, scalars_summaries_dict, histograms_summaries_dict
