import os.path
import random
import tensorflow as tf
from common.utils.misc import print_progress_bar, get_git_revision_short_hash
from common.utils.tf import optimistic_restore, Logger
from data.interp.interp_data import InterpDataSet
from train.trainer import Trainer
from pwcnet.model import PWCNet
from interp.interp_saved_model.synth_saved_model import SynthSavedModel
from mains.evaluate_interp import get_metrics
from common.utils.interp_eval import load_saved_models
import time
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt


class InterpTrainer(Trainer):
    def __init__(self, model, datasets, session, config, verbose=True):
        super().__init__(model, datasets, session, config, verbose)
        assert isinstance(datasets[0], InterpDataSet)

        # Special summary writing keys.
        self.loss_key = 'total_loss'
        self.times_key = 'times'

        short_git_hash = get_git_revision_short_hash()
        self.saved_models_dir = os.path.join(self.config['checkpoint_directory'], 'saved_model_' + str(short_git_hash))
        for dataset in self.datasets:
            dataset.load(self.session)
        self.next_sequence_tensor, self.next_sequence_timing_tensor = self.datasets[0].get_next_batch()
        with tf.variable_scope('train'):
            self.global_step = tf.Variable(initial_value=0, trainable=False, dtype=tf.int32, name='global_step')
        self.train_op = self.build_interp_train_graph(self.next_sequence_tensor,
                                                      self.next_sequence_timing_tensor,
                                                      self.global_step)
        # Checkpoint saving.
        self.saver = tf.train.Saver()
        self.valid_logger = Logger(os.path.join(self.config['checkpoint_directory'], 'valid'), self.session.graph)
        self.train_logger = Logger(os.path.join(self.config['checkpoint_directory'], 'train'), self.session.graph)
        self.valid_logger_extra = Logger(os.path.join(self.config['checkpoint_directory'], 'valid_extra'),
                                         self.session.graph)
        with tf.name_scope('summaries'):
            self._write_config_summary(self.train_logger.writer)
            s = self.get_summaries_dicts()
            self.images_summaries_dict, self.scalars_summaries_dict, self.histograms_summaries_dict = s

        # Saved models.
        self.synth_model_path = os.path.join(self.saved_models_dir, 'synthesis')
        self.synth_model_saver = SynthSavedModel(self.synth_model_path)
        self.flow_model_saver = PWCNet.create_frozen_model(os.path.join(self.saved_models_dir, 'flow'))

    def build_interp_train_graph(self, next_sequence_tensor, next_timing_tensor, global_step):
        """
        Builds the TensorFlow training graph.
        :param next_sequence_tensor: An image sequence tensor, of shape [batch_size, seq_len, H, W, 3].
        :param next_timing_tensor: An image sequence timing information tensor, of shape [batch_size, seq_len + 2].
        :param global_step: A tf.int32 defining the global training step.
        :return: train_op: The TensorFlow train op.
        """
        raise NotImplementedError

    def get_summaries_dicts(self):
        """
        :return: scalars_dict: A dictionary of Tensor scalars to log.
                 images_dict: A dictionary of Tensors to log as images.
                 histograms_dict: A dictionary of Tensors to log as histograms.
        """
        raise NotImplementedError

    def restore(self):
        """
        Overridden.
        """
        if tf.train.latest_checkpoint(self.config['checkpoint_directory']) is not None:
            print('Restoring checkpoint...')
            checkpoint_file = tf.train.latest_checkpoint(self.config['checkpoint_directory'])
            try:
                self.saver.restore(self.session, checkpoint_file)
            except Exception as e:
                print('')
                print(e.message)
                print('')
                print('Will attempt optimistic restore instead...')
                optimistic_restore(self.session, checkpoint_file)

    def train_for(self, iterations):
        """
        Overridden.
        """

        start_t = time.time()
        for i in range(iterations):
            dataset_idx = random.randint(0, len(self.datasets) - 1)
            if i % self.config['log_every'] == 0:
                run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                run_metadata = tf.RunMetadata()
                _ = self.session.run(self.train_op,
                                     feed_dict=self.datasets[dataset_idx].get_train_feed_dict(),
                                     options=run_options, run_metadata=run_metadata)

                # Get the summaries.
                global_step = self._eval_global_step()
                self.train_logger.add_run_metadata(run_metadata, global_step)
                summaries = self._get_np_summaries(feed_dict=self.datasets[dataset_idx].get_train_feed_dict())
                images_dict, scalars_dict, histograms_dict, timing = summaries

                # Write the summaries, along with the time for the inbetween frame.
                inbetween_times = timing[:, 1]
                for key, value in images_dict.items():
                    keys = self._get_keys_from_times(key, inbetween_times)
                    self.train_logger.log_images(keys, value, global_step)
                for key, value in scalars_dict.items():
                    self.train_logger.log_scalar(key, value, global_step)
                for key, value in histograms_dict.items():
                    self.train_logger.log_histogram(key, value, global_step)

                # Save model checkpoint.
                print('Saving model checkpoint...')
                ckpt_save_path = self.saver.save(self.session,
                                                 os.path.join(self.config['checkpoint_directory'], 'model.ckpt'),
                                                 global_step=self._eval_global_step())
                print('Model checkpoint saved in path:', ckpt_save_path)
                self.save_saved_models()
            else:
                _ = self.session.run(self.train_op, feed_dict=self.datasets[dataset_idx].get_train_feed_dict())
            if i % 10 == 0:
                print_progress_bar(i + 1, iterations,
                                   prefix='Train Steps (%.2f s / step)' % ((time.time() - start_t) / 10),
                                   suffix='Complete', use_percentage=False)
                start_t = time.time()
            if (i == iterations - 1 or i % self.config['validate_extras_every'] == 0) and i >= self.config['log_every']:
                self.validate_extra()

    def save_saved_models(self):
        self.synth_model_saver.freeze(self.model, self.session)
        self.flow_model_saver.freeze(self.model.pwcnet, self.session)

    def load_saved_models(self):
        """
        :return: synthesis model, flow model.
        """
        synth_model, flow_model = load_saved_models(self.synth_model_path)
        return synth_model, flow_model

    def _add_summaries_to_np_dicts(self, np_summaries, image_summaries, scalar_summaries,
                                   batch_size, iter, validation_logging_period):
        images_dict, scalars_dict, histograms_dict, timing = np_summaries
        for key, value in images_dict.items():
            if key not in image_summaries:
                image_summaries[key] = []
            for i, image in enumerate(value):
                cur_idx = i + batch_size * iter
                if cur_idx % validation_logging_period == 0:
                    image_summaries[key].append(image)

        # Accumulate scalar summary values.
        for key, value in scalars_dict.items():
            if key not in scalar_summaries:
                scalar_summaries[key] = 0.0
            scalar_summaries[key] += value

    def validate_extra(self):
        print('Starting extra validation...', flush=True)
        global_step = self._eval_global_step()
        # Log metrics for frame consistency over different time steps as histograms.
        # For speed purposes, we only evaluate on a small percentage of the combined train/val set.
        if 'vimeo_train_raw_dir' in self.config:

            # Copied from https://stackoverflow.com/questions/7821518/matplotlib-save-plot-to-numpy-array.
            def _get_plot(array):
                # Make a random plot...
                fig = plt.figure()
                fig.tight_layout(pad=0)
                x_axis = np.arange(len(array)) / float(len(array) - 1)
                plt.plot(x_axis, array)

                # If we haven't already shown or saved the plot, then we need to
                # draw the figure first...
                fig.canvas.draw()

                # Now we can save it to a numpy array.
                data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
                data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))
                return data

            synth_model, flow_model = self.load_saved_models()
            metrics, _ = get_metrics(synth_model, flow_model, self.config['vimeo_train_raw_dir'], 0.3,
                                     self.session, num_inbetween_steps=5)
            psnr_plot = _get_plot(metrics.psnr_steps)
            ssim_plot = _get_plot(metrics.ssim_steps)
            self.valid_logger_extra.log_images('psnr_steps', [psnr_plot], global_step)
            self.valid_logger_extra.log_images('ssim_steps', [ssim_plot], global_step)

        self.datasets[0].init_validation_data(self.session)
        if self.datasets[0].has_extra_validation:
            print('Validating on the extra validation set...', flush=True)

        # Go over the smaller extra validation set.
        hit_np_val_end = not self.datasets[0].has_extra_validation
        image_summaries_extra = {}
        scalar_summaries_extra = {}
        batch_iter = 0
        while not hit_np_val_end:
            hit_np_val_end = self.datasets[0].ready_next_np_validation_element(self.session)
            np_summaries = self._get_np_summaries(self.datasets[0].get_validation_np_feed_dict())
            iter = self.datasets[0].validation_np_data.iter
            max_iter = self.datasets[0].validation_np_data.num_elements
            if not hit_np_val_end:
                print_progress_bar(iter, max_iter, prefix='Validating on extra set', suffix='Completed',
                                   use_percentage=False)
            else:
                print_progress_bar(max_iter, max_iter, prefix='Validating on extra set', suffix='Completed',
                                   use_percentage=False)
            self._add_summaries_to_np_dicts(np_summaries, image_summaries_extra, scalar_summaries_extra,
                                            1, batch_iter, 1)
            batch_iter += 1

        # Log images.
        for key, value in image_summaries_extra.items():
            self.valid_logger_extra.log_images(key, value, global_step, max_width=640)

        # Average and log accumulated scalars.
        for key, value in scalar_summaries_extra.items():
            self.valid_logger_extra.log_scalar(key, value / batch_iter, global_step)
   
    def validate(self):
        """
        Overridden.
        """
        self.datasets[0].init_validation_data(self.session)
        global_step = self._eval_global_step()

        print('Starting validation on the main set...', flush=True)

        # Go over the main validation set.
        batch_iter = 0
        image_summaries = {}
        scalar_summaries = {}
        while True:
            try:
                np_summaries = self._get_np_summaries(self.datasets[0].get_validation_feed_dict())
                self._add_summaries_to_np_dicts(np_summaries, image_summaries, scalar_summaries,
                                                self.datasets[0].batch_size, batch_iter,
                                                self.config['validation_logging_period'])
                batch_iter += 1
                if batch_iter % 1000 == 0:
                    print('Validated %d examples.' % (batch_iter * self.datasets[0].batch_size))

            except tf.errors.OutOfRangeError:
                # End of validation epoch.
                break

        # Log images.
        for key, value in image_summaries.items():
            self.valid_logger.log_images(key, value, global_step)

        # Average and log accumulated scalars.
        for key, value in scalar_summaries.items():
            self.valid_logger.log_scalar(key, value / batch_iter, global_step)

    def _get_np_summaries(self, feed_dict):
        np_image_summaries_dict = {}
        np_scalars_summaries_dict = {}
        np_histograms_summaries_dict = {}
        num_image_entries = len(self.images_summaries_dict)
        num_scalar_entries = len(self.scalars_summaries_dict)
        num_histogram_entries = len(self.histograms_summaries_dict)
        tensors = []

        # Add tensors to query.
        for key, value in self.images_summaries_dict.items():
            tensors.append(value)
        for key, value in self.scalars_summaries_dict.items():
            tensors.append(value)
        for key, value in self.histograms_summaries_dict.items():
            tensors.append(value)
        tensors.append(self.next_sequence_timing_tensor)

        # Put the numpy arrays in the right dictionaries.
        np_vals = self.session.run(tensors, feed_dict=feed_dict)
        assert len(np_vals) == num_scalar_entries + num_image_entries + num_histogram_entries + 1
        for i, (key, value) in enumerate(self.images_summaries_dict.items()):
            np_image_summaries_dict[key] = np_vals[i]
        for i, (key, value) in enumerate(self.scalars_summaries_dict.items()):
            idx = i + num_image_entries
            np_scalars_summaries_dict[key] = np_vals[idx]
        for i, (key, value) in enumerate(self.histograms_summaries_dict.items()):
            idx = i + num_image_entries + num_scalar_entries
            np_histograms_summaries_dict[key] = np_vals[idx]
        timing = np_vals[-1]
        return np_image_summaries_dict, np_scalars_summaries_dict, np_histograms_summaries_dict, timing

    def _eval_global_step(self):
        return self.session.run(self.global_step)

    def _get_keys_from_times(self, prefix, times):
        """
        :param prefix: Str.
        :param times: A list of floats of any length.
        :return: Prefix appended with times. e.g 'prefix_0', 'prefix_1'.
        """
        assert type(prefix) is str
        keys = [prefix + ', pred_t=' + str(round(time, 2)) for time in times]
        return keys
