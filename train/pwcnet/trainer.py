import numpy as np
import os.path
import random
import tensorflow as tf
import time
from common.utils.flow import get_tf_flow_visualization
from common.utils.multi_gpu import get_available_gpus, TensorIO, create_train_op
from common.utils.profile import save_timeline
from data.flow.flow_data import FlowDataSet
from pwcnet.model import PWCNet
from mains.eval.eval_sintel_dataset import eval_sintel
from tensorboard.plugins.beholder import Beholder
from train.trainer import Trainer


class PWCNetTrainer(Trainer):
    def __init__(self, model, datasets, session, config, verbose=True, lr_half_life=-1):
        """
        :param model: A neural network model.
        :param datasets: A list of dataset objects.
        :param session: Tensorflow session.
        :param config: Dictionary.
        :param verbose: Bool.
        :param lr_half_life: Float. Iteration at which the learning rate will be halved.
                If negative, then no decay will be used.
        """
        super().__init__(model, datasets, session, config, verbose)
        assert isinstance(self.model, PWCNet)
        assert isinstance(datasets[0], FlowDataSet)

        self.lr_half_life = lr_half_life
        # Iteration counter local to this trainer instance (not the global training step).
        self.local_iter = 0

        # Gradient clipping parameter. Set if exists in the config and its value is greater than 0.
        self.gradient_clipping_norm = None
        if 'gradient_clipping_norm' in self.config:
            if self.config['gradient_clipping_norm'] > 0:
                self.gradient_clipping_norm = self.config['gradient_clipping_norm']

        for dataset in self.datasets:
            dataset.load(self.session)
        self.images_a, self.images_b, self.flows, self.next_valid_mask = self.datasets[0].get_next_batch()

        # Create the network's forward and train ops.
        self.final_flow = None
        self.previous_flows = None
        self.loss = None
        self.layer_losses = None
        self.regularization_loss = None
        self.global_step = None
        self.train_op = None
        self.valid_mask = None
        self.learning_rate = tf.placeholder(dtype=tf.float32, shape=(), name='learning_rate')
        self.training = tf.placeholder(dtype=tf.bool, shape=(), name='training')
        self._create_ops()

        # Summary variables.
        self.merged_summ = None
        self.train_writer = None
        self.valid_writer = None
        self.train_log_dir = os.path.join(self.config['checkpoint_directory'], 'train')
        self.valid_log_dir = os.path.join(self.config['checkpoint_directory'], 'valid')
        self._make_summaries()

        # Checkpoint saving.
        self.saver = tf.train.Saver()
        self.npz_save_file = os.path.join(self.config['checkpoint_directory'], 'pwcnet_weights.npz')

        # Frozen model saving.
        self.frozen_model_dir = os.path.join(self.config['checkpoint_directory'], 'frozen')
        self.frozen_model = PWCNet.create_frozen_model(self.frozen_model_dir)

        # Beholder.
        self.beholder = None
        self.use_beholder = False if 'use_beholder' not in self.config else self.config['use_beholder']
        if self.use_beholder:
            self.beholder = Beholder(self.config['checkpoint_directory'])

        # Sintel validation input folders.
        self.sintel_clean_dir = None if 'sintel_clean_dir' not in self.config else self.config['sintel_clean_dir']
        self.sintel_final_dir = None if 'sintel_final_dir' not in self.config else self.config['sintel_final_dir']

    def _create_ops(self):
        optimizer = tf.train.AdamOptimizer(self.learning_rate)

        def build_network_outputs(images_a, images_b, flows, valid_mask):
            # Get the train network.
            final_flow, previous_flows, _ = self.model.get_forward(images_a, images_b, reuse_variables=tf.AUTO_REUSE,
                                                                   training=self.training)
            if self.config['fine_tune']:
                loss, layer_losses, regularization_loss = self.model.get_fine_tuning_loss(previous_flows, flows,
                                                                                          loss_mask=valid_mask)
            else:
                loss, layer_losses, regularization_loss = self.model.get_training_loss(previous_flows, flows,
                                                                                       loss_mask=valid_mask)

            tensor_io_dict = {
                'loss': TensorIO([loss], TensorIO.AVERAGED_SCALAR),
                'layer_losses': TensorIO(layer_losses, TensorIO.AVERAGED_SCALAR),
                'regularization_loss': TensorIO([regularization_loss], TensorIO.AVERAGED_SCALAR),
                'previous_flows': TensorIO(previous_flows, TensorIO.BATCH),
                'final_flow': TensorIO([final_flow], TensorIO.BATCH),
                'valid_mask': TensorIO([valid_mask], TensorIO.BATCH)
            }
            return tensor_io_dict

        # Use a helper function to split the batch across multiple GPUs.
        self.train_op, self.global_step, outputs = create_train_op(
            optimizer, build_network_outputs, [self.images_a, self.images_b, self.flows, self.next_valid_mask], [],
            available_devices=get_available_gpus(), gradient_clipping_norm=self.gradient_clipping_norm,
            add_update_ops=True, verbose=True)

        self.final_flow = outputs['final_flow'].first()
        self.previous_flows = outputs['previous_flows'].tensors
        self.loss = outputs['loss'].first()
        self.layer_losses = outputs['layer_losses'].tensors
        self.regularization_loss = outputs['regularization_loss'].first()
        self.valid_mask = outputs['valid_mask'].first()

    def restore(self):
        """
        Overridden.
        """
        latest_checkpoint = tf.train.latest_checkpoint(self.train_log_dir)
        if latest_checkpoint is not None:
            print('Restoring checkpoint...')
            self.saver.restore(self.session, latest_checkpoint)
        if os.path.isfile(self.npz_save_file):
            print('Restoring weights from npz...')
            self.model.restore_from(self.npz_save_file, self.session)

    def train_for(self, iterations):
        """
        Overridden.
        """
        def update_beholder():
            if self.beholder is not None:
                self.beholder.update(session=self.session)

        def get_feed_dict(dataset_idx):
            dataset = self.datasets[dataset_idx]
            feed_dict = {self.learning_rate: self._get_decayed_lr(dataset.get_learning_rate()), self.training: True}
            feed_dict = {**feed_dict, **dataset.get_train_feed_dict()}
            return feed_dict

        def step_with_logging_and_profiling(dataset_idx):
            feed_dict = get_feed_dict(dataset_idx)
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            _, summ = self.session.run([self.train_op, self.merged_summ], feed_dict=feed_dict,
                                       options=run_options, run_metadata=run_metadata)
            global_step = self._eval_global_step()
            self._log_scalar(self.train_writer, 'learning_rate', feed_dict[self.learning_rate], global_step)
            self.train_writer.add_run_metadata(run_metadata, 'step%d' % global_step, global_step=global_step)
            self.train_writer.add_summary(summ, global_step=global_step)
            self.train_writer.flush()
            save_timeline(run_metadata, self.train_log_dir, detailed=False)
            update_beholder()

        def step_with_logging_only(dataset_idx):
            feed_dict = get_feed_dict(dataset_idx)
            _, summ = self.session.run([self.train_op, self.merged_summ], feed_dict=feed_dict)
            global_step = self._eval_global_step()
            self._log_scalar(self.train_writer, 'learning_rate', feed_dict[self.learning_rate], global_step)
            self.train_writer.add_summary(summ, global_step=global_step)
            self.train_writer.flush()
            update_beholder()

        def step(dataset_idx):
            self.session.run(self.train_op, feed_dict=get_feed_dict(dataset_idx))

        start_t = time.time()
        for i in range(iterations):
            dataset_idx = random.randint(0, len(self.datasets) - 1)
            # Let the trainer warm up for a while before logging a full profile.
            if i == min(iterations - 1, 32):
                step_with_logging_and_profiling(dataset_idx)
            elif i % self.config['log_every'] == 0:
                # Write a summary every once in a while.
                step_with_logging_only(dataset_idx)
            else:
                step(dataset_idx)
            if self.verbose and i % 10 == 0 and i != 0:
                print('Averaged seconds per step: ' + str((time.time() - start_t) / 10), flush=True)
                start_t = time.time()
            self.local_iter += 1

        print('Saving model checkpoint...', flush=True)
        self.model.save_to(self.npz_save_file, self.session)
        save_path = self.saver.save(self.session, os.path.join(self.train_log_dir, 'model.ckpt'),
                                    global_step=self._eval_global_step())
        self.frozen_model.freeze(self.model, self.session)
        print('Model saved in path:', save_path, flush=True)

    def validate(self):
        """
        Overridden.
        """
        dataset_idx = random.randint(0, len(self.datasets) - 1)
        self.datasets[dataset_idx].init_validation_data(self.session)
        while True:
            try:
                feed_dict = self.datasets[dataset_idx].get_validation_feed_dict()
                feed_dict = {**feed_dict, **{self.training: False}}
                summ = self.session.run(self.merged_summ, feed_dict=feed_dict)
                self.valid_writer.add_summary(summ, self._eval_global_step())
            except tf.errors.OutOfRangeError:
                # End of validation epoch.
                break
        self.valid_writer.flush()
        self._validate_sintel(self.sintel_clean_dir, self.sintel_final_dir)

    def _validate_sintel(self, sintel_clean_dir=None, sintel_final_dir=None):
        """
        Logs AEPE for Sintel Clean and Final.
        The provided directories will be used directly with the SintelFlowDataPreprocessor interface.
        """
        def log_sintel(aepe, low, med, hi, global_step, name):
            print('Sintel', name, 'AEPE is %.2f' % aepe, flush=True)
            print('s0-10: %.2f' % low, flush=True)
            print('s10-40: %.2f' % med, flush=True)
            print('s40+: %.2f' % hi, flush=True)
            self._log_scalar(self.valid_writer, 'sintel_' + name + '_AEPE', aepe, global_step)
            self._log_scalar(self.valid_writer, 'sintel_' + name + '_AEPE_s0-10', low, global_step)
            self._log_scalar(self.valid_writer, 'sintel_' + name + '_AEPE_s10-40', med, global_step)
            self._log_scalar(self.valid_writer, 'sintel_' + name + '_AEPE_s40+', hi, global_step)

        self.frozen_model.load()
        global_step = self._eval_global_step()
        if sintel_clean_dir is not None and sintel_clean_dir is not "":
            print('Evaluating on Sintel clean ...', flush=True)
            aepe, low, med, hi = eval_sintel(self.frozen_model, None, sintel_clean_dir, print_output=False)
            log_sintel(aepe, low, med, hi, global_step, 'clean')
        else:
            print('No Sintel clean directory was provided, skipping validation...')
        if sintel_final_dir is not None and sintel_final_dir is not "":
            print('Evaluating on Sintel final ...', flush=True)
            aepe, low, med, hi = eval_sintel(self.frozen_model, None, sintel_final_dir, print_output=False)
            log_sintel(aepe, low, med, hi, global_step, 'final')
        else:
            print('No Sintel final directory was provided, skipping validation...', flush=True)
        print('Completed validation on Sintel.', flush=True)

    def _log_scalar(self, writer, tag, value, step):
        summary = tf.Summary(value=[tf.Summary.Value(tag=tag, simple_value=value)])
        writer.add_summary(summary, step)
        writer.flush()

    def _eval_global_step(self):
        return self.session.run(self.global_step)

    def _make_summaries(self):
        with tf.name_scope('summaries'):
            # Get trainable variables for histograms.
            for var in tf.trainable_variables(scope=self.model.name):
                tf.summary.histogram(var.name + '_hist', var)

            tf.summary.scalar('total_loss', self.loss)
            tf.summary.scalar('regularization_loss', self.regularization_loss)
            for i, layer_loss in enumerate(self.layer_losses):
                tf.summary.scalar('layer_' + str(i) + '_loss', layer_loss)
            for i, previous_flow in enumerate(self.previous_flows):
                tf.summary.histogram('flow_' + str(i) + '_hist', previous_flow)
                tf.summary.image('flow_' + str(i), get_tf_flow_visualization(previous_flow))
            tf.summary.image('image_a', tf.clip_by_value(self.images_a, 0.0, 1.0))
            tf.summary.image('image_b', tf.clip_by_value(self.images_b, 0.0, 1.0))
            tf.summary.image('valid_mask', self.valid_mask)
            if self.final_flow is not None:
                tf.summary.image('final_flow', get_tf_flow_visualization(self.final_flow))
                tf.summary.histogram('final_flow_hist', self.final_flow)
            tf.summary.image('gt_flow', get_tf_flow_visualization(self.flows))
            tf.summary.histogram('gt_flow_hist', self.flows)
            self.merged_summ = tf.summary.merge_all()

            self.train_writer = tf.summary.FileWriter(self.train_log_dir, self.session.graph)
            self.valid_writer = tf.summary.FileWriter(self.valid_log_dir)
            self._write_config_summary(self.train_writer)

    def _get_decayed_lr(self, lr):
        """
        :param lr: Float. Original learning rate.
        :return: Learning rate decayed based on the number of local iterations this trainer has made.
        """
        if self.lr_half_life is None or self.lr_half_life < 0:
            return lr
        var = self.lr_half_life * self.lr_half_life / np.log(4)
        exp = np.exp(-self.local_iter * self.local_iter / (2 * var))
        return lr * exp
