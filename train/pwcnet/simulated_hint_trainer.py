import tensorflow as tf
from common.utils.flow import get_tf_flow_visualization
from common.utils.multi_gpu import get_available_gpus, TensorIO, create_train_op
from pwcnet.hinted_model import HintedPWCNet
from pwcnet.losses.loss import create_training_hints
from train.pwcnet.trainer import PWCNetTrainer


class PWCNetSimulatedHintTrainer(PWCNetTrainer):
    def __init__(self, model, datasets, session, config, verbose=True, lr_half_life=-1):
        """
        Expects model to be a hinted pwcnet.
        """
        self.initial_flow = None
        self.initial_loss = None
        self.flow_errors = None
        self.initial_layer_losses = None
        self.loss_masks = None
        self.final_flows = None
        self.flow_hint_masks = None
        self.flow_hints = None
        super().__init__(model, datasets, session, config, verbose=verbose, lr_half_life=lr_half_life)

        # Frozen model saving.
        self.frozen_model = HintedPWCNet.create_frozen_model(self.frozen_model_dir)

    def _create_ops(self):
        """
        Overridden.
        """
        optimizer = tf.train.AdamOptimizer(self.learning_rate)

        def build_network_outputs(images_a, images_b, flows, valid_mask):
            B, H, W, C = tf.unstack(tf.shape(flows))

            with tf.name_scope('initial_network'):
                # Set the initial hint to 0.
                zero_hint = tf.zeros_like(flows)
                zero_mask = tf.zeros(shape=(B, H, W, 1), dtype=tf.float32)
                self.model.set_flow_hint(zero_hint, zero_mask)

                # Get the first train network.
                initial_flow, initial_previous_flows, _ = self.model.get_forward(images_a, images_b,
                                                                                 reuse_variables=tf.AUTO_REUSE,
                                                                                 training=self.training)
                if self.config['fine_tune']:
                    initial_loss, initial_layer_losses, initial_regularization_loss = self.model.get_fine_tuning_loss(
                        initial_previous_flows, flows, loss_mask=valid_mask)
                else:
                    initial_loss, initial_layer_losses, initial_regularization_loss = self.model.get_training_loss(
                        initial_previous_flows, flows, loss_mask=valid_mask)

            previous_previous_flows = initial_previous_flows
            all_losses = [initial_loss]
            all_regularization_losses = [initial_regularization_loss]
            flow_errors = []
            loss_masks = []
            final_flows = []
            keep_masks = []
            flow_hints = []
            previous_flows = None
            layer_losses = None
            keep_mask = None
            num_hint_iterations = self.config['hint_iterations'] if 'hint_iterations' in self.config else 1
            with tf.name_scope('resized_gt'):
                # Resize the ground truth to match the last flow size.
                _, last_flow_height, last_flow_width, _ = tf.unstack(tf.shape(previous_previous_flows[-1]))
                gt_resized = tf.image.resize_nearest_neighbor(flows, [last_flow_height, last_flow_width],
                                                              align_corners=True)
            num_hints = tf.random.uniform(shape=[], minval=1, maxval=self.config['num_hints'], dtype=tf.int32,
                                          name='num_hints')
            for i in range(num_hint_iterations):
                with tf.name_scope('hint_iteration_' + str(i)):
                    with tf.name_scope('loss_based_hints_' + str(i)):
                        # Unscale to match GT magnitude.
                        last_flow = previous_previous_flows[-1]  # Not resized for output.
                        last_flow_unscaled = last_flow / self.model.flow_scaling
                        keep_mask, hints, loss_mask, flow_error = create_training_hints(
                            last_flow_unscaled, gt_resized, flows, keep_mask, self.config['flow_error_hint_thresh'],
                            num_hints, self.config['hint_noise_std_dev'], valid_mask,
                            probablistic_sampling=self.config['hint_probablistic_sampling'])

                    with tf.name_scope('hinted_network_' + str(i)):
                        # Set the new hint.
                        self.model.set_flow_hint(hints, keep_mask)

                        # Get the second train network.
                        final_flow, previous_flows, _ = self.model.get_forward(images_a, images_b,
                                                                               reuse_variables=tf.AUTO_REUSE,
                                                                               training=self.training)
                        # Get loss without regularization.
                        if self.config['fine_tune']:
                            loss, layer_losses, regularization_loss = self.model.get_fine_tuning_loss(
                                previous_flows, flows, loss_mask=loss_mask * valid_mask)
                        else:
                            loss, layer_losses, regularization_loss = self.model.get_training_loss(
                                previous_flows, flows, loss_mask=loss_mask * valid_mask)

                    previous_previous_flows = previous_flows
                    all_losses.append(loss)
                    all_regularization_losses.append(regularization_loss)
                    loss_masks.append(loss_mask)
                    flow_errors.append(flow_error)
                    final_flows.append(final_flow)
                    keep_masks.append(keep_mask)
                    flow_hints.append(hints)

            with tf.name_scope('combined_loss'):
                loss = tf.add_n(all_losses) / (num_hint_iterations + 1)
                regularization_loss = tf.add_n(all_regularization_losses) / (num_hint_iterations + 1)

            tensor_io_dict = {
                'initial_loss': TensorIO([initial_loss], TensorIO.AVERAGED_SCALAR),
                'loss': TensorIO([loss], TensorIO.AVERAGED_SCALAR),
                'layer_losses': TensorIO(layer_losses, TensorIO.AVERAGED_SCALAR),
                'initial_layer_losses': TensorIO(initial_layer_losses, TensorIO.AVERAGED_SCALAR),
                'regularization_loss': TensorIO([regularization_loss], TensorIO.AVERAGED_SCALAR),
                'previous_flows': TensorIO(previous_flows, TensorIO.BATCH),
                'initial_flow': TensorIO([initial_flow], TensorIO.BATCH),
                'final_flows': TensorIO(final_flows, TensorIO.BATCH),
                'flow_hints': TensorIO(flow_hints, TensorIO.BATCH),
                'flow_hint_masks': TensorIO(keep_masks, TensorIO.BATCH),
                'flow_errors': TensorIO(flow_errors, TensorIO.BATCH),
                'loss_masks': TensorIO(loss_masks, TensorIO.BATCH),
                'valid_mask': TensorIO([valid_mask], TensorIO.BATCH)
            }
            return tensor_io_dict

        # Use a helper function to split the batch across multiple GPUs.
        self.train_op, self.global_step, outputs = create_train_op(
            optimizer, build_network_outputs, [self.images_a, self.images_b, self.flows, self.next_valid_mask], [],
            available_devices=get_available_gpus(), verbose=True)

        self.initial_flow = outputs['initial_flow'].first()
        self.final_flow = None  # Use final_flows instead.
        self.previous_flows = outputs['previous_flows'].tensors
        self.initial_loss = outputs['initial_loss'].first()
        self.loss = outputs['loss'].first()
        self.layer_losses = outputs['layer_losses'].tensors
        self.initial_layer_losses = outputs['initial_layer_losses'].tensors
        self.regularization_loss = outputs['regularization_loss'].first()
        self.flow_errors = outputs['flow_errors'].tensors
        self.flow_hint = None  # Use flow_hints instead.
        self.flow_hints = outputs['flow_hints'].tensors
        self.flow_hint_mask = None  # Use flow_hint_masks instead.
        self.flow_hint_masks = outputs['flow_hint_masks'].tensors
        self.loss_masks = outputs['loss_masks'].tensors
        self.final_flows = outputs['final_flows'].tensors
        self.valid_mask = outputs['valid_mask'].first()

    def _make_summaries(self):
        """
        Overridden.
        """
        with tf.name_scope('simulated_hint_summaries'):
            tf.summary.scalar('initial_loss', self.initial_loss)
            tf.summary.image('initial_flow', get_tf_flow_visualization(self.initial_flow))
            for i, flow_error in enumerate(self.flow_errors):
                tf.summary.image('flow_error_' + str(i), flow_error)
            for i, loss_mask in enumerate(self.loss_masks):
                tf.summary.image('loss_mask_' + str(i), loss_mask)
            for i, final_flow in enumerate(self.final_flows):
                tf.summary.histogram('final_flow_' + str(i) + '_hist', final_flow)
                tf.summary.image('final_flow_' + str(i), get_tf_flow_visualization(final_flow))
            for i, flow_hint in enumerate(self.flow_hints):
                tf.summary.image('flow_hint_' + str(i), get_tf_flow_visualization(flow_hint))
            for i, layer_loss in enumerate(self.initial_layer_losses):
                tf.summary.scalar('initial_layer_' + str(i) + '_loss', layer_loss)
            for i, flow_hint_mask in enumerate(self.flow_hint_masks):
                tf.summary.image('flow_hint_mask_' + str(i), flow_hint_mask)

        # Create all other PWC Net summaries and merge them together.
        super()._make_summaries()
